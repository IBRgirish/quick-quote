<?php

set_time_limit (0);
ini_set('memory_limit', '-1');
ini_set("post_max_size","200M");  
ini_set("upload_max_filesize","200M"); 
//ini_set("upload_tmp_dir","/tmp"); 

date_default_timezone_set('America/Los_Angeles');
$timestamp = date('Y-m-d H-i-s'); 
// A list of permitted file extensions
$allowed = array('png', 'jpg', 'gif','zip','rar','jpeg', 'txt', 'doc','docx','xls','xlsx', 'ppt','pdf');

//print_r($_FILES);
if(isset($_FILES['upl']) && $_FILES['upl']['error'] == 0){

	$extension = pathinfo($_FILES['upl']['name'], PATHINFO_EXTENSION);

	$path_parts = pathinfo($_FILES["upl"]["name"]);
	$new_filename = $path_parts['filename'].'_'.$timestamp.'.'.$path_parts['extension'];

//print_r($new_filename);
	
	 if(!in_array(strtolower($extension), $allowed)){
		echo '';
		exit;
	} 

	if(move_uploaded_file($_FILES['upl']['tmp_name'], 'uploads/docs/'.$new_filename)){
		//echo '{"status":"success","file_name":"'.$new_filename.'"}';
		echo $new_filename;
		exit;
	}
}

echo '{"status":"error"}';
exit;