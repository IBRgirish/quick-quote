var myApp = angular.module('qq',['angular-price-format'],function($interpolateProvider){
	/*$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');	*/
});

	  myApp.directive('priceFormat', function () {
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function (scope, element, attrs, ngModelCtrl) {
					
					element.priceFormat({
		                    prefix: '$ ',
		                    centsLimit: 0,
		                    thousandsSeparator: ','
		            });
				}
			}
		});
		
  myApp.directive('jqdatepicker', function () {
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function (scope, element, attrs, ngModelCtrl) {
					
					element.datepicker({
						dateFormat: 'mm/dd/yyyy',
						autoclose: true,
						onSelect: function() {                   
						}
					}).on('changeDate', function(e){ 
						$('.datepicker').hide();
						_endDate = new Date(e.date.getTime()); //get new end date						
						$('#'+attrs.end).datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
					})
					 
				}
			};
		});
		
		
  myApp.directive('jdatepickers', function () {
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function (scope, element, attrs, ngModelCtrl) {
					var startDate = new Date('01/01/2012');
                    var FromEndDate = new Date();
					element.datepicker({
						dateFormat: 'mm/dd/yyyy',
						startDate: '01/01/1950',
                        endDate: FromEndDate, 
						autoclose: true,
						onSelect: function() {                   
						}
					})					 
				}
			};
		});

myApp.service('myServ', function() {
	  this.state_pip='';	 
    });
insuredCtrl.$inject = ['$scope', 'myServ','$window'];
function insuredCtrl($scope,myServ,$window){
	$scope.rows = [];
	$scope.rows['in_garags'] = [];
	$scope.rows['filing'] = [];
	$scope.rows['filing_nos'] = [];
	$scope.rows['owners'] = [];
	$scope.rows['certificates'] = [];
	$scope.rows['sups'] = [];
	$scope.rows['supps'] = [];	
	$scope.rows['suppss'] = [];
	$scope.rows['persons'] = [];
	$scope.rows['exclusions'] = [];
	var state=$('#in_state').val();
	
	if(state=='CA'){
	$scope.myServ=myServ;
	myServ.state_pip=state;
	}
	$scope.insured_info = JSON.parse($window.insured_info);
	if($scope.insured_info==''){
		 $scope.insured_info = null;
		}
		
	if($scope.insured_info!=null){
	  
	  $scope.insured_info =$scope.insured_info[0];
	}else{		
	  $scope.insured_info = {in_this_is:'Corporation',in_garag_in:'Yes',in_garag_fax:'No',filings_need:'No',in_country:'US',business_years:'New venture',in_state:'CA'};
      
	}

	
	$scope.in_garag = JSON.parse($window.ins_info);
	
	if($scope.in_garag==''){
		 $scope.in_garag = null;
		}
	if($scope.in_garag!=null){			 
	      
	angular.forEach($scope.in_garag, function(values, index_1) {
	
	$scope.rows['in_garags'].push(values);	
		
	$scope.ins_phone = JSON.parse($window.ins_phone);
	if($scope.ins_phone==''){
		 $scope.ins_phone = null;
		}		
	if($scope.ins_phone!=null){
	   angular.forEach($scope.ins_phone, function(value, index) {
		   if(values['in_ga_id']==value['parent_id']){	
	          $scope.rows['in_garags'][index_1]['phones'].push(value);
		   }
	});
	}else{		
	    $scope.rows['in_garags'][index_1]['phones'].push({sequence:'',pri_country:1,code_type:'1',phone_type:'Office'});
	}
	$scope.ins_email = JSON.parse($window.ins_email);
	if($scope.ins_email==''){
		 $scope.ins_email = null;
		}
	if($scope.ins_email!=null){
	   angular.forEach($scope.ins_email, function(value, index) {	
	      if(values['in_ga_id']==value['parent_id']){	 
	         $scope.rows['in_garags'][index_1]['emails'].push(value);	
		  }
	});
	}else{		
	    $scope.rows['in_garags'][index_1]['emails'].push({sequence:'',priority_email:1,email_type:'Office'});
	  } 
	  
	 $scope.ins_fax = JSON.parse($window.ins_fax);
	if($scope.ins_fax==''){
		 $scope.ins_fax = null;
		}
	if($scope.ins_fax!=null){
	   angular.forEach($scope.ins_fax, function(value, index) {	
	    if(values['in_ga_id']==value['parent_id']){	
	      $scope.rows['in_garags'][index_1]['faxs'].push(value);
		}
	});
	}else{		
	    $scope.rows['in_garags'][index_1]['faxs'].push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'});
	}
   });
		  
	   }else{
		 var indexs = $scope.rows['in_garags'].length;
		
	     $scope.rows['in_garags'].push({sequence:'',phones:[],emails:[],faxs:[],in_garag_fax:'No',in_ga_state:'CA',in_ga_country:'US'});
	     $scope.rows['in_garags'][indexs]['phones'].push({sequence:'',pri_country:1,code_type:'1',phone_type:'Office'});
		 $scope.rows['in_garags'][indexs]['emails'].push({sequence:'',priority_email:1,email_type:'Office'});
		 $scope.rows['in_garags'][indexs]['faxs'].push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'});
	  
	   
	   }
	$scope.rows['filing_nos'].push({sequence:'',in_fi_state:'CA'});
	
	
	
	
	$scope.filing = JSON.parse($window.filing);
	if($scope.filing==''){
		 $scope.filing = null;
		}
	if($scope.filing!=null){
		angular.forEach($scope.filing, function(value, index) {	
	    $scope.rows['filing'].push(value);
	});
	
	}else{
	$scope.rows['filing'].push({sequence:'',in_fi_state:'CA'});	
	}
	
	$scope.filings = JSON.parse($window.filings);
	if($scope.filings==''){
		 $scope.filings = null;
		}
	if($scope.filings!=null){
		$scope.rows['filing_nos'] = [];
		angular.forEach($scope.filings, function(value, index) {	
	    $scope.rows['filing_nos'].push(value);
	});
	
	}
	
	$scope.n_owners = 0;
	$scope.add_row = function(type){	
		
		if(type == 'owners'){
		$scope.n_owners = $scope.rows[type].length;
		}else if(type == 'in_garags'){
		$scope.rows['in_garags'].push({sequence:'',phones:[],emails:[],faxs:[],in_garag_fax:'No',in_ga_state:'CA',in_ga_country:'US'});
		var indexs = $scope.rows['in_garags'].length;	
	    $scope.rows['in_garags'][indexs-1]['phones'].push({sequence:'',pri_country:1,code_type:'1',phone_type:'Office'});
	    $scope.rows['in_garags'][indexs-1]['emails'].push({sequence:'',priority_email:1,email_type:'Office'});
	    $scope.rows['in_garags'][indexs-1]['faxs'].push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'});
		setTimeout(function(){
			  $('input').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 });
				  $('select').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 }); 
		 },200);
		
		}else{
		 $scope.rows[type].push({sequence:''});	
		}
	}
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rows[type].splice(index, 1);
		}
		if(type == 'owners'){
			$scope.n_owners = $scope.rows[type].length;
		}
    }
	
	$scope.add_row_ = function(type){		
		var score = $scope.n_owners;
		var length = $scope.rows[type].length;		
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows[type].splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		   $scope.rows[type].push({sequence:''});
	       data_dictionary();

		 i++;
		} 	
	}
	
	$scope.insured_emails = [];
	$scope.insured_phones = [];
	$scope.insured_faxs = [];
	
	$scope.insured_phone=JSON.parse($window.insured_phone);
	if($scope.insured_phone==''){
		 $scope.insured_phone = null;
		}
	if($scope.insured_phone!=null){
		
	    angular.forEach($scope.insured_phone, function(value, index) {	
	    $scope.insured_phones.push(value);	
	});
	}else{
		
	    $scope.insured_phones.push({sequence:'',pri_country:1,code_type:'1',phone_type:'Office'});
	}	
	
	$scope.insured_email=JSON.parse($window.insured_email);
	if($scope.insured_email==''){
		 $scope.insured_email = null;
		}
	if($scope.insured_email!=null){
		$scope.insured_email=JSON.parse($window.insured_email);
	    angular.forEach($scope.insured_email, function(value, index) {	
	    $scope.insured_emails.push(value);	
	});
	}else{
	    $scope.insured_emails.push({sequence:'',priority_email:1,email_type:'Office'});
	}
	
	$scope.insured_fax=JSON.parse($window.insured_fax);
	if($scope.insured_fax==''){
		 $scope.insured_fax = null;
		}
	if($scope.insured_fax != null){	
		
	    angular.forEach($scope.insured_fax, function(value, index) {	
	    $scope.insured_faxs.push(value);	
	});
	}else{
	    $scope.insured_faxs.push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'});
	}
	
	
	$scope.addRowPhone = function(type,index){
	if(type=="in_garags"){		
	    var len_in_garags=$scope.rows[type][index]['phones'].length+1;			   
		$scope.rows[type][index]['phones'].push({sequence:'',pri_country:len_in_garags,code_type:'1',phone_type:'Office'});

           
	}else{
		var length=$scope.insured_phones.length+1;
		$scope.insured_phones.push({sequence:'',pri_country:length,code_type:'1',phone_type:'Office'});	
		}
	}
	$scope.remove_row_phone = function(index,type,parent_index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
			if(type=="in_garags")	{	
		    $scope.rows[type][parent_index]['phones'].splice(index, 1);
	        }else{
		    $scope.insured_phones.splice(index, 1);
			}
		}		
    }
	$scope.addRowEmail = function(type,index){	
	if(type=="in_garags")	{
		 var len_in_garags=$scope.rows[type][index]['emails'].length+1;	
		$scope.rows[type][index]['emails'].push({sequence:'',priority_email:len_in_garags,email_type:'Office'});
	 }else{
		var length=$scope.insured_emails.length+1;
		$scope.insured_emails.push({sequence:'',priority_email:length,email_type:'Office'});	
		}
	}
	$scope.remove_row_email = function(index,type,parent_index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
			if(type=="in_garags")	{	
		    $scope.rows[type][parent_index]['emails'].splice(index, 1);
	        }else{
		    $scope.insured_emails.splice(index, 1);
	      }
		}		
    }
	$scope.addRowFax = function(type,index){	
	if(type=="in_garags")	{	
	    var len_in_garags=$scope.rows[type][index]['faxs'].length+1;
		$scope.rows[type][index]['faxs'].push({sequence:'',country_fax:len_in_garags,code_type_fax:'1',fax_type:'Office'});
	 }else{
		 var length=$scope.insured_faxs.length+1;
		$scope.insured_faxs.push({sequence:'',country_fax:length,code_type_fax:'1',fax_type:'Office'});	
		}		
	}
	$scope.remove_row_fax = function(index,type,parent_index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
			if(type=="in_garags")	{	
		    $scope.rows[type][parent_index]['faxs'].splice(index, 1);
	        }else{
		    $scope.insured_faxs.splice(index, 1);
			}
		}		
    }
	$scope.operation_info=function(item){
		   
		   if(item=='Yes'){
			   var nature_of_business = $('#nature_of_business').val();			  
			   $('.nature_bus').val(nature_of_business);
			   }
		
		  }
	$scope.checkLicense=function(state){
			
	$scope.myServ=myServ;
	myServ.state_pip=state;
	if(state!='CA'){
		$( "select.pip_changes_option option:selected").val('Rejected');
   
	 }
		 var baseurl = $("#baseurls").val();
		 $.ajax({
						url: baseurl+'quote/checkLicense',
						type: 'post',
						data : {state: state},
						success: function(json) {						
							
							if(json == 1)
								{
								 	$("input.myclass").attr("disabled", false);
								   	$("button.myclass").attr("disabled", false);	
								    $("textarea.myclass").attr("disabled", false);
									$("select.myclass").attr("disabled", false);
									$("#error_msg").html("");
							      chenge_pip_um_uim();
								}
								else if(json == 0 || json == 2)
								{
									
							 	$("input.myclass").attr("disabled", true);
							 	$("button.myclass").attr("disabled", true);
							    $("textarea.myclass").attr("disabled", true);
							    $("select.myclass").attr("disabled", true);
								 $(window).scrollTop(0);
								 if(json == 0)
								 {
								$("#error_msg").html("<div class='alert alert-success' style='margin:0px 100px 15px; width:84%;'><strong>Info :</strong> Your License Expire for this state..!!</div>")
								 }
								  if(json == 2){
								$("#error_msg").html("<div class='alert alert-success' style='margin:0px 100px 15px; width:84%;'><strong>Info :</strong> You don't have license for this state..!!</div>")
								 }
								
								}
								
	    
	     
        	var street= $('#in_address').val();		
			var city= $('#in_city').val();
			var state= $('#in_state').val();
			var county= $('#in_county').val();
			var zip= $('#in_zip').val();
			var country= $('#in_country').val();			
			var address=street+' '+city+' '+state+' '+county+' '+country+' '+zip;

			if($.trim(address).length > 0 ){
				
				var truck_no=$("input[name=truck_no]").val();
				var tractor_no=$("input[name=tractor_no]").val();				
				var trailer_no=$("input[name=trailer_no]").val();
				var other_no=$("input[name=other_no]").val();
				var map='';
				var map_1='';
				var map_2='';
				var map_3='';
	        	var address_data = address;		
				var r_ope=$('#radius_of_operation').val();
				var radiuss=r_ope*1609.344/10;					
				var geocoder = new google.maps.Geocoder();
				geocoder.geocode({address: address_data}, function(results, status) {
				  centre = results[0].geometry.location;				
				  var mapOptions = {
					  zoom: 8,
					  center: centre,
					  mapTypeId: google.maps.MapTypeId.ROADMAP
				  };
				 
				  if(truck_no > 0){
				     for(var address_id=0;address_id < truck_no;address_id++){	
								 
				      map = new google.maps.Map(document.getElementById('map-address-'+ address_id), mapOptions);
					  if(map!=''){
				  var marker = new google.maps.Marker({
					  map: map,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map);
				  }
					
					  }
				  }
				  if(tractor_no > 0){
					  for(var address_id=0;address_id < tractor_no;address_id++){
						  
				  map_1 = new google.maps.Map(document.getElementById('t_map-address-'+ address_id), mapOptions);
					 	  if(map_1!=''){
				  var marker = new google.maps.Marker({
					  map: map_1,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map_1);
				  }
					 
					  }
				  }
				  if(trailer_no > 0){
					  for(var address_id=0;address_id < trailer_no;address_id++){
				  map_2 = new google.maps.Map(document.getElementById('tr_map-address-'+ address_id), mapOptions);
					 var marker = new google.maps.Marker({
					  map: map_2,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map_2);
				  
					  }
				  }
				  if(other_no > 0){
					  for(var address_id=0;address_id < other_no;address_id++){
				  map_3 = new google.maps.Map(document.getElementById('o_map-address-'+ address_id), mapOptions);
					var marker = new google.maps.Marker({
					  map: map_3,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map_3);
				  
					  
					  }
				  }
				
	    });
	   }
    
	 
						}
						
		 	});
		
		
		}
		
		
		
		  $scope.google_address=function(){	    
	    
        	var street= $('#in_address').val();		
			var city= $('#in_city').val();
			var state= $('#in_state').val();
			var county= $('#in_county').val();
			var zip= $('#in_zip').val();
			var country= $('#in_country').val();			
			var address=street+' '+city+' '+state+' '+county+' '+country+' '+zip;

			if($.trim(address).length > 0 ){				
				var truck_no=$("input[name=truck_no]").val();
				var tractor_no=$("input[name=tractor_no]").val();				
				var trailer_no=$("input[name=trailer_no]").val();
				var other_no=$("input[name=other_no]").val();
				var map='';
				var map_1='';
				var map_2='';
				var map_3='';
	        	var address_data = address;		
			
				var r_ope=$('#radius_of_operation').val();
				var radiuss=r_ope*1609.344/10;					
				var geocoder = new google.maps.Geocoder();
				geocoder.geocode({address: address_data}, function(results, status) {
				  centre = results[0].geometry.location;				
				  var mapOptions = {
					  zoom: 8,
					  center: centre,
					  mapTypeId: google.maps.MapTypeId.ROADMAP
				  };
				 
				  if(truck_no > 0){
				     for(var address_id=0;address_id < truck_no;address_id++){	
								 
				      map = new google.maps.Map(document.getElementById('map-address-'+ address_id), mapOptions);
					  if(map!=''){
				  var marker = new google.maps.Marker({
					  map: map,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map);
				  }
					
					  }
				  }
				  if(tractor_no > 0){
					  for(var address_id=0;address_id < tractor_no;address_id++){
						  
				  map_1 = new google.maps.Map(document.getElementById('t_map-address-'+ address_id), mapOptions);
					 	  if(map_1!=''){
				  var marker = new google.maps.Marker({
					  map: map_1,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map_1);
				  }
					 
					  }
				  }
				  if(trailer_no > 0){
					  for(var address_id=0;address_id < trailer_no;address_id++){
				  map_2 = new google.maps.Map(document.getElementById('tr_map-address-'+ address_id), mapOptions);
					 var marker = new google.maps.Marker({
					  map: map_2,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map_2);
				  
					  }
				  }
				  if(other_no > 0){
					  for(var address_id=0;address_id < other_no;address_id++){
				  map_3 = new google.maps.Map(document.getElementById('o_map-address-'+ address_id), mapOptions);
					var marker = new google.maps.Marker({
					  map: map_3,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map_3);
				  
					  
					  }
				  }
				
	    });
	   }
    
	 }
	$scope.commodities_haulted_v=function(val){	
		
		  $('.commhauled2').val(val);
		} 
	$scope.add_row_i = function(type,scores,index){
		
		if(type=='certificates'){
		var score = $scope.insured_t.cert_num;	
		
		}
		else if(type=='sups'){
		var score = $scope.insured_add.supplement_val;		
		}
		else if(type=='supps'){
		var score = $scope.insured_add.sup_val;		
		}
		else if(type=='suppss'){
		var score = scores;
		
		}
		else if(type=='persons'){
		var score = $scope.insured_add.persons_val;
				
		}
		else if(type=='exclusions'){
			
		var score = $scope.insured_add.ex_id;
		
		}
		if(type=='suppss'){
		var length = $scope.rows['persons'][index]['suppss'].length;
		}else{
		var length = $scope.rows[type].length;
		}
		var parent_index=$scope.rows['persons'].length;	
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				if(type=='suppss'){
				$scope.rows['persons'][index]['suppss'].splice($scope.toRemove, 1);
				
	data_dictionary();
				}else{
				$scope.rows[type].splice($scope.toRemove, 1);
				}
				score++;
			}
		}
		while(i <= score){
			if(type=='suppss'){				
			 $scope.rows['persons'][index]['suppss'].push({sequence:''});
			}else if(type=='persons'){
			 $scope.rows[type].push({sequence:'','suppss':[],reg_r1:'No'});	
			}else{
		     $scope.rows[type].push({sequence:''});	
			}
		   	
		 i++;
		} 	
	}
	$scope.getState=function(val, id, name, attr){
		 var baseurl = $("#baseurls").val();
	$.ajax({
		type: "POST",
		url: baseurl+"index.php/application_form/getState", 
		data:"id="+val+"&name="+name+"&attr="+attr+"",
		success: function(json) 
		{
		   
		   $("#"+id).html(json);	  
		 	
		}
	});			
		
}   
    $scope.rows['datas']=[];
	$scope.rows['datas_p']=[];
	$scope.rows['datas_e']=[];
	$scope.rows['datas_s']=[];
	$scope.rows['datas_y']=[];
	$scope.rows['datas_emp']=[];
	
	$scope.rows['datas_gh']=[];
	$scope.rows['datas_p_gh']=[];
	$scope.rows['datas_e_gh']=[];
	$scope.rows['datas_s_gh']=[];
	$scope.rows['datas_y_gh']=[];
	$scope.rows['datas_emp_gh']=[];

    $scope.insured_b = JSON.parse($window.insured_b);
	if($scope.insured_b==''){
		 $scope.insured_b = null;
		}
		
	if($scope.insured_b!=null){
	  
	  $scope.insured_b =$scope.insured_b[0];
	}else{		
	  $scope.insured_b = {exp_state:'CA',insured_b:''};
      
	}
	
	$scope.insured_gh8 = JSON.parse($window.insured_gh8);
	if($scope.insured_gh8==''){
		 $scope.insured_gh8 = null;
		}
		
	if($scope.insured_gh8!=null){
	  
	  $scope.insured_gh8 =$scope.insured_gh8[0];
	}else{		
	  $scope.insured_gh8 = {applicant_entity:'Yes',applicant_state:'CA'};
      
	}
	
	$scope.insured_t = JSON.parse($window.insured_t);
	if($scope.insured_t==''){
		 $scope.insured_t = null;
		}
		
	if($scope.insured_t!=null){
	  
	  $scope.insured_t =$scope.insured_t[0];
	}else{		
	  $scope.insured_t = {};
      
	}
	
	$scope.insured_add = JSON.parse($window.insured_add);
	if($scope.insured_add==''){
		 $scope.insured_add = null;
		}
		
	if($scope.insured_add!=null){	  
	  $scope.insured_add =$scope.insured_add[0];
	}else{		
	  $scope.insured_add = {};
      
	}
	
	$scope.certificate = JSON.parse($window.certificate);
	if($scope.certificate==''){
		 $scope.certificate = null;
		}
	if($scope.certificate!=null){
		angular.forEach($scope.certificate, function(value, index) {	
	    $scope.rows['certificates'].push(value);
	});
	
	}
	
	$scope.insured_gh6 = JSON.parse($window.insured_gh6);
	if($scope.insured_gh6==''){
		 $scope.insured_gh6 = null;
		}
	if($scope.certificate!=null){
		angular.forEach($scope.insured_gh6, function(value, index) {	
	    $scope.rows['sups'].push(value);
		
	});
	
	}
	
	$scope.insured_gh61 = JSON.parse($window.insured_gh61);
	if($scope.insured_gh61==''){
		 $scope.insured_gh61 = null;
		}
	if($scope.insured_gh61!=null){
		angular.forEach($scope.insured_gh61, function(value, index) {	
	    $scope.rows['supps'].push(value);
	});
	
	}
	
	
	
	$scope.insured_gh9 = JSON.parse($window.insured_gh9);
	if($scope.insured_gh9==''){
		 $scope.insured_gh9 = null;
		}
	if($scope.insured_gh9!=null){
		angular.forEach($scope.insured_gh9, function(value, per_index) {	
	    $scope.rows['persons'].push(value);
		if(value['reg_r1']=='Yes'){
	$scope.insured_gh62 = JSON.parse($window.insured_gh62);
		
	if($scope.insured_gh62==''){
		 $scope.insured_gh62 = null;
		}
	if($scope.insured_gh62!=null){
		angular.forEach($scope.insured_gh62, function(value, index) {
	
		$scope.rows['persons'][per_index]['suppss'].push(value);	
	    //$scope.rows['suppss'].push(value);
	         });
	
	      }
		}
	 });
	
	}
    $scope.insured_gh11 = JSON.parse($window.insured_gh11);
	if($scope.insured_gh11==''){
		 $scope.insured_gh11 = null;
		}
	if($scope.insured_gh11!=null){
		angular.forEach($scope.insured_gh11, function(value, index) {	
	    $scope.rows['exclusions'].push(value);
	});
	
	}
	
	$scope.insured_gh5 = JSON.parse($window.insured_gh5);
	if($scope.insured_gh5==''){
		 $scope.insured_gh5 = null;
		}
	if($scope.insured_gh5!=null){	  
	  $scope.insured_gh5 =$scope.insured_gh5[0];
	}else{		
	  $scope.insured_gh5 = {haul_agree:'No',haul_attach:'No',hauler_Own:'No',hauler_team:'No',hauler_Vehi:'No',hauler_drivers:'No',hauler_rest:'No',hauler_roth:'No' ,hauler_haul:'No',haul_equi:'No',haul_agreement:'No',haul_ope:'No',haul_yins:'No',ins_pol:'No',subcon_y:'No',subcon_emp_y:'No',sub_haul_y:'No',audit_y:'No',vehicles_y:'No',ride_y:'No',driver_y:'No',activity_y:'No',prior_y:'No'};
      
	}
	
		
	$scope.insured_gh51 = JSON.parse($window.insured_gh51);
	if($scope.insured_gh51==''){
		 $scope.insured_gh51 = null;
		}
	if($scope.insured_gh51!=null){	  
	  $scope.insured_gh51 =$scope.insured_gh51[0];
	}else{		
	  $scope.insured_gh51 = {haul_agree:'No',haul_attach:'No',hauler_Own:'No',hauler_team:'No',hauler_Vehi:'No',hauler_drivers:'No',hauler_rest:'No',hauler_roth:'No' ,hauler_haul:'No',haul_equi:'No',haul_agreement:'No',haul_ope:'No',haul_yins:'No',ins_pol:'No',subcon_y:'No',subcon_emp_y:'No',sub_haul_y:'No',audit_y:'No',vehicles_y:'No',ride_y:'No',driver_y:'No',activity_y:'No',prior_y:'No'};
      
	}
	

     $scope.insured_gh5b = JSON.parse($window.insured_gh5b);
	if($scope.insured_gh5b==''){
		 $scope.insured_gh5b = null;
		}
	if($scope.insured_gh5b!=null){
		angular.forEach($scope.insured_gh5b, function(value, index) {	
	    $scope.rows['datas'].push(value);
	});
	
	}

     $scope.insured_gh5c = JSON.parse($window.insured_gh5c);
	if($scope.insured_gh5c==''){
		 $scope.insured_gh5c = null;
		}
	if($scope.insured_gh5c!=null){
		angular.forEach($scope.insured_gh5c, function(value, index) {	
	    $scope.rows['datas_p'].push(value);
	});
	
	}
	
	   $scope.insured_gh5d = JSON.parse($window.insured_gh5d);
	if($scope.insured_gh5d==''){
		 $scope.insured_gh5d = null;
		}
	if($scope.insured_gh5d!=null){
		angular.forEach($scope.insured_gh5d, function(value, index) {	
	    $scope.rows['datas_e'].push(value);
	});
	
	}
	
	
	$scope.insured_gh5e = JSON.parse($window.insured_gh5e);
	if($scope.insured_gh5e==''){
		 $scope.insured_gh5e = null;
		}
	if($scope.insured_gh5e!=null){
		angular.forEach($scope.insured_gh5e, function(value, index) {	
	    $scope.rows['datas_s'].push(value);
	});
	
	}
	
	
	$scope.insured_gh5f = JSON.parse($window.insured_gh5f);
	if($scope.insured_gh5f==''){
		 $scope.insured_gh5f = null;
		}
	if($scope.insured_gh5f!=null){
		angular.forEach($scope.insured_gh5f, function(value, index) {	
	    $scope.rows['datas_y'].push(value);
	});
	
	}
	
	
	$scope.insured_gh5g = JSON.parse($window.insured_gh5g);
	if($scope.insured_gh5g==''){
		 $scope.insured_gh5g = null;
		}
	if($scope.insured_gh5g!=null){
		angular.forEach($scope.insured_gh5g, function(value, index) {	
	    $scope.rows['datas_emp'].push(value);
	});
	
	}
	
	  $scope.insured_gh51b = JSON.parse($window.insured_gh51b);
	if($scope.insured_gh51b==''){
		 $scope.insured_gh51b = null;
		}
	if($scope.insured_gh51b!=null){
		angular.forEach($scope.insured_gh51b, function(value, index) {	
	    $scope.rows['datas_gh'].push(value);
	});
	
	}

     $scope.insured_gh51c = JSON.parse($window.insured_gh51c);
	if($scope.insured_gh51c==''){
		 $scope.insured_gh51c = null;
		}
	if($scope.insured_gh51c!=null){
		angular.forEach($scope.insured_gh15c, function(value, index) {	
	    $scope.rows['datas_p_gh'].push(value);
	});
	
	}
	
	   $scope.insured_gh51d = JSON.parse($window.insured_gh5d);
	if($scope.insured_gh51d==''){
		 $scope.insured_gh51d = null;
		}
	if($scope.insured_gh51d!=null){
		angular.forEach($scope.insured_gh51d, function(value, index) {	
	    $scope.rows['datas_e_gh'].push(value);
	});
	
	}
	
	
	$scope.insured_gh51e = JSON.parse($window.insured_gh51e);
	if($scope.insured_gh51e==''){
		 $scope.insured_gh51e = null;
		}
	if($scope.insured_gh51e!=null){
		angular.forEach($scope.insured_gh51e, function(value, index) {	
	    $scope.rows['datas_s_gh'].push(value);
	});
	
	}
	
	
	$scope.insured_gh51f = JSON.parse($window.insured_gh5f);
	if($scope.insured_gh51f==''){
		 $scope.insured_gh51f = null;
		}
	if($scope.insured_gh51f!=null){
		angular.forEach($scope.insured_gh51f, function(value, index) {	
	    $scope.rows['datas_y_gh'].push(value);
	});
	
	}
	
	
	$scope.insured_gh51g = JSON.parse($window.insured_gh51g);
	if($scope.insured_gh51g==''){
		 $scope.insured_gh51g = null;
		}
	if($scope.insured_gh51g!=null){
		angular.forEach($scope.insured_gh51g, function(value, index) {	
	    $scope.rows['datas_emp_gh'].push(value);
	});
	
	}
	
	
	
	$scope.add_row_g = function(type){	
	if(type=='datas'){		
		 $scope.show_type=true;
	}else if(type=='datas_p'){
		 $scope.show_type_p=true;
	}else if(type=='datas_e'){
		 $scope.show_type_e=true;
	}else if(type=='datas_s'){
		 $scope.show_type_s=true;
	}else if(type=='datas_y'){
		 $scope.show_type_y=true;
	}else if(type=='datas_emp'){
		 $scope.show_type_emp=true;
	}	
	
	if(type=='datas_gh'){		
		 $scope.show_type_gh=true;
	}else if(type=='datas_p_gh'){
		 $scope.show_type_p_gh=true;
	}else if(type=='datas_e_gh'){
		 $scope.show_type_e_gh=true;
	}else if(type=='datas_s_gh'){
		 $scope.show_type_s_gh=true;
	}else if(type=='datas_y_gh'){
		 $scope.show_type_y_gh=true;
	}else if(type=='datas_emp_gh'){
		 $scope.show_type_emp_gh=true;
	}
	
		 $scope.rows[type].push({sequence:''});	
		
	}
	$scope.remove_row_g = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rows[type].splice(index, 1);
		}
		
    }
	
	
	$scope.zip_code_ga=function(index){				
					
			var city=$('#in_ga_city_'+index).val();  
			var state=$('#in_ga_state_'+index).val();
			var county=$('#in_ga_county_'+index).val(); 
			var zip_attr='class="span12" id="in_ga_zip_'+index+'" ng-model="in_garag_ad.in_ga_zip"';
			var zip_name='in_ga_zip[]';
			var baseurl = $("#baseurls").val();
			  $.ajax({
				  type: "POST",
				  url: baseurl+"index.php/quote/getZipcode", 
				  data:"city="+city+"&state="+state+"&county="+county+"&attr="+zip_attr+"&name="+zip_name+"",	
				  success: function(json) 
				  {	
					 $('#in_ga_zip_span_'+index).html(''); 	   
					 $('#in_ga_zip_span_'+index).html(json); 
					   
				  }
			  });
			  
			  var county_attr='class="span12" onChange="getZipCodes_ga('+index+')" id="in_ga_county_'+index+'"  ng-model="in_garag_ad.in_ga_county"';
			  var county_name='in_ga_county[]';
			  var baseurl = $("#baseurls").val();
			  $.ajax({
				  type: "POST",
				  url: baseurl+"index.php/quote/getcounty", 
				  data:"city="+city+"&state="+state+"&attr="+county_attr+"&name="+county_name+"",
				  success: function(json) 
				  {  
					
					 if(json.toLowerCase().indexOf('select')>=0){
					   $('#in_ga_county_span_'+index).html(''); 	   
					   $('#in_ga_county_span_'+index).html(json); 
						
						}else{
						if(city!='' && state!=''){
						  event.stopPropagation();
						  alert('No matches found');	
						 }
						}
				   
				  }
			  });	
		  
  

		
	}	
	

	
}

/*
* Angular Chosen directive
* 
* Ashvin Patel 30/Mar/2015
*/
myApp.directive('chosen', function ($http) {
	var linker = function (scope, element, attr) {
		scope.$watch(scope.states, function (oldVal, newVal) {
			setTimeout(function(){
				element.trigger('liszt:updated');
			}, 500);
		});
		scope.$watch(attr.ngModel, function () {
			setTimeout(function(){
				element.trigger('liszt:updated');
			}, 500);
		});
		setTimeout(function(){
			element.chosen({disable_search_threshold: 20});
		}, 500);
	};
	
	return {
		restrict: 'A',
		link: linker
	}
});



coverageCtrl.$inject = ['$scope', 'myServ','$window'];
function coverageCtrl($scope,myServ,$window){
	$scope.coverages=JSON.parse($window.coverage);
	$scope.rows = [];
	$scope.rows['trucks'] = [];
	$scope.rows['tractors'] = [];
	$scope.rows['trailers'] = [];
	$scope.rows['others'] = [];
	
	if($scope.coverages==''){
		$scope.coverages=null;
		}
	if($scope.coverages!=null){	
		   
		$scope.coverages=$scope.coverages[0];		
	    $scope.coverages.coverage = $scope.coverages.coverage.split(",");
		$scope.coverages.vh_type = $scope.coverages.vh_type.split(",");	
		
			
		  if($.inArray('Truck',$scope.coverages.vh_type)>-1){
		    $scope.truck_vh=JSON.parse($window.truck_vh);
		
		   if($scope.truck_vh==''){
		        $scope.truck_vh=null;
		      }
	       if($scope.truck_vh!=null){			   
			    angular.forEach($scope.truck_vh, function(value, index) {	
				var total_eups=0;
				var liability=(value['liability'])?value['liability']:value['liability_vh'];
		          if(liability!=null){
				  if(liability.indexOf("$")>=0){			 
				   var l= liability.replace("$","");
				   var lia= l.replace(/,/g,"");
				   
					 total_eups+=parseFloat(lia);
					
				   }
				  }
				var um=(value['um'])?value['um']:value['truck_um'];
				  if(um!=null){
				  if(um.indexOf("$")>=0){	
				   var um_p= um.replace("$","");
				   var u= um_p.replace(/,/g,"");	 
					total_eups+=parseFloat(u);
				   }
				  }
				var pip=(value['pip'])?value['pip']:value['truck_pip'];
				  if(pip!=null){
				  if(pip.indexOf("$")>=0){
				   var pip_p= pip.replace("$","");
				   var pi= pip_p.replace(/,/g,"");		 
					total_eups+=parseFloat(pi);
				   }
				  }
				var uim=(value['uim'])?value['uim']:value['truck_uim'];
				
				 if(uim!=null){
				  if(uim.indexOf("$")>=0){
				   var uim_p= uim.replace("$","");
				   var ui= uim_p.replace(/,/g,"");		 
					total_eups+=parseFloat(ui);
				  }
				 }
				var pd=(value['pd'])?value['pd']:value['pd_vh'];				
				  if(pd!=null){
				  if(pd.indexOf("$")>=0){	
				   var pd_p= pd.replace("$","");
				   var p= pd_p.replace(/,/g,"");	 
					total_eups+=parseFloat(p);
				  }
				 }
				var cargo=(value['cargo'])?value['cargo']:value['cargo_vh'];	
				  if(cargo!=null){
				  if(cargo.indexOf("$")>=0){	
				   var cargo_p= cargo.replace("$","");
				   var car= cargo_p.replace(/,/g,"");	 
					total_eups+=parseFloat(car);
				  }
				 }
                  value['sub_total_price']='$'+total_eups.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              $scope.rows['trucks'].push(value);
	             });	
		   
		   var parent_index = $scope.rows['trucks'].length; 
		   $scope.truck_lessor=JSON.parse($window.truck_lessor);
		   if($scope.truck_lessor==''){
		        $scope.truck_lessor=null;
		      }			
			
	       if($scope.truck_lessor!=null){
			 
		   angular.forEach($scope.truck_lessor, function(values, indexs) {	
		  			
		     $scope.rows['trucks'][parent_index-1]['lessor'].push(values);	
			 	
			 $scope.truck_email=JSON.parse($window.truck_email);
			
			   if($scope.truck_email==''){
					$scope.truck_email=null;
				  }
			   if($scope.truck_email!=null){ 
			  
					  angular.forEach($scope.truck_email, function(value, index) { 
					     if(values['vh_ls_id']==value['parent_id']){
			            $scope.rows['trucks'][parent_index-1]['lessor'][indexs]['emails'].push(value);
						 }
					  });
			   }
		   
			   $scope.truck_fax=JSON.parse($window.truck_fax);
			   if($scope.truck_fax==''){
					$scope.truck_fax=null;
			   }
			   if($scope.truck_fax!=null){ 
			        angular.forEach($scope.truck_fax, function(value, index) { 
					  if(values['vh_ls_id']==value['parent_id']){
			            $scope.rows['trucks'][parent_index-1]['lessor'][indexs]['faxs'].push(value);
					    }
					  });
			     }
			  
		      });
			  }else{
				  
		   var parent_index = $scope.rows['trucks'].length;	
		   $scope.rows['trucks'][parent_index-1]['lessor'].push({sequence:'',emails:[],faxs:[],vh_ls_state:'CA',vh_ls_country:'US',pri_country:1,vh_ls_country_code:'1',in_truck_fax:'No',lessor_type:'Lessor',vh_ls_ph_type:'Office'});	
		   var email_index = $scope.rows['trucks'][parent_index-1]['lessor'].length;		   
		   $scope.rows['trucks'][parent_index-1]['lessor'][email_index - 1]['emails'].push({sequence:'',priority_email:1,email_type:'Office'})
		   $scope.rows['trucks'][parent_index-1]['lessor'][email_index - 1]['faxs'].push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'})
		
			}
		   
		   $scope.truck_vh_eqp=JSON.parse($window.truck_vh_eqp);
		  
		   if($scope.truck_vh_eqp==''){
		        $scope.truck_vh_eqp=null;
		      }
			  
	       if($scope.truck_vh_eqp!=null){
			 
			  var parent_index = $scope.rows['trucks'].length;	
			  angular.forEach($scope.truck_vh_eqp, function(value, index) {	
			  var total_eups=0;
		 
				  	var liability=(value['liability'])?value['liability']:value['liability_vh'];
		          if(liability!=null){
				  if(liability.indexOf("$")>=0){			 
				   var l= liability.replace("$","");
				   var lia= l.replace(/,/g,"");
				   
					 total_eups+=parseFloat(lia);
					
				   }
				  }
				var um=(value['um'])?value['um']:value['truck_um'];
				  if(um!=null){
				  if(um.indexOf("$")>=0){	
				   var um_p= um.replace("$","");
				   var u= um_p.replace(/,/g,"");	 
					total_eups+=parseFloat(u);
				   }
				  }
				var pip=(value['pip'])?value['pip']:value['truck_pip'];
				  if(pip!=null){
				  if(pip.indexOf("$")>=0){
				   var pip_p= pip.replace("$","");
				   var pi= pip_p.replace(/,/g,"");		 
					total_eups+=parseFloat(pi);
				   }
				  }
				var uim=(value['uim'])?value['uim']:value['truck_uim'];
				
				 if(uim!=null){
				  if(uim.indexOf("$")>=0){
				   var uim_p= uim.replace("$","");
				   var ui= uim_p.replace(/,/g,"");		 
					total_eups+=parseFloat(ui);
				  }
				 }
				var pd=(value['pd'])?value['pd']:value['pd_vh'];				
				  if(pd!=null){
				  if(pd.indexOf("$")>=0){	
				   var pd_p= pd.replace("$","");
				   var p= pd_p.replace(/,/g,"");	 
					total_eups+=parseFloat(p);
				  }
				 }
				var cargo=(value['cargo'])?value['cargo']:value['cargo_vh'];	
				  if(cargo!=null){
				  if(cargo.indexOf("$")>=0){	
				   var cargo_p= cargo.replace("$","");
				   var car= cargo_p.replace(/,/g,"");	 
					total_eups+=parseFloat(car);
				  }
				 }
                  value['total_equipment']='$'+total_eups.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			       if(value!=''){
	              $scope.rows['trucks'][parent_index-1]['vh_eqpmnts'].push(value);
				   }
				/*  if(value['parent_id']==value['vh_eqp_id']){
					  $scope.rows['trucks'][parent_index-1]['vh_eqpmnts'][index]['vh_eqpmntss'][index].push(value);
					  }*/
	          });		  
			 
		     }else{
				
				 
		      }
		    }else{
			 
				
			 }
		   }
		   
		   
		   
		 if($.inArray('Tractor',$scope.coverages.vh_type)>-1){
		    $scope.tractor_vh=JSON.parse($window.tractor_vh);
		
		   if($scope.tractor_vh==''){
		        $scope.tractor_vh=null;
		      }
	       if($scope.tractor_vh!=null){
			   
			    angular.forEach($scope.tractor_vh, function(value, index) {	
			       var total_eups=0;
		 
				 	var liability=(value['liability'])?value['liability']:value['liability_vh'];
		          if(liability!=null){
				  if(liability.indexOf("$")>=0){			 
				   var l= liability.replace("$","");
				   var lia= l.replace(/,/g,"");
				   
					 total_eups+=parseFloat(lia);
					
				   }
				  }
				var um=(value['um'])?value['um']:value['truck_um'];
				  if(um!=null){
				  if(um.indexOf("$")>=0){	
				   var um_p= um.replace("$","");
				   var u= um_p.replace(/,/g,"");	 
					total_eups+=parseFloat(u);
				   }
				  }
				var pip=(value['pip'])?value['pip']:value['truck_pip'];
				  if(pip!=null){
				  if(pip.indexOf("$")>=0){
				   var pip_p= pip.replace("$","");
				   var pi= pip_p.replace(/,/g,"");		 
					total_eups+=parseFloat(pi);
				   }
				  }
				var uim=(value['uim'])?value['uim']:value['truck_uim'];
				
				 if(uim!=null){
				  if(uim.indexOf("$")>=0){
				   var uim_p= uim.replace("$","");
				   var ui= uim_p.replace(/,/g,"");		 
					total_eups+=parseFloat(ui);
				  }
				 }
				var pd=(value['pd'])?value['pd']:value['pd_vh'];				
				  if(pd!=null){
				  if(pd.indexOf("$")>=0){	
				   var pd_p= pd.replace("$","");
				   var p= pd_p.replace(/,/g,"");	 
					total_eups+=parseFloat(p);
				  }
				 }
				var cargo=(value['cargo'])?value['cargo']:value['cargo_vh'];	
				  if(cargo!=null){
				  if(cargo.indexOf("$")>=0){	
				   var cargo_p= cargo.replace("$","");
				   var car= cargo_p.replace(/,/g,"");	 
					total_eups+=parseFloat(car);
				  }
				 }
                  value['sub_total_price']='$'+total_eups.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              $scope.rows['tractors'].push(value);
	             });	
		     
			  
		   $scope.tractor_lessor=JSON.parse($window.tractor_lessor);
		   if($scope.tractor_lessor==''){
		        $scope.tractor_lessor=null;
		      }
			var parent_index = $scope.rows['tractors'].length;
			
	       if($scope.tractor_lessor!=null){
			 
		   angular.forEach($scope.tractor_lessor, function(values, indexs) {	
		  			
		     $scope.rows['tractors'][parent_index-1]['lessor'].push(values);	
			 	
			 $scope.tractor_email=JSON.parse($window.tractor_email);
			
			   if($scope.tractor_email==''){
					$scope.tractor_email=null;
				  }
			   if($scope.tractor_email!=null){ 
			  
					  angular.forEach($scope.tractor_email, function(value, index) { 
					     if(values['vh_ls_id']==value['parent_id']){
			            $scope.rows['tractors'][parent_index-1]['lessor'][indexs]['emails'].push(value);
						 }
					  });
			   }
		   
			   $scope.tractor_fax=JSON.parse($window.tractor_fax);
			   if($scope.tractor_fax==''){
					$scope.tractor_fax=null;
			   }
			   if($scope.tractor_fax!=null){ 
			        angular.forEach($scope.tractor_fax, function(value, index) { 
					  if(values['vh_ls_id']==value['parent_id']){
			            $scope.rows['tractors'][parent_index-1]['lessor'][indexs]['faxs'].push(value);
					    }
					  });
			     }
			  
		      });			  	 
			}else{
			 var parent_index = $scope.rows['tractors'].length;	
		   $scope.rows['tractors'][parent_index-1]['lessor'].push({sequence:'',emails:[],faxs:[],vh_ls_state:'CA',vh_ls_country:'US',pri_country:1,vh_ls_country_code:'1',in_truck_fax:'No',lessor_type:'Lessor',vh_ls_ph_type:'Office'});	
		   var email_index = $scope.rows['tractors'][parent_index-1]['lessor'].length;		   
		   $scope.rows['tractors'][parent_index-1]['lessor'][email_index - 1]['emails'].push({sequence:'',priority_email:1,email_type:'Office'})
		   $scope.rows['tractors'][parent_index-1]['lessor'][email_index - 1]['faxs'].push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'})
			
			}
		 
		   $scope.tractor_vh_eqp=JSON.parse($window.tractor_vh_eqp);
		  
		   if($scope.tractor_vh_eqp==''){
		        $scope.tractor_vh_eqp=null;
		      }
			  
			$scope.tractor_vh_eqpmntss=JSON.parse($window.tractor_vh_eqpmntss);
		  
		   if($scope.tractor_vh_eqpmntss==''){
		        $scope.tractor_vh_eqpmntss=null;
		      }
			  
	       if($scope.tractor_vh_eqp!=null){
			 
			  var parent_index = $scope.rows['tractors'].length;	
			  angular.forEach($scope.tractor_vh_eqp, function(value, index) {	
			        var total_eups=0;
		 
				var liability=(value['liability'])?value['liability']:value['liability_vh'];
		          if(liability!=null){
				  if(liability.indexOf("$")>=0){			 
				   var l= liability.replace("$","");
				   var lia= l.replace(/,/g,"");
				   
					 total_eups+=parseFloat(lia);
					
				   }
				  }
				var um=(value['um'])?value['um']:value['truck_um'];
				  if(um!=null){
				  if(um.indexOf("$")>=0){	
				   var um_p= um.replace("$","");
				   var u= um_p.replace(/,/g,"");	 
					total_eups+=parseFloat(u);
				   }
				  }
				var pip=(value['pip'])?value['pip']:value['truck_pip'];
				  if(pip!=null){
				  if(pip.indexOf("$")>=0){
				   var pip_p= pip.replace("$","");
				   var pi= pip_p.replace(/,/g,"");		 
					total_eups+=parseFloat(pi);
				   }
				  }
				var uim=(value['uim'])?value['uim']:value['truck_uim'];
				
				 if(uim!=null){
				  if(uim.indexOf("$")>=0){
				   var uim_p= uim.replace("$","");
				   var ui= uim_p.replace(/,/g,"");		 
					total_eups+=parseFloat(ui);
				  }
				 }
				var pd=(value['pd'])?value['pd']:value['pd_vh'];				
				  if(pd!=null){
				  if(pd.indexOf("$")>=0){	
				   var pd_p= pd.replace("$","");
				   var p= pd_p.replace(/,/g,"");	 
					total_eups+=parseFloat(p);
				  }
				 }
				var cargo=(value['cargo'])?value['cargo']:value['cargo_vh'];	
				  if(cargo!=null){
				  if(cargo.indexOf("$")>=0){	
				   var cargo_p= cargo.replace("$","");
				   var car= cargo_p.replace(/,/g,"");	 
					total_eups+=parseFloat(car);
				  }
				 }
                  value['total_equipment']='$'+total_eups.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	             				  
					 if(value!=''){	
						$scope.rows['tractors'][parent_index-1]['vh_eqpmnts'].push(value);
					 }
				 if($scope.tractor_vh_eqpmntss!=null){
				
				  angular.forEach($scope.tractor_vh_eqpmntss, function(values, indexs) {			
				 
				    if(values['parent_id']==value['vh_eqp_id']){									   		
					  $scope.rows['tractors'][parent_index-1]['vh_eqpmnts'][index]['vh_eqpmntss'].push(values);
					   }
					 });
	          	   }
				
			  });
		     }else{
				 
		   var parent_index=$scope.rows['tractors'].length-1;
		   var len=$scope.rows['tractors'][parent_index]['vh_eqpmnts'].length+1;		
		   $scope.rows['tractors'][parent_index]['vh_eqpmnts'].push({sequence:'',vh_lib_coverage:'Yes', vh_pd_coverage:'No', vh_cargo_coverage:'No',refrigerated_breakdown:'No',truck_um:'Rejected',truck_uim:'Rejected',truck_pip:'Rejected',vh_equip_trailer:'No',vh_eqpmntss:[]});
	       $('#tractors_vh_eqpmnt_pull_'+parent_index).val(len);
		  
		      }
		    }
		  }
		  if($.inArray('Trailer',$scope.coverages.vh_type)>-1){
		    $scope.trailer_vh=JSON.parse($window.trailer_vh);
		
		   if($scope.trailer_vh==''){
		        $scope.trailer_vh=null;
		      }
	       if($scope.trailer_vh!=null){
			   
			    angular.forEach($scope.trailer_vh, function(value, index) {	
			      var total_eups=0;
			var liability=(value['liability'])?value['liability']:value['liability_vh'];
		          if(liability!=null){
				  if(liability.indexOf("$")>=0){			 
				   var l= liability.replace("$","");
				   var lia= l.replace(/,/g,"");
				   
					 total_eups+=parseFloat(lia);
					
				   }
				  }
				var um=(value['um'])?value['um']:value['truck_um'];
				  if(um!=null){
				  if(um.indexOf("$")>=0){	
				   var um_p= um.replace("$","");
				   var u= um_p.replace(/,/g,"");	 
					total_eups+=parseFloat(u);
				   }
				  }
				var pip=(value['pip'])?value['pip']:value['truck_pip'];
				  if(pip!=null){
				  if(pip.indexOf("$")>=0){
				   var pip_p= pip.replace("$","");
				   var pi= pip_p.replace(/,/g,"");		 
					total_eups+=parseFloat(pi);
				   }
				  }
				var uim=(value['uim'])?value['uim']:value['truck_uim'];
				
				 if(uim!=null){
				  if(uim.indexOf("$")>=0){
				   var uim_p= uim.replace("$","");
				   var ui= uim_p.replace(/,/g,"");		 
					total_eups+=parseFloat(ui);
				  }
				 }
				var pd=(value['pd'])?value['pd']:value['pd_vh'];				
				  if(pd!=null){
				  if(pd.indexOf("$")>=0){	
				   var pd_p= pd.replace("$","");
				   var p= pd_p.replace(/,/g,"");	 
					total_eups+=parseFloat(p);
				  }
				 }
				var cargo=(value['cargo'])?value['cargo']:value['cargo_vh'];	
				  if(cargo!=null){
				  if(cargo.indexOf("$")>=0){	
				   var cargo_p= cargo.replace("$","");
				   var car= cargo_p.replace(/,/g,"");	 
					total_eups+=parseFloat(car);
				  }
				 }
                  value['sub_total_price']='$'+total_eups.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	              $scope.rows['trailers'].push(value);
	             });	
		     
			  
		   $scope.trailer_lessor=JSON.parse($window.trailer_lessor);
		   if($scope.trailer_lessor==''){
		        $scope.trailer_lessor=null;
		      }
		var parent_index = $scope.rows['trailers'].length;
			
	       if($scope.trailer_lessor!=null){
			 
		   angular.forEach($scope.trailer_lessor, function(values, indexs) {	
		  			
		     $scope.rows['trailers'][parent_index-1]['lessor'].push(values);	
			 	
			 $scope.trailer_email=JSON.parse($window.trailer_email);
			
			   if($scope.trailer_email==''){
					$scope.trailer_email=null;
				  }
			   if($scope.trailer_email!=null){ 
			  
					  angular.forEach($scope.trailer_email, function(value, index) { 
					     if(values['vh_ls_id']==value['parent_id']){
			            $scope.rows['trailers'][parent_index-1]['lessor'][indexs]['emails'].push(value);
						 }
					  });
			   }
		   
			   $scope.trailer_fax=JSON.parse($window.trailer_fax);
			   if($scope.trailer_fax==''){
					$scope.trailer_fax=null;
			   }
			   if($scope.trailer_fax!=null){ 
			        angular.forEach($scope.trailer_fax, function(value, index) { 
					  if(values['vh_ls_id']==value['parent_id']){
			            $scope.rows['trailers'][parent_index-1]['lessor'][indexs]['faxs'].push(value);
					    }
					  });
			     }
			  
		      });			  	 
		  }else{
		  var parent_index = $scope.rows['trailers'].length;	
		   $scope.rows['trailers'][parent_index-1]['lessor'].push({sequence:'',emails:[],faxs:[],vh_ls_state:'CA',vh_ls_country:'US',pri_country:1,vh_ls_country_code:'1',in_truck_fax:'No',lessor_type:'Lessor',vh_ls_ph_type:'Office'});	
		   var email_index = $scope.rows['trailers'][parent_index-1]['lessor'].length;		   
		   $scope.rows['trailers'][parent_index-1]['lessor'][email_index - 1]['emails'].push({sequence:'',priority_email:1,email_type:'Office'})
		   $scope.rows['trailers'][parent_index-1]['lessor'][email_index - 1]['faxs'].push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'})
		  
			}
		   
		   $scope.trailer_vh_eqp=JSON.parse($window.trailer_vh_eqp);
		  
		   if($scope.trailer_vh_eqp==''){
		        $scope.trailer_vh_eqp=null;
		      }
			  
	       if($scope.trailer_vh_eqp!=null){
			 
			  var parent_index = $scope.rows['trailers'].length;	
			  angular.forEach($scope.trailer_vh_eqp, function(value, index) {	
			       var total_eups=0;
		 
				 	var liability=(value['liability'])?value['liability']:value['liability_vh'];
		          if(liability!=null){
				  if(liability.indexOf("$")>=0){			 
				   var l= liability.replace("$","");
				   var lia= l.replace(/,/g,"");
				   
					 total_eups+=parseFloat(lia);
					
				   }
				  }
				var um=(value['um'])?value['um']:value['truck_um'];
				  if(um!=null){
				  if(um.indexOf("$")>=0){	
				   var um_p= um.replace("$","");
				   var u= um_p.replace(/,/g,"");	 
					total_eups+=parseFloat(u);
				   }
				  }
				var pip=(value['pip'])?value['pip']:value['truck_pip'];
				  if(pip!=null){
				  if(pip.indexOf("$")>=0){
				   var pip_p= pip.replace("$","");
				   var pi= pip_p.replace(/,/g,"");		 
					total_eups+=parseFloat(pi);
				   }
				  }
				var uim=(value['uim'])?value['uim']:value['truck_uim'];
				
				 if(uim!=null){
				  if(uim.indexOf("$")>=0){
				   var uim_p= uim.replace("$","");
				   var ui= uim_p.replace(/,/g,"");		 
					total_eups+=parseFloat(ui);
				  }
				 }
				var pd=(value['pd'])?value['pd']:value['pd_vh'];				
				  if(pd!=null){
				  if(pd.indexOf("$")>=0){	
				   var pd_p= pd.replace("$","");
				   var p= pd_p.replace(/,/g,"");	 
					total_eups+=parseFloat(p);
				  }
				 }
				var cargo=(value['cargo'])?value['cargo']:value['cargo_vh'];	
				  if(cargo!=null){
				  if(cargo.indexOf("$")>=0){	
				   var cargo_p= cargo.replace("$","");
				   var car= cargo_p.replace(/,/g,"");	 
					total_eups+=parseFloat(car);
				  }
				 }
                  value['total_equipment']='$'+total_eups.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			     if(value!=''){
	               $scope.rows['trailers'][parent_index-1]['vh_eqpmnts'].push(value);
				 }
				  /* if(value['parent_id']==value['vh_eqp_id']){
					  $scope.rows['trucks'][parent_index-1]['vh_eqpmnts'][index]['vh_eqpmntss'][index].push(value);
					  }*/
	          });		  
			 
		     }else{
				 
		      }
		    }
		  }
		   if($.inArray('Other',$scope.coverages.vh_type)>-1){
		    $scope.other_vh=JSON.parse($window.other_vh);
		
		   if($scope.other_vh==''){
		        $scope.other_vh=null;
		      }
	       if($scope.other_vh!=null){
			   
			    angular.forEach($scope.other_vh, function(value, index) {
			     var total_eups=0;		 
				var liability=(value['liability'])?value['liability']:value['liability_vh'];
		          if(liability!=null){
				  if(liability.indexOf("$")>=0){			 
				   var l= liability.replace("$","");
				   var lia= l.replace(/,/g,"");
				   
					 total_eups+=parseFloat(lia);
					
				   }
				  }
				var um=(value['um'])?value['um']:value['truck_um'];
				  if(um!=null){
				  if(um.indexOf("$")>=0){	
				   var um_p= um.replace("$","");
				   var u= um_p.replace(/,/g,"");	 
					total_eups+=parseFloat(u);
				   }
				  }
				var pip=(value['pip'])?value['pip']:value['truck_pip'];
				  if(pip!=null){
				  if(pip.indexOf("$")>=0){
				   var pip_p= pip.replace("$","");
				   var pi= pip_p.replace(/,/g,"");		 
					total_eups+=parseFloat(pi);
				   }
				  }
				var uim=(value['uim'])?value['uim']:value['truck_uim'];
				
				 if(uim!=null){
				  if(uim.indexOf("$")>=0){
				   var uim_p= uim.replace("$","");
				   var ui= uim_p.replace(/,/g,"");		 
					total_eups+=parseFloat(ui);
				  }
				 }
				var pd=(value['pd'])?value['pd']:value['pd_vh'];				
				  if(pd!=null){
				  if(pd.indexOf("$")>=0){	
				   var pd_p= pd.replace("$","");
				   var p= pd_p.replace(/,/g,"");	 
					total_eups+=parseFloat(p);
				  }
				 }
				var cargo=(value['cargo'])?value['cargo']:value['cargo_vh'];	
				  if(cargo!=null){
				  if(cargo.indexOf("$")>=0){	
				   var cargo_p= cargo.replace("$","");
				   var car= cargo_p.replace(/,/g,"");	 
					total_eups+=parseFloat(car);
				  }
				 }
                  value['sub_total_price']='$'+total_eups.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");	
			
	              $scope.rows['others'].push(value);
	             });	
		     
			  
		   $scope.other_lessor=JSON.parse($window.other_lessor);
		   if($scope.other_lessor==''){
		        $scope.other_lessor=null;
		      }
			//var parent_index = $scope.rows['others'].length;
			
	       if($scope.other_lessor!=null){
		
		   angular.forEach($scope.other_lessor, function(values, indexs) {	
		  			
		     $scope.rows['others'][parent_index-1]['lessor'].push(values);	
			 	
			 $scope.other_email=JSON.parse($window.other_email);
			
			   if($scope.other_email==''){
					$scope.other_email=null;
				  }
			   if($scope.other_email!=null){ 
			  
					  angular.forEach($scope.other_email, function(value, index) { 
					     if(values['vh_ls_id']==value['parent_id']){
			            $scope.rows['others'][parent_index-1]['lessor'][indexs]['emails'].push(value);
						 }
					  });
			   }
		   
			   $scope.other_fax=JSON.parse($window.other_fax);
			   if($scope.other_fax==''){
					$scope.other_fax=null;
			   }
			   if($scope.other_fax!=null){ 
			        angular.forEach($scope.other_fax, function(value, index) { 
					  if(values['vh_ls_id']==value['parent_id']){
			            $scope.rows['others'][parent_index-1]['lessor'][indexs]['faxs'].push(value);
					    }
					  });
			     }
			  
		         });
			  
			  	 
			  	  }else{
		   var parent_indexs = $scope.rows['others'].length;	
		   
		   $scope.rows['others'][parent_indexs-1]['lessor'].push({sequence:'',emails:[],faxs:[],vh_ls_state:'CA',vh_ls_country:'US',pri_country:1,vh_ls_country_code:'1',in_truck_fax:'No',lessor_type:'Lessor',vh_ls_ph_type:'Office'});	
		   var email_index = $scope.rows['others'][parent_indexs-1]['lessor'].length;		   
		   $scope.rows['others'][parent_indexs-1]['lessor'][email_index - 1]['emails'].push({sequence:'',priority_email:1,email_type:'Office'})
		   $scope.rows['others'][parent_indexs-1]['lessor'][email_index - 1]['faxs'].push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'})
		  
			}
		  
		   $scope.other_vh_eqp=JSON.parse($window.other_vh_eqp);
		  
		   if($scope.other_vh_eqp==''){
		        $scope.other_vh_eqp=null;
		      }
			 $scope.other_vh_eqpmntss=JSON.parse($window.other_vh_eqpmntss);
		  
		   if($scope.other_vh_eqpmntss==''){
		        $scope.other_vh_eqpmntss=null;
		      }
			 
	       if($scope.other_vh_eqp!=null){
			 
			  var parent_index = $scope.rows['others'].length;	
			  angular.forEach($scope.other_vh_eqp, function(value, index) {	
			  var total_eups=0;
		 
				var liability=(value['liability'])?value['liability']:value['liability_vh'];
		          if(liability!=null){
				  if(liability.indexOf("$")>=0){			 
				   var l= liability.replace("$","");
				   var lia= l.replace(/,/g,"");
				   
					 total_eups+=parseFloat(lia);
					
				   }
				  }
				var um=(value['um'])?value['um']:value['truck_um'];
				  if(um!=null){
				  if(um.indexOf("$")>=0){	
				   var um_p= um.replace("$","");
				   var u= um_p.replace(/,/g,"");	 
					total_eups+=parseFloat(u);
				   }
				  }
				var pip=(value['pip'])?value['pip']:value['truck_pip'];
				  if(pip!=null){
				  if(pip.indexOf("$")>=0){
				   var pip_p= pip.replace("$","");
				   var pi= pip_p.replace(/,/g,"");		 
					total_eups+=parseFloat(pi);
				   }
				  }
				var uim=(value['uim'])?value['uim']:value['truck_uim'];
				
				 if(uim!=null){
				  if(uim.indexOf("$")>=0){
				   var uim_p= uim.replace("$","");
				   var ui= uim_p.replace(/,/g,"");		 
					total_eups+=parseFloat(ui);
				  }
				 }
				var pd=(value['pd'])?value['pd']:value['pd_vh'];				
				  if(pd!=null){
				  if(pd.indexOf("$")>=0){	
				   var pd_p= pd.replace("$","");
				   var p= pd_p.replace(/,/g,"");	 
					total_eups+=parseFloat(p);
				  }
				 }
				var cargo=(value['cargo'])?value['cargo']:value['cargo_vh'];	
				  if(cargo!=null){
				  if(cargo.indexOf("$")>=0){	
				   var cargo_p= cargo.replace("$","");
				   var car= cargo_p.replace(/,/g,"");	 
					total_eups+=parseFloat(car);
				  }
				 }
                  value['total_equipment']='$'+total_eups.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			
	                 if(value!=''){
						$scope.rows['others'][parent_index-1]['vh_eqpmnts'].push(value);
					 }
					 if($scope.other_vh_eqpmntss!=null){
				
				      angular.forEach($scope.other_vh_eqpmntss, function(values, indexs) {			
				 
				    if(values['parent_id']==value['vh_eqp_id']){									   		
					  $scope.rows['others'][parent_index-1]['vh_eqpmnts'][index]['vh_eqpmntss'].push(values);
					   }
					 });
	          	   }
	          });		  
			 
		     }else{
		  var parent_index=$scope.rows['others'].length-1;
		  var len=$scope.rows['others'][parent_index]['vh_eqpmnts'].length+1;		
		   $scope.rows['others'][parent_index]['vh_eqpmnts'].push({sequence:'',vh_lib_coverage:'Yes', vh_pd_coverage:'No', vh_cargo_coverage:'No',refrigerated_breakdown:'No',truck_um:'Rejected',truck_uim:'Rejected',truck_pip:'Rejected',vh_equip_trailer:'No',vh_eqpmntss:[]});
	       $('#others_vh_eqpmnt_pull_'+parent_index).val(len);
		      }
		    }
		  }
		
		
		 }else{	
		
		   $scope.coverages = {'vh_type':[],'n_trucks':0,'n_tractors':0,'n_trailers':0,'n_others':0};
		   
	    }
   
	$scope.add_row = function(type){		
		$scope.rows[type].push({sequence:'',pri_country:1,code_type:'1',phone_type:'Office'});
		if(type == 'trucks'){
			$scope.n_trucks = $scope.rows[type].length;
		}
	}
	
	$scope.add_email = function(parent_type, parent_index, index){	
	var length=	$scope.rows[parent_type][parent_index]['lessor'][index]['emails'].length+1;
		$scope.rows[parent_type][parent_index]['lessor'][index]['emails'].push({sequence:'',priority_email:length,email_type:'Office'})
		
	}
	$scope.remove_email = function(parent_type, parent_parent_index, parent_index, index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  $scope.rows[parent_type][parent_parent_index]['lessor'][parent_index]['emails'].splice(index, 1);
		}
	}
	
	$scope.add_fax = function(parent_type, parent_index, index){
		var length=	$scope.rows[parent_type][parent_index]['lessor'][index]['faxs'].length+1;
		$scope.rows[parent_type][parent_index]['lessor'][index]['faxs'].push({sequence:'',country_fax:length,code_type_fax:'1',fax_type:'Office'})
		
	}
	$scope.remove_fax = function(parent_type, parent_parent_index, parent_index, index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  $scope.rows[parent_type][parent_parent_index]['lessor'][parent_index]['faxs'].splice(index, 1);
		}
	}
	
	$scope.add_eqp = function(parent_type, parent_index, index){	
	    var len=$scope.rows[parent_type][parent_index]['vh_eqpmnts'].length+1;		
		$scope.rows[parent_type][parent_index]['vh_eqpmnts'].push({sequence:'',vh_lib_coverage:'Yes', vh_pd_coverage:'No', vh_cargo_coverage:'No',refrigerated_breakdown:'No',truck_um:'Rejected',truck_uim:'Rejected',truck_pip:'Rejected',vh_equip_trailer:'No',vh_eqpmntss:[]});
	    $('#'+parent_type+'_vh_eqpmnt_pull_'+parent_index).val(len);
	}
	 $scope.myServ=myServ;
     myServ.state_pip= myServ.state_pip;
	$scope.remove_eqp = function(parent_type, parent_index, index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  var len=$scope.rows[parent_type][parent_index]['vh_eqpmnts'].length-1;
		   $scope.rows[parent_type][parent_index]['vh_eqpmnts'].splice(index, 1);
		  $('#'+parent_type+'_vh_eqpmnt_pull_'+parent_index).val(len);
		}
	}
	
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rows[type].splice(index, 1);
		}
		if(type == 'trucks'){
			$scope.n_trucks = $scope.rows[type].length;
		}
    }
	$scope.vh_eqpmnt_pull=function(parent_type, parent_index, index){
		var length = $scope.rows[parent_type][parent_index]['vh_eqpmnts'].length;	
		var score =index;		
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows[parent_type][parent_index]['vh_eqpmnts'].splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		$scope.rows[parent_type][parent_index]['vh_eqpmnts'].push({sequence:'',vh_lib_coverage:'Yes', vh_pd_coverage:'No', vh_cargo_coverage:'No',refrigerated_breakdown:'No',truck_um:'Rejected',truck_uim:'Rejected',truck_pip:'Rejected',vh_equip_trailer:'No',vh_eqpmntss:[]});
		i++;
		}
	 }
		
	$scope.add_row_ = function(score, type){		
		var length = $scope.rows[type].length;	
		if(type == 'trucks'){			
			var score = $scope.coverages.truck_no;
		}else if(type == 'tractors'){	
			var score = $scope.coverages.tractor_no;
		}else if(type == 'trailers'){	
			var score = $scope.coverages.trailer_no;
		}else if(type == 'others'){	
			var score = $scope.coverages.other_no;
		}
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows[type].splice($scope.toRemove, 1);
				score++;
			}
		}
		var k=0;
		while(i <= score){
			
			if($('#operation').val()=='Yes'){
		     var nature_bus=$('#nature_of_business').val();
			}else{
			 var nature_bus='';
			}
			 var l_vh='';
			 var l_ded_vh='';
		  if($('#liability_vh_0').val()!=''){
			    l_vh=$('#liability_vh_0').val();				  
					
			 }
			 
	      if($('#liability_ded_vh_0').val()!=''){
			  l_ded_vh=$('#liability_ded_vh_0').val();	  
					
			 }
		   var lib_cov =	$('#lib_coverage_0').val();
		   var lib_c=(lib_cov)?lib_cov:'Yes';
		   
		   var vals='';
		   var Reefer='No';
		   if($('#commodities_haulted').val()!=''){
			   vals=$('#commodities_haulted').val();
			   }
			if(vals){
		  for(var i=0;vals.length>i;i++){
			  var r=vals[i].split(',');
			  if(r[1]=='Reefer'){
			      Reefer='Yes'; 
			    }
			  }
			}
			var street= $('#in_address').val();
			var city= $('#in_city').val();
			var state= $('#in_state').val();
			var county= $('#in_county').val();
			var zip= $('#in_zip').val();
			var country= $('#in_country').val();
		
			if(state=='CA'){
					   $scope.myServ=myServ;
		               myServ.state_pip=state;
				}
		
		 
		   $scope.rows[type].push({sequence:'',truck_trailer_owned_vehicle:'Yes', lessor:[],lessor_n:0,vh_equipment_pull:'No',vh_eqpmnts:[], vh_lib_coverage:$window.liability, vh_pd_coverage:$window.pd, vh_cargo_coverage:$window.cargo,vh_nature_of_business:nature_bus,truck_um:'Rejected',truck_uim:'Rejected',truck_pip:'Rejected',liability_vh:l_vh,liability_ded_vh:l_ded_vh,vh_lib_coverage:lib_c,commodities_haulted:vals,vh_salvage:'No',refrigerated_breakdown:Reefer,vh_equipment_trailers:1});		   
		   var parent_index = $scope.rows[type].length;	
		   $scope.rows[type][parent_index-1]['lessor'].push({sequence:'',emails:[],faxs:[],vh_ls_state:'CA',vh_ls_country:'US',pri_country:1,vh_ls_country_code:'1',in_truck_fax:'No',lessor_type:'Lessor',vh_ls_ph_type:'Office'});	
		   var email_index = $scope.rows[type][parent_index-1]['lessor'].length;		   
		   $scope.rows[type][parent_index-1]['lessor'][email_index - 1]['emails'].push({sequence:'',priority_email:1,email_type:'Office'})
		   $scope.rows[type][parent_index-1]['lessor'][email_index - 1]['faxs'].push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'})
		   $scope.rows[type][parent_index-1]['vh_eqpmnts'].push({sequence:'',vh_lib_coverage:$window.liability, vh_pd_coverage:$window.pd, vh_cargo_coverage:$window.cargo,vh_nature_of_business:nature_bus,truck_um:'Rejected',truck_uim:'Rejected',truck_pip:'Rejected',liability_vh:l_vh,liability_ded_vh:l_ded_vh,vh_lib_coverage:lib_c,commodities_haulted:vals,vh_salvage:'No',refrigerated_breakdown:Reefer,vh_equip_trailer:'No',vh_eqpmntss:[]});
		   var index=$scope.rows[type][parent_index-1]['vh_eqpmnts'].length;
		   $scope.rows[type][parent_index-1]['vh_eqpmnts'][index-1]['vh_eqpmntss'].push({sequence:'',vh_lib_coverage:$window.liability, vh_pd_coverage:$window.pd, vh_cargo_coverage:$window.cargo,vh_nature_of_business:nature_bus,truck_um:'Rejected',truck_uim:'Rejected',truck_pip:'Rejected',liability_vh:l_vh,liability_ded_vh:l_ded_vh,vh_lib_coverage:lib_c,commodities_haulted:vals,vh_salvage:'No',refrigerated_breakdown:Reefer});
	
	    setTimeout(function(){
		
				  $('input').blur(function() {	
					 if($(this).val()!=''){	
					  $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					  $(this).removeAttr('style');
					  }
				   });
				
				 $('select').blur(function() {	
				
					if($(this).val()!=''){
					  $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
				   }else{
					   $(this).removeAttr('style');
					 } });
		 },200);
	
  data_dictionary();

		   var address=street+' '+city+' '+state+' '+county+' '+country+' '+zip;
		
			if(address!=''){
				$( document ).ready(function() {      
				var address_data = address;
				var address_id = i-1;
				var geocoder = new google.maps.Geocoder();
				geocoder.geocode({address: address_data}, function(results, status) {
				  centre = results[0].geometry.location;				
				  var mapOptions = {
					  zoom: 8,
					  center: centre,
					  mapTypeId: google.maps.MapTypeId.ROADMAP
				  };
				  if(type=='trucks'){
					
				  map = new google.maps.Map(document.getElementById('map-address-'+ address_id), mapOptions);
				  }
				  else if(type=='tractors'){
				  map = new google.maps.Map(document.getElementById('t_map-address-'+ address_id), mapOptions);
				 
				  }
				  else if(type=='trailers'){
				  map = new google.maps.Map(document.getElementById('tr_map-address-'+ address_id), mapOptions);
				  
				  }
				  else if(type=='others'){
				  map = new google.maps.Map(document.getElementById('o_map-address-'+ address_id), mapOptions);
				 
				  }
				  var marker = new google.maps.Marker({
					  map: map,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:20000,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map);
				});								
			  });					
			}
			
		     k++;
		 i++;
	    
		} 	

	  
	}
    $scope.vh_equip_tr=function(score,type,parent_index,index){		 		
		
		 var length = $scope.rows[type][parent_index]['vh_eqpmnts'][index]['vh_eqpmntss'].length;			  
		  var vals='';
		  var Reefer='No';
		   if($('#commodities_haulted').val()!=''){
			   vals=$('#commodities_haulted').val();
			   }
			if(vals){
		  for(var i=0;vals.length>i;i++){
			  var r=vals[i].split(',');
			  if(r[1]=='Reefer'){
			      Reefer='Yes'; 
			    }
			  }
			}
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows[type][parent_index]['vh_eqpmnts'][index]['vh_eqpmntss'].splice($scope.toRemove, 1);
				score++;
				
			}
		}
		
		while(i <= score){			
		
		$scope.rows[type][parent_index]['vh_eqpmnts'][index]['vh_eqpmntss'].push({sequence:'',vh_lib_coverage:'No', vh_pd_coverage:'No', vh_cargo_coverage:'No',truck_um:'Rejected',truck_uim:'Rejected',truck_pip:'Rejected',commodities_haulted:vals,refrigerated_breakdown:Reefer,vh_equipment_trailers:'Yes',vh_equip_trailer:'No'});
		   setTimeout(function(){
			  $('input').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 });
				  $('select').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 }); 
				  $('input').blur(function() {	
					 if($(this).val()!=''){	
					  $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					  $(this).removeAttr('style');
					  }
				   });
				
				$('select').blur(function() {
					 if($(this).val()!=''){		
					$(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					 $(this).removeAttr('style'); 
					 }
				   });
		 },200);
		i++;
                
		}
	}
	$scope.vh_equip_tr_remove=function(type,parent_index,index){
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		$scope.rows[type][parent_index]['vh_eqpmnts'][parent_index]['vh_eqpmntss'].splice(index, 1);
		}
	}
		
	$scope.in_truck_fax='No';
	$scope.in_tractor_fax='No';
	$scope.in_trailer_fax='No';
	$scope.in_other_fax='No';
		
	$scope.add_loss_row = function(type,index){		
		
		  $scope.rows[type][index]['lessor'].push({sequence:'',emails:[],faxs:[],vh_ls_state:'CA',vh_ls_country:'US',pri_country:1,code_type:'1',in_truck_fax:'No',phone_type:'Office'});	
		   var email_index = $scope.rows[type][index]['lessor'].length;		   
		   $scope.rows[type][index]['lessor'][email_index - 1]['emails'].push({sequence:'',priority_email:1,email_type:'Office'})
		   $scope.rows[type][index]['lessor'][email_index - 1]['faxs'].push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'})
	}
	
	$scope.remove_type= function(type,parent_index,index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  $scope.rows[type][parent_index]['lessor'].splice(index, 1);
		}
	
	}
	
	
	
	
	$scope.isOptionSelected = function(option_values, option_id) {		
		if(option_values){				
			if(option_values.indexOf(option_id) >= 0){				
				return true;	
			}else{
				return  false;	
			}
		}
		return false;		
  };     
  
   $scope.total_eup=function(liability,um,pip,uim,pd,cargo){		
	$scope.total_eups='';
		 
		if(liability.indexOf("$")>=0){			 
		 var l= liability.replace("$","");
		 var lia= l.replace(",","");
           $scope.total_eups+=parseFloat(lia);
		  
		}
		if(um.indexOf("$")>=0){	
		 var um_p= um.replace("$","");
		 var u= um_p.replace(",","");	 
		  $scope.total_eups+=parseFloat(u);
		}
		if(pip.indexOf("$")>=0){
		 var pip_p= pip.replace("$","");
		 var pi= pip_p.replace(",","");		 
		  $scope.total_eups+=parseFloat(pi);
		}
		if(uim.indexOf("$")>=0){
		 var uim_p= uim.replace("$","");
		 var ui= uim_p.replace(",","");		 
		  $scope.total_eups+=parseFloat(ui);
		}
		if(pd.indexOf("$")>=0){	
		 var pd_p= pd.replace("$","");
		 var p= pd_p.replace(",","");	 
          $scope.total_eups+=parseFloat(p);
		}
		if(cargo.indexOf("$")>=0){	
		 var cargo_p= cargo.replace("$","");
		 var car= cargo_p.replace(",","");	 
		  $scope.total_eups+=parseFloat(car);
		}
	
		if($scope.total_eups!=''){
		 return $scope.total_eups;
		}
     }
	 
	 $scope.lib_coverage=function(){
		  var vh=$('#lib_coverage_0').val();	  		
		   
			 var i_c;
			 var j_c;
			 var k_c;
			 var l_c;
			 for (i_c = 0; i_c < $scope.rows['trucks'].length; i_c++) { 		 
		     $scope.rows['trucks'][i_c]['vh_lib_coverage']=vh;
			 }
			  for (j_c = 0; j_c < $scope.rows['tractors'].length; j_c++) { 		 
		     $scope.rows['tractors'][j_c]['vh_lib_coverage']=vh;
			  for (j_vh = 0; j_vh < $scope.rows['tractors'][j_c]['vh_eqpmnts'].length; j_vh++) { 
			    $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_lib_coverage']=vh;
			   for (j_vh1 = 0; j_vh1 < $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'].length; j_vh1++) { 
		         $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'][j_vh1]['vh_lib_coverage']=vh;
			     }
			  }
			 }
			  for (k_c = 0; k_c < $scope.rows['trailers'].length; k_c++) { 		 
		     $scope.rows['trailers'][k_c]['vh_lib_coverage']=vh;
			 }
			  for (l_c = 0; l_c < $scope.rows['others'].length; l_c++) { 		 
		     $scope.rows['others'][l_c]['vh_lib_coverage']=vh;
			  for (l_vh = 0; l_vh < $scope.rows['others'][l_c]['vh_eqpmnts'].length; l_vh++) { 
			    $scope.rows['others'][l_vh]['vh_eqpmnts']['vh_lib_coverage']=vh;
				for (l_vh1 = 0; l_vh1 < $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'].length; l_vh1++) { 
		         $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'][l_vh1]['vh_lib_coverage']=vh;
			     }
			  }
			 }
			
		 }
	
	 
	 $scope.lia_first_v=function(){
		  var l_vh=$('#liability_vh_0').val();	  		
		         
			 var i_c;
			 var j_c;
			 var k_c;
			 var l_c;
			 for (i_c = 0; i_c < $scope.rows['trucks'].length; i_c++) { 		 
		     $scope.rows['trucks'][i_c]['liability_vh']=l_vh;			 
			 }
			  for (j_c = 0; j_c < $scope.rows['tractors'].length; j_c++) { 		 
		      $scope.rows['tractors'][j_c]['liability_vh']=l_vh;
			  
			  for (j_vh = 0; j_vh < $scope.rows['tractors'][j_c]['vh_eqpmnts'].length; j_vh++) { 
			    $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['liability_vh']=l_vh;
				   for (j_vh1 = 0; j_vh1 < $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'].length; j_vh1++) { 
		         $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'][j_vh1]['liability_vh']=l_vh;
			     }
			  }
			 }
			  for (k_c = 0; k_c < $scope.rows['trailers'].length; k_c++) { 		 
		     $scope.rows['trailers'][k_c]['liability_vh']=l_vh;
			 }
			  for (l_c = 0; l_c < $scope.rows['others'].length; l_c++) { 		 
		       $scope.rows['others'][l_c]['liability_vh']=l_vh;
			 for (l_v = 0; l_v < $scope.rows['others'][l_c]['vh_eqpmnts'].length; l_v++) { 
			    $scope.rows['others'][l_c]['vh_eqpmnts'][l_v]['liability_vh']=l_vh;
				
				for (l_vh1 = 0; l_vh1 < $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'].length; l_vh1++) { 
		         $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'][l_vh1]['liability_vh']=l_vh;
			     }
			  }
			 }
		 }
    $scope.lia_first_ded_v=function(){
		  var l_ded_vh=$('#liability_ded_vh_0').val();
				
		    
			 var i_c;
			 var j_c;
			 var k_c;
			 var l_c;
			 for (i_c = 0; i_c < $scope.rows['trucks'].length; i_c++) { 		 
		     $scope.rows['trucks'][i_c]['liability_ded_vh']=l_ded_vh;
			 }
			  for (j_c = 0; j_c < $scope.rows['tractors'].length; j_c++) { 		 
		      $scope.rows['tractors'][j_c]['liability_ded_vh']=l_ded_vh;
			  for (j_vh = 0; j_vh < $scope.rows['tractors'][j_c]['vh_eqpmnts'].length; j_vh++) { 
			    $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['liability_ded_vh']=l_ded_vh;
			  for (j_vh1 = 0; j_vh1 < $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'].length; j_vh1++) { 
			   $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'][j_vh1]['liability_ded_vh']=l_ded_vh;
			   }
			  
			  }
			 }
			  for (k_c = 0; k_c < $scope.rows['trailers'].length; k_c++) { 		 
		     $scope.rows['trailers'][k_c]['liability_ded_vh']=l_ded_vh;
			 }
			  for (l_c = 0; l_c < $scope.rows['others'].length; l_c++) { 		 
		      $scope.rows['others'][l_c]['liability_ded_vh']=l_ded_vh;
			  for (l_vh = 0; l_vh < $scope.rows['others'][l_c]['vh_eqpmnts'].length; l_vh++) { 
			    $scope.rows['others'][l_c]['vh_eqpmnts'][l_vh]['liability_ded_vh']=l_ded_vh;
				
				for (l_vh1 = 0; l_vh1 < $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'].length; l_vh1++) { 
		         $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'][l_vh1]['liability_ded_vh']=l_ded_vh;
			     }
			  }
			 }
				
				
		
		}
    $scope.truck_um_first=function(){
		  var truck_ums=$('#truck_um_0').val();			
		    
			 var i_c;
			 var j_c;
			 var k_c;
			 var l_c;
			 for (i_c = 0; i_c < $scope.rows['trucks'].length; i_c++) { 		 
		     $scope.rows['trucks'][i_c]['truck_um']=truck_ums;
			 
			 }
			  for (j_c = 0; j_c < $scope.rows['tractors'].length; j_c++) { 		 
		     $scope.rows['tractors'][j_c]['truck_um']=truck_ums;
			 for (j_vh = 0; j_vh < $scope.rows['tractors'][j_c]['vh_eqpmnts'].length; j_vh++) { 
			    $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['truck_um']=truck_ums;
				 for (j_vh1 = 0; j_vh1 < $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'].length; j_vh1++) { 
			     $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'][j_vh1]['truck_um']=truck_ums;
			   }
			  }
			 }
			  for (k_c = 0; k_c < $scope.rows['trailers'].length; k_c++) { 		 
		     $scope.rows['trailers'][k_c]['truck_um']=truck_ums;
			 }
			  for (l_c = 0; l_c < $scope.rows['others'].length; l_c++) { 		 
		     $scope.rows['others'][l_c]['truck_um']=truck_ums;
			 for (l_vh = 0; l_vh < $scope.rows['others'][l_c]['vh_eqpmnts'].length; l_vh++) { 
			    $scope.rows['others'][l_c]['vh_eqpmnts'][l_vh]['truck_um']=truck_ums;
				for (l_vh1 = 0; l_vh1 < $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'].length; l_vh1++) { 
		         $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'][l_vh1]['truck_um']=truck_ums;
			     }
			   }
			 }
		
		}
		
       $scope.truck_pip_first=function(){
		  var truck_pip=$('#truck_pip_0').val();		
		    
			 var i_c;
			 var j_c;
			 var k_c;
			 var l_c;
			 for (i_c = 0; i_c < $scope.rows['trucks'].length; i_c++) { 		 
		     $scope.rows['trucks'][i_c]['truck_pip']=truck_pip;
			 
			 }
			  for (j_c = 0; j_c < $scope.rows['tractors'].length; j_c++) { 		 
		     $scope.rows['tractors'][j_c]['truck_pip']=truck_pip;
			 for (j_vh = 0; j_vh < $scope.rows['tractors'][j_c]['vh_eqpmnts'].length; j_vh++) { 
			    $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['truck_pip']=truck_pip;
				 for (j_vh1 = 0; j_vh1 < $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'].length; j_vh1++) { 
			   $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'][j_vh1]['truck_pip']=truck_pip;
			   }
			  }
			 }
			  for (k_c = 0; k_c < $scope.rows['trailers'].length; k_c++) { 		 
		     $scope.rows['trailers'][k_c]['truck_pip']=truck_pip;
			 }
			  for (l_c = 0; l_c < $scope.rows['others'].length; l_c++) { 		 
		     $scope.rows['others'][l_c]['truck_pip']=truck_pip;
			 for (l_vh = 0; l_vh < $scope.rows['others'][l_c]['vh_eqpmnts'].length; l_vh++) { 
			    $scope.rows['others'][l_c]['vh_eqpmnts'][l_vh]['truck_pip']=truck_pip;
				for (l_vh1 = 0; l_vh1 < $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'].length; l_vh1++) { 
		         $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'][l_vh1]['truck_pip']=truck_pip;
			     }
			  }
			 }
	   }
	   		
       $scope.truck_uim_first=function(){
		  var truck_uim=$('#truck_uim_0').val();			
		    
			 var i_c;
			 var j_c;
			 var k_c;
			 var l_c;
			 for (i_c = 0; i_c < $scope.rows['trucks'].length; i_c++) { 		 
		     $scope.rows['trucks'][i_c]['truck_uim']=truck_uim;
			 }
			  for (j_c = 0; j_c < $scope.rows['tractors'].length; j_c++) { 		 
		     $scope.rows['tractors'][j_c]['truck_uim']=truck_uim;
			 for (j_vh = 0; j_vh < $scope.rows['tractors'][j_c]['vh_eqpmnts'].length; j_vh++) { 
			    $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['truck_uim']=truck_uim;
				 for (j_vh1 = 0; j_vh1 < $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'].length; j_vh1++) { 
			   $scope.rows['tractors'][j_c]['vh_eqpmnts'][j_vh]['vh_eqpmntss'][j_vh1]['truck_uim']=truck_uim;
			   }
			  }
			 }
			  for (k_c = 0; k_c < $scope.rows['trailers'].length; k_c++) { 		 
		     $scope.rows['trailers'][k_c]['truck_uim']=truck_uim;
			 }
			  for (l_c = 0; l_c < $scope.rows['others'].length; l_c++) { 		 
		     $scope.rows['others'][l_c]['truck_uim']=truck_uim;
			  for (l_vh = 0; l_vh < $scope.rows['others'][l_c]['vh_eqpmnts'].length; l_vh++) { 
			    $scope.rows['others'][l_c]['vh_eqpmnts'][l_vh]['truck_uim']=truck_uim;
				for (l_vh1 = 0; l_vh1 < $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'].length; l_vh1++) { 
		         $scope.rows['others'][j_c]['vh_eqpmnts'][l_vh]['vh_eqpmntss'][l_vh1]['truck_uim']=truck_uim;
			     }
			  }
			 }
	   }
	   
	$scope.coverage_change=function(val){
		
		 if(val.indexOf('Liability')>=0){	
		     var i_l;
			 var j_l;
			 var k_l;
			 var l_l;
			 for (i_l = 0; i_l < $scope.rows['trucks'].length; i_l++) { 		 
		     $scope.rows['trucks'][i_l]['vh_lib_coverage']='Yes';
			 }
			  for (j_l = 0; j_l < $scope.rows['tractors'].length; j_l++) { 		 
		     $scope.rows['tractors'][j_l]['vh_lib_coverage']='Yes';
			 }
			  for (k_l = 0; k_l < $scope.rows['trailers'].length; k_l++) { 		 
		     $scope.rows['trailers'][k_l]['vh_lib_coverage']='Yes';
			 }
			  for (l_l = 0; l_l < $scope.rows['others'].length; l_l++) { 		 
		     $scope.rows['others'][l_l]['vh_lib_coverage']='Yes';
			 }
			}else{
			
			 var i_l;
			 var j_l;
			 var k_l;
			 var l_l;
			 for (i_l = 0; i_l < $scope.rows['trucks'].length; i_l++) { 		 
		     $scope.rows['trucks'][i_l]['vh_lib_coverage']='No';
			 }
			  for (j_l = 0; j_l < $scope.rows['tractors'].length; j_l++) { 		 
		     $scope.rows['tractors'][j_l]['vh_lib_coverage']='No';
			 }
			  for (k_l = 0; k_l < $scope.rows['trailers'].length; k_l++) { 		 
		     $scope.rows['trailers'][k_l]['vh_lib_coverage']='No';
			 }
			  for (l_l = 0; l_l < $scope.rows['others'].length; l_l++) { 		 
		     $scope.rows['others'][l_l]['vh_lib_coverage']='No';
			 }
			}
		if(val.indexOf('PD')>=0){
			
			 var i_p;
			 var j_p;
			 var k_p;
			 var l_p;
			 for (i_p = 0; i_p < $scope.rows['trucks'].length; i_p++) { 		 
		     $scope.rows['trucks'][i_p]['vh_pd_coverage']='Yes';
			 }
			  for (j_p = 0; j_p < $scope.rows['tractors'].length; j_p++) { 		 
		     $scope.rows['tractors'][j_p]['vh_pd_coverage']='Yes';
			 }
			  for (k_p = 0; k_p < $scope.rows['trailers'].length; k_p++) { 		 
		     $scope.rows['trailers'][k_p]['vh_pd_coverage']='Yes';
			 }
			  for (l_p = 0; l_p < $scope.rows['others'].length; l_p++) { 		 
		     $scope.rows['others'][l_p]['vh_pd_coverage']='Yes';
			 }
		
		  }else{
		     var i_p;
			 var j_p;
			 var k_p;
			 var l_p;
			 for (i_p = 0; i_p < $scope.rows['trucks'].length; i_p++) { 		 
		     $scope.rows['trucks'][i_p]['vh_pd_coverage']='No';
			 }
			  for (j_p = 0; j_p < $scope.rows['tractors'].length; j_p++) { 		 
		     $scope.rows['tractors'][j_p]['vh_pd_coverage']='No';
			 }
			  for (k_p = 0; k_p < $scope.rows['trailers'].length; k_p++) { 		 
		     $scope.rows['trailers'][k_p]['vh_pd_coverage']='No';
			 }
			  for (l_p = 0; l_p < $scope.rows['others'].length; l_p++) { 		 
		     $scope.rows['others'][l_p]['vh_pd_coverage']='No';
			 }
		  
		  }
		if(val.indexOf('Cargo')>=0){
			 var i_c;
			 var j_c;
			 var k_c;
			 var l_c;
			 for (i_c = 0; i_c < $scope.rows['trucks'].length; i_c++) { 		 
		     $scope.rows['trucks'][i_c]['vh_cargo_coverage']='Yes';
			 }
			  for (j_c = 0; j_c < $scope.rows['tractors'].length; j_c++) { 		 
		     $scope.rows['tractors'][j_c]['vh_cargo_coverage']='Yes';
			 }
			  for (k_c = 0; k_c < $scope.rows['trailers'].length; k_c++) { 		 
		     $scope.rows['trailers'][k_c]['vh_cargo_coverage']='Yes';
			 }
			  for (l_c = 0; l_c < $scope.rows['others'].length; l_c++) { 		 
		     $scope.rows['others'][l_c]['vh_cargo_coverage']='Yes';
			 }
			}else{
			 var i_c;
			 var j_c;
			 var k_c;
			 var l_c;
			 for (i_c = 0; i_c < $scope.rows['trucks'].length; i_c++) { 		 
		     $scope.rows['trucks'][i_c]['vh_cargo_coverage']='No';
			 }
			  for (j_c = 0; j_c < $scope.rows['tractors'].length; j_c++) { 		 
		     $scope.rows['tractors'][j_c]['vh_cargo_coverage']='No';
			 }
			  for (k_c = 0; k_c < $scope.rows['trailers'].length; k_c++) { 		 
		     $scope.rows['trailers'][k_c]['vh_cargo_coverage']='No';
			 }
			  for (l_c = 0; l_c < $scope.rows['others'].length; l_c++) { 		 
		     $scope.rows['others'][l_c]['vh_cargo_coverage']='No';
			 }
			
			
			}			
				var truck_no= $("input[name='truck_no']").val();
				var tractor_no=$("input[name='tractor_no']").val();
				var trailer_no=$("input[name='trailer_no']").val();
				var other_no=$("input[name='other_no']").val();
				var vals=$('#coverages_req').val();
				if(truck_no > 0 || tractor_no > 0 || trailer_no > 0|| other_no > 0){	
				if(confirm('Do you want to save changes')){
					$('#coverage').val(vals);
					$('#coverage').trigger('liszt:updated');
					}else{
					event.preventDefault();
					return false; 
					}
				}
			$('#coverages_req').val(val);
            $('#coverages_req').trigger('liszt:updated');
			
		}
  $scope.radius_ope=function(type,index,value){
	  		var street= $('#in_address').val();
			var city= $('#in_city').val();
			var state= $('#in_state').val();
			var county= $('#in_county').val();
			var zip= $('#in_zip').val();
			var country= $('#in_country').val();			
			
			var address=street+' '+city+' '+state+' '+county+' '+country+' '+zip;
			if(address!=''){
	        	var address_data = address;		
				var address_id = index;	
				var radiuss=value*1609.344/10;					
				var geocoder = new google.maps.Geocoder();
				geocoder.geocode({address: address_data}, function(results, status) {
				  centre = results[0].geometry.location;				
				  var mapOptions = {
					  zoom: 8,
					  center: centre,
					  mapTypeId: google.maps.MapTypeId.ROADMAP
				  };
				 
				  if(type=='trucks'){
				  map = new google.maps.Map(document.getElementById('map-address-'+ address_id), mapOptions);
				  }
				  else if(type=='tractors'){
				  map = new google.maps.Map(document.getElementById('t_map-address-'+ address_id), mapOptions);
				 
				  }
				  else if(type=='trailers'){
				  map = new google.maps.Map(document.getElementById('tr_map-address-'+ address_id), mapOptions);
				  
				  }
				  else if(type=='others'){
				  map = new google.maps.Map(document.getElementById('o_map-address-'+ address_id), mapOptions);
				 
				  }
				  var marker = new google.maps.Marker({
					  map: map,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map);
	  });
	}
  }
  $scope.make_pic_vh=function(scope,vh,index){
	  
	       	 var baseurl = $("#baseurls").val();
                       
                  $.ajax({
						url: baseurl+'quote/vh_picture',
						type: 'post',
						data : {scope: scope,vh:vh},
						async: false, //blocks window close
						success: function(htm) {	
						   if(vh==44){
							$('#vh_make_'+index).html(htm);		
						   }
						   else if(vh==45){
							   $('#vh_make_t'+index).html(htm);	
							}
						   else if(vh==46){
							    $('#vh_make_tr'+index).html(htm);	
							 }
							//$('.mapping').html('<img src="'+baseurl+'/uploads/file/'+htm+'" />');
							}
					 });
	          
	  
	    }
  angular.element(document).ready(function () {
	  	
        	var street= $('#in_address').val();		
			var city= $('#in_city').val();
			var state= $('#in_state').val();
			var county= $('#in_county').val();
			var zip= $('#in_zip').val();
			var country= $('#in_country').val();			
			var address=street+' '+city+' '+state+' '+county+' '+country+' '+zip;

			if($.trim(address).length > 0 ){
				
				var truck_no=$("input[name=truck_no]").val();
				var tractor_no=$("input[name=tractor_no]").val();				
				var trailer_no=$("input[name=trailer_no]").val();
				var other_no=$("input[name=other_no]").val();
				var map='';
				var map_1='';
				var map_2='';
				var map_3='';
	        	var address_data = address;		
				
				var radiuss=250*1609.344/10;					
				var geocoder = new google.maps.Geocoder();
				geocoder.geocode({address: address_data}, function(results, status) {
				  centre = results[0].geometry.location;				
				  var mapOptions = {
					  zoom: 8,
					  center: centre,
					  mapTypeId: google.maps.MapTypeId.ROADMAP
				  };
				 
				  if(truck_no > 0){
				     for(var address_id=0;address_id < truck_no;address_id++){	
								 
				      map = new google.maps.Map(document.getElementById('map-address-'+ address_id), mapOptions);
					  if(map!=''){
				  var marker = new google.maps.Marker({
					  map: map,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map);
				  }
					
					  }
				  }
				  if(tractor_no > 0){
					  for(var address_id=0;address_id < tractor_no;address_id++){
						  
				  map_1 = new google.maps.Map(document.getElementById('t_map-address-'+ address_id), mapOptions);
					 	  if(map_1!=''){
				  var marker = new google.maps.Marker({
					  map: map_1,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map_1);
				  }
					 
					  }
				  }
				  if(trailer_no > 0){
					  for(var address_id=0;address_id < trailer_no;address_id++){
				  map_2 = new google.maps.Map(document.getElementById('tr_map-address-'+ address_id), mapOptions);
					 var marker = new google.maps.Marker({
					  map: map_2,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map_2);
				  
					  }
				  }
				  if(other_no > 0){
					  for(var address_id=0;address_id < other_no;address_id++){
				  map_3 = new google.maps.Map(document.getElementById('o_map-address-'+ address_id), mapOptions);
					var marker = new google.maps.Marker({
					  map: map_3,
					  position: results[0].geometry.location,
					  title: 'The Address',
				  });
				  
				   var myCity = new google.maps.Circle({
					  center:centre,
					  radius:radiuss,
					  strokeColor:"#0000FF",
					  strokeOpacity:0.8,
					  strokeWeight:2,
					  fillColor:"#0000FF",
					  fillOpacity:0.4
					  });
					  myCity.setMap(map_3);
				  
					  
					  }
				  }
				
	    });
	   }
    });
  		
		 
	$scope.cov_section_map=function(val,id){
	
		    if(val=='show'){
				 $('#hide-cov_map'+id).show();
			     $('#show-cov_map'+id).hide();
				 $('#map-address-'+id).show();
				}else{
				$('#hide-cov_map'+id).hide();
			    $('#show-cov_map'+id).show();	
				$('#map-address-'+id).hide();	
			  }
		}	
	$scope.t_cov_section_map=function(val,id){
		  if(val=='show'){
				 $('#t_hide-cov_map'+id).show();
			     $('#t_show-cov_map'+id).hide();
				 $('#t_map-address-'+id).show();
				}else{
				$('#t_hide-cov_map'+id).hide();
			    $('#t_show-cov_map'+id).show();	
				$('#t_map-address-'+id).hide();	
			  }
	}	
	$scope.tr_cov_section_map=function(val,id){
	 if(val=='show'){
				 $('#tr_hide-cov_map'+id).show();
			     $('#tr_show-cov_map'+id).hide();
				 $('#tr_map-address-'+id).show();
				}else{
				 $('#tr_hide-cov_map'+id).hide();
			     $('#tr_show-cov_map'+id).show();	
				 $('#tr_map-address-'+id).hide();	
			  }
	}
	$scope.o_cov_section_map=function(val,id){
		if(val=='show'){
				 $('#o_hide-cov_map'+id).show();
			     $('#o_show-cov_map'+id).hide();
				 $('#o_map-address-'+id).show();
				}else{
				$('#o_hide-cov_map'+id).hide();
			    $('#o_show-cov_map'+id).show();	
				$('#o_map-address-'+id).hide();	
			  }
	} 
  $scope.cov_section_o=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_o'+id).show();
			 $('#show-cov_o'+id).hide();	 
			 $('#cov_div_o'+id).show();
			 }else{
			$('#hide-cov_o'+id).hide();
			$('#show-cov_o'+id).show();	 
			$('#cov_div_o'+id).hide(); 
			}
	} 
  $scope.cov_section_t=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_t'+id).show();
			 $('#show-cov_t'+id).hide();	 
			 $('#cov_div_t'+id).show();
			 }else{
			$('#hide-cov_t'+id).hide();
			$('#show-cov_t'+id).show();	 
			$('#cov_div_t'+id).hide(); 
			}
	}
  $scope.cov_section_tr=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_tr'+id).show();
			 $('#show-cov_tr'+id).hide();	 
			 $('#cov_div_tr'+id).show();
			 }else{
			$('#hide-cov_tr'+id).hide();
			$('#show-cov_tr'+id).show();	 
			$('#cov_div_tr'+id).hide(); 
			}
	} 
  $scope.cov_section_trl=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_trl'+id).show();
			 $('#show-cov_trl'+id).hide();	 
			 $('#cov_div_trl'+id).show();
			 }else{
			  $('#hide-cov_trl'+id).hide();
			  $('#show-cov_trl'+id).show();	 
			  $('#cov_div_trl'+id).hide(); 
			}
	}
  $scope.cov_section_ov=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_ov'+id).show();
			 $('#show-cov_ov'+id).hide();	 
			 $('#cov_div_ov'+id).show();
			 }else{
			$('#hide-cov_ov'+id).hide();
			$('#show-cov_ov'+id).show();	 
			$('#cov_div_ov'+id).hide(); 
			}
	} 
  $scope.cov_section_tv=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_tv'+id).show();
			 $('#show-cov_tv'+id).hide();	 
			 $('#cov_div_tv'+id).show();
			 }else{
			$('#hide-cov_tv'+id).hide();
			$('#show-cov_tv'+id).show();	 
			$('#cov_div_tv'+id).hide(); 
			}
	}
  $scope.cov_section_trv=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_trv'+id).show();
			 $('#show-cov_trv'+id).hide();	 
			 $('#cov_div_trv'+id).show();
			 }else{
			$('#hide-cov_trv'+id).hide();
			$('#show-cov_trv'+id).show();	 
			$('#cov_div_trv'+id).hide(); 
			}
	} 
  $scope.cov_section_trlv=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_trlv'+id).show();
			 $('#show-cov_trlv'+id).hide();	 
			 $('#cov_div_trlv'+id).show();
			 }else{
			  $('#hide-cov_trlv'+id).hide();
			  $('#show-cov_trlv'+id).show();	 
			  $('#cov_div_trlv'+id).hide(); 
			}
	}  
$scope.cov_section_ove=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_ove'+id).show();
			 $('#show-cov_ove'+id).hide();	 
			 $('#cov_div_ove'+id).show();
			 }else{
			$('#hide-cov_ove'+id).hide();
			$('#show-cov_ove'+id).show();	 
			$('#cov_div_ove'+id).hide(); 
			}
	} 
  $scope.cov_section_tve=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_tve'+id).show();
			 $('#show-cov_tve'+id).hide();	 
			 $('#cov_div_tve'+id).show();
			 }else{
			$('#hide-cov_tve'+id).hide();
			$('#show-cov_tve'+id).show();	 
			$('#cov_div_tve'+id).hide(); 
			}
	}
  $scope.cov_section_trve=function(show,id){
		 if(show=="show"){
			 $('#hide-cov_trve'+id).show();
			 $('#show-cov_trve'+id).hide();	 
			 $('#cov_div_trve'+id).show();
			 }else{
			$('#hide-cov_trve'+id).hide();
			$('#show-cov_trve'+id).show();	 
			$('#cov_div_trve'+id).hide(); 
			}
	} 
  $scope.min_max_val=function(val,id,str){
	  
	 if(val.length>3 && val.length<9){		
	  if(val.indexOf("$")>=0){			 
		 var l= val.replace("$","");
		 var lia= l.replace(",","");
		
          if(lia>=1000 && lia<=10000){		
			 $('#'+str+'_ph_ded_vh_span_'+id).hide();
			}else{				
			  $('#'+str+'_ph_ded_vh_'+id).val(''); 		
			  $('#'+str+'_ph_ded_vh_span_'+id).show();
			}
		  
		}
	  }else{
		  
		 $('#'+str+'_ph_ded_vh_'+id).val(''); 
		 $('#'+str+'_ph_ded_vh_span_'+id).show();
	}
  }


 	$scope.zip_code_ls=function(type){				
					
			var city=$('#'+type+'_vh_ls_city').val();  
			var state=$('#'+type+'_vh_ls_state').val();
			var county=$('#'+type+'_vh_ls_county').val(); 			
			var zip_attr='class="span12" id="'+type+'_vh_ls_zip" ng-model="lessor.vh_ls_zip"';
			var zip_name='vh_ls_zip[]';
			var baseurl = $("#baseurls").val();
			  $.ajax({
				  type: "POST",
				  url: baseurl+"index.php/quote/getZipcode", 
				  data:"city="+city+"&state="+state+"&county="+county+"&attr="+zip_attr+"&name="+zip_name+"",	
				  success: function(json) 
				  {	
					 $('#'+type+'_vh_ls_zip_span').html(''); 	   
					 $('#'+type+'_vh_ls_zip_span').html(json); 
					   
				  }
			  });			
			  
			  var county_attr='class="span12" onChange="getZipCodes_vh('+type+')" id="'+type+'_vh_ls_county"  ng-model="lessor.vh_ls_county"';
			  var county_name='vh_ls_county[]';
			  var baseurl = $("#baseurls").val();
			  $.ajax({
				  type: "POST",
				  url: baseurl+"index.php/quote/getcounty", 
				  data:"city="+city+"&state="+state+"&attr="+county_attr+"&name="+county_name+"",
				  success: function(json) 
				  {   
					 if(json.toLowerCase().indexOf('select')>=0){
					 $('#'+type+'_vh_ls_county_span').html(''); 	   
					 $('#'+type+'_vh_ls_county_span').html(json); 
					 }else{
					   if(city!='' && state!=''){
						event.stopPropagation();
						alert('No matches found');
						}
						
					  }
		 
		 
				   
				  }
			  });	 

		
	}	


}


myApp.service('myservices', function() {
	  this.drivers='';
	  this.rows=[];

    });
driverCtrl.$inject = ['$scope', 'myservices','$window'];
function driverCtrl($scope,myservices,$window){
    $scope.myservices=myservices;
	 myservices.rows = [];
	$scope.drivers=JSON.parse($window.drivers);
	
	if($scope.drivers == ""){
	 $scope.drivers=null;	 
	}
	
	if($scope.drivers!=null){
	  $scope.drivers=$scope.drivers[0];	  
	  $scope.n_drivers = ($scope.drivers.driver_count)?$scope.drivers.driver_count:1;
	  $scope.driver_ids = ($scope.drivers.drivers_id)?$scope.drivers.drivers_id:'';
	  $scope.driv_mvr_file = ($scope.drivers.driv_mvr_file)?$scope.drivers.driv_mvr_file:'';
		
		}else{
	   $scope.n_drivers = 1;	
	   myservices.rows.push({sequence:'',driver_license_issue_state:'CA'});
	   }
	$scope.driver=JSON.parse($window.driver);
	
	if($scope.driver=='undefined'){
		$scope.driver=null;
		}
	
    if($scope.driver!=null){		

	 angular.forEach($scope.driver, function(values, index_1) {
	        myservices.rows.push(values); 
		});
	
	}
	$scope.add_row = function(){		
		myservices.rows.push({sequence:'',driver_license_issue_state:'CA'});		
		$scope.n_drivers = myservices.rows.length;
		data_dictionary();
	}
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  myservices.rows.splice(index, 1);
		}
		$scope.n_drivers = myservices.rows.length;
    }
	
	$scope.add_row_ = function(){
		var score = $scope.n_drivers;	
		var length = myservices.rows.length;
		
		if(score > 0 )
		{   $scope.Schedule_Drivers = true; 
		}
		else{$scope.Schedule_Drivers = false;	
		}
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				myservices.rows.splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		   myservices.rows.push({sequence:'',driver_license_issue_state:'CA'});	
		  data_dictionary();
		 i++;
		} 	
	}
}
ownerCtrl.$inject = ['$scope', 'myservices','$window'];
function ownerCtrl($scope,myservices,$window){
	$scope.rowss = [];
	$scope.owners=JSON.parse($window.owners);
	
	if($scope.owners== ""){
	   $scope.owners=null;	 
	}
	if($scope.owners!=null){
	  $scope.owners=$scope.owners[0];
	  $scope.n_owners = ($scope.owners.number_of_owner)?$scope.owners.number_of_owner:0;
	  $scope.owner_ids = ($scope.owners.owners_id)?$scope.owners.owners_id:'';
	  $scope.own_mvr_file = ($scope.owners.own_mvr_file)?$scope.owners.own_mvr_file:'';		
		}else{
	   $scope.n_owners = 0;	
	   }

	$scope.owner=JSON.parse($window.owner);
	
    if($scope.owner==null){
	   $scope.owner='undefined';
	}
	if($scope.owner!='undefined'){
	
	  angular.forEach($scope.owner, function(values, index_1) {
	        $scope.rowss.push(values); 
		});
	
	}
	$scope.add_row = function(){		
		$scope.rowss.push({sequence:'',license_issue_state:'CA'});		
		$scope.n_owners = $scope.rowss.length;
		data_dictionary();

	}
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rowss.splice(index, 1);
		}
		$scope.n_drivers = $scope.rowss.length;
    }
	
	$scope.add_row_ = function(){
		var score = $scope.n_owners;
		var length = $scope.rowss.length;	
		
		if(score > 0 ){ $scope.Owner_Infomation = true ; }	else {  $scope.Owner_Infomation = false ;}
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rowss.splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		   $scope.rowss.push({sequence:'',license_issue_state:'CA'});
           data_dictionary();
 
		 i++;
		} 	
	}
	
	$scope.owner_d=function(val){	
	  var driver_name=$('#driver_name').val();
	  var num_of_drivers=$('#num_of_drivers').val();
	          if(driver_name!=''){
	          $scope.rowss=[];
			 
			  if(num_of_drivers > 0){
			  if(val=='Y'){
			  $scope.n_owners = (num_of_drivers)?num_of_drivers:0;
			  angular.forEach(myservices.rows, function(value, index) {		
		  
			     if(value!=''){	
                    $scope.rowss.push({owner_name:value['driver_name'],owner_middle_name:value['driver_middle_name'],owner_last_name:value['driver_last_name'],owner_is_driver:'Y',owner_license:value['driver_license'],license_issue_state:value['driver_license_issue_state']});						  					 
	       				
			        }
	            });	
				}else{
				var score = $scope.n_owners;
				var length = $scope.rowss.length;			
				if(score > 0 ){ $scope.Owner_Infomation = true ; }	else {  $scope.Owner_Infomation = false ;}
				if(score > length){					
					var i =length + 1;			  
					} else {
					while(score < length){
						$scope.rowss.splice($scope.toRemove, 1);
						score++;
					}
				}
				while(i <= score){
					 
					$scope.rowss.push({license_issue_state:'CA',owner_is_driver:'N'});	
			    i++;
				
				  }
				  }
				 
				}else{
					
			    var score = $scope.n_owners;
				
				var length = $scope.rowss.length;	
		
				if(score > 0 ){ $scope.Owner_Infomation = true ; }	else {  $scope.Owner_Infomation = false ;}
				if(score > length){					
					var i =length + 1;			  
					} else {
					while(score < length){
						$scope.rowss.splice($scope.toRemove, 1);
						score++;
					}
				}
				while(i <= score){
					
					$scope.rowss.push({sequence:'',license_issue_state:'CA'});	
			 i++;
			} 		
		 }
	   }
	 }
	
	
	}

myApp.service('myservice', function() {
	  this.broker_infos='';
	  this.broker_phones=[];
	  this.broker_emails=[];
	  this.broker_faxs=[];
    });
	


brokerCtrl.$inject = ['$scope', 'myservice','$window'];
function brokerCtrl($scope, myservice, $window){     
	
    $scope.myservice=myservice;
	myservice.broker_emails = [];	
	$scope.broker_email =JSON.parse($window.broker_email);		
	if($scope.broker_email != null){
	  angular.forEach($scope.broker_email, function(value, index) {	
	   myservice.broker_emails.push(value);	
	});
    }else{	

	  myservice.broker_emails.push({sequence:'',priority_email:1,email_type:'Office'});	
	}
		
	
	$scope.broker_info = JSON.parse($window.broker_info);
	
	if($scope.broker_info != null){	
	    myservice.broker_infos=$scope.broker_info[0];		
		
	}else{	
		
	   	myservice.broker_infos = {country:'US',state:'CA',fax:'No'};
	}

	
	$scope.addRowEmail = function(){
		var length=	myservice.broker_emails.length+1;		
	   myservice.broker_emails.push({sequence:'',priority_email:length,email_type:'Office'});
    
      };
	
	$scope.remove_row_email = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  myservice.broker_emails.splice(index, 1);
		}		
    }
	
	
	myservice.broker_phones = [];	
	$scope.broker_phone =JSON.parse($window.broker_phone);

	if($scope.broker_phone != null){
	   angular.forEach($scope.broker_phone, function(value, index) {	
        myservice.broker_phones.push(value);	
	});
    }else{
	    myservice.broker_phones.push({sequence:'',pri_country:1,code_type:'1',phone_type:'Office'});	
	}
	
	
	$scope.addRowPhone = function(){
	 var length = myservice.broker_phones.length+1;		
	  myservice.broker_phones.push({sequence:'',pri_country:length,code_type:'1',phone_type:'Office'});

    };
	
	$scope.remove_row_phone = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  myservice.broker_phones.splice(index, 1);
		}		
    }
	
  	myservice.broker_faxs = [];
	$scope.broker_fax =JSON.parse($window.broker_fax);	
	if($scope.broker_fax==''){
		$scope.broker_fax=null;
		}
	
	if($scope.broker_fax != null ){      
	  angular.forEach($scope.broker_fax, function(value, index) {	
	    myservice.broker_faxs.push(value);	
	});
    }else{		
	    myservice.broker_faxs.push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'});	
	}
	
	$scope.addRowFax = function(){	
	var length=	myservice.broker_faxs.length+1;
	   myservice.broker_faxs.push({sequence:'',country_fax:length,code_type_fax:'1',fax_type:'Office'});
      };
	
	$scope.remove_row_fax = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  myservice.broker_faxs.splice(index, 1);
		}		
    }
$scope.quick_dba=function(val){
  $('#quick_dba_v').html(val);
 }
}
personCtrl.$inject = ['$scope', 'myservice','$window'];  
function personCtrl($scope,myservice, $window){	
	$scope.myservice=myservice;
	$scope.handleClick=function(){	
	var info=$('#infosubmitted').attr('checked');	
	
 if(info!='checked'){

			$scope.person_infos={fistname:myservice.broker_infos.fistname,middlename:myservice.broker_infos.middlename,lastname:myservice.broker_infos.lastname,street:myservice.broker_infos.street,
			                     city:myservice.broker_infos.city,state:myservice.broker_infos.state,county:myservice.broker_infos.county,zip:myservice.broker_infos.zip,country:myservice.broker_infos.country,
							     special_remark:myservice.broker_infos.special_remark,fax:myservice.broker_infos.fax};
								
			$scope.person_phones=[];
			$scope.person_emails=[];
			$scope.person_faxs=[];
			  angular.forEach(myservice.broker_phones, function(value, index) {				  
			     if(value!=''){					   
	                   $scope.person_phones.push(value);	
			        }
	            });	
			  angular.forEach(myservice.broker_emails, function(value, index) {	
			    if(value!=''){
				  
				   $scope.person_emails.push(value);	
			      }
			   });	
			  angular.forEach(myservice.broker_faxs, function(value, index) {	
			     if(value!=''){
				  
				   $scope.person_faxs.push(value);	
				 }
			 });	
  var city=myservice.broker_infos.city; 
  var state=myservice.broker_infos.state;    
  var county_attr='class="span12" onChange="getZipCodes_p(this.value)" id="per_county"  ng-model="person_infos.county"';
  var county_name='per_county';
	var baseurl = $("#baseurls").val();
	$.ajax({
		type: "POST",
		url: baseurl+"index.php/quote/getcounty", 
		data:"city="+city+"&state="+state+"&attr="+county_attr+"&name="+county_name+"",
		success: function(json) 
		{  
		
		   $('#per_county_td').html(''); 	   
		   $('#per_county_td').html(json); 
		 
		}
	});

	/*$('#person_cntrl input').each(function(){			  
		 if($(this).val()!=''){			
		  $('input').css({'border-color':'rgba(82,168,236,0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82,168,236,0.6)'});
		 }
		  
	   });
		$('#person_cntrl select').each(function(){
		 if($(this).val()!=''){	 
		  $('select').css({'border-color':'rgba(82,168,236,0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82,168,236,0.6)'});
		 }
	   });*/


	       }else{
			$scope.person_phones=[];
			$scope.person_emails=[];
			$scope.person_faxs=[];
			$scope.person_infos={country:'US',state:'CA',fax:myservice.broker_infos.fax};  
			$scope.person_emails.push({sequence:'',priority_email:1,email_type:'Office'});
			$scope.person_phones.push({sequence:'',pri_country:1,code_type:'1',phone_type:'Office'});	
			$scope.person_faxs.push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'});	
		   
  var city='';  
  var state='';   
  var county_attr='class="span12" onChange="getZipCodes_p(this.value)" id="per_county"  ng-model="person_infos.county"';
  var county_name='per_county';
	var baseurl = $("#baseurls").val();
	$.ajax({
		type: "POST",
		url: baseurl+"index.php/quote/getcounty", 
		data:"city="+city+"&state="+state+"&attr="+county_attr+"&name="+county_name+"",
		success: function(json) 
		{  
		
		   $('#per_county_td').html(''); 	   
		   $('#per_county_td').html(json); 
		 
		}
	});
		   
		   
		    }
setTimeout(function(){
 $('input').each(function(){
   if($(this).val()!=''){	 
    $(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
   }else{
	 $(this).removeAttr('style'); 
	}
 });
  $('select').each(function(){
   if($(this).val()!=''){	 
    $(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
   }else{
	 $(this).removeAttr('style'); 
	 }
 });		
}, 300);

	 }
     

		   
	$scope.person_emails = [];
	$scope.person_email =JSON.parse($window.person_email);	
		
		if($scope.person_email==''){
			$scope.person_email=null;
			}
	if($scope.person_email != null){	

	  angular.forEach($scope.person_email, function(value, index) {	
	$scope.person_emails.push(value);	
	});
    }else{
	$scope.person_emails.push({sequence:'',priority_email:1,email_type:'Office'});	
	}
		

	
	$scope.person_info = JSON.parse($window.person_info);
	
		if($scope.person_info==''){
		$scope.person_info=null;
		}
	if($scope.person_info != null){	
	
	    $scope.person_infos=$scope.person_info[0];
	
	}else{
		   $scope.person_infos={country:'US',state:'CA',fax:'No'};	   
	
	 }
	
	$scope.addRowEmail = function(){
	var	length=	$scope.person_emails.length+1;			
	$scope.person_emails.push({sequence:'',priority_email:length,email_type:'Office'});
    
   };
	
	$scope.remove_row_email = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.person_emails.splice(index, 1);
		}		
    }
	
	
	$scope.person_phones = [];	
	$scope.person_phone =JSON.parse($window.person_phone);
	
	if($scope.person_phone != null){
	   angular.forEach($scope.person_phone, function(value, index) {	
	     $scope.person_phones.push(value);	
	  });
    }else{
	$scope.person_phones.push({sequence:'',pri_country:1,code_type:'1',phone_type:'Office'});	
	}
	
	
	$scope.addRowPhone = function(){	
	var	length=$scope.person_phones.length+1;
	$scope.person_phones.push({sequence:'',pri_country:length,code_type:'1',phone_type:'Office'});

    };
	
	$scope.remove_row_phone = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.person_phones.splice(index, 1);
		}		
    }
	
	$scope.person_faxs = [];
	$scope.person_fax =JSON.parse($window.person_fax);	
	if($scope.person_fax==''){
		$scope.person_fax=null;
		}
	
	if($scope.person_fax != null ){      
	  angular.forEach($scope.person_fax, function(value, index) {	
	  $scope.person_faxs.push(value);	
	});
    }else{		
	  $scope.person_faxs.push({sequence:'',country_fax:1,code_type_fax:'1',fax_type:'Office'});	
	}
	
	$scope.addRowFax = function(){	
	var length=	$scope.person_faxs.length+1;
	$scope.person_faxs.push({sequence:'',country_fax:length,code_type_fax:'1',fax_type:'Office'});

   };
	
	$scope.remove_row_fax = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.person_faxs.splice(index, 1);
		}		
    }
	
	$scope.getSinglepersons = function(item) {	
	      
				 var baseurl = $("#baseurls").val();                       
                  $.ajax({
						url: baseurl+'quote/getSingleperonInfo',
						type: 'post',
						data : {person_id: item},
						async: false, //blocks window close
						success: function(json) {							
						  json_data = JSON.parse(json);			
					      angular.forEach(json_data, function(value, index) {							        
								if(index=='person_info')	
								{
								 $scope.person_infos = [];	
								 $scope.person_infos = value;									 
								}
								else if(index=='person_phone')	
								{
								 $scope.person_phones = [];	
								 $scope.person_phones = value;	
								 							 
								}else if(index=='person_email')	
								{
								 $scope.person_emails = [];
								 $scope.person_emails= value;								 		
																	 
								}else if(index=='person_fax')	
								{
								 $scope.person_faxs = [];	
								 $scope.person_faxs= value;									 
								}
								$scope.person_infos.info_submitted_broker=item;
	                            $('#infosubmitted').removeAttr('checked','un');
setTimeout(function(){
 $('input').each(function(){
   if($(this).val()!=''){	 
    $(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
   }else{
	 $(this).removeAttr('style');   
   }
 });
  $('select').each(function(){
   if($(this).val()!=''){	 
    $(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
   }else{
	 $(this).removeAttr('style');   
   }
 });		
}, 300);	                       
						   
						   });		
						}
					 });
				 
           }

		   $scope.quick_fname=function(val){
              $('#quick_fname_v').html(val);
            }
		   $scope.quick_mname=function(val){
             $('#quick_mname_v').html(val);
            }
		  $scope.quick_lname=function(val){
             $('#quick_lname_v').html(val);
           }
       }




function lossesCtrl($scope,$window){

	
	$scope.rows = [];
	$scope.pd_rows = [];
	$scope.cargo_rows = [];

	$scope.loss=JSON.parse($window.losses);
	
	if($scope.loss == null){
	$scope.loss= {'losses_need' :'No'};		
	}else{
	 $scope.loss=$scope.loss[0];

	 $scope.liability_show=true;
	 $scope.pd_show=true;
	 $scope.cargo_show=true;	
	}
   
	


$scope.add_losses = function(){
  var score = $scope.loss.liability;
  var length = $scope.rows.length; 
   
 if(score>0){
	  $('#liabilty_shows').show();
	  }else{
	 $('#liabilty_shows').hide(); 
    } 
  if(score > length){     
   var i =length + 1;     
  } else {
   while(score < length){
    $scope.rows.splice($scope.toRemove, 1);
    score++;
   }
  }
  while(i <= score){
     $scope.rows.push({sequence:'','losses_fault':'No'}); 
	 data_dictionary();

   i++;
  }  
  
  
  
  var pd_score = $scope.loss.pd;
  
  var pd_length = $scope.pd_rows.length; 
  if(pd_score>0){
	  $('#pd_show').show();
	  }else{
	 $('#pd_show').hide(); 
    } 
  if(pd_score > pd_length){     
   var pd_i =pd_length + 1;     
  } else {
   while(pd_score < pd_length){
    $scope.pd_rows.splice($scope.toRemove, 1);
    pd_score++;
   }
  }
  while(pd_i <= pd_score){
     $scope.pd_rows.push({sequence:'','losses_fault':'No'}); 
	 data_dictionary();

   pd_i++;
  }  
  
  
   var cargo_score = $scope.loss.cargo;
   var cargo_length = $scope.cargo_rows.length; 
  if(cargo_score>0){
	  $('#cargo_show').show();
	  }else{
	 $('#cargo_show').hide(); 
    } 
  if(cargo_score > cargo_length){     
   var cargo_i =cargo_length + 1;     
  } else {
   while(cargo_score < cargo_length){
    $scope.cargo_rows.splice($scope.toRemove, 1);
    cargo_score++;
   }
  }
  while(cargo_i <= cargo_score){
     $scope.cargo_rows.push({sequence:'','losses_fault':'No'}); 
	 data_dictionary();

   cargo_i++;
  }  
  
  
  
 }
 $scope.show_losses_=function(item){
	 var lia=$('#'+item).attr('id');
	
	 if(lia=="liability_open"){
			$('#liability_text').text('Click To Close');
			$('#liability_open').attr('id','liability_close');
			
			}else{
			$('#liability_text').text('Click To Open');
			$('#liability_close').attr('id','liability_open');
		
			}
			
	 if($scope.liability_show){
		 $scope.liability_show=false;
		 }else{
		 $scope.liability_show=true;	 
		}
 }
 $scope.show_losses_pd=function(item){
	  var pd=$('#'+item).attr('id');
	
	 if(pd=="pd_open"){
			$('#pd_text').text('Click To Close');
			$('#pd_open').attr('id','pd_close');
		
			}else{
			$('#pd_text').text('Click To Open');
			$('#pd_close').attr('id','pd_open');
		
			}
	 if($scope.pd_show){
		 $scope.pd_show=false;
		 }else{
		 $scope.pd_show=true;	 
		}
 }
 $scope.show_losses_cargo=function(item){
	   var pd=$('#'+item).attr('id');
	
	 if(pd=="cargo_open"){
			$('#cargo_text').text('Click To Close');
			$('#cargo_open').attr('id','cargo_close');
			
			}else{
			$('#cargo_text').text('Click To Open');
			$('#cargo_close').attr('id','cargo_open');
			}
	 if($scope.cargo_show){
		 $scope.cargo_show=false;
		 }else{
		 $scope.cargo_show=true;	 
		}	 
	 }
 
 	$scope.losses_lia=JSON.parse($window.losses_lia);
	if($scope.losses_lia!=null){
		angular.forEach($scope.losses_lia, function(values, index_1) {
		
	    $scope.rows.push(values); 
		});
	}
	$scope.losses_pd=JSON.parse($window.losses_pd);
	if($scope.losses_pd!=null){
		angular.forEach($scope.losses_pd, function(values, index_1) {
	        $scope.pd_rows.push(values); 
		});
	}
	$scope.losses_cargo=JSON.parse($window.losses_cargo);
	if($scope.losses_cargo!=null){
		angular.forEach($scope.losses_cargo, function(values, index_1) {
	        $scope.cargo_rows.push(values); 
		});
	}
	

	/* $scope.losses_need_value=function(item){
				if(item=='yes'){
					$('#losses_inform').show();
					}else{
					$('#losses_inform').hide();
					}
					}*/
					
					
			
	}
myApp.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
           /* if (!ctrl) return;
            ctrl.$formatters.unshift(function (e) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });
            ctrl.$parsers.unshift(function (viewValue) {
     elem.priceFormat({
      prefix: '$ ',
      centsSeparator: ',',
      thousandsSeparator: '.'
     });         
              return elem[0].value;
            });*/
   elem.priceFormat({
     prefix: '$ ',
     centsSeparator: '.',
     thousandsSeparator: ',',
	 centsLimit: 0 
   });
        }
    };
	
}]);	

 myApp.directive('splitArray', function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, element, attr, ngModel) {

                function fromUser(text) {
                    return text.split("\n");
                }

                function toUser(array) {                        
                    return array.join("\n");
                }

                ngModel.$parsers.push(fromUser);
                ngModel.$formatters.push(toUser);
            }
        };
    })


$(document).ready(function(){
var vals=$('#coverage').val();
$('#coverages_req').val(vals);
$('#coverages_req').trigger('liszt:updated');
data_dictionary();


//setInterval(function(){get_fb();}, 10000);

 $('input').each(function(){
   if($(this).val()!=''){	 
    $(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
   }
 });
  $('select').each(function(){
   if($(this).val()!=''){	 
    $(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
   }
 });
 $('input').blur(function() {
	 if($(this).val()!=''){	
      $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
	 }else{
	  $(this).removeAttr('style');
	  }
   });

 $('select').blur(function() {	
 if($(this).val()!=''){
    $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
 }else{
	 $(this).removeAttr('style');
   }
 });
   
 $('select.chzn-select').change(function() {
	 var id=$(this).attr('id');	
	 if($(this).val()!=null){		 	 
      $('#'+id+'_chzn').css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
	  $('#'+id+'_chzn .chzn-choices').css({'border-color':'rgba(177, 72, 173, 0.8)'});
	 }else{
	  $('#'+id+'_chzn').removeAttr('style');
	  $('#'+id+'_chzn .chzn-choices').removeAttr('style');
	  }
   });
   
   
     $('#terms_from').change(function() {
	 if($(this).val()!=''){	
      $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
	 }else{
	  $(this).removeAttr('style');
	  }
   });
   
   
    $('#terms_to').change(function() {
	 if($(this).val()!=''){	
      $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
	 }else{
	  $(this).removeAttr('style');
	  }
   });
   
$('.required').blur(function(){

	  var value = $(this).val();
	  var id = $(this).attr('id');	 
	  if(value=='')
	  {
		  $('#'+id).attr('required', 'required');
		  $('#'+id).addClass('ui-state-error'); 
	  }
	  else
	  {
		  $('#'+id).removeAttr('required');
		  $('#'+id).removeClass('ui-state-error');
	  }
  });


  $('#arrow_1').click(function(){
	  $('#arrow_1').hide();
	  $('#arrow_2').show();	  
	  $('#arrows_1').hide();
	  $('#arrows_3').show();
	  $('#p_info').hide();
	  $('.width105').hide();
	  $('#p_info_1').show();	  	  
	  $(".span6.divided_broker").css("width", "90%");	  
	  $(".span6.divided_person").css("width", "6%");
	  $(".width_50").css({'width' : '50%','float':'left'});
	  $(".width_51").css({'width' : '51%','float':'left'});
	  $(".width_49").css({'width' : '49%','float':'left'});
	  $(".span6.divided_person").css("width", "6%");
	  $("#p_info_1").css("font-size", "13px");	
	  });
	  
   $('#arrow_2').click(function(){
	  $('#arrow_2').hide();
	  $('#arrow_1').show();
	  $('#arrows_1').show();
	  $('#arrows_3').hide();
	  $('#p_info').show();	
	  $('.width105').show();
	  $('#p_info_1').hide();
	  $(".span6.divided_broker").removeAttr("style");
	  $(".span6.divided_person").removeAttr("style");
	  $(".width_50").removeAttr("style");
	  $(".width_51").removeAttr("style");
	  $(".width_49").removeAttr("style");
	 
	  });
	  
      $('#arrow_3').click(function(){
	  $('#arrows_2').hide();
	  $('#arrows_1').show();
	  $('#arrow_1').show();
	  $('#arrow_3').hide();
	  $('#p_infos').show();	
	  //$('.width105').show();
	  $('#p_infos_1').hide();
	  $(".span6.divided_broker").removeAttr("style");
	  $(".span6.divided_person").removeAttr("style");
	  $(".widths_50").removeAttr("style");
	  $(".widths_51").removeAttr("style");
	  $(".widths_49").removeAttr("style");
	  $('.width_50').show();
	  $('.width_51').show();
	  $('.width_49').show();
	  $('.width_46').show();	 
	  }); 
	  
	  $('#arrows_1').click(function(){
	  $('#arrows_1').hide();
	  $('#arrows_2').show();
	  $('#arrow_1').hide();
	  $('#arrow_3').show();
	  $('#p_infos').hide();
	  //$('.width105').hide();
	  $('#p_infos_1').show();	  	  
	  $(".span6.divided_broker").css("width", "6%");	  
	  $(".span6.divided_person").css("width", "90%");
	  $(".widths_50").css({'width' : '50%','float':'left'});
	  $(".widths_51").css({'width' : '51%','float':'left'});
	  $(".margin-14").css({'margin-left' : '-14%'});
	  $(".widths_49").css({'width' : '49%','float':'left'});	  
	  $("#p_info_1").css("font-size", "13px");
	  $('.width_50').hide();
	  $('.width_51').hide();
	  $('.width_49').hide();
	  $('.width_46').hide();	 
	  });
	  
   $('#arrows_2').click(function(){
	  $('#arrows_2').hide();
	  $('#arrows_1').show();
	  $('#p_infos').show();	
	  $('#arrow_1').show();
	  $('#arrow_3').hide();
	  $('#p_infos_1').hide();
	  $(".span6.divided_broker").removeAttr("style");
	  $(".span6.divided_person").removeAttr("style");
	  $(".widths_50").removeAttr("style");
	  $(".widths_51").removeAttr("style");
	  $(".widths_49").removeAttr("style");
	  $('.width_50').show();
	  $('.width_51').show();
	  $('.width_49').show();
	  $('.width_46').show();	 
	  });
	  
	 $('#arrows_3').click(function(){
	  $('#arrow_2').hide();
	  $('#arrow_1').show();
	  $('#arrows_1').show();
	  $('#arrows_3').hide();
	  $('#p_info').show();	
	  $('.width105').show();
	  $('#p_info_1').hide();
	  $(".span6.divided_broker").removeAttr("style");
	  $(".span6.divided_person").removeAttr("style");
	  $(".width_50").removeAttr("style");
	  $(".width_51").removeAttr("style");
	  $(".width_49").removeAttr("style");
	 
	  }); 
	
});
if (window.history && window.history.pushState) { 
   
    $(window).on('popstate', function() {
      if(confirm('Your work will be lost Click Ok to continue or Cancel.')){
		  window.history.go(-1)  
		  
		  }else{
		   window.history.pushState('forward', null,'');	
		 }	 
    });
    window.history.pushState('forward', null,'');
   }
 /*  setInterval(function(){
    var save='saved';  	
	var baseurl = $("#baseurls").val();
	var formData = new FormData($('#main_form')[0]);
	var request_id=$("#req_id").val();	
	var insured_name=$('#insured_name').val();
	var insured_last_name=$('#insured_last_name').val();
	if(insured_name!='' || insured_last_name!=''){
	if(request_id==''){
		console.log(request_id);
	$.ajax({
		type: "POST",
		url: baseurl+"index.php/quote/information_save/"+save+"/"+request_id+"", 
		data:formData,
		async:false,
		processData: false,
        contentType: false,	
		success: function(id) 
		{ 					
		  if(request_id==''){
			 		 
			$('#req_id').val($.trim(id));
			var action =$('form').attr('action');
			$('form').attr('action',action+'/'+$.trim(id));
		   }
		}
	 
     });
	} 
  }
}, 3000);*/

	 setTimeout(function(){		 
      corp_name($('#in_this_is').val());
	  fill_color();
	 }, 6000);
    function commodities_haulted_v(val){	
		
		  $('.commhauled2').val(val);
		} 
		
		
	function validateForm() {

       var form_valid=$('#coverages_req').val();		
	   var form_nature=$('#nature_of_business').val();
	   var form_com=$('#commodities_haulteds').val();
	   var form_cov=$('#coverage').val();
	   var form_vh_type=$('#vh_type').val();
	 
	   var coverages_req = $('#coverages_req').val();
	  if(!form_valid ){ 
	         event.preventDefault() ;		
		    $('#coverages_req_chzn input').focus();
		    $( "#coverages_req_chzn" ).addClass( "ui-state-error" );
		
		
		} else if(coverages_req!=null){
	    $( "#coverages_req_chzn" ).removeClass( "ui-state-error" );	
		if(coverages_req.indexOf('Liability')>-1){
		 if(!form_nature ){      
           	event.preventDefault() ;		
			$('#nature_of_business_chzn input').focus();
			$("#nature_of_business_chzn" ).addClass( "ui-state-error" );
		        
        }else if(!form_cov ){
			event.preventDefault() ;		
			$('#coverage_chzn input').focus();
			$( "#coverage_chzn" ).addClass( "ui-state-error" );
			$( "#nature_of_business_chzn" ).removeClass( "ui-state-error" );  			
				  
			}else if(!form_vh_type ){
			 event.preventDefault() ;
			 $('#vh_type_chzn input').focus();
			 $( "#vh_type_chzn" ).addClass( "ui-state-error" );
			 $("#coverage_chzn" ).removeClass( "ui-state-error" );	
			 
		      }else{
			  $("#vh_type_chzn" ).removeClass( "ui-state-error" ); 
			 }
		  }
		}
  }
  
  
function data_dictionary(){
	
setTimeout(function(){
   var vals=$('#coverage').val();
   var module =$("#modules").val();
   var url = $("#baseurls").val();  
  	$.ajax({
		type: "POST",
		url: url+"quote/data_dictionary", 
		async:true, 						             
        processData: false,
        contentType: false,		
		success: function(json) 
		{ 			
		var obj = $.parseJSON( json );
		$(".prefer_id").remove();
		  $.each(obj, function(i, val) {              
				$('input[type="text"]').each(function(index, element) {
					if(val['prefer']=='1' && val['field_name']==$(this).attr('name') && val['coverage']==vals){
							//console.log($(".prefer_id"));
							$(this).css('margin-bottom','0px');
							$(this).after('<i class="prefer_id" style="font-size:12px;">Prefer to have</i>');
						} else if(val['prefer']=='2' && val['field_name']==$(this).attr('name') && val['coverage']==vals){
							//console.log($(".prefer_id"));
							$(this).css('margin-bottom','0px');
							$(this).after('<i class="prefer_id" style="font-size:12px;">Optional</i>');
						} else{
							$(".prefer_id").remove();
						}
					var name=$(this).attr('name');
						if(val['field_name']!='insured_name' && val['field_name']!='dba'){
							 if(val['field_name']==name){
							 if(val['app']==module){								 													
							   if(val['show']==1){								   
									var coverages_req = $('#coverages_req').val();
									if(coverages_req != null && val['coverage']!='' ){
										
									 if(val['app']==module && coverages_req.indexOf(val['coverage'])>-1){
										 $(this).css('background-color','#'+val['color']);
										 var id=$(this).attr('id');
										 $('#'+id+'_chzn .chzn-choices').css('background','#'+val['color']);
										 
										 if(val['mandatory']=='Yes'){
											 $(this).attr({required:"required"});																		  								 
											  var ids=name.replace('[]','');
											  var h=$('#label_'+ids).text();											
											  if(h!='undefined' || h!=''){
												if(h.indexOf('*')==-1){																		 
											   var htm=h.replace('*','');								  								 
											    $('#label_'+ids).html(htm+'*');
												}
											  }
											  
											  var h1=$('.label_'+ids).text();									 										  
											  if(h1!='undefined' || h1!=''){										  
												  if(h1.indexOf('*')==-1){										  										 
												  var htm1=h1.replace('*','');								  								 
												  $('.label_'+ids).html(htm1+'*');	
												  }
											  }
											 }
								
										 
								}else{
										  if(val['mandatory']=='Yes'){
											 $(this).removeAttr("required");																		  								 
											  var ids=name.replace('[]','');
											  var h=$('#label_'+ids).text();											
											  if(h!='undefined' || h!=''){
												if(h.indexOf('*')>-1){																		 
											   var htm=h.replace('*','');								  								 
											    $('#label_'+ids).html(htm);
												}
											  }
											  
											  var h1=$('.label_'+ids).text();									 										  
											  if(h1!='undefined' || h1!=''){										  
												  if(h1.indexOf('*')>-1){										  										 
												  var htm1=h1.replace('*','');								  								 
												  $('.label_'+ids).html(htm1);	
												  }
											  }
										   }
										 var id=$(this).attr('id');
										 $('#'+id+'_chzn .chzn-choices').removeAttr('style');
										 $(this).removeAttr('style');	 
										}
									  }else{
										  if(val['coverage']!=''){
											  
										    if(val['mandatory']=='Yes'){
											 $(this).removeAttr("required");																		  								 
											  var ids=name.replace('[]','');
											  var h=$('#label_'+ids).text();											
											  if(h!='undefined' || h!=''){
												if(h.indexOf('*')>-1){																		 
											   var htm=h.replace('*','');								  								 
											    $('#label_'+ids).html(htm);
												}
											  }
											  
											  var h1=$('.label_'+ids).text();									 										  
											  if(h1!='undefined' || h1!=''){										  
												  if(h1.indexOf('*')>-1){										  										 
												  var htm1=h1.replace('*','');								  								 
												  $('.label_'+ids).html(htm1);	
												  }
											  }
										   }
										  
										  $(this).removeAttr('style');
										  }
										}
									 if(val['min_value']!=''|| val['max_value']!=''){
										$(this).attr({min:val['min_value'],max:val['max_value']});
									 }
								    if(val['disable']==1){
									 $(this).attr({Readonly:'Readonly'});
									 }
								    $(this).attr({placeholder:val['placeholder']});
								    $(this).attr({title:val['title']});
								 	
								   if(val['default_value']!=''){
									 
									 if($(this).val()=='' || $(this).val()==' '){
									 $(this).val(val['default_value']); 
									 }
									 }	
							       }else{
									$(this).hide();   
									}
								 }else{	
								 	 			
							   if(val['default_value']!=''){								   									 
							            $(this).val(' ');
									 }
									 
									if(val['mandatory']=='Yes'){
									 $(this).attr({required:"required"});																		  								 
									 var ids=name.replace('[]','');
									  var h=$('#label_'+ids).text();
									  if(h!='undefined' || h!=''){
										if(h.indexOf('*')==-1){							 
									   var htm=h.replace('*','');								  								 
									   $('#label_'+ids).html(htm+'*');
										}
									  }
									  var h1=$('.label_'+ids).text();									  
									  if(h1!='undefined' || h1!=''){
										  if(h1.indexOf('*')==-1){										 
									      var htm1=h1.replace('*','');								  								 
									      $('.label_'+ids).html(htm1+'*');	
										  }
									  }
									 }
									var coverages_req = $('#coverages_req').val();
									if(coverages_req != null && val['coverage']!='' ){
									 if(val['app']==module && coverages_req.indexOf(val['coverage'])>-1){
										 $(this).removeAttr('style');
										 }else{
										 $(this).removeAttr('style');	 
										}
									}else{
										$(this).removeAttr('style');	
										}
									if(val['min_value']!=''|| val['max_value']!=''){
										$(this).removeAttr('min_value');
							            $(this).removeAttr('max_value');
									 }
								 if(val['disable']==1){
									  $(this).removeAttr('Readonly');
									 }
								 $(this).attr({placeholder:''});
								 $(this).attr({title:''});	
								 if(val['default_value']!=''){
									 if($(this).val()==''){
									 $(this).val(val['default_value']); 
									 }
									 }	
									 
									 
						       }
							 }
						}
							   
							});
						$('select').each(function(index, element) {                
							 var name=$(this).attr('name');
							 if(val['field_name']!='truck_radius_of_operation[]'){
							 if(val['field_name']==name){
							 if(val['app']==module){								 													
							   if(val['show']==1){								   
									var coverages_req = $('#coverages_req').val();
									if(coverages_req != null && val['coverage']!='' ){
										
									 if(val['app']==module && coverages_req.indexOf(val['coverage'])>-1){
										 $(this).css('background-color','#'+val['color']);
										 var id=$(this).attr('id');
										 $('#'+id+'_chzn .chzn-choices').css('background','#'+val['color']);
										 
										 if(val['mandatory']=='Yes'){
											 $(this).attr({required:"required"});																		  								 
											  var ids=name.replace('[]','');
											  var h=$('#label_'+ids).text();											
											  if(h!='undefined' || h!=''){
												if(h.indexOf('*')==-1){																		 
											   var htm=h.replace('*','');								  								 
											    $('#label_'+ids).html(htm+'*');
												}
											  }
											  
											  var h1=$('.label_'+ids).text();									 										  
											  if(h1!='undefined' || h1!=''){										  
												  if(h1.indexOf('*')==-1){										  										 
												  var htm1=h1.replace('*','');								  								 
												  $('.label_'+ids).html(htm1+'*');	
												  $('.label_make_truck').html(' ');
												  $('.label_make_truck').html('2 Make*');
												  }
											  }
											 }
								
									if(val['disable']==1){
									 $(this).find('option:not(:selected)').remove();								
									 $(this).attr({Readonly:'Readonly'});
									 } 
								}else{   if(val['coverage']!=''){
										  if(val['mandatory']=='Yes'){
											 $(this).removeAttr("required");																		  								 
											  var ids=name.replace('[]','');
											  var h=$('#label_'+ids).text();
											 											
											  if(h!='undefined' || h!=''){
												if(h.indexOf('*')>-1){																		 
											   var htm=h.replace('*','');
											   								  								 
											    $('#label_'+ids).html(htm);
												
												}
											  }
											  
											  var h1=$('.label_'+ids).text();	
											  								 										  
											  if(h1!='undefined' || h1!=''){										  
												  if(h1.indexOf('*')>-1){										  										 
												  var htm1=h1.replace('*','');	
												   
												  							  								 
												  $('.label_'+ids).html(htm1);
												  $('.label_make_truck').html('');
												  $('.label_make_truck').html('2 Make');	
												  }
											  }
										   }
										 var id=$(this).attr('id');
										 $('#'+id+'_chzn .chzn-choices').removeAttr('style');
										 $(this).removeAttr('style');	 
										}
									   }
									  }else{
										  if(val['coverage']!=''){
											  
										    if(val['mandatory']=='Yes'){
											 $(this).removeAttr("required");																		  								 
											  var ids=name.replace('[]','');
											  var h=$('#label_'+ids).text();											
											  if(h!='undefined' || h!=''){
												if(h.indexOf('*')>-1){																		 
											   var htm=h.replace('*','');								  								 
											    $('#label_'+ids).html(htm);
												}
											  }
											  
											  var h1=$('.label_'+ids).text();									 										  
											  if(h1!='undefined' || h1!=''){										  
												  if(h1.indexOf('*')>-1){										  										 
												  var htm1=h1.replace('*','');								  								 
												  $('.label_'+ids).html(htm1);	
												  $('.label_make_truck').html('2 Make');
												  }
											  }
										   }
										  var id=$(this).attr('id');
										  $('#'+id+'_chzn .chzn-choices').removeAttr('style');
										  $(this).removeAttr('style');
										  
										  }
										}
									 if(val['min_value']!=''|| val['max_value']!=''){
										$(this).attr({min:val['min_value'],max:val['max_value']});
									 }
								    
								    $(this).attr({placeholder:val['placeholder']});
								    $(this).attr({title:val['title']});
								 	
								   if(val['default_value']!=''){
									 
									 if($(this).val()=='' || $(this).val()==' '){
									 $(this).val(val['default_value']); 
									 }
									 }	
							       }else{
									$(this).hide();   
									}
								 }else{	
								 	 			
							   if(val['default_value']!=''){								   									 
							            $(this).val(' ');
									 }
									 
									if(val['mandatory']=='Yes'){
									 $(this).attr({required:"required"});																		  								 
									  var ids=name.replace('[]','');
									  var h=$('#label_'+ids).text();
									  if(h!='undefined' || h!=''){
										if(h.indexOf('*')==-1){	
																 
									   var htm=h.replace('*','');								  								 
									   $('#label_'+ids).html(htm+'*');
										}
									  }
									  var h1=$('.label_'+ids).text();									  
									  if(h1!='undefined' || h1!=''){
										  	
										  if(h1.indexOf('*')==-1){										 
									      var htm1=h1.replace('*','');								  								 
									      $('.label_'+ids).html(htm1+'*');	
										  }
									  }
									 }
									var coverages_req = $('#coverages_req').val();
									if(coverages_req != null && val['coverage']!='' ){
									 if(val['app']==module && coverages_req.indexOf(val['coverage'])>-1){
										 $(this).removeAttr('style');
										 var id=$(this).attr('id');
										 $('#'+id+'_chzn .chzn-choices').removeAttr('style');
										 }else{
										 var id=$(this).attr('id');
										 $('#'+id+'_chzn .chzn-choices').removeAttr('style');
										 $(this).removeAttr('style');	 
										}
									}else{
										$(this).removeAttr('style');	
										}
									if(val['min_value']!=''|| val['max_value']!=''){
										$(this).removeAttr('min_value');
							            $(this).removeAttr('max_value');
									 }
								 if(val['disable']==1){
									  $(this).removeAttr('Readonly');
									 }
								 $(this).attr({placeholder:''});
								 $(this).attr({title:''});	
								 if(val['default_value']!=''){
									 if($(this).val()==''){
									 $(this).val(val['default_value']); 
									 }
									 }	
									 
									 
						       }
							 }
						}else{
						 radius_ope();	
						}
							   
					});
             }); 
			 setTimeout(function(){
		$('select.chzn-select').each(function(index, element) {
           $(this).hide();
        });	
	}, 200)       		
		}
    });
  }, 100);
}