$(document).ready(function(e) {


$('.efl3').change(function(){
	if (($(this).val()=="No") || ($(this).val()=="Rejected") || ($(this).val()=="None") || ($(this).val()=="0"))
	{
		$(this).parents('.row-efl3').find('.mkread1').attr("disabled",true); 			
	}
	else
	{
		$(this).parents('.row-efl3').find('.mkread1').attr("disabled",false);
	}
});

function rofoperationOther(val , id){
	if(val == 'other'){
		$('#'+id).show();
	} else {
		$('#'+id).hide();
	}	
}
$('#tractor_radius_of_operation_pd').change(function(e) {
    val = $(this).val();
	rofoperationOther(val , 'pdRoOther');
});
$('.rocargo').change(function(e) {
    val = $(this).val();
	rofoperationOther(val , 'cargo_show_tractor_others');
});
	
});

function changeCarrier1(val, type, url, selected)
{ 
	var baseurl = url;
	var val1 = $("#quote_id").val();
	$.ajax({
	  url: baseurl,
	  type: "post",
	  data: {'state' : val,'id':val1,'type':type, 'selected':selected},
	 datatype: 'json',
	  success: function(data){
			//alert(data);	
			if(type=='cargo') { 
				$('.cargo_carrier_drop_down').html(data);	
				updatePolicyValue(type, '');	
			}else if(type=='pd') {
				$('.pd_carrier_drop_down').html(data);	
				updatePolicyValue(type, '');
			}else if(type=='liability'){					
				$('.liability_carrier_drop_down').html(data);	
				updatePolicyValue(type, '');
			}else if(type=='excess_cargo') { 
				$('.excess_cargo_carrier_drop_down').html(data);	
				updatePolicyValue(type, '');	
			}
	  },
	  error:function(){
		
	  }   
	});
}
function updatePolicyValue1(type, val, url){	
var state='';
	if(val==''){
		if(type=='cargo'){
			var val = $('#carrier_cargo option:selected').val();
			
		} else if(type=='pd'){
			var val = $('#carrier_cargo2 option:selected').val();
			
		}else if(type=='liability'){
			var val = $('#carrier_cargo3 option:selected').val();
			
		}
		else if(type=='excess_cargo'){
			var val = $('#excess_carrier_cargo option:selected').val();
			
		}
	}

	if(type=='cargo'){
			
			state=$('#main_form2 select[name=state]').val();
		} else if(type=='pd'){
			
			state=$('#main_form3 select[name=state]').val();
		}else if(type=='liability'){
			
			state=$('#main_form4 select[name=state]').val();
		}
		else if(type=='excess_cargo'){
			
			 state=$('#main_form6 select[name=excess_state]').val();
		}
		
	
var val1 = $("#quote_id").val();

if(val!='')
{
	var baseurl = url;
		$.ajax({
		  url: baseurl,
		  type: "post",
		  data: {'name' : val,'id':val1,'type':type,'states':state},
		   //data: {},
		 datatype: 'json',
		  success: function(data){
			 var obj = $.parseJSON(data);  
			
			 if(type=='cargo'){
				 $("#cargo_policy_fee").val(obj['cargo_policy_fee']);					 
				 //console.log(return_numeric(obj['slatax']));
				 //console.log(return_numeric($("#total_cargo_premium1").val()));
				 var sla_tax = return_numeric($("#total_cargo_premium1").val()) * return_numeric(obj['slatax']);	     
				 sla_tax = sla_tax/100; 
				 					 	 
				var  element= $("#cargo_sla_tax");
				applyDecimal(element, sla_tax, 2);
				//$("#cargo_sla_tax").val(values);
			}else if(type=='pd') {
				// $("#pd_policy_fee").val(obj['cargo_policy_fee']);					 
				 var sla_tax = return_numeric($("#total_pd_pr").val()) * return_numeric(obj['slatax']);	
				 sla_tax = sla_tax/100; 						 			 
				 var  element=$("#pd_sla_tax");
				 applyDecimal(element, sla_tax, 2);
			}else if(type=='liability') {
				 //$("#liability_policy_fee").val(obj['cargo_policy_fee']);					 
				 //$("#cargo_sla_tax").val(return_numeric($("#total_cargo_premium1").val()) * return_numeric(obj['slatax']));
			}else  if(type=='excess_cargo'){
				// $("#excess_cargo_policy_fee").val(obj['cargo_policy_fee']);					 
				// console.log(return_numeric(obj['slatax']));
				 //console.log(return_numeric($("#total_cargo_premium1").val()));
				 var sla_tax = return_numeric($("#excess_total_cargo_premium1").val()) * return_numeric(obj['slatax']);	     
				 sla_tax = sla_tax/100; 
				 						 	 
				  var  element=$("#excess_cargo_sla_tax");
				  applyDecimal(element, sla_tax, 2);
				//$("#excess_cargo_sla_tax").val(values);
			}
			 $('#cargo_policy_fee').priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});
				$('.price_format').priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});
		/*		$("#cargo_sla_tax").priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});
				$("#excess_cargo_sla_tax").priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});
				$("#pd_sla_tax").priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});*/
		  },
		  error:function(){
			
		  }   
		});
	}
	else
	{
		$('#cargo_policy_fee').val('');
		//$('#cargo_sla_tax').val('');
		$('#pd_policy_fee').val('');
		$('#pd_sla_tax').val('');
	}
	
	
	//alert(val);
}

function pip_change_coverages(type, val, url,state){
	       if(val!=''){
				if(state!=''){
			var states=state;
				}else{
			//var states=$('#main_form3 select[name=state]').val();		
				}
			
			var baseurl = url;
			$.ajax({
		    url: baseurl,
		    type: "post",
		    data: {'name' : val,'state':state,'coverages':'pip'},
		    datatype: 'post',
		    success: function(data){ 
			  $('.pip_changes_option').html(data);
			  }
			});
		   }
		
	}
	function um_change_coverages(type, val, url,state){
		
		    if(val!=''){
				if(state!=''){
			var states=state;
				}else{
			//var states=$('#main_form3 select[name=state]').val();		
				}
			var baseurl = url;
			$.ajax({
		    url: baseurl,
		    type: "post",
		    data: {'name' : val,'state':states,'coverages':'um'},
		    datatype: 'post',
		    success: function(data){
			    $('.um_changes_option').html(data);
			  }
			});
		}
	}
	
		function uim_change_coverages(type, val, url,state){
		
		    if(val!=''){
				if(state!=''){
			var states=state;
				}else{
			//var states=$('#main_form3 select[name=state]').val();		
				}
			var baseurl = url;
			$.ajax({
		    url: baseurl,
		    type: "post",
		    data: {'name' : val,'state':states,'coverages':'uim'},
		    datatype: 'post',
		    success: function(data){
			    $('.uim_changes_option').html(data);
			  }
			});
		}
	}