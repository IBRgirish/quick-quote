/*************Pd Calculation ************
 ****************** Ashvin Patel 21/july/2014**********/
$(document).ready(function(e) {
	
	$('.efl,.ou').change(function(){					
	if (($(this).val()=="None") || ($(this).val()=="0") || ($(this).val()=="No")||($(this).val()==""))
	{
		$(this).parents('.row-efl').find('.mkread1').attr("disabled",true); 		
		$(this).parents('.row-efl').find('.mkread1').val(0);
		$(this).parents('.row-efl').find('.tls').val('None'); 
		$(this).parents('.row-efl').find('.drp').val('$ 0');
		$(this).parents('.row-efl').find('.tlp').val('$ 0');
		$(this).parents('.row-efl').find('.nvp').val('$ 0');
		$(this).parents('.row-efl').find('.scp').val('$ 0');		
		$(this).parents('.row-efl').find('.efp').val(0);
		$(this).parents('.row-efl').find('.ofp').val(0);
		$('.price_format_decimal').priceFormat({
			prefix: '$ ',
			centsLimit: 2,
			thousandsSeparator: ','
		}); 
	}
	else
	{
		$(this).parents('.row-efl').find('.mkread1').attr("disabled",false);
	}
	calculatepd();
});
$('.vehicle_rate, .vehicle_frate, .driver_surcharge').blur(function(e) {
	calculatePdAllPremium();
});
function calculatePdAllPremium(){	
	$('.row-efl').each(function() {
		var lval = $(this).find('.efl').val();
		if((lval != 'No') && (lval != '') && (lval != 'Rejected') && (lval != 'None') && (lval != '0') && (lval != undefined)){
			if($(this).find(".nvc").val() != undefined)
			{
				  reCalculatePdAllPremiumn($(this));				
			}
		}
	});
}
$('.no_of_tractor').blur(function(e) {
    var val1 = $('select[name=debris_removal_limit]').val();
	if((val1 != 'No') && (val1 != '') && (val1 != 'Rejected') && (val1 != 'None') && (val1 != '0') && (val1 != undefined)){
		var element = $('.row-efl');
		if(element.find(".drc").val() != undefined)
		{
			 var val2 = $(this).val();	
			 var val3 = element.find(".drc").val();
			 var val4 = val2*val3;
			 $('.drp').val(val4);	
			 $('.price_format').priceFormat({
				prefix: '$ ',
				centsLimit: 0,
				thousandsSeparator: ','
			});		
		}
	}	
});
$('.no_of_tractor').blur(function(e) {
    var val1 = $('select[name=debris_removal_limit]').val();
	if((val1 != 'No') && (val1 != '') && (val1 != 'Rejected') && (val1 != 'None') && (val1 != '0') && (val1 != undefined)){
		var element = $('.row-efl');
		if(element.find(".drc").val() != undefined)
		{
			 var val2 = $(this).val();	
			 var val3 = element.find(".drc").val();
			 var val4 = val2*val3;
			 $('.drp').val(val4);	
			 $('.price_format').priceFormat({
				prefix: '$ ',
				centsLimit: 0,
				thousandsSeparator: ','
			});		
		}
	}
	
	 var val5 = $('select[name=towing_labor_storage]').val();
	 if((val5 != 'No') && (val5 != '') && (val5 != 'Rejected') && (val5 != 'None') && (val5 != '0') && (val5 != undefined)){
		var element = $('.row-efl');
		if(element.find(".tls").val() != undefined)
		{
			 var val6 = $(this).val();	
			 var val7 = element.find(".tls").val();
			 var val8 = val6*val7;
			 $('.tlp').val(val8);	
			 $('.price_format').priceFormat({
				prefix: '$ ',
				centsLimit: 0,
				thousandsSeparator: ','
			});		
		}
	}	
	calculateTTTPremium();
});
function reCalculatePdAllPremiumn(element){
	var field = element.find('.nvc').attr('name');
	var val1=return_numeric($('.pd_base_rate').val());
	//var val2=$('.quantity').val();
	var val2 = element.find('.nvc').val();
	if((field=='losses_surcharge_cost')||(field=='miscellaneos_surcharge_cost'))
	{
		var tractor_p = return_numeric($("#total_tractor_premium_pd").val());
		var trailer_p = return_numeric($("#total_trailer_premium_pd").val());
		total = (tractor_p+trailer_p)*val2;
		total = total/100;
		var element1 = element.find(".nvp");									
		applyDecimal(element1, total, 2);
		
	}
	else if(field='driver_surcharge_cost')
	{
		var driver_surcharge = $(".driver_surcharge").val();
		total = driver_surcharge*val2;
		total = total/100;
		var element1 = element.find(".nvp");									
		applyDecimal(element1, total, 2);
	}
	else{
		total = val1*val2;
		total = total/100;
		var element1 = element.find(".nvp");									
		applyDecimal(element1, total, 2);
	}	
	calculatepd();	
}
function calculatepd(){
	var tot_nvp_pre = 0;
	$('.nvp').each(function() {
		tot_nvp_pre = tot_nvp_pre + parseFloat(return_numeric($(this).val()));
	});
	var scp = return_numeric($(".scp").val());
	var drp = return_numeric($(".drp").val());
	var tlp = return_numeric($(".tlp").val());
	var total_tractor_pre = return_numeric($("#total_tractor_premium_pd").val());
	var total_trailer_pre = return_numeric($("#total_trailer_premium_pd").val());
	total_pd_pre = parseFloat(tot_nvp_pre) + parseFloat(scp) + parseFloat(drp) + parseFloat(tlp) + parseFloat(total_tractor_pre) + parseFloat(total_trailer_pre);
	
	element = $("#total_pd_pr");	
	applyDecimal(element, total_pd_pre, 2);
	updatePolicyValue('pd', '');	
	$('.price_format_decimal').priceFormat({
		prefix: '$ ',
		centsLimit: 2,
		thousandsSeparator: ','
	});
}
$('.nvc').change(function(){
	var field = $(this).attr('name');
	var val1=return_numeric($('.pd_base_rate').val());
	//var val2=$('.quantity').val();
	var val2=$(this).val();
	if((field=='losses_surcharge_cost')||(field=='miscellaneos_surcharge_cost'))
	{
		var tractor_p = return_numeric($("#total_tractor_premium_pd").val());
		var trailer_p = return_numeric($("#total_trailer_premium_pd").val());
		total = (tractor_p+trailer_p)*val2;
		total = total/100;
		var element = $(this).parent().parent().find(".nvp");									
		applyDecimal(element, total, 2);
		
	}
	else if(field='driver_surcharge_cost')
	{
		var driver_surcharge = $(".driver_surcharge").val();
		total = driver_surcharge*val2;
		total = total/100;
		var element = $(this).parent().parent().find(".nvp");									
		applyDecimal(element, total, 2);
	}
	else{
		total = val1*val2;
		total = total/100;
		var element = $(this).parent().parent().find(".nvp");									
		applyDecimal(element, total, 2);
	}
	
	
	calculatepd();
});
$('.tractor_trailer_qunt,.ttcc').change(function(){
	var val5=return_numeric($('.ttcc').val());			
	var val6=$('.tractor_trailer_qunt').val();
	$(".ttcp").val(val5 * val6);	
	calculatepd();
	$(".ttcp").priceFormat({
		prefix: '$ ',
		centsLimit: 0,
		thousandsSeparator: ','
	});
	
});
$(".drc").change(function(){  
  var val1 = $(this).val();
  var val2 = $('.no_of_tractor').val();
  $(".drp").val(val1*val2);
  calculatepd();
  $('.drp').priceFormat({
	  prefix: '$ ',
	  centsLimit: 0,
	  thousandsSeparator: ','
  });
});

$(".tls").change(function(){					
  var val1 = $(this).val();
  var val2 = $('.no_of_tractor').val();
  $(".tlp").val(val1*val2);
  calculatepd();
  $('.tlp').priceFormat({
	  prefix: '$ ',
	  centsLimit: 0,
	  thousandsSeparator: ','
  });
});
$('.vehicle_rp').blur(function(){
	calculateTTTPremium();
});
function calculateTTTPremium(){
	tot_trailer = 0;
	tot_trac_premium = 0;
	tot_truc_premium = 0;
	$(".trailer_prem").each(function(){
		if($(this).val() != '')
		{
			tot_trailer =  tot_trailer + parseFloat(return_numeric($(this).val()));
		}	
	});
	var element = $("#total_trailer_premium_pd");
	applyDecimal(element, tot_trailer, 2);
	$(".tractor_prem").each(function(){
		if($(this).val() != '')
		{
			tot_trac_premium =  tot_trac_premium + parseFloat(return_numeric($(this).val()));
			
		}		
	});
	var element = $("#total_tractor_premium_pd");
	applyDecimal(element, tot_trac_premium, 2);
	/**By Ashvin Patel 12/may/2014**/
	$(".truck_prem1").each(function(){
		if($(this).val() != '')
		{
			tot_truc_premium =  tot_truc_premium + parseFloat(return_numeric($(this).val()));
			
		}		
	});	
	var element = $("#total_truck_premium_pd");
	applyDecimal(element, tot_truc_premium, 2);
	calculatepd();	
	calculatePdAllPremium();
}
$('body').on('blur','.vehicle_frate',function(e){
	var totded = $(this).val();
	var frate = $(this).val();
	var rate = $(this).parent().parent().find(".vehicle_rate").val();
	var ded = return_numeric($(this).parent().parent().find(".vehicle_value").val());
	if(rate != ''){						
		totded1 = (parseFloat(rate) * parseFloat(ded))/100;
		if(totded!=''){
			var n = totded1.toString();
			var lastChar = n.substr(-1);				
			if(lastChar=='0'){
				
				totded = parseFloat(totded) + parseFloat(totded1);								
				if(isInt(totded)){
					//totded = totded+'0';	
				}
				totded = parseFloat(totded);
				totded = totded.toFixed(2);
			}else{
				totded = parseFloat(totded) + parseFloat(totded1);
				if(isInt(frate)){
					var n = totded.toString();
					var lastChar = n.substr(-1);
					if(lastChar!='0'){
						//totded = totded+'0';
					}
				}
				totded = parseFloat(totded);
				totded = totded.toFixed(2);
			}
			
		}else{
			totded = parseFloat(totded1);
			totded = totded.toFixed(2);
		}
	}else if(totded != ''){							
		totded = parseFloat(totded);
		totded = totded.toFixed(2);
	}else{
		//totded = $(this).parent().parent().find(".vehicle_rp").val();
	}
	
	$(this).parent().parent().find(".vehicle_rp").val(totded);
	$(this).parent().parent().find(".vehicle_rp").priceFormat({
		  prefix: '$ ',
		  centsLimit: 2,
		  thousandsSeparator: ','
	  });
	tot_trailer = 0;
	tot_trac_premium = 0;
	tot_truc_premium = 0;
	$(".trailer_prem").each(function(){
		if($(this).val() != '')
		{
			tot_trailer =  tot_trailer + parseFloat(return_numeric($(this).val()));
			
			
		}
		var element = $("#total_trailer_premium_pd");
		applyDecimal(element, tot_trailer, 2);
	});
	$(".tractor_prem").each(function(){
		if($(this).val() != '')
		{
			tot_trac_premium =  tot_trac_premium + parseFloat(return_numeric($(this).val()));
			
		}
		var element = $("#total_tractor_premium_pd");
		applyDecimal(element, tot_trac_premium, 2);
	});
	$(".truck_prem1").each(function(){
		if($(this).val() != '')
		{
			tot_truc_premium =  tot_truc_premium + parseFloat(return_numeric($(this).val()));
			
		}
		var element = $("#total_truck_premium_pd");
		applyDecimal(element, tot_truc_premium, 2);
	});
	calculatepd();	
	calculatePdAllPremium();		
});
function calculateTTTRowPremium(){
	$('.tractorrw').each(function() {
        element = $(this);
		var limit = return_numeric(element.find('.tractor_value').val());
		//console.log(limit);
		var rate = element.find('.vehicle_rate').val();
		//console.log(rate);
		var frate = element.find('.vehicle_frate').val();
		//console.log(frate);
		var premium = (limit*rate)/100;
		if(frate!=''){
			premium = parseFloat(premium) + parseFloat(frate);
		}		
		element1 = element.find('.tractor_prem');
		applyDecimal(element1, premium, 2);
		
    });	
	
	$('.truckrw').each(function() {
        element = $(this);
		var limit = return_numeric(element.find('.truck_value').val());
		//console.log(limit);
		var rate = element.find('.vehicle_rate').val();
		//console.log(rate);
		var frate = element.find('.vehicle_frate').val();
		//console.log(frate);
		var premium = (limit*rate)/100;
		if(frate!=''){
			premium = parseFloat(premium) + parseFloat(frate);
		}
		element1 = element.find('.truck_prem1');
		applyDecimal(element1, premium, 2);
		//calculatePdAllPremium();
    });	
	
	$('.trailerrw').each(function() {
        element = $(this);
		var limit = return_numeric(element.find('.trailer_value').val());
		//console.log(limit);
		var rate = element.find('.vehicle_rate').val();
		//console.log(rate);
		var frate = element.find('.vehicle_frate').val();
		//console.log(frate);
		var premium = (limit*rate)/100;
		if(frate!=''){
			premium = parseFloat(premium) + parseFloat(frate);
		}
		element1 = element.find('.trailer_prem');
		applyDecimal(element1, premium, 2);
		//calculatePdAllPremium();
    });	
	//calculatepd();	
	calculateTTTPremium();
	calculatePdAllPremium();
}
$('body').on('blur','.tractor_value',function(e){
	totallimit = 0;
	$(".tractor_value").each(function(){
		totallimit = totallimit + parseInt(return_numeric($(this).val()));
	});
	
	$(".total_tractor_limit").val(totallimit);
	//apply_on_all_elements();
	calculateTTTRowPremium();
});
$('body').on('blur','.trailer_value',function(e){
	totallimit = 0;
	$(".trailer_value").each(function(){
		totallimit = totallimit + parseInt(return_numeric($(this).val()));
	});
	
	$(".total_trailer_limit").val(totallimit);
	//apply_on_all_elements();
	calculateTTTRowPremium();
});				
/**By Ashvin Patel 12/may/2014**/
$('body').on('blur','.truck_value',function(e){
	totallimit = 0;
	$(".truck_value").each(function(){
		totallimit = totallimit + parseInt(return_numeric($(this).val()));
	});
	
	$(".total_truck_limit").val(totallimit);
	var total_truck_limit = return_numeric($(".total_truck_limit").val());
	var total_truck_pre = return_numeric($("#total_truck_premium_pd").val());
	total_pd_pre = parseInt(total_truck_limit)+parseInt(total_truck_pre);
	$("#total_pd_pr").val(total_pd_pre);
	//apply_on_all_elements();
	calculateTTTRowPremium();
});
/**By Ashvin Patel 12/may/2014**/
$('body').on('blur','.vehicle_rate',function(e){		
	var rate = $(this).val();
	var frate = $(this).parent().parent().find(".vehicle_frate").val();
	ded = return_numeric($(this).parent().parent().find(".vehicle_value").val());
	var totded = '';
	if(rate != ''){
		totded = (parseFloat(rate) * parseFloat(ded))/100;				
		if(frate!=''){
			var n = totded.toString();
			var lastChar = n.substr(-1);
			console.log(lastChar);
			if(lastChar=='0'){
				totded = parseFloat(frate) + parseFloat(totded);
				if(isInt(frate)){
					//totded = totded+'0';	
				}
				totded = parseFloat(totded);
				totded = totded.toFixed(2);
			}else{
				totded = parseFloat(frate) + parseFloat(totded);
				if(!isInt(frate)){
					//totded = totded+'0';	
				}
				totded = parseFloat(totded);
				totded = totded.toFixed(2);
			}					
		  }else{
			  totded =  parseFloat(totded);	
			  totded = totded.toFixed(2);
		  }
	}else if(frate!=''){
		if(rate != ''){
			totded = (parseFloat(rate) * parseFloat(ded))/100;	
			totded = parseFloat(frate) + parseFloat(totded);
			totded = totded.toFixed(2);
		}else{
			totded = parseFloat(frate);	totded = totded.toFixed(2);
		}
	}
	$(this).parent().parent().find(".vehicle_rp").val(totded);
	$(this).parent().parent().find(".vehicle_rp").priceFormat({
		  prefix: '$ ',
		  centsLimit: 2,
		  thousandsSeparator: ','
	  });
	tot_trailer = 0;
	tot_trac_premium = 0;
	tot_truc_premium = 0;
	$(".trailer_prem").each(function(){
		if($(this).val() != '')
		{
			tot_trailer =  tot_trailer + parseFloat(return_numeric($(this).val()));
		}
		
		var element = $("#total_trailer_premium_pd");
		applyDecimal(element, tot_trailer, 2);
	});
	$(".tractor_prem").each(function(){
		if($(this).val() != '')
		{
			tot_trac_premium =  tot_trac_premium + parseFloat(return_numeric($(this).val()));
			
		}
		var element = $("#total_tractor_premium_pd");
		applyDecimal(element, tot_trac_premium, 2);
	});
	/**By Ashvin Patel 12/may/2014**/
	$(".truck_prem1").each(function(){
		if($(this).val() != '')
		{
			tot_truc_premium =  tot_truc_premium + parseFloat(return_numeric($(this).val()));
			
		}
		var element = $("#total_truck_premium_pd");
		applyDecimal(element, tot_truc_premium, 2);
	});
	calculatepd();	
	calculatePdAllPremium();
});
});