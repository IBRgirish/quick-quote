var myApp = angular.module('qq',[], function($interpolateProvider){
	/*$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');	*/
});
function insuredCtrl($scope){
	$scope.in_this_is = 'Corporation';
	$scope.in_garag_in = 'Yes';	
	$scope.filings_need = 'Yes';
	$scope.rows = [];
	$scope.rows['in_garags'] = [];
	$scope.rows['filing'] = [];
	$scope.rows['filing_nos'] = [];
	$scope.rows['owners'] = [];
	
	$scope.rows['in_garags'].push(1);
	$scope.rows['filing_nos'].push({sequence:''});
	$scope.n_owners = 0;
	$scope.add_row = function(type){		
		$scope.rows[type].push({sequence:''});
		if(type == 'owners'){
			$scope.n_owners = $scope.rows[type].length;
		}
	}
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rows[type].splice(index, 1);
		}
		if(type == 'owners'){
			$scope.n_owners = $scope.rows[type].length;
		}
    }
	
	$scope.add_row_ = function(type){
		var score = $scope.n_owners;
		var length = $scope.rows[type].length;		
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows[type].splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		   $scope.rows[type].push({sequence:''});	
		 i++;
		} 	
	}
	
}
/*
* Angular Chosen directive
* 
* Ashvin Patel 30/Mar/2015
*/
myApp.directive('chosen', function ($http) {
	var linker = function (scope, element, attr) {
		scope.$watch(scope.states, function (oldVal, newVal) {
			setTimeout(function(){
				element.trigger('liszt:updated');
			}, 500);
		});
		scope.$watch(attr.ngModel, function () {
			setTimeout(function(){
				element.trigger('liszt:updated');
			}, 500);
		});
		setTimeout(function(){
			element.chosen({disable_search_threshold: 20});
		}, 500);
	};
	
	return {
		restrict: 'A',
		link: linker
	}
});
function coverageCtrl($scope, $window){
	$scope.rows = [];
	$scope.rows['trucks'] = [];
	$scope.rows['tractors'] = [];
	$scope.rows['trailers'] = [];
	$scope.rows['others'] = [];
	$scope.n_trucks = 0;
	$scope.n_tractors = 0;
	$scope.n_trailers = 0;
	$scope.n_others = 0;
	
	$scope.add_row = function(type){		
		$scope.rows[type].push({sequence:''});
		if(type == 'trucks'){
			$scope.n_trucks = $scope.rows[type].length;
		}
	}
	
	$scope.add_email = function(parent_type, parent_index, index){	
		$scope.rows[parent_type][parent_index]['lessor'][index]['emails'].push({sequence:''})
		
	}
	$scope.remove_email = function(parent_type, parent_parent_index, parent_index, index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  $scope.rows[parent_type][parent_parent_index]['lessor'][parent_index]['emails'].splice(index, 1);
		}
	}
	
	$scope.add_fax = function(parent_type, parent_index, index){	
		$scope.rows[parent_type][parent_index]['lessor'][index]['faxs'].push({sequence:''})
		
	}
	$scope.remove_fax = function(parent_type, parent_parent_index, parent_index, index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  $scope.rows[parent_type][parent_parent_index]['lessor'][parent_index]['faxs'].splice(index, 1);
		}
	}
	
	$scope.add_eqp = function(parent_type, parent_index, index){	
		$scope.rows[parent_type][parent_index]['vh_eqpmnts'].push({sequence:''})
		
	}
	$scope.remove_eqp = function(parent_type, parent_index, index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  $scope.rows[parent_type][parent_index]['vh_eqpmnts'].splice(index, 1);
		}
	}
	
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rows[type].splice(index, 1);
		}
		if(type == 'trucks'){
			$scope.n_trucks = $scope.rows[type].length;
		}
    }
	
	$scope.add_row_ = function(score, type){
		//var score = $scope.n_trucks;
		var length = $scope.rows[type].length;	
		if(type == 'trucks'){	
			var score = $scope.n_trucks;
		}else if(type == 'tractors'){	
			var score = $scope.n_tractors;
		}else if(type == 'trailers'){	
			var score = $scope.n_trailers;
		}else if(type == 'others'){	
			var score = $scope.n_others;
		}
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows[type].splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		   $scope.rows[type].push({sequence:'',truck_trailer_owned_vehicle:'Yes', lessor:[],lessor_n:0,vh_equipment_pull:'No',vh_eqpmnts:[], vh_lib_coverage:$window.liability, vh_pd_coverage:$window.pd, vh_cargo_coverage:$window.cargo});
		   var parent_index = $scope.rows[type].length;	
		   $scope.rows[type][parent_index-1]['vh_eqpmnts'].push({sequence:''});
		 i++;
		} 	
	}
	$scope.add_loss_row = function(parent_type, index){	
		var score =  $scope.rows[parent_type][index]['lessor_n'];
		var length = $scope.rows[parent_type][index]['lessor'].length;		
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows[parent_type][index]['lessor'].splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		   $scope.rows[parent_type][index]['lessor'].push({sequence:'',emails:[],faxs:[]});	
		   var email_index = $scope.rows[parent_type][index]['lessor'].length;		   
		   $scope.rows[parent_type][index]['lessor'][email_index - 1]['emails'].push({sequence:''})
		   $scope.rows[parent_type][index]['lessor'][email_index - 1]['faxs'].push({sequence:''})
		 i++;
		} 	
	}
	
	$scope.isOptionSelected = function(option_values, option_id) {		
		if(option_values){				
			if(option_values.indexOf(option_id) >= 0){				
				return true;	
			}else{
				return  false;	
			}
		}
		return false;		
  };
}
function driverCtrl($scope){

	$scope.rows = [];
	$scope.n_drivers = 0;
	$scope.add_row = function(){		
		$scope.rows.push({sequence:''});		
		$scope.n_drivers = $scope.rows.length;
	}
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rows.splice(index, 1);
		}
		$scope.n_drivers = $scope.rows.length;
    }
	
	$scope.add_row_ = function(){
		var score = $scope.n_drivers;
		var length = $scope.rows.length;		
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows.splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		   $scope.rows.push({sequence:''});	
		 i++;
		} 	
	}
}