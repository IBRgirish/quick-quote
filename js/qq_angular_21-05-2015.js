var myApp = angular.module('qq',['angular-price-format'],function($interpolateProvider){
	/*$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');	*/
});

	  myApp.directive('priceFormat', function () {
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function (scope, element, attrs, ngModelCtrl) {
					
					element.priceFormat({
		                    prefix: '$ ',
		                    centsLimit: 0,
		                    thousandsSeparator: ','
		            });
				}
			}
		});
		
  myApp.directive('jqdatepicker', function () {
			return {
				restrict: 'A',
				require: 'ngModel',
				link: function (scope, element, attrs, ngModelCtrl) {
					
					element.datepicker({
						dateFormat: 'mm/dd/yyyy',
						autoclose: true,
						onSelect: function() {                   
						}
					}).on('changeDate', function(e){ 
						$('.datepicker').hide();
						_endDate = new Date(e.date.getTime()); //get new end date						
						$('#'+attrs.end).datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
					})
					 
				}
			};
		});


 		
function insuredCtrl($scope, $window){
	$scope.insured_info = JSON.parse($window.insured_info);
	if($scope.insured_info==''){
		 $scope.insured_info = null;
		}
		
	if($scope.insured_info!=null){
	  
	  $scope.insured_info =$scope.insured_info[0];
	}else{
		
	  $scope.insured_info = {in_this_is:'Corporation',in_garag_in:'Yes',in_garag_fax:'No',filings_need:'Yes',in_country:'US',business_years:'New venture',in_state:'CA'};
	 
	}
	$scope.rows = [];
	$scope.rows['in_garags'] = [];
	$scope.rows['filing'] = [];
	$scope.rows['filing_nos'] = [];
	$scope.rows['owners'] = [];
	
	$scope.in_garag = JSON.parse($window.ins_info);
	
	if($scope.in_garag==''){
		 $scope.in_garag = null;
		}
	if($scope.in_garag!=null){	
		 
	     $scope.rows['in_garags'].push($scope.in_garag[0]);	  
	   }else{
	     $scope.rows['in_garags'].push({sequence:'',phones:[],emails:[],faxs:[],in_garag_fax:'No',in_ga_state:'CA',in_ga_country:'US'});
	   }
	$scope.rows['filing_nos'].push({sequence:''});
	var indexs = $scope.rows['in_garags'].length;	
	$scope.ins_phone = JSON.parse($window.ins_phone);
	if($scope.ins_phone==''){
		 $scope.ins_phone = null;
		}		
	if($scope.ins_phone!=null){
	   angular.forEach($scope.ins_phone, function(value, index) {	
	    $scope.rows['in_garags'][indexs-1]['phones'].push(value);	
	});
	}else{		
	    $scope.rows['in_garags'][indexs-1]['phones'].push({sequence:'',pri_country:1});
	}
	$scope.ins_email = JSON.parse($window.ins_email);
	if($scope.ins_email==''){
		 $scope.ins_email = null;
		}
	if($scope.ins_email!=null){
	   angular.forEach($scope.ins_email, function(value, index) {	
	    $scope.rows['in_garags'][indexs-1]['emails'].push(value);	
	});
	}else{		
	    $scope.rows['in_garags'][indexs-1]['emails'].push({sequence:'',priority_email:1});
	}
	
	$scope.ins_fax = JSON.parse($window.ins_fax);
	if($scope.ins_fax==''){
		 $scope.ins_fax = null;
		}
	if($scope.ins_fax!=null){
	   angular.forEach($scope.ins_fax, function(value, index) {	
	    $scope.rows['in_garags'][indexs-1]['faxs'].push(value);	
	});
	}else{		
	    $scope.rows['in_garags'][indexs-1]['faxs'].push({sequence:'',country_fax:1});
	}
	
	$scope.filing = JSON.parse($window.filing);
	if($scope.filing==''){
		 $scope.filing = null;
		}
	if($scope.filing!=null){
		angular.forEach($scope.filing, function(value, index) {	
	    $scope.rows['filing'].push(value);
	});
	
	}
	
	$scope.filings = JSON.parse($window.filings);
	if($scope.filings==''){
		 $scope.filings = null;
		}
	if($scope.filings!=null){
		angular.forEach($scope.filings, function(value, index) {	
	    $scope.rows['filing_nos'].push(value);
	});
	
	}
	
	$scope.n_owners = 0;
	$scope.add_row = function(type){	
		
		if(type == 'owners'){
		$scope.n_owners = $scope.rows[type].length;
		}else if(type == 'in_garags'){
		$scope.rows['in_garags'].push({sequence:'',phones:[],emails:[],faxs:[]});
		var indexs = $scope.rows['in_garags'].length;	
	    $scope.rows['in_garags'][indexs-1]['phones'].push({sequence:'',pri_country:1});
	    $scope.rows['in_garags'][indexs-1]['emails'].push({sequence:'',priority_email:1});
	    $scope.rows['in_garags'][indexs-1]['faxs'].push({sequence:'',country_fax:1});
		}else{
		 $scope.rows[type].push({sequence:''});	
		}
	}
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rows[type].splice(index, 1);
		}
		if(type == 'owners'){
			$scope.n_owners = $scope.rows[type].length;
		}
    }
	
	$scope.add_row_ = function(type){
		var score = $scope.n_owners;
		var length = $scope.rows[type].length;		
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows[type].splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		   $scope.rows[type].push({sequence:''});	
		 i++;
		} 	
	}
	
	$scope.insured_emails = [];
	$scope.insured_phones = [];
	$scope.insured_faxs = [];
	
	$scope.insured_phone=JSON.parse($window.insured_phone);
	if($scope.insured_phone==''){
		 $scope.insured_phone = null;
		}
	if($scope.insured_phone!=null){
		
	    angular.forEach($scope.insured_phone, function(value, index) {	
	    $scope.insured_phones.push(value);	
	});
	}else{
		
	    $scope.insured_phones.push({sequence:'',pri_country:1});
	}	
	
	$scope.insured_email=JSON.parse($window.insured_email);
	if($scope.insured_email==''){
		 $scope.insured_email = null;
		}
	if($scope.insured_email!=null){
		$scope.insured_email=JSON.parse($window.insured_email);
	    angular.forEach($scope.insured_email, function(value, index) {	
	    $scope.insured_emails.push(value);	
	});
	}else{
	    $scope.insured_emails.push({sequence:'',priority_email:1});
	}
	
	$scope.insured_fax=JSON.parse($window.insured_fax);
	if($scope.insured_fax==''){
		 $scope.insured_fax = null;
		}
	if($scope.insured_fax != null){	
		
	    angular.forEach($scope.insured_fax, function(value, index) {	
	    $scope.insured_faxs.push(value);	
	});
	}else{
	    $scope.insured_faxs.push({sequence:'',country_fax:1});
	}
	
	
	$scope.addRowPhone = function(type,index){
	if(type=="in_garags"){		
	    var len_in_garags=$scope.rows[type][index]['phones'].length+1;			   
		$scope.rows[type][index]['phones'].push({sequence:'',pri_country:len_in_garags});
		
	}else{
		var length=$scope.insured_phones.length+1;
		$scope.insured_phones.push({sequence:'',pri_country:length});	
		}
	}
	$scope.remove_row_phone = function(index,type,parent_index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
			if(type=="in_garags")	{	
		    $scope.rows[type][parent_index]['phones'].splice(index, 1);
	        }else{
		    $scope.insured_phones.splice(index, 1);
			}
		}		
    }
	$scope.addRowEmail = function(type,index){	
	if(type=="in_garags")	{
		 var len_in_garags=$scope.rows[type][index]['emails'].length+1;	
		$scope.rows[type][index]['emails'].push({sequence:'',priority_email:len_in_garags});
	 }else{
		var length=$scope.insured_emails.length+1;
		$scope.insured_emails.push({sequence:'',priority_email:length});	
		}
	}
	$scope.remove_row_email = function(index,type,parent_index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
			if(type=="in_garags")	{	
		    $scope.rows[type][parent_index]['emails'].splice(index, 1);
	        }else{
		    $scope.insured_emails.splice(index, 1);
	      }
		}		
    }
	$scope.addRowFax = function(type,index){	
	if(type=="in_garags")	{	
	    var len_in_garags=$scope.rows[type][index]['faxs'].length+1;
		$scope.rows[type][index]['faxs'].push({sequence:'',country_fax:len_in_garags});
	 }else{
		 var length=$scope.insured_faxs.length+1;
		$scope.insured_faxs.push({sequence:'',country_fax:length});	
		}		
	}
	$scope.remove_row_fax = function(index,type,parent_index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
			if(type=="in_garags")	{	
		    $scope.rows[type][parent_index]['faxs'].splice(index, 1);
	        }else{
		    $scope.insured_faxs.splice(index, 1);
			}
		}		
    }
	$scope.operation_info=function(item){
		   
		   if(item=='Yes'){
			   var nature_of_business = $('#nature_of_business').val();			  
			   $('.nature_bus').val(nature_of_business);
			   }
		
		}
}
/*
* Angular Chosen directive
* 
* Ashvin Patel 30/Mar/2015
*/
myApp.directive('chosen', function ($http) {
	var linker = function (scope, element, attr) {
		scope.$watch(scope.states, function (oldVal, newVal) {
			setTimeout(function(){
				element.trigger('liszt:updated');
			}, 500);
		});
		scope.$watch(attr.ngModel, function () {
			setTimeout(function(){
				element.trigger('liszt:updated');
			}, 500);
		});
		setTimeout(function(){
			element.chosen({disable_search_threshold: 20});
		}, 500);
	};
	
	return {
		restrict: 'A',
		link: linker
	}
});




function coverageCtrl($scope, $window){
	$scope.coverages=JSON.parse($window.coverage);
	$scope.rows = [];
	$scope.rows['trucks'] = [];
	$scope.rows['tractors'] = [];
	$scope.rows['trailers'] = [];
	$scope.rows['others'] = [];
	
	if($scope.coverages==''){
		$scope.coverages=null;
		}
	if($scope.coverages!=null){
		$scope.coverages=$scope.coverages[0];		
	    $scope.coverages.coverage = $scope.coverages.coverage.split(",");
		$scope.coverages.vh_type = $scope.coverages.vh_type.split(",");	
		
		  if($.inArray('Truck',$scope.coverages.vh_type)>-1){
		    $scope.truck_vh=JSON.parse($window.truck_vh);
		
		   if($scope.truck_vh==''){
		        $scope.truck_vh=null;
		      }
	       if($scope.truck_vh!=null){			   
			    angular.forEach($scope.truck_vh, function(value, index) {	
			
	              $scope.rows['trucks'].push(value);
	             });	
		     
			  
		   $scope.truck_lessor=JSON.parse($window.truck_lessor);
		   if($scope.truck_lessor==''){
		        $scope.truck_lessor=null;
		      }
			var parent_index = $scope.rows['trucks'].length;
			
	       if($scope.truck_lessor!=null){
			  $scope.truck_lessor=$scope.truck_lessor[0];			  			
		      $scope.rows['trucks'][parent_index-1]['lessor'].push($scope.truck_lessor);		 
			  }
		    var email_index = $scope.rows['trucks'][parent_index-1]['lessor'].length;
	       $scope.truck_email=JSON.parse($window.truck_email);
		   if($scope.truck_email==''){
		        $scope.truck_email=null;
		      }
	       if($scope.truck_email!=null){ 
		   		   
		   $scope.rows['trucks'][parent_index-1]['lessor'][email_index - 1]['emails'].push($scope.truck_email)
		   }
		   $scope.truck_fax=JSON.parse($window.truck_fax);
		   if($scope.truck_fax==''){
		        $scope.truck_fax=null;
		   }
		   if($scope.truck_fax!=null){ 
		   $scope.rows['trucks'][parent_index-1]['lessor'][email_index - 1]['faxs'].push($scope.truck_fax)
		   }
		   $scope.truck_vh_eqp=JSON.parse($window.truck_vh_eqp);
		  
		   if($scope.truck_vh_eqp==''){
		        $scope.truck_vh_eqp=null;
		      }
			  
	       if($scope.truck_vh_eqp!=null){
			 
			  var parent_index = $scope.rows['trucks'].length;	
			  angular.forEach($scope.truck_vh_eqp, function(value, index) {	
			
	               $scope.rows['trucks'][parent_index-1]['vh_eqpmnts'].push(value);
	          });		  
			 
		     }else{
				 
		      }
		    }
		   }
		 if($.inArray('Tractor',$scope.coverages.vh_type)>-1){
		    $scope.tractor_vh=JSON.parse($window.tractor_vh);
		
		   if($scope.tractor_vh==''){
		        $scope.tractor_vh=null;
		      }
	       if($scope.tractor_vh!=null){
			   
			    angular.forEach($scope.tractor_vh, function(value, index) {	
			
	              $scope.rows['tractors'].push(value);
	             });	
		     
			  
		   $scope.tractor_lessor=JSON.parse($window.tractor_lessor);
		   if($scope.tractor_lessor==''){
		        $scope.tractor_lessor=null;
		      }
			var parent_index = $scope.rows['trucks'].length;
			
	       if($scope.tractor_lessor!=null){
			  $scope.tractor_lessor=$scope.tractor_lessor[0];
			  			
		      $scope.rows['tractors'][parent_index-1]['lessor'].push($scope.tractor_lessor);		 
			  }
		    var email_index = $scope.rows['tractors'][parent_index-1]['lessor'].length;
	       $scope.tractor_email=JSON.parse($window.tractor_email);
		   if($scope.tractor_email==''){
		        $scope.tractor_email=null;
		      }
	       if($scope.tractor_email!=null){ 
		   		   
		   $scope.rows['tractors'][parent_index-1]['lessor'][email_index - 1]['emails'].push($scope.tractor_email)
		   }
		   $scope.tractor_fax=JSON.parse($window.tractor_fax);
		   if($scope.tractor_fax==''){
		        $scope.tractor_fax=null;
		   }
		   if($scope.tractor_fax!=null){ 
		   $scope.rows['tractors'][parent_index-1]['lessor'][email_index - 1]['faxs'].push($scope.tractor_fax)
		   }
		   $scope.tractor_vh_eqp=JSON.parse($window.tractor_vh_eqp);
		  
		   if($scope.tractor_vh_eqp==''){
		        $scope.tractor_vh_eqp=null;
		      }
			  
	       if($scope.tractor_vh_eqp!=null){
			 
			  var parent_index = $scope.rows['tractors'].length;	
			  angular.forEach($scope.tractor_vh_eqp, function(value, index) {	
			
	               $scope.rows['tractors'][parent_index-1]['vh_eqpmnts'].push(value);
	          });		  
			 
		     }else{
				 
		      }
		    }
		  }
		  if($.inArray('Trailer',$scope.coverages.vh_type)>-1){
		    $scope.trailer_vh=JSON.parse($window.trailer_vh);
		
		   if($scope.trailer_vh==''){
		        $scope.trailer_vh=null;
		      }
	       if($scope.trailer_vh!=null){
			   
			    angular.forEach($scope.trailer_vh, function(value, index) {	
			
	              $scope.rows['trailers'].push(value);
	             });	
		     
			  
		   $scope.trailer_lessor=JSON.parse($window.trailer_lessor);
		   if($scope.trailer_lessor==''){
		        $scope.trailer_lessor=null;
		      }
			var parent_index = $scope.rows['trailers'].length;
			
	       if($scope.trailer_lessor!=null){
			  $scope.trailer_lessor=$scope.trailer_lessor[0];
			  			
		      $scope.rows['trailers'][parent_index-1]['lessor'].push($scope.trailer_lessor);		 
			  }
		    var email_index = $scope.rows['trailers'][parent_index-1]['lessor'].length;
	       $scope.trailer_email=JSON.parse($window.trailer_email);
		   if($scope.trailer_email==''){
		        $scope.trailer_email=null;
		      }
	       if($scope.trailer_email!=null){ 
		   		   
		   $scope.rows['trailers'][parent_index-1]['lessor'][email_index - 1]['emails'].push($scope.trailer_email)
		   }
		   $scope.trailer_fax=JSON.parse($window.trailer_fax);
		   if($scope.trailer_fax==''){
		        $scope.trailer_fax=null;
		   }
		   if($scope.trailer_fax!=null){ 
		   $scope.rows['trailers'][parent_index-1]['lessor'][email_index - 1]['faxs'].push($scope.trailer_fax)
		   }
		   $scope.trailer_vh_eqp=JSON.parse($window.trailer_vh_eqp);
		  
		   if($scope.trailer_vh_eqp==''){
		        $scope.trailer_vh_eqp=null;
		      }
			  
	       if($scope.trailer_vh_eqp!=null){
			 
			  var parent_index = $scope.rows['trailers'].length;	
			  angular.forEach($scope.trailer_vh_eqp, function(value, index) {	
			
	               $scope.rows['trailers'][parent_index-1]['vh_eqpmnts'].push(value);
	          });		  
			 
		     }else{
				 
		      }
		    }
		  }
		   if($.inArray('Other',$scope.coverages.vh_type)>-1){
		    $scope.other_vh=JSON.parse($window.other_vh);
		
		   if($scope.other_vh==''){
		        $scope.other_vh=null;
		      }
	       if($scope.other_vh!=null){
			   
			    angular.forEach($scope.other_vh, function(value, index) {	
			
	              $scope.rows['others'].push(value);
	             });	
		     
			  
		   $scope.other_lessor=JSON.parse($window.other_lessor);
		   if($scope.other_lessor==''){
		        $scope.other_lessor=null;
		      }
			var parent_index = $scope.rows['others'].length;
			
	       if($scope.other_lessor!=null){
			  $scope.other_lessor=$scope.other_lessor[0];
			  			
		      $scope.rows['others'][parent_index-1]['lessor'].push($scope.other_lessor);		 
			  }
		    var email_index = $scope.rows['others'][parent_index-1]['lessor'].length;
	       $scope.other_email=JSON.parse($window.other_email);
		   if($scope.other_email==''){
		        $scope.other_email=null;
		      }
	       if($scope.other_email!=null){ 
		   		   
		   $scope.rows['others'][parent_index-1]['lessor'][email_index - 1]['emails'].push($scope.other_email)
		   }
		   $scope.other_fax=JSON.parse($window.other_fax);
		   if($scope.other_fax==''){
		        $scope.other_fax=null;
		   }
		   if($scope.other_fax!=null){ 
		   $scope.rows['others'][parent_index-1]['lessor'][email_index - 1]['faxs'].push($scope.other_fax)
		   }
		   $scope.other_vh_eqp=JSON.parse($window.other_vh_eqp);
		  
		   if($scope.other_vh_eqp==''){
		        $scope.other_vh_eqp=null;
		      }
			  
	       if($scope.other_vh_eqp!=null){
			 
			  var parent_index = $scope.rows['others'].length;	
			  angular.forEach($scope.other_vh_eqp, function(value, index) {	
			
	               $scope.rows['others'][parent_index-1]['vh_eqpmnts'].push(value);
	          });		  
			 
		     }else{
				 
		      }
		    }
		  }
		}else{	
		
		   $scope.coverages = {'vh_type':[],'n_trucks':0,'n_tractors':0,'n_trailers':0,'n_others':0};
		   
		
	    }
	
	

	$scope.add_row = function(type){		
		$scope.rows[type].push({sequence:'',pri_country:1});
		if(type == 'trucks'){
			$scope.n_trucks = $scope.rows[type].length;
		}
	}
	
	$scope.add_email = function(parent_type, parent_index, index){	
	var length=	$scope.rows[parent_type][parent_index]['lessor'][index]['emails'].length+1;
		$scope.rows[parent_type][parent_index]['lessor'][index]['emails'].push({sequence:'',priority_email:length})
		
	}
	$scope.remove_email = function(parent_type, parent_parent_index, parent_index, index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  $scope.rows[parent_type][parent_parent_index]['lessor'][parent_index]['emails'].splice(index, 1);
		}
	}
	
	$scope.add_fax = function(parent_type, parent_index, index){
		var length=	$scope.rows[parent_type][parent_index]['lessor'][index]['faxs'].length+1;
		$scope.rows[parent_type][parent_index]['lessor'][index]['faxs'].push({sequence:'',country_fax:length})
		
	}
	$scope.remove_fax = function(parent_type, parent_parent_index, parent_index, index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  $scope.rows[parent_type][parent_parent_index]['lessor'][parent_index]['faxs'].splice(index, 1);
		}
	}
	
	$scope.add_eqp = function(parent_type, parent_index, index){	
		$scope.rows[parent_type][parent_index]['vh_eqpmnts'].push({sequence:'',vh_lib_coverage:'No', vh_pd_coverage:'No', vh_cargo_coverage:'No',refrigerated_breakdown:'false'});
		
	}
	$scope.remove_eqp = function(parent_type, parent_index, index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  $scope.rows[parent_type][parent_index]['vh_eqpmnts'].splice(index, 1);
		}
	}
	
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rows[type].splice(index, 1);
		}
		if(type == 'trucks'){
			$scope.n_trucks = $scope.rows[type].length;
		}
    }
	
	$scope.add_row_ = function(score, type){
		
		var length = $scope.rows[type].length;	
		if(type == 'trucks'){			
			var score = $scope.coverages.truck_no;
		}else if(type == 'tractors'){	
			var score = $scope.coverages.tractor_no;
		}else if(type == 'trailers'){	
			var score = $scope.coverages.trailer_no;
		}else if(type == 'others'){	
			var score = $scope.coverages.other_no;
		}
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows[type].splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
			if($('#operation').val()=='Yes'){
		     var nature_bus=$('#nature_of_business').val();
			}else{
			 var nature_bus='';
			}
		   $scope.rows[type].push({sequence:'',truck_trailer_owned_vehicle:'Yes', lessor:[],lessor_n:0,vh_equipment_pull:'No',vh_eqpmnts:[], vh_lib_coverage:$window.liability, vh_pd_coverage:$window.pd, vh_cargo_coverage:$window.cargo,vh_nature_of_business:nature_bus});		   
		   var parent_index = $scope.rows[type].length;	
		   $scope.rows[type][parent_index-1]['lessor'].push({sequence:'',emails:[],faxs:[]});	
		   var email_index = $scope.rows[type][parent_index-1]['lessor'].length;		   
		   $scope.rows[type][parent_index-1]['lessor'][email_index - 1]['emails'].push({sequence:''})
		   $scope.rows[type][parent_index-1]['lessor'][email_index - 1]['faxs'].push({sequence:''})
		   $scope.rows[type][parent_index-1]['vh_eqpmnts'].push({sequence:'',vh_lib_coverage:'No', vh_pd_coverage:'No', vh_cargo_coverage:'No'});
		 i++;
	     
		} 	

	  
	}

	$scope.in_truck_fax='No';
	$scope.in_tractor_fax='No';
	$scope.in_trailer_fax='No';
	$scope.in_other_fax='No';
	
	$scope.add_loss_row = function(type,index){		
		
		 $scope.rows[type][index]['lessor'].push({sequence:'',emails:[],faxs:[],pri_country:1});	
		 var email_index = $scope.rows[type][index]['lessor'].length;		   
		   $scope.rows[type][index]['lessor'][email_index - 1]['emails'].push({sequence:'',priority_email:1})
		   $scope.rows[type][index]['lessor'][email_index - 1]['faxs'].push({sequence:'',country_fax:1})
	}
	
	$scope.remove_type= function(type,parent_index,index){		
		var retVal = confirm("Are you sure you want to delete this row");
		if(retVal == true) {
		  $scope.rows[type][parent_index]['lessor'].splice(index, 1);
		}
	
	}
	
	
	
	
	$scope.isOptionSelected = function(option_values, option_id) {		
		if(option_values){				
			if(option_values.indexOf(option_id) >= 0){				
				return true;	
			}else{
				return  false;	
			}
		}
		return false;		
  };     
}
function driverCtrl($scope,$window){

	$scope.rows = [];
	$scope.drivers=JSON.parse($window.drivers);
	
	if($scope.drivers == ""){
	 $scope.drivers=null;
	 
	}
	if($scope.drivers!=null){
	  $scope.drivers=$scope.drivers[0];
	  
	  $scope.n_drivers = ($scope.drivers.driver_count)?$scope.drivers.driver_count:0;
	  $scope.driver_ids = ($scope.drivers.drivers_id)?$scope.drivers.drivers_id:'';
	  $scope.driv_mvr_file = ($scope.drivers.driv_mvr_file)?$scope.drivers.driv_mvr_file:'';
		
		}else{
	   $scope.n_drivers = 0;	
	   }
	$scope.driver=JSON.parse($window.driver);
	
	if($scope.driver=='undefined'){
		$scope.driver=null;
		}
	
    if($scope.driver!=null){
		
	$scope.driver=$scope.driver[0];
	$scope.rows.push($scope.driver);
	
	}
	$scope.add_row = function(){		
		$scope.rows.push({sequence:''});		
		$scope.n_drivers = $scope.rows.length;
	}
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rows.splice(index, 1);
		}
		$scope.n_drivers = $scope.rows.length;
    }
	
	$scope.add_row_ = function(){
		var score = $scope.n_drivers;
		var length = $scope.rows.length;
		
		if(score > 0 )
		{   $scope.Schedule_Drivers = true; 
		}
		else{$scope.Schedule_Drivers = false;	
		}
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows.splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		   $scope.rows.push({sequence:''});	
		 i++;
		} 	
	}
}

function ownerCtrl($scope,$window){
	$scope.rows = [];
	$scope.owners=JSON.parse($window.owners);
	if($scope.owners== ""){
	 $scope.owners=null;
	 
	}
	if($scope.owners!=null){
	  $scope.owners=$scope.owners[0];
	  $scope.n_owners = ($scope.owners.number_of_owner)?$scope.owners.number_of_owner:0;
	  $scope.owner_ids = ($scope.owners.owners_id)?$scope.owners.owners_id:'';
	  $scope.own_mvr_file = ($scope.owners.own_mvr_file)?$scope.owners.own_mvr_file:'';		
		}else{
	   $scope.n_owners = 0;	
	   }

	$scope.owner=JSON.parse($window.owner);
	
    if($scope.owner==null){
	   $scope.owner='undefined';
	}
	if($scope.owner!='undefined'){
	$scope.owner=$scope.owner[0];
	$scope.rows.push($scope.owner);
	}
	$scope.add_row = function(){		
		$scope.rows.push({sequence:''});		
		$scope.n_owners = $scope.rows.length;
	}
	$scope.remove_row = function(index, type) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.rows.splice(index, 1);
		}
		$scope.n_drivers = $scope.rows.length;
    }
	
	$scope.add_row_ = function(){
		var score = $scope.n_owners;
		var length = $scope.rows.length;	
		
		if(score > 0 ){ $scope.Owner_Infomation = true ; }	else {  $scope.Owner_Infomation = false ;}
		if(score > length){					
			var i =length + 1;			  
		} else {
			while(score < length){
				$scope.rows.splice($scope.toRemove, 1);
				score++;
			}
		}
		while(i <= score){
		   $scope.rows.push({sequence:''});	
		 i++;
		} 	
	}
	
	
	}

myApp.service('myservice', function() {
	  this.broker_infos='';
	  this.broker_phones=[];
	  this.broker_emails=[];
	  this.broker_faxs=[];
    });
brokerCtrl.$inject = ['$scope', 'myservice','$window'];
function brokerCtrl($scope, myservice, $window){      	
    $scope.myservice=myservice;
	myservice.broker_emails = [];
		
	if($window.broker_email=='undefined'){
	 $window.broker_email=null;
	}
	if($window.broker_email != null){
	  $scope.broker_email =JSON.parse($window.broker_email);
	  angular.forEach($scope.broker_email, function(value, index) {	
	   myservice.broker_emails.push(value);	
	});
    }else{
	  myservice.broker_emails.push({sequence:'',priority_email:1});	
	}
		
	
	if($window.broker_info=='undefined'){
	 $window.broker_info=null;
	}
	
	if($window.broker_info != null){	
	   $scope.broker_info = JSON.parse($window.broker_info);
	    myservice.broker_infos=$scope.broker_info[0];		
		
	}else{		
	   	myservice.broker_infos = {country:'US',state:'CA',fax:'No'};
	}

	
	$scope.addRowEmail = function(){
		var length=	myservice.broker_emails.length+1;		
	   myservice.broker_emails.push({sequence:'',priority_email:length});
    
      };
	
	$scope.remove_row_email = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  myservice.broker_emails.splice(index, 1);
		}		
    }
	
	
	myservice.broker_phones = [];
	if($window.broker_phone=='undefined'){
	 $window.broker_phone=null;
	}	
	

	if($window.broker_phone != null){
		$scope.broker_phone =JSON.parse($window.broker_phone);
	   angular.forEach($scope.broker_phone, function(value, index) {	
        myservice.broker_phones.push(value);	
	});
    }else{
	    myservice.broker_phones.push({sequence:'',pri_country:1});	
	}
	
	
	$scope.addRowPhone = function(){
	 var length = myservice.broker_phones.length+1;		
	  myservice.broker_phones.push({sequence:'',pri_country:length});

    };
	
	$scope.remove_row_phone = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  myservice.broker_phones.splice(index, 1);
		}		
    }
	
  	myservice.broker_faxs = [];
	
	if($window.broker_fax=='undefined'){
	   $window.broker_fax=null;
	  }	
	if($window.broker_fax==''){
		$window.broker_fax=null;
		}
	
	if($window.broker_fax != null ){ 
	  $scope.broker_fax =JSON.parse($window.broker_fax);	     
	  angular.forEach($scope.broker_fax, function(value, index) {	
	    myservice.broker_faxs.push(value);	
	});
    }else{		
	    myservice.broker_faxs.push({sequence:'',country_fax:1});	
	}
	
	$scope.addRowFax = function(){	
	var length=	myservice.broker_faxs.length+1;
	   myservice.broker_faxs.push({sequence:'',country_fax:length});
      };
	
	$scope.remove_row_fax = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  myservice.broker_faxs.splice(index, 1);
		}		
    }

}
personCtrl.$inject = ['$scope', 'myservice','$window'];  
function personCtrl($scope,myservice, $window){
	
	$scope.myservice=myservice;
	$scope.handleClick=function(){		
				
			$scope.person_infos={fistname:myservice.broker_infos.fistname,middlename:myservice.broker_infos.middlename,lastname:myservice.broker_infos.lastname,street:myservice.broker_infos.street,
			                     city:myservice.broker_infos.city,state:myservice.broker_infos.state,county:myservice.broker_infos.county,zip:myservice.broker_infos.zip,country:myservice.broker_infos.country,
							     special_remark:myservice.broker_infos.special_remark,fax:myservice.broker_infos.fax};
			$scope.person_phones=[];
			$scope.person_emails=[];
			$scope.person_faxs=[];
			  angular.forEach(myservice.broker_phones, function(value, index) {	
			  
			     if(value!=''){
					   
	                   $scope.person_phones.push(value);	
			        }
	            });	
			  angular.forEach(myservice.broker_emails, function(value, index) {	
			    if(value!=''){
				  
				   $scope.person_emails.push(value);	
			      }
			   });	
			  angular.forEach(myservice.broker_faxs, function(value, index) {	
			     if(value!=''){
				  
				   $scope.person_faxs.push(value);	
				 }
			 });	
			
	       }
		   
		   
	$scope.person_emails = [];
		if($window.person_email=='undefined'){
	      $window.person_email=null;
	     }
		
		
		if($window.person_email==''){
			$window.person_email=null;
			}
			console.log($window.person_email);
	if($window.person_email != null){	
      $scope.person_email =JSON.parse($window.person_email);
	
	  angular.forEach($scope.person_email, function(value, index) {	
	$scope.person_emails.push(value);	
	});
    }else{
	$scope.person_emails.push({sequence:'',priority_email:1});	
	}
		
		
        if($window.person_info=='undefined'){
	      $window.person_info=null;
	     }
		
		if($window.person_info==''){
		$window.person_info=null;
		}
	if($scope.person_info != null){	
	    $scope.person_info = JSON.parse($window.person_info);
	    $scope.person_infos=$scope.person_info[0];
	
	}else{
		   $scope.person_infos={country:'US',state:'CA',fax:'No'};	   
	
	 }
	
	$scope.addRowEmail = function(){
	var	length=	$scope.person_emails.length+1;			
	$scope.person_emails.push({sequence:'',priority_email:length});
    
   };
	
	$scope.remove_row_email = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.person_emails.splice(index, 1);
		}		
    }
	
	
	$scope.person_phones = [];	
	  if($window.person_phone=='undefined'){
	      $window.person_phone=null;
	     }
	
	
	if($window.person_phone != null){
		$scope.person_phone =JSON.parse($window.person_phone);
	   angular.forEach($scope.person_phone, function(value, index) {	
	     $scope.person_phones.push(value);	
	  });
    }else{
	$scope.person_phones.push({sequence:'',pri_country:1});	
	}
	
	
	$scope.addRowPhone = function(){	
	var	length=$scope.person_phones.length+1;
	$scope.person_phones.push({sequence:'',pri_country:length});

    };
	
	$scope.remove_row_phone = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.person_phones.splice(index, 1);
		}		
    }
	
	$scope.person_faxs = [];
	 if($window.person_fax=='undefined'){
	      $window.person_fax=null;
	     }
		
	if($window.person_fax==''){
		$window.person_fax=null;
		}
	
	if($window.person_fax != null ){   
	$scope.person_fax =JSON.parse($window.person_fax);   
	  angular.forEach($scope.person_fax, function(value, index) {	
	  $scope.person_faxs.push(value);	
	});
    }else{		
	  $scope.person_faxs.push({sequence:'',country_fax:1});	
	}
	
	$scope.addRowFax = function(){	
	var length=	$scope.person_faxs.length+1;
	$scope.person_faxs.push({sequence:'',country_fax:length});

   };
	
	$scope.remove_row_fax = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.person_faxs.splice(index, 1);
		}		
    }
	
	$scope.getSinglepersons = function(item) {
	             
				 var baseurl = $("#baseurls").val();
                       
                  $.ajax({
						url: baseurl+'quote/getSingleperonInfo',
						type: 'post',
						data : {person_id: item},
						async: false, //blocks window close
						success: function(json) {
						  json_data = JSON.parse(json);			
					      angular.forEach(json_data, function(value, index) {							        
								if(index=='person_info')	
								{
								 $scope.person_infos = [];	
								 $scope.person_infos = value;									 
								}
								else if(index=='person_phone')	
								{
								 $scope.person_phones = [];	
								 $scope.person_phones = value;	
								 							 
								}else if(index=='person_email')	
								{
								 $scope.person_emails = [];
								 $scope.person_emails= value;								 		
																	 
								}else if(index=='person_fax')	
								{
								 $scope.person_faxs = [];	
								 $scope.person_faxs= value;									 
								}
								$scope.person_infos.info_submitted_broker=item;
	                             
	                       });		
						}
					 });
				 
           }
	
}







function lossesCtrl($scope,$window){
	

	
	$scope.rows = [];
	$scope.pd_rows = [];
	$scope.cargo_rows = [];

	$scope.loss=JSON.parse($window.losses);
	
	if($scope.loss == null){
	$scope.loss= {'losses_need' :'No'};		
	}else{
	 $scope.loss=$scope.loss[0];

	 $scope.liability_show=true;
	 $scope.pd_show=true;
	 $scope.cargo_show=true;	
	}
   
	


$scope.add_losses = function(){
  var score = $scope.loss.liability;
  var length = $scope.rows.length; 
   
 if(score>0){
	  $('#liabilty_shows').show();
	  }else{
	 $('#liabilty_shows').hide(); 
    } 
  if(score > length){     
   var i =length + 1;     
  } else {
   while(score < length){
    $scope.rows.splice($scope.toRemove, 1);
    score++;
   }
  }
  while(i <= score){
     $scope.rows.push({sequence:''}); 
   i++;
  }  
  
  
  
  var pd_score = $scope.loss.pd;
  
  var pd_length = $scope.pd_rows.length; 
  if(pd_score>0){
	  $('#pd_show').show();
	  }else{
	 $('#pd_show').hide(); 
    } 
  if(pd_score > pd_length){     
   var pd_i =pd_length + 1;     
  } else {
   while(pd_score < pd_length){
    $scope.pd_rows.splice($scope.toRemove, 1);
    pd_score++;
   }
  }
  while(pd_i <= pd_score){
     $scope.pd_rows.push({sequence:''}); 
   pd_i++;
  }  
  
  
   var cargo_score = $scope.loss.cargo;
   var cargo_length = $scope.cargo_rows.length; 
  if(cargo_score>0){
	  $('#cargo_show').show();
	  }else{
	 $('#cargo_show').hide(); 
    } 
  if(cargo_score > cargo_length){     
   var cargo_i =cargo_length + 1;     
  } else {
   while(cargo_score < cargo_length){
    $scope.cargo_rows.splice($scope.toRemove, 1);
    cargo_score++;
   }
  }
  while(cargo_i <= cargo_score){
     $scope.cargo_rows.push({sequence:''}); 
   cargo_i++;
  }  
  
  
  
 }
 $scope.show_losses_=function(item){
	 if($scope.liability_show){
		 $scope.liability_show=false;
		 }else{
		 $scope.liability_show=true;	 
		}
 }
 $scope.show_losses_pd=function(item){
	 if($scope.pd_show){
		 $scope.pd_show=false;
		 }else{
		 $scope.pd_show=true;	 
		}
 }
 $scope.show_losses_cargo=function(item){
	 if($scope.cargo_show){
		 $scope.cargo_show=false;
		 }else{
		 $scope.cargo_show=true;	 
		}	 
	 }
 
 	$scope.losses_lia=JSON.parse($window.losses_lia);
	if($scope.losses_lia!=null){
		$scope.losses_lia=$scope.losses_lia[0];
	    $scope.rows.push($scope.losses_lia); 
	}
	$scope.losses_pd=JSON.parse($window.losses_pd);
	if($scope.losses_pd!=null){
		$scope.losses_pd=$scope.losses_pd[0];
	    $scope.pd_rows.push($scope.losses_pd); 
	}
	$scope.losses_cargo=JSON.parse($window.losses_cargo);
	if($scope.losses_cargo!=null){
		$scope.losses_cargo=$scope.losses_cargo[0];
	    $scope.cargo_rows.push($scope.losses_cargo); 
	}
	

	/* $scope.losses_need_value=function(item){
				if(item=='yes'){
					$('#losses_inform').show();
					}else{
					$('#losses_inform').hide();
					}
					}*/
					
					
			
	}
myApp.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
           /* if (!ctrl) return;
            ctrl.$formatters.unshift(function (e) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });
            ctrl.$parsers.unshift(function (viewValue) {
     elem.priceFormat({
      prefix: '$ ',
      centsSeparator: ',',
      thousandsSeparator: '.'
     });         
              return elem[0].value;
            });*/
   elem.priceFormat({
    prefix: '$ ',
    centsSeparator: '.',
    thousandsSeparator: ','
   });
        }
    };
	
	


}]);	