/*************Cargo Calculation ************
 ****************** Ashvin Patel 21/july/2014**********/
$(document).ready(function(e) {
	$('.efl,.ou').change(function(){					
	if (($(this).val()=="None") || ($(this).val()=="0") || ($(this).val()=="No")||($(this).val()==""))
	{
		$(this).parents('.row-efl').find('.mkread1').attr("disabled",true); 		
		$(this).parents('.row-efl').find('.mkread1').val(0);
		$(this).parents('.row-efl').find('.tls').val('None'); 
		$(this).parents('.row-efl').find('.drp').val('$ 0');
		$(this).parents('.row-efl').find('.tlp').val('$ 0');
		$(this).parents('.row-efl').find('.nvp').val('$ 0');
		$(this).parents('.row-efl').find('.scp').val('$ 0');		
		$(this).parents('.row-efl').find('.efp').val(0);
		$(this).parents('.row-efl').find('.ofp').val(0);
		$('.price_format_decimal').priceFormat({
			prefix: '$ ',
			centsLimit: 2,
			thousandsSeparator: ','
		}); 
	}
	else
	{
		$(this).parents('.row-efl').find('.mkread1').attr("disabled",false);
	}
	calctotalcargo();
	
});
function calctotalcargo(){
  var total_cargo_premium = 0;
  $('.row-efl').each(function(){
	  if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != '0') && ($(this).find('.efl').val() != '') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined))
	  {
		  if($(this).find('.efp').val()!=undefined){
			  premium = $(this).find('.efp').val();			  
			  total_cargo_premium = total_cargo_premium + parseFloat(return_numeric(premium)); 	
		  }
	  }
  });	
  base_unit=	$('.npu').val();
  base_rate = $("#bsr").val();
  if(base_rate == '') {base_rate = '$ 0'; }
  npu_br = 	parseFloat(base_unit) * parseFloat(return_numeric(base_rate));
  if(npu_br > 0)
  {
	  total_cargo_premium = parseFloat(total_cargo_premium) + parseFloat(npu_br);
  }
  var old_unit_premium = $(".ofp").val();
  if(old_unit_premium != '')
  {
	  total_cargo_premium = parseFloat(total_cargo_premium) + parseFloat(return_numeric(old_unit_premium));
  }
  
  element = $(".total_cargo");
  applyDecimal(element, total_cargo_premium, 2);
  var cargo_carrier_val = $('#carrier_cargo').val(); 
  updatePolicyValue('cargo', cargo_carrier_val);
}
$('.efc').change(function(){
  field = $(this).attr('name');
  base_unit=	$('.no_of_power_unit').val();
  base_rate=	return_numeric($('.base_rate').val());					
  val = $(this).val();
  total = 0;		
  if(field=='drivers_surcharge_percentage')
  {
	  total =  (val * base_rate)/100;
	  element = $(this).parent().parent().find(".efp");
	  applyDecimal(element, total, 2);
  }else if(field=='lapse_in_coverage_percentage'||field=='new_venture_percentage'||field=='losses_percentage') {
	  total =  (base_unit * base_rate)*val;
	  total = total/100;		
	  element = $(this).parent().parent().find(".efp");
	  applyDecimal(element, total, 2);
  }else {
	  total =  base_unit * val;
	  element = $(this).parent().parent().find(".efp");
	  applyDecimal(element, total, 0);
  }
  calctotalcargo();
});
$(".oup, .ou").change(function(){
	
	old_unit = $(".ou").val();
	if ((old_unit=="None") || ($(this).val()=="0") || ($(this).val()=="No")){
		$(this).parents('.row-efl').find('.efp').val(0);
		$(this).parents('.row-efl').find('.ofp').val(0);
	}else{
		val = $("#oup").val();
		total = old_unit * val;
		$(".ofp").val(total);
		$('.ofp').priceFormat({
				prefix: '$ ',
				centsLimit: 0,
				thousandsSeparator: ','
			});		
	}
	calctotalcargo();
});
$('.no_of_power_unit, .base_rate').blur(function(e) {
    calculateCargoRowPreimium();
});
function calculateCargoRowPreimium(){
	$('.row-efl').each(function(){
		if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != '0') && ($(this).find('.efl').val() != '') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined))
		{		
			var element = $(this).find('.efc');
			field = element.attr('name');
			base_unit=	$('.no_of_power_unit').val();
			base_rate=	return_numeric($('.base_rate').val());					
			val = element.val();
			total = 0;		
			if(field=='drivers_surcharge_percentage')
			{
				total =  (val * base_rate)/100;
				element = $(this).find(".efp");
				applyDecimal(element, total, 2);
			}else if(field=='lapse_in_coverage_percentage'||field=='new_venture_percentage'||field=='losses_percentage') {
				total =  (base_unit * base_rate)*val;
				total = total/100;		
				element = $(this).find(".efp");
				applyDecimal(element, total, 2);
			}else {
				total =  base_unit * val;
				element = $(this).find(".efp");
				applyDecimal(element, total, 0);
			}
		}
	});	
	calctotalcargo();	
}
});