/*************Cargo Calculation ************
 ****************** Ashvin Patel 21/july/2014**********/
$(document).ready(function(e) {
	$('#main_form6 .efl,.ou').change(function(){					
	if (($(this).val()=="None") || ($(this).val()=="0") || ($(this).val()=="No")||($(this).val()==""))
	{
		$(this).parents('.row-efl').find('.mkread1').attr("disabled",true); 		
		$(this).parents('.row-efl').find('.mkread1').val(0);
		$(this).parents('.row-efl').find('.tls').val('None'); 
		$(this).parents('.row-efl').find('.drp').val('$ 0');
		$(this).parents('.row-efl').find('.tlp').val('$ 0');
		$(this).parents('.row-efl').find('.nvp').val('$ 0');
		$(this).parents('.row-efl').find('.scp').val('$ 0');		
		$(this).parents('.row-efl').find('.efp12').val(0);
		$(this).parents('.row-efl').find('.ofp').val(0);
		$('.price_format_decimal').priceFormat({
			prefix: '$ ',
			centsLimit: 2,
			thousandsSeparator: ','
		}); 
	}
	else
	{
		$(this).parents('.row-efl').find('.mkread1').attr("disabled",false);
	}
	calctotalcargo();
	
});
function calctotalcargo(){
  var total_cargo_premium1 = 0;
  $('#main_form6 .row-efl').each(function(){
	  if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != '0') && ($(this).find('.efl').val() != '') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined))
	  {
		  if($(this).find('.efp12').val()!=undefined){
			  premium = $(this).find('.efp12').val();			  
			  total_cargo_premium1 = total_cargo_premium1 + parseFloat(return_numeric(premium)); 	
		  }
	  }
  });	
  base_unit=	$('#main_form6 .npu').val();
  base_rate = $("#main_form6 #bsr").val();
  if(base_rate == '') {base_rate = '$ 0'; }
  npu_br = 	parseFloat(base_unit) * parseFloat(return_numeric(base_rate));
  if(npu_br > 0)
  {
	  total_cargo_premium1 = parseFloat(total_cargo_premium1) + parseFloat(npu_br);
  }
  var old_unit_premium = $("#main_form6 .ofp").val();
  if(old_unit_premium != '')
  {
	  total_cargo_premium1 = parseFloat(total_cargo_premium1) + parseFloat(return_numeric(old_unit_premium));
  }
   
  element = $(".total_excess_cargo");
  applyDecimal(element, total_cargo_premium1, 2);
  var cargo_carrier_val = $('#excess_cargo').val(); 
  updatePolicyValue('excess_cargo', cargo_carrier_val);
}
$('#main_form6 .efc').change(function(){
  field = $(this).attr('name');
  base_unit=	$('#main_form6 .no_of_power_unit').val();
  base_rate=	return_numeric($('#main_form6 .base_rate').val());					
  val = $(this).val();
  total = 0;		
  if(field=='excess_drivers_surcharge_percentage')
  {
	  total =  (val * base_rate)/100;
	  element = $(this).parent().parent().find(".efp12");
	  applyDecimal(element, total, 2);
  }else if(field=='excess_lapse_in_coverage_percentage'||field=='excess_new_venture_percentage'||field=='excess_losses_percentage') {
	  total =  (base_unit * base_rate)*val;
	  total = total/100;	
	 
	  element = $(this).parent().parent().find(".efp12");
	  applyDecimal(element, total, 2);
  }else {
	  total =  base_unit * val;
	  element = $(this).parent().parent().find(".efp12");
	  applyDecimal(element, total, 0);
  }
  calctotalcargo();
});
$("#main_form6 .oup, .ou").change(function(){
	
	old_unit = $("#main_form6 .ou").val();
	if ((old_unit=="None") || ($(this).val()=="0") || ($(this).val()=="No")){
		$(this).parents('.row-efl').find('.efp12').val(0);
		$(this).parents('.row-efl').find('.ofp').val(0);
	}else{
		val = $("#main_form6 #oup").val();
		total = old_unit * val;
		$("#main_form6 .ofp").val(total);
		$('#main_form6 .ofp').priceFormat({
				prefix: '$ ',
				centsLimit: 0,
				thousandsSeparator: ','
			});		
	}
	calctotalcargo();
});
$('#main_form6 .no_of_power_unit, .base_rate').blur(function(e) {
    calculateCargoRowPreimium();
});
function calculateCargoRowPreimium(){
	$('#main_form6 .row-efl').each(function(){
		if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != '0') && ($(this).find('.efl').val() != '') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined))
		{		
			var element = $(this).find('#main_form6 .efc');
			field = element.attr('name');
			base_unit=	$('#main_form6 .no_of_power_unit').val();
			base_rate=	return_numeric($('#main_form6 .base_rate').val());					
			val = element.val();
			total = 0;		
			if(field=='excess_drivers_surcharge_percentage')
			{
				total =  (val * base_rate)/100;
				element = $(this).find(".efp12");
				applyDecimal(element, total, 2);
			}else if(field=='excess_lapse_in_coverage_percentage'||field=='excess_new_venture_percentage'||field=='excess_losses_percentage') {
				total =  (base_unit * base_rate)*val;
				total = total/100;		
				element = $(this).find(".efp12");
				applyDecimal(element, total, 2);
			}else {
				total =  base_unit * val;
				element = $(this).find(".efp12");
				applyDecimal(element, total, 0);
			}
		}
	});	
	calctotalcargo();	
}
});