/*************Liability Calculation ************
 ****************** Ashvin Patel 21/july/2014**********/
$(document).ready(function(e) {
	function reCalculateAllPremiumn(element){
		var field = element.find(".lnvc").attr('name');
		var unit_price = return_numeric($('.unit_price').val());
		var quantity = return_numeric($('.quantity').val());
		var unit_price2 = return_numeric($('.unit_price2').val());	
		var quantity2 = return_numeric($('.quantity2').val());	
						
		var val2 = element.find(".lnvc").val();
		
		if(field=='drivers_surcharge_a_percentage')
		{
			var total = unit_price*val2;
			total = total/100;
			var element1 = element.find(".lnvp");
			applyDecimal(element1, total, 2);
		}
		else if(field=='drivers_surcharge_b_percentage')
		{
			var total = unit_price2*val2;
			total = total/100;
			var element1 = element.find(".lnvp");
			applyDecimal(element1, total, 2);
		}	
		else{
			var total = (unit_price*quantity)+(unit_price2*quantity2);
			total = total*val2/100;
			var element1 = element.find(".lnvp");
			applyDecimal(element1, total, 2);
		}	  
		calctotalliability();	
	}
	$('.lno_of_power_unit').change(function(e) {
		var total_liability_premium = return_numeric($('.total_liability_premium').val());
		var total_unit = $(this).val();
		unit_price_breakdown = total_liability_premium / total_unit;
		var element = $('.unit_price_breakdown');
		applyDecimal(element, unit_price_breakdown, 2);		
	});
	$('.unit_price, .quantity, .unit_price2, .quantity2').blur(function(e) {
		$('.row-lefl').each(function(){		
			if(($(this).find('.lefl').val() != 'No') && ($(this).find('.lefl').val() != '') && ($(this).find('.lefl').val() != 'Rejected') && ($(this).find('.lefl').val() != 'None') && ($(this).find('.lefl').val() != '0') && ($(this).find('.lefl').val() != undefined))
			{		
				if($(this).find(".lnvp").val() != undefined)
				{
					  reCalculateAllPremiumn($(this));				
				}
			}
		});
		$('.row-lefl').each(function(){
			
			if(($(this).find('.ldsa').val() != 'No') && ($(this).find('.ldsa').val() != '') && ($(this).find('.ldsa').val() != 'Rejected') && ($(this).find('.ldsa').val() != 'None') && ($(this).find('.ldsa').val() != '0') && ($(this).find('.ldsa').val() != undefined))
			{		
				if($(this).find(".lnvp").val() != undefined)
				{
					reCalculateAllPremiumn($(this)); 						
				}
			}
		});
		$('.row-efl2,.row-efl1').each(function(){
			if(($(this).find('.efl1').val() != '0')&& ($(this).find('.efl1').val() != undefined))
			{
				var trailer_quantity = return_numeric($(this).find('.efl1').val());
				//console.log(trailer_quantity);
				var trailer_cost=$(this).find('.etc').val();
				var total=parseInt(trailer_quantity)*parseInt(return_numeric(trailer_cost));
				$(this).find(".etp").val(total);	
				calctotalliability();						
			}
		});
		$('.row-efl3').each(function(){
			if(($(this).find('.umc').val() != '0')&& ($(this).find('.umc').val() != undefined))
			{			
				 $(this).find('.ump').val($('.lno_of_power_unit').val()*$(this).find('.umc').val());	
				  $('.price_format').priceFormat({
					  prefix: '$ ',
					  centsLimit: 0,
					  thousandsSeparator: ','
				  });
				  calctotalliability(); 						
			}
		});
	});
	$('.ldsa').change(function(e) {
		if($(this).val()=='Yes'){
			$(this).parents('.row-lefl').find('.lnvc').attr("disabled",false);
		}else{
			$(this).parents('.row-lefl').find('.lnvc').attr("disabled",true);
			$(this).parents('.row-lefl').find('.lnvc').val(0);
			$(this).parents('.row-lefl').find('.lnvp').val(0);
		}
	});
	$(".umc").change(function(){
	  $(this).parents('.row-efl3').find('.ump').val($('.lno_of_power_unit').val()*$(this).val());	
	  $('.price_format').priceFormat({
		  prefix: '$ ',
		  centsLimit: 0,
		  thousandsSeparator: ','
	  });
	  calctotalliability();
  });
  $('.efl3').change(function(){			
	  if ($(this).val()=="Rejected")
	  {				
		  $(this).parents('.row-efl').find('.nvc2').attr("disabled",true); 
		  $(this).parents('.row-efl3').find('.nvc2').val("0");	
		  $(this).parents('.row-efl3').find('.ump').val("0");	
	  }
	  else if ($(this).val()=="15/30")
	  {
		  $(this).parents('.row-efl3').find('.nvc2').val("50"); 
		  var element = $(this).parents('.row-efl3').find('.ump');	
		  var value = $('.lno_of_power_unit').val()*50;
		  applyDecimal(element, value, 0);	
		  
	  }
	  else if ($(this).val()=="30/60")
	  {
		  $(this).parents('.row-efl3').find('.nvc2').val("80");
		  var element = $(this).parents('.row-efl3').find('.ump');
		  var value = $('.lno_of_power_unit').val()*80;
		  applyDecimal(element, value, 0);	 	
		  
	  }
	  else if ($(this).val()=="2500")
	  {
	  
		  $(this).parents('.row-efl3').find('.nvc2').val("750"); 
		  var element = $(this).parents('.row-efl3').find('.ump');
		  var value = $('.lno_of_power_unit').val()*750;	
		  applyDecimal(element, value, 0);	
		  
	  }			
	  else
	  {
		  $(this).parents('.row-efl').find('.nvc2').attr("disabled",false);
	  }						
	  calctotalliability();
  });
  function calctotalliability(){
		total_cargo_premium = 0;	
		$('.row-lefl').each(function(){
			
			if(($(this).find('.lefl').val() != 'No') && ($(this).find('.lefl').val() != '') && ($(this).find('.lefl').val() != 'Rejected') && ($(this).find('.lefl').val() != 'None') && ($(this).find('.lefl').val() != '0') && ($(this).find('.lefl').val() != undefined))
			{		
				if($(this).find(".lnvp").val() != undefined)
				{
					premium = return_numeric($(this).find(".lnvp").val());	
					total_cargo_premium = parseFloat(total_cargo_premium) + parseFloat(premium); 						
				}
			}
		});
		$('.row-lefl').each(function(){
			
			if(($(this).find('.ldsa').val() != 'No') && ($(this).find('.ldsa').val() != '') && ($(this).find('.ldsa').val() != 'Rejected') && ($(this).find('.ldsa').val() != 'None') && ($(this).find('.ldsa').val() != '0') && ($(this).find('.ldsa').val() != undefined))
			{		
				if($(this).find(".lnvp").val() != undefined)
				{
					premium = return_numeric($(this).find(".lnvp").val());	
					total_cargo_premium = parseFloat(total_cargo_premium) + parseFloat(premium); 						
				}
			}
		});
		$('.row-efl2,.row-efl1').each(function(){
			if(($(this).find('.efl1').val() != '0')&& ($(this).find('.efl1').val() != undefined))
			{			
				premium = $(this).find(".nvp2").val();	
				total_cargo_premium = parseFloat(total_cargo_premium) + parseFloat(return_numeric(premium)); 						
			}
		});
		$('.row-efl3').each(function(){
			if(($(this).find('.umc').val() != '0')&& ($(this).find('.umc').val() != undefined))
			{			
				premium = $(this).find(".ump").val();	
				total_cargo_premium = parseFloat(total_cargo_premium) + parseFloat(return_numeric(premium)); 						
			}
		});
	
		var val1=return_numeric($('.unit_price').val());
		var val2=$('.quantity').val();
		var val3=return_numeric($('.unit_price2').val());	
		var val4=$('.quantity2').val();	
		total_cargo_premium = (parseFloat(val1*val2)+parseFloat(val3*val4)) + parseFloat(total_cargo_premium);
		//total_unit = parseInt(val2) + parseInt(val4);
		total_unit = $('.lno_of_power_unit').val();
		total_cargo_premium = parseFloat(total_cargo_premium) - parseFloat(return_numeric($(".disc3").val()));
		if(total_unit != 0)
		{
			unit_price_breakdown = total_cargo_premium / total_unit;
		}
		
		var element = $(".unit_price_breakdown");
		var element1 = $(".total_liability_premium");
		applyDecimal(element, unit_price_breakdown, 2);
		applyDecimal(element1, total_cargo_premium, 2);
		$('.price_format_decimal').priceFormat({
			prefix: '$ ',
			centsLimit: 2,
			thousandsSeparator: ','
		});
	}
	$('.lnvc').change(function(){
	  var field = $(this).attr('name');
	  var unit_price = return_numeric($('.unit_price').val());
	  var quantity = return_numeric($('.quantity').val());
	  var unit_price2 = return_numeric($('.unit_price2').val());	
	  var quantity2 = return_numeric($('.quantity2').val());	
					  
	  var val2=$(this).val();
	  
	  if(field=='drivers_surcharge_a_percentage')
	  {
		  var total = unit_price*val2;
		  total = total/100;
		  var element = $(this).parent().parent().find(".lnvp");
		  applyDecimal(element, total, 2);
	  }
	  else if(field=='drivers_surcharge_b_percentage')
	  {
		  var total = unit_price2*val2;
		  total = total/100;
		  var element = $(this).parent().parent().find(".lnvp");
		  applyDecimal(element, total, 2);
	  }	
	  else{
		  var total = (unit_price*quantity)+(unit_price2*quantity2);
		  total = total*val2/100;
		  var element = $(this).parent().parent().find(".lnvp");
		  applyDecimal(element, total, 2);
	  }	  
	  calctotalliability();
  });
  $(".disc1").change(function(){
	if($(this).val() == 'Yes')
	{
	  $(".disc2").attr("disabled",false);
	}else
	{
	  $(".disc2").attr("disabled",true);
	  $(".disc2").val(0);
	  $(".disc3").val(0);
	  calctotalliability();
	}
  });
  $(".disc2").change(function(){
	  var val1=return_numeric($('.unit_price').val());
	  var val2=$('.quantity').val();
	  var val3=return_numeric($('.unit_price2').val());	
	  var val4=$('.quantity2').val();	
	  var val5=$(this).val();
	  var val6 = (val1*val2)+(val3*val4);
	  var val7 = (val6*val5)/100;
	 
	  console.log(val7);	 
	  var element = $(".disc3");
	  applyDecimal(element, val7, 2);
	  calctotalliability();
  });
  $('.etq').blur(function(){
	  var trailer_quantity = return_numeric($(this).val());
	  //console.log(trailer_quantity);
	  var trailer_cost=$(this).parents('.row-efl1').find('.etc').val();
	  var total=parseInt(trailer_quantity)*parseInt(return_numeric(trailer_cost));
	  $(this).parents('.row-efl1').find(".etp").val(total);	
	  calctotalliability();	
	  $(this).parents('.row-efl1').find(".etp").priceFormat({
			prefix: '$ ',
			centsLimit: 0,
			thousandsSeparator: ','
		});					
  });	
  $('.etc').change(function(){
	  var trailer_quantity = return_numeric($(this).val());
	  //console.log(trailer_quantity);
	  var trailer_cost = $(this).parents('.row-efl1').find('.etq').val();
	  var total = parseInt(trailer_quantity)*parseInt(return_numeric(trailer_cost));
	  $(this).parents('.row-efl1').find(".etp").val(total);
	  calctotalliability();
	  $(this).parents('.row-efl1').find(".etp").priceFormat({
			prefix: '$ ',
			centsLimit: 0,
			thousandsSeparator: ','
		});						
  });
  $('.lefl').change(function(){
	if (($(this).val()=="None") || ($(this).val()=="0") || ($(this).val()=="No"))
	{
		$(this).parents('.row-lefl').find('.mkread1').attr("disabled",true); 		
		$(this).parents('.row-lefl').find('.mkread1').val(0); 
		$(this).parents('.row-lefl').find('.lnvp').val(0); 
		calctotalliability();
	}
	else
	{
		$(this).parents('.row-lefl').find('.mkread1').attr("disabled",false);
	}
});
$('.efl1').blur(function(){
	var val = return_numeric($(this).val());
	if ((val=="No") || (val=="Rejected") || (val=="None") || (val=="0"))
	{
		$(this).parents('.row-efl1').find('.mkread1').attr("disabled",true); 			
	}
	else
	{
	   
		$(this).parents('.row-efl1').find('.mkread1').attr("disabled",false);
	}
	calctotalliability();
});
});