var liability = 'No';
var pd = 'No';
var cargo = 'No';
$(document).ready(function(e) {
	$('#coverage').change(function(e) {
		var coverage = $(this).val();
		if(coverage){
			if(coverage.indexOf('Liability') >= 0){				
				liability = 'Yes';
			}else{
				liability = 'No';
			}
			
			if(coverage.indexOf('PD') >= 0){				
				pd = 'Yes';
			}else{
				pd = 'No';
			}
			
			if(coverage.indexOf('Cargo') >= 0){				
				cargo = 'Yes';
			}else{
				cargo = 'No';
			}
		}else{
		  liability = 'No';
		  pd = 'No';
		  cargo = 'No';
		}
    });
	$('#vh_type').change(function(e) {
    	var vh_type = $('#vh_type').val();
   		if(vh_type){
		  if(vh_type.indexOf('Truck') >= 0){				
			  $('.truck_box').show();
		  }else{
			  $('.truck_box').hide();
		  }
		  
		  if(vh_type.indexOf('Tractor') >= 0){				
			  $('.tractor_box').show();
		  }else{
			  $('.tractor_box').hide();
		  }
		  
		  if(vh_type.indexOf('Trailer') >= 0){				
			  $('.trailer_box').show();
		  }else{
			  $('.trailer_box').hide();
		  }
		  
		  if(vh_type.indexOf('Other') >= 0){				
			  $('.other_box').show();
		  }else{
			  $('.other_box').hide();
		  }
		}else{
			$('.truck_box').hide();	
			$('.tractor_box').hide();
			$('.trailer_box').hide();
			$('.other_box').hide();
		}
    });
   
});


function check_commodities_other(){
		
}
$(function(){
	
	var uploaded_files = '';
	var uploaded_files_2 = '';
    var ul = $('#upload ul');
	//document.getElementById('uploaded_files_values').value = '';

	
    $('#drop a').click(function(){
        // Simulate a click on the file input button
        // to show the file browser dialog
		uploaded_files = '';
        document.getElementById('uploaded_files_values').value = '';
		$(this).parent().find('input').click();
    });

	$('#upload').fileupload({
			
        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),

        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {

            var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+
                ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" />  <p></p><fil style="display:none" ></fil><span></span></li>');

            // Append the file name and file size
            tpl.find('p').text(data.files[0].name)
                         .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

            // Add the HTML to the UL element
            data.context = tpl.appendTo("#upload_file_name1");

            // Initialize the knob plugin
            tpl.find('input').knob();
			
			
			
			
            // Listen for clicks on the cancel icon
            tpl.find('span').click(function(){

                if(tpl.hasClass('working')){
                    jqXHR.abort();
                }
			
			
			 	var n = tpl.find('fil').text();
				//alert(n);
				
				var list = $('#uploaded_files_values').val(); 
				
				var res = removevalue(list, n, ',');

				document.getElementById('uploaded_files_values').value = res;
				uploaded_files = res;
				
                tpl.fadeOut(function(){
                    tpl.remove();
                });

            });

            // Automatically upload the file once it is added to the queue
            var jqXHR = data.submit() .done(function(e, data) { filename=e; //alert(+' e'); //alert('data '+data);

tpl.find('fil').text(filename);
	//	alert(jqXHR.responseText);
			if(uploaded_files == '')
			{
				uploaded_files = filename;
			//	uploaded_files = data.files[0].name;
			}
			else
			{
				uploaded_files += ',';
				//uploaded_files += data.files[0].name;
				uploaded_files += filename;
			}
	
		$('#uploaded_files_values').val(uploaded_files);
    })
		},
		//done: function (e, data){ alert('rs '+data.toSource());},
        progress: function(e, data){ 

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if(progress == 100){
                data.context.removeClass('working');
            }
        },

        fail:function(e, data){
            // Something has gone wrong!
            data.context.addClass('error');
        }

    });
	

   /*  // Initialize the jQuery File Upload plugin
    $('#upload').fileupload({
		
        // This element will accept file drag/drop uploading
        dropZone: $('#drop'),

        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {

            var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+
                ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" /><p></p><span></span></li>');

            // Append the file name and file size
            tpl.find('p').text(data.files[0].name)
                         .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

            // Add the HTML to the UL element
            data.context = tpl.appendTo("#upload_file_name1");

            // Initialize the knob plugin
            tpl.find('input').knob();

            // Listen for clicks on the cancel icon
            tpl.find('span').click(function(){

                if(tpl.hasClass('working')){
                    jqXHR.abort();
                }

                tpl.fadeOut(function(){
                    tpl.remove();
                });

            });

            // Automatically upload the file once it is added to the queue
            var jqXHR = data.submit();
			if(uploaded_files == '')
			{
				uploaded_files = data.files[0].name;
			}
			else
			{
				uploaded_files += ',';
				uploaded_files += data.files[0].name;
			}
       document.getElementById('uploaded_files_values').value = uploaded_files;
		},

        progress: function(e, data){

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if(progress == 100){
                data.context.removeClass('working');
            }
        },

        fail:function(e, data){
            // Something has gone wrong!
            data.context.addClass('error');
        }

    }); */

	$('#upload_second').fileupload({
			
        // This element will accept file drag/drop uploading
        dropZone: $('#drop_second'),

        // This function is called when a file is added to the queue;
        // either via the browse button, or via drag/drop:
        add: function (e, data) {
//alert(data.files[0].name);
            var tpl = $('<li class="working"><input type="text" value="0" data-width="48" data-height="48"'+
                ' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" />  <p></p><fil style="display:none" ></fil><span></span></li>');
               //' data-fgColor="#0788a5" data-readOnly="1" data-bgColor="#3e4043" />  <p></p><div style="display:none" id="file_up2"></div><span></span></li>');

            // Append the file name and file size
            tpl.find('p').text(data.files[0].name)
                         .append('<i>' + formatFileSize(data.files[0].size) + '</i>');

            // Add the HTML to the UL element
            data.context = tpl.appendTo("#upload_file_name_second");

            // Initialize the knob plugin
            tpl.find('input').knob();
			
			var file_name2=$( "#file_up2" );
			//tpl.find(file_name2).text('<input type="hidden" name="upload2[]" value="'+data.files[0].name+'" >');
			//tpl.find('fil').text(data.files[0].name);
			
            // Listen for clicks on the cancel icon
            tpl.find('span').click(function(){

                if(tpl.hasClass('working')){
                    jqXHR.abort();
                }
				var file_name2=$( "#file_up2" );
			
			 	var n = tpl.find('fil').text();
				//alert(n);
				
				var list = $('#uploaded_files_values_second').val(); //document.getElementById('uploaded_files_values_second').value();
				//alert(' List '+list);			
				//var res = explode(',',item_n);
				//alert(res); 
				
				
				var res = removevalue(list, n, ',');
				//alert('removed '+res);
				//document.getElementById('uploaded_files_values_second').value = '';
				document.getElementById('uploaded_files_values_second').value = res;
				uploaded_files_2 = res;
				
                tpl.fadeOut(function(){
                    tpl.remove();
                });

            });

            // Automatically upload the file once it is added to the queue
            var jqXHR = data.submit() .done(function(e, data) { filename=e; //alert(+' e'); //alert('data '+data);

tpl.find('fil').text(filename);
	//	alert(jqXHR.responseText);
			if(uploaded_files_2 == '')
			{
				uploaded_files_2 = filename;
			//	uploaded_files = data.files[0].name;
			}
			else
			{
				uploaded_files_2 += ',';
				//uploaded_files += data.files[0].name;
				uploaded_files_2 += filename;
			}
				$('#uploaded_files_values_second').val(uploaded_files_2);
			})
			//uploaded_files_2 = $('#uploaded_files_values_second').val();
			//alert(uploaded_files_2);
			/* if(uploaded_files_2 == '')
			{
				uploaded_files_2 = data.files[0].name;
			}
			else
			{
				uploaded_files_2 += ',';
				uploaded_files_2 += data.files[0].name;
			} */
		//alert('rewrite');	
	
      // document.getElementById('uploaded_files_values_second').value = uploaded_files_2;
       //document.getElementById('file_div').append('test');
		},

        progress: function(e, data){

            // Calculate the completion percentage of the upload
            var progress = parseInt(data.loaded / data.total * 100, 10);

            // Update the hidden input field and trigger a change
            // so that the jQuery knob plugin knows to update the dial
            data.context.find('input').val(progress).change();

            if(progress == 100){
                data.context.removeClass('working');
            }
        },

        fail:function(e, data){
            // Something has gone wrong!
            data.context.addClass('error');
        }

    });
    // Prevent the default action when a file is dropped on the window
    $(document).on('drop dragover', function (e) {
        e.preventDefault();
    });

    // Helper function that formats the file sizes
    function formatFileSize(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }

        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }

        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }

        return (bytes / 1000).toFixed(2) + ' KB';
    }
	
	function implode (glue, pieces) {
  // http://kevin.vanzonneveld.net
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Waldo Malqui Silva
  // +   improved by: Itsacon (http://www.itsacon.net/)
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // *     example 1: implode(' ', ['Kevin', 'van', 'Zonneveld']);
  // *     returns 1: 'Kevin van Zonneveld'
  // *     example 2: implode(' ', {first:'Kevin', last: 'van Zonneveld'});
  // *     returns 2: 'Kevin van Zonneveld'
  var i = '',
    retVal = '',
    tGlue = '';
  if (arguments.length === 1) {
    pieces = glue;
    glue = '';
  }
  if (typeof pieces === 'object') {
    if (Object.prototype.toString.call(pieces) === '[object Array]') {
      return pieces.join(glue);
    }
    for (i in pieces) {
      retVal += tGlue + pieces[i];
      tGlue = glue;
    }
    return retVal;
  }
  return pieces;
}

function explode (delimiter, string, limit) {

  if ( arguments.length < 2 || typeof delimiter === 'undefined' || typeof string === 'undefined' ) return null;
  if ( delimiter === '' || delimiter === false || delimiter === null) return false;
  if ( typeof delimiter === 'function' || typeof delimiter === 'object' || typeof string === 'function' || typeof string === 'object'){
    return { 0: '' };
  }
  if ( delimiter === true ) delimiter = '1';

  // Here we go...
  delimiter += '';
  string += '';

  var s = string.split( delimiter );


  if ( typeof limit === 'undefined' ) return s;

  // Support for limit
  if ( limit === 0 ) limit = 1;

  // Positive limit
  if ( limit > 0 ){
    if ( limit >= s.length ) return s;
    return s.slice( 0, limit - 1 ).concat( [ s.slice( limit - 1 ).join( delimiter ) ] );
  }

  // Negative limit
  if ( -limit >= s.length ) return [];

  s.splice( s.length + limit );
  return s;
}

function removevalue (list, value, separator) {
				  separator = separator || ",";
				  var values = list.split(separator);
				  for(var i = 0 ; i < values.length ; i++) {
					if(values[i] == value) {
					  values.splice(i, 1);
					  return values.join(separator);
					}
				  }
				  return list;
				}


});


