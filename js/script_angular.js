var myApp = angular.module('edit',[], function($interpolateProvider){
	$interpolateProvider.startSymbol('<%');
	$interpolateProvider.endSymbol('%>');	
});
function getLevelName(level_id){
	if(level_id == 1){
		return 'Lower Primary';	
	}else if(level_id == 2){
		return 'Upper Primary';	
	}else if(level_id == 3){
		return 'Lower Secondary';	
	}else if(level_id == 4){
		return 'Lower Secondary';	
	}
}
function getSubName(level_id){
	if(level_id == 1){
		return 'English';	
	}else if(level_id == 2){
		return 'Mathematics';	
	}else if(level_id == 3){
		return 'Science';	
	}else if(level_id == 4){
		return 'Chinese';	
	}
}
function levels($http, $scope, $rootScope, $window) {
	//var levels = $.parseJSON($window.levels_0);
	$scope.datarows = [];
	$scope.init = function(user_id){
		var tutor_id = (user_id) ? user_id : $window.session_user_id;
		if(tutor_id){
			$.ajax({
				url: base_url+'/getLSLS',
				type: 'POST',
				async: false,
				data:{_token:csrf_token,tutor_id:tutor_id},
				success:function(json){					
					if(json){
						if(json.length > 0){
							var levels = $.parseJSON(json);
							$.each(levels, function(index, value){								
								$scope.datarows.push(value);
								$('.b_sublevels').trigger('liszt:updated');
							});
						}
					}
				}
			});
			
		}
		//console.log($scope.datarows);	
	}
	
	$scope.add_row = function() {
		  $scope.datarows.push(
            { column1: 'row #' + $scope.datarows.length}
        );
    };
	 $scope.delet_row = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.datarows.splice(index, 1);
		}		
    }
	$scope.get_subjects = function(elem){
		var level_id = elem.row;		
	}
	$scope.isOptionSelected = function(option_values, option_id) {		
		if(option_values){				
			if(option_values.indexOf(option_id) >= 0){				
				return 'selected';	
			}else{
				return  '';	
			}
		}
		return '';		
  };
}

/*
* Angular Chosen directive
* 
* Ashvin Patel 30/Mar/2015
*/
myApp.directive('chosen', function ($http) {
	var linker = function (scope, element, attr) {
		scope.$watch(scope.states, function (oldVal, newVal) {
			setTimeout(function(){
				element.trigger('liszt:updated');
			}, 500);
		});
		scope.$watch(attr.ngModel, function () {
			setTimeout(function(){
				element.trigger('liszt:updated');
			}, 500);
		});
		setTimeout(function(){
			element.chosen({disable_search_threshold: 20});
		}, 500);
	};
	
	return {
		restrict: 'A',
		link: linker
	}
});

/*
* Angular datepicker range directive
* 
* Ashvin Patel 30/Mar/2015
*/
myApp.directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {			
            element.datepicker({
                dateFormat: 'mm/dd/yyyy',
                autoclose: true,
                onSelect: function() {                   
                }
            }).on('changeDate', function(e){ //alert('here');
                $('.datepicker').hide();
                _endDate = new Date(e.date.getTime()); //get new end date
                //alert(_endDate);
                $('#'+attrs.end).datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
            })
             
        }
    };
});

/*
* Angular timepicker directive
* 
* Ashvin Patel 06/Mar/2015
*/
myApp.directive('jqtimepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {	
			$.mask.definitions['H'] = '[012]';
			$.mask.definitions['N'] = '[012345]';
			$.mask.definitions['n'] = '[0123456789]';
			$.mask.definitions['am'] = '[AM]';
			$.mask.definitions['am'] = '[PM]';
			//element.mask("Hn:Nn am");		
            element.timepicker({ 'scrollDefaultNow': true });
        }
    };
});

/*
* Angular time mask directive
* 
* Ashvin Patel 06/Mar/2015
*/
myApp.directive('jqtimemask', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {			
            
        }
    };
});

myApp.filter('capitalize', function() {
    return function(input, scope) {
        return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
});

  
function students($http, $scope, $rootScope, $window) {	
    
	$scope.students = [];	
	var json=$window.student;
	if(json){
		var obj = jQuery.parseJSON(json);
		var names=obj.f_name+' '+obj.l_name;
	$scope.students.push({ id:obj.id,name: names , mobile: $window.session_user_mobile, email: obj.email} );
		}else{
	 $scope.students.push({ id:$window.session_user_id,name: $window.session_user_name, mobile: $window.session_user_mobile, email: $window.session_user_email} );
		}
	$scope.add_row = function() {
		
		  $scope.students.push({name: '', mobile: '', email: ''});
    };
	 $scope.delete_row = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.students.splice(index, 1);
		  setTimeout(function(){
		 	 calculatePayment();
		  }, 200);
		}		
    }	
}
function avail_week($http, $scope, $rootScope, $window){
	$scope.Monday = [];
	$scope.Tuesday = [];
	$scope.Wednesday = [];
	$scope.Thursday = [];
	$scope.Friday = [];
	$scope.Saturday = [];
	$scope.Sunday = [];
	
	var avail_days = $window.avail_days;
	avail_days = $.parseJSON(avail_days);
	if(avail_days){
		$.each(avail_days, function(index, value){			
			switch(index){
				case '1':
				case 1:
					$.each(value, function(i, v){						
						$scope.Monday.push({id:v['id'],start: v['start'], end: v['end']});
					});					
				break;
				case '2':
				case 2:
					$.each(value, function(i, v){
						$scope.Tuesday.push({id:v['id'],start: v['start'], end: v['end']});
					});
				break;
				case '3':
				case 3:
					$.each(value, function(i, v){
						$scope.Wednesday.push({id:v['id'],start: v['start'], end: v['end']});
					});
				break;
				case '4':
				case 4:
					$.each(value, function(i, v){
						$scope.Thursday.push({id:v['id'],start: v['start'], end: v['end']});
					});
				break;
				case '5':
				case 5:
					$.each(value, function(i, v){
						$scope.Friday.push({id:v['id'],start: v['start'], end: v['end']});
					});
				break;
				case '6':
				case 6:
					$.each(value, function(i, v){
						$scope.Saturday.push({id:v['id'],start: v['start'], end: v['end']});
					});
				break;
				case '7':
				case 7:
					$.each(value, function(i, v){
						$scope.Sunday.push({id:v['id'],start: v['start'], end: v['end']});
					});
				break;
						
			}
		});
		if(!$scope.Monday.length)
		$scope.Monday.push({start: '', end: ''});
		
		if(!$scope.Tuesday.length)
		$scope.Tuesday.push({start: '', end: ''});
		
		if(!$scope.Wednesday.length)
		$scope.Wednesday.push({start: '', end: ''});
		
		if(!$scope.Thursday.length)
		$scope.Thursday.push({start: '', end: ''});
		
		if(!$scope.Friday.length)
		$scope.Friday.push({start: '', end: ''});
		
		if(!$scope.Saturday.length)
		$scope.Saturday.push({start: '', end: ''});
		
		if(!$scope.Sunday.length)
		$scope.Sunday.push({start: '', end: ''});
	}else{
		$scope.Monday.push({start: '', end: ''});
		$scope.Tuesday.push({start: '', end: ''});
		$scope.Wednesday.push({start: '', end: ''});
		$scope.Thursday.push({start: '', end: ''});
		$scope.Friday.push({start: '', end: ''});
		$scope.Saturday.push({start: '', end: ''});
		$scope.Sunday.push({start: '', end: ''});
	}
	
	$scope.add_slot = function(day){
		switch(day){
			case 'Monday':
				if($scope.Monday.length < 2){
					$scope.Monday.push({start: '', end: ''});
				}
				if($scope.Monday.length > 1){
					show_hide_and('Monday')	;
				}
			break;
			case 'Tuesday':
				if($scope.Tuesday.length < 2){
					$scope.Tuesday.push({start: '', end: ''});
				}
				if($scope.Tuesday.length > 1){
					show_hide_and('Tuesday')	;
				}
			break;
			case 'Wednesday':
				if($scope.Wednesday.length < 2){
					$scope.Wednesday.push({start: '', end: ''});
				}
				if($scope.Wednesday.length > 1){
					show_hide_and('Wednesday')	;
				}
			break;
			case 'Thursday':
				if($scope.Thursday.length < 2){
					$scope.Thursday.push({start: '', end: ''});
				}
				if($scope.Thursday.length > 1){
					show_hide_and('Thursday')	;
				}
			break;
			case 'Friday':
				if($scope.Friday.length < 2){
					$scope.Friday.push({start: '', end: ''});
				}
				if($scope.Friday.length > 1){
					show_hide_and('Friday')	;
				}
			break;
			case 'Saturday':
				if($scope.Saturday.length < 2){
					$scope.Saturday.push({start: '', end: ''});
				}
				if($scope.Saturday.length > 1){
					show_hide_and('Saturday')	;
				}
			break;
			case 'Sunday':
				if($scope.Sunday.length < 2){
					$scope.Sunday.push({start: '', end: ''});
				}
				if($scope.Sunday.length > 1){
					show_hide_and('Sunday')	;
				}
			break;
		}	
	}
	$scope.remove_slot = function(index, day){
		switch(day){
			case 'Monday':
				$scope.Monday.splice(index, 1);
			break;
			case 'Tuesday':
				$scope.Tuesday.splice(index, 1);
			break;
			case 'Wednesday':
				$scope.Wednesday.splice(index, 1);
			break;
			case 'Thursday':
				$scope.Thursday.splice(index, 1);
			break;
			case 'Friday':
				$scope.Friday.splice(index, 1);
			break;
			case 'Saturday':
				$scope.Saturday.splice(index, 1);
			break;
			case 'Sunday':
				$scope.Sunday.splice(index, 1);
			break;
		}	
	}
}

function show_hide_and(day){
	//console.log($('#Monday_1'));
			
}
/*
* Education rows
* 
*/
function education($http, $scope, $rootScope, $window){
	$scope.educationrows = [];
	var user_education = $window.user_education
	if(user_education.length){
		user_education = $.parseJSON(user_education);
		$.each(user_education, function(index, value){
			value['is_current'] = (value['is_current']) ? true : false;
			value['readonly'] = (value['is_current']) ? true : false;
			value['end'] = (value['is_current']) ? 'present' : value['end'] ;
			var files = (value['files']) ? value['files'].split(',') : '';
			value['files_0'] = files;
			value['files_old'] = value['files'];
			value['files'] = [{file:''}];
			$scope.educationrows.push(value);	
		});
	}
	//$scope.education.push({start:'',end:'',files:[{file:''}]});
	
	$scope.add_row = function(){
		$scope.educationrows.push({start:'',end:'present',is_current:true,readonly:true,files:[{file:''}]});	
	}	

	$scope.remove_row = function(index){
		if(confirm('Are you sure you want to delete this row')){
			$scope.educationrows.splice(index, 1);	
		}
	}
	
	$scope.add_file = function(index){
		$scope.educationrows[index].files.push({file:''});
	}
	$scope.remove_file = function(parent_index, index){
		$scope.educationrows[parent_index].files.splice(index, 1);		
	}
}

/*
* Location rows
* 
*/
function locations($http, $scope, $rootScope, $window){
	$scope.locationrows = [];
	var tutor_id = $window.session_user_id;
	$scope.init = function(){
		$.ajax({
		  url: base_url+'/getRMRT',
		  type: 'POST',
		  async: false,
		  data:{_token:csrf_token,tutor_id:tutor_id},
		  success:function(json){					
			  if(json){
				  if(json.length > 0){
					  var locations = $.parseJSON(json);
					  $.each(locations, function(index, location){								
						  $scope.locationrows.push(location);
						  $('.locationrows').trigger('liszt:updated');
					  });
				  }
			  }
		  }
	  });		
	}
	
	$scope.add_row = function(){
		$scope.locationrows.push({start:'',end:''});	
	}
	$scope.remove_row = function(index){
		if(confirm('Are you sure you want to delete this row')){
			$scope.locationrows.splice(index, 1);	
		}
	}
	$scope.isOptionSelected = function(option_values, option_id) {		
		if(option_values){				
			if(option_values.indexOf(option_id) >= 0){				
				return 'selected';	
			}else{
				return  '';	
			}
		}
		return '';		
  };
}


/*
* job location rows
* 
*/
function job_locations($http, $scope, $rootScope, $window){
	$scope.locationrowss = [];
	var job_id = $window.job_location;
	
	$scope.init = function(){
		$.ajax({
		  url: base_url+'/get_LOC_RMRT',
		  type: 'POST',
		  async: false,
		  data:{_token:csrf_token,job_id:job_id},
		  success:function(json){					
			  if(json){
				  if(json.length > 0){
					  var locations = $.parseJSON(json);
					  
					  $.each(locations, function(index, location){	
					  	  		
						  $scope.locationrowss.push(location);
						  $('.locationrowss').trigger('liszt:updated');
					  });
				  }
			  }
		  }
	  });		
	}
	
	$scope.add_row = function(){
		$scope.locationrowss.push({start:'',end:''});	
	}
	$scope.remove_row = function(index){
		if(confirm('Are you sure you want to delete this row')){
			$scope.locationrowss.splice(index, 1);	
		}
	}
	$scope.isOptionSelected = function(option_values, option_id) {		
		if(option_values){				
			if(option_values.indexOf(option_id) >= 0){				
				return 'selected';	
			}else{
				return  '';	
			}
		}
		return '';		
  };
}



/*
* Experience
*
*/
function experience($http, $scope, $rootScope, $window){
	$scope.experiencerows = [];
	var user_experience = $window.user_experience;
	if(user_experience.length){
		user_experience = $.parseJSON(user_experience);
		//$scope.experiencerows = user_experience;
		$.each(user_experience, function(index, value){
			value['is_current'] = (value['is_current']) ? true : false;
			value['readonly'] = (value['is_current']) ? true : false;
			value['end'] = (value['is_current']) ? 'present' : value['end'] ;
			value['title_m'] = str_replace('_', ' ', value['title'])
			$scope.experiencerows.push(value);	
		});	
	}
	//$scope.experiencerows.push({start:'',end:''});
	
	$scope.add_row = function(){
		$scope.experiencerows.push({start:'',end:''});	
	}
	$scope.remove_row = function(index){
		if(confirm('Are you sure you want to delete this row')){
			$scope.experiencerows.splice(index, 1);	
		}
	}	
}

/*
* Tutot Teaching location
* 
* 
*/
function tt_location($http, $scope, $rootScope, $window){
	$scope.data = [];
	 var user_tt_location = $window.user_tt_location
	if(user_tt_location){
	 if(user_tt_location.length){
	  user_tt_location = $.parseJSON(user_tt_location); 
	   
	   $scope.data=user_tt_location;  
	  
	 }
	}
	//$scope.data.push({zip:'',street_no:'',block_no:'',unit_no:'',floor_no:''});	
	
	$scope.add_row = function(){
		$scope.data.push({zip:'',street_no:'', block_no:'',unit_no:'',floor_no:''});
	}
	$scope.remove_row = function(index){
		if(confirm('Are you sure you want to delete this row')){
			$scope.data.splice(index, 1);	
		}
	}	
}
function booking_location($http, $scope, $rootScope, $window){
	$scope.data = [];		
	
	$scope.add_row = function(){
		$scope.data.push({zip:'',street:'', block_no:'',unit_no:'',floor_no:''});
		$('a.btn.top5.bottom10').hide();
	}
	$scope.remove_row = function(index){
		if(confirm('Are you sure you want to delete this row')){
			
			$scope.data.splice(index, 1);	
			$('a.btn.top5.bottom10').show();
		}
	}			
}
/** booking level **/
function level($http, $scope, $rootScope, $window) {
	//var levels = $.parseJSON($window.levels_0);
	$scope.datarow = [];
	
	
	$scope.add_row = function() {
		  $scope.datarow.push(
            { column1: 'row #' + $scope.datarow.length}
        );
    };
	 $scope.delet_row = function(index) {
		var retVal = confirm("Are you sure you want to delete this row");
		if (retVal == true) {
		  $scope.datarow.splice(index, 1);
		}		
    }
	

}

/**Job Posting level subjects**/
function job_levels($http, $scope, $rootScope, $window) {
 //var levels = $.parseJSON($window.levels_0);
 $scope.datarows = [];
 $scope.init = function(job_id){
  var job_id = job_id;
  if(job_id){
   $.ajax({
    url: base_url+'/getJobLSLS',
    type: 'POST',
    async: false,
    data:{_token:csrf_token,job_id:job_id},
    success:function(json){     
     if(json){
      if(json.length > 0){
       var levels = $.parseJSON(json);
	   	if(levels){
		   $.each(levels, function(index, value){        
			$scope.datarows.push(value);
			$('.b_sublevels').trigger('liszt:updated');
		   });
		 }
      }
     }
    }
   });
   
  }
  //console.log($scope.datarows); 
 }
 
 $scope.add_row = function() {
    $scope.datarows.push(
            { column1: 'row #' + $scope.datarows.length}
        );
    };
  $scope.delet_row = function(index) {
  var retVal = confirm("Are you sure you want to delete this row");
  if (retVal == true) {
    $scope.datarows.splice(index, 1);
  }  
    }
 $scope.get_subjects = function(elem){
  var level_id = elem.row;  
 }
 $scope.isOptionSelected = function(option_values, option_id) { 
	 if(!isNaN(option_id) && !isNaN(option_id) ) {
		option_id = '"'+option_id+'"';
		option_values = '"'+option_values+'"';
		  if(option_values){    
		   if(option_values.indexOf(option_id) >= 0){    
			return 'selected'; 
		   }else{
			return  ''; 
		   }
		  }
		  return ''; 
	}	else{
		 if(option_values){    
		   if(option_values.indexOf(option_id) >= 0){    
			return 'selected'; 
		   }else{
			return  ''; 
		   }
		  }
		  return '';	
	}
	  
  };
}