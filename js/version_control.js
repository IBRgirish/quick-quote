// JavaScript Document
//Version diff for vehicls
function version_diff(v_id, v1_id, type){
  $.fancybox({
	type: 'iframe',
	href: base_url+'quote/vehicle_version_diff/'+v_id+'/'+v1_id+'/'+type,
	autoSize: true,
	closeBtn: true,
	//width: '850',
	/*height: '150',*/
	closeClick: true,
	enableEscapeButton: true,
	beforeLoad: function () {},
  });	
}

//Version diff for drivers
function driver_version_diff(v_id, v1_id, type){
  $.fancybox({
	type: 'iframe',
	href: base_url+'quote/driver_version_diff/'+v_id+'/'+v1_id+'/'+type,
	autoSize: true,
	closeBtn: true,
	//width: '850',
	/*height: '150',*/
	closeClick: true,
	enableEscapeButton: true,
	beforeLoad: function () {},
  });	
}

//Version diff for owners
function owner_version_diff(v_id, v1_id, type){
  $.fancybox({
	type: 'iframe',
	href: base_url+'quote/owner_version_diff/'+v_id+'/'+v1_id+'/'+type,
	autoSize: true,
	closeBtn: true,
	//width: '850',
	/*height: '150',*/
	closeClick: true,
	enableEscapeButton: true,
	beforeLoad: function () {},
  });	
}

//Version diff for losses
function losse_version_diff(v_id, v1_id, type){
  $.fancybox({
	type: 'iframe',
	href: base_url+'quote/losses_version_diff/'+v_id+'/'+v1_id+'/'+type,
	autoSize: true,
	closeBtn: true,
	//width: '850',
	/*height: '150',*/
	closeClick: true,
	enableEscapeButton: true,
	beforeLoad: function () {},
  });	
}
//Version diff for filing
function filing_version_diff(v_id, v1_id, type){
  $.fancybox({
	type: 'iframe',
	href: base_url+'quote/filing_version_diff/'+v_id+'/'+v1_id+'/'+type,
	autoSize: true,
	closeBtn: true,
	//width: '850',
	/*height: '150',*/
	closeClick: true,
	enableEscapeButton: true,
	beforeLoad: function () {},
  });	
}