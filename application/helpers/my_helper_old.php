<?php  
//-****** FILE /system/application/helpers/form_helper.php code ********-->

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Drop-down Menu FROM database
 *
 * @access    public
 * @param    string
 * @param    array
 * @param    string
 * @param    string
 * @return    string
 */

   function form_simple_dropdown_from_db_valuebyname($name = '', $sql, $selected = array(),$choose='', $extra = '')
    {
        $CI =& get_instance();
        if ( ! is_array($selected))
        {
            $selected = array($selected);
        }
		
        // If no selected state was submitted we will attempt to set it automatically
        if (count($selected) === 0)
        {
            // If the form name appears in the $_POST array we have a winner!
            if (isset($_POST[$name]))
            {
                $selected = array($_POST[$name]);
            }
        }

        if ($extra != '') $extra = ' '.$extra;

        $multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

        $form = '<select id="'.$name.'" name="'.$name.'"'.$extra.$multiple.">\n";

		if($choose=='Choose'){
			$form .= '<option value="">Please Select</option>';
		}
		//$form .= '<option value="">Please Select</option>';
        $query=$CI->db->query($sql);
        if ($query->num_rows() > 0)
        {
           foreach ($query->result_array() as $row)
           {
			   //echo "<pre>";print_r($row);
                  $values = array_values($row);
                  if (count($values)===2){
                    $key = (string) $values[0];
                    $val = (string) $values[1];
                    //$this->option($values[0], $values[1]);
                  }
				
            $sel = (in_array($val, $selected))?' selected="selected"':'';
			
				$class = (in_array($key, $selected))?' class="selected"':'';
				
                //$form .= '<option value="'.$key.'"'.$sel.'>'.$val."</option>\n";//old
				/* Encode Value */
				//$form .= '<option value="'.rawurlencode($val).'"'.$sel.' "'.$class.'>'.$val."</option>\n";
				$form .= '<option value="'.$val.'"'.$sel.' '.$class.'>'.$val."</option>\n";
				
			//	$form .= '<option value="'.$key.'"'.$sel.' "'.$class.'>'.$val."</option>\n";
           }
        }
        $form .= '</select>';
        return $form;
    }  
	
	function get_view_path($view_name)
	{
		
		$target_file= APPPATH.'modules/pdf/views/'.$view_name.'.php';
		if(file_exists($target_file)) return $target_file;
	}

function get_state_dropdown($name, $selected_val='CA', $attr=''){ 

	if($selected_val == '') $selected_val='CA'; 
	//echo $attr;
?>
	<select name="<?php echo $name; ?>"  required="required"  <?php echo $attr; ?>>
		<option <?php if($selected_val == 'AL') echo 'selected="selected"'; ?> value="AL">AL</option>
        <option <?php if($selected_val == 'AK') echo 'selected="selected"'; ?> value="AK">AK</option>
		<option  <?php if($selected_val == 'AZ') echo 'selected="selected"'; ?> value="AZ">AZ</option>
		<option  <?php if($selected_val == 'AR') echo 'selected="selected"'; ?> value="AR">AR</option>
		<option  <?php if($selected_val == 'CA') echo 'selected="selected"'; ?> value="CA">CA</option>
		<option  <?php if($selected_val == 'CO') echo 'selected="selected"'; ?> value="CO">CO</option>
		<option  <?php if($selected_val == 'CT') echo 'selected="selected"'; ?> value="CT">CT</option>
		<option  <?php if($selected_val == 'DE') echo 'selected="selected"'; ?> value="DE">DE</option>
		<option  <?php if($selected_val == 'FL') echo 'selected="selected"'; ?> value="FL">FL</option>
		<option  <?php if($selected_val == 'GA') echo 'selected="selected"'; ?> value="GA">GA</option>
		<option  <?php if($selected_val == 'HI') echo 'selected="selected"'; ?> value="HI">HI</option>
		<option  <?php if($selected_val == 'ID') echo 'selected="selected"'; ?> value="ID">ID</option>
		<option  <?php if($selected_val == 'IL') echo 'selected="selected"'; ?> value="IL">IL</option>
		<option  <?php if($selected_val == 'IN') echo 'selected="selected"'; ?> value="IN">IN</option>
		<option  <?php if($selected_val == 'IA') echo 'selected="selected"'; ?> value="IA">IA</option>
		<option  <?php if($selected_val == 'KS') echo 'selected="selected"'; ?> value="KS">KS</option>
		<option  <?php if($selected_val == 'KY') echo 'selected="selected"'; ?> value="KY">KY</option>
		<option  <?php if($selected_val == 'LA') echo 'selected="selected"'; ?> value="LA">LA</option>
		<option  <?php if($selected_val == 'ME') echo 'selected="selected"'; ?> value="ME">ME</option>
		<option  <?php if($selected_val == 'MD') echo 'selected="selected"'; ?> value="MD">MD</option>
		<option  <?php if($selected_val == 'MA') echo 'selected="selected"'; ?> value="MA">MA</option>
		<option  <?php if($selected_val == 'MI') echo 'selected="selected"'; ?> value="MI">MI</option>
		<option  <?php if($selected_val == 'MN') echo 'selected="selected"'; ?> value="MN">MN</option>
		<option  <?php if($selected_val == 'MS') echo 'selected="selected"'; ?> value="MS">MS</option>
		<option  <?php if($selected_val == 'MO') echo 'selected="selected"'; ?> value="MO">MO</option>
		<option  <?php if($selected_val == 'MT') echo 'selected="selected"'; ?> value="MT">MT</option>
		<option  <?php if($selected_val == 'NE') echo 'selected="selected"'; ?> value="NE">NE</option>
		<option  <?php if($selected_val == 'NV') echo 'selected="selected"'; ?> value="NV">NV</option>
		<option  <?php if($selected_val == 'NH') echo 'selected="selected"'; ?> value="NH">NH</option>
		<option  <?php if($selected_val == 'NJ') echo 'selected="selected"'; ?> value="NJ">NJ</option>
		<option  <?php if($selected_val == 'NM') echo 'selected="selected"'; ?> value="NM">NM</option>
		<option  <?php if($selected_val == 'NY') echo 'selected="selected"'; ?> value="NY">NY</option>
		<option  <?php if($selected_val == 'NC') echo 'selected="selected"'; ?> value="NC">NC</option>
		<option  <?php if($selected_val == 'ND') echo 'selected="selected"'; ?> value="ND">ND</option>
		<option  <?php if($selected_val == 'OH') echo 'selected="selected"'; ?> value="OH">OH</option>
		<option  <?php if($selected_val == 'OK') echo 'selected="selected"'; ?> value="OK">OK</option>
		<option  <?php if($selected_val == 'OR') echo 'selected="selected"'; ?> value="OR">OR</option>
		<option  <?php if($selected_val == 'PA') echo 'selected="selected"'; ?> value="PA">PA</option>
		<option  <?php if($selected_val == 'RI') echo 'selected="selected"'; ?> value="RI">RI</option>
		<option  <?php if($selected_val == 'SC') echo 'selected="selected"'; ?> value="SC">SC</option>
		<option  <?php if($selected_val == 'SD') echo 'selected="selected"'; ?> value="SD">SD</option>
		<option  <?php if($selected_val == 'TN') echo 'selected="selected"'; ?> value="TN">TN</option>
		<option  <?php if($selected_val == 'TX') echo 'selected="selected"'; ?> value="TX">TX</option>
		<option  <?php if($selected_val == 'UT') echo 'selected="selected"'; ?> value="UT">UT</option>
		<option  <?php if($selected_val == 'VT') echo 'selected="selected"'; ?> value="VT">VT</option>
		<option  <?php if($selected_val == 'VA') echo 'selected="selected"'; ?> value="VA">VA</option>
		<option  <?php if($selected_val == 'WA') echo 'selected="selected"'; ?> value="WA">WA</option>
		<option  <?php if($selected_val == 'WV') echo 'selected="selected"'; ?> value="WV">WV</option>
		<option  <?php if($selected_val == 'WI') echo 'selected="selected"'; ?> value="WI">WI</option>
		<option  <?php if($selected_val == 'WY') echo 'selected="selected"'; ?> value="WY">WY</option>
	</select>
            
<?php }   
function get_tractor_count($quote_id){
		
		// get instance of codeigniter
		$CI =& get_instance();
                                    // select amount
                                    $CI->db->select('vehicle_id');

                                    $CI->db->where("vehicle_type", 'TRACTOR');
                                    $CI->db->where("quote_id", $quote_id);

                                    // from table mob_transaction
                                    $CI->db->from("rq_rqf_quote_vehicle");
		 $query =  $CI->db->get();
                 
	   
		return $query->num_rows();
}

function get_vehicle_count($quote_id='',$vehicle_type=''){
		
		// get instance of codeigniter
		$CI =& get_instance();
                                    // select amount
                                    $CI->db->select('vehicle_id');

                                    $CI->db->where("vehicle_type", $vehicle_type);
                                    $CI->db->where("quote_id", $quote_id);

                                    // from table mob_transaction
                                    $CI->db->from("rq_rqf_quote_vehicle");
		 $query =  $CI->db->get();
                 
	   
		return $query->num_rows();
}

function get_trailer_count($parent_id=''){
		
		// get instance of codeigniter
		$CI =& get_instance();
                                    // select amount
                                    $CI->db->select('vehicle_id');

                               //     $CI->db->where("vehicle_type", $vehicle_type);
                                    $CI->db->where("parent_id", $parent_id);

                                    // from table mob_transaction
                                    $CI->db->from("rq_rqf_quote_vehicle");
		 $query =  $CI->db->get();
                 
	   
		return $query->num_rows();
}

function get_mail_from_id(){
	// get instance of codeigniter
		$CI =& get_instance();
		// select amount
		$CI->db->select('id, name, value, status');	
	
		$CI->db->where("status", 1);	
		
		$CI->db->from($CI->config->item('config_table'));
		$query =  $CI->db->get();     
	   
		$result = $query->result();
		//print_r($result);
		
		foreach($result as $data)
		{
			if(($data->name=='Support Email')&&($data->status==1))
			{
				return $data->value;
				break;
			}
			else if(($data->name=='Supervisor Mail')&&($data->status==1)){			
				return $data->value;
				break;
			}
			else
			{
				break;
			}
		}
}
function get_active_catlog_dropdown($selected='',$attr='',$field_name,$id=''){ 
		
		
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->where('as_catlog', '1');
		$CI->db->where('status', '1');
		$CI->db->from('rq_rqf_quote_catlog_type');
		
		$CI->db->order_by('catlog_type', 'asc');
		$query = $CI->db->get();
		//echo $this->db->last_query(); die;
		if($query->num_rows() > 0) {
			$catlog_data = $query->result();
		} else {
			$catlog_data = '';
		}
		$res = '';
		$res .='<select type="text" id="'.$id.'" name="'.$field_name.'[]" '.$attr.' >';
		foreach ($catlog_data as $catlogData){ 
				//$select_this = ($selected == $catlogData->catlog_value) ? 'selected="selected"' : '';
				if($catlogData->id == $selected){
					 $select_this = 'selected="selected"';
				}else{
					$select_this = '';
				}
				//if(!empty($select_this)){ $selec_opt = 1; }
				$res .='<option '.$select_this.' value="'.$catlogData->id.'">'.$catlogData->catlog_type.'</option>';
				
			}
		$res .='</select>';
		return $res;
	
	}
function getMailContent($type)
{
	$CI =& get_instance();
	$CI->db->select('id,sub,msg');
	$CI->db->where('delete', '0');
	$CI->db->where('status', '1');
	$CI->db->where('key', $type);
	$CI->db->from('rq_config');	
	
	$query = $CI->db->get();
	//echo $this->db->last_query(); die;
	if($query->num_rows() > 0) {
		$config_data = $query->row();
	} else {
		$config_data = '';
	}
	
	return $config_data;
	/*print_r($config_data);*/	
}
function restrict(){

}
function get_ref_id($id)
{
	$CI =& get_instance();
	$CI->db->select('quote_ref');
	$CI->db->from('rq_rqf_quote_bundle');
	$CI->db->where('quote_id', $id);
	$query = $CI->db->get();
	$result = $query->row();
	if(isset($result->quote_ref)){
		return $result->quote_ref;
	}
}
function get_agency_detail($id, $field, $where='id')
{
	$CI =& get_instance();
	$CI->db->select($field);
	$CI->db->from('rq_members');
	$CI->db->where($where, $id);
	$query = $CI->db->get();
	$result = $query->row();
	if(isset($result->$field)){
		return $result->$field;
	}
}
?>
