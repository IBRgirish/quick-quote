<?php  
//-****** FILE /system/application/helpers/form_helper.php code ********-->

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Drop-down Menu FROM database
 *
 * @access    public
 * @param    string
 * @param    array
 * @param    string
 * @param    string
 * @return    string
 */

ob_start();

   function form_simple_dropdown_from_db_valuebyname($name = '', $sql, $selected = array(),$choose='', $extra = '')
    {
        $CI =& get_instance();
        if ( ! is_array($selected))
        {
            $selected = array($selected);
        }
		
        // If no selected state was submitted we will attempt to set it automatically
        if (count($selected) === 0)
        {
            // If the form name appears in the $_POST array we have a winner!
            if (isset($_POST[$name]))
            {
                $selected = array($_POST[$name]);
            }
        }

        if ($extra != '') $extra = ' '.$extra;

        $multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

        $form = '<select id="'.$name.'" name="'.$name.'"'.$extra.$multiple.">\n";

		if($choose=='Choose'){
			$form .= '<option value="">Please Select</option>';
		}
		//$form .= '<option value="">Please Select</option>';
        $query=$CI->db->query($sql);
        if ($query->num_rows() > 0)
        {
           foreach ($query->result_array() as $row)
           {
			   //echo "<pre>";print_r($row);
                  $values = array_values($row);
                  if (count($values)===2){
                    $key = (string) $values[0];
                    $val = (string) $values[1];
                    //$this->option($values[0], $values[1]);
                  }
				
            $sel = (in_array($val, $selected))?' selected="selected"':'';
			
				$class = (in_array($key, $selected))?' class="selected"':'';
				
                //$form .= '<option value="'.$key.'"'.$sel.'>'.$val."</option>\n";//old
				/* Encode Value */
				//$form .= '<option value="'.rawurlencode($val).'"'.$sel.' "'.$class.'>'.$val."</option>\n";
				$form .= '<option value="'.$val.'"'.$sel.' '.$class.'>'.$val."</option>\n";
				
			//	$form .= '<option value="'.$key.'"'.$sel.' "'.$class.'>'.$val."</option>\n";
           }
        }
        $form .= '</select>';
        return $form;
    }  
	
	
	
	  function form_simple_dropdown_from_db_valuebyname1($name = '', $sql, $selected = array(),$choose='', $extra = '')
    {
        $CI =& get_instance();
        if (!is_array($selected))
        {
            $selected = array($selected);
        }
		
        // If no selected state was submitted we will attempt to set it automatically
        if (count($selected) === 0)
        {
            // If the form name appears in the $_POST array we have a winner!
            if (isset($_POST[$name]))
            {
                $selected = array($_POST[$name]);
            }
        }

        if ($extra != '') $extra = ' '.$extra;

        $multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

        $form = '<select id="'.$name.'" name="'.$name.'"'.$extra.$multiple.">\n";

		if($choose=='Choose'){
			$form .= '<option value="">Please Select</option>';
		}
		//$form .= '<option value="">Please Select</option>';
        $query=$CI->db->query($sql);
        if ($query->num_rows() > 0)
        {
           foreach ($query->result_array() as $row)
           {
			   //echo "<pre>";print_r($row);
                  $values = array_values($row);
                  if (count($values)===2){
                    $key = (string) $values[0];
                    $val = (string) $values[1];
                    //$this->option($values[0], $values[1]);
                  }
				//print_r($selected);
            $sel = (in_array($val, $selected))?' selected="selected"':'';
			
				$class = (in_array($key, $selected))?' class="selected"':'';
				
                //$form .= '<option value="'.$key.'"'.$sel.'>'.$val."</option>\n";//old
				/* Encode Value */
				//$form .= '<option value="'.rawurlencode($val).'"'.$sel.' "'.$class.'>'.$val."</option>\n";
				$form .= '<option value="'.$val.'"'.$sel.' '.$class.'>'.$val."</option>\n";
				
			//	$form .= '<option value="'.$key.'"'.$sel.' "'.$class.'>'.$val."</option>\n";
           }
        }
        $form .= '</select>';
        return $form;
    }  
	
	
	
	function get_view_path($view_name)
	{
		
		$target_file= APPPATH.'modules/pdf/views/'.$view_name.'.php';
		if(file_exists($target_file)) return $target_file;
	}

function get_state_dropdown($name, $selected_val='CA', $attr=''){ 

	if($selected_val == '') $selected_val='CA'; 
	//echo $attr;
?>
	<select name="<?php echo $name; ?>"  required="required"  <?php echo $attr; ?>>
		<option <?php if($selected_val == 'AL') echo 'selected="selected"'; ?> value="AL">AL</option>
        <option <?php if($selected_val == 'AK') echo 'selected="selected"'; ?> value="AK">AK</option>
		<option  <?php if($selected_val == 'AZ') echo 'selected="selected"'; ?> value="AZ">AZ</option>
		<option  <?php if($selected_val == 'AR') echo 'selected="selected"'; ?> value="AR">AR</option>
		<option  <?php if($selected_val == 'CA') echo 'selected="selected"'; ?> value="CA">CA</option>
		<option  <?php if($selected_val == 'CO') echo 'selected="selected"'; ?> value="CO">CO</option>
		<option  <?php if($selected_val == 'CT') echo 'selected="selected"'; ?> value="CT">CT</option>
		<option  <?php if($selected_val == 'DE') echo 'selected="selected"'; ?> value="DE">DE</option>
		<option  <?php if($selected_val == 'FL') echo 'selected="selected"'; ?> value="FL">FL</option>
		<option  <?php if($selected_val == 'GA') echo 'selected="selected"'; ?> value="GA">GA</option>
		<option  <?php if($selected_val == 'HI') echo 'selected="selected"'; ?> value="HI">HI</option>
		<option  <?php if($selected_val == 'ID') echo 'selected="selected"'; ?> value="ID">ID</option>
		<option  <?php if($selected_val == 'IL') echo 'selected="selected"'; ?> value="IL">IL</option>
		<option  <?php if($selected_val == 'IN') echo 'selected="selected"'; ?> value="IN">IN</option>
		<option  <?php if($selected_val == 'IA') echo 'selected="selected"'; ?> value="IA">IA</option>
		<option  <?php if($selected_val == 'KS') echo 'selected="selected"'; ?> value="KS">KS</option>
		<option  <?php if($selected_val == 'KY') echo 'selected="selected"'; ?> value="KY">KY</option>
		<option  <?php if($selected_val == 'LA') echo 'selected="selected"'; ?> value="LA">LA</option>
		<option  <?php if($selected_val == 'ME') echo 'selected="selected"'; ?> value="ME">ME</option>
		<option  <?php if($selected_val == 'MD') echo 'selected="selected"'; ?> value="MD">MD</option>
		<option  <?php if($selected_val == 'MA') echo 'selected="selected"'; ?> value="MA">MA</option>
		<option  <?php if($selected_val == 'MI') echo 'selected="selected"'; ?> value="MI">MI</option>
		<option  <?php if($selected_val == 'MN') echo 'selected="selected"'; ?> value="MN">MN</option>
		<option  <?php if($selected_val == 'MS') echo 'selected="selected"'; ?> value="MS">MS</option>
		<option  <?php if($selected_val == 'MO') echo 'selected="selected"'; ?> value="MO">MO</option>
		<option  <?php if($selected_val == 'MT') echo 'selected="selected"'; ?> value="MT">MT</option>
		<option  <?php if($selected_val == 'NE') echo 'selected="selected"'; ?> value="NE">NE</option>
		<option  <?php if($selected_val == 'NV') echo 'selected="selected"'; ?> value="NV">NV</option>
		<option  <?php if($selected_val == 'NH') echo 'selected="selected"'; ?> value="NH">NH</option>
		<option  <?php if($selected_val == 'NJ') echo 'selected="selected"'; ?> value="NJ">NJ</option>
		<option  <?php if($selected_val == 'NM') echo 'selected="selected"'; ?> value="NM">NM</option>
		<option  <?php if($selected_val == 'NY') echo 'selected="selected"'; ?> value="NY">NY</option>
		<option  <?php if($selected_val == 'NC') echo 'selected="selected"'; ?> value="NC">NC</option>
		<option  <?php if($selected_val == 'ND') echo 'selected="selected"'; ?> value="ND">ND</option>
		<option  <?php if($selected_val == 'OH') echo 'selected="selected"'; ?> value="OH">OH</option>
		<option  <?php if($selected_val == 'OK') echo 'selected="selected"'; ?> value="OK">OK</option>
		<option  <?php if($selected_val == 'OR') echo 'selected="selected"'; ?> value="OR">OR</option>
		<option  <?php if($selected_val == 'PA') echo 'selected="selected"'; ?> value="PA">PA</option>
		<option  <?php if($selected_val == 'RI') echo 'selected="selected"'; ?> value="RI">RI</option>
		<option  <?php if($selected_val == 'SC') echo 'selected="selected"'; ?> value="SC">SC</option>
		<option  <?php if($selected_val == 'SD') echo 'selected="selected"'; ?> value="SD">SD</option>
		<option  <?php if($selected_val == 'TN') echo 'selected="selected"'; ?> value="TN">TN</option>
		<option  <?php if($selected_val == 'TX') echo 'selected="selected"'; ?> value="TX">TX</option>
		<option  <?php if($selected_val == 'UT') echo 'selected="selected"'; ?> value="UT">UT</option>
		<option  <?php if($selected_val == 'VT') echo 'selected="selected"'; ?> value="VT">VT</option>
		<option  <?php if($selected_val == 'VA') echo 'selected="selected"'; ?> value="VA">VA</option>
		<option  <?php if($selected_val == 'WA') echo 'selected="selected"'; ?> value="WA">WA</option>
		<option  <?php if($selected_val == 'WV') echo 'selected="selected"'; ?> value="WV">WV</option>
		<option  <?php if($selected_val == 'WI') echo 'selected="selected"'; ?> value="WI">WI</option>
		<option  <?php if($selected_val == 'WY') echo 'selected="selected"'; ?> value="WY">WY</option>
	</select>
            
<?php }   
function get_tractor_count($quote_id){
		
		// get instance of codeigniter
		$CI =& get_instance();
                                    // select amount
                                    $CI->db->select('vehicle_id');

                                    $CI->db->where("vehicle_type", 'TRACTOR');
                                    $CI->db->where("quote_id", $quote_id);

                                    // from table mob_transaction
                                    $CI->db->from("rq_rqf_quote_vehicle");
		 $query =  $CI->db->get();
                 
	   
		return $query->num_rows();
}

function get_vehicle_count($quote_id='',$vehicle_type=''){
		
		// get instance of codeigniter
		$CI =& get_instance();
                                    // select amount
                                    $CI->db->select('vehicle_id');

                                    $CI->db->where("vehicle_type", $vehicle_type);
                                    $CI->db->where("quote_id", $quote_id);

                                    // from table mob_transaction
                                    $CI->db->from("rq_rqf_quote_vehicle");
		 $query =  $CI->db->get();
                 
	   
		return $query->num_rows();
}

function get_trailer_count($parent_id=''){
		
		// get instance of codeigniter
		$CI =& get_instance();
                                    // select amount
                                    $CI->db->select('vehicle_id');

                               //     $CI->db->where("vehicle_type", $vehicle_type);
                                    $CI->db->where("parent_id", $parent_id);

                                    // from table mob_transaction
                                    $CI->db->from("rq_rqf_quote_vehicle");
		 $query =  $CI->db->get();
                 
	   
		return $query->num_rows();
}

function get_mail_from_id(){
	// get instance of codeigniter
		$CI =& get_instance();
		// select amount
		$CI->db->select('id, name, value, status');	
	
		$CI->db->where("status", 1);	
		
		$CI->db->from($CI->config->item('config_table'));
		$query =  $CI->db->get();     
	   
		$result = $query->result();
		//print_r($result);
		
		foreach($result as $data)
		{
			if(($data->name=='Support Email')&&($data->status==1))
			{
				return $data->value;
				break;
			}
			else if(($data->name=='Supervisor Mail')&&($data->status==1)){			
				return $data->value;
				break;
			}
			else
			{
				break;
			}
		}
}
function get_active_catlog_dropdown($selected='',$attr='',$field_name,$id=''){ 
		
		
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->where('as_catlog', '1');
		$CI->db->where('status', '1');
		$CI->db->from('rq_rqf_quote_catlog_type');
		
		$CI->db->order_by('catlog_type', 'asc');
		$query = $CI->db->get();
		//echo $this->db->last_query(); die;
		if($query->num_rows() > 0) {
			$catlog_data = $query->result();
		} else {
			$catlog_data = '';
		}
		$res = '';
		$res .='<select type="text" id="'.$id.'" name="'.$field_name.'[]" '.$attr.' >';
		foreach ($catlog_data as $catlogData){ 
				//$select_this = ($selected == $catlogData->catlog_value) ? 'selected="selected"' : '';
				if($catlogData->id == $selected){
					 $select_this = 'selected="selected"';
				}else{
					$select_this = '';
				}
				//if(!empty($select_this)){ $selec_opt = 1; }
				$res .='<option '.$select_this.' value="'.$catlogData->id.'">'.$catlogData->catlog_type.'</option>';
				
			}
		$res .='</select>';
		return $res;
	
	}
	
	
	function get_active_carriers_dropdown($selected='',$attr='',$field_name,$id=''){ 
		
		
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->where('status', '1');
		$CI->db->from('carriers');		
		$CI->db->order_by('carrier_id', 'asc');
		$query = $CI->db->get();
		if($query->num_rows() > 0) {
			$catlog_data = $query->result();
		} else {
			$catlog_data = '';
		}
		$res = '';
		$res .='<select type="text" id="'.$id.'" name="'.$field_name.'" '.$attr.' >';
		foreach ($catlog_data as $catlogData){ 
		'';
				if($catlogData->carrier_id == $selected){
					 $select_this = 'selected="selected"';
				}else{
					$select_this = '';
				}
				//if(!empty($select_this)){ $selec_opt = 1; }
				$res .='<option '.$select_this.' value="'.$catlogData->carrier_id.'">'.$catlogData->carrier_value.'</option>';
				
			}
		$res .='</select>';
		return $res;
	
	}
	
		
	function get_state_carriers_dropdown($selected='',$attr='',$field_name,$id=''){ 
		
		
		$CI =& get_instance();
		$CI->db->select('*');		
		$CI->db->from('real_state');		
		$CI->db->order_by('id', 'asc');
		$query = $CI->db->get();
		if($query->num_rows() > 0) {
			$catlog_data = $query->result();
		} else {
			$catlog_data = '';
		}
		$res = '';
		$res .='<select type="text" id="'.$id.'" name="'.$field_name.'" '.$attr.' >';
		foreach ($catlog_data as $catlogData){ 
		'';
				if($catlogData->id == $selected){
					 $select_this = 'selected="selected"';
				}else{
					$select_this = '';
				}
				//if(!empty($select_this)){ $selec_opt = 1; }
				$res .='<option '.$select_this.' value="'.$catlogData->id.'">'.$catlogData->state_name.'</option>';
				
			}
		$res .='</select>';
		return $res;
	
	}
	
	
	
function getMailContent($type)
{
	$CI =& get_instance();
	$CI->db->select('id,sub,msg');
	$CI->db->where('delete', '0');
	$CI->db->where('status', '1');
	$CI->db->where('key', $type);
	$CI->db->from('rq_config');	
	
	$query = $CI->db->get();
	//echo $this->db->last_query(); die;
	if($query->num_rows() > 0) {
		$config_data = $query->row();
	} else {
		$config_data = '';
	}
	
	return $config_data;
	/*print_r($config_data);*/	
}
function restrict(){

}
function get_ref_id($id)
{
	$CI =& get_instance();
	$CI->db->select('quote_ref');
	$CI->db->from('rq_rqf_quote_bundle');
	$CI->db->where('quote_id', $id);
	$query = $CI->db->get();
	$result = $query->row();
	if(isset($result->quote_ref)){
		return $result->quote_ref;
	}
}
function get_agency_detail($id, $field, $where='id')
{

	$CI =& get_instance();
	$CI->db->select($field);
	$CI->db->from('rq_members');
	$CI->db->where($where, $id);
	$query = $CI->db->get();
	$result = $query->row();
	if(isset($result->$field)){
		return $result->$field;
	}
	
	}
	
function get_agency_details($id, $field, $where='id')
{

	$CI =& get_instance();
	$CI->db->select($field);
	$CI->db->from('rq_rqf_insured_info');
	$CI->db->where($where, $id);
	$query = $CI->db->get();
	$result = $query->row();
	if(isset($result->$field)){
		return $result->$field;
	}
	
	}
	
	function get_underwriter_detail($id, $field, $where='id'){
		
	$CI =& get_instance();
	$CI->db->select($field);
	$CI->db->from('rq_underwriter');
	$CI->db->where($where, $id);
	$query = $CI->db->get();
	$result = $query->row();
	if(isset($result->$field)){
		return $result->$field;
	}
		
  }
	
	
 function get_underwriter_details($ids, $field, $where='id'){
	 
 if($ids!=''){
	$id = explode(',',$ids);
	for($i=0;$i<count($id);$i++){
	$CI =& get_instance();
	$CI->db->select($field);
	$CI->db->from('rq_underwriter');
	$CI->db->where($where, $id[$i]);
	$query = $CI->db->get();
	$results[] = $query->row();
	
	 }
	
	 if(!empty($results)){
		 foreach($results as $result)
	    	 {
			       $val[]=$result->name;
			 }
		return implode(',',$val);
	  }
	
   }
  }
	
	
	
function get_catlog_dropdown_company($field_name, $catlog_type='', $attr='', $other_val=0,  $module_name ='', $selected='',$otr_relative_field='', $otr_relative_field_attr='', $roi_state='', $roi_state_selected='', $other_selected='', $loop_count=''){ 
		
		$CI =& get_instance();
		if(!empty($catlog_type)){
			$CI->db->where('rq_rqf_quote_catlog_type.catlog_type', $catlog_type);
		}
		if(!empty($module_name)){
			$CI->db->where('module_name', $module_name);
		}
		$CI->db->where('rq_rqf_quote_catlog_type.status', 1);
		
		$CI->db->select('rq_rqf_quote_catlogs.catlog_value,rq_rqf_quote_catlogs.Company_logo');
		$CI->db->select('rq_rqf_quote_catlogs.is_catalog');
		$CI->db->select('rq_rqf_quote_catlogs.status');
	
		
		
		$CI->db->from('rq_rqf_quote_catlogs');
		$CI->db->join('rq_rqf_quote_catlog_type', 'rq_rqf_quote_catlogs.catlog_type = rq_rqf_quote_catlog_type.id', 'inner');
		$CI->db->order_by('rq_rqf_quote_catlog_type.catlog_type', 'asc');
		
		$CI->db->where('rq_rqf_quote_catlogs.status','1');
		
		$query = $CI->db->get(); 
		
		if($query->num_rows() > 0) {
			$catlog_data = $query->result();
		} else {
			$catlog_data = '';
		}
		
		$catlog ='';
		$catlog .='<select type="text" id="'.$catlog_type.$loop_count.'" name="'.$field_name.'" '.$attr.' >';
		
			$catlog .='<option value="">Select</option>';
			//$select_this = '';
			$selec_opt = '';
			
		//$catlog_data = $this->quote_model->get_catlog_options('commodities_haulted');
		$sel_array = $selected;
		
		if(!is_array($selected) && !empty($selected)){
			$sel_array = array($selected);		
			//print_r($sel_array);	
		}
		if(empty($sel_array)) { $sel_array = array(); }
		$cat_val = array();
		//print_r($catlog_data);
		
		if($catlog_data)
		{
			foreach ($catlog_data as $catlogData){ 
				//$select_this = ($selected == $catlogData->catlog_value) ? 'selected="selected"' : '';
				if(in_array($catlogData->catlog_value, $sel_array)){
					 $select_this = 'selected="selected"';
				}else{
					$select_this = '';
				}
				//if(!empty($select_this)){ $selec_opt = 1; }
				if($catlogData->Company_logo == '')
				{
				$catlogData->Company_logo = base_url('uploads').'/logo/images.jpg';
				}
				else
				{
				$catlogData->Company_logo = base_url('uploads').'/companylogo/'.$catlogData->Company_logo;
				}
				$catlog .='<option '.$select_this.' value="'.$catlogData->catlog_value.'" data-image="'.$catlogData->Company_logo.'">'.$catlogData->catlog_value.'</option>';
				$cat_val[] = $catlogData->catlog_value;
			}
		}
		$otr_value = '';
		if($other_val) { 
			$select_otr = '';
			if(!empty($sel_array)){
				foreach($sel_array as $sel_item){
					if(!in_array($sel_item, $cat_val)){
						$select_otr = 'selected="selected"'; 
						$otr_value = $sel_item;
					}
				}
			}
			//$arr_mer = array_merge($cat_val, $sel_array);
			//$diff = array_diff($cat_val, $arr_mer); 
			/* if((!$selec_opt && !empty($selected)) || !empty($diff)){
				$select_otr = 'selected="selected"'; 
			} */
				
		}	
		$catlog .='</select>';
		if($otr_relative_field){
			if($otr_value) { $style_attr = '';} else { $style_attr = ''; }
				if($otr_relative_field_attr == 1){ $status_rad_of_ope = 1;  $style_roo_br = ''; $style_span = ''; $style_other='span8';} else { $style_roo_br = ''; $status_rad_of_ope = '';  $style_span ='span2'; $style_other='span10'; }
			
			$catlog .= '</div><div class="'.$style_span.' '.$field_name.$loop_count.'_relative_section relative" '.$style_attr.' >';
			if(!$status_rad_of_ope){
			$catlog .= '<label>Specify other</label>';
			}
			$catlog .= '<input class="textinput pull-left '.$style_other.' '.$field_name./*$loop_count.*/'_relative" type="text" id="ren_no_" placeholder="specify other" name="'.$field_name.'_other[]" value="'.$other_selected.'">';
					 if($roi_state){
						if($roi_state_selected){
							$broker_state = $roi_state_selected;
							$class_new = 'chzn-select-other-1';
						}else{						
							//$broker_state = 'CA';
							$broker_state = '';
							$class_new = 'chzn-select-other';
						}
						
						//$broker_state ='CA,AL';
						if(!$status_rad_of_ope){
							$attr = 'multiple class="chzn-select span4"';
						}else{
							$attr = 'multiple class="'.$class_new.' span12"';
						}
						
						$catlog .= '</div>'.$style_roo_br.'<div class="'.$style_span.' '.$field_name.$loop_count.'_relative_section relative" '.$style_attr.' >';
						
						if(!$status_rad_of_ope){
						$catlog .= '<label>Select State</label>'; 
						}
						$catlog .= $style_roo_br;
						//$catlog .= get_state_dropdown($field_name.'_state'.$loop_count.'[]', $broker_state, $attr);
					 }
		}
		
		
		return $catlog;
		
	
}

function getCountryDropDown1($name='', $att='', $c_name='', $selected='')
{
	$option = '';
	$CI =& get_instance();
	$CI->db->select('*');
    $CI->db->from('country');	
	$CI->db->order_by('country_code', 'asc');
   
	$query = $CI->db->get();
    
    $result = $query->result();
	
	if($c_name!='')
	{
		$n = 1;
	}
	foreach($result as $reidd)
	{
		//echo $selected;
		if($reidd->country_code==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		$option .= '<option value="'.$reidd->country_code.'" '.$select.'>'.$reidd->country_code.'</option>';
	}
	echo '<select name="'.$name.'" '.$att.'>
		<option value="">Select</option>
		'.$option.'
	</select>';
}

function getpip($name='', $att='',  $selected='',$state='',$c_name='')
{
       $CI =& get_instance(); 
		$CI->db->select("b.default_value,b.catlog_type");
		$CI->db->join('pip_state b', 'b.id_state=a.id');	
        $CI->db->from('real_state a');
	    $CI->db->like('a.state_name',$state);
	    $CI->db->where('b.status',1);
		$CI->db->distinct();
		$query = $CI->db->get();
	    $result= $query->result(); 
	$option = '';
	
	if($c_name!='')
	{
		$n = 1;
	}
	
	foreach($result as $reidd)
	{
		if($reidd->catlog_type==26){
		if($reidd->default_value==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		
		
		$option .= '<option value="'.$reidd->default_value.'" '.$select.'>'.$reidd->default_value.'</option>';
	 }
	}
	
	$selects='';
	 if($selected=='Rejected'){
	   $selects = 'selected';	
	  }
	echo '<select name="'.$name.'" '.$att.'>
		<option value="Rejected" '.$selects.'>Rejected</option>
		'.$option.'		
	</select>';
}

function getpipcoverages($name='', $att='',  $selected='',$state='',$names='',$c_name='')
{        
         $CI =& get_instance();    	   	
		 $CI->db->select("a.catlog_type,a.value");
		 $CI->db->join('pip_carrier_value b', 'b.id_pip_value=a.id_pip_value');
		 $CI->db->join('pip_carrier c', 'c.id_pip_carrier=b.id_pip_carrier');
		 $CI->db->join('carriers d', 'd.carrier_id=c.id_carrier');
		 $CI->db->join('pip_state_carrier e', 'e.id_pip_carrier=c.id_pip_carrier');
		 $CI->db->join('pip_state f', 'f.id_pip_state=e.id_pip_state');
		 $CI->db->join('real_state g', 'g.id=f.id_state');
		 $CI->db->from('pip_value a');
		 $CI->db->like('g.state_name',$state);	
		 if($names!=''){
		 $CI->db->like('d.carrier_value',$names);
		 }
		 $CI->db->where('a.status',1);
		 $CI->db->where('a.is_delete',0);
		 $CI->db->distinct();
		$query = $CI->db->get();
	    $result= $query->result(); 
	$option = '';
	
	if($c_name!='')
	{
		$n = 1;
	}
	
	foreach($result as $reidd)
	{
		if($reidd->catlog_type==26){
		if($reidd->value==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		
		
		$option .= '<option value="'.$reidd->value.'" '.$select.'>'.$reidd->value.'</option>';
		}
	}
	
	$selects='';
	 if($selected=='Rejected'){
	   $selects = 'selected';	
	  }
	echo '<select name="'.$name.'" '.$att.'>
		<option value="Rejected" '.$selects.'>Rejected</option>
		'.$option.'		
	</select>';
}

function getumcoverages($name='', $att='',  $selected='',$state='',$names='',$c_name='')
{     
         $CI =& get_instance();    	   	
		 $CI->db->select("a.catlog_type,a.value");
		 $CI->db->join('pip_carrier_value b', 'b.id_pip_value=a.id_pip_value');
		 $CI->db->join('pip_carrier c', 'c.id_pip_carrier=b.id_pip_carrier');
		 $CI->db->join('carriers d', 'd.carrier_id=c.id_carrier');
		 $CI->db->join('pip_state_carrier e', 'e.id_pip_carrier=c.id_pip_carrier');
		 $CI->db->join('pip_state f', 'f.id_pip_state=e.id_pip_state');
		 $CI->db->join('real_state g', 'g.id=f.id_state');
		 $CI->db->from('pip_value a');
		 $CI->db->like('g.state_name',$state);	
		  if($names!=''){
		 $CI->db->like('d.carrier_value',$names);
		 }
		 $CI->db->where('a.status',1);
		 $CI->db->where('a.is_delete',0);
		 $CI->db->distinct();
		$query = $CI->db->get();
	    $result= $query->result(); 
		
		
	$option = '';
	
	if($c_name!='')
	{
		$n = 1;
	}
	
	foreach($result as $reidd)
	{
		if($reidd->catlog_type==27){
		if($reidd->value==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		
		
		$option .= '<option value="'.$reidd->value.'" '.$select.'>'.$reidd->value.'</option>';
		}
	}
	
	$selects='';
	 if($selected=='Rejected'){
	   $selects = 'selected';	
	  }
	echo '<select name="'.$name.'" '.$att.'>
		<option value="Rejected" '.$selects.'>Rejected</option>
		'.$option.'		
	</select>';
}

function getum($name='', $att='',  $selected='',$state='',$c_name='')
{      $CI =& get_instance();
       
		$CI->db->select("b.default_value,b.catlog_type");
		$CI->db->join('pip_state b', 'b.id_state=a.id');	
        $CI->db->from('real_state a');
	    $CI->db->like('a.state_name',$state);
	    $CI->db->where('b.status',1);
		$CI->db->distinct();
		$query = $CI->db->get();
	    $result= $query->result(); 
		
		 
	$option = '';
	
	if($c_name!='')
	{
		$n = 1;
	}
	foreach($result as $reidd)
	{
		if($reidd->catlog_type==27){
		if($reidd->default_value==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		
		
		$option .= '<option value="'.$reidd->default_value.'" '.$select.'>'.$reidd->default_value.'</option>';
	 }
	}
	$selects='';
	 if($selected=='Rejected'){
	   $selects = 'selected';	
	  }
	echo '<select name="'.$name.'" '.$att.'>
		<option value="Rejected" '.$selects.'>Rejected</option>
		'.$option.'		
	</select>';
	
	
}

function getuim($name='', $att='',  $selected='',$state='',$c_name='')
{
	    $CI =& get_instance();   
		$CI->db->select("b.default_value,b.catlog_type");
		$CI->db->join('pip_state b', 'b.id_state=a.id');
		$CI->db->from('real_state a');
		$CI->db->like('a.state_name',$state);
        $CI->db->distinct();
	    $query = $CI->db->get();
	    $result= $query->result();
		
		 
	$option = '';
	
	if($c_name!='')
	{
		$n = 1;
	}
	foreach($result as $reidd)
	{
		if($reidd->catlog_type==28){
		if($reidd->default_value==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		
		
		$option .= '<option value="'.$reidd->default_value.'" '.$select.'>'.$reidd->default_value.'</option>';
	 }
	}

	$selects='';
	 if($selected=='Rejected'){
	   $selects = 'selected';	
	  }
	echo '<select name="'.$name.'" '.$att.'>
		<option value="Rejected" '.$selects.'>Rejected</option>
		'.$option.'		
	</select>';
}
function getuimcoverages($name='', $att='',  $selected='',$state='',$names='',$c_name='')
{        
         $CI =& get_instance();    	   	
		 $CI->db->select("a.catlog_type,a.value");
		 $CI->db->join('pip_carrier_value b', 'b.id_pip_value=a.id_pip_value');
		 $CI->db->join('pip_carrier c', 'c.id_pip_carrier=b.id_pip_carrier');
		 $CI->db->join('carriers d', 'd.carrier_id=c.id_carrier');
		 $CI->db->join('pip_state_carrier e', 'e.id_pip_carrier=c.id_pip_carrier');
		 $CI->db->join('pip_state f', 'f.id_pip_state=e.id_pip_state');
		 $CI->db->join('real_state g', 'g.id=f.id_state');
		 $CI->db->from('pip_value a');
		 $CI->db->like('g.state_name',$state);	
		 if($names!=''){
		 $CI->db->like('d.carrier_value',$names);
		 }
		 $CI->db->where('a.status',1);
		 $CI->db->where('a.is_delete',0);
		 $CI->db->distinct();
		 $query = $CI->db->get();
	     $result= $query->result(); 
		
		
	$option = '';
	
	if($c_name!='')
	{
		$n = 1;
	}
	
	foreach($result as $reidd)
	{
		if($reidd->catlog_type==28){
		if($reidd->value==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		
		
		$option .= '<option value="'.$reidd->value.'" '.$select.'>'.$reidd->value.'</option>';
		}
	}
	
	$selects='';
	 if($selected=='Rejected'){
	   $selects = 'selected';	
	  }
	echo '<select name="'.$name.'" '.$att.'>
		<option value="Rejected" '.$selects.'>Rejected</option>
		'.$option.'		
	</select>';
}


function getMakeModelVehicles($name='', $att='',  $selected='',$catlog_type='',$c_name='')
{
	    $CI =& get_instance();   
		$CI->db->select("catlog_value");		
		$CI->db->where('catlog_type',$catlog_type);
		$CI->db->where('status',1);
		$CI->db->order_by('catlog_value','ASC');
		$CI->db->from('rq_rqf_quote_catlogs');
        $CI->db->distinct();
	    $query = $CI->db->get();
	    $result= $query->result();
	$selects='';	
		 
	$option = '';
	
	if($c_name!='')
	{
		$n = 1;
	}
	foreach($result as $reidd)
	{
		
		if($reidd->catlog_value==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		
		
		$option .= '<option value="'.$reidd->catlog_value.'" '.$select.'>'.$reidd->catlog_value.'</option>';
	 }
	 $selects = '';
	if($catlog_type!=51){
		$selects='<option value="">Select</option>';
		}	
	echo '<select name="'.$name.'" '.$att.'>
	         '.$selects.'	   	
		      '.$option.'		
	   </select>';
}

function getequipVehicles($name='', $att='',  $selected='',$catlog_type='',$c_name='')
{
	    $CI =& get_instance();   
		$CI->db->select("catlog_value,module_name");		
		$CI->db->where('catlog_type',$catlog_type);
		$CI->db->where('status',1);
		$CI->db->order_by('catlog_value','ASC');
		$CI->db->from('rq_rqf_quote_catlogs');
        $CI->db->distinct();
	    $query = $CI->db->get();
	    $result= $query->result();
		
		 
	$option = '';
	
	if($c_name!='')
	{
		$n = 1;
	}
	foreach($result as $reidd)
	{
		
		if($reidd->catlog_value==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		
	  $catlogData->Company_logo = base_url('uploads').'/file/'.$reidd->module_name;			
	  
				
	  $option .= '<option value="'.$reidd->catlog_value.'" '.$select.' data-image="'.$catlogData->Company_logo.'" >'.$reidd->catlog_value.'</option>';
	 }
		
	echo '<select name="'.$name.'" '.$att.'>
	   <option value="">Select</option>		
		'.$option.'		
	   </select>';
}

function getCatlogValue($name='', $att='',  $selected='',$catlog_type='',$c_name='')
{
	    $CI =& get_instance();   
		$CI->db->select("catlog_value");		
		$CI->db->from('rq_rqf_quote_catlogs');
		$CI->db->like('catlog_type',$catlog_type);
        $CI->db->distinct();
	    $query = $CI->db->get();
	    $result= $query->result();
		
		 
	$option = '';
	
	if($c_name!='')
	{
		$n = 1;
	}
	foreach($result as $reidd)
	{
		
		if($reidd->catlog_value==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		
		
		$option .= '<option value="'.$reidd->catlog_value.'" '.$select.'>'.$reidd->catlog_value.'</option>';
	 }
	

	
	echo '<select name="'.$name.'" '.$att.'>
	   <option value="">Select</option>		
		'.$option.'		
	   </select>';
}

function getCountryDropDown12($name='', $att='', $c_name='', $selected='')
{
	$option = '';
	$CI =& get_instance();
	$CI->db->select('*');
    $CI->db->from('real_country');	
	$CI->db->order_by('country_iso_code_2', 'asc');
   // $CI->db->where('status', 1);	
	$query = $CI->db->get();
    // print_r($this->db->last_query());
    $result = $query->result();
	//print_r($data['quote']);
	if($c_name!='')
	{
		$n = 1;
	}
	foreach($result as $reidd)
	{
		//echo $selected;
		if($reidd->country_iso_code_2==$selected)
		{
			 $select = 'selected';
		}
		else
		{
			$select = '';
		}
		$option .= '<option value="'.$reidd->country_iso_code_2.'" '.$select.'>'.$reidd->country_name.'</option>';
	}
	echo '<select name="'.$name.'" '.$att.'>
		<option value="">Select</option>
		'.$option.'
	</select>';
}


function getStateDropDown($id='', $name='', $att='', $c_name='', $selected='')
{

	if($id!='')
	{

	$option = '';
	$CI =& get_instance();
	$CI->db->select('*');
    $CI->db->from('state');	
    $CI->db->where('descript',$id);	
	$CI->db->order_by('name', 'asc');
	$query = $CI->db->get();
     //print_r($this->db->last_query());
    $result = $query->result();
	//print_r($data['quote']);
	if($c_name!='')
	{
		$n = 1;
	}
	//print_r($result);
	foreach($result as $reidd)
	{
		//print_r($reidd);
	 $stt_name = trim($reidd->name);
	
	 $selected = trim($selected);
	
		//echo strlen($selected);
		if($stt_name==$selected)
		{
			  $select = 'selected';
		}
		else
		{
			$select = '';
		}
	
		if($reidd->descript=='US')
		{
			$option .= '<option value="'.$reidd->cd.'" '.$select.'>'.$reidd->cd.'</option>';
		}
		else
		{
			$option .= '<option value="'.$reidd->name.'" '.$select.'>'.$reidd->name.'</option>';
		}
	}
	echo '<select name="'.$name.'" '.$att.' class="span12">
		<option value="">Select</option>
	   	'.$option.'
	</select>';
	}
	 
}




function getStateDropDown12($id='', $name='', $att='', $c_name='', $selected='')
{

	if($id!='')
	{

	$option = '';
	$CI =& get_instance();
	$CI->db->select('*');
    $CI->db->from('state');	
    $CI->db->where('descript',$id);	
	$CI->db->order_by('name', 'asc');
	$query = $CI->db->get();
   
    $result = $query->result();
	
	if($c_name!='')
	{
		$n = 1;
	}
	
	foreach($result as $reidd)
	{
		
	 $stt_name = trim($reidd->name);
	 $stt_cd = trim($reidd->cd);
	 $selected = trim($selected);
	
		
		if($stt_name==$selected)
		{
			  $select = 'selected';
		}
		else
		{
			$select = '';
		}
		if($stt_cd==$selected)
		{
			  $select1 = 'selected';
		}
		else
		{
			$select1 = '';
		}
		if($reidd->descript=='US')
		{
			$option .= '<option value="'.$reidd->cd.'" '.$select1.'>'.$reidd->cd.'</option>';
		}
		else
		{
			$option .= '<option value="'.$reidd->name.'" '.$select.'>'.$reidd->name.'</option>';
		}
	}
	echo '<select name="'.$name.'" '.$att.' class="span12">
		<option value="">Select</option>
		'.$option.'
	</select>';
	}
	else
	{
		echo ' <select name="'.$name.'"  '.$att.' class="span12" title="Select country">
                                    <option>Select</option>
                                    
                                    </select>';
	}
}


function getCatelogDropDown($field_name, $catlog_type='', $attr='', $other_val='',  $module_name ='', $selected='', $selected1='', $otr_relative_field='', $otr_relative_field_attr='', $roi_state='', $roi_state_selected='', $other_selected='', $loop_count='')
{
	
	
	
		$option = '';
		$CI =& get_instance();
		
		$CI->db->where('catlog_type', $catlog_type);		
		$CI->db->where('status', 1);			
		$CI->db->from('catlogs');		
		//$CI->db->order_by('catlog_value', 'asc');		
		$query = $CI->db->get();
		//echo $CI->db->last_query();
		$result = $query->result();
		if($result)
		{
		foreach($result as $catlog){			
			$array[$catlog->unique_id] = ucfirst($catlog->catlog_value);
		}
		if($other_val){
			$add_new = '<option value="add">Add New</option>';
		}else{ 
			$add_new = '<option value="">--Select--</option>';
		}
		 $result_array = short_array($array);		
		 foreach($result_array as $key=>$value){
		 	if(is_array($selected)){
				if(in_array($key, $selected)){
					$select_value = 'selected';
				}else{
					if($value==$selected1){
						$select_value = 'selected';
					}else{
						$select_value = '';
					}
				}
			}else if($key==$selected){
				$select_value = 'selected';
			}else{
				if($value==$selected1){
					$select_value = 'selected';
				}else{
					$select_value = '';
				}
			}
		 	$option .= '<option value="'.$value.'" '.$select_value	.'>'.$value.'</option>';
		 }
		$dropdown =  '<select name="'.$field_name.'" '.$attr.' id="'.str_replace('[]', '', $field_name).'">
		'.$add_new.'
		'.$option.'
	</select>';
	}
	else
	{
		$dropdown =  '<select name="'.$field_name.'" '.$attr.' id="'.$field_name.'">
		<option value="add">Add New</option>		
	</select>';
	}
	
	//echo $dropdown;
	return $dropdown;
}



function short_array($array){
	 asort($array);
	return $array;
}


function get_app_dropdown($name, $selected_val='', $attr=''){ 

	//if($selected_val == '') $selected_val='CA'; 
	//echo $selected_val[0];
?>
	<select name="<?php echo $name; ?>"   <?php echo $attr; ?>>

	<option  value=""></option>
    	<option <?php foreach($selected_val as $selected){ if($selected  == 'Liability') echo 'selected="selected"'; }?> value="Liability">Liability</option>
        <option <?php foreach($selected_val as $selected){if($selected == 'Physical Damage') echo 'selected="selected"';} ?> value="Physical Damage">Physical Damage</option>
		<option  <?php foreach($selected_val as $selected){ if($selected == 'Cargo') echo 'selected="selected"'; }?> value="Cargo">Cargo</option>
		<option  <?php foreach($selected_val as $selected){if($selected == 'Bobtail') echo 'selected="selected"'; } ?> value="Bobtail">Bobtail</option>
		<option  <?php foreach($selected_val as $selected){if($selected == 'Dead head') echo 'selected="selected"'; }?> value="Dead head">Dead head</option>
		<option  <?php foreach($selected_val as $selected){if($selected == 'Non Trucking') echo 'selected="selected"';} ?> value="Non Trucking">Non Trucking</option>
		<option  <?php foreach($selected_val as $selected){if($selected == 'Excess') echo 'selected="selected"'; }?> value="Excess">Excess</option>
		
	</select>
            


<?php
}
function get_member_underwriter(){
	$ci = &get_instance();
	$ci->db->select('underwriter');
	$ci->db->where('id', $ci->session->userdata('member_id'));
	$query = $ci->db->get('rq_members');
	//echo $ci->db->last_query();
	$row = $query->row();
	$result = explode(',', $row->underwriter);
	$underwriter_id = array();
	if(is_array($result)){
		foreach($result as $data){
			$data_r = explode(':', $data);
			$underwriter_id[] = isset($data_r[1]) ? $data_r[1] : $data_r[0];	
		}
	}
	if($underwriter_id){
		$ci->db->select('email');
		$ci->db->where_in('id', $underwriter_id);
		$query = $ci->db->get('rq_underwriter');
		//echo $ci->db->last_query();
		$result = $query->result();
		if(is_array($result)){
			foreach($result as $data){
				$underwriter[] = $data->email;
			}
		}
	}
	return isset($underwriter) ? implode(',',$underwriter) : '';
}

function form_safe_json($json) {
    $json = empty($json) ? '[]' : $json ;
    $search = array('\\',"\n","\r","\f","\t","\b","'") ;
    $replace = array('\\\\',"\\n", "\\r","\\f","\\t","\\b", "&#039");
    $json = str_replace($search,$replace,$json);
    return strip_tags($json);
}

function getCountryCodeDropDown($name='', $att='', $c_name, $selected='')
{
	$option = '';
	$CI =& get_instance();
	$CI->db->select('*');
    $CI->db->from('country_code');	
    $CI->db->order_by('name', 'asc');	
	$query = $CI->db->get();
    // print_r($this->db->last_query());
    $result = $query->result();
	//print_r($data['quote']);
	if($c_name!='')
	{
		$n = 1;
	}
	foreach($result as $reidd)
	{
		if($reidd->$c_name==$selected)
		{
			$select = 'selected';
		}
		else
		{
			$select = '';
		}
		$option .= '<option value="'.$reidd->$c_name.'" '.$select.'>'.$reidd->name.' - '.$reidd->$c_name.'</option>';
	}
	echo '<select name="'.$name.'" '.$att.'>
		<option value="">Select</option>
		'.$option.'
	</select>';
}

function getCatDropDownss($name='', $att='', $id, $selected='')
{  
	$option = '';
	$CI =& get_instance();
	$CI->db->select('*');
	$CI->db->where('catlog_type',$id);
	$CI->db->where('status',1);
    $CI->db->from('rq_rqf_quote_catlogs');	
    $CI->db->order_by('catlog_value', 'asc');	
	$query = $CI->db->get();   
    $result = $query->result();

	
	foreach($result as $reidd)
	{
		$option .= '<option value="'.$reidd->module_name.'" '.$select.'>'.$reidd->catlog_value.'</option>';
	}
	echo '<select name="'.$name.'" '.$att.'>
		<option value="">Select</option>
		'.$option.'
	</select>';
	
}

function getCounty($name='', $att='', $city='', $state='')
{  
    $citys = ucwords(strtolower($city));
    $states = trim($state);
	$option = '';
	$CI =& get_instance();
	$CI->db->select('county');
	$CI->db->where('city', $citys);
	$CI->db->where('state',$states);
	$CI->db->distinct();
    $CI->db->from('zip_code');	   
	$query = $CI->db->get();   
    $result = $query->result();	
  
	if(!empty($result)){	
	foreach($result as $reidd)
	{
		$option .= '<option value="'.$reidd->county.'" >'.$reidd->county.'</option>';
	}
	echo '<select name="'.$name.'" '.$att.'>
		<option value="">Select</option>
		'.$option.'
	</select>';
	}else{
		
	echo '<input type="text" name="'.$name.'" '.$att.' />';	
	}
}

function getZipCode($name='', $att='', $city='', $state='',$county='')
{  
    $citys = ucwords(strtolower($city));
    $states = trim($state);
	$countys = ucwords(strtolower($county));
	$option = '';
	$CI =& get_instance();
	$CI->db->select('zip_code');
	$CI->db->where('city', $citys);
	$CI->db->where('state',$states);
	$CI->db->where('county',$countys);
	$CI->db->distinct();
    $CI->db->from('zip_code');	   
	$query = $CI->db->get();   
    $result = $query->result();
		
	if(!empty($result)){	
	foreach($result as $reidd)
	{
		$option .= '<option value="'.$reidd->zip_code.'" >'.$reidd->zip_code.'</option>';
	}
	echo '<select name="'.$name.'" '.$att.'>
		<option value="">Select</option>
		'.$option.'		
	</select>';
	}else{
		
	echo '<input type="text" name="'.$name.'" '.$att.' />';	
	}
}


function commodities_haulteds($data='') {

		$catlog ='';
        $select='';
		$catlog .='<select type="text" id="commodities_haulteds" name="commodities_haulted[]" class="span12 chzn-select" multiple   onChange="commodities_haulted_v(this.value)" >'; //onchange="check_commodities_other(this.id)"

		$CI =& get_instance();		
        $CI->db->select('c.*');
		if(is_numeric($catlog_type)){
		  $CI->db->where('c.catlog_type', 'commodities_haulted');
		}else{
		  $CI->db->where('ct.catlog_type', 'commodities_haulted');
		  $CI->db->join('rq_rqf_quote_catlog_type as ct', 'c.catlog_type = ct.id');
		}
		$CI->db->order_by('c.catlog_type', 'asc');
		$query = $CI->db->get('rq_rqf_quote_catlogs as c');
		$catlog_data = $query->result();
		

		if($catlog_data)

		{

			

			foreach ($catlog_data as $catlogData)

			{

				$catlog_array[] =  ucfirst($catlogData->catlog_value);

			}

			sort($catlog_array);
			
			
            $data1=explode(',',$data);
			$data1_value = array();
			foreach ($data1 as $data11){
				$data1_value[] = $data11;
			}
			
			foreach ($catlog_array as $catlog_value)
			{
			
             if(in_array($catlog_value,$data1_value)){ $select="selected='selected'";  }else{ $select="";}
				$catlog .='<option value="'.$catlog_value.'"'.$select.'>'.$catlog_value.'</option>';	
				
			}

		}
		      if(in_array('other',$data1_value)){ $select="selected='selected'";  }else{ $select="";}
		$catlog .='<option value="other" '.$select.'>other</option>';	

		$catlog .='</select>';

		return $catlog;

    }
?>

