<?php
function revision_controll_icon($log_data=''){
	$icon = "<img src='".base_url('images/order-history.png')."' obj_data='".json_encode($log_data)."' class='history'>";
	return ($log_data) ? $icon : '';
}
function getModificationLog($type='', $id='', $fields='', $row=0){
	$CI =& get_instance();
	$where = array('ml.parent_type' => $type, 'ml.parent_id' => $id);
	$CI->db->select('ml.parent_type, ml.created_on, ml.created_by_name, mlv.field, mlv.value');
	$CI->db->where($where);
	$CI->db->where_in('mlv.field', $fields);
	$CI->db->order_by('ml.created_on', 'desc');
	$CI->db->join('modification_log_values as mlv', 'mlv.modification_id = ml.id');	
	$query = $CI->db->get('modification_logs as ml');	
	$CI->db->last_query();
	$logs = $query->result();	
	foreach($fields as $field){
	  foreach($logs as $log){				
		if($log->field==$field){
		  $log_data[$field][] = $log;	
		}
	  }
	}
	return isset($log_data) ? $log_data : '';
}
function modificationLogValue($modification_id, $modification_log_value){
	$CI =& get_instance();
	$column_names = array('modification_id', 'field', 'value');
	foreach($modification_log_value as $field => $value){
	  $rows[] =array($modification_id, $field, $value);
	}
	insert_rows('modification_log_values', $column_names, $rows, $escape = true);
	return $CI->db->insert_id();	
}
function modificationLogs($modification_log, $modification_log_value){
	$CI =& get_instance();	
	
	$modification_log['created_on'] = date('Y-m-d H:i:s');
	$modification_log['created_by_id'] = $CI->session->userdata('member_id');
	$modification_log['created_by_name'] = $CI->session->userdata('member_name');
	$modification_log['created_by_email'] = $CI->session->userdata('username');
	
	$CI->db->insert('modification_logs', $modification_log);
	$log_id = $CI->db->insert_id();
	modificationLogValue($log_id, $modification_log_value);
}
function compare_array($array, $array1){
  foreach($array as $key => $value){
	  if($key=='losses_from_date'||$key=='losses_to_date'||$key=='date_of_loss'){
		  $a[$key] = ($value);
	  }else{
		  $a[$key] = $value;
	  }
  }
  foreach($array1 as $key => $value){	
	  if($key=='losses_from_date'||$key=='losses_to_date'||$key=='date_of_loss'){
		  $b[$key] = ($value);
	  }else{
		  $b[$key] = $value;
	  }
  }		
  /*echo '<pre>';
				  print_r($a);
				  print_r($b);
				   echo '</pre>';*/
  return array_diff_assoc($a, $b);
}
function prepare_column_name(& $name){
  $name = "`$name`";
}
function escape_value(& $value){
  if( is_string($value) ){
	  $value = "'" . mysql_real_escape_string($value) . "'";
  }
}
//Insert multiple rows at ones
function insert_rows($table_name, $column_names, $rows, $escape = true){
  $CI =& get_instance();	
  /* Build a list of column names */
  $columns    = array_walk($column_names, 'prepare_column_name' );
  $columns    = implode(',', $column_names);

  /* Escape each value of the array for insertion into the SQL string */
  if( $escape ) array_walk_recursive( $rows, 'escape_value' );

  /* Collapse each rows of values into a single string */
  $length = count($rows);
  for($i = 0; $i < $length; $i++) $rows[$i] = implode(',', $rows[$i]);
 
  $values = "(" . implode( '),(', $rows ) . ")";

   $sql = "INSERT INTO $table_name ( $columns ) VALUES $values";

  return $CI->db->simple_query($sql);
}
function get_largest_element($a='', $b='', $c=''){
  if($a>$b){
	if($a>$c){
	  return $a;			
	}else{
	  return $c;
	}
  }else if($b>$c){
	return $b;	
  }else{
	return $c;	
  }
}
function array_to_version($r, $version_data){
  $p = '';
  $pr = '';
  for($i=0;$i<$r;$i++){
	$pr = isset($version_data[$i]->value) ? $version_data[$i]->value : '';
	if($pr){
	  $p[] = 'v'.$i.' '.$pr;
	}
  }
 $v = is_array($p) ? ', '.implode(', ', $p) : '';
 return $v;
}
function get_version_detail($id=''){
  if($id){
	$CI = & get_instance();
	$CI->db->select('date_added,requester_id');
	$CI->db->where('quote_id', $id);
	$query = $CI->db->get('rq_rqf_quote');	
	$row = $query->row();
	return $row;
  }
}
function get_username($id=''){
  if($id){
	$CI = & get_instance();
	$CI->db->select('first_name,middle_name,last_name');
	$CI->db->where('id', $id);
	$query = $CI->db->get('rq_members');	
	$row = $query->row();
	$name = isset($row->first_name) ? ($row->first_name) ? $row->first_name : ''  :'';
	$name .= isset($row->middle_name) ? ($row->middle_name) ? ' '.$row->middle_name : ''  :'';
	$name .= isset($row->last_name) ? ($row->last_name) ? ' '.$row->last_name : ''  :'';
	return ucfirst($name);
  }		
}
?>