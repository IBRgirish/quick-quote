<?php
/**
 * Core Class extends CI_Controller
 *
 * @package    ACMSe
 * @category   Global
 * @author     Gary
 * @link http://www.example.com/core.html
 */

class RQ_Controller extends CI_Controller
{
 
  /**
 * Class constructor
 *
 * @access	public
 */
	public function __construct()
	{
	//$this->output->cache(10);
		parent::__construct();
		$this->output->enable_profiler(FALSE);
		
	}
	
/**
 * require_login function of class to protect member logged in sections
 *
 * @access	public
 * @return	string
 */
	public function require_login()
	{
		if(!$this->session->userdata('user_id'))
		{
				 redirect();
		}
	}


	public function require_login_user()
	{
		if(!$this->session->userdata('member_id'))
		{
				 return false;
		} else {
			return true;
		}
	}
	
	public function require_login_underwriter()
	{
		if(!$this->session->userdata('underwriter_id'))
		{
			return false;
		} else {
			return true;
		}
	}
	
/**
 * require_admin_login function of class to protect admin section
 *
 * @access	public
 * @return	string
 */
	public function require_admin_login()
	{
		if(!$this->session->userdata('admin_id'))
		{
		  echo '<script language="javascript">window.location.href=\''.base_url('administration/index').'\';</script>';
		  exit;
		}
	}
	

}

/* End of file ACMS_Controller.php */
/* Location: /application/libraries/ACMS_Controller.php */