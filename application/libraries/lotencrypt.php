<?php
class lotencrypt
{
	/*var $encrypt_mapk=array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
	//var $encrypt_mapk=array('1', '2', '3', '4', '5', '6', '7', '8', '9');
	var $encryt_mapv=array( '9', '8', '7', '6', '5', '4', '3', '2', '1', '0', 'Z', 'Y', 'X', 'W', 'V', 'U', 'T', 'S', 'R', 'Q', 'P', 'O', 'N', 'M', 'L', 'K', 'J', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A');
	//var $encryt_mapv=array( '9', '8', '7', '6', '5', '4', '3', '2', '1', '0');*/

	var $encrypt_mapk=array('0', '1', '3', '7', '4', '2', '6', '5', '8', '9');
	var $encryt_mapv=array( '2', '4', '7', '6', '9', '5', '3', '8', '1', '0');

	var $key; // aka Reseller ID
	var $mape;  // master encryption map
	var $mapd;  // master decryption map
	var $mapoffset;
	var $strrotate;
	
	function lotencrypt($key='999999')
	{
		if (!function_exists('array_combine')){
			function array_combine($arr1, $arr2) {
				$out = array();

				$arr1 = array_values($arr1);
				$arr2 = array_values($arr2);

				foreach($arr1 as $key1 => $value1) {
					$out[(string)$value1] = $arr2[$key1];
				}

				return $out;
			}
		}
	   $this->mapoffset=$key;		// We offset (rotate array) the map values by $key amount
	   $this->strrotate=$key-200;   
	   if ($this->strrotate<0)
	   		$this->strrotate+=101;  // We rotate the finale encrypted string using the last 2 digits of the RID.
	   							    // If RID < 200 (Abemor, NYDT, etc), then we add back 101 to it so it's pos.
	   // Combine the map key array with new map values array to make master encryption map
	   $this->mape= array_combine($this->encrypt_mapk,$this->array_rotate($this->encryt_mapv,$this->mapoffset));
	   $this->mapd= array_combine($this->array_rotate($this->encryt_mapv,$this->mapoffset),$this->encrypt_mapk);
	}
	
	function array_rotate($arr,$qty)
	{
	  for ($i=1;$i<=$qty;$i++){
		  $elm = array_shift($arr);
		  array_push($arr, $elm);
	  }
	  return $arr;
	}
	
	function amenc($lot)
	{
		$lot = "22".$lot."22";
		$outtext = '';
	   $lot=strtoupper($lot);
	   for ($i=strlen($lot)-1;$i>=0;$i--)
		   $outtext .= ($lot[$i]=='-'?$lot[$i]:$this->mape[$lot[$i]]);
	   $outtext = substr($outtext,$this->strrotate%strlen($outtext)) . substr($outtext,0,$this->strrotate%strlen($outtext));
	   return $outtext;
	}

	function amdec($lot)
	{
		$outtext = ''; 
	   $lot = substr($lot,-($this->strrotate%strlen($lot))).substr($lot,0,-($this->strrotate%strlen($lot)));
	   for ($i=strlen($lot)-1;$i>=0;$i--)
		   $outtext .= ($lot[$i]=='-'?$lot[$i]:$this->mapd[$lot[$i]]);
		   //$outtext .= $this->mapd[$lot[$i]];
	   $outtext = substr($outtext, 2);
		$outtext = substr($outtext, 0, strlen($outtext)-2);
	   return $outtext;
	} 

}
?>