<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
  /**
  * Ignited API
  *
  * This is a wrapper class/library for getting reponse from link  * 
  *
  * @package    CodeIgniter
  * @subpackage libraries
  * @category   library
  * @version    0.1
  * @author     Ashvin Patel <ibr.ashvin@gmail.com>  *             
  * @link       http://ibrinfotech.com.com/
  */
  class API
  {
    /**
    * Global container variables for chained argument results
    *
    */
    protected $ci;
    protected $table;
    protected $distinct;
    protected $group_by; 

    /**
    * Copies an instance of CI
    */
    public function __construct()
    {
      $this->ci =& get_instance();
    }
    
    /**
    * Get response from given URL 
    * @$response return respose
    */ 
    public function get_response($url='')
    {
		
        $response = @file_get_contents($url);
		return $response;
    }
    
    /**
     * Decode response data
     */ 
    public function prepare_response($data)
    {
		return json_decode(strip_tags($data)); 
    }
}
