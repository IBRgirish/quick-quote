<?php $this->load->view('includes/head_style_script'); ?>
<style>
.sheet-header:hover
{
	cursor:pointer;
}
</style>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<!-- Navbar
    ================================================== -->
<div class="navbar navbar-fixed-top">
  <?php if($this->session->userdata('admin_id')) {?>
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <!--<a class="brand" href="#">Bootstrap</a>-->
      <div class="nav-collapse collapse" id="main-menu">
        <ul class="nav" id="main-menu-left">
          <li class="<?php echo ($this->uri->segment(2)=='index'?'active':'');?>"><a href="<?php echo base_url('administration/index');?>">Home</a></li>
          <li class="<?php echo ($this->uri->segment(2)=='underwriter'?'active':'');?>"><a href="<?php echo base_url('administration/underwriter');?>">Manage Underwriter</a></li>
		  <li class="<?php echo ($this->uri->segment(2)=='members'?'active':'');?>"><a href="<?php echo base_url('administration/agency');?>">Manage Agency</a></li>
		  <li class="<?php echo ($this->uri->segment(2)=='catlog'?'active':'');?>"><a href="<?php echo base_url('administration/catlog');?>">Manage Catlog</a></li>
   		  <li class="<?php echo (($this->uri->segment(2)=='quotes' && $this->uri->segment(3)=='') ?'active':'');?>"><a href="<?php echo base_url('administration/quotes');?>">Manage Quotes</a></li>
          <li class="<?php echo (($this->uri->segment(2)=='data_dictionary' && $this->uri->segment(3)=='') ?'active':'');?>"><a href="<?php echo base_url('administration/data_dictionary');?>">Data Dictionary</a></li>
         <li><a href="<?php echo base_url('report_form'); ?>">Report Quote Process</a></li>
 		  <!-- <li class="<?php echo ((($this->uri->segment(2)=='quotes' && $this->uri->segment(3)=='rate_by_quote1')||($this->uri->segment(1)=='ratesheet' && $this->uri->segment(2)=='edit_ratesheet')||($this->uri->segment(2)=='quotes' && $this->uri->segment(3)=='check_quotes'))?'active':'');?>"><a href="<?php echo base_url('administration/quotes/check_quotes');?>">Manage Quotes</a></li>-->
      <!--   <li><a id="swatch-link" href="<?php echo base_url('application_form'); ?>">Application</a></li>-->
      <!--   <li class="<?php echo ($this->uri->segment(2)=='application'?'active':'');?>"><a href="<?php echo base_url('application_form');?>">Application</a></li>-->
          <li class="<?php echo ($this->uri->segment(2)=='settings'?'active':'');?>"><a href="<?php echo base_url('administration/settings');?>">Settings</a></li>
	 </ul>
        <ul class="nav pull-right" id="main-menu-right">
          <li><a href="<?php echo base_url('administration/index/logout');?>" title="logout" >Logout<i class="icon-share-alt"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<div class="container">
