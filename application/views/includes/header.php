<?php $this->load->view('includes/head_style_script'); ?>
<?php 
/* Diasble Error Reporting */
error_reporting(0);
@ini_set('display_errors', 0); ?>
<script>
var _debug = false;
var _placeholderSupport = function() {
    var t = document.createElement("input");
    t.type = "text";
    return (typeof t.placeholder !== "undefined");
}();

window.onload = function() {
    var arrInputs = document.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var curInput = arrInputs[i];
        if (!curInput.type || curInput.type == "" || curInput.type == "text")
            HandlePlaceholder(curInput);
        else if (curInput.type == "password")
            ReplaceWithText(curInput);
    }

    if (!_placeholderSupport) {
        for (var i = 0; i < document.forms.length; i++) {
            var oForm = document.forms[i];
            if (oForm.attachEvent) {
                oForm.attachEvent("onsubmit", function() {
                    PlaceholderFormSubmit(oForm);
                });
            }
            else if (oForm.addEventListener)
                oForm.addEventListener("submit", function() {
                    PlaceholderFormSubmit(oForm);
                }, false);
        }
    }
};

function PlaceholderFormSubmit(oForm) {    
    for (var i = 0; i < oForm.elements.length; i++) {
        var curElement = oForm.elements[i];
        HandlePlaceholderItemSubmit(curElement);
    }
}

function HandlePlaceholderItemSubmit(element) {
    if (element.name) {
        var curPlaceholder = element.getAttribute("placeholder");
        if (curPlaceholder && curPlaceholder.length > 0 && element.value === curPlaceholder) {
            element.value = "";
            window.setTimeout(function() {
                element.value = curPlaceholder;
            }, 100);
        }
    }
}

function ReplaceWithText(oPasswordTextbox) {
    if (_placeholderSupport)
        return;
    var oTextbox = document.createElement("input");
    oTextbox.type = "text";
    oTextbox.id = oPasswordTextbox.id;
    oTextbox.name = oPasswordTextbox.name;
    //oTextbox.style = oPasswordTextbox.style;
    oTextbox.className = oPasswordTextbox.className;
    for (var i = 0; i < oPasswordTextbox.attributes.length; i++) {
        var curName = oPasswordTextbox.attributes.item(i).nodeName;
        var curValue = oPasswordTextbox.attributes.item(i).nodeValue;
        if (curName !== "type" && curName !== "name") {
            oTextbox.setAttribute(curName, curValue);
        }
    }
    oTextbox.originalTextbox = oPasswordTextbox;
    oPasswordTextbox.parentNode.replaceChild(oTextbox, oPasswordTextbox);
    HandlePlaceholder(oTextbox);
    if (!_placeholderSupport) {
        oPasswordTextbox.onblur = function() {
            if (this.dummyTextbox && this.value.length === 0) {
                this.parentNode.replaceChild(this.dummyTextbox, this);
            }
        };
    }
}

function HandlePlaceholder(oTextbox) {
    if (!_placeholderSupport) {
        var curPlaceholder = oTextbox.getAttribute("placeholder");
        if (curPlaceholder && curPlaceholder.length > 0) {
            Debug("Placeholder found for input box '" + oTextbox.name + "': " + curPlaceholder);
            oTextbox.value = curPlaceholder;
            oTextbox.setAttribute("old_color", oTextbox.style.color);
            oTextbox.style.color = "#c0c0c0";
            oTextbox.onfocus = function() {
                var _this = this;
                if (this.originalTextbox) {
                    _this = this.originalTextbox;
                    _this.dummyTextbox = this;
                    this.parentNode.replaceChild(this.originalTextbox, this);
                    _this.focus();
                }
                Debug("input box '" + _this.name + "' focus");
                _this.style.color = _this.getAttribute("old_color");
                if (_this.value === curPlaceholder)
                    _this.value = "";
            };
            oTextbox.onblur = function() {
                var _this = this;
                Debug("input box '" + _this.name + "' blur");
                if (_this.value === "") {
                    _this.style.color = "#c0c0c0";
                    _this.value = curPlaceholder;
                }
            };
        }
        else {
            Debug("input box '" + oTextbox.name + "' does not have placeholder attribute");
        }
    }
    else {
        Debug("browser has native support for placeholder");
    }
}

function Debug(msg) {
    if (typeof _debug !== "undefined" && _debug) {
        var oConsole = document.getElementById("Console");
        if (!oConsole) {
            oConsole = document.createElement("div");
            oConsole.id = "Console";
            document.body.appendChild(oConsole);
        }
        oConsole.innerHTML += msg + "<br />";
    }
}


</script>
	<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
	<?php

	if(empty($print)){ 
	$this->load->view('includes/front_nav');
	}
	/* if(!$this->session->userdata('member_id'))
	{
		$this->load->view('includes/front_nav');
	}
	else
	{
		$this->load->view('includes/user_nav');
	} */
	?>
	<div class="container">
<!-- Masthead
================================================== -->
<!--header class="jumbotron subhead" id="overview"-->
<header class="jumbotron" id="overview">
  <div class="row">
    <div class="span6">
      <!--h1>Cerulean</h1>
      <p class="lead">A calm, blue sky.</p-->
	<!--  <img src="<?php echo base_url('images');?>/logo.png" alt="Logo" >-->
    </div>
    <!--div class="span6">
      <div class="bsa well">
          <div id="bsap_1277971" class="bsap_1277971 bsap"><div class="bsa_it one">
		  <div class="bsa_it_ad ad1 odd" id="bsa_2634481">
		<span class="bsa_it_i">
			<img src="<?php echo base_url('images');?>/logo.png" alt="Drag &amp; Drop for WordPress" height="100" width="130">
		</span>
		<span class="bsa_it_t">Drag &amp; Drop for WordPress</span>
		<span class="bsa_it_d">A revolutionary new way to build pro sites; faster &amp; easier, than ever before. </span>
		<div style="clear:both"></div>
		</div></div></div>
      </div>
    </div-->
  </div>
  <!--<div class="subnav">
    <ul class="nav nav-pills">
      <li class="active"><a href="#typography">Typography</a></li>
      <li class=""><a href="#navbar">Navbar</a></li>
      <li class=""><a href="#buttons">Buttons</a></li>
      <li class=""><a href="#forms">Forms</a></li>
      <li class=""><a href="#tables">Tables</a></li>
      <li class=""><a href="#miscellaneous">Miscellaneous</a></li>
    </ul>
  </div>-->
</header>
