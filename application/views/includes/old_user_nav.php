 <!-- Navbar
    ================================================== --> 
 <div class="navbar navbar-fixed-top">
   <div class="navbar-inner">
     <div class="container">
       <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </a>
       <a class="brand" href="#">Bootswatch</a>
       <div class="nav-collapse collapse" id="main-menu">
        <ul class="nav" id="main-menu-left">
          <li><a onclick="pageTracker._link(this.href); return false;" href="<?php echo base_url();?>">Home</a></li>
          <li><a id="swatch-link" href="http://bootswatch.com/#gallery">Gallery</a></li>
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Preview <b class="caret"></b></a>
            <ul class="dropdown-menu" id="swatch-menu">
              <li><a href="http://bootswatch.com/default/">Default</a></li>
              <li class="divider"></li>
              <li><a href="http://bootswatch.com/amelia/">Amelia</a></li>
              <li><a href="http://bootswatch.com/cerulean/">Cerulean</a></li>
              <li><a href="http://bootswatch.com/cosmo/">Cosmo</a></li>
              <li><a href="http://bootswatch.com/cyborg/">Cyborg</a></li>
              <li><a href="http://bootswatch.com/flatly/">Flatly</a></li>
              <li><a href="http://bootswatch.com/journal/">Journal</a></li>
              <li><a href="http://bootswatch.com/readable/">Readable</a></li>
              <li><a href="http://bootswatch.com/simplex/">Simplex</a></li>
              <li><a href="http://bootswatch.com/slate/">Slate</a></li>
              <li><a href="http://bootswatch.com/spacelab/">Spacelab</a></li>
              <li><a href="http://bootswatch.com/spruce/">Spruce</a></li>
              <li><a href="http://bootswatch.com/superhero/">Superhero</a></li>
              <li><a href="http://bootswatch.com/united/">United</a></li>
            </ul>
          </li>

        </ul>
        <ul class="nav pull-right" id="main-menu-right">
          <?php if(!$this->session->userdata('member_id')) { ?>
		  <li class="<?php echo ($this->uri->segment(2)=='login'?'active':'');?>"><a href="<?php echo base_url('administration/index');?>"><a href="<?php echo base_url('user/login');?>" title="logout" >Login<i class="icon-user"></i></a></li>
          <li class="<?php echo ($this->uri->segment(2)=='register'?'active':'');?>"><a href="<?php echo base_url('user/register');?>" title="logout" >Register<i class="icon-arrow-right"></i></a></li>
          <?php } else { ?>
		   <li class="dropdown" id="preview-menu">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo base_url('profile');?>">Profile</a></li>
              <li><a href="<?php echo base_url('change_password');?>">Change Password</a></li>
            </ul>
          </li>
		  <li><a href="<?php echo base_url('user/user/logout');?>" title="logout" >Logout<i class="icon-share-alt"></i></a></li>
		  <?php } ?>
		</ul>
       </div>
     </div>
   </div>
 </div>