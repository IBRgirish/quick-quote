<table class="table hover" style="width:500px;">	
	<thead>
    	<tr>
        	<th>Value</th>
            <th>Created By</th>
            <th>Date Time</th>
        </tr>
    </thead>
    <tbody>
    <?php /*print_r($data);*/ if(is_array($data)){ ?>
    	<?php foreach($data as $value){?>        
    	<tr>
        	<td>
			<?php
				echo $value->value;		
			 ?>
            </td>
            <td><?= $value->created_by_name; ?></td>
            <td><?= ($value->created_on) ? date('m/d/Y h:i a', strtotime($value->created_on)) : ''; ?></td>
        </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>