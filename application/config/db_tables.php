<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| db_tables BD-TABLES
| -------------------------------------------------------------------
| This file is the collection of DB Tables.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Helper files
| 4. Custom config files
| 5. Language files
| 6. Models
|
*/

$config['admin_table'] = 'real_admins';
$config['member_table'] = 'rq_members'; /* chnage from real to rq */
$config['underwriter_table'] = 'rq_underwriter';
$config['config_table'] = 'rq_config';
$config['country_table'] = 'real_country';
$config['property_listing_table'] = 'real_property_listings';
$config['realestate_valuation_table'] = 'real_estate_valuation';
$config['realestate_order_table'] = 'real_orders';
$config['page_table'] = 'real_pages';
$config['underwriter_table'] = 'rq_underwriter';

function generate_unique($val)
{
	return $val.time();
}
function generate_random_string($length=8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $string = '';    
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, (strlen($characters)-1))];
    }
    return $string;
}
function is_email_add($Addr)
{
		$p = '/^[a-z0-9!#$%&*+-=?^_`{|}~]+(\.[a-z0-9!#$%&*+-=?^_`{|}~]+)*';
		$p.= '@([-a-z0-9]+\.)+([a-z]{2,3}';
		$p.= '|info|arpa|aero|coop|name|museum)$/ix';
	return preg_match($p, $Addr);
}


/* End of file db_tables.php */
/* Location: /application/config/db_tables.php */