<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//$route['default_controller'] = "frontend";
$route['default_controller'] = "user/login";
$route['admin'] = "administration/index";
$route['login'] = "user/login";
$route['register'] = "user/register";
$route['account/profile'] = "user/account";
$route['account/profile_edit'] = "user/account/edit";
$route['account/change_password'] = "user/changepassword";
$route['logout'] = "user/user/logout";
//$route['request_quote'] = "quote/index/$1";
$route['list_quote'] = "quote/list_quote";
$route['quote_status'] = "quote/quote_status";
$route['list_assigned_quote'] = "quote/list_assigned_quote";
$route['underwriter'] = "underwriter/index";
$route['administration/agency']	=	"administration/members";
$route['administration/agency/add_agency']	=	"administration/members/add_member";
$route['administration/agency/edit_agency/(:num)']	=	"administration/members/edit_member/$1";
$route['request_ratesheet'] = "ratesheet";

//$route['page/(:any)'] = "frontend/index/page/$1";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */