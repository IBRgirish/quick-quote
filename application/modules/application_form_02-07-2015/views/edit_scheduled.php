<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/head_style_script');	
} 
?>
<html lang="en">
  
  <head>

    <title>GHI-11</title>
  </head>
  
  <body>
    <div class="container-fluid">
      <div class="page-header">
        <h1>GHI-11 <small>Non-operational supplement form</small>
        </h1>
      </div>
      <div class="row-fluid">
        <span class="span12">
          <label class="control-label">Exclusion</label>
        </span>
      </div>
      <p>The Named Insured agrees that the said Policy shall not and does not protect the Named Insured from claims for injury, damage or loss sustained by any person when caused by a motor vehicle not specified in said policy, and if the company shall be obliged
        to pay any claim that it would not otherwise be obligated to pay but for the attachment of any endorsements required by any State or Federal authority, the Insured agrees to reimburse the company in the amount paid and all sums including costs and expenses
        which shall have been paid in connection with such claims.</p>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">How many additional vehicles?</label>
        </span>
        <span class="span1">
          <input class="textinput span12" type="text" name="" placeholder="">
        </span>
        <span class="span7">
          <button class="btn"> <i class="icon icon-plus"></i> Add</button>
        </span>
      </div>
      <div class="well">
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">VIN &nbsp;</label>
            <input class="textinput" type="text" placeholder="" name="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">GVW &nbsp;</label>
            <input class="textinput span10" type="text" name="" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">Year &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">Make &nbsp;</label>
            <input class="textinput" type="text" placeholder="" name="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">Body Type &nbsp;</label>
            <input class="textinput span8" type="text" name="" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">Model &nbsp;</label>
            <input class="textinput span7" type="text" name="" placeholder="Tractors, Trailers, etc">
          </span>
        </div>
      </div>
      <div>
        <div class="form-actions">
          <button class="btn btn-primary">Save</button>
          <button class="btn">Cancel</button>
        </div>
      </div>
    </div>
  </body>

</html>