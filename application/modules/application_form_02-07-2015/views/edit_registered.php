<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/head_style_script');	
} 
?>
<html lang="en">
  
  <head>

    <title>GHI-9</title>
  </head>
  
  <body>
    <div class="container-fluid">
      <div class="page-header">
        <h1>GHI-9 <small>Registered owner info supplement form</small>
        </h1>
      </div>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">How many additional persons?</label>
        </span>
        <span class="span1">
          <input class="textinput span12" type="text" name="" placeholder="">
        </span>
        <span class="span7">
          <button class="btn"> <i class="icon icon-plus"></i> Add</button>
        </span>
      </div>
      <div class="well">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">Registered Owner or Partner's name &nbsp;</label>
            <input class="textinput span3" type="text" name="" placeholder="First name">
            <input class="textinput span3" type="text" name="" placeholder="Middle name">
            <input class="textinput span3" type="text" name="" placeholder="Last name">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">Address</label>
            <input class="textinput span12" type="text" placeholder="" name="">
          </span>
          <span class="span3">
            <label class="control-label">City</label>
            <input class="textinput span12" type="text" name="" placeholder="">
          </span>
          <span class="span3">
            <label class="control-label">County</label>
            <input class="textinput span12" type="text" placeholder="" name="">
          </span>
          <span class="span1">
            <label class="control-label">State</label>
            <select name="">
              <option value="CA">CA</option>
              <option value="TX">TX</option>
              <option value="FL">FL</option>
              <option value="DV">DV</option>
            </select>
          </span>
          <span class="span1">
            <label class="control-label">Zip</label>
            <input class="textinput span12" type="text" placeholder="" name="">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">License Number</label>
            <input class="textinput" type="text" placeholder="" name="">
          </span>
          <span class="span4">
            <label class="control-label">S.S.N</label>
            <input class="textinput" type="text" placeholder="" name="">
          </span>
          <span class="span4">
            <label class="control-label">Commercial License</label>
            <label class="radio pull-left">
              <input type="radio" value="" name="">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="" name="">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">Please provide a photo copy of license and MVR &nbsp;</label>
            <input class="textinput" type="text" placeholder="Upload file" name="">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <a class="dom-link fancybox edit" href="#addowner" >If vehicle is not driven by the above person, please use GHI-6</a>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">Please explain</label>
            <textarea class="span12" placeholder="" name=""></textarea>
          </span>
        </div>
      </div>
      <div>
        <div class="form-actions">
          <button class="btn btn-primary">Save</button>
          <button class="btn">Cancel</button>
        </div>
      </div>
    </div>
  </body>

</html>