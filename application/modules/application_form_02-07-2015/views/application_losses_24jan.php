<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php
/*if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');	

} */
?>

        <style>
		   .span_width1{
			   width: 40px!important;
			   }
            .table thead th {
                text-align: center;
                vertical-align: inherit;
            }

            .table th, .table td {
                font-size: 0.8em;
            }

            .row_added_vehicle{
                margin:0px 0px 5px 0px;
            }
            .status_msg {
                color: #ff0000;
            }
			
			#upload_file_name_second{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second input{
				display: none;
			}
			
			#upload_file_name_second li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second li span{
				width: 15px;
				height: 12px;
				background: url('<?php echo base_url();  ?>images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second li.error p{
				color:red;
			}

				#upload_file_name_second_div{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second_div li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second_div input{
				display: none;
			}
			
			#upload_file_name_second_div li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second_div li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second_div li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second_div li span{
				width: 15px;
				height: 12px;
				background: url('<?php echo base_url();  ?>images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second_div li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second_div li.error p{
				color:red;
			}

			.pull-left{
			float:left;
			}
			
			.upload_file_name1{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			.upload_file_name1 li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			.upload_file_name1 input{
				display: none;
			}
			
			.upload_file_name1 li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			.upload_file_name1 li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			.upload_file_name1 li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			.upload_file_name1 li span{
				width: 15px;
				height: 12px;
				background: url('<?php echo base_url();  ?>images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			.upload_file_name1 li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			.upload_file_name1 li.error p{
				color:red;
			}
        </style>	 

<script type="text/javascript">
	
//	$(document).ready(add_losses());
			

function apply_on_all_elements(){
		
		//$('input').not('.datepick').autotab_magic().autotab_filter();
		//$('input').not('.require').autotab_magic().autotab_filter();
		//$('select').not('.require').autotab_magic().autotab_filter();
		
		//$('input').attr('required', 'required');
		
		$('.price_format').priceFormat({
		prefix: '$ ',
		centsLimit: 0,
		thousandsSeparator: ','
		});
		
		$(".license_issue_date").mask("99/99/9999");
		
		//  $('input').attr('required', 'required');  
		//$('select').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		
		$('#vehicle_tractor').removeAttr('required');
		$('#vehicle_truck').removeAttr('required');
		$('#zip2').removeAttr( 'required' );
		$('.zip2').removeAttr( 'required' );
		$('.filing_numbers').removeAttr('required');
		$('.filing_type').removeAttr('required');
		$('.last_name').removeAttr('required');
		$('.middle_name').removeAttr('required');
		
		//$('#vehicle_pd').removeAttr('required');
		//$('#vehicle_liability').removeAttr('required');
		//$('#vehicle_cargo').removeAttr('required');
		$('#cab').removeAttr('required');
		$('#tel_ext').removeAttr('required');
		$('#insured_email').removeAttr('required');
		$('#insured_tel_ext').removeAttr('required');
		//alert();
		
		// $('form').h5Validate(); 	
		//alert($(".chosen").val());
}

	$(document).ready(function() {

	
			$('#first_uploading1').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				//alert();
				document.getElementById('uploaded_files_values123').value = '';
				$('#droping12 a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});
			
					$('#first_upload').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				//alert();
				document.getElementById('uploaded_files_values').value = '';
				$('#drop a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});
			
			
			
			
					$('#first_upload1').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values1').value = '';
				$('#drop1 a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});
					$('#first_upload2').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values2').value = '';
				$('#drop2 a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});
					$('#first_upload3').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values3').value = '';
				$('#drop3 a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});
					$('#first_upload4').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values4').value = '';
				$('#drop4 a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});
			
					$('#first_upload5').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values5').value = '';
				$('#drop5 a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});
			
			
			
					$('#first_upload25').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values25').value = '';
				$('#drop25 a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});
			
	/*		$('#first_upload_second').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values_second').value = '';
				$('#drop_second a').parent().find('input').click();
    		});*/
			
		
			
			
		apply_on_all_elements();
		$("#coverage_date").mask("99/99/9999");
		
		//$('form').h5Validate();
		
		//$('input').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		//$('input[type="checkbox"]').removeAttr( 'required' );
		$('input[type="file"]').removeAttr( 'required' ); 
		
/* 		$('#coverage_date').datepicker({
			format: 'mm/dd/yyyy'
		}); */
		//$("#coverage_date").mask("99/99/9999");
 	 	/* var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

		var checkin = $('#coverage_date').datepicker({
		onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
		}).on('changeDate', function(ev) {
		// $(this).blur();
		checkin.hide();		 
		}).data('datepicker');   */
		
		/* $('#coverage_date').blur(function() {
  			$('#coverage_date').datepicker("hide");
		});
		 */
		
		//Hide datepicker on Focus Out		
	/* 	$('#coverage_date').blur(function(){
			//setTimeout(function(){$('#coverage_date').datepicker('hide');},100)
		}); */
		
		var date = new Date();
		date.setDate(date.getDate()-0);
				
		 var inputs1 = $('#coverage_date').datepicker({
			startDate: date,
			format: 'mm/dd/yyyy',
			autoclose: true
		});  


		
		$('#dba').change(function(){
			//check_insured_dba_required();			
		});
		
		$('#insured_name').change(function(){
		//alert();
			//check_insured_dba_required();			
		});

		

    var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }

//$('.request_quote').validateForm();
/* 
		var hiddenEls = $("body").find(":hidden").not("script");

	$("body").find(":hidden").each(function(index, value) {
	selctor= this.id;
	if(selctor){
		$( '#'+selctor+' :input').removeAttr("required");
		}
		alert(this.id);
	}); */
	
	/*  $(".request_quote").validateForm({
		ignore: ":hidden",
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			
			$('#loss_report_upload1').click(function() {  
				//var up_id = $('.upload_select:last').attr('id');
				//alert(up_id);
				$('#fileup1_1').click();  
				//$('#'+up_id).click();  
			
			});
			 
			$('#mvr_upload').click(function() { 
			$('#fileup_1').click();  });
 


	});
	
	
			
		function form_sub(){
		/* $(".request_quote").validateForm({
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			//$('.request_quote').validateForm();
			
/* 			if($('#theid').css('display') == 'none'){ 
   $('#theid').show('slow'); 
} else { 
   $('#theid').hide('slow'); 
}
$("br[style$='display: none;']") */


			
	}
	
	function check_insured_dba_required(){
			dba_val = $('#dba').val().trim();
			insured_val = $('#insured_name').val().trim();
			if(dba_val){
				$('#insured_name').removeAttr('required'); 
				$('#insured_middle_name').removeAttr('required');
				$('#dba').attr('required', 'required'); 
			}else if(insured_val){
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').removeAttr('required'); 		
			} else {
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').attr('required', 'required'); 	
			}
		}
		
	
	<!-----updated by amit03-03-2014------>
	$("body").on("change",".rofo:first",function(){
				val = $('.rofo:first').val();
				$('.rofo:first').attr("readonly",false)
				$('.rofothers:first').attr("readonly",false)
				otherval = $('.rofothers:first').val();
				$(".tractor_radius_of_operation").val(val);
				$(".truck_radius_of_operation").val(val);
				if(val == 'other')
				{
					$('.rofothers').show();
					$('.rofothers').val(otherval);
					$('.rofothers').not(":first").attr("readonly",true);
					$(".show_v_others").show();
				}
				
				$(".truck_radius_of_operation").not(":first").attr("readonly",true);
				$(".tractor_radius_of_operation").not(":first").attr("readonly",true);
				$('.show_tractor_others').not(":first").attr("readonly",true);
				$('.show_truck_others').not(":first").attr("readonly",true);
			});
			$("body").on("keyup",".rofothers:first",function(){
				
				$(".rofothers").val($(this).val());
			});
	
			function rofoperation(val , id){
			
				if(id=='1tractor_radius_of_operation')
				{							
					$(".tractor_radius_of_operation").val(val);
					$(".tractor_radius_of_operation").not(":first").attr("disabled",true);
					if(val == 'other'){
						$('.show_tractor_others').show();
						$(".specify_other_cp").not(":first").attr("disabled",true);
					} else {
						$('.show_tractor_others').hide();
					}	
				}
				else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').show();
					} else {
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').hide();
					}	
					
				}
				
				
				
			/*  alert(id);
			alert(val);  */
			
			
			}
			
			
			
			function rofoperation2(val , id){
				
				if(id=='1truck_radius_of_operation')
				{
					$(".truck_radius_of_operation").val(val);
					if(val == 'other'){
						$('.show_truck_others').show();
					} else {
						$('.show_truck_others').hide();
					}
				}	
							else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').show();
					} else {
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').hide();
					}	
					
				}
			}

				
				
				
				
			
					
			
		function check_commodities_other(id){ 
			$("#"+id+" option:selected").each(function(){
			  val = $(this).text(); 
			  if(val == 'other'){ 
					$('.'+id+'_otr').show();
				} else { 
					$('.'+id+'_otr').hide();
				}
			});
		}
	
	
	$(document).ready(function(e) {
        $("body").on("keyup",".commhauled:first",function(){
			$(".commhauled").val($(this).val());
		});
		$("body").on("keyup",".commhauled2:first",function(){
			$(".commhauled2").val($(this).val());
		});
		
		
		 $("body").on("keyup",".cargo_class_price:first",function(){
			$(".cargo_class_price").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_price2:first",function(){
			$(".cargo_class_price2").val($(this).val());
		});
		
		
		$("body").on("keyup",".cargo_class_ded:first",function(){
			$(".cargo_class_ded").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_ded2:first",function(){
			$(".cargo_class_ded2").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp:first",function(){
			$(".specify_other_cp").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp2:first",function(){
			$(".specify_other_cp2").val($(this).val());
		});
			/**Dba required if name if blank or name required if dba is blank By Ashvin Patel 12/may/2014**/

		$('#insured_name').blur(function(){
			//alert();
			var f_name = $(this).val();
			var l_name = $('#insured_last_name').val();
			if(f_name==''||l_name=='')
			{
				$('#dba').attr('required', 'required');
				
			}
			else
			{
				$('#dba').removeAttr('required');
				$('#dba').removeClass('ui-state-error');
			}
		});
	
		$('#insured_last_name').blur(function(){
			//alert();
			var l_name = $(this).val();
			var f_name = $('#insured_name').val();
			if(f_name==''||l_name=='')
			{
				$('#dba').attr('required', 'required');
			}
			else
			{
				$('#dba').removeAttr('required');
				$('#dba').removeClass('ui-state-error');
			}
		});
		$('#dba').blur(function(){
			var dba = $(this).val();
			var f_name = $('#insured_name').val();
			if(dba=='')
			{
				$('#insured_name').attr('required', 'required');
				$('#insured_last_name').attr('required', 'required');
			}
			else
			{
				$('#insured_name').removeAttr('required');
				$('#insured_last_name').removeAttr('required');
				$('#insured_name').removeClass('ui-state-error');
				$('#insured_last_name').removeClass('ui-state-error');
			}
		});
    });
	

		
   
	
	
	
	<!-----updated by amit03-03-2014------>
	
	
	<!-----updated 13-10-2014------>
/*	
	jQuery(document).ready(function($) {

 if (window.history && window.history.pushState) {

    $(window).on('popstate', function() {
      var hashLocation = location.hash;
      var hashSplit = hashLocation.split("#!/");
      var hashName = hashSplit[1];
        
      if (hashName !== '') {
        var hash = window.location.hash;
		//alert(hash);
		var datastring = $("#app_form1").serialize();
		console.log(datastring);
        if (hash === '') {
		var val =$('#insert_id').val();
		alert(val);
		$.ajax({
		type: "POST",
		url: "<?php echo base_url('application_form/app_form');?>?id="+val+"", 
        data: datastring,
        dataType: "json",
         success: function(data) {
               var obj = jQuery.parseJSON(data); //if the dataType is not specified as json uncomment this
                // do what ever you want with the server response
				window.history.go(-1);
            },
            error: function(){
                 // alert('error handing here');
          }
	});	
        // alert('Back button was pressed.');
		 //$("#app_form").submit();
		 
        }else{
			
			 //
			 
			 alert('forward button was pressed.');
		     window.history.go(1);
			}
      }
    });

    window.history.pushState('forward', null, './#forward');
  }

});*/
	
</script>
       
       <script>
$(document).ready(function(){
	$("#agencies").change(function(){
		//window.location.href = window.location.href + "/" + $(this).val();
		$("#broker_email").val($(this).text());
		
		$.ajax({
			url: '<?php echo base_url();?>manualquote/setpublishersession',
			type: "POST",
			data: {
					action:'via_ajax',
					publisherid : $(this).val()
			},
			success: function(data) {
				
				window.location.reload(true);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(thrownError);
			}
		});
	});
});
function update_unknown(val){
	var owned_veh = $('#trailer_owned_vehicle'+val).val();


	if(owned_veh == 'no'){
		$("#vehicle_year_vh"+val).removeAttr("selected");
		$("#vehicle_year_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#make_model_vh"+val).removeAttr("selected");
		$("#make_model_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#trailer_type_veh"+val).removeAttr("selected");
		$("#trailer_type_veh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
	}else{
		$("#vehicle_year_vh"+val+" option[value='Unknown']").remove();
		$("#make_model_vh"+val+" option[value='Unknown']").remove();
		$("#trailer_type_veh"+val+" option[value='Unknown']").remove();
	}
	/* vehicle_year_vh1-2
	make_model_vh1-2
	trailer_type_veh1-2 */
	//alert();
}


	
	//$("#coverage_date").mask("99/99/9999");
	
	/* ('#dp3').datepicker({
			//format: 'mm/dd/yyyy'
		}); */

    /* $("input").each(function() {
      this.value = "";}) */
    //add_driver();
    //alert($(window).width()); 
	
/* 	$( ".number_limit" ).validate({
rules: {
field: {

range: [1, 99]
}
}
}); */
jQuery(document).ready(function(){
	$(".chzn-select").chosen({ allow_single_deselect: true });
	
	
	
	//contact_tel$('.fax').mask(9999-99999);
//	$('.tel').mask(999-999-9999-99999);
	//$('.cell').mask(999-999-9999);
	
});
</script>

,<!--<h2 style="text-align:center;">Application Sheet</h2>-->
<!--<form action="<?php echo base_url();?>application_form/losses/<?php echo  isset($app[0]->general_id) ? $app[0]->general_id :  '' ;?>" method="post" accept-charset="utf-8" id="main_form" enctype="multipart/form-data" novalidate="novalidate" id="app_form1"/>-->
<div class="well">

     <input type="hidden" name="quote_id" value="<?php echo isset($quote_id) ? $quote_id :'';?>"/>
<!--<input type="hidden" id="id_text_app_val" name="id_text_app_val[]" value="<?php echo implode(",",$app_val);?>"/>-->
<!--LOSS REPORT -->
<input type="hidden" name="insert_id" value="<?php echo isset($app[0]->general_id) ? $app[0]->general_id : '';?>" id="insert_id"/>
 <input type="hidden" name="ref_id" value="<?php echo isset($ref_id[0]->general_id) ? $ref_id[0]->general_id : '';?>"/>
 <input type="hidden" value="<?php echo isset($app_loss[0]->losses_id) ? $app_loss[0]->losses_id : ''; ?>" name="losses_id" id="losses_id" />
<fieldset class="loses_information_box">
	<legend>Loss(es) information</legend>
	<div class="row-fluid">
		<div class="span11">
		
		<label class="control-label pull-left pull-left">5. Does the application garage all vehicles at the above address? &nbsp;&nbsp;</label>
     
	
	   <?php $app_garage = isset($app_loss[0]->app_garage) ? $app_loss[0]->app_garage : '';?>
	   <select name="app_garage"  class="width_option h5-active ui-state-valid" required id="app_garage" onChange="applicant_above_address(this.value)">	
        <option value="">Select</option>	
               <option <?php if($app_garage  == 'No') echo 'selected="selected"'; ?> value="No">No</option>
			  <option <?php if($app_garage  == 'Yes') echo 'selected="selected"'; ?> value="Yes">Yes</option>
             
                   </select>
				   <strong>
				   <label class="control-label pull-left pull-left-1"></label>
 </strong>
			</div>
		
	</div>
	<script>
	
	jQuery(document).ready(function(){
	//var v=$('#app_garage').val();
	/*$('.check_zip').mask('9999-99999');
	$('.check_tel').mask('999-999-9999-99999');
	$('.check_tel').mask('9999-99999');
	$('.check_tel').mask('9999-99999');*/
	//alert(v);
	applicant_above_address($('#app_garage').val());
    });
	
	</script>
	
	 <!-- ............................................................................................................................---->
    <div class="container-fluid" id="above_address_ami" <?php echo ($app_garage=='No') ? '' : 'style="display:none;"' ; ?>>
      <div class="page-header">
        <h1>GHI-1 <small>Additional mailing address; Garaging or Terminal supplement form</small>
        </h1>
      </div>
      <div class="well">
	  <input class="textinput span3" type="hidden" value="<?php echo isset($app_ghi1a[0]->mail_id)? $app_ghi1a[0]->mail_id  :''; ?>" name="mail_id" placeholder="">
        
        <div class="row-fluid">
          <span class="span5">
		  <label class="control-label">5a. Mailing Address</label>
            <input class="textinput span12" type="text" value="<?php echo isset($app_ghi1a[0]->mail_address)? $app_ghi1a[0]->mail_address  :''; ?>" name="mail_address" placeholder="">
          </span>
      
          <span class="span2">
            <label class="control-label">City</label>
            <input class="textinput span12" type="text" value="<?php echo isset($app_ghi1a[0]->mail_city)? $app_ghi1a[0]->mail_city  :''; ?>" placeholder="" name="mail_city">
          </span>
          <span class="span2">
            <label class="control-label">County</label>
            <input class="textinput span12" type="text" value="<?php echo isset($app_ghi1a[0]->mail_country)? $app_ghi1a[0]->mail_country  :''; ?>" placeholder="" name="mail_country">
          </span>
          <span class="span1">
            <label class="control-label">State</label>
  			    <?php
									$attr = "class=' width_option ui-state-valid' id='mail_state'";
									$broker_state =   isset($app_ghi1a[0]->mail_state)? $app_ghi1a[0]->mail_state  :'';
									$driver_state = 'mail_state'
									?>
									<?php echo get_state_dropdown($driver_state, $broker_state, $attr); ?>
          </span>
          <span class="span2">
            <label class="control-label">Zip</label>
            <input class="textinput span8 check_zip" type="text" pattern="[0-9]+" maxlength="9" value="<?php echo isset($app_ghi1a[0]->mail_zip)? $app_ghi1a[0]->mail_zip  :''; ?>" placeholder="" name="mail_zip">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">5b. Contact Person</label>
	     <input class="textinput span4" type="text" name="contact_fname" value="<?php echo isset($app_ghi1a[0]->contact_fname)? $app_ghi1a[0]->contact_fname  :''; ?>" placeholder="First name">
        <input class="textinput span4" type="text" name="contact_mname" value="<?php echo isset($app_ghi1a[0]->contact_mname)? $app_ghi1a[0]->contact_mname  :''; ?>" placeholder="Middle name">
        <input class="textinput span4" type="text" placeholder="Last name" value="<?php echo isset($app_ghi1a[0]->contact_lname)? $app_ghi1a[0]->contact_lname  :''; ?>" name="contact_lname">
       
          </span>
		   <span class="span2">
            <label class="control-label">5c. Telephone</label>
            <input class="textinput tel span12" pattern="[0-9]+" maxlength="15" type="text" placeholder="" value="<?php echo isset($app_ghi1a[0]->contact_tel)? $app_ghi1a[0]->contact_tel  :''; ?>" name="contact_tel">
          </span>
		   <span class="span2">
            <label class="control-label">5e. Cellphone</label>
            <input class="textinput cell span12" pattern="[0-9]+" maxlength="15" type="text" placeholder="" value="<?php echo isset($app_ghi1a[0]->contact_cell)? $app_ghi1a[0]->contact_cell  :''; ?>" name="contact_cell">
          </span>
		   <span class="span2">
            <label class="control-label">5f. Fax</label>
            <input class="textinput fax span12" pattern="[0-9]+" maxlength="15" type="text" placeholder="" value="<?php echo isset($app_ghi1a[0]->contact_fax)? $app_ghi1a[0]->contact_fax  :''; ?>" name="contact_fax">
          </span>
		   <span class="span2">
            <label class="control-label">5g. Email</label>
            <input class="textinput email span12" type="text" placeholder="" value="<?php echo isset($app_ghi1a[0]->contact_email)? $app_ghi1a[0]->contact_email  :''; ?>" name="contact_email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$">
          </span>
        </div>
   
      <div class="row-fluid">
        <span class="span4" style="width: 300px;">
          <label class="control-label">5h. How many additional garaging locations?</label>
        </span>
        <span class="span1">
          <input class="textinput span7" type="text" placeholder="" id="garag_rows1" value="<?php if(isset($app_ghi1b)) { if(count($app_ghi1b)>0) { echo count($app_ghi1b); }}?>" onchange="garage_loc(this.value);" name="">
        </span>
        <span class="span7">
    <!-- <button class="btn"> <i class="icon icon-plus"></i> Add</button>-->
        </span>
      </div>
	  
	  <?php  if(!empty($app_ghi1b)) { $g1=1; foreach($app_ghi1b as $val) {?>
	 
	  <div id="add_garag_rows1<?php echo $g1;?>" class="add_garag_rows1">
	   <div class="well">
	  <input class="textinput span12" type="hidden" value="<?php echo isset($val->garage_id)? $val->garage_id  :''; ?>" name="garage_id[]" placeholder="">
	   <div class="row-fluid">
                 <span class="span12">
           <button class="btn pull-right" style="margin-bottom:5px;" onclick="persons_delete31(<?php echo $g1;?>);">Delete</button>
		   </span>
		   </div>  

		 <div class="row-fluid">
          <span class="span5">
            <label class="control-label">5ha. Garaging Location</label>
            <input class="textinput span12" type="text" value="<?php echo $val->garage_location;?>" name="garage_location[]" placeholder="">
          </span>
    
          <span class="span2">
            <label class="control-label">City</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo $val->garage_city;?>" name="garage_city[]">
          </span>
          <span class="span2">
            <label class="control-label">County</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo $val->garage_country;?>" name="garage_country[]">
          </span>
          <span class="span1">
            <label class="control-label">State</label>
		<?php $garage_country= $val->garage_country; ?>
		<?php
									$attr = "class=' width_option ui-state-valid'";
									$broker_state =   $val->garage_country;
									$driver_state = 'garage_state[]'
									?>
									<?php echo get_state_dropdown( $driver_state, $broker_state, $attr); ?>
           
          </span>
          <span class="span2">
            <label class="control-label">Zip</label>
            <input class="textinput span8 zip" pattern="[0-9]+" maxlength="9" value="<?php echo $val->garage_zip;?>"  type="text" placeholder="" name="garage_zip[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">5hb. Contact Person</label>
			 <input class="textinput span4" type="text" value="<?php echo $val->contacts_fname;?>" name="contacts_fname[]" placeholder="First name">
             <input class="textinput span4" type="text" value="<?php echo $val->contacts_mname;?>" name="contacts_mname[]" value="" placeholder="Middle name">
             <input class="textinput span4" type="text" value="<?php echo $val->contacts_fname;?>" placeholder="Last name" name="contacts_fname[]">
       
          </span>
		  
		   <span class="span2">
            <label class="control-label">5hc. Telephone</label>
            <input class="textinput tel span12" pattern="[0-9]+" maxlength="15" type="text" value="<?php echo $val->contacts_tel;?>" placeholder="" name="contacts_tel[]">
          </span>
          <span class="span2">
            <label class="control-label">5hd. Cellphone</label>
            <input class="textinput cell span12" pattern="[0-9]+" maxlength="15" type="text" value="<?php echo $val->contacts_cell;?>" placeholder="" name="contacts_cell[]">
          </span>
          <span class="span2">
            <label class="control-label">5.he Fax</label>
            <input class="textinput fax span12" pattern="[0-9]+" maxlength="15" type="text" value="<?php echo $val->contacts_fax;?>" placeholder="" name="contacts_fax[]">
          </span>
          <span class="span2">
            <label class="control-label">5hf. Email</label>
            <input class="textinput email span12" type="text" value="<?php echo $val->contacts_email;?>" placeholder="" name="contacts_email[]" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$">
          </span>
		  
		  
        </div>
     
		<?php 
		$values=explode(',',$val->contacts_checkbox); ?>
        <div class="row-fluid">
	
		
          <span class="span1">
            <label class="checkbox">
              <input type="checkbox" value="House" <?php if(in_array('House',$values)) echo 'checked'; ?>  name="contacts_checkbox[]">
              <span>House</span>
            </label>
          </span>
		
          <span class="span1">
            <label class="checkbox">
              <input type="checkbox" value="Storage" <?php if(in_array('Storage',$values)) echo 'checked'; ?>   name="contacts_checkbox[]">
              <span>Storage</span>
            </label>
          </span>
		 
          <span class="span1">
            <label class="checkbox">
              <input type="checkbox" value="Yard" <?php if(in_array('Yard',$values)) echo 'checked'; ?> name="contacts_checkbox[]">
              <span>Yard</span>
            </label>
          </span>
		
          <span class="span1">
            <label class="checkbox">
              <input type="checkbox" value="Street" <?php isset($pd_coverage) ? is_array($pd_coverage) ? in_array('Street',$pd_coverage) ? 'checked' : '' : '' : ''; ?> name="contacts_checkbox[]">
              <span>Street</span>
            </label>
          </span>
		 
          <span class="span4">
            <label class="control-label"></label>
          </span>
        </div>
	  </div>
	  </div>
	  <?php  $g1++; } } ?>
	  <div id="add_garag1">
	  </div>
	  
	  </div>
	  <div id="add_garaging_location" style="display:none">
	  
     
      <!--  <div>
          <button class="btn pull-right">Delete</button>
        </div>-->
        <div class="row-fluid">
          <span class="span5">
            <label class="control-label">5ha. Garaging Location</label>
            <input class="textinput span12" type="text" name="garage_location[]" placeholder="">
          </span>
		   <span class="span2">
            <label class="control-label">City</label>
            <input class="textinput span12" type="text" placeholder="" name="garage_city[]">
          </span>
          <span class="span2">
            <label class="control-label">County</label>
            <input class="textinput span12" type="text" placeholder="" name="garage_country[]">
          </span>
          <span class="span1">
            <label class="control-label">State</label>
           <?php
									$attr = "class=' width_option ui-state-valid'";
									$broker_state =   'CA';
									$driver_state = 'garage_state[]'
									?>
									<?php echo get_state_dropdown( $driver_state, $broker_state, $attr); ?>
          </span>
          <span class="span2">
            <label class="control-label">Zip</label>
            <input class="textinput span8" type="text" pattern="[0-9]+" maxlength="9" placeholder="" name="garage_zip[]">
          </span>
        </div>
       
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">5hb. Contact Person</label>
	            <input class="textinput span4" type="text" name="contacts_fname[]" placeholder="First name">
                <input class="textinput span4" type="text" name="contacts_mname[]" value="" placeholder="Middle name">
                <input class="textinput span4" type="text" placeholder="Last name" name="contacts_fname[]">
          </span>
		  
		       <span class="span2">
            <label class="control-label">5hc. Telephone</label>
            <input class="textinput span12" pattern="[0-9]+" maxlength="15" type="text" placeholder="" name="contacts_tel[]">
          </span>
          <span class="span2">
            <label class="control-label">5hd. Cellphone</label>
            <input class="textinput cell span12" pattern="[0-9]+" maxlength="15" type="text" placeholder="" name="contacts_cell[]">
          </span>
          <span class="span2">
            <label class="control-label">5he. Fax</label>
            <input class="textinput fax span12" pattern="[0-9]+" maxlength="15" type="text" placeholder="" name="contacts_fax[]">
          </span>
          <span class="span2">
            <label class="control-label">5hf. Email</label>
            <input class="textinput email span12" type="text" placeholder="" name="contacts_email[]" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$">
          </span>
        </div>
      <div class="row-fluid">
          <span class="span1">
            <label class="checkbox">
              <input type="checkbox" value="House" name="contacts_checkbox[]">
              <span>House</span>
            </label>
          </span>
          <span class="span1">
            <label class="checkbox">
              <input type="checkbox" value="Storage" name="contacts_checkbox[]">
              <span>Storage</span>
            </label>
          </span>
          <span class="span1">
            <label class="checkbox">
              <input type="checkbox" value="Yard" name="contacts_checkbox[]">
              <span>Yard</span>
            </label>
          </span>
          <span class="span1">
            <label class="checkbox">
              <input type="checkbox" value="Street" name="contacts_checkbox[]">
              <span>Street</span>
            </label>
          </span>
          <span class="span4">
            <label class="control-label"></label>
          </span>
        </div>
      
	 </div>
    <div>
        <div class="form-actions">
      <!--    <button class="btn btn-primary">Save</button>
          <button class="btn">Cancel</button>-->
        </div>
      </div>
    </div>
    <!-- ............................................................................................................................---->
	
	
	
    <div class="row-fluid">
    <div class="span12 lightblue">
	<div class="span5">
     <div class="span8">
        <label class="left_pull">6. Legal Business Status: &nbsp;</label>
		
		<?php $audit_name_losses = isset($app_loss[0]->audit_name_losses) ? $app_loss[0]->audit_name_losses : '';?>
	
		 
        <select class="left_pull select-4 " style="margin-right:5px; width: 110px;" name="audit_name_losses" id="audit_name1" onchange="audit1();">
		 <option value="">Select</option>
		 <option <?php if($audit_name_losses  == 'Individual') echo 'selected="selected"'; ?> value="Individual">Individual</option>
         <option <?php if($audit_name_losses  == 'Partnership') echo 'selected="selected"'; ?> value="Partnership">Partnership</option>
		 <option <?php if($audit_name_losses  == 'Corporation') echo 'selected="selected"'; ?> value="Corporation">Corporation</option>
         <option <?php if($audit_name_losses  == 'Other') echo 'selected="selected"'; ?> value="Other">Other</option>
		     </select>
             </div>
			 <div id="Specify_other" <?php echo ($audit_name_losses=='Other') ? '' : 'style="display:none;"'?> >
			 <input class="textinput span3"  type="text" data-autosize-input='{ "space": 40 }' value="<?php echo isset($app_loss[0]->audit_name_other) ? $app_loss[0]->audit_name_other : ''; ?>" placeholder="Other" name="audit_name_other" >
			      
            </div>
            </div>
			<span class="span6">
            <label class="control-label lbl_qt pull-left">SSN or Tax I.D&nbsp;</label>
            <select class="left_pull width_option" style="margin-right:5px;" name="SSN" id="SSN12" onchange="SSN1(this.value);">
            <?php $SSN= isset($app_loss[0]->SSN) ? $app_loss[0]->SSN : ''; ?>
		 <option value="">Select</option>
		 <option <?php if($SSN  == 'SSN') echo 'selected="selected"'; ?> value="SSN">SSN</option>
         <option <?php if($SSN  == 'Tax I.D.') echo 'selected="selected"'; ?> value="Tax I.D.">Tax I.D.</option>
         </select>
            <input class="textinput left_pull span3" type="text" name="audit_SSN" id="audit_SSN12" <?php echo isset($SSN) ? '' : 'style="display:none;"'?>  value="<?php echo isset($app_loss[0]->audit_SSN) ? $app_loss[0]->audit_SSN : ''; ?>" placeholder="">
        
         
          </span>
          <script>
		   $(document).ready(function(){
																	
	        SSN1($('#SSN12').val());
																	 
          });
		  </script>
     </div>
   <!--<div class="row-fluid">
	<div class="span12 lightblue">
	<label>Has the applicant, a business partner or any associate of the applicant ever filed for bankruptcy under any name in the past 10 years?</label>
	</div>
	<div class="row-fluid">
	 <select class="pull-left pull-left-4" name="">
              <option value="Select">Select</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
	</div>
	
	</div>-->
	   <div class="row-fluid">
	
	<label class="pull-left">7. Has the applicant, a business partner or any associate of the applicant ever filed for bankruptcy under any name in the past 10 years?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
	
	
	<?php $applicant_ever = isset($app_loss[0]->applicant_ever) ? $app_loss[0]->applicant_ever : '';?>
	 <select class="pull-left pull-left-4 width_option" name="applicant_ever" id="applicant_ever" onchange="applicant_business();" required>
     <option value="">Select</option>
            <option <?php if($applicant_ever  == 'No') echo 'selected="selected"'; ?> value="No">No</option>
          <option <?php if($applicant_ever  == 'Yes') echo 'selected="selected"'; ?> value="Yes">Yes</option>
          
            </select>
    </div>    
	<div id="explain_business1" class="row-fluid">
       <span class="span2">
          <label class="control-label">Year</label>
              <select name="exp_year" class="span6" id="exp_year">
                <?php $select_vehicle_year= isset($app_loss[0]->exp_year) ? $app_loss[0]->exp_year : ''; ?>
                 <option value="1985"  <?php if($select_vehicle_year  == '1985') echo 'selected="selected"'; ?>>1985</option>
                 <option value="1986"  <?php if($select_vehicle_year  == '1986') echo 'selected="selected"'; ?>>1986</option>
                                                                <option value="1987"  <?php if($select_vehicle_year  == '1987') echo 'selected="selected"'; ?>>1987</option>
                                                                <option value="1988"  <?php if($select_vehicle_year  == '1988') echo 'selected="selected"'; ?>>1988</option>
                                                                <option value="1989"  <?php if($select_vehicle_year  == '1989') echo 'selected="selected"'; ?>>1989</option>
																<option value="1990"  <?php if($select_vehicle_year  == '1990') echo 'selected="selected"'; ?>>1990</option>
                                                                <option value="1991"  <?php if($select_vehicle_year  == '1991') echo 'selected="selected"'; ?>>1991</option>
                                                                <option value="1992"  <?php if($select_vehicle_year  == '1992') echo 'selected="selected"'; ?>>1992</option>
                                                                <option value="1993"  <?php if($select_vehicle_year  == '1993') echo 'selected="selected"'; ?>>1993</option>
                                                                <option value="1994"  <?php if($select_vehicle_year  == '1994') echo 'selected="selected"'; ?>>1994</option>
                                                                <option value="1995"  <?php if($select_vehicle_year  == '1995') echo 'selected="selected"'; ?>> 1995</option>
                                                                <option value="1996"  <?php if($select_vehicle_year  == '1996') echo 'selected="selected"'; ?>>1996</option>
                                                                <option value="1997"  <?php if($select_vehicle_year  == '1997') echo 'selected="selected"'; ?>>1997</option>
                                                                <option value="1998"  <?php if($select_vehicle_year  == '1998') echo 'selected="selected"'; ?>>1998</option>
                                                                <option value="1999"  <?php if($select_vehicle_year  == '1999') echo 'selected="selected"'; ?>>1999</option>
                                                                <option value="2000"  <?php if($select_vehicle_year  == '2000') echo 'selected="selected"'; ?>>2000</option>
                                                                <option value="2001"  <?php if($select_vehicle_year  == '2001') echo 'selected="selected"'; ?>>2001</option>
                                                                <option value="2002"  <?php if($select_vehicle_year  == '2002') echo 'selected="selected"'; ?>>2002</option>
                                                                <option value="2003"  <?php if($select_vehicle_year  == '2003') echo 'selected="selected"'; ?>>2003</option>
                                                                <option value="2004"  <?php if($select_vehicle_year  == '2004') echo 'selected="selected"'; ?>>2004</option>
                                                                <option value="2005"  <?php if($select_vehicle_year  == '2005') echo 'selected="selected"'; ?>>2005</option>
                                                                <option value="2006"  <?php if($select_vehicle_year  == '2006') echo 'selected="selected"'; ?>>2006</option>
                                                                <option value="2007"  <?php if($select_vehicle_year  == '2007') echo 'selected="selected"'; ?>>2007</option>
                                                                <option value="2008"  <?php if($select_vehicle_year  == '2008') echo 'selected="selected"'; ?>>2008</option>
                                                                <option value="2009"  <?php if($select_vehicle_year  == '2009') echo 'selected="selected"'; ?>>2009</option>
                                                                <option value="2010"  <?php if($select_vehicle_year  == '2010') echo 'selected="selected"'; ?>>2010</option>
                                                                <option value="2011"  <?php if($select_vehicle_year  == '2011') echo 'selected="selected"'; ?>>2011</option>
                                                                <option value="2012"  <?php if($select_vehicle_year  == '2012') echo 'selected="selected"'; ?>>2012</option>
                                                                <option value="2013"  <?php if($select_vehicle_year  == '2013') echo 'selected="selected"'; ?>>2013</option>
																<option value="2014"  <?php if($select_vehicle_year  == '2014') echo 'selected="selected"'; ?>>2014</option>
                                                                <option value="2015"  <?php if($select_vehicle_year  == '2015') echo 'selected="selected"'; ?>>2015</option>
              </select>
                </span>
    		   <span class="span2" style="float:left;margin-left:-46px;">
            <label class="control-label pull-left">Case &nbsp;</label>
            <input class="textinput span11" type="text" placeholder="" value="<?php echo isset($app_loss[0]->exp_case) ? $app_loss[0]->exp_case : ''; ?>" name="exp_case">
          </span>
		   <span class="span1">
            <label class="control-label pull-left">Number &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($app_loss[0]->exp_number) ? $app_loss[0]->exp_number : ''; ?>" name="exp_number">
          </span>
		  
		    <span class="span1">
            <label class="control-label pull-left">County &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($app_loss[0]->exp_county) ? $app_loss[0]->exp_county : ''; ?>" name="exp_county">
          </span>
          <span class="span1">
            <label class="control-label pull-left ">State &nbsp;</label>
			<?php
									$attr = "class='span12 ui-state-valid exp_state' id='exp_state'";
									$broker_state =  isset($app_loss[0]->exp_state) ? $app_loss[0]->exp_state : ''; 
									
									?>
									<?php echo get_state_dropdown('exp_state', $broker_state, $attr); ?>
           
          </span>
        
            
            <textarea class="span10" placeholder="If yes, please explain"  name="explain_business" id="explain_business" required><?php echo isset($app_loss[0]->explain_business) ? trim($app_loss[0]->explain_business) : '';?></textarea>
        
	</div>
	
	
		   <div class="row-fluid">
	
    
	<label class="pull-left">8. Has the applicant ever been a partner, member or an associate of any other transportation firm or related field in the last five years?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
	
	<?php $applicant_transportation = isset($app_loss[0]->applicant_transportation) ? $app_loss[0]->applicant_transportation : '';?>
	 <select class="pull-left pull-left-4 width_option" name="applicant_transportation" id="applicant_transportation" onchange="applicant_transportation1();" required>
      <option value="">Select</option>
               <option <?php if($applicant_transportation  == 'No') echo 'selected="selected"'; ?> value="No">No</option>
          <option <?php if($applicant_transportation  == 'Yes') echo 'selected="selected"'; ?> value="Yes">Yes</option>
         
            </select>
			<label class="control-label pull-left pull-left-1">&nbsp; </label>
	
	
	<textarea class="span10" placeholder="If yes, please explain" name="explain_conducted"  id="explain_conducted" required><?php echo isset($app_loss[0]->explain_conducted) ? trim($app_loss[0]->explain_conducted) : '';?></textarea>
</div>	  
	  <div class="row-fluid">
	
	<label class="pull-left">9. Has the applicant, a business partner or any associate of the applicant conducted business under any other name in the past five years?&nbsp;</label>
	
	<?php $applicant_conducted = isset($app_loss[0]->applicant_conducted) ? $app_loss[0]->applicant_conducted : '';?>
	 <select class="pull-left pull-left-4 width_option" style="float:left"; name="applicant_conducted" id="applicant_conducted" onchange="applicant_conducted1(this.value);" required>
      <option value="">Select</option>
               <option <?php if($applicant_conducted  == 'No') echo 'selected="selected"'; ?> value="No">No</option>
          <option <?php if($applicant_conducted  == 'Yes') echo 'selected="selected"'; ?> value="Yes">Yes</option>
         
            </select>
			
			
		<!--	<a href="#addconducted"   class="fancybox edit" ><label class="pull-left edit_button" id="addconduct"  style="display:none;">Edit</label></a>
		    <!--<a href="<?php echo base_url();?>application_form/applicant_conducted/<?php echo isset($quote_id) ?$quote_id :'';?>"  data-fancybox-type="iframe" style="" class="fancybox edit"> <label class="pull-left edit_button">Edit&nbsp;&nbsp;&nbsp;&nbsp;</label></a>
			<a href="<?php echo base_url();?>application_form/applicant_conducted/<?php echo isset($quote_id) ?$quote_id :'';?>" ><label class="edit_button">Edit </label></a>-->
	
	
	</div>
    
      <div class="container-fluid" id="addconduct" <?php ($applicant_conducted=='Yes') ? '' : 'style="display:none;"' ;?>>
      <div class="page-header">
        <h1>GHI-8 <small>Previous / Subsidiary name supplement form</small>
        </h1>
      </div>
	 
      <div class="well">
      <?php //if(!empty($app_ghi8)) { ?>
     <input type="hidden"  name="app_edit_id" value="<?php echo isset($app_ghi8[0]->app_edit_id) ? $app_ghi8[0]->app_edit_id : '';?>"/> 
        <div class="row-fluid">
          <span class="span4">
            <div class="row-fluid">
              <span class="span12">
                <label class="control-label">9.a Applicant's Name</label>
                <input class="textinput span4" type="text" name="applicant_fname" value="<?php echo isset($app_ghi8[0]->applicant_fname) ? $app_ghi8[0]->applicant_fname : ''; ?>" placeholder="First name">
                <input class="textinput span4" type="text" placeholder="Middle name" value="<?php echo isset($app_ghi8[0]->applicant_mname) ? $app_ghi8[0]->applicant_mname : ''; ?>" name="applicant_mname">
                <input class="textinput span4" type="text" placeholder="Last name" value="<?php echo isset($app_ghi8[0]->applicant_lname) ? $app_ghi8[0]->applicant_lname : ''; ?>" name="applicant_lname">
              </span>
            </div>
          </span>
          <span class="span2">
            <label class="control-label">9.b DBA(if applicable)</label>
            <input class="textinput span12" type="text" name="applicant_dba" value="<?php echo isset($app_ghi8[0]->applicant_dba) ? $app_ghi8[0]->applicant_dba : ''; ?>" placeholder="DBA">
          </span>
		  
		   <span class="span3">
            <label class="control-label pull-left">9.c Address &nbsp;</label>
            <input class="textinput span11" type="text" placeholder="" value="<?php echo isset($app_ghi8[0]->applicant_address) ? $app_ghi8[0]->applicant_address : ''; ?>" name="applicant_address">
          </span>
		   <span class="span1">
            <label class="control-label pull-left"> City &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($app_ghi8[0]->applicant_city) ? $app_ghi8[0]->applicant_city : ''; ?>" name="applicant_city">
          </span>
		  
		    <span class="span1">
            <label class="control-label pull-left"> County</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($app_ghi8[0]->applicant_country) ? $app_ghi8[0]->applicant_country : ''; ?>" name="applicant_country">
          </span>
          <span class="span1">
            <label class="control-label pull-left "> State </label>
			<?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state =  isset($app_ghi8[0]->applicant_state) ? $app_ghi8[0]->applicant_state : ''; 
									
									?>
									<?php echo get_state_dropdown('applicant_state', $broker_state, $attr); ?>
           
          </span>
        </div>
       
        <div class="row-fluid">
         
        
          <span class="span2">
            <label class="control-label pull-left span12">Zip &nbsp;</label>
			<?php $app_zip= isset($value6->applicant_zip) ? explode("-",$value6->applicant_zip) : ''; ?>
            <input class="textinput span5" maxlength="5" type="text" style="margin-top: -10px;" pattern="[0-9]+" value="<?php echo isset($app_zip[0]) ? $app_zip[0] : ''; ?>" placeholder="" name="applicant_zip1" pattern="[0-9]+" maxlength="5">
			<input class="textinput span5" maxlength="4" type="text" style="margin-top: -10px;" pattern="[0-9]+" value="<?php echo isset($app_zip[1]) ? $app_zip[1] : ''; ?>" placeholder="" name="applicant_zip2" pattern="[0-9]+" maxlength="5">
          
		   </span>
		   
		          <span class="span3" >
            <label class="control-label pull-left span12" >9.d Date operation started &nbsp;</label>
            <input class="textinput pull-left input-small datepick" style="margin-top: -10px;"type="text" value="<?php echo isset($app_ghi8[0]->applicant_from) ? $app_ghi8[0]->applicant_from : ''; ?>" id="date_from" placeholder="mm/dd/yyyy" name="applicant_from">
          
        
            <label class="control-label pull-left">&nbsp; &nbsp;To &nbsp;</label>
            <input class="textinput input-small datepick" type="text" style="margin-top: -12px;float: right;" id="date_to" value="<?php echo isset($app_ghi8[0]->applicant_to) ?$app_ghi8[0]->applicant_to : ''; ?>" placeholder="mm/dd/yyyy" name="applicant_to">
       
         
		  </span>
		  
		    <span class="span2">
            <label class="control-label pull-left">9.e CA# &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($app_ghi8[0]->applicant_ca) ? $app_ghi8[0]->applicant_ca : ''; ?>" name="applicant_ca">
          </span>
          <span class="span2">
            <label class="control-label pull-left">9.f MC# &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($app_ghi8[0]->applicant_ma) ? $app_ghi8[0]->applicant_ma : ''; ?>" name="applicant_ma">
          </span>
          <span class="span2">
            <label class="control-label pull-left">9.g DOT# &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($app_ghi8[0]->applicant_dot) ? $app_ghi8[0]->applicant_dot : ''; ?>" name="applicant_dot">
          </span>
        </div>
   
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">9.h Is this entity still in operation? &nbsp;</label>
            <select name="applicant_entity" class="span1" id="applicant_entity" onChange="applicant_entity1(this.value)">
            <?php $applicant_entity = isset($value6->certificate_name) ? $value6->applicant_entity : '';?>
            <option value="Yes" <?php if($applicant_entity  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                <option value="No" <?php if($applicant_entity  == 'No') echo 'selected="selected"'; ?> >No</option>
                 
            </select>
            <script>
																
																 $(document).ready(function(){
																	
																     applicant_entity1($('#applicant_entity').val());
																	 
																 });
																 </script>
          </span>
        </div>
        <div class="row-fluid" id="applicant_role" >
          <span class="span12">
            <label class="control-label">9.i If yes, please give full details and role of who controls it.</label>
            <textarea class="span12" placeholder="" name="applicant_role"><?php echo isset($app_ghi8[0]->applicant_role) ? $app_ghi8[0]->applicant_role : ''; ?></textarea>
          </span>
        </div>
        <div class="row-fluid" id="applicant_explain" style="display:none;">
          <span class="span12">
            <label class="control-label">9.j If no, please give detail of termination and explain reason for termination.</label>
            <textarea class="span12" placeholder=""  name="applicant_explain"><?php echo isset($app_ghi8[0]->applicant_explain) ? $app_ghi8[0]->applicant_explain : ''; ?></textarea>
          </span>
        </div>
		<?php //} else { ?>
            
          <!--       <div class="row-fluid">
          <span class="span4">
            <div class="row-fluid">
              <span class="span12">
                <label class="control-label">Applicant's Name</label>
                <input class="textinput span4" type="text" name="applicant_fname" value="<?php echo isset($value6->applicant_fname) ? $value6->applicant_fname : ''; ?>" placeholder="First name">
                <input class="textinput span4" type="text" placeholder="Middle name" value="<?php echo isset($value6->applicant_mname) ? $value6->applicant_mname : ''; ?>" name="applicant_mname">
                <input class="textinput span4" type="text" placeholder="Last name" value="<?php echo isset($value6->applicant_lname) ? $value6->applicant_lname : ''; ?>" name="applicant_lname">
              </span>
            </div>
          </span>
          <span class="span2">
            <label class="control-label">DBA(if applicable)</label>
            <input class="textinput span12" type="text" name="applicant_dba" value="<?php echo isset($value6->applicant_dba) ? $value6->applicant_dba : ''; ?>" placeholder="DBA">
          </span>
		  
		   <span class="span3">
            <label class="control-label pull-left">Address &nbsp;</label>
            <input class="textinput span11" type="text" placeholder="" value="<?php echo isset($value6->applicant_address) ? $value6->applicant_address : ''; ?>" name="applicant_address">
          </span>
		   <span class="span1">
            <label class="control-label pull-left">City &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($value6->applicant_city) ? $value6->applicant_city : ''; ?>" name="applicant_city">
          </span>
		  
		    <span class="span1">
            <label class="control-label pull-left">County &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($value6->applicant_country) ? $value6->applicant_country : ''; ?>" name="applicant_country">
          </span>
          <span class="span1">
            <label class="control-label pull-left ">State &nbsp;</label>
			<?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state =  isset($value6->applicant_state) ? $value6->applicant_state : ''; 
									
									?>
									<?php echo get_state_dropdown('applicant_state', $broker_state, $attr); ?>
           
          </span>
        </div>
       
        <div class="row-fluid">
         
        
          <span class="span2">
            <label class="control-label pull-left span12">Zip &nbsp;</label>
			
            <input class="textinput span5" maxlength="5" type="text" style="margin-top: -10px;" value="<?php echo isset($value6->applicant_zip1) ? $value6->applicant_zip1 : ''; ?>" placeholder="" name="applicant_zip1" pattern="[0-9]+"  maxlength="5">
			<input class="textinput span5" maxlength="4" type="text" style="margin-top: -10px;" value="<?php echo isset($value6->applicant_zip2) ? $value6->applicant_zip2 : ''; ?>" placeholder="" name="applicant_zip2" pattern="[0-9]+"  maxlength="4">
          
		   </span>
		   
		          <span class="span3" >
            <label class="control-label pull-left span12" >Date operation started &nbsp;</label>
            <input class="textinput pull-left input-small datepick" style="margin-top: -10px;"type="text" value="<?php echo isset($value6->applicant_from) ? $value6->applicant_from : ''; ?>" id="date_from" placeholder="mm/dd/yyyy" name="applicant_from">
          
        
            <label class="control-label pull-left">&nbsp; &nbsp;To &nbsp;</label>
            <input class="textinput input-small datepick" type="text" style="margin-top: -35px;float: right;" id="date_to" value="<?php echo isset($value6->applicant_to) ? $value6->applicant_to : ''; ?>" placeholder="mm/dd/yyyy" name="applicant_to">
       
         
		  </span>
		  
		    <span class="span2">
            <label class="control-label pull-left">CA#  &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($value6->applicant_ca) ? $value6->applicant_ca : ''; ?>" name="applicant_ca">
          </span>
          <span class="span2">
            <label class="control-label pull-left">MC# &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($value6->applicant_ma) ? $value6->applicant_ma : ''; ?>" name="applicant_ma">
          </span>
          <span class="span2">
            <label class="control-label pull-left">DOT# &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($value6->applicant_dot) ? $value6->applicant_dot : ''; ?>" name="applicant_dot">
          </span>
        </div>
   
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">Is this entity still in operation? &nbsp;</label>
            <select name="applicant_entity" class="span1" id="applicant_entity" onChange="applicant_entity1(this.value)">
            <?php $applicant_entity = isset($value6->certificate_name) ? $value6->applicant_entity : '';?>
            <option value="Yes" <?php if($applicant_entity  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                <option value="No" <?php if($applicant_entity  == 'No') echo 'selected="selected"'; ?> >No</option>
                 
            </select>
       
          </span>
        </div>
        <div class="row-fluid" id="applicant_role" >
          <span class="span12">
            <label class="control-label">If yes, please give full details and role of who controls it.</label>
            <textarea class="span12" placeholder=""  name="applicant_role"><?php echo isset($value6->applicant_role) ? $value6->applicant_role : ''; ?></textarea>
          </span>
        </div>
        <div class="row-fluid" id="applicant_explain" style="display:none;">
          <span class="span12">
            <label class="control-label">If no, please give detail of termination and explain reason for termination.</label>
            <textarea class="span12" placeholder=""  name="applicant_explain"><?php echo isset($value6->applicant_explain) ? $value6->applicant_explain : ''; ?></textarea>
          </span>
        </div>-->
            
            
        <?php //} ?>
      </div>
   <!--   <div>
        <div class="form-actions">
          <button class="btn btn-primary" onclick="fancybox_close();" name="save1">Save</button>
          <button class="btn" onclick="reload_close();">Cancel</button>
        </div>
      </div>-->
    </div>
    
	<div class="row-fluid">
	<div class="lightblue span6">
        <label class="left_pull">10. Do you have any losses in  the last three years ?&nbsp; </label>
		
			<?php  $losses_need = isset($app_loss[0]->losses_need) ? $app_loss[0]->losses_need : isset($yes) ;?>
	
			
			 <select name="losses_need" id="losses_need" required="required" class="h5-active ui-state-valid width_option" style="margin-left: 0px;">
             <option value="">Select</option>
           <option <?php if($losses_need  == 'No') echo 'selected="selected"'; ?> value="No">No</option>
          <option <?php if($losses_need  == 'Yes') echo 'selected="selected"'; ?> value="Yes">Yes</option>
		
		
	
		  
            </select>
    </div>
    
  
	<span id="row_hide_los" <?php ($losses_need=='Yes') ? '' : 'style="display:none;"'; ?>>
			<div class="span6 ">
	<label class="left_pull">10a. Are you going to provide loss run? &nbsp; </label>
	
	
         <?php $yes=''; if(!empty($app_loss1) || !empty($app_loss2) || !empty($app_loss3)){$yes = 'Yes';}  $losses_name = isset($app_loss[0]->losses_name) ? $app_loss[0]->losses_name : $yes;?>
		
        <select class="pull-left pull-left-4 width_option" id="pull_left_yes" name="losses_name" onChange="los_name(this.value);" required>
         <option value="">Select</option>
          <option <?php if($losses_name  == 'No') echo 'selected="selected"'; ?> value="No">No</option>  
          <option <?php if($losses_name  == 'Yes') echo 'selected="selected"'; ?> value="Yes">Yes</option>
          
        </select>
			</div>	
			</span>
			    <script>
																
																 $(document).ready(function(){
																	
																     los_name($('#pull_left_yes').val());
																	 
																 });
																 </script>
	</div>
	
	
<input type="hidden" id="id_text_app_val" name="id_text_app_val[]" value="<?php echo isset($app_val[0]) ? $app_val[0] : '';?>"/>
 	
	

 <div id="losses_div" class="losses_div" <?php ($losses_need=='Yes') ? '' : 'style="display:none;"'; ?>>
 
 <div class="row-fluid " id="losses_below" style="display:none">
	<div class="row-fluid">
         <label> Please upload the loss run or fill the info below </label>
        </div>
	</div>


			
	
	<div class="row-fluid " id="losses_lia" <?php (in_array('Liability', $apps)) ? '' : 'style="display:none"'; ?> >		
		 <div class="well" style="padding-bottom: 65px;">	
			<div id="" style="">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span5">
						10aa. Number of Liability losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_liability')" class="span_width1 number_limit" value="<?php if(isset($app_loss1)){ if(count($app_loss1)==0){ }else echo count($app_loss1);}?>">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid" >  
					<div class="span12 lightblue">
						<table id="losses_liability_table" class="table table-bordered table-striped table-hover" 
						<?php isset($app_loss1) ? (count($app_loss1)>0) ? '' : 'style="display:none"': 'style="display:none"'; ?>>
							<thead> 
							  <tr>
                              <!--Start Ashvin Patel 14nov2014-->
								<th colspan="2">Policy Period </th>
                                <th rowspan="2">Cliam</th>                                
                                <th rowspan="2">Accident Location</th>
								<th rowspan="2">Liability <br> Amount</th>
								<th rowspan="2">Insurance Company</th>
                                <!--End Ashvin Patel 14nov2014-->
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_liability_row">
							<?php 
							if(isset($app_loss1)){
								if(is_array($app_loss1)){
							$i=1; foreach($app_loss1 as $value) {?>
                           
							<script>
				var i=1;			
	 $(document).ready(function() {
		
	          date('date_from_liability'+i,'date_to_liability'+i);
		
				
		 i++;
	 });
	
							</script>
                            
<tr>
                         <input type="hidden" value="<?php echo isset($value->losses_id) ? $value->losses_id : ''; ?>" name="losses_liability_id[]" id="lia_id" />                         
                   <input type="hidden" value="<?php echo isset($value->losses_type) ?$value->losses_type : 'losses_liability'; ?> " id="losses_liability_sec" name="losses_liability_type[]">
							
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value->losses_from_date) ? date("m/d/Y",strtotime($value->losses_from_date)) : ''; ?>" name="losses_liability_from[]"  id=<?php echo "date_from_liability".$i; ?> class="span12  datepick date_from_losses_liability" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value->losses_to_date) ? date("m/d/Y",strtotime($value->losses_to_date)) : ''; ?>" name="losses_liability_to[]"  id=<?php echo"date_to_liability".$i ?> class="span12 datepick date_to_losses_liability" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        
        <!--End Ashvin Patel 14/nov/2014-->
        <td>
        <input type="text" id="claim_losses" class="span12" name="losses_liability_cliam[]" value="<?php echo isset($value->losses_claim) ? $value->losses_claim : ''; ?>">
        </td>
        <td>
        <input type="text" id="ac_location_losses_liability" class="span6" style="float: left;margin-right: 2px;" name="losses_liability_city[]" value="<?php echo isset($value->losses_ac_city) ? $value->losses_ac_city : ''; ?>">
        <?php $loss_state = isset($value->losses_ac_state) ? $value->losses_ac_state : 'CA'; ?>
		<?php get_state_dropdown('losses_liability_ac_state[]', $loss_state, 'class="span6"'); ?>
        </td>
        <td><!--<input required="required" type="text" name="losses_amount[]" value="" class="span12 price_format" maxlength="15" >-->
         <?php $loss_amount = isset($value->losses_amount) ? $value->losses_amount : ''; ?>
         <select name="losses_liability_amount[]" class="span12" required="required">
			<option value="open" <?= ($loss_amount=='open') ? 'selected' : ''; ?>>open</option>
			<option value="closed" <?= ($loss_amount=='closed') ? 'selected' : ''; ?>>closed</option>
			<option value="reserved" <?= ($loss_amount=='reserved') ? 'selected' : ''; ?>>reserved</option>
		</select>
         </td>
        <!--End Ashvin Patel 14/nov/2014-->
        
        
        <td><input type="text" name="losses_liability_company[]" value="<?php echo isset($value->losses_company) ? $value->losses_company : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_liability_general_agent[]" value="<?php echo isset($value->losses_general_agent) ? $value->losses_general_agent : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_liability_date_of_loss[]" value="<?php echo isset($value->date_of_loss) ? date("m/d/Y",strtotime($value->date_of_loss)) : ''; ?>" class="span12 datepick <?php echo "losses_liability_date_of_loss".$i; ?> " maxlength="10" ></td>
		</tr>
							
							<?php $i++; }
							}
							}?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="span12"> 
				  <div class="span6 lightblue1">
                                    <label class="loses_information_box" style="float:left;">Attach Loss Report(s) for all Accident(s)</label>
                                
									<input type="button" id="first_uploading1" value="Upload">
				
                              <?php $file_value1=array(); if(isset($app_loss1)) { foreach($app_loss1 as $values1){ ?>
<?php  $file_value1[]= isset($values1->uploaded_files_values) ? $values1->uploaded_files_values : ''; ?>
<?php } } $file_values1 = isset($file_value1)? implode(',',$file_value1):'';?>
<?php $file= isset($file_values1) ? explode(',',$file_values1) : ''; ?>
<?php if(!empty($file)){ for($i=0;$i<count($file);$i++) { 
                 if(isset($file[$i])){	 ?>						

									<input type="hidden" name="losses_liability_values1[]" id="uploaded_files_values123" value="<?php echo  $file[$i]; ?>">
                                    <br/>
             
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } 
			  } }else{?>
			    <input type="hidden" name="losses_liability_values1[]" id="uploaded_files_values123" value="">
                                    <br/>
			  <?php } ?>
									<ul id="upload_file_name1" class="upload_file_name1"></ul>
                                </div>
						</div>
					
					
		    </div> 
        
        </div>
        
    <input type="hidden" value="<?php echo isset($los_id[0]) ? $los_id[0] : ''; ?>" name="los_id" />
    
    
    
		<div class="row-fluid " id="losses_pd" <?php (in_array('Physical Damage', $apps)) ? '' : 'style="display:none"'; ?>>
			
		 <div class="well" style="padding-bottom: 65px;">
			
			<div id="" style="">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span5">
						10ab. Number of P.D. losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_pd')" class="span_width1 number_limit" value="<?php if(isset($app_loss2)) { if(count($app_loss2)==0){ }else {echo count($app_loss2);} };?>">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_pd_table" class="table table-bordered table-striped table-hover" 
						<?php isset($app_loss2) ? (count($app_loss2)>0) ? '' : 'style="display:none"' : 'style="display:none"'; ?>>
							<thead> 
							  <tr>
                              <!--Start Ashvin Patel 14nov2014-->
								<th colspan="2">Policy Period</th>
                                <th rowspan="2">Cliam</th>
                                <th rowspan="2">Accident Location</th>
								<th rowspan="2">P.D. <br> Amount</th>
								<th rowspan="2">Insurance Company</th>
                                <!--End Ashvin Patel 14nov2014-->
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_pd_row">
                            <?php if(isset($app_loss2)){
								if(is_array($app_loss2)){
								
								?>
							<?php $i=1; foreach($app_loss2 as $value2) {?>
                            
                            						<script>
				var j=1;			
	 $(document).ready(function() {
		
	          date('date_from_pd'+j,'date_to_pd'+j);
		
				
		 j++;
	 });
	
							</script>
                          
							<tr>
		<input type="hidden" value="<?php echo isset($value2->losses_id) ? $value2->losses_id : ''; ?>" name="losses_pd_id[]"   id="pd_id"/>
        <input type="hidden" value="<?php echo isset($value2->losses_type) ?$value2->losses_type : 'losses_pd'; ?> " id="losses_pd_sec" name="losses_pd_type[]">       
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_from_date) ? date("m/d/Y",strtotime($value2->losses_from_date)) : ''; ?>" name="losses_pd_from[]"  id=<?php echo "date_from_pd".$i;?> class="span12  datepick date_from_pd" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_to_date) ? date("m/d/Y",strtotime($value2->losses_to_date)) : ''; ?>" name="losses_pd_to[]"  id=<?php echo "date_to_pd".$i;?> class="span12 datepick date_to_pd" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        
        <!--End Ashvin Patel 14/nov/2014-->
        <td>
        <input type="text" id="claim_losses" class="span12" name="losses_pd_cliam[]" value="<?php echo isset($value2->losses_claim) ? $value2->losses_claim : ''; ?>">
        </td>
        <td>
        <input type="text" id="ac_location_losses_liability" class="span6" style="float: left;margin-right: 2px;" name="losses_pd_city[]" value="<?php echo isset($value2->losses_ac_city) ? $value2->losses_ac_city : ''; ?>">
        <?php $loss_state = isset($value2->losses_ac_state) ? $value2->losses_ac_state : 'CA'; ?>
		<?php get_state_dropdown('losses_pd_ac_state[]', $loss_state, 'class="span6"'); ?>
        </td>
        <td><!--<input required="required" type="text" name="losses_amount[]" value="" class="span12 price_format" maxlength="15" >-->
         <?php $loss_amount = isset($value2->losses_amount) ? $value2->losses_amount : ''; ?>
         <select name="losses_pd_amount[]" class="span12" required="required">
			<option value="open" <?= ($loss_amount=='open') ? 'selected' : ''; ?>>open</option>
			<option value="closed" <?= ($loss_amount=='closed') ? 'selected' : ''; ?>>closed</option>
			<option value="reserved" <?= ($loss_amount=='reserved') ? 'selected' : ''; ?>>reserved</option>
		</select>
         </td>
        <!--End Ashvin Patel 14/nov/2014-->
       
        <td><input type="text" name="losses_pd_company[]" value="<?php echo isset($value2->losses_company) ? $value2->losses_company : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_pd_general_agent[]" value="<?php echo isset($value2->losses_general_agent) ? $value2->losses_general_agent : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_pd_date_of_loss[]" value="<?php echo isset($value2->date_of_loss) ? date("m/d/Y",strtotime($value2->date_of_loss)) : ''; ?>" class="span12 datepick <?php echo "losses_pd_date_of_loss".$i;?>" maxlength="10" ></td>
		</tr>
							
							<?php $i++; }
								}
							}?>
							
							
							</tbody>
						</table>
					</div>
				</div>
			</div> 
			<div class="span12"> 
				  <div class="span6 lightblue1">
                                    <label class="loses_information_box" style="float:left;">Attach Loss Report(s) for all Accident(s)</label>
                               
									<input type="button" id="first_upload1" value="Upload">
									
<?php $file_value2=array(); if(isset($app_loss2)) { foreach($app_loss2 as $value2){  ?>
<?php  $file_value2[]= isset($value2->uploaded_files_values) ? $value2->uploaded_files_values : ''; ?>
<?php } } $file_values2 = isset($file_value2)? implode(',',$file_value2):'';?>
<?php $file= isset($file_values2) ? explode(',',$file_values2) : ''; 
      if(!empty($file)){ ?>
<?php for($i=0;$i<count($file);$i++) {  ?>
             	<?php if(isset($file[$i])){ ?>
									<input type="hidden" name="losses_pd_values1[]" id="uploaded_files_values1" value="<?php echo  isset($file[$i])? $file[$i]:'';?>">
                                    <br/>
                                                                          
             
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
              <?php } }else{ ?>
                		<input type="hidden" name="losses_pd_values1[]" id="uploaded_files_values1" value="">
               
                             
								<?php }?>	
									<ul id="upload_file_name2" class="upload_file_name1"></ul>
                                </div>
							</div>
		</div> 
    </div>
		<div class="row-fluid " id="losses_cargo" <?php (in_array('Cargo', $apps)) ? '' : 'style="display:none"'; ?>>
		
             <div class="well" style="padding-bottom: 65px;">
            			<div id="" style="">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span8">
						10ac. Number of Cargo losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_cargo')" class=" span_width1 number_limit" value="<?php if(isset($app_loss3)){ if(count($app_loss3)==0){ } else { echo count($app_loss3); } }?>">
						</div>
					</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_cargo_table" class="table table-bordered table-striped table-hover" <?php (in_array('Cargo', $apps)) ? '' : 'style="display:none"'; ?>>
							<thead> 
							  <tr>
                              <!--Start Ashvin Patel 14nov2014-->
								<th colspan="2">Policy Period</th>
                                <th rowspan="2">Cliam</th>
                                <th rowspan="2">Accident Location</th>
								<th rowspan="2">Cargo <br> Amount</th>
								<th rowspan="2">Insurance Company</th>
                                <!--End Ashvin Patel 14nov2014-->
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_cargo_row">
                            
                            <?php $i=1; if(isset($app_loss3)){
								if(is_array($app_loss3)){
								foreach($app_loss3 as $value2) {?>
                           						<script>
				var k=1;			
	 $(document).ready(function() {
		
	          date('date_from_cargo'+k,'date_to_cargo'+k);
		
				
		 k++;
	 });
	
							</script>

                          	<tr>
			<input type="hidden" value="<?php echo isset($value2->losses_id) ? $value2->losses_id : ''; ?>" name="losses_cargo_id[]" id="cargo_id"/>
            <input type="hidden" value="<?php echo isset($value2->losses_type) ?$value2->losses_type : 'losses_cargo'; ?> " id="losses_cargo_sec" name="losses_cargo_type[]">
						
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_from_date) ? date("m/d/Y",strtotime($value2->losses_from_date)) : ''; ?>" name="losses_cargo_from[]"  id=<?php echo "date_from_cargo".$i;?> class="span12  datepick date_from_cargo" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_to_date) ? date("m/d/Y",strtotime($value2->losses_to_date)) : ''; ?>" name="losses_cargo_to[]"  id=<?php echo "date_to_cargo".$i;?> class="span12 datepick date_to_cargo" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        
        <!--End Ashvin Patel 14/nov/2014-->
        <td>
        <input type="text" id="claim_losses" class="span12" name="losses_cargo_cliam[]" value="<?php echo isset($value2->losses_claim) ? $value2->losses_claim : ''; ?>">
        </td>
        <td>
        <input type="text" id="ac_location_losses" class="span6" style="float: left;margin-right: 2px;" name="losses_cargo_city[]" value="<?php echo isset($value2->losses_ac_city) ? $value2->losses_ac_city : ''; ?>">
        <?php $loss_state = isset($value2->losses_ac_state) ? $value2->losses_ac_state : 'CA'; ?>
		<?php get_state_dropdown('losses_cargo_ac_state[]', $loss_state, 'class="span6"'); ?>
        </td>
        <td><!--<input required="required" type="text" name="losses_amount[]" value="" class="span12 price_format" maxlength="15" >-->
         <?php $loss_amount = isset($value2->losses_amount) ? $value2->losses_amount : ''; ?>
         <select name="losses_cargo_amount[]" class="span12" required="required">
			<option value="open" <?= ($loss_amount=='open') ? 'selected' : ''; ?>>open</option>
			<option value="closed" <?= ($loss_amount=='closed') ? 'selected' : ''; ?>>closed</option>
			<option value="reserved" <?= ($loss_amount=='reserved') ? 'selected' : ''; ?>>reserved</option>
		</select>
         </td>
        <!--End Ashvin Patel 14/nov/2014-->
        
      
        <td><input type="text" name="losses_cargo_company[]" value="<?php echo isset($value2->losses_company) ? $value2->losses_company : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_cargo_general_agent[]" value="<?php echo isset($value2->losses_general_agent) ? $value2->losses_general_agent : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_cargo_date_of_loss[]" value="<?php echo isset($value2->date_of_loss) ? date("m/d/Y",strtotime($value2->date_of_loss)) : ''; ?>" class="span12 datepick <?php echo "losses_cargo_date_of_loss".$i;?>" maxlength="10" ></td>
		</tr>
							
							<?php $i++;} 
							
							  }
							}
							
							?>
                            
                              
                            
							</tbody>
						</table>
					</div>
				</div>
			</div> 
			 <div class="span12">
				  <div class="span8 lightblue1">
                                    <label class="loses_information_box" style="float:left;">Attach Loss Report(s) for all Accident(s)</label>
                            
									<input type="button" id="first_upload2" value="Upload">
									
	<?php $file_value3=array(); if(isset($app_loss3)) { foreach($app_loss3 as $value2){  ?>
<?php  $file_value3[]= isset($value2->uploaded_files_values) ? $value2->uploaded_files_values : ''; ?>
<?php } } $file_values3 = isset($file_value3)? implode(',',$file_value3):'';?>
<?php $file= isset($file_values3) ? explode(',',$file_values3) : ''; ?>
<?php if(!empty($file)) { for($i=0;$i<count($file);$i++) { 
                 if(isset($file[$i])){			 ?>
				 
									<input type="hidden" name="losses_cargo_values1[]" id="uploaded_files_values2" value="<?php echo  isset($file[$i])? $file[$i]:'';  ?>">
                                    <br/>
                                      
             
	
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php }  } } else { ?>
			  			<input type="hidden" name="losses_cargo_values1[]" id="uploaded_files_values2" value="">
                                    <br/>
                           
			  <?php } ?>
									<ul id="upload_file_name3" class="upload_file_name1"></ul>
                                </div>
						</div>
		</div> 
       </div>
			<div class="row-fluid " id="losses_bobtail" <?php (in_array('Bobtail', $apps)) ? '' : 'style="display:none"'; ?>>
		
       
         <div class="well" style="padding-bottom: 65px;">
			<div id="" style="">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span8">
						10ad. Number of Bobtail losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_bobtail')" class=" span_width1 number_limit" value="<?php if(isset($app_loss4)){if(count($app_loss4)==0){ }else echo count($app_loss4);}?>">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_bobtail_table" class="table table-bordered table-striped table-hover" <?php (in_array('Bobtail', $apps)) ? '' : 'style="display:none"'; ?>>
							<thead> 
							  <tr>
                              <!--Start Ashvin Patel 14nov2014-->
								<th colspan="2">Policy Period</th>
                                <th rowspan="2">Cliam</th>
                                <th rowspan="2">Accident Location</th>
								<th rowspan="2">Bobtail <br> Amount</th>
								<th rowspan="2">Insurance Company</th>
                                <!--End Ashvin Patel 14nov2014-->
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_bobtail_row">
                            
                                               
                            <?php 
							if(isset($app_loss4)){
							
							$i=1; foreach($app_loss4 as $value2) {?>
                            
                            						<script>
				var l=1;			
	 $(document).ready(function() {
		
	          date('date_from_bobtail'+l,'date_to_bobtail'+l);
		
				
		 l++;
	 });
	
							</script>
                            
                              <tr>
	    <input type="hidden" value="<?php echo isset($value2->losses_id) ? $value2->losses_id : ''; ?>" name="losses_bobtail_id[]" id="bobtail_id" />
	    <input type="hidden" value="<?php echo isset($value2->losses_type) ?$value2->losses_type : 'losses_bobtail'; ?> " id="losses_bobtail_type" name="losses_bobtail_type[]">
                        						  
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_from_date) ? date("m/d/Y",strtotime($value2->losses_from_date)) : ''; ?>" name="losses_bobtail_from[]"  id=<?php echo "date_from_bobtail".$i;?> class="span12  datepick date_from_bobtail" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_to_date) ? date("m/d/Y",strtotime($value2->losses_to_date)) : ''; ?>" name="losses_bobtail_to[]"  id=<?php echo "date_to_bobtail".$i;?> class="span12 datepick date_to_bobtail" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        
        <!--End Ashvin Patel 14/nov/2014-->
        <td>
        <input type="text" id="claim_losses" class="span12" name="losses_bobtail_cliam[]" value="<?php echo isset($value2->losses_claim) ? $value2->losses_claim : ''; ?>">
        </td>
        <td>
        <input type="text" id="ac_location_losses" class="span6" style="float: left;margin-right: 2px;" name="losses_bobtail_city[]" value="<?php echo isset($value2->losses_ac_city) ? $value2->losses_ac_city : ''; ?>">
        <?php $loss_state = isset($value2->losses_ac_state) ? $value2->losses_ac_state : 'CA'; ?>
		<?php get_state_dropdown('losses_bobtail_ac_state[]', $loss_state, 'class="span6"'); ?>
        </td>
        <td><!--<input required="required" type="text" name="losses_amount[]" value="" class="span12 price_format" maxlength="15" >-->
         <?php $loss_amount = isset($value2->losses_amount) ? $value2->losses_amount : ''; ?>
         <select name="losses_bobtail_amount[]" class="span12" required="required">
			<option value="open" <?= ($loss_amount=='open') ? 'selected' : ''; ?>>open</option>
			<option value="closed" <?= ($loss_amount=='closed') ? 'selected' : ''; ?>>closed</option>
			<option value="reserved" <?= ($loss_amount=='reserved') ? 'selected' : ''; ?>>reserved</option>
		</select>
         </td>
        <!--End Ashvin Patel 14/nov/2014-->
        
        
        <td><input type="text" name="losses_bobtail_company[]" value="<?php echo isset($value2->losses_company) ? $value2->losses_company : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_bobtail_general_agent[]" value="<?php echo isset($value2->losses_general_agent) ? $value2->losses_general_agent : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_bobtail_date_of_loss[]" value="<?php echo isset($value2->date_of_loss) ? date("m/d/Y",strtotime($value2->date_of_loss)) : ''; ?>" class="span12 datepick <?php "losses_bobtail_date_of_loss".$i;?>"  maxlength="10" ></td>
		</tr>
							
	<?php $i++; } } ?>
                            
                            
							</tbody>
						</table>
					</div>
				</div>
			 
			</div><div class="span12 ">
				  <div class="span8 lightblue1">
                                    <label class="loses_information_box" style="float:left;">Attach Loss Report(s) for all Accident(s)</label>
                            
									<input type="button" id="first_upload3" value="Upload">
						<?php $file_value4=array(); if(isset($app_loss4)) { foreach($app_loss4 as $value2){  ?>
<?php  $file_value4[]= isset($value2->uploaded_files_values) ? $value2->uploaded_files_values : '';  ?>
<?php } } $file_values4 = isset($file_value4)? implode(',',$file_value4):'';?>
<?php $file= isset($file_values4) ? explode(',',$file_values4) : ''; ?>
<?php if(!empty($file)){ for($i=0;$i<count($file);$i++) { 
                 if(isset($file[$i])){			 ?>				

									<input type="hidden" name="losses_bobtail_values1[]" id="uploaded_files_values3" value="<?php echo  $file[$i];  ?>">
                                    
            
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } } } else { ?>
			  <input type="hidden" name="losses_bobtail_values1[]" id="uploaded_files_values3" value="<?php echo  $file[$i];  ?>">
			  <?php } ?>
			  <ul id="upload_file_name4" class="upload_file_name1" style="margin-top: 20px;"></ul>
                                </div>
							</div>
		  </div> 
      </div>
			<div class="row-fluid " id="losses_dead" <?php (in_array('Dead head', $apps)) ? '' : 'style="display:none"'; ?>>
	
            
            <div class="well" style="padding-bottom: 65px;">
			<div id="" style="">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span10">
						10ae. Number of Dead head losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_dead')" class=" span_width1 number_limit" value="<?php if(isset($app_loss5)){if(count($app_loss5)==0){ }else echo count($app_loss5);}?>">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_dead_table" class="table table-bordered table-striped table-hover" <?php (in_array('Dead head', $apps)) ? '' : 'style="display:none"'; ?>>
							<thead> 
							  <tr>
                              <!--Start Ashvin Patel 14nov2014-->
								<th colspan="2">Policy Period</th>
                                <th rowspan="2">Cliam</th>
                                <th rowspan="2">Accident Location</th>
								<th rowspan="2">Dead head <br> Amount</th>
								<th rowspan="2">Insurance Company</th>
                                <!--End Ashvin Patel 14nov2014-->
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_dead_row">
                            
                                  <?php if(isset($app_loss5)){
								 $i=1; foreach($app_loss5 as $value2) {?>
                                  
                                						<script>
				var m=1;			
	 $(document).ready(function() {
		
	          date('date_from_dead'+m,'date_to_dead'+m);
		
				
		 m++;
	 });
	
							</script>  
                                  
                                  
                               <tr>
		 <input type="hidden" value="<?php echo isset($value2->losses_id) ? $value2->losses_id : ''; ?>" name="losses_dead_id[]" id="dead_id"/>
							
         <input type="hidden" value="<?php echo isset($value2->losses_type) ?$value2->losses_type : 'losses_dead'; ?> " id="losses_dead_type" name="losses_dead_type[]">
                        					   
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_from_date) ? date("m/d/Y",strtotime($value2->losses_from_date)) : ''; ?>" name="losses_dead_from[]"  id= <?php echo "date_from_dead".$i;?> class="span12  datepick date_from_dead" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_to_date) ? date("m/d/Y",strtotime($value2->losses_to_date)) : ''; ?>" name="losses_dead_to[]"  id=<?php echo "date_to_dead".$i;?> class="span12 datepick date_to_dead" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        
        <!--End Ashvin Patel 14/nov/2014-->
        <td>
        <input type="text" id="claim_losses" class="span12" name="losses_dead_cliam[]" value="<?php echo isset($value2->losses_claim) ? $value2->losses_claim : ''; ?>">
        </td>
        <td>
        <input type="text" id="ac_location_losses" class="span6" style="float: left;margin-right: 2px;" name="losses_dead_city[]" value="<?php echo isset($value2->losses_ac_city) ? $value2->losses_ac_city : ''; ?>">
        <?php $loss_state = isset($value2->losses_ac_state) ? $value2->losses_ac_state : 'CA'; ?>
		<?php get_state_dropdown('losses_dead_ac_state[]', $loss_state, 'class="span6"'); ?>
        </td>
        <td><!--<input required="required" type="text" name="losses_amount[]" value="" class="span12 price_format" maxlength="15" >-->
         <?php $loss_amount = isset($value2->losses_amount) ? $value2->losses_amount : ''; ?>
         <select name="losses_dead_amount[]" class="span12" required="required">
			<option value="open" <?= ($loss_amount=='open') ? 'selected' : ''; ?>>open</option>
			<option value="closed" <?= ($loss_amount=='closed') ? 'selected' : ''; ?>>closed</option>
			<option value="reserved" <?= ($loss_amount=='reserved') ? 'selected' : ''; ?>>reserved</option>
		</select>
         </td>
        <!--End Ashvin Patel 14/nov/2014-->
        
        
        <td><input type="text" name="losses_dead_company[]" value="<?php echo isset($value2->losses_company) ? $value2->losses_company : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_dead_general_agent[]" value="<?php echo isset($value2->losses_general_agent) ? $value2->losses_general_agent : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_dead_date_of_loss[]" value="<?php echo isset($value2->date_of_loss) ? date("m/d/Y",strtotime($value2->date_of_loss)) : ''; ?>" class="span12 datepick <?php echo "losses_dead_date_of_loss".$i;?>" maxlength="10" ></td>
		</tr>
							
							<?php $i++; } } ?>
                            
                            
							
		                   </tbody>
		                 </table>
		           </div>
				</div>
		</div> 
			 <div class="span12">
				  <div class="span8 lightblue1">
                                    <label class="loses_information_box" style="float:left;">Attach Loss Report(s) for all Accident(s)</label>
                                
									<input type="button" id="first_upload" value="Upload">
								<?php $file_value5=array();; if(isset($app_loss5)) { foreach($app_loss5 as $value2){  ?>
<?php  $file_value5[]= isset($value2->uploaded_files_values) ? $value2->uploaded_files_values : '';  ?>
<?php } } $file_values5 = isset($file_value5)? implode(',',$file_value5):'';?>
<?php $file= isset($file_values5) ? explode(',',$file_values5) : ''; ?>
<?php if(!empty($file)) { for($i=0;$i<count($file);$i++) { 
                 if(isset($file[$i])){			 ?>					

			  <input type="hidden" name="losses_dead_values1[]" id="uploaded_files_values" value="<?php echo  $file[$i];  ?>">
                                   		 
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } } } else {?>
			  
			  <input type="hidden" name="losses_dead_values1[]" id="uploaded_files_values" value="">
                                   
			  
			  <?php } ?>
			  <ul id="upload_file_name" class="upload_file_name1" style="margin-top: 20px;"></ul>
                                </div>
								
					</div>
						
		</div> 
     </div>
    		<div class="row-fluid " id="losses_trunk" <?php (in_array('Non Trucking', $apps)) ? '' : 'style="display:none"'; ?>>
		
       <div class="well" style="padding-bottom: 65px;">
			<div id="" style="">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span10">
						10af. Number of Non Trucking losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_trunk')" class=" span_width1 number_limit" value="<?php if(isset($app_loss6)){if(count($app_loss6)==0){ }else echo count($app_loss6);}?>">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_trunk_table" class="table table-bordered table-striped table-hover" <?php (in_array('Non Trucking', $apps)) ? '' : 'style="display:none"'; ?>>
							<thead> 
							  <tr>
                              <!--Start Ashvin Patel 14nov2014-->
								<th colspan="2">Policy Period</th>
                                <th rowspan="2">Cliam</th>
                                <th rowspan="2">Accident Location</th>
								<th rowspan="2">Non Trucking<br> Amount</th>
								<th rowspan="2">Insurance Company</th>
                                <!--End Ashvin Patel 14nov2014-->
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_trunk_row">
                            
                                <?php if(isset($app_loss6)){ $i=1; foreach($app_loss6 as $value2) {?>
                                
                                
                                						<script>
				var n=1;			
	 $(document).ready(function() {
		
	          date('date_from_trunk'+n,'date_to_trunk'+n);
		
				
		 n++;
	 });
	
							</script>
                            
                            
                            <tr>
		 <input type="hidden" value="<?php echo isset($value2->losses_id) ? $value2->losses_id : ''; ?>" name="losses_trunk_id[]" id="trunk_id" />
		 <input type="hidden" value="<?php echo isset($value2->losses_type) ?$value2->losses_type : 'losses_trunk'; ?> " id="losses_trunk_sec" name="losses_trunk_type[]">
                           					
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_from_date) ? date("m/d/Y",strtotime($value2->losses_from_date)) : ''; ?>" name="losses_trunk_from[]"  id=<?php echo "date_from_trunk".$i; ?> class="span12  datepick date_from_trunk" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_to_date) ? date("m/d/Y",strtotime($value2->losses_to_date)) : ''; ?>" name="losses_trunk_to[]"  id=<?php echo "date_to_trunk".$i; ?> class="span12 datepick date_to_trunk" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        
        <!--End Ashvin Patel 14/nov/2014-->
        <td>
        <input type="text" id="claim_losses" class="span12" name="losses_trunk_cliam[]" value="<?php echo isset($value2->losses_claim) ? $value2->losses_claim : ''; ?>">
        </td>
        <td>
        <input type="text" id="ac_location_losses" class="span6" style="float: left;margin-right: 2px;" name="losses_trunk_city[]" value="<?php echo isset($value2->losses_ac_city) ? $value2->losses_ac_city : ''; ?>">
        <?php $loss_state = isset($value2->losses_ac_state) ? $value2->losses_ac_state : 'CA'; ?>
		<?php get_state_dropdown('losses_trunk_ac_state[]', $loss_state, 'class="span6"'); ?>
        </td>
        <td><!--<input required="required" type="text" name="losses_amount[]" value="" class="span12 price_format" maxlength="15" >-->
         <?php $loss_amount = isset($value2->losses_amount) ? $value2->losses_amount : ''; ?>
         <select name="losses_trunk_amount[]" class="span12" required="required">
			<option value="open" <?= ($loss_amount=='open') ? 'selected' : ''; ?>>open</option>
			<option value="closed" <?= ($loss_amount=='closed') ? 'selected' : ''; ?>>closed</option>
			<option value="reserved" <?= ($loss_amount=='reserved') ? 'selected' : ''; ?>>reserved</option>
		</select>
         </td>
        <!--End Ashvin Patel 14/nov/2014-->
        
       
        <td><input type="text" name="losses_trunk_company[]" value="<?php echo isset($value2->losses_company) ? $value2->losses_company : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_trunk_general_agent[]" value="<?php echo isset($value2->losses_general_agent) ? $value2->losses_general_agent : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_trunk_date_of_loss[]" value="<?php echo isset($value2->date_of_loss) ? date("m/d/Y",strtotime($value2->date_of_loss)) : ''; ?>" class="span12 datepick <?php echo "losses_trunk_date_of_loss".$i;?>" maxlength="10" ></td>
		</tr>
							
							<?php $i++; } } ?>
                            
                            
                            
                            
							</tbody>
						</table>
					</div>
				</div>
			</div> 
			 <div class="span12">
				  <div class="span8 lightblue1">
                                    <label class="loses_information_box" style="float:left;">Attach Loss Report(s) for all Accident(s)</label>
                                
									<input type="button" id="first_upload4" value="Upload">
				<?php $file_value6=array(); if(isset($app_loss6)) { foreach($app_loss6 as $value2){  ?>
<?php  $file_value6[]= isset($value2->uploaded_files_values) ? $value2->uploaded_files_values : '';  ?>
<?php } } $file_values6 = isset($file_value6)? implode(',',$file_value6):'';?>
<?php $file= isset($file_values6) ? explode(',',$file_values6) : ''; ?>
<?php if(!empty($file)) { for($i=0;$i<count($file);$i++) { 
                 if(isset($file[$i])){	 ?>							

					<input type="hidden" name="losses_trunk_values1[]" id="uploaded_files_values4" value="<?php echo  $file[$i];  ?>">
              
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } } }else{ ?>
                    <input type="hidden" name="losses_trunk_values1[]" id="uploaded_files_values4" value="">                     
              <?php } ?>
                        <ul id="upload_file_name5" class="upload_file_name1" style="margin-top: 20px;"></ul>
							   </div>
								</div>
		</div> 
      </div>
				<div class="row-fluid  " id="losses_excess" <?php (in_array('Excess', $apps)) ? '' : 'style="display:none"'; ?>>
                <div class="well" style="padding-bottom: 65px;">
		
    
			<div id="" style="">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span10">
						10ag. Number of Excess losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_excess')" class=" span_width1 number_limit" value="<?php  if(isset($app_loss7)) { if(count($app_loss7)==0){ }else echo count($app_loss7); } ?>">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_excess_table" class="table table-bordered table-striped table-hover" <?php (in_array('Excess', $apps)) ? '' : 'style="display:none"'; ?>>
							<thead> 
							  <tr>
                              <!--Start Ashvin Patel 14nov2014-->
								<th colspan="2">Policy Period</th>
                                <th rowspan="2">Cliam</th>
                                <th rowspan="2">Accident Location</th>
								<th rowspan="2">Excess <br> Amount</th>
								<th rowspan="2">Insurance Company</th>
                                <!--End Ashvin Patel 14nov2014-->
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_excess_row">
                            
                                <?php if(isset($app_loss7)) { $i=1; foreach($app_loss7 as $value2) { ?>
                                
                                						<script>
				var o=1;			
	 $(document).ready(function() {
		
	          date('date_from_excess'+o,'date_to_excess'+o);
		
				
		 o++;
	 });
	
							</script>
                                
                              <tr>
		  <input type="hidden" value="<?php echo isset($value2->losses_id) ? $value2->losses_id : ''; ?>" name="losses_excess_id[]" id="excess_id" />
		  <input type="hidden" value="<?php echo isset($value2->losses_type) ?$value2->losses_type : 'losses_excess'; ?> " id="losses_excess_type" name="losses_excess_type[]">	
                        					  
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_from_date) ? date("m/d/Y",strtotime($value2->losses_from_date)) : ''; ?>" name="losses_excess_from[]"  id=<?php echo "date_from_excess".$i; ?>class="span12  datepick date_from_excess" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        <td><input required="required" maxlength="10" type="text" value="<?php echo isset($value2->losses_to_date) ? date("m/d/Y",strtotime($value2->losses_to_date)) : ''; ?>" name="losses_excess_to[]"  id=<?php echo "date_to_excess".$i; ?> class="span12 datepick date_to_excess" maxlength="10" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ></td>
        
        <!--End Ashvin Patel 14/nov/2014-->
        <td>
        <input type="text" id="claim_losses" class="span12" name="losses_excess_cliam[]" value="<?php echo isset($value2->losses_claim) ? $value2->losses_claim : ''; ?>">
        </td>
        <td>
        <input type="text" id="ac_location_losses" class="span6" style="float: left;margin-right: 2px;" name="losses_excess_city[]" value="<?php echo isset($value2->losses_ac_city) ? $value2->losses_ac_city : ''; ?>">
        <?php $loss_state = isset($value2->losses_ac_state) ? $value2->losses_ac_state : 'CA'; ?>
		<?php get_state_dropdown('losses_excess_ac_state[]', $loss_state, 'class="span6"'); ?>
        </td>
         <td><!--<input required="required" type="text" name="losses_amount[]" value="" class="span12 price_format" maxlength="15" >-->
         <?php $loss_amount = isset($value2->losses_amount) ? $value2->losses_amount : ''; ?>
         <select name="losses_excess_amount[]" class="span12" required="required">
			<option value="open" <?= ($loss_amount=='open') ? 'selected' : ''; ?>>open</option>
			<option value="closed" <?= ($loss_amount=='closed') ? 'selected' : ''; ?>>closed</option>
			<option value="reserved" <?= ($loss_amount=='reserved') ? 'selected' : ''; ?>>reserved</option>
		</select>
         </td>
        <!--End Ashvin Patel 14/nov/2014-->        
       
        <td><input type="text" name="losses_excess_company[]" value="<?php echo isset($value2->losses_company) ? $value2->losses_company : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_excess_general_agent[]" value="<?php echo isset($value2->losses_general_agent) ? $value2->losses_general_agent : ''; ?>" class="span12 " maxlength="60" ></td>
        <td><input type="text" name="losses_excess_date_of_loss[]" value="<?php echo isset($value2->date_of_loss) ? date("m/d/Y",strtotime($value2->date_of_loss)) : ''; ?>" class="span12 datepick <?php echo "losses_excess_date_of_loss".$i; ?>" maxlength="10" ></td>
		</tr>
							
							<?php  $i++; } } ?>
                            
                            
							</tbody>
						</table>
					</div>
				</div>
			</div> 
            
			 <div class="span12">
			  <div class="span8 lightblue1">
                                    <label class="loses_information_box" style="float:left;">Attach Loss Report(s) for all Accident(s)</label>
                                  
									<input type="button" id="first_upload5" value="Upload">
						<?php $file_value7=array(); if(isset($app_loss7)) { foreach($app_loss7 as $value2){  ?>
<?php  $file_value7[]= isset($value2->uploaded_files_values) ? $value2->uploaded_files_values : '';  ?>
<?php } } $file_values7 = isset($file_value7)? implode(',',$file_value7):'';?>
<?php $file= isset($file_values7) ? explode(',',$file_values7) : ''; ?>
<?php if(!empty($file)){ for($i=0;$i<count($file);$i++) { 
                 if(isset($file[$i])){			 ?>						
                                   
			  <input type="hidden" name="losses_excess_values1[]" id="uploaded_files_values5" value="<?php echo  $file[$i];  ?>">
                           
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } } } else { ?>	
			  
			  <input type="hidden" name="losses_excess_values1[]" id="uploaded_files_values5" value="">
                           
			  <?php } ?>
			  <ul id="upload_file_name6" class="upload_file_name1" style="margin-top: 20px;"></ul>
                           </div>
						   </div>
		</div> 
       </div>
          

		              <div class="row-fluid">                                
                                <div class="span6 lightblue">
                                <label class="loses_information_box" style="float:left;">Attach Loss Report(s) for all Accident(s)</label>
                                	<input type="button" id="first_upload25" value="Upload">
									
<br/>
									<input type="hidden" name="loss_report_filename" id="uploaded_files_values25" value="<?php echo !empty($app_loss[0]->loss_report_filename) ? $app_loss[0]->loss_report_filename : isset($upload_file[0]->loss_report_filename);  ?>">
                                    <?php 
									 $file_name=!empty($app_loss[0]->loss_report_filename) ? $app_loss[0]->loss_report_filename : isset($upload_file[0]->loss_report_filename);
									$file=explode(",",$file_name); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>	<ul id="upload_file_name25" class="upload_file_name1" style="margin-top: -30px;"></ul>
                                </div>
                            </div>
</div>
                              <div class="row-fluid">
     
            <label class="control-label control-label-1"></label>
          
        </div>
			


</fieldset>
                          <!--   <fieldset id="section5" class="fillings_section_box">
                                <legend>Filing Section</legend>
						<!--MAYANK-->
  
                                    <div class="row-fluid">  
                                        
										<script type="text/javascript">
											$(document).ready(function(){
												$("#filings_need").change(function(){
													if($(this).val()=="yes") {
														$("#number_of_filings_div").show();
														$('#number_of_filings').attr('required', 'required'); 
														
													}else{
														$("#number_of_filings_div").hide();
														$('#number_of_filings').removeAttr('required'); 
														}
												});
											});
											
											/*function add_filing(val){
													if(val >=0 ){
														if(val == 0)
														{
															$("#filing_table").hide();
														}
														//$("#filing_table").show();
														$("#row_filing").show();
														//$("#add_filing_row").html('');
														var i =1;
														var row = '<tr>\
														<td><input type="text" name="filing_type[]" class="span12 filing_type" required="required" /></td> \
														<td><input type="text" name="filing_numbers[]" class="span12 filing_numbers" required="required"="required" /></td> \
														</tr>';
														var currcount = $("#add_filing_row tr").length;
														var diff = currcount - val;
														
														 if(diff < 0) {
																while(diff < 0){
																	var v = $('#add_filing_row').append(row) ; 
																	diff++;
																	
																} 
															 }
															 else if(diff > 0)
															 {
																
																 while(diff > 0){
																
																	 var v = $('#add_filing_row tr:last').remove() ; 
																	 diff--;
																 }
															 }	
															
														apply_on_all_elements();
													}
												}*/
										</script>
									<!--	<div class="span12 lightblue">
											<label>Do you need Filing / Filings :</label>
											<select id="filings_need" name="filings_need" required="required">
												<option value="">--Select--</option>
												<option value="yes">YES</option>
												<option value="no">NO</option>
											</select>
                                        </div>
										
										<div class="span12 lightblue" id="number_of_filings_div" style="display:none;">
											<label>Number of Filings :</label>
											<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" id="number_of_filings" name="number_of_filings" class="span1 filing_numbers" onchange="add_filing(this.value)">
										</div>
										
										<div id="row_filing" style="display:none;">
											<div class="row-fluid">  
												<div class="span6 lightblue">
													<table id="filing_table" class="table table-bordered table-striped table-hover">
														<thead>
															<tr>
																<th>Filing Type:</th>
																<th>Filing Num.</th>
															</tr>
														</thead>
														<tbody id="add_filing_row">
														</tbody>
													</table>
												</div>
											</div>
										</div> 
									</div>							
		                                <!--END OF MAYANK-->
 <input type="hidden" id="id_text_app_val" name="id_text_app_val[]" value="<?php echo isset($app_val[0]) ? $app_val[0] : '';?>"/>

 
   </div>
	
   </div>
     
						
                    
                         
                     
                         <?php $this->load->view('application_vehicle');?>
                         
                         
	

       
<?php 
/*if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}*/
?>
<script>
	 $(document).ready(function() {
	  var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		 var _startDate1 = new Date(); //todays date
		var _endDate1 = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		$('#date_from_effective').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to_expiration').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to_expiration').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate1 = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from_effective').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		
		
      $('#date_from_effective1').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate1 = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to_expiration1').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to_expiration1').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from_effective1').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		
		
		if($('#applicant_ever').val() == "Yes"){
					  $("#explain_business").show();
					   $("#explain_business1").show();
					}else{
					  $("#explain_business").hide();
					   $("#explain_business1").hide();
					}
		if($('#applicant_conducted').val() == "Yes"){
					  $("#explain_conducted").show();
					}else{
					  $("#explain_conducted").hide();
					}
					
					
					
					 	if($('#losses_need').val()=="Yes")
			{ 
				$("#losses_div").show();
				$("#row_hide_los").show();
				var arr=$('#id_text_app_val').val();
			
				myarray = arr.split(',');
             
				//alert(myarray[0]);
				//alert($.inArray( "Liability", arr ));
				if($.inArray( "Liability", myarray )> -1){
				$("#losses_lia").show();
				//$("#filings_id").show();
				 
				}else{
				$("#losses_lia").hide();
				//$("#filings_id").hide();
				}
				if($.inArray( "Physical Damage", myarray )> -1){
				$("#losses_pd").show();
				}else{
				$("#losses_pd").hide();
				}
				if($.inArray( "Cargo", myarray )> -1){
				$("#losses_cargo").show();
				}else{
				$("#losses_cargo").hide();
				}
				if($.inArray( "Bobtail", myarray )> -1){
				$("#losses_bobtail").show();
				}else{
				$("#losses_bobtail").hide();
				}
				if($.inArray( "Dead head", myarray )> -1){
				$("#losses_dead").show();
				}else{
				$("#losses_dead").hide();
				}
				if($.inArray( "Non Trucking", myarray )> -1){
				$("#losses_trunk").show();
				}else{
				$("#losses_trunk").hide();
				}
				if($.inArray( "Excess", myarray )> -1){
				$("#losses_excess").show();
				}else{
				$("#losses_excess").hide();
				}
				
			}
			else
			{
				$("#losses_div").hide();
				$("#row_hide_los").hide();
			}
		
		});
		
		
		
	/* 	$("#click_add_phone").click(function(){
          // $("#add_phone").show();
		   $( "#add_phone" ).slideToggle( "slow", function() {
    // Animation complete.
	if ($.trim($('input.textinput.span1').val()) != '')
{
      $('input.textinput.span1.phone').val('');
	  }
	  if ($.trim($('#select_home_p').val()) != '')
{
	   $('#select_home_p').val('Home');
	   }
	   if ($.trim($('#textinput_att_p').val()) != '')
{
	  $('#textinput_att_p').val('');
	  
}
  });
          });
		 	$("#click_add_email").click(function(){
             $( "#add_email1" ).slideToggle( "slow", function() {
    // Animation complete.
	if ($.trim($('input.textinput.email').val()) != '')
{
      $('input.textinput.email').val('');
	  }
	  	if ($.trim($('#select_home_e').val()) != '')
{
	   $('#select_home_e').val('Home');
	   }	if ($.trim($('#textinput_att_e').val()) != '')
{
	  $('#textinput_att_e').val('');
}
  });
          });
		  */

		  
		  
        </script>
        <script>
	/*$(document).ready(function(){

	
	
		//when the Add Filed button is clicked
		$("#add_phone_row").click(function (e) {
			//alert();
			//Append a new row of code to the "#items" div
			$("#table_phone").append('<tr><td><input class="textinput span1" type="text" placeholder="Area" id="insured_telephone1" name="insured_telephone_add[]" maxlength="3"  pattern="[0-9]+"  value="" required="required"="required">&nbsp;<input class="textinput span1" id="insured_telephone2" type="text" placeholder="Prefix" name="insured_telephone_add[]" maxlength="4"  pattern="[0-9]+"  value="" required="required"="">&nbsp;<input class="textinput span1" id="insured_telephone3" type="text" placeholder="Sub" name="insured_telephone_add[]" maxlength="3"  pattern="[0-9]+"  value="" required="required"="">&nbsp;<input class="textinput span1" type="text" placeholder="Ext" insured_tel_ext_add[]></td><td><select name="insured_telephone_type[]" class="select-1"><option value="Home">Home</option><option value="Business">Business</option></select></td><td><input class="textinput" type="text" placeholder="" name="insured_telephone_attention[]"></td><td> <button class="btn btn-mini btn-1 remove_phone_row" id="remove_phone_row"><i class="icon icon-remove"></i></button></td></tr>');
		});

	$("body").on("click", ".remove_phone_row", function (e) {
		//alert($(this).parent("div"));
		$(this).closest('tr').remove();
	});*/
	$(document).ready(function(){
		if($('#applicant_transportation').val()=='Yes')
	  {
	  
	  $('#explain_conducted').show();
	  
	  
	  } else{
	  
	  $('#explain_conducted').hide();
	  }
	
	
	if($('#applicant_conducted').val()=='Yes')
	  {
	  
	  $('#addconduct').show();
	  
	  
	  } else{
	  
	  $('#addconduct').hide();
	  }
	  
	  
	  
	  	if($('#pull_left_yes').val()=='Yes')
	  {
	  
	  $('#row_hide_los').show();
	  
	  
	  } else{
	  
	  $('#row_hide_los').hide();
	  }
	  
	 
   
	  
	  
	   	if($('#lia_id').val()>0)
	  {
	  
	  $('#losses_liability_div').show();
	  $('#losses_liability_table').show();
	  
	  } else{
	  $('#losses_liability_div').hide();
	  $('#losses_liability_table').hide();
	  }
	  
	  
      
      
	    	if($('#cargo_id').val()>0)
	  {
	  
	  $('#losses_cargo_div').show();
	  $('#losses_cargo_table').show();
	  
	  } else{
	  $('#losses_cargo_div').hide();
	  $('#losses_cargo_table').hide();
	  }
      
          	if($('#bobtail_id').val()>0)
	  {
	  
	  $('#losses_bobtail_div').show();
	  $('#losses_bobtail_table').show();
	  
	  } else{
	  $('#losses_bobtail_div').hide();
	  $('#losses_bobtail_table').hide();
	  }
      
            
          	if($('#dead_id').val()>0)
	  {
	  
	  $('#losses_dead_div').show();
	  $('#losses_dead_table').show();
	  
	  } else{
	  $('#losses_dead_div').hide();
	  $('#losses_dead_table').hide();
	  }
      
      
           	if($('#trunk_id').val()>0)
	  {
	  
	  $('#losses_trunk_div').show();
	  $('#losses_trunk_table').show();
	  
	  } else{
	  $('#losses_trunk_div').hide();
	  $('#losses_trunk_table').hide();
	  }
	  
                	if($('#excess_id').val()>0)
	  {
	  
	  $('#losses_excess_div').show();
	  $('#losses_excess_table').show();
	  
	  } else{
	  $('#losses_excess_div').hide();
	  $('#losses_excess_table').hide();
	  }
	  
      
	      	if($('#pd_id').val()>0)
	  {
	  
	  $('#losses_pd_div').show();
	  $('#losses_pd_table').show();
	  
	  } else{
	  $('#losses_pd_div').hide();
	  $('#losses_pd_table').hide();
	  }
	  
	  
	});
	
	
	$(document).ready(function(){
		
		//when the Add Filed button is clicked
	/*	$("#add_email_row").click(function (e) {
			//alert();
			//Append a new row of code to the "#items" div
			$("#table_email").append('<tr><td><input class="textinput" type="text" placeholder="" id="email" name="insured_email_add[]" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30"></td><td><select name="insured_email_type[]" class="select-1"><option value="Home">Home</option><option value="Business">Business</option></select></td><td><input class="textinput" type="text" placeholder="" name="insured_email_attention[]"></td><td> <button class="btn btn-mini btn-1 remove_phone_row" id="remove_phone_row"><i class="icon icon-remove"></i></button></td></tr>');
		});

	$("body").on("click", ".remove_email_row", function (e) {
		//alert($(this).parent("div"));
		$(this).closest('tr').remove();
	});
	});*/
		function audit() {
    var val=$("#audit_name").val();
	
	if(val === "Other"){
			$("#Specify").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify").hide();
		}
		}
	function audit1() {
    var val=$("#audit_name1").val();
	
	if(val === "Other"){
		
			$("#Specify_other").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify_other").hide();
		}
		}
		
		
		function application_select(){
		//remarks_id_show
		

				//alert(myarray[0]);
				//alert($.inArray( "Liability", arr ));
				
		var foo = []; 
$('#app_id :selected').map(function(i, selected){ 
  foo.push($(this).val()); 
 
});
var val_app1=foo;




		$("#id_text_app_val").val(val_app1);
		var arr=$('#id_text_app_val').val();
				myarray = arr.split(',');
			$("#losses_div").show();
				$("#row_hide_los").show();
		if($('#losses_need').val()=="yes")
			{
		

				//alert(myarray[0]);
				//alert($.inArray( "Liability", arr ));
				if($.inArray( "Liability", myarray )> -1){
				$("#losses_lia").show();
				}else{
				$("#losses_lia").hide();
				}
				if($.inArray( "Physical Damage", myarray )> -1){
				$("#losses_pd").show();
				}else{
				$("#losses_pd").hide();
				}
				if($.inArray( "Cargo", myarray )> -1){
				$("#losses_cargo").show();
				}else{
				$("#losses_cargo").hide();
				}
				if($.inArray( "Bobtail", myarray )> -1){
				$("#losses_bobtail").show();
				}else{
				$("#losses_bobtail").hide();
				}
				if($.inArray( "Dead head", myarray )> -1){
				$("#losses_dead").show();
				}else{
				$("#losses_dead").hide();
				}
				if($.inArray( "Non Trucking", myarray )> -1){
				$("#losses_trunk").show();
				}else{
				$("#losses_trunk").hide();
				}
				if($.inArray( "Excess", myarray )> -1){
				$("#losses_excess").show();
				}else{
				$("#losses_excess").hide();
				}				
				}
				if($.inArray( "Liability", myarray )> -1){
				//$("#losses_lia").show();
				$("#filings_id").show();
				 
				}else{
				//$("#losses_lia").hide();
				$("#filings_id").hide();
				}
				if(($.inArray( "Liability", myarray )> -1) || ($.inArray( "Cargo", myarray )> -1) ){
				//$("#losses_lia").show();
				$("#badge_id_2").show();
				$("#badge_id_4").show();
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_2").hide();
				$("#badge_id_4").hide();
				}
				if($.inArray( "Cargo", myarray )> -1){
				//$("#losses_lia").show();
				$("#badge_id_3").show();
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_3").hide();
				}
if(($.inArray( "Liability", myarray )> -1) || ($.inArray( "Cargo", myarray )> -1) || ($.inArray( "Physical Damage", myarray )> -1) ){

$("#remarks_id_show").show();
}else{
$("#remarks_id_show").hide();
}
// alert(foo);
	/*	var foo = []; 
$('#app_id :selected').map(function(i, selected){ 
  foo.push($(this).val()); 
 
});
var val_app1=foo;
		$("#id_remarks_text").val(val_app1);
	
	var val_app=$("#app_id").val();
		//alert(val_app);
		var val_remark=$("#id_remarks_text").val();
		// var myarray=[];
        // myarray.push(val_app);
		 // alert($(this).attr("selected",true));
		if(val_remark === ""){
		$("#id_remarks_text").val(val_app);		
		}else
		{
		  myarray = val_remark.split(',');
		 //alert(myarray[0]);
	
		//alert(myarray.indexOf( val_app ));
		
		if(myarray.indexOf( val_app )>=0){
			//alert(2);				
		 }else{
		// alert(3);
	//var val_app1=val_remark+","+val_app;
		//$("#id_remarks_text").val(val_app1);
		 }
		}*/
		
		
		}
		
	});

	
</script>
<script>
										 function trans_val(){
										 var n=$("#transported_id").val();
										 if(n==="No"){
										 $("#text_no").show();										 
										 }else{
										 $("#text_no").hide();	
										 
										 }
										 }
										 </script>
										 <script>
										 function Company_trucking(){
										 if($("#trucking_comp_id").val() ==="Other"){
										 $("#trucking_specify").show();
										 }else{
										 $("#trucking_specify").hide();
										 }
										 }
										 function certificate_app_val(){
										// alert($("#certificate_id_val").val());
										 if($("#certificate_id_val").val() ==="Yes"){
										 //alert(1);
										 $("#certificate_app_id").show();
										 }else{
										 $("#certificate_app_id").hide();
										// alert(2);
										 }
										 }
										 function owner_vehicle1(){
										 if($("#owner_id").val() ==="No"){
										 
										 $("#insurance_vehicle").show();
										// $("#Policy_id").show();
										 }else{
										 $("#insurance_vehicle").hide();
										// $("#Policy_id").hide();
										// alert(2);
										 }
										 }
										 function insurance_val_func(){
										  if($("#insurance_val_id").val() ==="Yes"){
										 //alert(1);
										 $("#Policy_id").show();
										 }else{
										 $("#Policy_id").hide();
										// alert(2);
										 }
										 
										 }
										 	

function load_trucks(){

	 var n=$("#loaded_trucks_id").val();
										 if(n==="Yes"){
										 $("#loaded_id").show();										 
										 }else{
										 $("#loaded_id").hide();	
										 
										 }

}						
          function primary_coverage(){
		  var n=$("#primary_coverage_id").val();
										 if(n==="Yes"){
										 $("#coverage_id").show();										 
										 }else{
										 $("#coverage_id").hide();	
										 
										 }
		  
		  }
					function inspection_func(){
					if($("#inspection_id").val()=== "Yes"){
					  $("#if_yes").show();
					}else{
					  $("#if_yes").hide();
					}
					
					}
					
					function applicant_business(){
							if($('#applicant_ever').val() == "Yes"){
					  $("#explain_business").show();
					   $("#explain_business1").show();
					  $("#explain_business").attr('required');
					  
					}else{
					  $("#explain_business").hide();
					   $("#explain_business1").hide();
					}
					}
					function applicant_transportation1(){
		if($('#applicant_transportation').val() == "Yes"){
					  $("#explain_conducted").show();
					}else{
					  $("#explain_conducted").hide();
					}
				}
										 </script>
										 
										 										 <script type="text/javascript">
    $(document).ready(function() {

        $('.fancybox').fancybox();

    });
		 $(document).ready(function() {
		
        
         if($('#pull_left_yes').val()=="Yes"){
         
         $('#row_hide_los').show();
         
         }else{
         
          $('#row_hide_los').hide();
         
         }
        
	  var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		 var _startDate1 = new Date(); //todays date
		var _endDate1 = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		$('#date_from').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate1 = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		
		 $('.datepick').mask('99/99/9999');
		
		});
		function fancybox_close(){
		parent.jQuery.fancybox.close();
		//location.reload(true);
		}
		function reload_close(){
	
		 var l=$('#addconducted input').length;
		
		 for(i=1;i<l;i++){
		 $('#addconducted input').val('');
		  $('#addconducted textarea').val('');
		 }
	
		parent.jQuery.fancybox.close();

		
		}
	function	applicant_conducted1(val){

	if(val=="Yes"){
		$('#addconduct').show();
		}else{
		$('#addconduct').hide();
		}
		}
		function los_name(val){
			
		if(val=="Yes"){
		//$('#row_hide_los').show();
		$('#losses_below').show();
		}else{
			//alert(val);
		//$('#row_hide_los').hide();
		$('#losses_below').hide();
		
		}
		
		}
		function applicant_entity1(val){
		
		if(val=="Yes"){
		$('#applicant_role').show();
		$('#applicant_explain').hide();
		}else{
		$('#applicant_role').hide();
		$('#applicant_explain').show();
		
		}
		
		
		}
	function date(from_id,to_id){
        var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		
		$('#'+from_id).datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 endDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#'+to_id).datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#'+to_id).datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#'+from_id).datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
        }
		function SSN1(val){
			//alert(val);
			if(val=='SSN'){
			$('#audit_SSN12').show();
			$('#audit_SSN12').mask('999-99-9999');
			}
			if(val=='Tax I.D.'){
				$('#audit_SSN12').show();
				$('#audit_SSN12').mask('99-9999999');
				}
			if(val==''){
				$('#audit_SSN12').hide('');
				//$('#audit_SSN12').unmask();
				}
			}
		 function applicant_above_address(val){
 if(val=="No")
 {
  $('#above_address_ami').show();
 }
 else
 {
  $('#above_address_ami').hide();
 }
 }
</script>



