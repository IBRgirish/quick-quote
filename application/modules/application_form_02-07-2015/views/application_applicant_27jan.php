<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');

	if($print) {	
	
		?>
   <script src="<?php echo base_url('js');?>/print.js"></script>     
      
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('css/print.css')?>"> 
 
        <?php
	}else{	
		?>

     <style>
			.span4_38{
				width: 38%!important;
			}
			.span1_11{
				width: 11%!important;	
			}
			.span1_10{
				width: 10%!important;	
			}
			.span2_18{
				width: 18%!important;	
			}
			.span2_19{
				width: 19%!important;	
			}
			 .span2_23{
				width: 23%!important;	
			}
			 .span2_21{
				width: 21%!important;	
			}
			div#app_id_chzn {
             width: 100%!important;
             }
		</style>
        <?php
	}
} 
$agency_name = $this->session->userdata('member_name'); 
$characters_only = ' pattern="^[a-zA-Z0-9][a-zA-Z0-9-_\.\ \;\,\*\@\&\)\(\-\=\+\:/]{0,50}$"  ';

//placeholder="MM/DD/YYYY" 
//$date_pattern = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" ';

$date_pattern_js = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ';

/*
  $date_pattern = ' placeholder="MM/DD/YYYY" class="date"  pattern="^([0-9]{2,2})/([0-9]{2,2})/([0-9]{4,4})" ';
 */

$email_pattern = 'pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$"';
$number_pattern = ' pattern="[0-9]+" ';
$less_100_limit = ' pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" ';
?>

<script src="<?php echo base_url('js'); ?>/msdropdown/jquery.dd.min.js" type="text/javascript"></script>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('css');?>/msdropdown/dd.css" />   
   <script language="javascript">
		$(document).ready(function(e) {
		try {
		$("#company").msDropDown();
		} catch(e) {
		//alert(e.message);
		}
		var val=$("#audit_name").val();
	
	if(val === "Other"){
			$("#Specify").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify").hide();
		}
	
		});
		</script>
<link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
 <!--<script type="text/javascript" src="<?php echo base_url('js')?>/jquery.js"></script>-->
 <script type="text/javascript" src="<?php echo base_url('js')?>/jquery.validate.min.js"></script>
 <!--    	<script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>-->
 <script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
    	<script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
    	<!--<script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script>-->
	<script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>

		<script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url('js'); ?>/application.js"></script>
      
		<script src="<?php echo base_url('js'); ?>/jquery.autosize.input.js"></script>
        <?php if(isset($show_hed)){?>
        <script>
		$(document).ready(function(e) {
			var config = {

			  'select'				   : {},
			  '.chzn-select'           : {},
		
			  '.chzn-select-deselect'  : {allow_single_deselect:true},
		
			  '.chzn-select-no-single' : {disable_search_threshold:10},
		
			  '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
		
			  '.chzn-select-width'     : {width:"95%"}
		
			}
		
			for (var selector in config) {
		
			  $(selector).chosen(config[selector]);
		
			} 
			var i = 1;
			$('.select_new').each(function(index, element) {
				var id = $(this).attr('id');	
				var id = $(this).attr('id');
				if($(this).attr('name')=='cd_damage[]'||$(this).attr('name')=='t_cd_damage[]'){
					$('#'+id+'_chzn').css('width', '15%');
				}else if($(this).attr('name')=='t_select_applicant[]'||$(this).attr('name')=='tr_select_applicant[]'){
					$('#'+id+'_chzn').css('width', '30%');
				}else{
                	$('#'+id+'_chzn').css('width', '7%');
				}
				if($(this).attr('name')=='select_equipment[]'){
					if($(this).val()=='Yes'){
						$('#put_on_truck'+i+'_chzn').show();	
						$('#put_on_truck'+i+'_chzn').css('width', '20%');	
					}else{
						$('#put_on_truck'+i+'_chzn').hide();		
					}
					$('select[name="'+i+'pd_loss_state[]"], select[name="'+i+'driver_state[]"]').each(function(index, element) {
						$(this).parent().find('.chzn-container').css('width', '100%');						
					});
					i++;
				}
            });
			$('select[name="state_driving[]"], select[name="t_state_driving[]"], select[name="select_applicant[]"]').each(function(index, element) {
				var id = $(this).attr('id');
				if($(this).attr('name')=='select_applicant[]'){
					$('#'+id+'_chzn').css('width', '30%');	
				}else{
                	$('#'+id+'_chzn').css('width', '50%');	
				}
            });
			var tractor_val = $('#vehicle_tractor').val();
			for(var i = 1;i==tractor_val;i++){
				$('select[name="tr_select_applicant'+i+'[]"]').each(function(index, element) {
					var id = $(this).attr('id');
                    $('#'+id+'_chzn').css('width', '30%');
                });	
			}
			$('.add_owner_y1').each(function(index, element) {
                $(this).parent().find('.chzn-container').css('width', '100%');
            });
			
        });
		</script>
        <script>
		  $(document).ready(function() {										
			if($('#add_add_yn').val()=='Yes'){
				$("#additional_add").show();	
				$("#address_container").show();
			}else if($('#add_add_yn').val()=='No'){
				$("#additional_add").hide();
				$("#address_container").hide();
			}
		  });		
		</script>  
        <?php } ?>
                         


        <style>
		#state_app1 select.span12 {
width: 80px!important;
}
		
		
		.changemargin{
		margin-top:5px;
		}
		 img.fnone {
              height: 30px;
              }
		div#app_id_chzn {
width: 70%!important;
}
		
            .table thead th {
                text-align: center;
                vertical-align: inherit;
            }

            .table th, .table td {
                font-size: 0.8em;
            }

            .row_added_vehicle{
                margin:0px 0px 5px 0px;
            }
            .status_msg {
                color: #ff0000;
            }
			
			#upload_file_name_second{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second input{
				display: none;
			}
			
			#upload_file_name_second li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second li span{
				width: 15px;
				height: 12px;
				background: url('images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second li.error p{
				color:red;
			}

				#upload_file_name_second_div{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second_div li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second_div input{
				display: none;
			}
			
			#upload_file_name_second_div li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second_div li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second_div li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second_div li span{
				width: 15px;
				height: 12px;
				background: url('images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second_div li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second_div li.error p{
				color:red;
			}
           
			
			
			#upload_file_name1{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name1 li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name1 input{
				display: none;
			}
			
			#upload_file_name1 li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name1 li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name1 li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name1 li span{
				width: 15px;
				height: 12px;
				background: url('<?php echo base_url();  ?>images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			a.remove {
    background: url('<?php echo base_url();  ?>images/add_remove_btn.png') no-repeat scroll -32px -30px transparent;
    padding: 9px 26px 6px 3px;
    border: medium none;
}
			
			
			#upload_file_name1 li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name1 li.error p{
				color:red;
			}
			.purpol-heading {
background-color: #990099!important;
border: 1px solid #460146!important;
color: #fff!important;
text-align: center!important;
min-height: 41px!important;
}
		.add_add{
			float: right;
margin: 0px 51px 0px 10px;
display: inline-block;
padding: 0px 0px 20px 0px;	
		}
		#appli_id1_chzn{
			margin-bottom: 6px;	
		}
		#Quote_num_new_chzn, #audit_name_chzn{
			width:100%!important;	
		}
        </style>	
<!--Start Add space between number and quetions BY Ashvin patel 19nov2014 -->

  <script>

  $(document).ready(function(e) {

            $('label, #losses_lia span5').each(function(index, element) {

                var html = $(this).html();

    if(html.match(".*\\d.*")){

     //var html_1 = html.replace( new RegExp(' ', 'g'), '' );

     var ar = html.split('.');

     var str = ar.join(". ");

     $(this).html(str);

    }

            });

        });

  </script>

  <!--End Add space between number and quetions BY Ashvin patel 19nov2014 -->		

<script type="text/javascript">
	
	
			

function apply_on_all_elements(){
		
		//$('input').not('.datepick').autotab_magic().autotab_filter();
		//$('input').not('.require').autotab_magic().autotab_filter();
		//$('select').not('.require').autotab_magic().autotab_filter();
		
		//$('input').attr('required', 'required');
		
		$('.price_format').priceFormat({
		prefix: '$ ',
		centsLimit: 0,
		thousandsSeparator: ','
		});
		
		$(".license_issue_date").mask("99/99/9999");
		
		//  $('input').attr('required', 'required');  
		//$('select').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		
		$('#vehicle_tractor').removeAttr('required');
		$('#vehicle_truck').removeAttr('required');
		$('#zip2').removeAttr( 'required' );
		$('.zip2').removeAttr( 'required' );
		$('.filing_numbers').removeAttr('required');
		$('.filing_type').removeAttr('required');
		$('.last_name').removeAttr('required');
		$('.middle_name').removeAttr('required');
		
		//$('#vehicle_pd').removeAttr('required');
		//$('#vehicle_liability').removeAttr('required');
		//$('#vehicle_cargo').removeAttr('required');
		$('#cab').removeAttr('required');
		$('#tel_ext').removeAttr('required');
		$('#insured_email').removeAttr('required');
		$('#insured_tel_ext').removeAttr('required');
		//alert();
		
		//$('form').h5Validate(); 	
		//alert($(".chosen").val());
}



$(document).ready(function() {

});
	$(document).ready(function() {

	
	
	
	// $('#date_from_effective').mask("99/99/9999");
	// $('#date_to_expiration').mask("99/99/9999");
	
	/*		$('#first_upload').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values').value = '';
				$('#drop a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});
			
			$('#first_upload_second').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values_second').value = '';
				$('#drop_second a').parent().find('input').click();
    		});
			
			$('#first_upload_second_div').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values_second_div').value = '';
				$('#drop_second_div a').parent().find('input').click();
    		});
			*/
			
		apply_on_all_elements();
		$("#coverage_date").mask("99/99/9999");
		
		//$('form').h5Validate();
		
		//$('input').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		//$('input[type="checkbox"]').removeAttr( 'required' );
		$('input[type="file"]').removeAttr( 'required' ); 
		
/* 		$('#coverage_date').datepicker({
			format: 'mm/dd/yyyy'
		}); */
		//$("#coverage_date").mask("99/99/9999");
 	 	/* var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

		var checkin = $('#coverage_date').datepicker({
		onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
		}).on('changeDate', function(ev) {
		// $(this).blur();
		checkin.hide();		 
		}).data('datepicker');   */
		
		/* $('#coverage_date').blur(function() {
  			$('#coverage_date').datepicker("hide");
		});
		 */
		
		//Hide datepicker on Focus Out		
	/* 	$('#coverage_date').blur(function(){
			//setTimeout(function(){$('#coverage_date').datepicker('hide');},100)
		}); */
		
		var date = new Date();
		date.setDate(date.getDate()-0);
				
		 var inputs1 = $('#coverage_date').datepicker({
			startDate: date,
			format: 'mm/dd/yyyy',
			autoclose: true
		});  


		
		$('#dba').change(function(){
			//check_insured_dba_required();			
		});
		
		$('#insured_name').change(function(){
		//alert();
			//check_insured_dba_required();			
		});

		

    var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }

//$('.request_quote').validateForm();
/* 
		var hiddenEls = $("body").find(":hidden").not("script");

	$("body").find(":hidden").each(function(index, value) {
	selctor= this.id;
	if(selctor){
		$( '#'+selctor+' :input').removeAttr("required");
		}
		alert(this.id);
	}); */
	
	/*  $(".request_quote").validateForm({
		ignore: ":hidden",
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			
			$('#loss_report_upload1').click(function() {  
				//var up_id = $('.upload_select:last').attr('id');
				//alert(up_id);
				$('#fileup1_1').click();  
				//$('#'+up_id).click();  
			
			});
			 
			$('#mvr_upload').click(function() { 
			$('#fileup_1').click();  });
 


	});
	
	
			
		function form_sub(){
		/* $(".request_quote").validateForm({
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			//$('.request_quote').validateForm();
			
/* 			if($('#theid').css('display') == 'none'){ 
   $('#theid').show('slow'); 
} else { 
   $('#theid').hide('slow'); 
}
$("br[style$='display: none;']") */


			
	}
	
	function check_insured_dba_required(){
			dba_val = $('#dba').val().trim();
			insured_val = $('#insured_name').val().trim();
			if(dba_val){
				$('#insured_name').removeAttr('required'); 
				$('#insured_middle_name').removeAttr('required');
				$('#dba').attr('required', 'required'); 
			}else if(insured_val){
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').removeAttr('required'); 		
			} else {
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').attr('required', 'required'); 	
			}
		}
		
	
	<!-----updated by amit03-03-2014------>
	$("body").on("change",".rofo:first",function(){
				val = $('.rofo:first').val();
				$('.rofo:first').attr("readonly",false)
				$('.rofothers:first').attr("readonly",false)
				otherval = $('.rofothers:first').val();
				$(".tractor_radius_of_operation").val(val);
				$(".truck_radius_of_operation").val(val);
				if(val == 'other')
				{
					$('.rofothers').show();
					$('.rofothers').val(otherval);
					$('.rofothers').not(":first").attr("readonly",true);
					$(".show_v_others").show();
				}
				
				$(".truck_radius_of_operation").not(":first").attr("readonly",true);
				$(".tractor_radius_of_operation").not(":first").attr("readonly",true);
				$('.show_tractor_others').not(":first").attr("readonly",true);
				$('.show_truck_others').not(":first").attr("readonly",true);
			});
			$("body").on("keyup",".rofothers:first",function(){
				
				$(".rofothers").val($(this).val());
			});
	
			function rofoperation(val , id){
			
				if(id=='1tractor_radius_of_operation')
				{							
					$(".tractor_radius_of_operation").val(val);
					$(".tractor_radius_of_operation").not(":first").attr("disabled",true);
					if(val == 'other'){
						$('.show_tractor_others').show();
						$(".specify_other_cp").not(":first").attr("disabled",true);
					} else {
						$('.show_tractor_others').hide();
					}	
				}
				else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').show();
					} else {
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').hide();
					}	
					
				}
				
				
				
			/*  alert(id);
			alert(val);  */
			
			
			}
			
			
			
			function rofoperation2(val , id){
				
				if(id=='1truck_radius_of_operation')
				{
					$(".truck_radius_of_operation").val(val);
					if(val == 'other'){
						$('.show_truck_others').show();
					} else {
						$('.show_truck_others').hide();
					}
				}	
							else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').show();
					} else {
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').hide();
					}	
					
				}
			}

				
				
				
				
			
					
			
		function check_commodities_other(id){ 
			$("#"+id+" option:selected").each(function(){
			  val = $(this).text(); 
			  if(val == 'other'){ 
					$('.'+id+'_otr').show();
				} else { 
					$('.'+id+'_otr').hide();
				}
			});
		}
	
	
	$(document).ready(function(e) {
        $("body").on("keyup",".commhauled:first",function(){
			$(".commhauled").val($(this).val());
		});
		$("body").on("keyup",".commhauled2:first",function(){
			$(".commhauled2").val($(this).val());
		});
		
		
		 $("body").on("keyup",".cargo_class_price:first",function(){
			$(".cargo_class_price").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_price2:first",function(){
			$(".cargo_class_price2").val($(this).val());
		});
		
		
		$("body").on("keyup",".cargo_class_ded:first",function(){
			$(".cargo_class_ded").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_ded2:first",function(){
			$(".cargo_class_ded2").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp:first",function(){
			$(".specify_other_cp").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp2:first",function(){
			$(".specify_other_cp2").val($(this).val());
		});
			/**Dba required if name if blank or name required if dba is blank By Ashvin Patel 12/may/2014**/

		$('#insured_name').blur(function(){
			//alert();
			var f_name = $(this).val();
			var l_name = $('#insured_last_name').val();
			if(f_name==''||l_name=='')
			{
				$('#dba').attr('required', 'required');
				
			}
			else
			{
				$('#dba').removeAttr('required');
				$('#dba').removeClass('ui-state-error');
			}
		});
	
		$('#insured_last_name').blur(function(){
			//alert();
			var l_name = $(this).val();
			var f_name = $('#insured_name').val();
			if(f_name==''||l_name=='')
			{
				$('#dba').attr('required', 'required');
			}
			else
			{
				$('#dba').removeAttr('required');
				$('#dba').removeClass('ui-state-error');
			}
		});
		$('#dba').blur(function(){
			var dba = $(this).val();
			var f_name = $('#insured_name').val();
			if(dba=='')
			{
				$('#insured_name').attr('required', 'required');
				$('#insured_last_name').attr('required', 'required');
			}
			else
			{
				$('#insured_name').removeAttr('required');
				$('#insured_last_name').removeAttr('required');
				$('#insured_name').removeClass('ui-state-error');
				$('#insured_last_name').removeClass('ui-state-error');
			}
		});
    });
	

		
   
	
	
	
	<!-----updated by amit03-03-2014------>
</script>
       
       <script>
$(document).ready(function(){
	$("#agencies").change(function(){
		//window.location.href = window.location.href + "/" + $(this).val();
		$("#broker_email").val($(this).text());
		
		$.ajax({
			url: '<?php echo base_url();?>manualquote/setpublishersession',
			type: "POST",
			data: {
					action:'via_ajax',
					publisherid : $(this).val()
			},
			success: function(data) {
				
				window.location.reload(true);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(thrownError);
			}
		});
	});
});
function update_unknown(val){
	var owned_veh = $('#trailer_owned_vehicle'+val).val();


	if(owned_veh == 'no'){
		$("#vehicle_year_vh"+val).removeAttr("selected");
		$("#vehicle_year_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#make_model_vh"+val).removeAttr("selected");
		$("#make_model_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#trailer_type_veh"+val).removeAttr("selected");
		$("#trailer_type_veh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
	}else{
		$("#vehicle_year_vh"+val+" option[value='Unknown']").remove();
		$("#make_model_vh"+val+" option[value='Unknown']").remove();
		$("#trailer_type_veh"+val+" option[value='Unknown']").remove();
	}
	/* vehicle_year_vh1-2
	make_model_vh1-2
	trailer_type_veh1-2 */
	//alert();
}


	
	//$("#coverage_date").mask("99/99/9999");
	
	/* ('#dp3').datepicker({
			//format: 'mm/dd/yyyy'
		}); */

    /* $("input").each(function() {
      this.value = "";}) */
    //add_driver();
    //alert($(window).width()); 
	
/* 	$( ".number_limit" ).validate({
rules: {
field: {

range: [1, 99]
}
}
}); */
jQuery(document).ready(function(){
	$("#app_id").chosen({ allow_single_deselect: true });
	//$(".chzn-select").chosen().attr('required');
	/*$(".chzn-select").chosen({
    //disable_search_threshold: 0,
	allow_single_deselect:false,
	disable_search:true,
   // no_results_text: "Oops, nothing found!",
   // width: "95%"
  })*/
  });;

	//$('.chosen-select').chosen({disable_search_threshold:0}) ;
 

</script>

<h2 style="text-align:center;">Application Sheet</h2>
<form action="<?php echo base_url();?>application_form/submit_applicant/<?php echo isset($app[0]->general_id) ? $app[0]->general_id : '';?>" method="post" accept-charset="utf-8" id="main_form" enctype="multipart/form-data" novalidate="novalidate" onsubmit="return validateForm();" />
<div class="well">

<input type="hidden" name="quote_id1" value="<?php echo isset($broker_insured[0]->quote_id) ? $broker_insured[0]->quote_id :'';?>"/>
<input type="hidden" name="insert_id" value="<?php echo isset($app[0]->general_id) ? $app[0]->general_id : '';?>" id="insert_id"/>
<input type="hidden" name="applicant_id" value="<?php echo isset($app_applicant[0]->applicant_id) ? $app_applicant[0]->applicant_id : '';?>"/>

<input type="hidden" name="underwriter" value="<?php echo isset($underwriter) ? $underwriter : '';?>"/>



     <div class="row-fluid" style="margin-bottom:8px;">
              <span class="left_float" >
                <label class="control-label control-label-1 control-label-3 changemargin" >This is</label>
				
              </span>
			  
	             <span class="left_float span5">
			  <?php echo get_catlog_dropdown_company($field_name ='company', $catlog_type='company_list', $attr = "class='span10 pull-right' required='required'", $other_val=0, $module_name ='', $selected = isset($app[0]->company) ? $app[0]->company : '',$otr_relative_field='', $otr_relative_field_attr=''); ?>
                <br/>
              
              </span>
			   <label class="left_float changemargin">&nbsp;&nbsp;&nbsp;&nbsp;Applications &nbsp; </label>
              <span class="span4">
			<?php // echo $rq_rqf_quote_losses[0]->losses_type;
			$selected='';
			$select='';
			if(!empty($rq_rqf_quote_losses)){
			
			foreach($rq_rqf_quote_losses as $quote_losses){
				
				//print_r($quote_losses);
				if($quote_losses->losses_type=='losses_liability'){
					$select.='Liability,';
					}
				if($quote_losses->losses_type=='losses_pd'){
					$select.='Physical Damage,';
					}
				 if($quote_losses->losses_type=='losses_cargo'){
					$select.='Cargo';
					}
				}
					
					 
					 
					//$selected = isset($select) ?  : ''; 
				}	
				else{
						
						
						}
					$selected = isset($app[0]->app_name) ? explode(",",$app[0]->app_name) : explode(",",$select);
					$apps = $selected;
			
			 
			
			   echo get_app_dropdown($field_name ='app_name[]',$selected, $attr = "class='span3 chzn-select left_float'   multiple onChange='application_select();' id='app_id'") ?>
              <br/>
              
              </span>
            </div>
<input type="hidden" id="id_text_app_val" name="id_text_app_val[]" value="<?php  echo isset($app[0]->app_name) ? $app[0]->app_name : $select; ?>"/>
                                <fieldset class="head_box">



                    <!-- Form Name -->
                    <legend>General Information</legend>

                    <div class="row-fluid" >
                        <div class="span12">

                            <div class="row-fluid"> 
							 <div class="span4 span4_38">
                                <div class="span1 span1_10 lightblue">
                                    <label><span class="enum"></span>Quote</label>
									<?php $Quote_num=isset($Quote_num)?$Quote_num:''; ?>
                                     <input class="span12" type="text" name="Quote_num" id="Quote_num" value="<?php echo isset($app[0]->Quote_num) ? $app[0]->Quote_num : $Quote_num;?>" placeholder="Quote num." required="required">
							   </div>
                                <div class="span2 span2_18 lightblue">                             
                                    <label>&ensp;</label>
									<?php $Quote_num_new = isset($app[0]->Quote_num_new) ? $app[0]->Quote_num_new : '';?>
                                   <select name="Quote_num_new" id="Quote_num_new" class="h5-active ui-state-valid span12" required="required" >
                                       <option value="">Select</option>
                                       <option <?php if($Quote_num_new  == 'New') echo 'selected="selected"'; ?> value="New">New</option>
                                       <option <?php if($Quote_num_new  == 'Renewal') echo 'selected="selected"'; ?> value="Renewal">Renewal</option>
                                       <option <?php if($Quote_num_new  == 'Rewritten') echo 'selected="selected"'; ?> value="Rewritten">Rewritten</option>
                                    </select>
								</div>
                                <div class="span2 span2_19 lightblue">
                                    <label><span class="enum"></span>Effective Date</label>
                                    <input required="required" maxlength="10" value="<?php echo isset($app[0]->effective_from) ? $app[0]->effective_from : '';?>" type="text" name="effective_from" id="date_from_effect" class="span12  datepick  " pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}">
								</div>	
								<div class="span2 span2_21 lightblue">
                                    <label><span class="enum"></span>Expiration Date</label>
                                   <input required="required" maxlength="10" value="<?php echo isset($app[0]->expiration_from) ? $app[0]->expiration_from : '';?>" type="text" name="expiration_from" id="date_to_expiration" class="span12  datepick" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}">
								 </div>
							     <div class="span2 span2_21 lightblue">
                              		<div class="row-fluid">
                                    <label><span class="enum"></span>Audit Period</label>
									<?php $selected_audit = isset($app[0]->audit_name) ? $app[0]->audit_name : '';?>
                                      <select name="audit_name" class="span12" id="audit_name" onChange="audit(this.value);" required="required">
                                      <option value="">Select</option>
                                      <option <?php if($selected_audit  == 'Anually') echo 'selected="selected"'; ?> value="Anually">Anually</option>
                                      <option <?php if($selected_audit  == 'Monthly') echo 'selected="selected"'; ?> value="Monthly">Monthly</option>
                                      <option <?php if($selected_audit  == 'Quaterly') echo 'selected="selected"'; ?> value="Quaterly">Quaterly</option>
                                      <option <?php if($selected_audit  == 'Semi-Anually') echo 'selected="selected"'; ?> value="Semi-Anually">Semi-Anually</option>
                                      <option <?php if($selected_audit  == 'Other') echo 'selected="selected"'; ?> value="Other">Other</option>
                                      </select>
                                    </div>
                                    <div class="row-fluid">
                                    	<div class="row-fluid lightblue" style="display:none" id="Specify">                                
                                        <label>Specify other&nbsp;</label>
                                       <input class="textinput span12" type="text" value="<?php echo isset($app[0]->Specify_oth) ? $app[0]->Specify_oth : '';?>" name="Specify_oth" placeholder="">
                                    	</div>
                                    </div>
								</div>
                            <div class="row-fluid" id="remarks_id_show"  style="display:none"> 
                                <div class="span12" >
                                    <label>Remarks</label>
                                    <textarea class="span12" name="remarks" id="id_remarks_text"> 
                                       <?php  if($app[0]->remarks==0) {} else{echo $app[0]->remarks; } ?>
                                    </textarea>
                                </div>
							</div>
								
								 
								
							</div>	
						
							
                                <div class="span7 lightblue">
                                    <label class="bro_inform"> Broker Information</label>
					<div class=	"span12">		 
                  <span class="span8" id="broker_n">
               
                    <label class="control-label left_pull "><span class="enum"></span>DBA:&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label>
                 
                    <select name="broker_name" class="left_pull span12" id="broker_name"  onchange="broker_info(this.value);">
					 <?php
					 //print_r($broker_inform);
								foreach($broker_inform as $broker){
							    $broker_name1= isset($broker->first_name) ? $broker->first_name : '';
								$broker_name2= isset($broker->middle_name) ? $broker->middle_name : '';
								$broker_name3=isset($broker->last_name) ? $broker->last_name : '';
								
								if($broker_name2!=0 && $broker_name2!= 'null'){
								
								$broker_name4=$broker_name2;
								
								}
								if($broker_name3!=0 && $broker_name3!= 'null'){
								
								$broker_name5=$broker_name3;
								
								}
								$broker_name=$broker_name1.' '.$broker_name4.' '.$broker_name5;
								
								$broker_id=isset($broker_information[0]->broker_name) ? $broker_information[0]->broker_name : $broker_inform1[0]->id;
								
								?>
                           <?php if($this->session->userdata('member_id')){
					if($broker->id == $broker_id)	{				
					       ?> 
				 	 <option <?php if($broker->id == $broker_id ) echo 'selected="selected"'; ?> value="<?php echo isset($broker->id) ? $broker->id : $broker_information[0]->broker_name;?>"><?php echo  isset($broker_name) ? $broker_name : ''; ?></option>
					 <?php } }else{ ?>
                 <option <?php if($broker->id == $broker_id ) echo 'selected="selected"'; ?> value="<?php echo isset($broker->id) ? $broker->id : $broker_information[0]->broker_name;?>"><?php echo  isset($broker_name) ? $broker_name : ''; ?></option>
                    <?php }  } ?>
				   </select>
                 
				 <?php 
				
				 $ph_num = isset($broker_inform1[0]->phone_number) ? $broker_inform1[0]->phone_number : '';
				 $brokr_tel= isset($broker_information[0]->brokr_tel) ? $broker_information[0]->brokr_tel : $ph_num  ; 
				 
				 $brokr_fax=isset($broker_information[0]->brokr_fax_number) ? $broker_information[0]->brokr_fax_number : isset($broker_inform1[0]->fax_number) ? $broker_inform1[0]->fax_number : '';// print_r(str_split($brokr_tel));
				// die;
				 ?>
			<!--	 <input type="hidden" value="<?php echo isset($broker_inform[0]->phone_number) ? $broker_inform[0]->phone_number : '';?>" name="brokr_tel"/>-->
               	
                
			   </span>
               <span class="span4" id="broker_name">
			   <?php $insured_dba= isset($broker_inform1[0]->insured_dba)? $broker_inform1[0]->insured_dba: ''; ?> 
				 <label class="left_pull" style="margin-left:0px;"  ><span class="enum"></span>Name:&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</label>
				<input name="broker_name_dba" id="broker_name_dba" type="text" class="span12 left_pull"  value="<?php echo isset($broker_information[0]->broker_name_dba) ? $broker_information[0]->broker_name_dba :$insured_dba  ;?>" readonly="readonly">
                
                </span>
				</div>
				<div class=	"span12">
				<span class="span4" id="broker_add">
			    <?php $address= isset($broker_inform1[0]->address)? $broker_inform1[0]->address: ''; ?>
				<label class="left_pull" style=""><span class="enum"></span>Address: &nbsp;&nbsp; </label>
				<input  name="broker_address" id="broker_address" type="text" class="span12" style="float:left;"  value="<?php echo isset($broker_information[0]->broker_address) ? $broker_information[0]->broker_address : $address  ;?>" readonly="readonly">
               
				 </span>
				 
				<span class="span2" >
				<?php  $country= isset($broker_inform1[0]->country)? $broker_inform1[0]->country: ''; ?>
				 <label class=" " style="float:left;">Country: &nbsp;&nbsp;&nbsp; </label>
                <input  style="float:left;" name="broker_country" type="text" class="span12"  value="<?php echo isset($broker_information[0]->broker_country) ? $broker_information[0]->broker_country : $country;?>" id="broker_country" readonly="readonly">
				</span>
				<span class="span2" >
				<label class=" " style="float:left;margin-left:5px;">State: &nbsp;</label>
				     <?php   $broker_state= isset($broker_inform1[0]->state)? $broker_inform1[0]->state: ''; ?>         
				<input class=" span12" style="float:left;"  type="text" placeholder="" value="<?php echo isset($broker_information[0]->broker_state) ? $broker_information[0]->broker_state:$broker_state ;?>" name="broker_state" id="broker_state"  maxlength="30" readonly="readonly">
				</span>
				<span class="span2" >
				<label class=" " style="float:left;margin-left:5px;">City: &nbsp;</label>
				             <?php $broker_city= isset($broker_inform1[0]->city)? $broker_inform1[0]->city: ''; ?> 
				<input class=" span12"  style="float:left;" type="text" placeholder="" value="<?php echo isset($broker_information[0]->broker_city) ? $broker_information[0]->broker_city:$broker_city ;?>" name="broker_city" id="broker_city"  maxlength="30" readonly="readonly">
			   </span>
				
                
				 
				 	<span class="span2" id="broker_zip">
				
				 <label class="" style="float:left;">Zip: &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                <input  style="float:left;" name="broker_zip_code" id="broker_zip_code" type="text" class="span7"  value="<?php echo isset($broker_information[0]->broker_zip_code) ? $broker_information[0]->broker_zip_code : isset($broker_inform1[0]->zip_code) ? $broker_inform1[0]->zip_code : '';?>" readonly="readonly">
				
				              
				<input class="span4"  style="float:left;"  type="text" placeholder="" value="<?php echo isset($broker_information[0]->broker_zip_code_extd) ? $broker_information[0]->broker_zip_code_extd: isset($broker_inform1[0]->zip_code_extd) ? $broker_inform1[0]->zip_code_extd :'' ;?>" name="broker_zip_code_extd" id="broker_zip_code_extd"  maxlength="30" readonly="readonly">
				
                </span>
				</div>
				 
                <div class="span12">
				<span class="span2">
				 <label class="" style="float:left;"><span class="enum"></span>Email: &nbsp;</label>
				              
				<input class="email span12" id="broker_email" type="text" placeholder="" value="<?php echo isset($broker_information[0]->broker_email) ? $broker_information[0]->broker_email: isset($broker_inform1[0]->email) ? $broker_inform1[0]->email : '' ;?>" name="broker_email" id="email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30" readonly="readonly">
				 </span>
				 <span class="span3">
                 
                   <?php $ph = isset($brokr_tel) ? explode('-', $brokr_tel) : ''; 
				  $ph0 = isset($ph[0]) ? $ph[0] : '';
				  $ph1 = isset($ph[1]) ? $ph[1] : '';
				  $ph2 = isset($ph[2]) ? $ph[2] : '';
			     $ph3 = !empty($ph[3]) ? $ph[3] : '';
				  ?>
				<label class="span12" style="float:left; "><span class="enum"></span>Telephone: </label>
				<span style="margin-top: -10px;float: left;">
				<input id="broker_tel1" style="" name="brokr_tel[]" type="text" class="span3" maxlength="3" pattern="[0-9]+" value="<?php echo $ph0;?>" readonly="readonly">
                <input id="broker_tel2" style="" name="brokr_tel[]" type="text" class="span3" maxlength="3" pattern="[0-9]+" value="<?php echo $ph1;?>" readonly="readonly">
				<input id="broker_tel3" style="" name="brokr_tel[]" type="text" class="span3" maxlength="4" pattern="[0-9]+" value="<?php echo $ph2;?>" readonly="readonly">
				<input id="broker_tel_ext" style="" name="broker_tel_ext" type="text" class="span3" maxlength="5" pattern="[0-9]+" 
                value="<?php  
				if(!empty($ph[3]) ) { 
				if($ph[3]!=0){
					echo $ph[3];
					} 
				} else{
					if(isset($broker_information[0]->broker_tel_ext)){					
						echo isset($broker_information[0]->broker_tel_ext) ? $broker_information[0]->broker_tel_ext : ''; 
						} 
				} 
				?>" readonly="readonly">
				</span>
				</span>

                    <span class="span2">
				
				 <label class="" style="float:left;"><span class="enum"></span>Website: &nbsp; &nbsp; </label>
                <input id="broker_website" style="float:left;" name="broker_website" type="text" class="span12"  value="<?php echo isset($broker_information[0]->broker_website) ? $broker_information[0]->broker_website : isset($broker_inform1[0]->website) ? $broker_inform1[0]->website : '';?>" readonly="readonly">
				</span>
				<span class="span2">
				<label class=" " style="float:left;"><span class="enum"></span>Primary email: &nbsp;</label>
				              
				<input class="email span12" type="text" placeholder="" value="<?php echo isset($broker_information[0]->broker_sec_email) ? $broker_information[0]->broker_sec_email : isset($broker_inform1[0]->sec_email) ? $broker_inform1[0]->sec_email : '' ;?>" name="broker_sec_email" id="broker_sec_email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30" readonly="readonly">
				
                </span>
				
				
				   <span class="span3">
				
			   <?php $ph = isset($brokr_fax) ? explode('-', $brokr_fax) : ''; 
				  $ph0 = isset($ph[0]) ? $ph[0] : '';
				  $ph1 = isset($ph[1]) ? $ph[1] : '';
				  $ph2 = isset($ph[2]) ? $ph[2] : '';
				//  $ph3 = isset($ph[3]) ? $ph[3] : '';
				  ?>
				<label class="span12" style="float:left;"><span class="enum"></span>Fax number: </label>
				<span style="margin-top: -10px;float: left;">
			<input id="brokr_fax_number1" style="" name="brokr_fax_number[]" type="text" class="span4" maxlength="3" pattern="[0-9]+" value="<?php echo substr($ph0,0,3);?>" readonly="readonly">
                <input id="brokr_fax_number2" style="" name="brokr_fax_number[]" type="text" class="span4" maxlength="3" pattern="[0-9]+" value="<?php echo substr($ph0,3,3);?>" readonly="readonly">
				<input id="brokr_fax_number3" style="" name="brokr_fax_number[]" type="text" class="span4" maxlength="4" pattern="[0-9]+" value="<?php echo substr($ph0,6,10);?>" readonly="readonly">
			<!--	<input id="broker_tel_ext" style="" name="broker_tel_ext" type="text" class="span2" maxlength="5" pattern="[0-9]+" value="<?php  if(!empty($broker_inform1[0]->phone_ext) ) { if($broker_inform1[0]->phone_ext!=0){echo $broker_inform1[0]->phone_ext;} } else{ if($broker_information[0]->broker_tel_ext!=0){echo $broker_information[0]->broker_tel_ext; } } ?>" readonly="readonly"> -->
				</span>
                </span>
				</div>
				
				
				
			 <span class="span12" id="broker_remark">
				
				 <label class=" " style="float:left;"><span class="enum"></span>Remarks: &nbsp; </label>
                <input id="broker_remarks" style="float:left;" name="broker_remarks" type="text" class="span12"  value="<?php echo isset($broker_information[0]->broker_remarks) ? $broker_information[0]->broker_remarks : '';?>" readonly="readonly">
				
				
                </span>
								</div>
							

                            </div>	
													 
								

								
								
							</div>
							</div></fieldset>
							
						<fieldset class="insured_info_box">
							<legend>Applicant's Section </legend>
						<div class="row-fluid">  
			
								
								<div class="span4 lightblue">
                                    <label> <span class="enum"></span> Applicant's Name </label>
									<?php $insured_fname=isset($broker_insured[0]->insured_fname)? $broker_insured[0]->insured_fname:''; ?>
									<?php $insured_mname=isset($broker_insured[0]->insured_mname)? $broker_insured[0]->insured_mname:''; ?>
									<?php $insured_lname=isset($broker_insured[0]->insured_lname)? $broker_insured[0]->insured_lname:''; ?>
                                   <input type="text" maxlength="255"  class="span4 first_name ui-state-valid name_val" placeholder="First Name" name="insured_first_name" id="fname" value="<?php echo isset($app_applicant[0]->insured_first_name) ? $app_applicant[0]->insured_first_name : $insured_fname;?>" >
									<input type="text"  class="span4 middle_name name_val" name="insur_middle_name" placeholder="Middle Name" id="mname" value="<?php echo isset($app_applicant[0]->insured_middle_name) ? $app_applicant[0]->insured_middle_name : $insured_mname;?>">
									<input type="text"  class="span4 last_name name_val" placeholder="Last Name" name="insured_last_name" id="lname" value="<?php echo isset($app_applicant[0]->insured_last_name) ? $app_applicant[0]->insured_last_name : $insured_lname;?>">
                             
									
								</div>
								
								<?Php $insured_dba1=isset($broker_insured[0]->insured_dba) ? $broker_insured[0]->insured_dba : '' ; ?>
                                <div class="span3 lightblue">
                                    <label><span class="enum"></span> DBA(if applicable)</label>
                                   <input id="dbaname" name="dba" type="text" class="span10 name_val" maxlength="255" value="<?php echo isset($app_applicant[0]->dba) ? $app_applicant[0]->dba: $insured_dba1;?>">
                                </div>
							         
                                       <div class="span3 lightblue">
                                        <label><span class="enum"></span>Address</label> 
									                                  
										<input class="textinput span10" type="text" placeholder="Address" name="app_address" value="<?php //echo isset($broker_insured[0]->insured_dba) ? $broker_insured[0]->insured_dba : '';?><?php echo isset($app_applicant[0]->app_address) ? $app_applicant[0]->app_address : '';?>">
                                    
									
                                </div>	
                                   <?php $insured_garaging_city1=isset($broker_insured[0]->insured_garaging_city) ? $broker_insured[0]->insured_garaging_city : '';?>								
                                <div class="span2 lightblue">
                                    <label >City</label>
                                    <input id="garaging_city" name="app_city" type="text" class="span6" value="<?php echo isset($app_applicant[0]->app_city) ? $app_applicant[0]->app_city : $insured_garaging_city1?>">
                                </div>								

                            </div>	

                            <div class="row-fluid">  
						
							 <div class="span1 lightblue">
                                    <label style="margin-bottom: 11px;">Country</label>
                            	<?php 
								
									//$attr = "class='span12 ui-state-valid'";
									$broker_country = isset($app_applicant[0]->country_app) ? $app_applicant[0]->country_app : 'AD';
									$insured_state1=isset($broker_insured[0]->insured_state) ? $broker_insured[0]->insured_state : '';
									//$attr = "class='span12 ui-state-valid'";
									$broker_state = isset($app_applicant[0]->state_app) ? $app_applicant[0]->state_app :$insured_state1   ;
									
									?>
									<?php //echo get_state_dropdown('state_app', $broker_state, $attr); ?>
                            <?php echo  getCountryDropDown1($name='country_app', $att='class="width_option" id="country_app" onChange="getState(this.value, &quot;state_app1&quot;, &quot;state_app&quot; )"',$broker_state, $broker_country);?>

                                </div>
							
							
								
								 <div class="span1 lightblue" >
                                    <label style="margin-bottom: 11px;">State</label>
                                    <div id="state_app1" style="width:50%">
                            	<?php
						
									
									?>
									<?php echo getStateDropDown($broker_country, $name='state_app', $att='id="state_app1"', '', $broker_state); ?>
                                  </div>

                                </div>
							 <?php  $broker_insured1= isset($broker_insured[0]->insured_zip) ? explode('-',$broker_insured[0]->insured_zip) : '';   ?>
							 <?php  $app_ins =  isset($app_applicant[0]->insured_zip) ? explode('-',$app_applicant[0]->insured_zip) : ''  ;    ?>
								 <div class="span1 lightblue">
                                    <label style="margin-bottom: 11px;">Zip</label>
                                    <?php $broker_insured12=isset($broker_insured1[0]) ? $broker_insured1[0] : '' ; ?>
									 <?php $broker_insured22=isset($broker_insured1[1]) ? $broker_insured1[1] : '' ; ?>
                                     <input id="zip1" name="insured_zip1" type="text" class="span12" maxlength="5" pattern="[0-9]+" value="<?php echo !empty($app_ins[0]) ? $app_ins[0]:$broker_insured12;  ?>" required="required"/>
                                    </div>
                                    <div class="span1 lightblue" style="margin-top: 25px;margin-left: 5px;">
                                     <label ></label>
									 <input id="zip2" name="insured_zip2" type="text" class="span12" maxlength="4" pattern="[0-9]+" value="<?php echo !empty($app_ins[1]) ? $app_ins[1]: $broker_insured22 ; ?>" required="required"/>
                                        <!--<input id="zip2" name="insured_zip[]" type="text" class="span6 zip2" value="">-->
                                   </div>
                                
								
								       <div class="span3 lightblue">
                                            <div class="row-fluid">
                  <span class="span12">
                    <label class="control-label pull-left"><span class="enum"></span> Phone</label>
                    <input type="button" id="click_add_phone" class="dom-link pull-right tdu click_add" value="Click for Additional phones">
                   <!-- <a class="dom-link pull-right tdu" href="#" id="click_add_phone">Click for Additional phones</a>-->
                  </span>
                </div>
				
                <div class="row-fluid">
            				         <div class="span12">
									    <?php $brokr_tel= isset($broker_insured[0]->insured_telephone) ? explode('-',$broker_insured[0]->insured_telephone) : '';   ?>
										<?php  $insured_telephone12 = isset($app_applicant[0]->insured_telephone) ? explode("-",$app_applicant[0]->insured_telephone) : ''; ?>
								      <input id="insured_telephone1" name="insured_telephone[1]" type="text" class="span3" maxlength="3"  pattern="[0-9]+"  value="<?php echo !empty($brokr_tel[0]) ? $brokr_tel[0] : isset($insured_telephone12[0]) ? $insured_telephone12[0] : '';?>"  >
                                        <input id="insured_telephone2" name="insured_telephone[2]" type="text" class="span3"   pattern="[0-9]+"  maxlength="3" value="<?php echo isset($brokr_tel[1]) ? $brokr_tel[1] : isset($insured_telephone12[1]) ? $insured_telephone12[1] : '';?>"  >
                                        <input id="insured_telephone3" name="insured_telephone[3]" type="text" class="span3"   pattern="[0-9]+"  maxlength="4" value="<?php echo isset($brokr_tel[2]) ? $brokr_tel[2] : isset($insured_telephone12[2]) ? $insured_telephone12[2] : '';?>"  >
									 <input id="insured_tel_ext" name="insured_tel_ext" placeholder="Ext" type="text" pattern="[0-9]+"  class="span3" maxlength="5" pattern="[0-9]+" value="<?php echo isset($app_applicant[0]->insured_tel_ext) ? $app_applicant[0]->insured_tel_ext : isset($insured_telephone[3]) ? $insured_telephone[3] : '';?>"  >
                                    
                                    </div>
                </div>
									
                                </div>
                                <div class="span3 lightblue">
                               <div class="row-fluid">
                  <span class="span12">
                    <label class="control-label left_pull">Email</label>
                    <input type="button" id="click_add_email" class="dom-link left_pull tdu click_add " value="Click for Additional email"/>
                    <!--<a class="dom-link pull-right tdu" href="#" id="click_add_email">Click for Additional email</a>-->
                  </span>
                </div><?php  $insured_email1=isset($broker_insured[0]->insured_email) ? $broker_insured[0]->insured_email : ''; ?>
				 
                <input class="textinput span9" type="text" placeholder="" id="email" name="insured_email" maxlength="100" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30" value="<?php echo isset($app_applicant[0]->insured_email) ? $app_applicant[0]->insured_email: $insured_email1 ; ?>">
								</div>
					   <div class="span2 lightblue" style="margin-left:-15px;">
                                    <label style="margin-bottom: 8px;">Other contact info</label>
                            <input class="span12" type="text" placeholder="" name="Other_contact" value="<?php echo isset($app_applicant[0]->Other_contact) ? $app_applicant[0]->Other_contact : '';?>">       
                                </div>
								
								
							
					  </div>
					  <fieldset>
					<div class="row-fluid">  
					 <?php  $add_add_yn= isset($app_applicant[0]->add_add_yn) ? $app_applicant[0]->add_add_yn : 'No';?>  
       
                                	<legend class="purpol-heading">
                                    	<div class="pull-right span5"><select name="add_add_yn" id="add_add_yn" onchange="show_hide_section(this.value, 'additional_add')" class="pull-right span2 ui-state-valid" style="margin: 6px 2px 0px 0px;"><option <?php if($add_add_yn=='No') { echo 'selected'; } ?> value="No">No</option><option <?php if($add_add_yn=='Yes') { echo 'selected'; } ?> value="Yes">Yes</option></select><label class="pull-right" style="margin: 9px 7px 0px 0px;">Any additional addresses?</label></div>	
                                        <span class="add_add">Additional Address(es)</span>
                                    </legend>	
                                    
                                    <div id="additional_add" <?php echo ($add_add_yn!='Yes') ? 'style="display: none;"' : ''?>>						
                                        <div class="row-fluid">
                                         <div class="span6">
                                            <label class="pull-left">How many address:</label>&nbsp;
                                            <input type="text" id="address_count" name="address_count" onblur="add_address(this.value)" value="<?php if(isset($adresses)){ if(count($adresses)==0){}else{echo count($adresses); } }?>" class="ui-state-valid" style="width:27px;" maxlength="3" pattern="([1-9]{1}|(10)|[0-9]{2,3})">
                                         </div><br>               
                                        </div>
                                        
                                    <div id="address_container"  <?php echo ($add_add_yn!='Yes') ? 'style="display: none;"' : ''?>>
                                    <table class="table table-bordered table-striped table-hover" width="100%">
                                    	<thead>
                                        	<tr>
                                            	<!--<th>#</th>-->
                                            	<th style="width:50px;">Type</th>
                                                <th style="width:26%">Address</th>
                                                <th style="width:65px;">Country</th>
                                                <th style="width: 65px;">State</th>
                                                 <th style="width:100px;">City</th>
                                                <th style="width:64px;">ZIP</th>
                                                <th style="width: 51px;">Ext ZIP</th>
                                               
                                                <th style="width: 51px; ">Directions</th>
                                                <th style="width: 15px;padding: 4px;">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody id="add_address_row">
                                               
  <?php //print_r($adresses);  
  if(isset($adresses)){
	  if(is_array($adresses)){
  $t=1; foreach($adresses as $address) { ?>   
  
                                   
         <tr class="address address_<?php echo $t;?>" id="address_<?php echo $t;?>">
        <input type="hidden" value="<?php  echo $address->id; ?>"  name="add_id[]"/>   
              <td>
              <?php 
			 // print_r($address);
			  $add_zip=	explode("-",$address->zip);	
	 	     $add_country=	$address->country;
			  $add_state=$address->state; 	
			   $address_link = $address->address.', '.$address->city.', '.$address->state.', '.$address->country;
				echo getCatelogDropDown('add_type[]', $catlog_type='address', $attr='class="span12" style="" required', '', '');
				?>
              </td>
              <td><input type="text" name="add_address[]" id="add_address_<?php echo $t;?>" value="<?php echo $address->address;?>"class="span12"></td>
              
                <td> <?php
                 /*getCountryCodeDropDown($name='add_country[]', $att='id="add_country__N_" style="width:135px;" class=" drop" required ', 'code');*/
				// $atrr = array('class="span3"')
				 getCountryDropDown1($name='add_country[]', $att='style="width:100%;" id="add_country_'.$t.'" onChange="getState(this.value, &quot;state_add_show_'.$t.'&quot;, &quot;add_state[]&quot; )"', '', $add_country);
									 ?></td> 
                <td id="state_add_show_<?php echo $t;?>" class="state_add_show"><!--<input type="text" name="add_state[]"  class="span2">-->
                <?php
                getStateDropDown($add_country, $name='add_state[]', $att='class="span12"  id="add_state_'.$t.'"', '', $add_state);
				?>
                </td> 
                 <td><input type="text" name="add_city[]" id="add_city_<?php echo $t;?>" value="<?php echo $address->city;?>" class="span12 "></td>            
            <td><input type="text" name="add_zip[]" id="add_zip_<?php echo $t;?>" class="span12" required <?php echo $number_pattern; ?> value="<?php echo $add_zip[0]; ?>" ></td>
              <td><input type="text" name="add_ext_zip[]" id="add_ext_zip_<?php echo $t;?>" value="<?php echo  $add_zip[1];?>" class="span12" value="<?php echo $add_zip[1]; ?>"></td>
                  
             <td><?php if(!empty($address_link)) {?><a href="http://maps.google.com/?q=<?php echo $address_link; ?>" target="_blank">Google Map</a> <?php } ?></td>
               <td><a href="javascript:;" onclick="removeRow('address_<?php echo $t;?>')" class="remove" title="Delete"></a></td>
                                              	
        </tr>
    
     <?php  $t++; }
  	}
	}
	  ?>
                                            </tbody>
                                    </table>
                                   
                                  </div>
                                 </div>
                               </div>   
                                </fieldset>                
								
					   <input type="hidden" name="edit_ids" value="">
                                      <input type="hidden" name="edit_ids1" id="edit_ids" value="">			
								
            
				 <div id="add_phone" <?php if(empty($app_tel_add)){?> style="display:none;"<?php } ?>>
				      <h5 class="heading sh">Additional Phones</h5>
            <input type="button" class="" style="background-color:white;" id="add_phone_row" value="+ Add"/> 
            <div id="items">
            <div>
            <table class="table" id="table_phone">
              <thead>
                <tr>
                  <th>Phone #</th>
                  <th>Type</th>
                  <th>Attention</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                
								<?php	
										if(!empty($app_tel_add)){	
										//print_r($app_tel_add);
									foreach ($app_tel_add as $value) {?>
									
                              <?php    if($value->insured_telephone_add!='' || $value->insured_telephone_type!='' || $value->insured_telephone_attention!=''){
									?>
									
									<tr>
					<input type="hidden" name="tel_id[]" id="tel_id" value="<?php echo isset($value->tel_id) ? $value->tel_id : '' ?>" />
				<?php  $insured_telephone1 = isset($value->insured_telephone_add) ? explode("-",$value->insured_telephone_add) : '';
				 
				?>
                  <td>
                    <input class="span1 phone" id="insured_telephone_add" name="insured_telephone_add1[]" type="text"  maxlength="3"  pattern="[0-9]+"  value="<?php echo isset($insured_telephone1[0]) ? $insured_telephone1[0] : '';?>"  >
                    <input class="span1 phone" id="insured_telephone_add" name="insured_telephone_add2[]" type="text"  pattern="[0-9]+"  maxlength="3" value="<?php echo isset($insured_telephone1[1]) ? $insured_telephone1[1] : '';?>" >
                    <input class="span1 phone" id="insured_telephone_add" name="insured_telephone_add3[]" type="text"  pattern="[0-9]+"  maxlength="4" value="<?php echo isset($insured_telephone1[2]) ? $insured_telephone1[2] : '';?>" >
                    <input class="span1 phone" id="insured_telephone_add" name="insured_tel_ext_add[]" type="text"  pattern="[0-9]+" placeholder="Ext "  maxlength="5"  value="<?php echo isset($value->insured_tel_ext_add) ? $value->insured_tel_ext_add : '';?>">
                  </td>
                  <td>
				  <?php  $insured_type = isset($value->insured_telephone_type) ? $value->insured_telephone_type : '';
				 
				  ?>
				  
                    <select name="insured_telephone_type[]" class="select-1" id="select_hom_p">
					<option <?php if($insured_type  == '') echo 'selected="selected"'; ?> value="">Select</option>
                      <option <?php if($insured_type  == 'Home') echo 'selected="selected"'; ?> value="Home">Home</option>
                      <option <?php if($insured_type  == 'Business') echo 'selected="selected"'; ?> value="Business">Business</option>
                    </select>
                  </td>
                  <td>
			
                    <input class="textinput" type="text" placeholder="" name="insured_telephone_attention[]" id="textinput_atte_p" value=" <?php  echo  isset($value->insured_telephone_attention) ? $value->insured_telephone_attention : '';?>">
                  </td>
                  <td> <a  class="btn btn-mini btn-1 remove_phone_row" name="button" id="remove_phone_row">X </a>
                  </td>
				 
                </tr>
				<?php //} 
				 }
				 }
				 
				 }else{
				 ?>
				 	<tr>
				<?php  $insured_telephone1 = isset($value->insured_telephone_add) ? explode("-",$value->insured_telephone_add) : '';
				 
				?>
                  <td>
                    <input class="span1 phone" placeholder="Area" id="insured_telephone_add" name="insured_telephone_add1[]" type="text"  maxlength="3"  pattern="[0-9]+"  value="<?php echo isset($insured_telephone1[0]) ? $insured_telephone1[0] : '';?>"  >
                    <input class="span1 phone"  placeholder="Prefix" id="insured_telephone_add" name="insured_telephone_add2[]" type="text"  pattern="[0-9]+"  maxlength="3" value="<?php echo isset($insured_telephone1[1]) ? $insured_telephone1[1] : '';?>" >
                    <input class="span1 phone" placeholder="Sub" id="insured_telephone_add" name="insured_telephone_add3[]" type="text"  pattern="[0-9]+"  maxlength="4" value="<?php echo isset($insured_telephone1[2]) ? $insured_telephone1[2] : '';?>" >
                    <input class="span1 phone" placeholder="Ext" name="insured_tel_ext_add[]" type="text" placeholder="Ext" name="" maxlength="5" value="<?php echo isset($value->insured_tel_ext_add) ? $value->insured_tel_ext_add : '';?>">
                  </td>
                  <td>
				  <?php  $insured_type = isset($value->insured_telephone_type) ? $value->insured_telephone_type : '';
				 
				  ?>
				  
                    <select name="insured_telephone_type[]" class="select-1" id="select_hom_p">
					<option <?php if($insured_type  == '') echo 'selected="selected"'; ?> value="">Select</option>
                      <option <?php if($insured_type  == 'Home') echo 'selected="selected"'; ?> value="Home">Home</option>
                      <option <?php if($insured_type  == 'Business') echo 'selected="selected"'; ?> value="Business">Business</option>
                    </select>
                  </td>
                  <td>
			
                    <input class="textinput" type="text" placeholder="" name="insured_telephone_attention[]" id="textinput_atte_p" value=" <?php  echo  isset($value->insured_telephone_attention) ? $value->insured_telephone_attention : '';?>">
                  </td>
                  <td> <a  class="btn btn-mini btn-1 remove_phone_row" name="button" id="remove_phone_row">X</a>
                  </td>
				 
                </tr>
				 
				 <?php } ?>
              </tbody>
            </table>
			</div> 
            </div>
            </div>
			<div id="add_email1" <?php if(empty($app_email_add)){?> style="display:none;" <?php } ?> >
            <h5 class="heading sh">Additional Email</h5>
			<input type="button" class="" style="background-color:white;" id="add_email_row" value="+ Add"/>
                        <div id="items">
            <table class="table" id="table_email">
              <thead>
                <tr>
                  <th>Email</th>
                  <th>Type</th>
                  <th>Attention</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
			  <?php	
										if(!empty($app_email_add)){							
									foreach ($app_email_add as $value1) {
                                 if($value1->insured_email_add!='' || $value1->insured_email_type!='' || $value1->insured_email_attention!=''){
									
									?>
									
									
                <tr>
				<input type="hidden" id="email_id_add" name="email_id[]" value="<?php echo isset($value1->email_id) ? $value1->email_id : 0 ?>" />
                  <td>
                    <input class="email" type="text" placeholder="" value="<?php  echo  isset($value1->insured_email_add) ? $value1->insured_email_add : '';?>" name="insured_email_add[]" id="email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30">
                  </td>
                  <td>
				  <?php  $insured_type = isset($value1->insured_email_type) ? $value1->insured_email_type : '';?>
                    <select name="insured_email_type[]" class="select-1" id="select_hom_e">
			          <option <?php if($insured_type  == '') echo 'selected="selected"'; ?> value="">Select</option>
                      <option <?php if($insured_type  == 'Home') echo 'selected="selected"'; ?> value="Home">Home</option>
                      <option <?php if($insured_type  == 'Business') echo 'selected="selected"'; ?> value="Business">Business</option>
                    </select>
                  </td>
                  <td>
                    <input class="textinput"  type="text" placeholder="" value="<?php  echo  isset($value1->insured_email_attention) ? $value1->insured_email_attention : '';?>" name="insured_email_attention[]" id="textinput_atte_e">
                  </td>
                  <td><a class="btn btn-mini btn-1 remove_phone_row" name="button" id="remove_phone_row">X </a> <!--<a  class="btn btn-mini btn-1" name="button" id="remove_phone_row">X </a>-->
                  </td>
                </tr>
				<?php 
				 }
				}  }else{
				 ?>
				         <tr>
                  <td>
                    <input class="email" type="text" placeholder="" value="<?php  echo  isset($value1->insured_email_add) ? $value1->insured_email_add : '';?>" name="insured_email_add[]" id="email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30">
                  </td>
                  <td>
				  <?php  $insured_type = isset($value1->insured_email_type) ? $value1->insured_email_type : '';?>
                    <select name="insured_email_type[]" class="select-1" id="select_hom_e">
			          <option <?php if($insured_type  == '') echo 'selected="selected"'; ?> value="">Select</option>
                      <option <?php if($insured_type  == 'Home') echo 'selected="selected"'; ?> value="Home">Home</option>
                      <option <?php if($insured_type  == 'Business') echo 'selected="selected"'; ?> value="Business">Business</option>
                    </select>
                  </td>
                  <td>
                    <input class="textinput"  type="text" placeholder="" value="<?php  echo  isset($value1->insured_email_attention) ? $value1->insured_email_attention : '';?>" name="insured_email_attention[]" id="textinput_atte_e">
                  </td>
                  <td> <a  class="btn btn-mini btn-1" name="button" id="remove_phone_row">X</a>
                  </td>
                </tr><?php } ?>
              </tbody>
            </table>
			</div>
			</div>
</fieldset>


 

                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                       <!--    <fieldset id="section4" class="number_of_owner_box">
                                <legend>Number of Owner</legend>

                                <div class="row-fluid ">  	
                                    <div class="span12 lightblue">
                                        MAYANK
                                        <label>Number of owner of the vehicle or entity?</label>
										<!--END MAYANK
                                        <input required="" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" type="text" id="number_of_owner" name="number_of_owner" onchange="add_owner(this.value)" class="span1" value="">
                                    </div>
                                </div>

  

                                    <div id="row_owner" style="display:none;">
                                        <div class="row-fluid">  
                                            <div class="span12 lightblue">
                                                <table id="owner_table" class="table table-bordered table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Middle Name</th>
                                                            <th>Last Name</th>
                                                            <th> Driver ?</th>
                                                            <th>Driver License number</th>
                                                            <th>License issue state</th>

                                                        </tr>

                                                    </thead>
                                                    <tbody id="add_owner_row">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> 

                                <!--span id="add_driver_div"></span

                            </fieldset>



                       
                       
                       
                       
                       
                       
                       
                            <fieldset id="section5" class="fillings_section_box">
                                <legend>Filing Section</legend>
						<!--MAYANK-->
  
                                    <div class="row-fluid">  
                                        
										<script type="text/javascript">
											$(document).ready(function(){
												$("#filings_need").change(function(){
													if($(this).val()=="yes") {
														$("#number_of_filings_div").show();
														$('#number_of_filings').attr('required', 'required'); 
														
													}else{
														$("#number_of_filings_div").hide();
														$('#number_of_filings').removeAttr('required'); 
														}
												});
											});
											
										/*	function add_filing(val){
													if(val >=0 ){
														if(val == 0)
														{
															$("#filing_table").hide();
														}
														//$("#filing_table").show();
														$("#row_filing").show();
														//$("#add_filing_row").html('');
														var i =1;
														var row = '<tr>\
														<td><input type="text" name="filing_type[]" class="span12 filing_type" required="required" /></td> \
														<td><input type="text" name="filing_numbers[]" class="span12 filing_numbers"  required="required" /></td> \
														</tr>';
														var currcount = $("#add_filing_row tr").length;
														var diff = currcount - val;
														
														 if(diff < 0) {
																while(diff < 0){
																	var v = $('#add_filing_row').append(row) ; 
																	diff++;
																	
																} 
															 }
															 else if(diff > 0)
															 {
																
																 while(diff > 0){
																
																	 var v = $('#add_filing_row tr:last').remove() ; 
																	 diff--;
																 }
															 }	
															
														apply_on_all_elements();
													}
												}*/
										</script>
									<!--	<div class="span12 lightblue">
											<label>Do you need Filing / Filings :</label>
											<select id="filings_need" name="filings_need" required="required">
												<option value="">--Select--</option>
												<option value="yes">YES</option>
												<option value="no">NO</option>
											</select>
                                        </div>
										
										<div class="span12 lightblue" id="number_of_filings_div" style="display:none;">
											<label>Number of Filings :</label>
											<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" id="number_of_filings" name="number_of_filings" class="span1 filing_numbers" onchange="add_filing(this.value)">
										</div>
										
										<div id="row_filing" style="display:none;">
											<div class="row-fluid">  
												<div class="span6 lightblue">
													<table id="filing_table" class="table table-bordered table-striped table-hover">
														<thead>
															<tr>
																<th>Filing Type:</th>
																<th>Filing Num.</th>
															</tr>
														</thead>
														<tbody id="add_filing_row">
														</tbody>
													</table>
												</div>
											</div>
										</div> 
									</div>							
		                                <!--END OF MAYANK-->




                         

				

                            
                            
                                    <div class="form-actions">
									
                                        <!--button class="btn btn-primary" type="submit" onclick="return check_vehicle_entry();">Submit Quote</button-->
                                        
                                    <!-- <button class="btn btn-primary" type="submit" id="subm" onclick="" style="float:right; padding:5px 5px;">Next Step</button>
                                        
                                    <button class="btn btn-primary" type="submit" name="save" id="subm1" onclick="" style="float:right; margin-right: 5px; padding:5px 5px;">Save</button> 

                               <!--       <button class="btn btn-primary" type="submit" name="save" id="subm" onclick="" style="float:right; margin-right: 5px; padding:5px 5px;">Save</button> -->
									    
                                       <!-- <button class="btn" type="reset">Cancel</button>-->
                                  
                            </div>
                        </div>
					</div>	
                    <?php $data_loss['apps'] = $apps;?>
                   <?php $this->load->view('application_losses', $data_loss); ?>
</form>
<?php 
if(!isset($show_hed)){
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('upload_file');

	$this->load->view('includes/footer');
}
}
?>




 <script src="<?php echo base_url('js');?>/jquery.knob.js"></script>

		<!-- jQuery File Upload Dependencies -->
		<script src="<?php echo base_url('js');?>/jquery.ui.widget.js"></script>
		<script src="<?php echo base_url('js');?>/jquery.iframe-transport.js"></script>
	<script src="<?php echo base_url('js');?>/jquery.fileupload.js"></script>
		
		<!-- Our main JS file -->
		<script src="<?php echo base_url('js');?>/scripts_file.js"></script>
<script>
	 $(document).ready(function() {
	 
	 
	 
	/*	
      $('#date_from_effective1').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate1 = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to_expiration1').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to_expiration1').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from_effective1').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		*/
		
		var app_val_text=$('#id_text_app_val').val();
		var myarray_val = app_val_text.split(',');
		//alert($.inArray("Liability",myarray_val ));
		//alert(app_val_text);
		if(app_val_text!=''){
		if(($.inArray("Liability",myarray_val )> -1) || ($.inArray( "Cargo",myarray_val )>-1) || ($.inArray( "Physical Damage",myarray_val )> -1) ){

$("#remarks_id_show").hide();
}else{

$("#remarks_id_show").show();
}
}else{
$("#remarks_id_show").hide();
}
/*var tel_id = $('#tel_id').val();

var email_id_add = $('#email_id_add').val();


if(email_id_add>0){
   $( "#add_email1" ).slideToggle( "slow", function() {
		});
}
if(tel_id>0){



		 $( "#add_phone" ).slideToggle( "slow", function() {
		});
}*/
//var p_array = $('#insured_telephone1').val();

//var a=<?php echo json_encode($value1); ?>;
//document.write('<?php echo $value1; ?>');
		//alert(s_array);
		});
		
		
		
		$("#click_add_phone").click(function(){
          // $("#add_phone").show();
		   $( "#add_phone" ).slideToggle("slow", function() {
    // Animation complete.
	if ($.trim($('input.textinput.span1').val()) != '')
{
      $('input.textinput.span1.phone').val('');
	  }
	  if ($.trim($('#select_home_p').val()) != '')
{
	   $('#select_home_p').val('');
	   }
	   if ($.trim($('#textinput_att_p').val()) != '')
{
	  $('#textinput_att_p').val('');
}
  });
          });
		  	$("#click_add_email").click(function(){
             $( "#add_email1" ).slideToggle( "slow", function() {
    // Animation complete.
	if ($.trim($('input.textinput.email').val()) != '')
{
      $('input.textinput.email').val('');
	  }
	  	if ($.trim($('#select_home_e').val()) != '')
{
	   $('#select_home_e').val('');
	   }	if ($.trim($('#textinput_att_e').val()) != '')
{
	  $('#textinput_att_e').val('');
}
  });
          });
		  

		  
		  
        </script>
<script>

</script>
        <script>
	$(document).ready(function(){
		//when the Add Filed button is clicked
		$("#add_phone_row").click(function (e) {
			//alert();
			//Append a new row of code to the "#items" div
			$("#table_phone").append('<tr><td><input class="textinput span1 phone" type="text" placeholder="Area" id="insured_telephone1" name="insured_telephone_add1[]" maxlength="3"  pattern="[0-9]+"  value=""  >&nbsp;<input class="textinput span1 phone" id="insured_telephone2" type="text" placeholder="Prefix" name="insured_telephone_add2[]" maxlength="4"  pattern="[0-9]+"  value=""  >&nbsp;<input class="textinput span1 phone" id="insured_telephone3" type="text" placeholder="Sub" name="insured_telephone_add3[]" maxlength="3"  pattern="[0-9]+"  value=""  >&nbsp;<input class="textinput span1" type="text" placeholder="Ext" pattern="[0-9]+" name=insured_tel_ext_add[]></td><td><select name="insured_telephone_type[]" id="select_home_p" class="select-1"><option  value="">Select</option><option value="Home">Home</option><option value="Business">Business</option></select></td><td><input class="textinput" type="text" placeholder="" id="textinput_att_p" name="insured_telephone_attention[]"></td><td> <button class="btn btn-mini btn-1 remove_phone_row" id="remove_phone_row"><i class="icon icon-remove"></i></button></td></tr>');
		});

	$("body").on("click", ".remove_phone_row", function (e) {
		//alert($(this).parent("div"));
		$(this).closest('tr').remove();
	});

	});
	
	 //var value21='';


	$(document).ready(function(){
		
		
		
		 
			 
			           $('.name_val').on('keyup',function(e){
						   
						    var value21=$('#fname').val();
							//console.log(value21);
							var value22=$('#mname').val();
						    var value23=$('#lname').val();
						    var value24=$('#dbaname').val();
							
							var value25=value21+' '+value22+' '+value23;
							//console.log(value24);
							if(value24!=''){
							  $('.supplement_dba').val(value25+'/'+value24);
							}else{
								$('.supplement_dba').val(value25);
								}





                           });


$('body').on('keyup','.val_inserts', function (e) {
//alert();
var id=$(this).attr('data');
//console.log(id);
if(id!=''){

var fname=$('#first_name'+id).val();
var mname=$('#middle_name'+id).val();
var lname=$('#last_name'+id).val();
var lnumber=$('#lnumber'+id).val();
var Snumber=$('#lnumber'+id).val();

//console.log(fname+mname+lname);
//alert(fname);
$('.fname'+id).val(fname) 
$('.mname'+id).val(mname) 
$('.lname'+id).val(lname) 
$('.license'+id).val(lnumber)
$('.ssn'+id).val(Snumber) 
}

});



    
				 var myarray2=$('#app_id').val();
		 
	//alert();
		 //alert(myarray2);
				//var myarray12 = arr1.split(',');
			    //  alert(myarray12);
				if($.inArray("Cargo", myarray2 )> -1){
					//alert();
				$(".cargo_show").show();
				//$("#badge_id_3").show();
				// alert(1);
				}else{
				$(".cargo_show").hide();
				//alert(2);
			//	$("#badge_id_3").hide();
				}

		});
	
	
	
	$(document).ready(function(){
		//when the Add Filed button is clicked
		$("#add_email_row").click(function (e) {
			//alert();
			//Append a new row of code to the "#items" div
			$("#table_email").append('<tr><td><input class="textinput email" type="text" placeholder="" id="email" name="insured_email_add[]" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30"></td><td><select name="insured_email_type[]" id="select_home_e" class="select-1"><option  value="">Select</option><option value="Home">Home</option><option value="Business">Business</option></select></td><td><input class="textinput" type="text" placeholder="" id="textinput_att_e" name="insured_email_attention[]"></td><td> <button class="btn btn-mini btn-1 remove_phone_row" id="remove_phone_row"><i class="icon icon-remove"></i></button></td></tr>');
		});

	$("body").on("click", ".remove_email_row", function (e) {
		//alert($(this).parent("div"));
		$(this).closest('tr').remove();
	});
	});
		function audit(val) {
    var val=$("#audit_name").val();
	
	if(val === "Other"){
			$("#Specify").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify").hide();
		}
		}
	function audit1() {
    var val=$("#audit_name1").val();
	
	if(val === "Other"){
		
			$("#Specify_other").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify_other").hide();
		}
		}
		
		
		function application_select(){
		//remarks_id_show
		//alert();

				//alert(myarray[0]);
				//alert($.inArray( "Liability", arr ));
				
		var foo = []; 
    $('#app_id :selected').map(function(i, selected){ 
  foo.push($(this).val()); 
 
});
var val_app1=foo;




		$("#id_text_app_val").val(val_app1);
		var arr=$('#id_text_app_val').val();
				myarray = arr.split(',');
		
		if($('#losses_need').val()=="yes")
			{
				$("#losses_div").show();
				

				//alert(myarray[0]);
				//alert($.inArray( "Liability", arr ));
				if($.inArray( "Liability", myarray )> -1){
				$("#losses_lia").show();
				}else{
				$("#losses_lia").hide();
				}
				if($.inArray( "Physical Damage", myarray )> -1){
				$("#losses_pd").show();
				}else{
				$("#losses_pd").hide();
				}
				if($.inArray( "Cargo", myarray )> -1){
				$("#losses_cargo").show();
				}else{
				$("#losses_cargo").hide();
				}
				if($.inArray( "Bobtail", myarray )> -1){
				$("#losses_bobtail").show();
				}else{
				$("#losses_bobtail").hide();
				}
				if($.inArray( "Dead head", myarray )> -1){
				$("#losses_dead").show();
				}else{
				$("#losses_dead").hide();
				}
				if($.inArray( "Non Trucking", myarray )> -1){
				$("#losses_trunk").show();
				}else{
				$("#losses_trunk").hide();
				}
				if($.inArray( "Excess", myarray )> -1){
				$("#losses_excess").show();
				}else{
				$("#losses_excess").hide();
				}				
				}
				if($.inArray( "Liability", myarray )> -1){
				//$("#losses_lia").show();
				$("#filings_id").show();
				 
				}else{
				//$("#losses_lia").hide();
				$("#filings_id").hide();
				}
				if(($.inArray( "Liability", myarray )> -1) || ($.inArray( "Cargo", myarray )> -1) ){
				//$("#losses_lia").show();
				$("#badge_id_2").show();
				$("#badge_id_4").show();
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_2").hide();
				$("#badge_id_4").hide();
				}
				if($.inArray( "Cargo", myarray )> -1){
				//$("#losses_lia").show();
				$("#badge_id_3").show();
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_3").hide();
	
	}
	if(arr!=''){
if(($.inArray( "Liability", myarray )> -1) || ($.inArray( "Cargo", myarray )> -1) || ($.inArray( "Physical Damage", myarray )> -1) ){

$("#remarks_id_show").hide();
}else{
$("#remarks_id_show").show();
}
}else{
$("#remarks_id_show").hide();
}

if($.inArray( "Cargo", myarray )> -1){
				$(".cargo_show").show();
				$("#badge_id_3").show();
				// alert(1);
				}else{
				$(".cargo_show").hide();
			//	alert(2);
				$("#badge_id_3").hide();
				}


// alert(foo);
	/*	var foo = []; 
$('#app_id :selected').map(function(i, selected){ 
  foo.push($(this).val()); 
 
});
var val_app1=foo;
		$("#id_remarks_text").val(val_app1);
	
	var val_app=$("#app_id").val();
		//alert(val_app);
		var val_remark=$("#id_remarks_text").val();
		// var myarray=[];
        // myarray.push(val_app);
		 // alert($(this).attr("selected",true));
		if(val_remark === ""){
		$("#id_remarks_text").val(val_app);		
		}else
		{
		  myarray = val_remark.split(',');
		 //alert(myarray[0]);
	
		//alert(myarray.indexOf( val_app ));
		
		if(myarray.indexOf( val_app )>=0){
			//alert(2);				
		 }else{
		// alert(3);
	//var val_app1=val_remark+","+val_app;
		//$("#id_remarks_text").val(val_app1);
		 }
		}*/
		
		
		}

</script>
<script>
						function show_hide_section(val, id)
{
	//alert(val);
	if(val=='Yes')
	{
		$("#"+id).show();	
	}
	else if(val=='No')
	{
		$("#"+id).hide();
	}
}

					
					
										 function trans_val(){
										 var n=$("#transported_id").val();
										 if(n==="No"){
										 $("#text_no").show();										 
										 }else{
										 $("#text_no").hide();	
										 
										 }
										 }
										 </script>
										 <script>
										 function Company_trucking(){
										 if($("#trucking_comp_id").val() ==="Other"){
										 $("#trucking_specify").show();
										 }else{
										 $("#trucking_specify").hide();
										 }
										 }
										 function certificate_app_val(){
										// alert($("#certificate_id_val").val());
										 if($("#certificate_id_val").val() ==="Yes"){
										 //alert(1);
										 $("#certificate_app_id").show();
										 }else{
										 $("#certificate_app_id").hide();
										// alert(2);
										 }
										 }
										/* function owner_vehicle1(){
										 if($("#owner_id").val() ==="No"){
										 
										 $("#insurance_vehicle").show();
										// $("#Policy_id").show();
										 }else{
										 $("#insurance_vehicle").hide();
										// $("#Policy_id").hide();
										// alert(2);
										 }
										 }*/
										 function insurance_val_func(){
										  if($("#insurance_val_id").val() ==="Yes"){
										 //alert(1);
										 $("#Policy_id").show();
										 }else{
										 $("#Policy_id").hide();
										// alert(2);
										 }
										 
										 }
										 	

function load_trucks(){

	 var n=$("#loaded_trucks_id").val();
										 if(n==="Yes"){
										 $("#loaded_id").show();										 
										 }else{
										 $("#loaded_id").hide();	
										 
										 }

}						
          function primary_coverage(){
		  var n=$("#primary_coverage_id").val();
										 if(n==="Yes"){
										 $("#coverage_id").show();										 
										 }else{
										 $("#coverage_id").hide();	
										 
										 }
		  
		  }
					function inspection_func(){
					if($("#inspection_id").val()=== "Yes"){
					  $("#if_yes").show();
					}else{
					  $("#if_yes").hide();
					}
					
					}
					
					
	function broker_info(id){
					
	 
		   $.ajax({
			url: '<?php echo site_url('application_form/newMembers');?>',
			type: 'post',
			data: {ids:id},
			success:function(json){
			
			var user_data = $.parseJSON(json);
			console.log(user_data);
			//var obj = JSON.parse(json);
			$.each(user_data['broker_inform'], function(index, value){
			
				//$br=index['broker_remarks'];
			$('#broker_name_dba').val('');
		//	city,country,state,country,zip_code,zip_code_extd,fax_number,sec_email,website
				var ba=value['address'];	
				//alert(ba);
				
				var bc=value['city'];
				var bc1=value['country'];
				var bs=value['state'];
				var bz=value['zip_code'];
				var bz1=value['zip_code_extd'];
				var bf=value['fax_number'];
				var bs1=value['sec_email'];
				var bw=value['website'];
				
				$('#broker_city').val(bc);
				$('#broker_country').val(bc1);
				$('#broker_state').val(bs);
				$('#broker_zip_code').val(bz);
				$('#broker_zip_code_extd').val(bz1);
				$('#broker_website').val(bs1);
				$('#broker_sec_email').val(bw);
				
			$('#broker_address').val(ba);
			
			
				var bt=value['phone_number'];	
				
				var res = bt.split("-");
				
			    var p1 = bf.substr(0, 3);
				var p2 = bf.substr(3, 3);
				var p3 = bf.substr(6, 10);
				
				
				$('#brokr_fax_number1').val(p1);
				$('#brokr_fax_number2').val(p2);
				$('#brokr_fax_number3').val(p3);
				
				var phn4='';
				if( typeof res[0] == "undefined" ) { phn1 = '' } else { phn1 = res[0]; }
				if( typeof res[1] == "undefined" ) { phn2 = '' } else { phn2 = res[1]; }
				if( typeof res[2] == "undefined" ) { phn3 = '' } else { phn3 = res[2]; }
				if( typeof res[3] == "undefined" ) { phn4 = '' } else { phn4 = res[3]; }
				if(value['phone_ext']==0){}else{ phn4=value['phone_ext']; }
				 
				$('#broker_tel1').val(phn1);
				$('#broker_tel2').val(phn2);
				$('#broker_tel3').val(phn3);
				$('#broker_tel_ext').val(phn4);
				
				

				
		//	$('#broker_tel_ext').val($bte);
			
				 var bte=value['phone_ext'];				
			//$('#broker_tel_ext').val(bte);
			
				var be=value['email'];
			$('#broker_email').val(be);
			
		//	$br=index['broker_remarks'];
			$('#broker_remarks').val('');
			
			//console.log(value);
			});
			
			}
		});
				
					
					}
				
					
 var _startDate = new Date(); //todays date
  var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
  
  $('#date_from_effect').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
    //startDate: _startDate,
    endDate: _startDate,
    todayHighlight: true
  }).on('changeDate', function(e){ //alert('here');
   _endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
   $('#date_to_expiration').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
   $('#date_from_effect').mask("99/99/9999");
   
   });
  
$('#date_to_expiration').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
   // startDate: _endDate,
   startDate: _endDate,
    todayHighlight: false
  }).on('changeDate', function(e){ 
   _endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
   //alert(_endDate);
   $('#date_from_effect').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
            $('#date_to_expiration').mask("99/99/9999"); 

 });

	
		$('.datepick').mask("99/99/9999");
				
$(document).ready(function(e) {
    var required_fields = new Array();
   $('form select, form input, form textarea').each(function(index, element) {
    var name = $(this).attr('name');       
    var required = $(this).attr('required');
    //console.log(name);
    if(required){
     $(this).removeAttr('required');
     $(this).addClass('required');
     
     required_fields[name] = 'required';  
    } 
          
   });
 $.validator.addMethod("uniqueName", function(value, element) {
	// alert(value);
    var parentForm = $(element).closest('form');
    //console.log(parentForm);
    if ($(parentForm.find('.required[value=' + value + ']')).size() > 1) {     
     return false;
    }
    else {
     //return true;
    }
   }, "");
   
   
 $.validator.addClassRules({
    NameField: {
     required: true,
     //minlength: 2,
     uniqueName: true
    }
   });
		
	
	$("#main_form").validate();	
});		

	 function validErrorInput(){
			
   $('form select,form input').each(function(index, element) {
    var error = $(this).hasClass('error'); 
	//console.log(error);
    if(error){
     $(this).focus();
	// $(this).addClass('ui-state-valid');
	 //$(this).addClass('ui-state-error');
     event.preventDefault();
     return false;  
    } 
   });
  }


function validateForm(){
//alert();
  
   //event.preventDefault();
   validErrorInput(); 
	var form_company=	$('#company').val();
	var form_valid=	$('#app_id').val();
		   if(!form_company){
       // alert('Fill Application');
	   //alert(form_valid);
        event.preventDefault() ;
		
		$('.dd').focus();
		$( ".dd" ).addClass( "ui-state-error" );
		//$('#company_title ul li').focus();
		
        }else{
			$( ".dd" ).removeClass( "ui-state-error" );
			}
        if(!form_valid){
       // alert('Fill Application');
	   //alert(form_valid);
        event.preventDefault() ;
		
		$('#app_id_chzn input').focus();
		$( "#app_id_chzn" ).addClass( "ui-state-error" );
		//$('#company_title ul li').focus();
		
        }else{
			$( "#app_id_chzn" ).removeClass( "ui-state-error" );
			
			}
  
  }
         

	
$(document).ready(function() { 

$('#company').change(function(){
	//alert();
var v1=	$('#company').val();
//alert(v);
if(!v1){}else{
	$( ".dd" ).removeClass( "ui-state-error" );
	}
	});
});	 
                       /*<!--   $('#fname').live(function () {
							  alert();
							  //value=val;
							  }-->*/
				  /*function first_value(val){
					value=val;
					alert(value);
					
					}
	*/
			/*	function stopDefAction(event){
						//alert();
				var form_valid=	$('#app_id').val();
				//alert();
					//alert(form_valid);
			//$('input:text[required]').focus();		
			

			validErrorInput();
			
	// $('select').focus();
	//$('textarea').focus();
 // $("#app_form").submit();
    if(!form_valid){
       // alert('Fill Application');
	   //alert(form_valid);
        event.preventDefault() ;
		
		$('#app_id_chzn input').focus();
		$( "#app_id_chzn" ).addClass( "ui-state-error" );
		//$('#company_title ul li').focus();
		
        }
		
		
		var form_company=	$('#company').val();
		   if(!form_company){
       // alert('Fill Application');
	   //alert(form_valid);
        event.preventDefault() ;
		
		$('.dd').focus();
		$( ".dd" ).addClass( "ui-state-error" );
		//$('#company_title ul li').focus();
		
        }
		   /* if(!$('#company').val()){
    event.preventDefault() ;
		
		$('.dd').focus();
		$( ".dd" ).addClass( "ui-state-error" ); 
    return false; 
   }else{
    $('.dd').removeClass('ui-state-error');   
   }
        if(!$('#app_id').val()){
    event.preventDefault() ;
		
		$('#app_id_chzn input').focus();
		$( "#app_id_chzn" ).addClass( "ui-state-error" );   
    return false; 
   }else{
    $('#app_id').removeClass('ui-state-error');   
   }


}
					
	
	document.getElementById('save').addEventListener(
    'click', stopDefAction, false
);		
*/
/*
$(document).ready(function() { 

$('#app_id_chzn').change(function(){
var v=	$('#app_id').val();
//alert(v);
if(!v){}else{
	$( "#app_id_chzn" ).removeClass( "ui-state-error" );
	}
	});
});

$(document).ready(function() { 

$('#company').change(function(){
	//alert();
var v1=	$('#company').val();
//alert(v);
if(!v1){}else{
	$( ".dd" ).removeClass( "ui-state-error" );
	}
	});
});
*/





										 </script>

  


<?php if(isset($show_hed)){?>
 <script>
	function resizeInput() {		
		$(this).css('width', 'auto');
		if($(this).val().length){
    		$(this).attr('size', $(this).val().length);
		}else{
			$(this).attr('size', 1);	
		}
	}
	
	/*$('input')
		// event handler
		.keyup(resizeInput)
		// resize on page load
		.each(resizeInput);*/
	</script>
<?php } ?>

<script>
  function getState(val, id, name, attr)
{
	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('index.php/application_form/getState');?>", 
		data:"id="+val+"&name="+name+"&attr="+attr+"",
		success: function(json) 
		{
		   //alert(json);	
		   $("#"+id).html(json);	  
		  // $("#email_add__"+id).hide();		 
		}
	});	
}
</script>
<div style="display:none;">

<table >

	<tbody id="address_row">

        <tr class="address address__N_" id="address__N_">

              <td>

              <?php 				

				echo getCatelogDropDown('add_type[]', $catlog_type='address', $attr='class="span12" style="" required', '', '');

				?>

              </td>

              <td><input type="text" name="add_address[]" id="add_address__N_" class="span12"></td>

              

                <td> <?php

                 /*getCountryCodeDropDown($name='add_country[]', $att='id="add_country__N_" style="width:135px;" class=" drop" required ', 'code');*/

				// $atrr = array('class="span3"')

				 getCountryDropDown1($name='add_country[]', $att='style="width:100%;" id="add_country__N_" onChange="getState(this.value, &quot;state_add_show__N_&quot;, &quot;add_state[]&quot; )"', '', 'US');

									 ?></td> 

                <td id="state_add_show__N_" class="state_add_show"><!--<input type="text" name="add_state[]"  class="span2">-->

                <?php

                getStateDropDown('US', $name='add_state[]', $att='class="span12"  id="add_state__N_"', '', 'California');

				?>

                </td> 

                 <td><input type="text" name="add_city[]" id="add_city__N_" class="span12 "></td>            

               <td><input type="text" name="add_zip[]" id="add_zip__N_" class="span12" required <?php echo $number_pattern; ?> ></td>

              <td><input type="text" name="add_ext_zip[]" id="add_ext_zip__N_" class="span12" <?php echo $number_pattern; ?>></td>

                  

              <td></td>

               <td><a href="javascript:;" onclick="removeRow('address__N_')" class="remove" title="Delete"></a></td>

                                              	

        </tr>

    </tbody>

</table>

</div>

<div class="ac_location_state" style="display:none;">

<?php get_state_dropdown('st_name[]', 'CA', 'class="span6"'); ?>

</div>