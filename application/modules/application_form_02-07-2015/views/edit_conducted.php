<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/head_style_script');	
} 
?>
<html lang="en">
  
  <head>

    <title>GHI-8</title>
  </head>
  
  <body>
<form action="<?php echo base_url();?>application_form/applicant/<?php echo isset($quote_id) ?$quote_id :'';?>" method="post" accept-charset="utf-8" id="main_form" enctype="multipart/form-data" novalidate="novalidate"/>
    <div class="container-fluid">
      <div class="page-header">
        <h1>GHI-8 <small>Previous / Subsidiary name supplement form</small>
        </h1>
      </div>
	  <input type="hidden" id="id_text_app_val" name="id_text_app_val[]" value="<?php echo isset($app_val[0]) ? $app_val[0] : '';?>"/>
      <div class="well">
        <div class="row-fluid">
          <span class="span6">
            <div class="row-fluid">
              <span class="span12">
                <label class="control-label">Applicant's Name</label>
                <input class="textinput span5" type="text" name="applicant_fname" value="" placeholder="First name">
                <input class="textinput span3" type="text" placeholder="Middle name" name="applicant_mname">
                <input class="textinput span4" type="text" placeholder="Last name" name="applicant_lname">
              </span>
            </div>
          </span>
          <span class="span6">
            <label class="control-label">DBA(if applicable)</label>
            <input class="textinput span12" type="text" name="applicant_dba" value="" placeholder="DBA">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">Address &nbsp;</label>
            <input class="textinput span11" type="text" placeholder="" name="applicant_address">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">City &nbsp;</label>
            <input class="textinput span10" type="text" placeholder="" name="applicant_city">
          </span>
          <span class="span4">
            <label class="control-label pull-left">Country &nbsp;</label>
            <input class="textinput span9" type="text" placeholder="" name="applicant_country">
          </span>
          <span class="span2">
            <label class="control-label pull-left span4">State &nbsp;</label>
            <select name="applicant_state" class="span6">
              <option value="CA">CA</option>
              <option value="TX">TX</option>
            </select>
          </span>
          <span class="span2">
            <label class="control-label pull-left">Zip &nbsp;</label>
            <input class="textinput span9" type="text" placeholder="" name="applicant_zip">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">Date operation started &nbsp;</label>
            <input class="textinput pull-left input-small" type="text" id="date_from" placeholder="mm/dd/yyyy" name="applicant_from">
          </span>
          <span class="span4">
            <label class="control-label pull-left">&nbsp; &nbsp;To &nbsp;</label>
            <input class="textinput input-small" type="text" id="date_to" placeholder="mm/dd/yyyy" name="applicant_to">
          </span>
          <span class="span4">
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">CA# &nbsp;</label>
            <input class="textinput" type="text" placeholder="" name="applicant_ca">
          </span>
          <span class="span4">
            <label class="control-label pull-left">MC# &nbsp;</label>
            <input class="textinput" type="text" placeholder="" name="applicant_ma">
          </span>
          <span class="span4">
            <label class="control-label pull-left">DOT# &nbsp;</label>
            <input class="textinput" type="text" placeholder="" name="applicant_dot">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">Is this entity still in operation? &nbsp;</label>
            <select name="applicant_entity">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">If yes, please give full details and role of who controls it.</label>
            <textarea class="span12" placeholder="" name="applicant_role"></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">If no, please give detail of termination and explain reason for termination.</label>
            <textarea class="span12" placeholder="" name="applicant_explain"></textarea>
          </span>
        </div>
      </div>
      <div>
        <div class="form-actions">
          <button class="btn btn-primary" name="save1">Save</button>
          <button class="btn">Cancel</button>
        </div>
      </div>
    </div>
	</form>
  </body>

</html>

<script>
	 $(document).ready(function() {
	  var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		 var _startDate1 = new Date(); //todays date
		var _endDate1 = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		$('#date_from').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate1 = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		});
		</script>