<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php
/*if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');	
} */
?>
<script src="<?php echo base_url('js'); ?>/msdropdown/jquery.dd.min.js" type="text/javascript"></script>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('css');?>/msdropdown/dd.css" />   
   <script language="javascript">
		$(document).ready(function(e) {
		try {
		$("#company").msDropDown();
		} catch(e) {
		alert(e.message);
		}
		});
		</script>
				<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
  <!--   	<script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>-->
    	<script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
    	<script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script >
		<script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>

		<script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url('js'); ?>/application.js"></script>
		<!--script src="<?php echo base_url('js'); ?>/jquery.validateForm.js"></script-->
   

        <style>
            .table thead th {
                text-align: center;
                vertical-align: inherit;
            }

            .table th, .table td {
                font-size: 0.8em;
            }

            .row_added_vehicle{
                margin:0px 0px 5px 0px;
            }
            .status_msg {
                color: #ff0000;
            }
			
			#upload_file_name_second{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second input{
				display: none;
			}
			
			#upload_file_name_second li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second li span{
				width: 15px;
				height: 12px;
				background: url('images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second li.error p{
				color:red;
			}

				#upload_file_name_second_div{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second_div li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second_div input{
				display: none;
			}
			
			#upload_file_name_second_div li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second_div li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second_div li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second_div li span{
				width: 15px;
				height: 12px;
				background: url('<?php echo base_url()?>images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second_div li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second_div li.error p{
				color:red;
			}

			
			
			#upload_file_name1{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name1 li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name1 input{
				display: none;
			}
			
			#upload_file_name1 li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name1 li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name1 li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name1 li span{
				width: 15px;
				height: 12px;
				background: url('images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name1 li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name1 li.error p{
				color:red;
			}
        </style>	 

<script type="text/javascript">
	
	
			

function apply_on_all_elements(){
		
		//$('input').not('.datepick').autotab_magic().autotab_filter();
		//$('input').not('.require').autotab_magic().autotab_filter();
		//$('select').not('.require').autotab_magic().autotab_filter();
		
		//$('input').attr('required', 'required');
		
		$('.price_format').priceFormat({
		prefix: '$ ',
		centsLimit: 0,
		thousandsSeparator: ','
		});
		
		$(".license_issue_date").mask("99/99/9999");
		
		//  $('input').attr('required', 'required');  
		//$('select').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		
		$('#vehicle_tractor').removeAttr('required');
		$('#vehicle_truck').removeAttr('required');
		$('#zip2').removeAttr( 'required' );
		$('.zip2').removeAttr( 'required' );
		$('.filing_numbers').removeAttr('required');
		$('.filing_type').removeAttr('required');
		$('.last_name').removeAttr('required');
		$('.middle_name').removeAttr('required');
		
		//$('#vehicle_pd').removeAttr('required');
		//$('#vehicle_liability').removeAttr('required');
		//$('#vehicle_cargo').removeAttr('required');
		$('#cab').removeAttr('required');
		$('#tel_ext').removeAttr('required');
		$('#insured_email').removeAttr('required');
		$('#insured_tel_ext').removeAttr('required');
		//alert();
		
		// $('form').h5Validate(); 	
		//alert($(".chosen").val());
}

	$(document).ready(function() {
		
		
$('.datepick').mask('99/99/9999');
	
	 var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		$('.datepick').datepicker({
          //startDate: date,
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		   endDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			//$('.date_to_'+section_name).datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});
	
	

		/*	$('#first_upload').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values').value = '';
				$('#drop a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});*/
			
	/*		$('#first_upload_second').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values_second').value = '';
				$('#drop_second a').parent().find('input').click();
    		});
			*/
			$('#first_upload_second_div').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values_second_div').value = '';
				$('#drop_second_div a').parent().find('input').click();
    		});
			
			
		apply_on_all_elements();
		$("#coverage_date").mask("99/99/9999");
		
	//	$('form').h5Validate();
		
		//$('input').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		//$('input[type="checkbox"]').removeAttr( 'required' );
		$('input[type="file"]').removeAttr( 'required' ); 
		
/* 		$('#coverage_date').datepicker({
			format: 'mm/dd/yyyy'
		}); */
		//$("#coverage_date").mask("99/99/9999");
 	 	/* var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

		var checkin = $('#coverage_date').datepicker({
		onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
		}).on('changeDate', function(ev) {
		// $(this).blur();
		checkin.hide();		 
		}).data('datepicker');   */
		
		/* $('#coverage_date').blur(function() {
  			$('#coverage_date').datepicker("hide");
		});
		 */
		
		//Hide datepicker on Focus Out		
	/* 	$('#coverage_date').blur(function(){
			//setTimeout(function(){$('#coverage_date').datepicker('hide');},100)
		}); */
		
		var date = new Date();
		date.setDate(date.getDate()-0);
				
		 var inputs1 = $('#coverage_date').datepicker({
			startDate: date,
			format: 'mm/dd/yyyy',
			autoclose: true
		});  


		
		$('#dba').change(function(){
			//check_insured_dba_required();			
		});
		
		$('#insured_name').change(function(){
		//alert();
			//check_insured_dba_required();			
		});

		

    var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }

//$('.request_quote').validateForm();
/* 
		var hiddenEls = $("body").find(":hidden").not("script");

	$("body").find(":hidden").each(function(index, value) {
	selctor= this.id;
	if(selctor){
		$( '#'+selctor+' :input').removeAttr("required");
		}
		alert(this.id);
	}); */
	
	/*  $(".request_quote").validateForm({
		ignore: ":hidden",
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			
			$('#loss_report_upload1').click(function() {  
				//var up_id = $('.upload_select:last').attr('id');
				//alert(up_id);
				$('#fileup1_1').click();  
				//$('#'+up_id).click();  
			
			});
			 
			$('#mvr_upload').click(function() { 
			$('#fileup_1').click();  });
 


	});
	
	
			
		function form_sub(){
		/* $(".request_quote").validateForm({
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			//$('.request_quote').validateForm();
			
/* 			if($('#theid').css('display') == 'none'){ 
   $('#theid').show('slow'); 
} else { 
   $('#theid').hide('slow'); 
}
$("br[style$='display: none;']") */


			
	}
	
	function check_insured_dba_required(){
			dba_val = $('#dba').val().trim();
			insured_val = $('#insured_name').val().trim();
			if(dba_val){
				$('#insured_name').removeAttr('required'); 
				$('#insured_middle_name').removeAttr('required');
				$('#dba').attr('required', 'required'); 
			}else if(insured_val){
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').removeAttr('required'); 		
			} else {
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').attr('required', 'required'); 	
			}
		}
		
	
	<!-----updated by amit03-03-2014------>
	$("body").on("change",".rofo:first",function(){
				val = $('.rofo:first').val();
				$('.rofo:first').attr("readonly",false)
				$('.rofothers:first').attr("readonly",false)
				otherval = $('.rofothers:first').val();
				$(".tractor_radius_of_operation").val(val);
				$(".truck_radius_of_operation").val(val);
				if(val == 'other')
				{
					$('.rofothers').show();
					$('.rofothers').val(otherval);
					$('.rofothers').not(":first").attr("readonly",true);
					$(".show_v_others").show();
				}
				
				$(".truck_radius_of_operation").not(":first").attr("readonly",true);
				$(".tractor_radius_of_operation").not(":first").attr("readonly",true);
				$('.show_tractor_others').not(":first").attr("readonly",true);
				$('.show_truck_others').not(":first").attr("readonly",true);
			});
			$("body").on("keyup",".rofothers:first",function(){
				
				$(".rofothers").val($(this).val());
			});
	
			function rofoperation(val , id){
			
				if(id=='1tractor_radius_of_operation')
				{							
					$(".tractor_radius_of_operation").val(val);
					$(".tractor_radius_of_operation").not(":first").attr("disabled",true);
					if(val == 'other'){
						$('.show_tractor_others').show();
						$(".specify_other_cp").not(":first").attr("disabled",true);
					} else {
						$('.show_tractor_others').hide();
					}	
				}
				else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').show();
					} else {
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').hide();
					}	
					
				}
				
				
				
			/*  alert(id);
			alert(val);  */
			
			
			}
			
			
			
			function rofoperation2(val , id){
				
				if(id=='1truck_radius_of_operation')
				{
					$(".truck_radius_of_operation").val(val);
					if(val == 'other'){
						$('.show_truck_others').show();
					} else {
						$('.show_truck_others').hide();
					}
				}	
							else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').show();
					} else {
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').hide();
					}	
					
				}
			}

				
				
				
				
			
					
			
		function check_commodities_other(id){ 
			$("#"+id+" option:selected").each(function(){
			  val = $(this).text(); 
			  if(val == 'other'){ 
					$('.'+id+'_otr').show();
				} else { 
					$('.'+id+'_otr').hide();
				}
			});
		}
	
	
	$(document).ready(function(e) {
        $("body").on("keyup",".commhauled:first",function(){
			$(".commhauled").val($(this).val());
		});
		$("body").on("keyup",".commhauled2:first",function(){
			$(".commhauled2").val($(this).val());
		});
		
		
		 $("body").on("keyup",".cargo_class_price:first",function(){
			$(".cargo_class_price").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_price2:first",function(){
			$(".cargo_class_price2").val($(this).val());
		});
		
		
		$("body").on("keyup",".cargo_class_ded:first",function(){
			$(".cargo_class_ded").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_ded2:first",function(){
			$(".cargo_class_ded2").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp:first",function(){
			$(".specify_other_cp").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp2:first",function(){
			$(".specify_other_cp2").val($(this).val());
		});
			/**Dba required if name if blank or name required if dba is blank By Ashvin Patel 12/may/2014**/

		$('#insured_name').blur(function(){
			//alert();
			var f_name = $(this).val();
			var l_name = $('#insured_last_name').val();
			if(f_name==''||l_name=='')
			{
				$('#dba').attr('required', 'required');
				
			}
			else
			{
				$('#dba').removeAttr('required');
				$('#dba').removeClass('ui-state-error');
			}
		});
	
		$('#insured_last_name').blur(function(){
			//alert();
			var l_name = $(this).val();
			var f_name = $('#insured_name').val();
			if(f_name==''||l_name=='')
			{
				$('#dba').attr('required', 'required');
			}
			else
			{
				$('#dba').removeAttr('required');
				$('#dba').removeClass('ui-state-error');
			}
		});
		$('#dba').blur(function(){
			var dba = $(this).val();
			var f_name = $('#insured_name').val();
			if(dba=='')
			{
				$('#insured_name').attr('required', 'required');
				$('#insured_last_name').attr('required', 'required');
			}
			else
			{
				$('#insured_name').removeAttr('required');
				$('#insured_last_name').removeAttr('required');
				$('#insured_name').removeClass('ui-state-error');
				$('#insured_last_name').removeClass('ui-state-error');
			}
		});
    });
	

		
   
	
	
	
	<!-----updated by amit03-03-2014------>
</script>
       
       <script>
$(document).ready(function(){
	$("#agencies").change(function(){
		//window.location.href = window.location.href + "/" + $(this).val();
		$("#broker_email").val($(this).text());
		
		$.ajax({
			url: '<?php echo base_url();?>manualquote/setpublishersession',
			type: "POST",
			data: {
					action:'via_ajax',
					publisherid : $(this).val()
			},
			success: function(data) {
				
				window.location.reload(true);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(thrownError);
			}
		});
	});
});
function update_unknown(val){
	var owned_veh = $('#trailer_owned_vehicle'+val).val();


	if(owned_veh == 'no'){
		$("#vehicle_year_vh"+val).removeAttr("selected");
		$("#vehicle_year_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#make_model_vh"+val).removeAttr("selected");
		$("#make_model_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#trailer_type_veh"+val).removeAttr("selected");
		$("#trailer_type_veh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
	}else{
		$("#vehicle_year_vh"+val+" option[value='Unknown']").remove();
		$("#make_model_vh"+val+" option[value='Unknown']").remove();
		$("#trailer_type_veh"+val+" option[value='Unknown']").remove();
	}
	/* vehicle_year_vh1-2
	make_model_vh1-2
	trailer_type_veh1-2 */
	//alert();
}


	
	//$("#coverage_date").mask("99/99/9999");
	
	/* ('#dp3').datepicker({
			//format: 'mm/dd/yyyy'
		}); */

    /* $("input").each(function() {
      this.value = "";}) */
    //add_driver();
    //alert($(window).width()); 
	
/* 	$( ".number_limit" ).validate({
rules: {
field: {

range: [1, 99]
}
}
}); */
jQuery(document).ready(function(){
	$(".chzn-select").chosen({ allow_single_deselect: true });
	
	
	
});
</script>

<!--<h2 style="text-align:center;">Application Sheet</h2>
<form action="<?php echo base_url();?>application_form/vehicle/<?php echo isset($app[0]->general_id) ? $app[0]->general_id : '';?>" method="post" accept-charset="utf-8" id="main_form" enctype="multipart/form-data" novalidate="novalidate" id="app_from3"/>-->
<div class="well">
<?php $less_100_limit = ' pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" ';?>
 <input type="hidden" name="ref_id" value="<?php echo isset($ref_id[0]->general_id) ? $ref_id[0]->general_id : '';?>"/>
 
<!--<input type="hidden" name="ref_id" value="<?php echo isset($app[0]->general_id) ? $app[0]->general_id : '';?>"/>-->
<input type="hidden" name="quote_id" value="<?php echo isset($quote_id) ? $quote_id :'';?>"/>
<input type="hidden" name="insert_id" value="<?php echo isset($app[0]->general_id) ? $app[0]->general_id : '';?>" id="insert_id"/>
<!--<input type="hidden" id="id_text_app_val" name="id_text_app_val[]" value="<?php echo implode(",",$app_val);?>"/>-->
<input type="hidden" id="id_text_app_val" name="id_text_app_val[]" value="<?php echo isset($app_val[0]) ? $app_val[0] : '';?>"/>
                             <fieldset id="section4" class="schedule_of_drivers_box">
                                <legend>Vehicle schedule section</legend>

								
								<div class="row-fluid">
          <span class="span12 ">
          <input type="hidden" value="<?php echo isset($app_load[0]->loaded__id) ? $app_load[0]->loaded__id : ''; ?>" name="loadedid" />
            <label class="control-label pull-left">28.Has the applicant ever have a risk declined, non-renewed, or cancelled in the past five years &nbsp;</label>
            <select class="left_pull  width_option" name="Vehicle_declined" id="Vehicle_declined" onChange="Vehicle_declined1(this.value);" required>
            <option value="" >Please select</option>
              <?php $Vehicle_declined = isset($app_load[0]->Vehicle_declined) ? $app_load[0]->Vehicle_declined : '';?>
                <option value="No" <?php if($Vehicle_declined  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($Vehicle_declined  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
             
            </select>
        <!--    <a href="#addrenewed"  class="fancybox edit"><label class="pull-left edit_button" style="display:none" id="addrenew">Edit</label></a>	-->
          </span>
		  		  
        </div>
			<div id="addrenew" style="display:none;">
							    <div class="container-fluid">
      <div class="page-header">
        <h1>GHI-13 <small>Declined coverage supplement form</small>
        </h1>
      </div>
      <p>Please list all insurance carriers who have declined to cover your trucking and/or non-trucking operations in the three years preceding the date of issuance of this policy.</p>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">28.A.How many additional carriers?</label>
        </span>
        <span class="span1">
          <input class="textinput span12" type="text" name="carriers_val" id="carriers_val" value="<?Php if(count($app_ghi13)==0){}else{echo count($app_ghi13); }?>" onchange="carriers_val1(this.value);" placeholder="" pattern="[0-9]+" mAxelngth="2" >
        </span>
        <span class="span7">
        <!--  <button class="btn"> <i class="icon icon-plus"></i> Add</button>-->
        </span>
      </div>
      <div class="well">
      
      <?php  if(!empty($app_ghi13)){  $a=1; foreach($app_ghi13 as $values1){ ?>
      
       <input type="hidden" value="<?php echo  isset($values1->carriers_id) ? $values1->carriers_id : '';?>" name="carriers_id[]" /> 
	  <div id="add_car_row<?php echo $a; ?>" class="carriers_add_row">			
      <div class="row-fluid">           
      <span class="span12">          
       <button class="btn pull-right" style="margin-bottom:5px;" onclick="persons_delete(<?php echo $a; ?>);">Delete</button>		  
        </span>		   </div>
        <div class="row-fluid">
          <span class="span8">
            <label class="control-label">28.AA.Ins. Co. Name &nbsp;</label>
            <input class="textinput span9" type="text"  value="<?php echo  isset($values1->carriers_name) ? $values1->carriers_name : '';?>" placeholder="Declined" name="carriers_name[]">
          </span>
          <span class="span4">
            <label class="control-label">28.AB.Tel &nbsp;</label>
            <input class="textinput span10" type="text"  value="<?php echo  isset($values1->carriers_tel) ? $values1->carriers_tel : '';?>" placeholder="" name="carriers_tel[]" pattern="[0-9]+" >
          </span>
        </div>
        <div class="row-fluid">
          <span class="span8">
            <label class="control-label">28.AC.GA/MGA Name &nbsp;</label>
            <input class="textinput span9" type="text" placeholder=""  value="<?php echo  isset($values1->carriers_ga) ? $values1->carriers_ga : '';?>" name="carriers_ga[]">
          </span>
          <span class="span4">
            <label class="control-label">28.AD.Tel &nbsp;</label>
             <?php $tel=  isset($values1->carriers_telephone) ? explode("-",$values1->carriers_telephone) : '';?>
            <input class="textinput span3" type="text" placeholder="Area" value="<?php echo  isset($tel[0]) ? $tel[0] : '';?>" name="carriers_telephone[1]" pattern="[0-9]+" >
            <input class="textinput span3" type="text" placeholder="Prefix" value="<?php echo  isset($tel[1]) ? $tel[1] : '';?>" name="carriers_telephone[2]" pattern="[0-9]+" >
            <input class="textinput span3" type="text" placeholder="Sub" value="<?php echo  isset($tel[2]) ? $tel[2] : '';?>" name="carriers_telephone[3]" pattern="[0-9]+" >
            <input class="textinput span3" type="text" placeholder="Ext" value="<?php echo  isset($tel[3]) ? $tel[3] : '';?>" name="carriers_telephone[4]" pattern="[0-9]+" >
          </span>
        </div>
        <div class="row-fluid">
          <span class="span8">
            <label class="control-label">28.AE.Retail broker / Agent name &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($values1->retail_broker) ? $values1->retail_broker : '';?>" name="retail_broker[]">
          </span>
          <span class="span4">
            <label class="control-label">28.AF.Telephone</label>
            <?php $ret_tel=  isset($values1->retail_telephone) ? explode("-",$values1->retail_telephone) : '';?>
            <input class="textinput span3" type="text" placeholder="Area" value="<?php echo  isset($ret_tel[0]) ? $ret_tel[0] : '';?>" name="retail_telephone[1]" pattern="[0-9]+">
            <input class="textinput span3" type="text" placeholder="Prefix" value="<?php echo  isset($ret_tel[1]) ? $ret_tel[1] : '';?>" name="retail_telephone[2]" pattern="[0-9]+">
            <input class="textinput span3" type="text" placeholder="Sub" value="<?php echo  isset($ret_tel[2]) ? $ret_tel[2] : '';?>" name="retail_telephone[3]" pattern="[0-9]+">
            <input class="textinput span3" type="text" placeholder="Ext" value="<?php echo  isset($ret_tel[3]) ? $ret_tel[3] : '';?>" name="retail_telephone[4]" pattern="[0-9]+">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">28.AG.Reason</label>
            <textarea class="span12" placeholder="" name="retail_reason[]"><?php echo  isset($values1->retail_reason) ? $values1->retail_reason : '';?></textarea>
          </span>
        </div>
      </div>
     
      
       <div id="add_carriers_row" style="display:none;">
        <div class="row-fluid">
          <span class="span8">
            <label class="control-label">28.AA.Ins. Co. Name &nbsp;</label>
            <input class="textinput span9" type="text" placeholder="Declined" name="carriers_name[]">
          </span>
          <span class="span4">
            <label class="control-label">28.AB.Tel &nbsp;</label>
            <input class="textinput span10" type="text" placeholder="" name="carriers_tel[]" pattern="[0-9]+" >
          </span>
        </div>
        <div class="row-fluid">
          <span class="span8">
            <label class="control-label">28.AC.GA/MGA Name &nbsp;</label>
            <input class="textinput span9" type="text" placeholder="" name="carriers_ga[]">
          </span>
          <span class="span4">
            <label class="control-label">28.AD.Tel &nbsp;</label>
            <input class="textinput span3" type="text" placeholder="Area" name="carriers_telephone[1]"pattern="[0-9]+" >
            <input class="textinput span3" type="text" placeholder="Prefix" name="carriers_telephone[2]" pattern="[0-9]+" >
            <input class="textinput span3" type="text" placeholder="Sub" name="carriers_telephone[3]" pattern="[0-9]+" >
            <input class="textinput span3" type="text" placeholder="Ext" name="carriers_telephone[4]" pattern="[0-9]+" >
          </span>
        </div>
        <div class="row-fluid">
          <span class="span8">
            <label class="control-label">28.AE.Retail broker / Agent name &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" name="retail_broker[]">
          </span>
          <span class="span4">
            <label class="control-label">28.AF.Telephone</label>
            <input class="textinput span3" type="text" placeholder="Area" name="retail_telephone[1]" pattern="[0-9]+">
            <input class="textinput span3" type="text" placeholder="Prefix" name="retail_telephone[2]" pattern="[0-9]+">
            <input class="textinput span3" type="text" placeholder="Sub" name="retail_telephone[3]" pattern="[0-9]+">
            <input class="textinput span3" type="text" placeholder="Ext" name="retail_telephone[4]" pattern="[0-9]+">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">28.AG.Reason</label>
            <textarea class="span12" placeholder="" name="retail_reason[]"></textarea>
          </span>
        </div>
      </div>
      <?php $a++; } } else { ?>
      
      
      
	  <div id="add_carriers_row" style="display:none;">
        <div class="row-fluid">
          <span class="span8">
            <label class="control-label">28.AA.Ins. Co. Name &nbsp;</label>
            <input class="textinput span9" type="text" placeholder="Declined" name="carriers_name[]">
          </span>
          <span class="span4">
            <label class="control-label">28.AB.Tel &nbsp;</label>
            <input class="textinput span10" type="text" placeholder="" name="carriers_tel[]" pattern="[0-9]+" >
          </span>
        </div>
        <div class="row-fluid">
          <span class="span8">
            <label class="control-label">28.AC.GA/MGA Name &nbsp;</label>
            <input class="textinput span9" type="text" placeholder="" name="carriers_ga[]">
          </span>
          <span class="span4">
            <label class="control-label">28.AD.Tel &nbsp;</label>
            <input class="textinput span3" type="text" placeholder="Area" name="carriers_telephone[1]"pattern="[0-9]+" >
            <input class="textinput span3" type="text" placeholder="Prefix" name="carriers_telephone[2]"pattern="[0-9]+" >
            <input class="textinput span3" type="text" placeholder="Sub" name="carriers_telephone[3]"pattern="[0-9]+" >
            <input class="textinput span3" type="text" placeholder="Ext" name="carriers_telephone[4]"pattern="[0-9]+" >
          </span>
        </div>
        <div class="row-fluid">
          <span class="span8">
            <label class="control-label">28.AE.Retail broker / Agent name &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" name="retail_broker[]">
          </span>
          <span class="span4">
            <label class="control-label">28.AF.Telephone</label>
            <input class="textinput span3" type="text" placeholder="Area" name="retail_telephone[1]" pattern="[0-9]+">
            <input class="textinput span3" type="text" placeholder="Prefix" name="retail_telephone[2]" pattern="[0-9]+">
            <input class="textinput span3" type="text" placeholder="Sub" name="retail_telephone[3]" pattern="[0-9]+">
            <input class="textinput span3" type="text" placeholder="Ext" name="retail_telephone[4]" pattern="[0-9]+">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">28.AG.Reason</label>
            <textarea class="span12" placeholder="" name="retail_reason[]"></textarea>
          </span>
        </div>
		</div>
        <?php } ?>
      
	  <div id="add_carriers">
	  </div>
	 </div>
   <!--   <div>
        <div class="form-actions">
          <button class="btn btn-primary" onclick="fancybox_close();">Save</button>
          <button class="btn" onclick="reload_close();">Cancel</button>
        </div>
      </div>-->
    </div>
			</div>				   
		   <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left span3"> 29.Vehicles Schedule</label>
         <!--   <label class="control-label pull-left span4"> Number of vehicles type &nbsp;</label>
            <input class="textinput span2" type="text" placeholder="" name="vehicles_type">-->
          </span>
        </div>
		     <div class="row-fluid">
          <span class="span1">
            <label class="control-label pull-left span3">Type&nbsp;</label>
         
          </span>
          <span class="span2">
            <label class="control-label pull-left span5"># Truck &nbsp;</label>
             <input pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" type="text" name="vehicle_count" id="vehicle_truck" onchange="add_truck1(this.value)" class="span3 ui-state-valid" value="<?php if(!empty($app_veh1)){ if(count($app_veh1)==0){ }else echo count($app_veh1); }else{ if(count($app_veh12)==0){ }else echo count($app_veh12); }?>" required="required" >
          </span>
     
          <span class="span2">
            <label class="control-label pull-left span5"># Tractor &nbsp;</label>
            <input required  pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2"  type="text"  name="vehicler_count" id="vehicle_tractor" onChange="add_tractor1(this.value)" class="span3" value="<?php if(!empty($app_veh2)){ if(count($app_veh2)==0){ }else echo count($app_veh2); }else{ if(count($app_veh22)==0){ }else echo count($app_veh22); }?>" required="required" >
          </span>
		            <span class="span4">
					 <label class="control-label pull-left span7">Are you mono hauling trailers? &nbsp;</label>
					  <select class="pull-left pull-left-1 width_option" onchange="Vehicle_hau(this.value);" name="Vehicle_hauling" required>
               <?php $Vehicle_hauling = isset($app_load[0]->Vehicle_hauling) ? $app_load[0]->Vehicle_hauling : '';?>
               <option value="" >Please select</option>
                <option value="No" <?php if($Vehicle_hauling  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($Vehicle_hauling  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                       
                    </select>
					</span>
                    <script>
					
					
					function Vehicle_hau(val){
						if(val=="Yes"){
							$('#trailers_show').show();
							}else{
							$('#trailers_show').hide();
							}
						
						
						}
					
					</script>
                    
					<span class="span3" style="display:none;" id="trailers_show">
					 <label class="control-label pull-left span7">How many trailers? &nbsp;</label>
					   <input class="textinput span3" type="text" placeholder="" onchange="vehicles_trail(this.value,1);" name="vehicles_trailers">
					</span>
        </div>
		

		<div class="well">
		<!--start truck-->
        <?php 
		$i=1;
		
	//$app_veh_truck =!empty($app_veh1)  ?   $app_veh1   : $app_veh12 ;
		if(!empty($app_veh1)){
		foreach($app_veh1 as $value){
		
		
		 ?>
         <input type="hidden" value="<?php echo  isset($value->vehicle_id) ? $value->vehicle_id : ''; ?>" name="vehicle_id[]"/>
		<div class="row_added_vehicle vehicle_schedule"><div class="div-1">     
               <div class="row-fluid btn-primary" style="padding: 5px 0px 5px 0px;">    
                         <span class="span4">      
                                   <label class="control-label control-label-2" style="font-weight:bold;font-size:15px; margin-left:10px;margin-top: 5px;">Truck #<?php echo $i;?></label>     
                                            </span>            
                                              <span class="span8" style="margin-left: 20px;">        
                                                      <a class="btn pull-right btn_sp" onclick="drivers_truck(<?php echo $i;?>,&quot;no&quot;);" id="drivers_button<?php echo $i;?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Drivers</a>       
                                                               <a class="btn pull-right btn_sp" onclick="cargo_truck(<?php echo $i;?>,&quot;no&quot;);" id="cargo_button<?php echo $i;?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Cargo</a>  
                                                                   <a class="btn pull-right btn_sp" onclick="pd_truck(<?php echo $i;?>,&quot;no&quot;);" id="pd_button<?php echo $i;?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Physical Damage</a> 
                                                                    <a class="btn pull-right btn-1 btn-2" onclick="liability_truck(<?php echo $i;?>,&quot;no&quot;);" id="liability_button<?php echo $i;?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Liability</a> 
                                                                          </span> 
                                                                                </div>          </div>
          <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="select_vehicle_year[]" class="select_new">
              <?php $select_vehicle_year= isset($value->select_vehicle_year) ? $value->select_vehicle_year : ''; ?>
                 <option value="1985"  <?php if($select_vehicle_year  == '1985') echo 'selected="selected"'; ?>>1985</option>
                 <option value="1986"  <?php if($select_vehicle_year  == '1986') echo 'selected="selected"'; ?>>1986</option>
                                                                <option value="1987"  <?php if($select_vehicle_year  == '1987') echo 'selected="selected"'; ?>>1987</option>
                                                                <option value="1988"  <?php if($select_vehicle_year  == '1988') echo 'selected="selected"'; ?>>1988</option>
                                                                <option value="1989"  <?php if($select_vehicle_year  == '1989') echo 'selected="selected"'; ?>>1989</option>
																<option value="1990"  <?php if($select_vehicle_year  == '1990') echo 'selected="selected"'; ?>>1990</option>
                                                                <option value="1991"  <?php if($select_vehicle_year  == '1991') echo 'selected="selected"'; ?>>1991</option>
                                                                <option value="1992"  <?php if($select_vehicle_year  == '1992') echo 'selected="selected"'; ?>>1992</option>
                                                                <option value="1993"  <?php if($select_vehicle_year  == '1993') echo 'selected="selected"'; ?>>1993</option>
                                                                <option value="1994"  <?php if($select_vehicle_year  == '1994') echo 'selected="selected"'; ?>>1994</option>
                                                                <option value="1995"  <?php if($select_vehicle_year  == '1995') echo 'selected="selected"'; ?>> 1995</option>
                                                                <option value="1996"  <?php if($select_vehicle_year  == '1996') echo 'selected="selected"'; ?>>1996</option>
                                                                <option value="1997"  <?php if($select_vehicle_year  == '1997') echo 'selected="selected"'; ?>>1997</option>
                                                                <option value="1998"  <?php if($select_vehicle_year  == '1998') echo 'selected="selected"'; ?>>1998</option>
                                                                <option value="1999"  <?php if($select_vehicle_year  == '1999') echo 'selected="selected"'; ?>>1999</option>
                                                                <option value="2000"  <?php if($select_vehicle_year  == '2000') echo 'selected="selected"'; ?>>2000</option>
                                                                <option value="2001"  <?php if($select_vehicle_year  == '2001') echo 'selected="selected"'; ?>>2001</option>
                                                                <option value="2002"  <?php if($select_vehicle_year  == '2002') echo 'selected="selected"'; ?>>2002</option>
                                                                <option value="2003"  <?php if($select_vehicle_year  == '2003') echo 'selected="selected"'; ?>>2003</option>
                                                                <option value="2004"  <?php if($select_vehicle_year  == '2004') echo 'selected="selected"'; ?>>2004</option>
                                                                <option value="2005"  <?php if($select_vehicle_year  == '2005') echo 'selected="selected"'; ?>>2005</option>
                                                                <option value="2006"  <?php if($select_vehicle_year  == '2006') echo 'selected="selected"'; ?>>2006</option>
                                                                <option value="2007"  <?php if($select_vehicle_year  == '2007') echo 'selected="selected"'; ?>>2007</option>
                                                                <option value="2008"  <?php if($select_vehicle_year  == '2008') echo 'selected="selected"'; ?>>2008</option>
                                                                <option value="2009"  <?php if($select_vehicle_year  == '2009') echo 'selected="selected"'; ?>>2009</option>
                                                                <option value="2010"  <?php if($select_vehicle_year  == '2010') echo 'selected="selected"'; ?>>2010</option>
                                                                <option value="2011"  <?php if($select_vehicle_year  == '2011') echo 'selected="selected"'; ?>>2011</option>
                                                                <option value="2012"  <?php if($select_vehicle_year  == '2012') echo 'selected="selected"'; ?>>2012</option>
                                                                <option value="2013"  <?php if($select_vehicle_year  == '2013') echo 'selected="selected"'; ?>>2013</option>
																<option value="2014"  <?php if($select_vehicle_year  == '2014') echo 'selected="selected"'; ?>>2014</option>
            </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="select_model[]" class="select_new">
                <?php $select_model= isset($value->select_model) ? $value->select_model : ''; ?>
                <option value="">Select</option>
				<option value="Freightliner" <?php if($select_model  == 'Freightliner') echo 'selected="selected"'; ?>>Freightliner</option>
                <option value="International" <?php if($select_model  == 'International') echo 'selected="selected"'; ?>>International</option>
                 <option value="Isuzu" <?php if($select_model  == 'Isuzu') echo 'selected="selected"'; ?>>Isuzu</option>
                 <option value="Kenworth" <?php if($select_model  == 'Kenworth') echo 'selected="selected"'; ?>>Kenworth</option>
                 <option value="Mack" <?php if($select_model  == 'Mack') echo 'selected="selected"'; ?>>Mack</option>
                 <option value="Peterbilt" <?php if($select_model  == 'Peterbilt') echo 'selected="selected"'; ?>>Peterbilt</option>
                 <option value="Sterling" <?php if($select_model  == 'Sterling') echo 'selected="selected"'; ?>>Sterling</option>
                 <option value="Western Star" <?php if($select_model  == 'Western Star') echo 'selected="selected"'; ?>>Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              
              <select name="select_GVW[]" class="select_new">
			   <?php $select_GVW= isset($value->select_GVW) ? $value->select_GVW : ''; ?>
                   <option value="0-10,000 pounds" <?php if($select_model  == '0-10,000 pounds') echo 'selected="selected"'; ?>>0-10,000 pounds</option>
                                                                <option value="10,001-26,000 pounds" <?php if($select_GVW  == '10,001-26,000 pounds') echo 'selected="selected"'; ?>>10,001-26,000 pounds</option>
                                                                <option value="26,001-40,000 pounds" <?php if($select_GVW  == '26,001-40,000 pounds') echo 'selected="selected"'; ?>>26,001-40,000 pounds</option>
                                                                <option value="40,001-80,000 pounds" <?php if($select_GVW  == '40,001-80,000 pounds') echo 'selected="selected"'; ?>>40,001-80,000 pounds</option>
                                                                <option value="Over 80,000 pounds" <?php if($select_GVW  == 'Over 80,000 pounds') echo 'selected="selected"'; ?>>Over 80,000 pounds</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->select_VIN) ? $value->select_VIN : ''; ?>" name="select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->select_Operation) ? $value->select_Operation : ''; ?>" name="select_Operation[]">
            </span>
          </div>
		 <div style="" id="c_damage_truck<?php echo $i; ?>"><div class="row-fluid">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="select_Radius[]" id="1select_Radius" class="select_new check_Radius">
               <?php $select_Radius= isset($value->select_Radius) ? $value->select_Radius : ''; ?>
			    <option value="">Please Select</option>
                 <option value="0-100 miles" <?php if($select_Radius  == '0-100 miles') echo 'selected="selected"'; ?>>0-100 miles</option>
                                                                <option value="101-500 miles" <?php if($select_Radius  == '101-500 miles') echo 'selected="selected"'; ?>>101-500 miles</option>           	
                                                                <option value="501+ miles" <?php if($select_Radius  == '501+ miles') echo 'selected="selected"'; ?>>501+ miles</option>
                                                              	<option value="other" <?php if($select_Radius  == 'other') echo 'selected="selected"'; ?>>Other</option>
              </select>
            </span>
			
            <span class="span2">
              <label class="control-label">States driving</label>
            									 <?php
									$attr = "class='span12 ui-state-valid state_driv' id='state_driving'" ;
									$broker_state = isset($value->state_driving) ? $value->state_driving : '';
									
									?>
									<?php echo get_state_dropdown('state_driving[]', $broker_state, $attr); ?>
            
            </span>
			
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12 check_Cities" type="text" id="1select_Cities" value="<?php echo  isset($value->select_Cities) ? $value->select_Cities : ''; ?>" placeholder="" name="select_Cities[]">
            </span>
		 <span class="span5">	
         		  <label class="control-label">Applicants business</label>	
                  		  <select name="select_applicant[]" class="select_new check_App ui-state-valid" id="select_App<?php echo $i;?>" onchange="business_app(<?php echo $i;?>,this.value);">	
                          
                                    <script>
																 var u=1;
																 $(document).ready(function(){
																	
																     business_app(u,$('#select_App'+u).val());
																	 u++;
																 });
																 </script>	
                          		 <?php $select_applicant= isset($value->select_applicant) ? $value->select_applicant : ''; ?> 
                                  <option value="">Please Select</option>		
                                  	  <option value="Tow truck driver" <?php if($select_applicant  == 'Tow truck driver') echo 'selected="selected"'; ?>>Tow truck driver</option>
                                  			  <option value="Other" <?php if($select_applicant  == 'Other') echo 'selected="selected"'; ?>> Other </option>	
                                              		  </select>     
                                                        
                                                             <input class="textinput span5 check_app_other" type="text" placeholder="Please specify" style="display:none;" value="<?php isset($value->select_applicant_driver) ? $value->select_applicant_driver : '';?>" id="select_app_other<?php echo $i;  ?>" name="select_applicant_driver[]">   
                                                                      </span>
                                                                      </div>
                                                                        <div class="row-fluid" id="checkbox_id1" style="display:none;">   
                                                                                 <span class="span4">        
                                                                                       <label class="checkbox">  
                                                                       <input type="checkbox" value="Yes" onclick="select_ch_rad1();"  class="checkbox_radius" id="select_ch_radius1" name="select_ch_radius[]">      
                                                                                 <span>Same Radius of Operation for all vehicles</span>    
                                                                                           </label>    
                                                                                      </span>  
                                                                                       <span class="span3">    
                                                                                       
                                                                                           <label class="checkbox">    
                                                                                                      
                   <input type="checkbox" value="Yes" onclick="select_ch_cit1();"  class="checkbox_cities" id="select_ch_cities1" name="select_ch_cities[]">     
                              <span>Same Cities for all vehicles</span>           
                                 </label>   
                                          </span>      
                                                <span class="span5">    
                                                          <label class="checkbox">            
                                                              <input type="checkbox" value="Yes" onclick="select_ch_app1();" class="checkbox_applicant" id="select_ch_applicant1" name="select_ch_applicant[]">    
                                                                          <span>Same Applicants Business for all vehicles</span>         
                                                                               </label>       
                                                                                </span>   
                                                                                  </div>
          <div class="row-fluid">
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" value="<?php echo  isset($value->select_special) ? $value->select_special : '';?>" type="text" placeholder="" name="select_special[]">
            </span>
            <span class="span2">
              <label class="control-label">Number of Axels</label>
             
              <select name="select_Axels[]" class="select_new">
			   <?php  $select_Axels= isset($value->select_Axels) ? $value->select_Axels : '';?>
                 <option value="">Please Select</option>
				 <option value="1 Axel" <?php if($select_Axels  == '1 Axel') echo 'selected="selected"'; ?> >1 Axel</option>
				 <option value="2 Axel" <?php if($select_Axels  == '2 Axel') echo 'selected="selected"'; ?> >2 Axel</option>
				 <option value="3 Axel" <?php if($select_Axels  == '3 Axel') echo 'selected="selected"'; ?> >3 Axel</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">Number of drivers:&nbsp;</label>
              <input class="span4" pattern="[0-9]+" type="text" value="<?php echo  isset($value->select_num_drivers) ? $value->select_num_drivers : '';?>" placeholder="" onchange="add_driver(this.value,<?php echo $i;?>)" name="select_num_drivers[]" maxlength="2">
            </span>
            <span class="span3">
              <label class="control-label"></label>
            </span>
          </div>
		 <div class="row-fluid">   
                  <span class="span12">   
                  
                             <label class="control-label pull-left">Do you pull equipment with this vehicle? &nbsp;</label>         
                                  <select name="select_equipment[]" class="select_new span1" id="select_equipment" onchange="equipment_pull(<?php echo $i; ?>,this.value);">       
                                                              <?php echo  $select_equipment = isset($value->select_equipment) ? $value->select_equipment : '';?>
                 <option value="Yes" <?php if($select_equipment  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>                                                
                <option value="No" <?php if($select_equipment  == 'No') echo 'selected="selected"'; ?> >No</option>
                	
                                                             			   </select>
                                                                                   <script>
																 var r=1;
																 $(document).ready(function(){
																	
																     equipment_pull(r,$('#select_equipment').val());
																	 r++;
																 });
																 </script>		
                                   		  <select id="put_on_truck<?php echo  $i; ?>" name="put_on_truck[]" style="display:none;" class="span2">  
                                          <?php $put_on_truck=isset($value->put_on_truck) ? $value->put_on_truck : '';?>
                                          <option value="None" <?php if($put_on_truck  == 'None') echo 'selected="selected"'; ?>>None</option>
                                           <option value="Dolly" <?php if($put_on_truck  == "Dolly") echo 'selected="selected"'; ?>>Dolly</option>                        
                                          <option value="Trailer" <?php if($put_on_truck  == "Trailer") echo 'selected="selected"'; ?>>Trailer</option>                        
                                          <option value="MotorHome" <?php if($put_on_truck  == "MotorHome") echo 'selected="selected"'; ?>>MotorHome</option>
                                            
                                                 <!--      	<option value="None" <?php if($put_on_truck  == 'None') echo 'selected="selected"'; ?>>None</option>		
                                   						<option value="Single" <?php if($put_on_truck  == 'Single') echo 'selected="selected"'; ?>>Single</option>		
                                      						<option value="Double" <?php if($put_on_truck  == 'Double') echo 'selected="selected"'; ?>>Double</option>		
                                                           <option value="Triple" <?php if($put_on_truck  == 'Triple') echo 'selected="selected"'; ?>>Triple</option>     -->
                                                           </select><div class="row-fluid">    
                                                             <span class="span6">     
                                                                  <label class="control-label pull-left">Do you need Physical Damage Coverage? &nbsp;</label> 
                                                               <select name="pd_damage[]" onchange="pd_damage_truck(<?php echo  $i; ?>);" id="pd_dam_truck<?php echo  $i; ?>" class="span2">
                                                                 <script>
																 var k=1;
																 $(document).ready(function(){
																	
																     pd_damage_truck(k);
																	 k++;
																 });
																 </script>	
                                                               
                                                               		   <?php $pd_damage = isset($value->pd_damage) ? $value->pd_damage : '';?>
                <option value="No" <?php if($pd_damage  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($pd_damage  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>  
                                                                                       </select>     
                                                                                      </span>      
                                                                               <span class="span6">    
                                                                      <label class="control-label pull-left">Do you need Cargo Coverage? &nbsp;</label>    
                                                      <select name="cd_damage[]" class="select_new span2" onchange="cd_damage_truck(<?php echo  $i; ?>);" id="cd_dam_truck<?php echo  $i; ?>">  
                                                      
                                                  
                                                            <?php $cd_damage = isset($value->cd_damage) ? $value->cd_damage : '';?>
                <option value="No" <?php if($cd_damage  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($cd_damage  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>   
                                                                     
                                                                                </select>    
                                                                                        <script>
																 var l=1;
																 $(document).ready(function(){
																	 
																     cd_damage_truck(l);
																	l++;
																 });
																 </script>	     
                                                                                   </span>   
                                                                             </div></span></div></div>
                                                              <span id="add_pd_truck_div<?php echo $i; ?>" style="">
                                                              <h5 class="heading sh sh-2">Physical Damage</h5>
                                                              <a class="btn btn-mini" style="" onclick="pd_add(<?php echo  $i; ?>);" id="pd_add_button<?php echo  $i; ?>"> 
                                                              <i class="icon icon-plus"></i> Add</a>	
															  
															 
                                                              		 <div class="row-fluid" id=<?php echo "pd_add_val".$i?>>  
                                                                      <?php   $j=1;foreach( $app_veh_pd as $value1){  
																	  if($value1->vehicle_id == $value->vehicle_id){
																	 
																	   ?>
                                                           <input type="hidden" value="<?php echo  isset($value1->v_id) ? $value1->v_id : ''; ?>" name="v_id<?php echo  $i; ?>[]"/>
                                                            <input type="hidden" value="<?php echo  isset($value->vehicle_id) ? $value->vehicle_id : ''; ?>" name="veh_id<?php echo  $i; ?>[]"/>
                                                           
                                                                      <div class="row-fluid" id=<?php echo "row_add_pd".$i?>>
                                                                                <span class="span1">        
                                                                                    <label class="control-label">Rate</label>  
                                                                                    
                                                                                     <?php $pd_rate = isset($value1->pd_rate) ? $value1->pd_rate : '';?>
                                                                                                    <select name="pd_rate<?php echo $i; ?>[]" class="select_new_per">      
                                   <option value="">select</option>                                                        
                <option value="5%" <?php if($pd_rate  == '5%') echo 'selected="selected"'; ?> >5%</option>
                 <option value="10%" <?php if($pd_rate  == '10%') echo 'selected="selected"'; ?>>10%</option>  
                                                                                                                              </select>      
                                                                                                                                    </span>      
                                <span class="span3">
                                    <label class="control-label">Loss payee and full address</label>   
                                        <select name="pd_loss_payee<?php echo  $i; ?>[]" class="span4" id="pd_loss_payee<?php echo  $j; ?>"  onchange="loss_payee(<?php echo  $j; ?>)">	
                                        <?php $pd_loss_payee = isset($value1->pd_loss_payee) ? $value1->pd_loss_payee : '';?>
                                        	 <option value="No" <?php if($pd_loss_payee  == 'No') echo 'selected="selected"'; ?> >No</option>
                                          <option value="Yes" <?php if($pd_loss_payee  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>   
                                                                  </select>     
                                                                         </span>		
                                                                         
                                                                           <script>
												  var vr=1
												  $(document).ready(function(){
													 
													   loss_payee(vr);
													  vr++;
													  });
												  
											
												  </script>
                                                                         
                                                                         	     <span class="span3" id="coverage<?php echo $j;?>">  
                                                 <label class="control-label">Additional optional coverage</label>     
                                                          <select name="pd_coverage<?php echo  $i; ?>[]" class="span8 chzn-select-width" multiple onchange="pd_coverage1(<?php echo  $j; ?>);" id="pd_coverage1<?php echo  $j; ?>">	
                                                           <?php   $pd_coverage = isset($value1->pd_coverage) ? explode(',',$value1->pd_coverage) : '';?>
                                                             <?php print_r($pd_coverage); if(!empty($pd_coverage)) {  ?>
                                                        <option value="Debris removal"  <?php if(in_array('Debris removal',$pd_coverage)) echo 'selected="selected"'; ?> >Debris removal</option>
                                               <option value="Towing & storage" <?php if(in_array('Towing & storage',$pd_coverage )  ) echo 'selected="selected"'; ?>>Towing &amp; storage</option>  
                                               <option value="Tarpaulin coverage" <?php if(in_array('Tarpaulin coverage',$pd_coverage )  ) echo 'selected="selected"'; ?>>Tarpaulin coverage</option>  
                                                <option value="Other" <?php if(in_array('Other',$pd_coverage ) ) echo 'selected="selected"'; ?>>Other</option> 
                                                 <?php } else { ?>
                                                    <option value="Debris removal"  <?php if($pd_coverage  == 'Debris removal') echo 'selected="selected"'; ?> >Debris removal</option>
                                               <option value="Towing & storage" <?php if($pd_coverage  == 'Towing & storage') echo 'selected="selected"'; ?>>Towing &amp; storage</option>  
                                               <option value="Tarpaulin coverage" <?php if($pd_coverage  == 'Tarpaulin coverage') echo 'selected="selected"'; ?>>Tarpaulin coverage</option>  
                                                <option value="Other" <?php if($pd_coverage  == 'Other') echo 'selected="selected"'; ?>>Other</option>
                                            <?php      } ?> 
                                                  </select> 
                                                  
                                                  <script>
												  var y=1;
												   var val1=[];
												  $(document).ready(function(){
													 
													  val1[y]=$("#pd_coverage1"+y).val();
													// alert(val1);
													 //pd_coverage1(y,$('#pd_coverage1'+y).val());
													  y++;
													  });
												  
											
												  </script>
                                              <?php     if(in_array('Other',$pd_coverage ) )  { ?>
                                                <input class="textinput  span4" type="text" id="Specify_o<?php echo  $j; ?>" style="" placeholder="Specify" value="<?php echo  isset($value1->pd_coverage_specify) ? $value1->pd_coverage_specify : '';?>" name="pd_coverage_specify<?php echo  $i; ?>[]">     
                                                    
											<?php  }		?>   </span>		
                                 	<span class="span2">   
                                      <label class="control-label">Premium</label>     
                                            <input class="textinput span9" type="text" value="<?php echo  isset($value1->pd_premium) ? $value1->pd_premium : '';?>" placeholder="" name="pd_premium<?php echo  $i; ?>[]">    
                                                     </span>      
                                  <span class="span2">       
                                                 <label class="control-label">Deductible</label>        
                                                                                           <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->pd_deductible) ? $value1->pd_deductible : '';?>" name="pd_deductible<?php echo  $i; ?>[]">     
                                                                                                              </span>   
                                                                                                              <span class="span1"> <label class="control-label">Remove</label><a class="btn btn-mini btn-1   " onclick="remove_pd_row1(<?php echo  $i; ?>);" id="remove_pd_row"><i class="icon icon-remove "></i></a></span>
                                                                                          
                                          <div class="row-fluid" style="" id="pd_amount<?php echo $i; ?>">    
                  <span class="span7">    
                            <label class="control-label"></label>  
                                        <table class="table">        
          <thead>     
        <tr>     
        <th>
       </th>  
      <th>Amount or Limit</th> 
     <th>Deductible</th>
      <th>Premium</th>
      </tr>  
        </thead>   
          <tbody id="pd_amounts<?php echo $i; ?>"> 
          <?php $pd_amount_limit=  isset($value1->pd_amount_limit) ? explode(',',$value1->pd_amount_limit) : '';?>
          <?php $pd_deductible_limit= isset($value1->pd_deductible_limit) ? explode(',',$value1->pd_deductible_limit) : '';?>
          <?php $pd_premium_limit=  isset($value1->pd_premium_limit) ? explode(',',$value1->pd_premium_limit) : '';?>
          <?php  for ($t=0; $t<count($pd_coverage); $t++)  { ?>
             <tr id="pd_remove<?php echo $t;?>">                    
             <td ><?php echo $pd_coverage[$t]?></td>                    
             <td>                     
             <input class="textinput input-mini" type="text" value="<?php echo $pd_amount_limit[$t] ?>" placeholder="" name="pd_amount_limit<?php echo  $i; ?>[]">                    
             </td>                   
              <td>                     
               <input class="textinput input-mini" type="text" value="<?php echo $pd_deductible_limit[$t] ?>" placeholder="" name="pd_deductible_limit<?php echo  $i; ?>[]">                    
               </td>                   
                <td>                      
                <input class="textinput input-mini" type="text" value="<?php echo $pd_premium_limit[$t] ?>" placeholder="" name="pd_premium_limit<?php echo  $i; ?>[]">                    
                </td>                  </tr>           
                
                <?php } ?>     </tbody>              </table>            
                </span>           
                 <span class="span5">             
                  <label class="control-label"></label>           
                   </span>         
                    </div>     
         </div>
                                                                                                          
                                                                                                     
                                                                                                       
                                                                                                              <?php $j++; } } ?>
                                                                                                          
          
                                                                          </div>                     
		        <label class="control-label control-label-2">Loss Payee Address(es)</label>
          <div class="row-fluid">
            <span class="span3">
              <label class="control-label">Entity name</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->pd_loss_name) ? $value->pd_loss_name : '';?>" name="pd_loss_name[]">
            </span>
            <span class="span3">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->pd_loss_address) ? $value->pd_loss_address : '';?>" name="pd_loss_address[]">
            </span>
            <span class="span1">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder=""  value="<?php echo  isset($value->pd_loss_city) ? $value->pd_loss_city : '';?>" name="pd_loss_city[]">
            </span>
            <span class="span1">
              <label class="control-label">State</label>
              						<?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = isset($value->pd_loss_state) ? $value->pd_loss_state : '';;
									
									?>
									<?php echo get_state_dropdown('pd_loss_state[]', $broker_state, $attr); ?>
            
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value->pd_loss_zip) ? $value->pd_loss_zip : '';?>" name="pd_loss_zip[]" pattern="[0-9]+" maxlength="9">
            </span>
            <span class="span3">
              <label class="control-label">Remarks</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->pd_loss_remarks) ? $value->pd_loss_remarks : '';?>" name="pd_loss_remarks[]">
            </span>
          </div>
        
		 
	</span>
					
					
					<span id="add_cd_truck_div<?php echo $i; ?>" style=""><h5 class="heading sh sh-3">Cargo</h5><div class="row-fluid">            
                    <span class="span3" style="" id="cargo_damages<?php $i; ?>">             
                     <label class="control-label">Average value per load &nbsp;</label>              
                     <input class="textinput span6 price_format" type="text" placeholder="$" value="<?php echo  isset($value->cargo_avg_val) ? $value->cargo_avg_val : '';?>" name="cargo_avg_val[]">             
                      <input class="textinput span6" type="text" placeholder="%" value="<?php echo  isset($value->cargo_avg_per) ? $value->cargo_avg_per : '';?>" name="cargo_avg_per[]">            
                      </span>            
                      <span class="span3" style="margin-left:10px;">              
                      <label class="control-label">Maximum values per load &nbsp;</label>              
                      <input class="textinput span6 price_format" type="text" placeholder="$" value="<?php echo  isset($value->cargo_max_val) ? $value->cargo_max_val : '';?>" name="cargo_max_val[]">              
                      <input class="textinput span6" type="text" placeholder="%" value="<?php echo  isset($value->cargo_max_per) ? $value->cargo_max_per : '';?>" name="cargo_max_per[]">            
                      </span>         
                         <span class="span2">     
                             <label class="control-label">% Factor</label>             
                       <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->cargo_factor) ? $value->cargo_factor : '';?>" name="cargo_factor[]">     
                              </span>           
                        <span class="span2">              <label class="control-label">Premium</label>            
                          <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->cargo_prem) ? $value->cargo_prem : '';?>" name="cargo_prem[]">    
                                  </span>            
                          <span class="span2">             
                          <label class="control-label">Deductible</label>              
                          <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->cargo_deduct) ? $value->cargo_deduct : '';?>" name="cargo_deduct[]">           
                           </span>          </div>		  
                             <div class="row-fluid">    
                                   <span class="span6">             
                            <label class="control-label">Additional optional coverage</label>             
                             <select name="coverage_name[]" class="select-15 chzn-select-width" multiple onchange="coverage_name1(<?php echo $i; ?>)"  id="coverage_name1<?php echo $i; ?>">	
                             <?php $coverage_name = isset($value->coverage_name) ? explode(',',$value->coverage_name) : ''; ?>		 
                              <option value="">Please Select</option>               
                               <option <?php if(in_array('Earned freight clause',$coverage_name)) echo 'selected="selected"'; ?> value="Earned freight clause">Earned freight clause</option>               
                                <option <?php if(in_array('Refrigeration breakdown deductible',$coverage_name)) echo 'selected="selected"'; ?> value="Refrigeration breakdown deductible">Refrigeration breakdown deductible</option>               
                                 <option <?php if(in_array('Debris removal',$coverage_name)) echo 'selected="selected"'; ?> value="Debris removal">Debris removal</option>               
                                  <option <?php if(in_array('Excess cargo',$coverage_name)) echo 'selected="selected"'; ?> value="Excess cargo">Excess cargo</option>             
                                  <option <?php if(in_array('Other',$coverage_name)) echo 'selected="selected"'; ?> value="Other">Other</option> 
                                   </select>            
                                   </span>      
                                
                                                <input class="textinput  span4" type="text" id="Specify_o<?php echo  $j; ?>" style="" placeholder="Specify" value="" name="pd_coverage_specify<?php echo  $i; ?>[]">     
                                                    
											
                        <?php     if(in_array('Other',$coverage_name ) )  { ?>                     
                         <span class="span6" id="t_cargo_Other2" style="margin-top: 19px;">	
                         		<label class="control-label"></label>
                                              <input class="textinput span3 ui-state-valid" type="text" value="<?php echo  isset($value->cargo_Other) ? $value->cargo_Other : '';?>" placeholder="" name="t_cargo_Other[]">           
                                               </span>                   
                                   <?php  }		?>  
                                         <script>
												  var x=1
												  $(document).ready(function(){
													 
													  // coverage_name1(x,$('#coverage_name1'+x).val());
													  x++;
													  });
												  
											
												  </script>   
                                     
                                    <span class="span6">             
                                     <label class="control-label"></label>     
                                         
                             </span>          </div>
             <?php  $cargo_amount_limit=  isset($value->cargo_amount_limit) ? explode(',',$value->cargo_amount_limit) : '';
			 ?>
          <?php $cargo_amount_deduct= isset($value->cargo_amount_deduct) ? explode(',',$value->cargo_amount_deduct) : '';?>
          <?php $cargo_amount_premium=  isset($value->cargo_amount_premium) ? explode(',',$value->cargo_amount_premium) : '';?>
          <?php if(!empty($coverage_name)){ ?>
		  <span id="cargo_dis<?php echo $i; ?>" >
           <div class="row-fluid" style="" id="cargo_amount<?php echo $i; ?>">    
                  <span class="span8">              
                  <table class="table">      
                            <thead>                  
                            <tr>             
                            
                                   <th></th>                    
                                   <th>Amount or Limit</th>                   
                                    <th>Deductible</th>                   
                                     <th>Premium</th>                 
                                     </tr>                
                                     </thead>               
                                      <tbody>  
                     <?php  for ($t=0; $t<count($coverage_name); $t++)  { ?>               
                                      <tr id="">                    
                                      <td><?php echo $coverage_name[$t]; ?></td>                    
                                      <td>                     
                                       <input class="textinput input-mini" type="text" placeholder="" value="<?php echo $cargo_amount_limit[$t] ;?>" name="cargo_amount_limit[]">                   
                                        </td>                    
                                        <td>                     
                                         <input class="textinput input-mini" type="text" placeholder="" value="<?php echo $cargo_amount_deduct[$t] ;?>" name="cargo_amount_deduct[]">                   
                                          </td>                  
                                            <td>                     
                                             <input class="textinput input-mini" type="text" placeholder="" value="<?php echo $cargo_amount_premium[$t] ;?>" name="cargo_amount_premium[]">          
                                              </td> 
                                              </tr>      
                                                                                  
                                             <?php } ?>                </tbody>             
                                                              </table>           
                                                               </span>    
                                                               
                                                               <?php } ?>       
                                                                <span class="span4">             
                                                                 <label class="control-label"></label>           
                                                                  </span>         
                                                                   </div></span>
																   </span>
																   
	 <span id="add_drivers_truck_div<?php echo $i;?>" style="display:block">
      <h5 class="heading sh sh-5">Drivers Schedule</h5>
          
          <table class="table">
            <thead>
              <tr>
                <th>First name</th>
                <th>Middle name</th>
                <th>Last name</th>
                <th>Driver License
                  <br>Number</th>
                <th>License class</th>
                <th>Date hired</th>
                <th>How many years
                  <br>Class A license</th>
                <th>License issue state</th>
                <th>Remarks</th>
                <th>Surcharge</th>
              </tr>
            </thead>
            <tbody id="<?php echo $i; ?>add_drivered_row1">
    
    
            <?php 
//if(!empty($app_driver)){ $app_driver ; }else{ $app_driver=$app_driver1; }
//print_r($app_driver);
if (!empty($app_driver)) { ?>
                <?php   foreach ($app_driver as $drivers) {
					  if ($drivers->vehicle_id == $value->vehicle_id ) {
					
					 ?>
        <input type="hidden" value="<?php echo  isset($value->vehicle_id) ? $value->vehicle_id : ''; ?>" name="<?php echo $i;?>dri_id[]"/>
      <input type="hidden" value="<?php echo (isset($drivers->driver_id) ? $drivers->driver_id : ''); ?>" name="<?php echo $i;?>driver_id[]"  />
              <tr>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_fname)){ echo (isset($drivers->driver_fname) ? $drivers->driver_fname : ''); }else{ echo (isset($drivers->driver_name) ? $drivers->driver_name : ''); } ?>" name="<?php echo $i;?>driver_fname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder=""  value="<?php if(!empty($drivers->driver_mname)){ echo (isset($drivers->driver_mname) ? $drivers->driver_mname : ''); }else{ echo (isset($drivers->driver_middle_name) ? $drivers->driver_middle_name : ''); } ?>" name="<?php echo $i;?>driver_mname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_lname)){ echo (isset($drivers->driver_lname) ? $drivers->driver_lname : ''); }else{ echo (isset($drivers->driver_last_name) ? $drivers->driver_last_name : ''); } ?>" name="<?php echo $i;?>driver_lname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_license_num)){ echo (isset($drivers->driver_license_num) ? $drivers->driver_license_num : ''); }else{ echo (isset($drivers->driver_licence_no) ? $drivers->driver_licence_no : ''); } ?>" name="<?php echo $i;?>driver_license_num[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_license_class)){ echo (isset($drivers->driver_license_class) ? $drivers->driver_license_class : ''); }else{ echo (isset($drivers->driver_license_class) ? $drivers->driver_license_class : ''); } ?>" name="<?php echo $i;?>driver_license_class[]">
                </td>
                <td>
                  <input class="textinput input-mini datepick" type="text" value="<?php if(!empty($drivers->driver_date_hired)){ echo (isset($drivers->driver_date_hired) ? $drivers->driver_date_hired : ''); }else{ echo (isset($drivers->driver_license_issue_date) ? date('m/d/Y', strtotime($drivers->driver_license_issue_date)) : ''); } ?>" required="required" placeholder="" name="<?php echo $i;?>driver_date_hired[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" value="<?php if(!empty($drivers->driver_class_A)){  echo (isset($drivers->driver_class_A) ? $drivers->driver_class_A : ''); }else{ echo (isset($drivers->driver_class_a_years) ? $drivers->driver_class_a_years : ''); } ?>" placeholder="" name="<?php echo $i;?>driver_class_A[]">
                </td>
                <td>
                   <?php
									$attr = "class='span12 width_td ui-state-valid'";
									$broker_state =   isset($drivers->driver_state) ? $drivers->driver_state : $drivers->driver_license_issue_state;
									$driver_state = $i.'driver_state[]'
									?>
									<?php echo get_state_dropdown( $driver_state, $broker_state, $attr); ?>
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_remarks)){ echo  (isset($drivers->driver_remarks) ? $drivers->driver_remarks : ''); } else{ isset($drivers->driver_remarks) ? $drivers->driver_remarks : ''; } ?>" name="<?php echo $i;?>driver_remarks[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_surcharge)){ echo (isset($drivers->driver_surcharge) ? $drivers->driver_surcharge : ''); } else{ isset($drivers->driver_surcharge) ? $drivers->driver_surcharge : ''; } ?>" name="<?php echo $i;?>driver_surcharge[]">
                </td>
              </tr>
           <?php } } } ?>
            </tbody>
           </table>
		  </span></div>
          
          <?php $i++; } }else { 
		  $i=1;
		  foreach( $app_veh12 as $value){
		  ?>
          
     <!--    <input type="hidden" value="<?php echo  isset($value->vehicle_id) ? $value->vehicle_id : ''; ?>" name="vehicle_id[]"/>-->
		<div class="row_added_vehicle vehicle_schedule"><div class="div-1">     
               <div class="row-fluid btn-primary" style="padding: 5px 0px 5px 0px;">    
                         <span class="span4">      
                                   <label class="control-label control-label-2" style="font-weight:bold;font-size:15px; margin-left:10px;margin-top: 5px;">Truck #<?php echo $i;?></label>     
                                            </span>            
                                              <span class="span8" style="margin-left: 20px;">        
                                                      <a class="btn pull-right btn_sp" onclick="drivers_truck(<?php echo $i;?>,&quot;no&quot;);" id="drivers_button<?php echo $i;?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Drivers</a>       
                                                               <a class="btn pull-right btn_sp" onclick="cargo_truck(<?php echo $i;?>,&quot;no&quot;);" id="cargo_button<?php echo $i;?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Cargo</a>  
                                                                   <a class="btn pull-right btn_sp" onclick="pd_truck(<?php echo $i;?>,&quot;no&quot;);" id="pd_button<?php echo $i;?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Physical Damage</a> 
                                                                    <a class="btn pull-right btn-1 btn-2" onclick="liability_truck(<?php echo $i;?>,&quot;no&quot;);" id="liability_button<?php echo $i;?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Liability</a> 
                                                                          </span> 
                                                                                </div>          </div>
          <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="select_vehicle_year[]" class="select_new">
              <?php $select_vehicle_year= isset($value->vehicle_year) ? $value->vehicle_year : ''; ?>
                 <option value="1985"  <?php if($select_vehicle_year  == '1985') echo 'selected="selected"'; ?>>1985</option>
                 <option value="1986"  <?php if($select_vehicle_year  == '1986') echo 'selected="selected"'; ?>>1986</option>
                                                                <option value="1987"  <?php if($select_vehicle_year  == '1987') echo 'selected="selected"'; ?>>1987</option>
                                                                <option value="1988"  <?php if($select_vehicle_year  == '1988') echo 'selected="selected"'; ?>>1988</option>
                                                                <option value="1989"  <?php if($select_vehicle_year  == '1989') echo 'selected="selected"'; ?>>1989</option>
																<option value="1990"  <?php if($select_vehicle_year  == '1990') echo 'selected="selected"'; ?>>1990</option>
                                                                <option value="1991"  <?php if($select_vehicle_year  == '1991') echo 'selected="selected"'; ?>>1991</option>
                                                                <option value="1992"  <?php if($select_vehicle_year  == '1992') echo 'selected="selected"'; ?>>1992</option>
                                                                <option value="1993"  <?php if($select_vehicle_year  == '1993') echo 'selected="selected"'; ?>>1993</option>
                                                                <option value="1994"  <?php if($select_vehicle_year  == '1994') echo 'selected="selected"'; ?>>1994</option>
                                                                <option value="1995"  <?php if($select_vehicle_year  == '1995') echo 'selected="selected"'; ?>> 1995</option>
                                                                <option value="1996"  <?php if($select_vehicle_year  == '1996') echo 'selected="selected"'; ?>>1996</option>
                                                                <option value="1997"  <?php if($select_vehicle_year  == '1997') echo 'selected="selected"'; ?>>1997</option>
                                                                <option value="1998"  <?php if($select_vehicle_year  == '1998') echo 'selected="selected"'; ?>>1998</option>
                                                                <option value="1999"  <?php if($select_vehicle_year  == '1999') echo 'selected="selected"'; ?>>1999</option>
                                                                <option value="2000"  <?php if($select_vehicle_year  == '2000') echo 'selected="selected"'; ?>>2000</option>
                                                                <option value="2001"  <?php if($select_vehicle_year  == '2001') echo 'selected="selected"'; ?>>2001</option>
                                                                <option value="2002"  <?php if($select_vehicle_year  == '2002') echo 'selected="selected"'; ?>>2002</option>
                                                                <option value="2003"  <?php if($select_vehicle_year  == '2003') echo 'selected="selected"'; ?>>2003</option>
                                                                <option value="2004"  <?php if($select_vehicle_year  == '2004') echo 'selected="selected"'; ?>>2004</option>
                                                                <option value="2005"  <?php if($select_vehicle_year  == '2005') echo 'selected="selected"'; ?>>2005</option>
                                                                <option value="2006"  <?php if($select_vehicle_year  == '2006') echo 'selected="selected"'; ?>>2006</option>
                                                                <option value="2007"  <?php if($select_vehicle_year  == '2007') echo 'selected="selected"'; ?>>2007</option>
                                                                <option value="2008"  <?php if($select_vehicle_year  == '2008') echo 'selected="selected"'; ?>>2008</option>
                                                                <option value="2009"  <?php if($select_vehicle_year  == '2009') echo 'selected="selected"'; ?>>2009</option>
                                                                <option value="2010"  <?php if($select_vehicle_year  == '2010') echo 'selected="selected"'; ?>>2010</option>
                                                                <option value="2011"  <?php if($select_vehicle_year  == '2011') echo 'selected="selected"'; ?>>2011</option>
                                                                <option value="2012"  <?php if($select_vehicle_year  == '2012') echo 'selected="selected"'; ?>>2012</option>
                                                                <option value="2013"  <?php if($select_vehicle_year  == '2013') echo 'selected="selected"'; ?>>2013</option>
																<option value="2014"  <?php if($select_vehicle_year  == '2014') echo 'selected="selected"'; ?>>2014</option>
            </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="select_model[]" class="select_new">
                <?php $select_model= isset($value->make_model) ? $value->make_model : ''; ?>
                <option value="">Select</option>
				<option value="Freightliner" <?php if($select_model  == 'Freightliner') echo 'selected="selected"'; ?>>Freightliner</option>
                <option value="International" <?php if($select_model  == 'International') echo 'selected="selected"'; ?>>International</option>
                 <option value="Isuzu" <?php if($select_model  == 'Isuzu') echo 'selected="selected"'; ?>>Isuzu</option>
                 <option value="Kenworth" <?php if($select_model  == 'Kenworth') echo 'selected="selected"'; ?>>Kenworth</option>
                 <option value="Mack" <?php if($select_model  == 'Mack') echo 'selected="selected"'; ?>>Mack</option>
                 <option value="Peterbilt" <?php if($select_model  == 'Peterbilt') echo 'selected="selected"'; ?>>Peterbilt</option>
                 <option value="Sterling" <?php if($select_model  == 'Sterling') echo 'selected="selected"'; ?>>Sterling</option>
                 <option value="Western Star" <?php if($select_model  == 'Western Star') echo 'selected="selected"'; ?>>Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              
              <select name="select_GVW[]" class="select_new">
			   <?php $select_GVW= isset($value->gvw) ? $value->gvw : ''; ?>
                   <option value="0-10,000 pounds" <?php if($select_model  == '0-10,000 pounds') echo 'selected="selected"'; ?>>0-10,000 pounds</option>
                                                                <option value="10,001-26,000 pounds" <?php if($select_GVW  == '10,001-26,000 pounds') echo 'selected="selected"'; ?>>10,001-26,000 pounds</option>
                                                                <option value="26,001-40,000 pounds" <?php if($select_GVW  == '26,001-40,000 pounds') echo 'selected="selected"'; ?>>26,001-40,000 pounds</option>
                                                                <option value="40,001-80,000 pounds" <?php if($select_GVW  == '40,001-80,000 pounds') echo 'selected="selected"'; ?>>40,001-80,000 pounds</option>
                                                                <option value="Over 80,000 pounds" <?php if($select_GVW  == 'Over 80,000 pounds') echo 'selected="selected"'; ?>>Over 80,000 pounds</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->vin) ? $value->vin : ''; ?>" name="select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->select_Operation) ? $value->select_Operation : ''; ?>" name="select_Operation[]">
            </span>
          </div>
		 <div style="" id="c_damage_truck<?php echo $i; ?>"><div class="row-fluid">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="select_Radius[]" id="1select_Radius" class="select_new check_Radius">
               <?php $select_Radius= isset($value->radius_of_operation) ? $value->radius_of_operation : ''; ?>
			    <option value="">Please Select</option>
                 <option value="0-100 miles" <?php if($select_Radius  == '0-100 miles') echo 'selected="selected"'; ?>>0-100 miles</option>
                                                                <option value="101-500 miles" <?php if($select_Radius  == '101-500 miles') echo 'selected="selected"'; ?>>101-500 miles</option>           	
                                                                <option value="501+ miles" <?php if($select_Radius  == '501+ miles') echo 'selected="selected"'; ?>>501+ miles</option>
                                                              	<option value="other" <?php if($select_Radius  == 'other') echo 'selected="selected"'; ?>>Other</option>
              </select>
            </span>
			
            <span class="span2">
              <label class="control-label">States driving</label>
            									 <?php
									$attr = "class='span12 ui-state-valid state_driv' id='state_driving'" ;
									$broker_state = isset($value->state_driving) ? $value->state_driving : '';
									
									?>
									<?php echo get_state_dropdown('state_driving[]', $broker_state, $attr); ?>
            
            </span>
			
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12 check_Cities" type="text" id="1select_Cities" value="<?php echo  isset($value->select_Cities) ? $value->select_Cities : ''; ?>" placeholder="" name="select_Cities[]">
            </span>
		 <span class="span5">	
         		  <label class="control-label">Applicants business</label>	
                  		  <select name="select_applicant[]" class="select_new check_App ui-state-valid" id="select_App<?php echo $i;?>" onchange="business_app(<?php echo $i;?>,this.value);">	
                          
                                    <script>
																 var u=1;
																 $(document).ready(function(){
																	
																     business_app(u,$('#select_App'+u).val());
																	 u++;
																 });
																 </script>	
                          		 <?php $select_applicant= isset($value->select_applicant) ? $value->select_applicant : ''; ?> 
                                  <option value="">Please Select</option>		
                                  	  <option value="Tow truck driver" <?php if($select_applicant  == 'Tow truck driver') echo 'selected="selected"'; ?>>Tow truck driver</option>
                                  			  <option value="Other" <?php if($select_applicant  == 'Other') echo 'selected="selected"'; ?>> Other </option>	
                                              		  </select>     
                                                        
                                                             <input class="textinput span5 check_app_other" type="text" placeholder="Please specify" style="display:none;" value="<?php isset($value->select_applicant_driver) ? $value->select_applicant_driver : '';?>" id="select_app_other<?php echo $i;  ?>" name="select_applicant_driver[]">   
                                                                      </span>
                                                                      </div>
                                                                        <div class="row-fluid" id="checkbox_id1" style="display:none;">   
                                                                                 <span class="span4">        
                                                                                       <label class="checkbox">  
                                                                       <input type="checkbox" value="Yes" onclick="select_ch_rad1();"  class="checkbox_radius" id="select_ch_radius1" name="select_ch_radius[]">      
                                                                                 <span>Same Radius of Operation for all vehicles</span>    
                                                                                           </label>    
                                                                                      </span>  
                                                                                       <span class="span3">    
                                                                                       
                                                                                           <label class="checkbox">    
                                                                                                      
                   <input type="checkbox" value="Yes" onclick="select_ch_cit1();"  class="checkbox_cities" id="select_ch_cities1" name="select_ch_cities[]">     
                              <span>Same Cities for all vehicles</span>           
                                 </label>   
                                          </span>      
                                                <span class="span5">    
                                                          <label class="checkbox">            
                                                              <input type="checkbox" value="Yes" onclick="select_ch_app1();" class="checkbox_applicant" id="select_ch_applicant1" name="select_ch_applicant[]">    
                                                                          <span>Same Applicants Business for all vehicles</span>         
                                                                               </label>       
                                                                                </span>   
                                                                                  </div>
          <div class="row-fluid">
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" value="<?php echo  isset($value->request_for_radius_of_operation) ? $value->request_for_radius_of_operation : '';?>" type="text" placeholder="" name="select_special[]">
            </span>
            <span class="span2">
              <label class="control-label">Number of Axels</label>
             
              <select name="select_Axels[]" class="select_new">
			   <?php  $select_Axels= isset($value->number_of_axles) ? $value->number_of_axles : '';?>
                 <option value="">Please Select</option>
				 <option value="1 Axel" <?php if($select_Axels  == '1 Axle') echo 'selected="selected"'; ?> >1 Axel</option>
				 <option value="2 Axel" <?php if($select_Axels  == '2 Axle') echo 'selected="selected"'; ?> >2 Axel</option>
				 <option value="3 Axel" <?php if($select_Axels  == '3 Axle') echo 'selected="selected"'; ?> >3 Axel</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">Number of drivers:&nbsp;</label>
               <input class="span4" pattern="[0-9]+" type="text" value="<?php echo  isset($value->select_num_drivers) ? $value->select_num_drivers : '';?>" placeholder="" onchange="add_driver(this.value,<?php echo $i;?>)" name="select_num_drivers[]" maxlength="2">
            </span>
            <span class="span3">
              <label class="control-label"></label>
            </span>
          </div>
		 <div class="row-fluid">   
                  <span class="span12">   
                  
                             <label class="control-label pull-left">Do you pull equipment with this vehicle? &nbsp;</label>         
                                  <select name="select_equipment[]" class="select_new span1" id="select_equipment" onchange="equipment_pull(<?php echo $i; ?>,this.value);">       
                                                              <?php echo  $select_equipment = isset($value->select_equipment) ? $value->select_equipment : '';?>
                   <option value="Yes" <?php if($select_equipment  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>                                             
                <option value="No" <?php if($select_equipment  == 'No') echo 'selected="selected"'; ?> >No</option>
                 	
                                                             			   </select>
                                                                                   <script>
																 var r=1;
																 $(document).ready(function(){
																	
																     equipment_pull(r,$('#select_equipment').val());
																	 r++;
																 });
																 </script>		
                                                                 <select id="put_on_truck<?php echo  $i; ?>" name="put_on_truck[]" style="display:none;" class="span2">  
                                          <?php $put_on_truck=isset($value->put_on_truck) ? $value->put_on_truck : '';?>
                                          <option value="None" <?php if($put_on_truck  == 'None') echo 'selected="selected"'; ?>>None</option>
                                           <option value="Dolly" <?php if($put_on_truck  == "Dolly") echo 'selected="selected"'; ?>>Dolly</option>                        
                                          <option value="Trailer" <?php if($put_on_truck  == "Trailer") echo 'selected="selected"'; ?>>Trailer</option>                        
                                          <option value="MotorHome" <?php if($put_on_truck  == "MotorHome") echo 'selected="selected"'; ?>>MotorHome</option>
                                          </select>
                                   <!--		  <select id="put_on_truck<?php echo  $i; ?>" name="put_on_truck[]" style="display:none;" class="span2">   
                                   
                                    
                                                       	<option value="None" <?php if($put_on_truck  == 'None') echo 'selected="selected"'; ?>>None</option>		
                                   						<option value="Single" <?php if($put_on_truck  == 'Single') echo 'selected="selected"'; ?>>Single</option>		
                                      						<option value="Double" <?php if($put_on_truck  == 'Double') echo 'selected="selected"'; ?>>Double</option>		
                                                           <option value="Triple" <?php if($put_on_truck  == 'Triple') echo 'selected="selected"'; ?>>Triple</option>     
                                                           </select>-->
                                                           
                                                           
                                                           <div class="row-fluid">    
                                                             <span class="span6">     
                                                                  <label class="control-label pull-left">Do you need Physical Damage Coverage? &nbsp;</label> 
                                                               <select name="pd_damage[]" onchange="pd_damage_truck(<?php echo  $i; ?>);" id="pd_dam_truck<?php echo  $i; ?>" class="span2">
                                                                 <script>
																 var k=1;
																 $(document).ready(function(){
																	
																     pd_damage_truck(k);
																	 k++;
																 });
																 </script>	
                                                               
                                                               		   <?php $pd_damage = isset($value->pd_damage) ? $value->pd_damage : '';?>
                <option value="No" <?php if($pd_damage  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($pd_damage  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>  
                                                                                       </select>     
                                                                                      </span>      
                                                                               <span class="span6">    
                                                                      <label class="control-label pull-left">Do you need Cargo Coverage? &nbsp;</label>    
                                                      <select name="cd_damage[]" class="select_new span2" onchange="cd_damage_truck(<?php echo  $i; ?>);" id="cd_dam_truck<?php echo  $i; ?>">  
                                                      
                                                  
                                                            <?php $cd_damage = isset($value->cd_damage) ? $value->cd_damage : '';?>
                <option value="No" <?php if($cd_damage  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($cd_damage  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>   
                                                                     
                                                                                </select>    
                                                                                        <script>
																 var l=1;
																 $(document).ready(function(){
																	 
																     cd_damage_truck(l);
																	l++;
																 });
																 </script>	     
                                                                                   </span>   
                                                                             </div></span></div></div>
                                                              <span id="add_pd_truck_div<?php echo $i; ?>" style="">
                                                              <h5 class="heading sh sh-2">Physical Damage</h5>
                                                              <a class="btn btn-mini" style="" onclick="pd_add(<?php echo  $i; ?>);" id="pd_add_button<?php echo  $i; ?>"> 
                                                              <i class="icon icon-plus"></i> Add</a>	
															  
															 
                                                              		 <div class="row-fluid" id=<?php echo "pd_add_val".$i?>>  
                                                                      <?php $j=1;
																	 
																	  foreach( $app_veh_pd as $value1){  
																	  if($value1->vehicle_id == $value->vehicle_id){
																	  
																	   ?>
                                                           <input type="hidden" value="<?php echo  isset($value1->v_id) ? $value1->v_id : ''; ?>" name="v_id<?php echo  $i; ?>[]"/>
                                                            <input type="hidden" value="<?php echo  isset($value->vehicle_id) ? $value->vehicle_id : ''; ?>" name="veh_id<?php echo  $i; ?>[]"/>
                                                           
                                                                      <div class="row-fluid" id=<?php echo "row_add_pd".$i?>>
                                                                                <span class="span1">        
                                                                                    <label class="control-label">Rate</label>  
                                                                                                    <select name="pd_rate<?php echo $i; ?>[]" class="select_new_per">      
                                                                                                          
                                                      <?php $pd_rate = isset($value1->pd_rate) ? $value1->pd_rate : '';?>
                                                      <option value="">select</option>
                <option value="5%" <?php if($pd_rate  == '5%') echo 'selected="selected"'; ?> >5%</option>
                 <option value="10%" <?php if($pd_rate  == '10%') echo 'selected="selected"'; ?>>10%</option>  
                                                                                                                              </select>      
                                                                                                                                    </span>      
                                <span class="span3">
                                    <label class="control-label">Loss payee and full address</label>   
                                        <select name="pd_loss_payee<?php echo  $i; ?>[]" class="span4" id="pd_loss_payee<?php echo  $j; ?>"  onchange="loss_payee(<?php echo  $j; ?>)">	
                                        <?php $pd_loss_payee = isset($value1->pd_loss_payee) ? $value1->pd_loss_payee : '';?>
                                        	 <option value="No" <?php if($pd_loss_payee  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($pd_loss_payee  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>   
                                                                  </select>     
                                                                         </span>		
                                                                         	     <span class="span3" id="coverage<?php echo $j;?>">  
                                                 <label class="control-label">Additional optional coverage</label>     
                                                          <select name="pd_coverage<?php echo  $i; ?>[]" class="span8" onchange="pd_coverage1(<?php echo  $j; ?>,this.value);" id="pd_coverage1<?php echo  $j; ?>">	
                                                           <?php $pd_coverage = isset($value1->pd_coverage) ? $value1->pd_coverage : '';?>
                                                           
                                                        <option value="Debris removal"  <?php if($pd_coverage  == 'Debris removal') echo 'selected="selected"'; ?> >Debris removal</option>
                                               <option value="Towing & storage" <?php if($pd_coverage  == 'Towing & storage') echo 'selected="selected"'; ?>>Towing &amp; storage</option>  
                                               <option value="Tarpaulin coverage" <?php if($pd_coverage  == 'Tarpaulin coverage') echo 'selected="selected"'; ?>>Tarpaulin coverage</option>  
                                                <option value="Other" <?php if($pd_coverage  == 'Other') echo 'selected="selected"'; ?>>Other</option> 
                                                  </select> 
                                                  
                                                  <script>
												  var y=1
												  $(document).ready(function(){
													 
													   pd_coverage1(y,$('#pd_coverage1'+y).val());
													  y++;
													  });
												  
											
												  </script>
                                                <input class="textinput  span4" type="text" id="Specify_o<?php echo  $j; ?>" style="display:none;" placeholder="Specify" value="<?php echo  isset($value1->pd_coverage_specify) ? $value1->pd_coverage_specify : '';?>" name="pd_coverage_specify<?php echo  $i; ?>[]">     
                                                       </span>		
                                 	<span class="span2">   
                                      <label class="control-label">Premium</label>     
                                            <input class="textinput span9" type="text" value="<?php echo  isset($value1->pd_premium) ? $value1->pd_premium : '';?>" placeholder="" name="pd_premium<?php echo  $i; ?>[]">    
                                                     </span>      
                                  <span class="span2">       
                                                 <label class="control-label">Deductible</label>        
                                                                                           <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->pd_deductible) ? $value1->pd_deductible : '';?>" name="pd_deductible<?php echo  $i; ?>[]">     
                                                                                                              </span>   
                                                                                                              <span class="span1"> <label class="control-label">Remove</label><a class="btn btn-mini btn-1   " onclick="remove_pd_row1(<?php echo  $i; ?>);" id="remove_pd_row"><i class="icon icon-remove "></i></a></span>
                                                         
                                          <div class="row-fluid" style="" id="pd_amount<?php echo $i; ?>">    
                  <span class="span7">    
                            <label class="control-label"></label>  
                                        <table class="table">        
                                                <thead>     
                                                             <tr>     
                                                                  <th>
                                                                  </th>  
                              <th>Amount or Limit</th> 
                                                 <th>Deductible</th>
                                                                     <th>Premium</th>
                                                                                       </tr>  
                                                                                                     </thead>   
                                                                                                                  <tbody> 
             <tr>                    
             <td id="removal_name<?php echo $i; ?>">Debris removal</td>                    
             <td>                     
             <input class="textinput input-mini" type="text" value="<?php echo  isset($value1->pd_amount_limit) ? $value1->pd_amount_limit : '';?>" placeholder="" name="pd_amount_limit<?php echo  $i; ?>[]">                    
             </td>                   
              <td>                     
               <input class="textinput input-mini" type="text" value="<?php echo  isset($value1->pd_deductible_limit) ? $value1->pd_deductible_limit : '';?>" placeholder="" name="pd_deductible_limit<?php echo  $i; ?>[]">                    
               </td>                   
                <td>                      
                <input class="textinput input-mini" type="text" value="<?php echo  isset($value1->pd_premium_limit) ? $value1->pd_premium_limit : '';?>" placeholder="" name="pd_premium_limit<?php echo  $i; ?>[]">                    
                </td>                  </tr>                </tbody>              </table>            
                </span>           
                 <span class="span5">             
                  <label class="control-label"></label>           
                   </span>         
                    </div>                                                                                                           
                                                                                                          
                                                                                                          
                                                                                                          
                                                                                                           </div>
																		          
                                                                                                     
                                                                                               <?php $j++; } } ?>
                                                                                                         
          </div>
                                                                                             
		        <label class="control-label control-label-2">Loss Payee Address(es)</label>
          <div class="row-fluid">
            <span class="span3">
              <label class="control-label">Entity name</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->pd_loss_name) ? $value->pd_loss_name : '';?>" name="pd_loss_name[]">
            </span>
            <span class="span3">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->pd_loss_address) ? $value->pd_loss_address : '';?>" name="pd_loss_address[]">
            </span>
            <span class="span1">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder=""  value="<?php echo  isset($value->pd_loss_city) ? $value->pd_loss_city : '';?>" name="pd_loss_city[]">
            </span>
            <span class="span1">
              <label class="control-label">State</label>
              						<?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = isset($value->pd_loss_state) ? $value->pd_loss_state : '';;
									
									?>
									<?php echo get_state_dropdown('pd_loss_state[]', $broker_state, $attr); ?>
            
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value->pd_loss_zip) ? $value->pd_loss_zip : '';?>" name="pd_loss_zip[]" pattern="[0-9]+" maxlength="9">
            </span>
            <span class="span3">
              <label class="control-label">Remarks</label>

              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->pd_loss_remarks) ? $value->pd_loss_remarks : '';?>" name="pd_loss_remarks[]">
            </span>
          </div>
       	</span>
					
					
					<span id="add_cd_truck_div<?php echo $i; ?>" style=""><h5 class="heading sh sh-3">Cargo</h5><div class="row-fluid">            
                    <span class="span3" style="" id="cargo_damages<?php $i; ?>">             
                     <label class="control-label">Average value per load &nbsp;</label>              
                     <input class="textinput span6 price_format" type="text" placeholder="$" value="<?php echo  isset($value->cargo_avg_val) ? $value->cargo_avg_val : '';?>" name="cargo_avg_val[]">             
                      <input class="textinput span6" type="text" placeholder="%" value="<?php echo  isset($value->cargo_avg_per) ? $value->cargo_avg_per : '';?>" name="cargo_avg_per[]">            
                      </span>            
                      <span class="span3" style="margin-left:10px;">              
                      <label class="control-label">Maximum values per load &nbsp;</label>              
                      <input class="textinput span6 price_format" type="text" placeholder="$" value="<?php echo  isset($value->cargo_max_val) ? $value->cargo_max_val : '';?>" name="cargo_max_val[]">              
                      <input class="textinput span6" type="text" placeholder="%" value="<?php echo  isset($value->cargo_max_per) ? $value->cargo_max_per : '';?>" name="cargo_max_per[]">            
                      </span>         
                         <span class="span2">     
                             <label class="control-label">% Factor</label>             
                       <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->cargo_factor) ? $value->cargo_factor : '';?>" name="cargo_factor[]">     
                              </span>           
                        <span class="span2">              <label class="control-label">Premium</label>            
                          <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->cargo_prem) ? $value->cargo_prem : '';?>" name="cargo_prem[]">    
                                  </span>            
                          <span class="span2">             
                          <label class="control-label">Deductible</label>              
                          <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value->cargo_deduct) ? $value->cargo_deduct : '';?>" name="cargo_deduct[]">           
                           </span>          </div>		  
                             <div class="row-fluid">    
                                   <span class="span6">             
                            <label class="control-label">Additional optional coverage</label>             
                             <select name="coverage_name[]" class="select-15" onchange="coverage_name1(<?php echo $i; ?>,this.value)"  id="coverage_name1<?php echo $i; ?>">	
                             <?php $coverage_name = isset($value->coverage_name) ? $value->coverage_name : '';?>		 
                              <option value="">Please Select</option>               
                               <option <?php if($coverage_name  == 'Earned freight clause') echo 'selected="selected"'; ?> value="Earned freight clause">Earned freight clause</option>               
                                <option <?php if($coverage_name  == 'Refrigeration breakdown deductible') echo 'selected="selected"'; ?> value="Refrigeration breakdown deductible">Refrigeration breakdown deductible</option>               
                                 <option <?php if($coverage_name  == 'Debris removal') echo 'selected="selected"'; ?> value="Debris removal">Debris removal</option>               
                                  <option <?php if($coverage_name  == 'Excess cargo') echo 'selected="selected"'; ?> value="Excess cargo">Excess cargo</option>             
                                   </select>            
                                   </span>      
                                   
                                         <script>
												  var x=1
												  $(document).ready(function(){
													 
													   coverage_name1(x,$('#coverage_name1'+x).val());
													  x++;
													  });
												  
											
												  </script>   
                                     
                                    <span class="span6">             
                                     <label class="control-label"></label>     
                                         
                             </span>          </div>
        
		  <span id="cargo_dis<?php echo $i; ?>" style=""> <div class="row-fluid" style="" id="cargo_amount<?php echo $i; ?>">    
                  <span class="span8">              <table class="table">      
                            <thead>                  <tr>             
                            
                                   <th></th>                    <th>Amount or Limit</th>                   
                                    <th>Deductible</th>                    <th>Premium</th>                 
                                     </tr>                </thead>                <tbody>                 
                                      <tr>                    <td>Earned freight clause</td>                    <td>                     
                                       <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value->cargo_amount_limit) ? $value->cargo_amount_limit : '';?>" name="cargo_amount_limit[]">                   
                                        </td>                    
                                        <td>                     
                                         <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value->cargo_amount_deduct) ? $value->cargo_amount_deduct : '';?>" name="cargo_amount_deduct[]">                   
                                          </td>                  
                                            <td>                     
                                             <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value->cargo_amount_premium) ? $value->cargo_amount_premium : '';?>" name="cargo_amount_premium[]">          
                                                       </td>       
                                                                  </tr>      
                                                                              <tr>           
                                           <td>Refrigeration breakdown deductible</td>    
                               <td>          <input class="textinput input-mini" type="text" placeholder=""  value="<?php echo  isset($value->cargo_breakdown_limit) ? $value->cargo_breakdown_limit : '';?>" name="cargo_breakdown_limit[]">              
                                    </td>        
                                                <td>           
                                                           <input class="textinput input-mini" type="text"  value="<?php echo  isset($value->cargo_breakdown_deduct) ? $value->cargo_breakdown_deduct : '';?>" placeholder="" name="cargo_breakdown_deduct[]">                    
                                                           </td>                    
                                                           <td>                      
                                                           <input class="textinput input-mini" type="text"  value="<?php echo  isset($value->cargo_breakdown_premium) ? $value->cargo_breakdown_premium : '';?>" placeholder="" name="cargo_breakdown_premium[]">                   
                                                            </td>                 
                                                             </tr>                
                                                             </tbody>             
                                                              </table>           
                                                               </span>           
                                                                <span class="span4">             
                                                                 <label class="control-label"></label>           
                                                                  </span>         
                                                                   </div></span>
																   </span>
			
  															   
	<span id="add_drivers_truck_div<?php echo $i;?>" style="display:none;">
      <h5 class="heading sh sh-5">Drivers Schedule</h5>
          
          <table class="table">
            <thead>
              <tr>
                <th>First name</th>
                <th>Middle name</th>
                <th>Last name</th>
                <th>Driver License
                  <br>Number</th>
                <th>License class</th>
                <th>Date hired</th>
                <th>How many years
                  <br>Class A license</th>
                <th>License issue state</th>
                <th>Remarks</th>
                <th>Surcharge</th>
              </tr>
            </thead>
            <tbody id="<?php echo $i; ?>add_drivered_row1">
            <?php 


if (!empty($app_driver1)) { ?> 	
                <?php   foreach ($app_driver1 as $drivers) {
					  if ($drivers->vehicle_id == $value->vehicle_id) {
					
					 ?>
          <input type="hidden" value="<?php echo  isset($value->vehicle_id) ? $value->vehicle_id : ''; ?>" name="<?php echo $i; ?>dri_id[]"/>
      <input type="hidden" value="<?php echo (isset($drivers->driver_id) ? $drivers->driver_id : ''); ?>" name="<?php echo $i; ?>driver_id[]"  />
              <tr>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_fname)){ echo (isset($drivers->driver_fname) ? $drivers->driver_fname : ''); }else{ echo (isset($drivers->driver_name) ? $drivers->driver_name : ''); } ?>" name="<?php echo $i;?>driver_fname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder=""  value="<?php if(!empty($drivers->driver_mname)){ echo (isset($drivers->driver_mname) ? $drivers->driver_mname : ''); }else{ echo (isset($drivers->driver_middle_name) ? $drivers->driver_middle_name : ''); } ?>" name="<?php echo $i;?>driver_mname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_lname)){ echo (isset($drivers->driver_lname) ? $drivers->driver_lname : ''); }else{ echo (isset($drivers->driver_last_name) ? $drivers->driver_last_name : ''); } ?>" name="<?php echo $i;?>driver_lname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_license_num)){ echo (isset($drivers->driver_license_num) ? $drivers->driver_license_num : ''); }else{ echo (isset($drivers->driver_licence_no) ? $drivers->driver_licence_no : ''); } ?>" name="<?php echo $i;?>driver_license_num[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_license_class)){ echo (isset($drivers->driver_license_class) ? $drivers->driver_license_class : ''); }else{ echo (isset($drivers->driver_license_class) ? $drivers->driver_license_class : ''); } ?>" name="<?php echo $i;?>driver_license_class[]">
                </td>
                <td>
                  <input class="textinput input-mini datepick" type="text" value="<?php if(!empty($drivers->driver_date_hired)){ echo (isset($drivers->driver_date_hired) ? $drivers->driver_date_hired : ''); }else{ echo (isset($drivers->driver_license_issue_date) ? date('m/d/Y', strtotime($drivers->driver_license_issue_date)) : ''); } ?>" required="required" placeholder="" name="<?php echo $i;?>driver_date_hired[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" value="<?php if(!empty($drivers->driver_class_A)){  echo (isset($drivers->driver_class_A) ? $drivers->driver_class_A : ''); }else{ echo (isset($drivers->driver_class_a_years) ? $drivers->driver_class_a_years : ''); } ?>" placeholder="" name="<?php echo $i;?>driver_class_A[]">
                </td>
                <td>
                   <?php
									$attr = "class='span12 width_td ui-state-valid'";
									$broker_state =   isset($drivers->driver_state) ? $drivers->driver_state : $drivers->driver_license_issue_state;
									$driver_state = $i.'driver_state[]'
									?>
									<?php echo get_state_dropdown($driver_state, $broker_state, $attr); ?>
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_remarks)){ echo  (isset($drivers->driver_remarks) ? $drivers->driver_remarks : ''); } else{ isset($drivers->driver_remarks) ? $drivers->driver_remarks : ''; } ?>" name="<?php echo $i;?>driver_remarks[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_surcharge)){ echo (isset($drivers->driver_surcharge) ? $drivers->driver_surcharge : ''); } else{ isset($drivers->driver_surcharge) ? $drivers->driver_surcharge : ''; } ?>" name="<?php echo $i;?>driver_surcharge[]">
                </td>
              </tr>
           <?php } } } ?>
            </tbody>
           </table>
		  </span></div>
          <?php $i++;} } ?>
          
		<span id="add_tractor_div"></span>
        
         <?php $k=1; if(!empty($app_veh2)){ foreach($app_veh2 as $value1) { ?>
       <input type="hidden" value="<?php echo  isset($value1->vehicle_id) ? $value1->vehicle_id : ''; ?>" name="t_vehicle_id[]"/
        <div class="row_added_tractor vehicle_schedule"><div class="div-1">    
                <div class="row-fluid btn-success" style="padding: 5px 0px 5px 0px;">  
                            <span class="span4">                
                            <label class="control-label control-label-2" style="font-weight:bold;font-size:15px; margin-left:10px;margin-top: 5px;">TRACTOR #<?php echo $k; ?></label>         
                                 </span>              
                                 <span class="span8" style="margin-left: 20px;">              
   <a class="btn pull-right btn_sp" onclick="drivers_tractor(<?php echo $k; ?>,&quot;yes&quot;);" id="t_drivers_button<?php echo $k; ?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Drivers</a>               
    <a class="btn pull-right btn_sp" onclick="trailer_tractor(<?php echo $k; ?>,&quot;no&quot;);" id="t_trailer_button<?php echo $k; ?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Trailers</a>                
    <a class="btn pull-right btn_sp" onclick="cargo_tractor(<?php echo $k; ?>,&quot;no&quot;);" id="t_cargo_button<?php echo $k; ?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Cargo</a>                
    <a class="btn pull-right btn_sp" onclick="pd_tractor(<?php echo $k; ?>,&quot;no&quot;);" id="t_pd_button<?php echo $k; ?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Physical Damage</a>               
     <a class="btn pull-right btn-1 btn-2" onclick="liability_tractor(<?php echo $k; ?>,&quot;no&quot;);" id="t_liability_button<?php echo $k; ?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Liability</a>              
     </span>            </div>
           <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="t_select_vehicle_year[]" class="select_new">
                 <?php echo  $t_select_vehicle_year= isset($value1->select_vehicle_year) ? $value1->select_vehicle_year : '' ?>
                 <option value="1985"  <?php if($t_select_vehicle_year  == '1985') echo 'selected="selected"'; ?>>1985</option>
                 <option value="1986"  <?php if($t_select_vehicle_year  == '1986') echo 'selected="selected"'; ?>>1986</option>
                                                                <option value="1987"  <?php if($t_select_vehicle_year  == '1987') echo 'selected="selected"'; ?>>1987</option>
                                                                <option value="1988"  <?php if($t_select_vehicle_year  == '1988') echo 'selected="selected"'; ?>>1988</option>
                                                                <option value="1989"  <?php if($t_select_vehicle_year  == '1989') echo 'selected="selected"'; ?>>1989</option>
																<option value="1990"  <?php if($t_select_vehicle_year  == '1990') echo 'selected="selected"'; ?>>1990</option>
                                                                <option value="1991"  <?php if($t_select_vehicle_year  == '1991') echo 'selected="selected"'; ?>>1991</option>
                                                                <option value="1992"  <?php if($t_select_vehicle_year  == '1992') echo 'selected="selected"'; ?>>1992</option>
                                                                <option value="1993"  <?php if($t_select_vehicle_year  == '1993') echo 'selected="selected"'; ?>>1993</option>
                                                                <option value="1994"  <?php if($t_select_vehicle_year  == '1994') echo 'selected="selected"'; ?>>1994</option>
                                                                <option value="1995"  <?php if($t_select_vehicle_year  == '1995') echo 'selected="selected"'; ?>> 1995</option>
                                                                <option value="1996"  <?php if($t_select_vehicle_year  == '1996') echo 'selected="selected"'; ?>>1996</option>
                                                                <option value="1997"  <?php if($t_select_vehicle_year  == '1997') echo 'selected="selected"'; ?>>1997</option>
                                                                <option value="1998"  <?php if($t_select_vehicle_year  == '1998') echo 'selected="selected"'; ?>>1998</option>
                                                                <option value="1999"  <?php if($t_select_vehicle_year  == '1999') echo 'selected="selected"'; ?>>1999</option>
                                                                <option value="2000"  <?php if($t_select_vehicle_year  == '2000') echo 'selected="selected"'; ?>>2000</option>
                                                                <option value="2001"  <?php if($t_select_vehicle_year  == '2001') echo 'selected="selected"'; ?>>2001</option>
                                                                <option value="2002"  <?php if($t_select_vehicle_year  == '2002') echo 'selected="selected"'; ?>>2002</option>
                                                                <option value="2003"  <?php if($t_select_vehicle_year  == '2003') echo 'selected="selected"'; ?>>2003</option>
                                                                <option value="2004"  <?php if($t_select_vehicle_year  == '2004') echo 'selected="selected"'; ?>>2004</option>
                                                                <option value="2005"  <?php if($t_select_vehicle_year  == '2005') echo 'selected="selected"'; ?>>2005</option>
                                                                <option value="2006"  <?php if($t_select_vehicle_year  == '2006') echo 'selected="selected"'; ?>>2006</option>
                                                                <option value="2007"  <?php if($t_select_vehicle_year  == '2007') echo 'selected="selected"'; ?>>2007</option>
                                                                <option value="2008"  <?php if($t_select_vehicle_year  == '2008') echo 'selected="selected"'; ?>>2008</option>
                                                                <option value="2009"  <?php if($t_select_vehicle_year  == '2009') echo 'selected="selected"'; ?>>2009</option>
                                                                <option value="2010"  <?php if($t_select_vehicle_year  == '2010') echo 'selected="selected"'; ?>>2010</option>
                                                                <option value="2011"  <?php if($t_select_vehicle_year  == '2011') echo 'selected="selected"'; ?>>2011</option>
                                                                <option value="2012"  <?php if($t_select_vehicle_year  == '2012') echo 'selected="selected"'; ?>>2012</option>
                                                                <option value="2013"  <?php if($t_select_vehicle_year  == '2013') echo 'selected="selected"'; ?>>2013</option>
																<option value="2014"  <?php if($t_select_vehicle_year  == '2014') echo 'selected="selected"'; ?>>2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="t_select_model[]" class="select_new">
                <?php $select_model= isset($value1->select_model) ? $value1->select_model : ''; ?>
                <option value="">Select</option>
				<option value="Freightliner" <?php if($select_model  == 'Freightliner') echo 'selected="selected"'; ?>>Freightliner</option>
                <option value="International" <?php if($select_model  == 'International') echo 'selected="selected"'; ?>>International</option>
                 <option value="Isuzu" <?php if($select_model  == 'Isuzu') echo 'selected="selected"'; ?>>Isuzu</option>
                 <option value="Kenworth" <?php if($select_model  == 'Kenworth') echo 'selected="selected"'; ?>>Kenworth</option>
                 <option value="Mack" <?php if($select_model  == 'Mack') echo 'selected="selected"'; ?>>Mack</option>
                 <option value="Peterbilt" <?php if($select_model  == 'Peterbilt') echo 'selected="selected"'; ?>>Peterbilt</option>
                 <option value="Sterling" <?php if($select_model  == 'Sterling') echo 'selected="selected"'; ?>>Sterling</option>
                 <option value="Western Star" <?php if($select_model  == 'Western Star') echo 'selected="selected"'; ?>>Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="t_select_GVW[]" class="select_new">
                   <?php $select_GVW= isset($value1->select_GVW) ? $value1->select_GVW : ''; ?>
                   <option value="0-10,000 pounds" <?php if($select_model  == '0-10,000 pounds') echo 'selected="selected"'; ?>>0-10,000 pounds</option>
                                                                <option value="10,001-26,000 pounds" <?php if($select_GVW  == '10,001-26,000 pounds') echo 'selected="selected"'; ?>>10,001-26,000 pounds</option>
                                                                <option value="26,001-40,000 pounds" <?php if($select_GVW  == '26,001-40,000 pounds') echo 'selected="selected"'; ?>>26,001-40,000 pounds</option>
                                                                <option value="40,001-80,000 pounds" <?php if($select_GVW  == '40,001-80,000 pounds') echo 'selected="selected"'; ?>>40,001-80,000 pounds</option>
                                                                <option value="Over 80,000 pounds" <?php if($select_GVW  == 'Over 80,000 pounds') echo 'selected="selected"'; ?>>Over 80,000 pounds</option>
              </select>
            </span>
             <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->select_VIN) ? $value1->select_VIN : ''; ?>" name="t_select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->select_Operation) ? $value1->select_Operation : ''; ?>" name="t_select_Operation[]">
            </span>
          </div>
		  
              </div>
			  <div style="" class="row-fluid" id="c_damage_tractor<?php echo $k; ?>">
            	 <div style="" id="c_damage_truck"><div class="row-fluid">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="t_select_Radius[]" id="1select_Radius" class="select_new check_Radius">
               <?php $select_Radius= isset($value1->select_Radius) ? $value1->select_Radius : ''; ?>
			    <option value="">Please Select</option>
                 <option value="0-100 miles" <?php if($select_Radius  == '0-100 miles') echo 'selected="selected"'; ?>>0-100 miles</option>
                                                                <option value="101-500 miles" <?php if($select_Radius  == '101-500 miles') echo 'selected="selected"'; ?>>101-500 miles</option>           	
                                                                <option value="501+ miles" <?php if($select_Radius  == '501+ miles') echo 'selected="selected"'; ?>>501+ miles</option>
                                                              	<option value="other" <?php if($select_Radius  == 'other') echo 'selected="selected"'; ?>>Other</option>
              </select>
            </span>
			
            <span class="span2">
              <label class="control-label">States driving</label>
            									 <?php
									$attr = "class='span12 ui-state-valid state_driv' id='state_driving'" ;
									$broker_state = isset($value1->state_driving) ? $value1->state_driving : '';
									
									?>
									<?php echo get_state_dropdown('t_state_driving[]', $broker_state, $attr); ?>
            
            </span>
			
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12 check_Cities" type="text" id="1select_Cities" value="<?php echo  isset($value1->select_Cities) ? $value1->select_Cities : ''; ?>" placeholder="" name="t_select_Cities[]">
            </span>
       
               <span class="span5">              
			   <label class="control-label">Applicants business</label>    
			   <select name="t_select_applicant[]" id="t_select_App<?php echo $k;?>" onchange="t_business_app(<?php echo $k;?>,this.value);" class="select_new t_check_App">		
               	<script>
                 var w=1;
																 $(document).ready(function(){
																	
																     t_business_app(w,$('#t_select_App'+w).val());
																	 w++;
																 });
																 </script>	  
			  
 <?php $select_applicant= isset($value1->select_applicant) ? $value1->select_applicant : ''; ?>			   
			  <option value="">Please Select</option>		
                                  	  <option value="Tow truck driver" <?php if($select_applicant  == 'Tow truck driver') echo 'selected="selected"'; ?>>Tow truck driver</option>
                                  			  <option value="Other" <?php if($select_applicant  == 'Other') echo 'selected="selected"'; ?>> Other </option>	         
			   </select>              
			   <input class="textinput span5 t_check_app_other" value="<?php isset($value1->select_applicant_driver) ? $value1->select_applicant_driver : '';?>" type="text" id="t_select_app_other1" style="display:none;" placeholder="Please specify" name="t_select_applicant_driver[]">            
			   </span>  
			   <div class="row-fluid" id="t_checkbox_id1" style="display:none;">            
			   <span class="span4">              
			   <label class="checkbox">                
			   <input type="checkbox" value="Yes" onclick="t_select_ch_rad1();"  class="checkbox_radius" id="t_select_ch_radius1" name="t_select_ch_radius[]">                
			   <span>Same Radius of Operation for all vehicles</span>              
			   </label>            </span>            
			   <span class="span3">             
			   <label class="checkbox">                
			   <input type="checkbox" value="Yes" onclick="t_select_ch_cit1();" class="checkbox_cities" id="t_select_ch_cities1" name="t_select_ch_cities[]">                
			   <span>Same Cities for all vehicles</span>              
			   </label>            
			   </span>            
			   <span class="span5">              
			   <label class="checkbox">                
			   <input type="checkbox" value="Yes" onclick="t_select_ch_app1();" class="checkbox_applicant" id="t_select_ch_applicant1" name="t_select_ch_applicant[]">                
			   <span>Same Applicants Business for all vehicles</span>              
			   </label>            
			   </span>         
			   </div>
		   <div class="row-fluid">
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" value="<?php echo isset($value1->select_special) ? $value1->select_special : '';?>" placeholder="" name="t_select_special[]">
            </span>
            <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="t_select_Axels[]" class="select_new">
              <?php $select_Axels= isset($value1->select_Axels) ? $value1->select_Axels : '';?>
                 <option value="">Please Select</option>
				 <option value="1 Axel" <?php if($select_Axels  == '1 Axel') echo 'selected="selected"'; ?> >1 Axel</option>
				 <option value="2 Axel" <?php if($select_Axels  == '2 Axel') echo 'selected="selected"'; ?> >2 Axel</option>
				 <option value="3 Axel" <?php if($select_Axels  == '3 Axel') echo 'selected="selected"'; ?> >3 Axel</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">Number of drivers?&nbsp;</label>
              <input class="span4" type="text" pattern="[0-9]+" placeholder="" value="<?php echo  isset($value1->select_num_drivers) ? $value1->select_num_drivers : '';?>"  on name="t_select_num_drivers[]" maxlength="2">
            </span>
            <span class="span3">
              <label class="control-label"></label>
            </span>
			</div>
          <div class="row-fluid">            
          <span class="span12">              
          <label class="control-label pull-left">Do you pull equipment with this vehicle? &nbsp;</label>              
          <select name="t_select_equipment[]" id="t_select_equipment" class="select_new span1 ui-state-valid" onchange="t_equipment_pull(<?php echo $k; ?>,this.value);">           
          <?php $select_equipment = isset($value1->select_equipment) ? $value1->select_equipment : '';?>
				 <option value="Yes" <?php if($select_equipment  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>												  
                <option value="No" <?php if($select_equipment  == 'No') echo 'selected="selected"'; ?> >No</option>
                
                                                             	 </select>			  
                                                                  			   </select>
                                                                                   <script>
																 var z=1;
																 $(document).ready(function(){
																	
																     t_equipment_pull(z,$('#t_select_equipment').val());
																	 z++;
																 });
																 </script>	
           <select type="text" name="t_put_on_truck[]" id="vehicle_trailer<?php echo $k; ?>" style="" onchange="add_trailer1(this.value,<?php echo $k; ?> );" class="span2 ui-state-valid">			  
         <?php //echo count($app_veh3);
		// print_r($app_veh3);
		$put_on_truck = isset($value1->put_on_truck) ? $value1->put_on_truck : '';?>
           <option value="0" <?php if($put_on_truck  == 0) echo 'selected="selected"'; ?>>None</option>			  
           <option value="1" <?php if($put_on_truck  == 1) echo 'selected="selected"'; ?>>Single</option>				 
            <option value="2" <?php if($put_on_truck  == 2) echo 'selected="selected"'; ?>>Double</option>				  
            <option value="3" <?php if($put_on_truck  == 3) echo 'selected="selected"'; ?>>Triple</option>	
             </select> </span> 
             </div><div class="row-fluid">            
             <span class="span6">             
              <label class="control-label pull-left">Do you need Physical Damage Coverage? &nbsp;</label>              
              <select name="t_pd_damage[]" onchange="pd_damage(<?php echo  $k; ?>);" id="pd_dam_id<?php echo  $k; ?>" class="span2">		 
              <script>
																 var o=1;
																 $(document).ready(function(){
																	
																     pd_damage(o);
																	 o++;
																 });
																 </script>	
                                                               
                                                               		   <?php $pd_damage = isset($value1->pd_damage) ? $value1->pd_damage : '';?>
                <option value="No" <?php if($pd_damage  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($pd_damage  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>              
              </select>            
              </span>            
              <span class="span6">              
              <label class="control-label pull-left">Do you need Cargo Coverage? &nbsp;</label>              
              <select name="t_cd_damage[]" class="select_new span2" onchange="cd_damage(<?php echo  $k; ?>);" id="cd_dam_id<?php echo  $k; ?>">			 
             <script>
																 var m=1;
																 $(document).ready(function(){
																	 
																     cd_damage(m);
																	m++;
																 });
																 </script>	
                                                            <?php $cd_damage = isset($value1->cd_damage) ? $value1->cd_damage : '';?>
                <option value="No" <?php if($cd_damage  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($cd_damage  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>   
                                                                                 
               </select>            
               </span>           
               </div>
               </div>
               <?php $g=1; foreach($app_veh3 as $value2)  { 
			   
			   if($value2 ->parent_id === $value1 ->vehicle_id){
			    ?>
               
               <input type="hidden" value="<?php echo  isset($value2->vehicle_id) ? $value2->vehicle_id : ''; ?>" name="tr_vehicle_id[]"/>
               <div class="row_added_trailer<?php echo  $k; ?> vehicle_schedule trailer_divs_here">
               <div class="row-fluid ">				
               <div class="span12" style="text-align:center; font-weight:bold; background-color:yellow; color:black;">				TRAILER #<?php echo  $g; ?></div>
               
          
             <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="tr_select_vehicle_year[]" class="select_new">
                <?php $select_vehicle_year= isset($value2->select_vehicle_year) ? $value2->select_vehicle_year : ''; ?>
                 <option value="1985"  <?php if($select_vehicle_year  == '1985') echo 'selected="selected"'; ?>>1985</option>
                 <option value="1986"  <?php if($select_vehicle_year  == '1986') echo 'selected="selected"'; ?>>1986</option>
                                                                <option value="1987"  <?php if($select_vehicle_year  == '1987') echo 'selected="selected"'; ?>>1987</option>
                                                                <option value="1988"  <?php if($select_vehicle_year  == '1988') echo 'selected="selected"'; ?>>1988</option>
                                                                <option value="1989"  <?php if($select_vehicle_year  == '1989') echo 'selected="selected"'; ?>>1989</option>
																<option value="1990"  <?php if($select_vehicle_year  == '1990') echo 'selected="selected"'; ?>>1990</option>
                                                                <option value="1991"  <?php if($select_vehicle_year  == '1991') echo 'selected="selected"'; ?>>1991</option>
                                                                <option value="1992"  <?php if($select_vehicle_year  == '1992') echo 'selected="selected"'; ?>>1992</option>
                                                                <option value="1993"  <?php if($select_vehicle_year  == '1993') echo 'selected="selected"'; ?>>1993</option>
                                                                <option value="1994"  <?php if($select_vehicle_year  == '1994') echo 'selected="selected"'; ?>>1994</option>
                                                                <option value="1995"  <?php if($select_vehicle_year  == '1995') echo 'selected="selected"'; ?>> 1995</option>
                                                                <option value="1996"  <?php if($select_vehicle_year  == '1996') echo 'selected="selected"'; ?>>1996</option>
                                                                <option value="1997"  <?php if($select_vehicle_year  == '1997') echo 'selected="selected"'; ?>>1997</option>
                                                                <option value="1998"  <?php if($select_vehicle_year  == '1998') echo 'selected="selected"'; ?>>1998</option>
                                                                <option value="1999"  <?php if($select_vehicle_year  == '1999') echo 'selected="selected"'; ?>>1999</option>
                                                                <option value="2000"  <?php if($select_vehicle_year  == '2000') echo 'selected="selected"'; ?>>2000</option>
                                                                <option value="2001"  <?php if($select_vehicle_year  == '2001') echo 'selected="selected"'; ?>>2001</option>
                                                                <option value="2002"  <?php if($select_vehicle_year  == '2002') echo 'selected="selected"'; ?>>2002</option>
                                                                <option value="2003"  <?php if($select_vehicle_year  == '2003') echo 'selected="selected"'; ?>>2003</option>
                                                                <option value="2004"  <?php if($select_vehicle_year  == '2004') echo 'selected="selected"'; ?>>2004</option>
                                                                <option value="2005"  <?php if($select_vehicle_year  == '2005') echo 'selected="selected"'; ?>>2005</option>
                                                                <option value="2006"  <?php if($select_vehicle_year  == '2006') echo 'selected="selected"'; ?>>2006</option>
                                                                <option value="2007"  <?php if($select_vehicle_year  == '2007') echo 'selected="selected"'; ?>>2007</option>
                                                                <option value="2008"  <?php if($select_vehicle_year  == '2008') echo 'selected="selected"'; ?>>2008</option>
                                                                <option value="2009"  <?php if($select_vehicle_year  == '2009') echo 'selected="selected"'; ?>>2009</option>
                                                                <option value="2010"  <?php if($select_vehicle_year  == '2010') echo 'selected="selected"'; ?>>2010</option>
                                                                <option value="2011"  <?php if($select_vehicle_year  == '2011') echo 'selected="selected"'; ?>>2011</option>
                                                                <option value="2012"  <?php if($select_vehicle_year  == '2012') echo 'selected="selected"'; ?>>2012</option>
                                                                <option value="2013"  <?php if($select_vehicle_year  == '2013') echo 'selected="selected"'; ?>>2013</option>
																<option value="2014"  <?php if($select_vehicle_year  == '2014') echo 'selected="selected"'; ?>>2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="tr_select_model[]" class="select_new">
            <?php $select_model= isset($value2->select_model) ? $value2->select_model : ''; ?>
                <option value="">Select</option>
				<option value="Freightliner" <?php if($select_model  == 'Freightliner') echo 'selected="selected"'; ?>>Freightliner</option>
                <option value="International" <?php if($select_model  == 'International') echo 'selected="selected"'; ?>>International</option>
                 <option value="Isuzu" <?php if($select_model  == 'Isuzu') echo 'selected="selected"'; ?>>Isuzu</option>
                 <option value="Kenworth" <?php if($select_model  == 'Kenworth') echo 'selected="selected"'; ?>>Kenworth</option>
                 <option value="Mack" <?php if($select_model  == 'Mack') echo 'selected="selected"'; ?>>Mack</option>
                 <option value="Peterbilt" <?php if($select_model  == 'Peterbilt') echo 'selected="selected"'; ?>>Peterbilt</option>
                 <option value="Sterling" <?php if($select_model  == 'Sterling') echo 'selected="selected"'; ?>>Sterling</option>
                 <option value="Western Star" <?php if($select_model  == 'Western Star') echo 'selected="selected"'; ?>>Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="tr_select_GVW[]" class="select_new">
               <?php $select_GVW= isset($value2->select_GVW) ? $value2->select_GVW : ''; ?>
                   <option value="0-10,000 pounds" <?php if($select_model  == '0-10,000 pounds') echo 'selected="selected"'; ?>>0-10,000 pounds</option>
                                                                <option value="10,001-26,000 pounds" <?php if($select_GVW  == '10,001-26,000 pounds') echo 'selected="selected"'; ?>>10,001-26,000 pounds</option>
                                                                <option value="26,001-40,000 pounds" <?php if($select_GVW  == '26,001-40,000 pounds') echo 'selected="selected"'; ?>>26,001-40,000 pounds</option>
                                                                <option value="40,001-80,000 pounds" <?php if($select_GVW  == '40,001-80,000 pounds') echo 'selected="selected"'; ?>>40,001-80,000 pounds</option>
                                                                <option value="Over 80,000 pounds" <?php if($select_GVW  == 'Over 80,000 pounds') echo 'selected="selected"'; ?>>Over 80,000 pounds</option>
             
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value2->select_VIN) ? $value2->select_VIN : ''; ?>" name="tr_select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" value="<?php echo  isset($value2->select_Operation) ? $value2->select_Operation : ''; ?>" placeholder="" name="tr_select_Operation[]">
            </span>
          </div>
		  <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="tr_select_Radius[]" class="select_new">
               <?php $select_Radius= isset($value2->select_Radius) ? $value2->select_Radius : ''; ?>
			    <option value="">Please Select</option>
                 <option value="0-100 miles" <?php if($select_Radius  == '0-100 miles') echo 'selected="selected"'; ?>>0-100 miles</option>
                                                                <option value="101-500 miles" <?php if($select_Radius  == '101-500 miles') echo 'selected="selected"'; ?>>101-500 miles</option>           	
                                                                <option value="501+ miles" <?php if($select_Radius  == '501+ miles') echo 'selected="selected"'; ?>>501+ miles</option>
                                                              	<option value="other" <?php if($select_Radius  == 'other') echo 'selected="selected"'; ?>>Other</option>
            
              </select>
            </span>
            <span class="span2">
              <label class="control-label">States driving</label>
            										
				 <?php
									$attr = "class='span12 ui-state-valid state_driv' id='state_driving'" ;
									$broker_state = isset($value2->state_driving) ? $value2->state_driving : '';
									
									?>
									<?php echo get_state_dropdown('tr_state_driving[]', $broker_state, $attr); ?>
            
            
            </span>
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value2->select_Cities) ? $value2->select_Cities : ''; ?>" name="tr_select_Cities[]">
            </span>

          <span class="span5">              <label class="control-label">Applicants business</label>              
          <select name="tr_select_applicant[]" id="tr_select_App<?php echo $g;?>" onchange="tr_business_app(<?php echo $g;?>,this.value);" class="select_new">    
          
          <script>
                 var e=1;
																 $(document).ready(function(){
																	
																     tr_business_app(e,$('#tr_select_App'+e).val());
																	 e++;
																 });
																 </script>	             
         <?php $select_applicant= isset($value2->select_applicant) ? $value2->select_applicant : ''; ?> 
               <option value="">Please Select</option>		
              <option value="Tow truck driver" <?php if($select_applicant  == 'Tow truck driver') echo 'selected="selected"'; ?>>Tow truck driver</option>
              <option value="Other" <?php if($select_applicant  == 'Other') echo 'selected="selected"'; ?>> Other </option>	
               </select>
           <input class="textinput span5" type="text" style="display:none;" placeholder="Please specify" id="tr_select_app_other<?php echo $g; ?>" value="<?php isset($value2->select_applicant_driver) ? $value2->select_applicant_driver : '';?>" name="tr_select_applicant_driver[]">            
           </span>
           </div>
           <div class="row-fluid">           
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($value2->select_special) ? $value2->select_special : '';?>" name="tr_select_special[]">
            </span>
            <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="tr_select_Axels[]" class="select_new">
               <?php $select_Axels= isset($value2->select_Axels) ? $value2->select_Axels : '';?>
                 <option value="">Please Select</option>
				 <option value="1 Axel" <?php if($select_Axels  == '1 Axel') echo 'selected="selected"'; ?> >1 Axel</option>
				 <option value="2 Axel" <?php if($select_Axels  == '2 Axel') echo 'selected="selected"'; ?> >2 Axel</option>
				 <option value="3 Axel" <?php if($select_Axels  == '3 Axel') echo 'selected="selected"'; ?> >3 Axel</option>
            
              </select>
            </span>
      <!--      <span class="span2">
              <label class="control-label">Number of drivers?&nbsp;</label>
              <input class="textinput input-mini" type="text" value="<?php echo  isset($value2->select_num_drivers) ? $value2->select_num_drivers : '';?>" placeholder="" name="tr_select_num_drivers[]"  pattern="[0-9]+" mAxelngth="2">
            </span>-->
            <span class="span3">
              <label class="control-label"></label>
            </span>
          </div></div>
		  </div>
		  
		  
		  <?php $g++; }  }     ?>
          
          <span id="add_trailer_div<?php echo $k;?>"></span>
		  <span id="add_pd_tractor_div<?php echo $k;?>" style="">
		  <h5 class="heading sh sh-2">Physical Damage</h5>
		  <a class="btn btn-mini" style="" onclick="t_pd_add(<?php  echo $k;?>);" id="t_pd_add_button<?php  echo $k;?>"> <i class="icon icon-plus"></i> Add</a>			                 
           <div class="row-fluid" id="t_pd_add_val<?php  echo $k;?>">   
           
         <?php  $n=1; foreach($app_veh_pd1 as $value3){ 
		
		 if($value3->vehicle_id == $value1->vehicle_id){
		
														  
						 ?>   
                                 <input type="hidden" value="<?php echo  isset($value3->v_id) ? $value3->v_id : ''; ?>" name="t_v_id<?php  echo $k;?>[]"/>
                 <input type="hidden" value="<?php echo  isset($value3->vehicle_id) ? $value3->vehicle_id : ''; ?>" name="t_veh_id<?php  echo $k;?>[]"/>
               <div class="row-fluid" id=<?php echo "row_add_pd".$k?>>                                               
            <span class="span1">             
             <label class="control-label">Rate</label>              
             <select name="t_pd_rate<?php echo $k;?>[]" class="select_new_per">                
              <option value="">select</option>                                                   
               <?php $pd_rate = isset($value3->pd_rate) ? $value3->pd_rate : '';?>
                <option value="5%" <?php if($pd_rate  == '5%') echo 'selected="selected"'; ?> >5%</option>
                 <option value="10%" <?php if($pd_rate  == '10%') echo 'selected="selected"'; ?>>10%</option>     </select>           
                </span>           
                 <span class="span3">              
                 <label class="control-label">Loss payee and full address</label>              
                 <select name="t_pd_loss_payee<?php echo $k;?>[]" class="span6" id="t_pd_loss_payee<?php echo $n ;?>"  onchange="t_loss_payee(<?php echo $n ;?>)">			
                 <?php $pd_loss_payee = isset($value3->pd_loss_payee) ? $value3->pd_loss_payee : '';?>
               <option value="No" <?php if($pd_loss_payee  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($pd_loss_payee  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>   
                                            </select>            
                   </span>	
                                                   
                                                                           <script>
												  var vk=1
												  $(document).ready(function(){
													 
													   t_loss_payee(vk);
													  vk++;
													  });
												  
											
												  </script>		    
                    <span class="span3" id="t_coverage<?php echo $n ;?>">              
                    <label class="control-label">Additional optional coverage</label>              
                    <select name="t_pd_coverage<?php echo $k;?>[]" class="span8 chzn-select-width" multiple onchange="t_pd_coverage1(<?php echo $n ;?>,this.value)" id="t_pd_coverage1<?php echo $n ;?>">			  
                   <?php $pd_coverage = isset($value3->pd_coverage) ? explode(',',$value3->pd_coverage) : '';?>
             
              <?php if(!empty($pd_coverage)) {  ?>
                                                        <option value="Debris removal"  <?php if(in_array('Debris removal',$pd_coverage)) echo 'selected="selected"'; ?> >Debris removal</option>
                                               <option value="Towing & storage" <?php if(in_array('Towing & storage',$pd_coverage )  ) echo 'selected="selected"'; ?>>Towing &amp; storage</option>  
                                               <option value="Tarpaulin coverage" <?php if(in_array('Tarpaulin coverage',$pd_coverage )  ) echo 'selected="selected"'; ?>>Tarpaulin coverage</option>  
                                                <option value="Other" <?php if(in_array('Other',$pd_coverage ) ) echo 'selected="selected"'; ?>>Other</option> 
                                                 <?php } else { ?>
                                                    <option value="Debris removal"  <?php if($pd_coverage  == 'Debris removal') echo 'selected="selected"'; ?> >Debris removal</option>
                                               <option value="Towing & storage" <?php if($pd_coverage  == 'Towing & storage') echo 'selected="selected"'; ?>>Towing &amp; storage</option>  
                                               <option value="Tarpaulin coverage" <?php if($pd_coverage  == 'Tarpaulin coverage') echo 'selected="selected"'; ?>>Tarpaulin coverage</option>  
                                                <option value="Other" <?php if($pd_coverage  == 'Other') echo 'selected="selected"'; ?>>Other</option>
                                            <?php      } ?> 
                                   </select>   
                                   
                                      <script>
												  var b=1
												  $(document).ready(function(){
													
													   t_pd_coverage1(b,$('#t_pd_coverage1'+b).val());
													  b++;
													  });
												  
											
												  </script>   
                                                  <?php     if(in_array('Other',$pd_coverage ) )  { ?>
                                             <input class="textinput  span4" type="text" placeholder="Specify" id="Specify_o1<?php echo $n ;?>"  value="<?php echo  isset($value3->pd_coverage_specify) ? $value3->pd_coverage_specify : '';?>" name="t_pd_coverage_specify<?php echo $k;?>[]">            
                                                    
											<?php  }		?>       
                       
                       </span>			<span class="span2">              
                       <label class="control-label">Premium</label>              
                       <input class="textinput span9" type="text" placeholder="" value="<?php echo  isset($value3->pd_premium) ? $value3->pd_premium : '';?>" name="t_pd_premium<?php echo $k;?>[]"    >           
                        </span>            <span class="span2">             
                         <label class="control-label">Deductible</label>             
                          <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value3->pd_deductible) ? $value3->pd_deductible : '';?>" name="t_pd_deductible<?php echo $k;?>[]">           
                           </span>    
                   <span class="span1"> <label class="control-label">Remove</label><a class="btn btn-mini btn-1   " onclick="t_remove_pd_row1(<?php echo  $k; ?>);" id="remove_pd_row"><i class="icon icon-remove "></i></a></span>              
                           
        <div class="row-fluid" style="" id="t_pd_amount<?php echo k; ?>">            
          <span class="span7">              
          <label class="control-label"></label>              
          <table class="table">               
           <thead>                  
           <tr>                    
           <th></th>                    
           <th>Amount or Limit</th>                   
            <th>Deductible</th>                    
            <th>Premium</th>                 
             </tr>               
              </thead> 
             <?php $pd_amount_limit=  isset($value3->pd_amount_limit) ? explode(',',$value3->pd_amount_limit) : '';?>
          <?php $pd_deductible_limit= isset($value3->pd_deductible_limit) ? explode(',',$value3->pd_deductible_limit) : '';?>
          <?php $pd_premium_limit=  isset($value3->pd_premium_limit) ? explode(',',$value3->pd_premium_limit) : '';?>
          <?php  for ($t=0; $t<count($pd_coverage); $t++)  { ?>               
              <tbody>                  
              <tr>                    
              <td id="t_removal_name<?php echo $k;?>"><?php echo $pd_coverage[$t]; ?></td>                    
              <td>                      
              <input class="textinput input-mini" type="text" placeholder="" value="<?php echo $pd_amount_limit[$t]; ?>" name="t_pd_amount_limit<?php echo $k;?>[]">                   
               </td>                    <td>                     
                <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  $pd_deductible_limit[$t];?>" name="t_pd_deductible_limit<?php echo $k;?>[]">                   
                 </td>                    
                 <td>                      
                 <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  $pd_premium_limit[$t];?>" name="t_pd_premium_limit<?php echo $k;?>[]">                   
                  </td>                  
                  </tr>  
                  <?php } ?>             
                   </tbody>             
                    </table>           
                     </span>           
                      <span class="span5">             
                       <label class="control-label"></label>           
                        </span>          
                        </div>     
                           
                           
                            </div>
                            
        <?php $n++; }  }  ?>
        </div>
		        <label class="control-label control-label-2">Loss Payee Address(es)</label>
          <div class="row-fluid">
            <span class="span3">
              <label class="control-label">Entity name</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->pd_loss_name) ? $value1->pd_loss_name : '';?>" name="t_pd_loss_name[]">
            </span>
            <span class="span3">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->pd_loss_address) ? $value1->pd_loss_address : '';?>" name="t_pd_loss_address[]">
            </span>
            <span class="span1">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder=""  value="<?php echo  isset($value1->pd_loss_city) ? $value1->pd_loss_city : '';?>" name="t_pd_loss_city[]">
            </span>
            <span class="span1">
              <label class="control-label">State</label>
              								<?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = isset($value1->pd_loss_state) ? $value1->pd_loss_state : '';;
									
									?>
									<?php echo get_state_dropdown('t_pd_loss_state[]', $broker_state, $attr); ?>
	</select>
            
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" value="<?php echo  isset($value1->pd_loss_zip) ? $value1->pd_loss_zip : '';?>" placeholder="" name="t_pd_loss_zip[]" pattern="[0-9]+" maxlength="9">
            </span>
            <span class="span3">
              <label class="control-label">Remarks</label>
              <input class="textinput span12" type="text" value="<?php echo  isset($value1->pd_loss_remarks) ? $value1->pd_loss_remarks : '';?>" placeholder="" name="t_pd_loss_remarks[]">
            </span>
          </div>
		 </span>
         
         
         
         
         <span id="add_cd_tractor_div<?php echo $k;?>" style=""><h5 class="heading sh sh-3">Cargo</h5><div class="row-fluid">           
                         <span class="span3" style="display:none;" id="cargo_damages<?php echo $k;?>">             
                          <label class="control-label">Average value per load &nbsp;</label>             
                           <input class="textinput span6 price_format" type="text" placeholder="$" value="<?php echo  isset($value1->cargo_avg_val) ? $value1->cargo_avg_val : '';?>" name="t_cargo_avg_val[]">             
                            <input class="textinput span6" type="text" placeholder="%" value="<?php echo  isset($value1->cargo_avg_per) ? $value1->cargo_avg_per : '';?>" name="t_cargo_avg_per[]">           
                             </span>            
                             <span class="span3" style="margin-left:10px;">              
                             <label class="control-label">Maximum values per load &nbsp;</label>             
                              <input class="textinput span6 price_format" type="text" placeholder="$" value="<?php echo  isset($value1->cargo_max_val) ? $value1->cargo_max_val : '';?>" name="t_cargo_max_val[]">              
                              <input class="textinput span6" type="text" placeholder="%" value="<?php echo  isset($value1->cargo_max_per) ? $value1->cargo_max_per : '';?>" name="t_cargo_max_per[]">           
                               </span>            <span class="span2">             
                               <label class="control-label">% Factor</label>              
                               <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->cargo_factor) ? $value1->cargo_factor : '';?>" name="t_cargo_factor[]">           
                                </span>            <span class="span2">             
                                 <label class="control-label">Premium</label>             
                                  <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->cargo_prem) ? $value1->cargo_prem : '';?>" name="t_cargo_prem[]">            
                                  </span>            
                                  <span class="span2">             
                                   <label class="control-label">Deductible</label>              
                                   <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->cargo_deduct) ? $value1->cargo_deduct : '';?>" name="t_cargo_deduct[]">            
                                   </span>         
                                    </div>  <div class="row-fluid">            
                                    <span class="span6">              
                                    <label class="control-label">Additional optional coverage</label>             
                                     <select name="t_coverage_name[]" class="select-15 chzn-select-width" multiple onchange="t_coverage_name1(<?php echo $k; ?>)" id="t_coverage_name1<?php echo $k;?>">			   
                                    <?php $coverage_name = isset($value1->coverage_name) ? explode(',', $value1->coverage_name) : '';?>		 
                              <option value="">Please Select</option>               
                               <option <?php if(in_array('Earned freight clause',$coverage_name)) echo 'selected="selected"'; ?> value="Earned freight clause">Earned freight clause</option>               
                                <option <?php if(in_array('Refrigeration breakdown deductible',$coverage_name)) echo 'selected="selected"'; ?> value="Refrigeration breakdown deductible">Refrigeration breakdown deductible</option>               
                                 <option <?php if(in_array('Debris removal',$coverage_name)) echo 'selected="selected"'; ?> value="Debris removal">Debris removal</option>               
                                  <option <?php if(in_array('Excess cargo',$coverage_name)) echo 'selected="selected"'; ?> value="Excess cargo">Excess cargo</option> 
                                   <option <?php if(in_array('Other',$coverage_name)) echo 'selected="selected"'; ?> value="Other">Other</option>       </select>          
                                          </span> 
                                          <script>
												  var s=1
												  $(document).ready(function(){
													 
													   //t_coverage_name1(s,$('#t_coverage_name1'+s).val());
													  s++;
													  });
												  
											
												  </script>   
                                     
                                      <?php  $cargo_amount_limit=  isset($value1->cargo_amount_limit) ? explode(',',$value1->cargo_amount_limit) : '';
			 ?>
          <?php $cargo_amount_deduct= isset($value1->cargo_amount_deduct) ? explode(',',$value1->cargo_amount_deduct) : '';?>
          <?php $cargo_amount_premium=  isset($value1->cargo_amount_premium) ? explode(',',$value1->cargo_amount_premium) : '';?>
          <?php  if(!empty($coverage_name)) {  //print_r($coverage_name);?>                
                                          </div></span><span id="t_cargo_dis<?php echo k;?>" > <div class="row-fluid" id="t_cargo_amount<?php echo k;?>">          
                                           <span class="span8">              <table class="table">               
                                            <thead>                  <tr>                    <th></th>                  
                                              <th>Amount or Limit</th>                   
                                               <th>Deductible</th>                    
                                               <th>Premium</th>                  
                                               </tr>               
                                                </thead>               
                                                 <tbody>                 
                                               <?php  for ($t=0; $t<count($coverage_name); $t++)  { ?>  
                                                  <tr>                    
                                                  <td><?php echo  $coverage_name[$t] ; ?></td>                   
             <td>                     
         <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  $cargo_amount_limit[$t];?>" name="t_cargo_amount_limit[]">                  
        </td>                    
         <td>                      
       <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  $cargo_amount_deduct[$t] ;?>" name="t_cargo_amount_deduct[]">                   
      </td>                   
      <td>                      
            <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  $cargo_amount_limit[$t];?>" name="t_cargo_amount_premium[]">                   
                </td>                 
                </tr>               
             <?php } ?>           
                   </tbody>             
                  </table>           
                      </span>       
                     
                    <span class="span4">     
                     <label class="control-label"></label>           
                 </span>          
                  </div></span>
     <?php } ?>   
															   
	<span id="add_drivers_tractor_div<?php echo $k;?>" style="display:block;">
      <h5 class="heading sh sh-5">Drivers Schedule</h5>
          
          <table class="table">
            <thead>
              <tr>
                <th>First name</th>
                <th>Middle name</th>
                <th>Last name</th>
                <th>Driver License
                  <br>Number</th>
                <th>License class</th>
                <th>Date hired</th>
                <th>How many years
                  <br>Class A license</th>
                <th>License issue state</th>
                <th>Remarks</th>
                <th>Surcharge</th>
              </tr>
            </thead>
            <tbody id="<?php echo $k; ?>t_add_drivered_row1">
                
    <?php 


if (!empty($app_driver)) { ?> 	
                <?php   foreach ($app_driver as $drivers) {
					  if ($drivers->vehicle_id == $value1->vehicle_id) {
					
					 ?>
       <input type="hidden" value="<?php echo  isset($drivers->vehicle_id) ? $drivers->vehicle_id : ''; ?>" name="<?php echo $k; ?>t_dri_id[]"/>
      <input type="hidden" value="<?php echo (isset($drivers->driver_id) ? $drivers->driver_id : ''); ?>" name="<?php echo $k; ?>t_driver_id[]"  />
              <tr>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_fname)){ echo (isset($drivers->driver_fname) ? $drivers->driver_fname : ''); }else{ echo (isset($drivers->driver_name) ? $drivers->driver_name : ''); } ?>" name="<?php echo $k; ?>t_driver_fname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder=""  value="<?php if(!empty($drivers->driver_mname)){ echo (isset($drivers->driver_mname) ? $drivers->driver_mname : ''); }else{ echo (isset($drivers->driver_middle_name) ? $drivers->driver_middle_name : ''); } ?>" name="<?php echo $k; ?>t_driver_mname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_lname)){ echo (isset($drivers->driver_lname) ? $drivers->driver_lname : ''); }else{ echo (isset($drivers->driver_last_name) ? $drivers->driver_last_name : ''); } ?>" name="<?php echo $k; ?>t_driver_lname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_license_num)){ echo (isset($drivers->driver_license_num) ? $drivers->driver_license_num : ''); }else{ echo (isset($drivers->driver_licence_no) ? $drivers->driver_licence_no : ''); } ?>" name="<?php echo $k; ?>t_driver_license_num[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_license_class)){ echo (isset($drivers->driver_license_class) ? $drivers->driver_license_class : ''); }else{ echo (isset($drivers->driver_license_class) ? $drivers->driver_license_class : ''); } ?>" name="<?php echo $k; ?>t_driver_license_class[]">
                </td>
                <td>
                  <input class="textinput input-mini datepick" type="text" value="<?php if(!empty($drivers->driver_date_hired)){ echo (isset($drivers->driver_date_hired) ? $drivers->driver_date_hired : ''); }else{ echo (isset($drivers->driver_license_issue_date) ? date('m/d/Y', strtotime($drivers->driver_license_issue_date)) : ''); } ?>" required="required" placeholder="" name="<?php echo $k; ?>t_driver_date_hired[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" value="<?php if(!empty($drivers->driver_class_A)){  echo (isset($drivers->driver_class_A) ? $drivers->driver_class_A : ''); }else{ echo (isset($drivers->driver_class_a_years) ? $drivers->driver_class_a_years : ''); } ?>" placeholder="" name="<?php echo $k; ?>t_driver_class_A[]">
                </td>
                <td>
                   <?php
									$attr = "class='span12 width_td ui-state-valid'";
									$broker_state =   isset($drivers->driver_state) ? $drivers->driver_state : $drivers->driver_license_issue_state;
									
									?>
									<?php echo get_state_dropdown($k.'t_driver_state[]', $broker_state, $attr); ?>
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_remarks)){ echo  (isset($drivers->driver_remarks) ? $drivers->driver_remarks : ''); } else{ isset($drivers->driver_remarks) ? $drivers->driver_remarks : ''; } ?>" name="<?php echo $k; ?>t_driver_remarks[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_surcharge)){ echo (isset($drivers->driver_surcharge) ? $drivers->driver_surcharge : ''); } else{ isset($drivers->driver_surcharge) ? $drivers->driver_surcharge : ''; } ?>" name="<?php echo $k; ?>t_driver_surcharge[]">
                </td>
              </tr>
           <?php } } } ?>
            </tbody>
           </table>
		  </span></div>
                  
           
		  
		  <?php $k++; } } else { ?>
			  
			  
<?php $k=1; foreach($app_veh22 as $value1) { ?>
      <!--   <input type="hidden" value="<?php echo  isset($value1->vehicle_id) ? $value1->vehicle_id : ''; ?>" name="t_vehicle_id[]"/>-->
        <div class="row_added_tractor vehicle_schedule"><div class="div-1">    
                <div class="row-fluid btn-success" style="padding: 5px 0px 5px 0px;">  
                            <span class="span4">                
                            <label class="control-label control-label-2" style="font-weight:bold;font-size:15px; margin-left:10px;margin-top: 5px;">TRACTOR #<?php echo $k; ?></label>         
                                 </span>              
                                 <span class="span8" style="margin-left: 20px;">              
   <a class="btn pull-right btn_sp" onclick="drivers_tractor(<?php echo $k; ?>,&quot;yes&quot;);" id="t_drivers_button<?php echo $k; ?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Drivers</a>               
    <a class="btn pull-right btn_sp" onclick="trailer_tractor(<?php echo $k; ?>,&quot;no&quot;);" id="t_trailer_button<?php echo $k; ?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Trailers</a>                
    <a class="btn pull-right btn_sp" onclick="cargo_tractor(<?php echo $k; ?>,&quot;no&quot;);" id="t_cargo_button<?php echo $k; ?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Cargo</a>                
    <a class="btn pull-right btn_sp" onclick="pd_tractor(<?php echo $k; ?>,&quot;no&quot;);" id="t_pd_button<?php echo $k; ?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Physical Damage</a>               
     <a class="btn pull-right btn-1 btn-2" onclick="liability_tractor(<?php echo $k; ?>,&quot;no&quot;);" id="t_liability_button<?php echo $k; ?>" style="background: rgb(214, 214, 214);"> <i class="icon icon-eye-open"></i> Liability</a>              
     </span>            </div>
           <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="t_select_vehicle_year[]" class="select_new">
                 <?php echo  $t_select_vehicle_year= isset($value1->vehicle_year) ? $value1->vehicle_year : '' ?>
                 <option value="1985"  <?php if($t_select_vehicle_year  == '1985') echo 'selected="selected"'; ?>>1985</option>
                 <option value="1986"  <?php if($t_select_vehicle_year  == '1986') echo 'selected="selected"'; ?>>1986</option>
                                                                <option value="1987"  <?php if($t_select_vehicle_year  == '1987') echo 'selected="selected"'; ?>>1987</option>
                                                                <option value="1988"  <?php if($t_select_vehicle_year  == '1988') echo 'selected="selected"'; ?>>1988</option>
                                                                <option value="1989"  <?php if($t_select_vehicle_year  == '1989') echo 'selected="selected"'; ?>>1989</option>
																<option value="1990"  <?php if($t_select_vehicle_year  == '1990') echo 'selected="selected"'; ?>>1990</option>
                                                                <option value="1991"  <?php if($t_select_vehicle_year  == '1991') echo 'selected="selected"'; ?>>1991</option>
                                                                <option value="1992"  <?php if($t_select_vehicle_year  == '1992') echo 'selected="selected"'; ?>>1992</option>
                                                                <option value="1993"  <?php if($t_select_vehicle_year  == '1993') echo 'selected="selected"'; ?>>1993</option>
                                                                <option value="1994"  <?php if($t_select_vehicle_year  == '1994') echo 'selected="selected"'; ?>>1994</option>
                                                                <option value="1995"  <?php if($t_select_vehicle_year  == '1995') echo 'selected="selected"'; ?>> 1995</option>
                                                                <option value="1996"  <?php if($t_select_vehicle_year  == '1996') echo 'selected="selected"'; ?>>1996</option>
                                                                <option value="1997"  <?php if($t_select_vehicle_year  == '1997') echo 'selected="selected"'; ?>>1997</option>
                                                                <option value="1998"  <?php if($t_select_vehicle_year  == '1998') echo 'selected="selected"'; ?>>1998</option>
                                                                <option value="1999"  <?php if($t_select_vehicle_year  == '1999') echo 'selected="selected"'; ?>>1999</option>
                                                                <option value="2000"  <?php if($t_select_vehicle_year  == '2000') echo 'selected="selected"'; ?>>2000</option>
                                                                <option value="2001"  <?php if($t_select_vehicle_year  == '2001') echo 'selected="selected"'; ?>>2001</option>
                                                                <option value="2002"  <?php if($t_select_vehicle_year  == '2002') echo 'selected="selected"'; ?>>2002</option>
                                                                <option value="2003"  <?php if($t_select_vehicle_year  == '2003') echo 'selected="selected"'; ?>>2003</option>
                                                                <option value="2004"  <?php if($t_select_vehicle_year  == '2004') echo 'selected="selected"'; ?>>2004</option>
                                                                <option value="2005"  <?php if($t_select_vehicle_year  == '2005') echo 'selected="selected"'; ?>>2005</option>
                                                                <option value="2006"  <?php if($t_select_vehicle_year  == '2006') echo 'selected="selected"'; ?>>2006</option>
                                                                <option value="2007"  <?php if($t_select_vehicle_year  == '2007') echo 'selected="selected"'; ?>>2007</option>
                                                                <option value="2008"  <?php if($t_select_vehicle_year  == '2008') echo 'selected="selected"'; ?>>2008</option>
                                                                <option value="2009"  <?php if($t_select_vehicle_year  == '2009') echo 'selected="selected"'; ?>>2009</option>
                                                                <option value="2010"  <?php if($t_select_vehicle_year  == '2010') echo 'selected="selected"'; ?>>2010</option>
                                                                <option value="2011"  <?php if($t_select_vehicle_year  == '2011') echo 'selected="selected"'; ?>>2011</option>
                                                                <option value="2012"  <?php if($t_select_vehicle_year  == '2012') echo 'selected="selected"'; ?>>2012</option>
                                                                <option value="2013"  <?php if($t_select_vehicle_year  == '2013') echo 'selected="selected"'; ?>>2013</option>
																<option value="2014"  <?php if($t_select_vehicle_year  == '2014') echo 'selected="selected"'; ?>>2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="t_select_model[]" class="select_new">
                <?php $select_model= isset($value1->make_model) ? $value1->make_model : ''; ?>
                <option value="">Select</option>
				<option value="Freightliner" <?php if($select_model  == 'Freightliner') echo 'selected="selected"'; ?>>Freightliner</option>
                <option value="International" <?php if($select_model  == 'International') echo 'selected="selected"'; ?>>International</option>
                 <option value="Isuzu" <?php if($select_model  == 'Isuzu') echo 'selected="selected"'; ?>>Isuzu</option>
                 <option value="Kenworth" <?php if($select_model  == 'Kenworth') echo 'selected="selected"'; ?>>Kenworth</option>
                 <option value="Mack" <?php if($select_model  == 'Mack') echo 'selected="selected"'; ?>>Mack</option>
                 <option value="Peterbilt" <?php if($select_model  == 'Peterbilt') echo 'selected="selected"'; ?>>Peterbilt</option>
                 <option value="Sterling" <?php if($select_model  == 'Sterling') echo 'selected="selected"'; ?>>Sterling</option>
                 <option value="Western Star" <?php if($select_model  == 'Western Star') echo 'selected="selected"'; ?>>Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="t_select_GVW[]" class="select_new">
                   <?php $select_GVW= isset($value1->gvw) ? $value1->gvw : ''; ?>
                   <option value="0-10,000 pounds" <?php if($select_model  == '0-10,000 pounds') echo 'selected="selected"'; ?>>0-10,000 pounds</option>
                                                                <option value="10,001-26,000 pounds" <?php if($select_GVW  == '10,001-26,000 pounds') echo 'selected="selected"'; ?>>10,001-26,000 pounds</option>
                                                                <option value="26,001-40,000 pounds" <?php if($select_GVW  == '26,001-40,000 pounds') echo 'selected="selected"'; ?>>26,001-40,000 pounds</option>
                                                                <option value="40,001-80,000 pounds" <?php if($select_GVW  == '40,001-80,000 pounds') echo 'selected="selected"'; ?>>40,001-80,000 pounds</option>
                                                                <option value="Over 80,000 pounds" <?php if($select_GVW  == 'Over 80,000 pounds') echo 'selected="selected"'; ?>>Over 80,000 pounds</option>
              </select>
            </span>
             <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->vin) ? $value1->vin : ''; ?>" name="t_select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->select_Operation) ? $value1->select_Operation : ''; ?>" name="t_select_Operation[]">
            </span>
          </div>
		  
              </div>
			  <div style="" class="row-fluid" id="c_damage_tractor<?php echo $k; ?>">
            	 <div style="" id="c_damage_truck"><div class="row-fluid">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="t_select_Radius[]" id="1select_Radius" class="select_new check_Radius">
               <?php $select_Radius= isset($value1->radius_of_operation) ? $value1->radius_of_operation : ''; ?>
			    <option value="">Please Select</option>
                 <option value="0-100 miles" <?php if($select_Radius  == '0-100 miles') echo 'selected="selected"'; ?>>0-100 miles</option>
                                                                <option value="101-500 miles" <?php if($select_Radius  == '101-500 miles') echo 'selected="selected"'; ?>>101-500 miles</option>           	
                                                                <option value="501+ miles" <?php if($select_Radius  == '501+ miles') echo 'selected="selected"'; ?>>501+ miles</option>
                                                              	<option value="other" <?php if($select_Radius  == 'other') echo 'selected="selected"'; ?>>Other</option>
              </select>
            </span>
			
            <span class="span2">
              <label class="control-label">States driving</label>
            									 <?php
									$attr = "class='span12 ui-state-valid state_driv' id='state_driving'" ;
									$broker_state = isset($value1->state_driving) ? $value1->state_driving : '';
									
									?>
									<?php echo get_state_dropdown('t_state_driving[]', $broker_state, $attr); ?>
            
            </span>
			
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12 check_Cities" type="text" id="1select_Cities" value="<?php echo  isset($value1->select_Cities) ? $value1->select_Cities : ''; ?>" placeholder="" name="t_select_Cities[]">
            </span>
       
               <span class="span5">              
			   <label class="control-label">Applicants business</label>    
			   <select name="t_select_applicant[]" id="t_select_App<?php echo $k;?>" onchange="t_business_app(<?php echo $k;?>,this.value);" class="select_new t_check_App">		
               	<script>
                 var w=1;
																 $(document).ready(function(){
																	
																     t_business_app(w,$('#t_select_App'+w).val());
																	 w++;
																 });
																 </script>	  
			  
 <?php $select_applicant= isset($value1->select_applicant) ? $value1->select_applicant : ''; ?>			   
			  <option value="">Please Select</option>		
                                  	  <option value="Tow truck driver" <?php if($select_applicant  == 'Tow truck driver') echo 'selected="selected"'; ?>>Tow truck driver</option>
                                  			  <option value="Other" <?php if($select_applicant  == 'Other') echo 'selected="selected"'; ?>> Other </option>	         
			   </select>              
			   <input class="textinput span5 t_check_app_other" value="<?php isset($value1->select_applicant_driver) ? $value1->select_applicant_driver : '';?>" type="text" id="t_select_app_other1" style="display:none;" placeholder="Please specify" name="t_select_applicant_driver[]">            
			   </span>  
			   <div class="row-fluid" id="t_checkbox_id1" style="display:none;">            
			   <span class="span4">              
			   <label class="checkbox">                
			   <input type="checkbox" value="Yes" onclick="t_select_ch_rad1();"  class="checkbox_radius" id="t_select_ch_radius1" name="t_select_ch_radius[]">                
			   <span>Same Radius of Operation for all vehicles</span>              
			   </label>            </span>            
			   <span class="span3">             
			   <label class="checkbox">                
			   <input type="checkbox" value="Yes" onclick="t_select_ch_cit1();" class="checkbox_cities" id="t_select_ch_cities1" name="t_select_ch_cities[]">                
			   <span>Same Cities for all vehicles</span>              
			   </label>            
			   </span>            
			   <span class="span5">              
			   <label class="checkbox">                
			   <input type="checkbox" value="Yes" onclick="t_select_ch_app1();" class="checkbox_applicant" id="t_select_ch_applicant1" name="t_select_ch_applicant[]">                
			   <span>Same Applicants Business for all vehicles</span>              
			   </label>            
			   </span>         
			   </div>
		   <div class="row-fluid">
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" value="<?php echo isset($value1->request_for_radius_of_operation) ? $value1->request_for_radius_of_operation : '';?>" placeholder="" name="t_select_special[]">
            </span>
            <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="t_select_Axels[]" class="select_new">
              <?php  $select_Axels= isset($value1->number_of_axles) ? $value1->number_of_axles : '';?>
                 <option value="">Please Select</option>
				 <option value="1 Axel" <?php if($select_Axels  == '1 Axle') echo 'selected="selected"'; ?> >1 Axel</option>
				 <option value="2 Axel" <?php if($select_Axels  == '2 Axle') echo 'selected="selected"'; ?> >2 Axel</option>
				 <option value="3 Axel" <?php if($select_Axels  == '3 Axle') echo 'selected="selected"'; ?> >3 Axel</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">Number of drivers?&nbsp;</label>
               <input class="span4" pattern="[0-9]+" type="text" value="<?php echo  isset($value->select_num_drivers) ? $value->select_num_drivers : '';?>" placeholder="" onchange="t_add_driver(this.value,<?php echo $k;?>)" name="t_select_num_drivers[]" mAxelngth="2">
            </span>
            <span class="span3">
              <label class="control-label"></label>
            </span>
			</div>
          <div class="row-fluid">            
          <span class="span12">              
          <label class="control-label pull-left">Do you pull equipment with this vehicle? &nbsp;</label>              
          <select name="t_select_equipment[]" id="t_select_equipment" class="select_new span1 ui-state-valid" onchange="t_equipment_pull(<?php echo $k; ?>,this.value);">           
          <?php $select_equipment = isset($value1->select_equipment) ? $value1->select_equipment : '';?>
				<option value="Yes" <?php if($select_equipment  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>											  
                <option value="No" <?php if($select_equipment  == 'No') echo 'selected="selected"'; ?> >No</option>
                 	
                                                             	 </select>			  
                                                                  			   </select>
                                                                                   <script>
																 var z=1;
																 $(document).ready(function(){
																	
																     t_equipment_pull(z,$('#t_select_equipment').val());
																	 z++;
																 });
																 </script>	
           <select type="text" name="t_put_on_truck[]" id="vehicle_trailer<?php echo $k; ?>" style="" onchange="add_trailer1(this.value,<?php echo $k; ?> );" class="span2 ui-state-valid">			  
         <?php  $put_on_truck =     count($app_veh32);
		 
		 
		 ?>
           <option value="0" <?php if($put_on_truck  == 0) echo 'selected="selected"'; ?>>None</option>			  
           <option value="1" <?php if($put_on_truck  == 1) echo 'selected="selected"'; ?>>Single</option>				 
            <option value="2" <?php if($put_on_truck  == 2) echo 'selected="selected"'; ?>>Double</option>				  
            <option value="3" <?php if($put_on_truck  == 3) echo 'selected="selected"'; ?>>Triple</option>				 
             </select> </span> 
             </div><div class="row-fluid">            
             <span class="span6">             
              <label class="control-label pull-left">Do you need Physical Damage Coverage? &nbsp;</label>              
              <select name="t_pd_damage[]" onchange="pd_damage(<?php echo  $k; ?>);" id="pd_dam_id<?php echo  $k; ?>" class="span2">		 
              <script>
																 var o=1;
																 $(document).ready(function(){
																	
																     pd_damage(o);
																	 o++;
																 });
																 </script>	
                                                               
                                                               		   <?php $pd_damage = isset($value1->pd_damage) ? $value1->pd_damage : '';?>
                <option value="No" <?php if($pd_damage  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($pd_damage  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>              
              </select>            
              </span>            
              <span class="span6">              
              <label class="control-label pull-left">Do you need Cargo Coverage? &nbsp;</label>              
              <select name="t_cd_damage[]" class="select_new span2" onchange="cd_damage(<?php echo  $k; ?>);" id="cd_dam_id<?php echo  $k; ?>">			 
             <script>
																 var m=1;
																 $(document).ready(function(){
																	 
																     cd_damage(m);
																	m++;
																 });
																 </script>	
                                                            <?php $cd_damage = isset($value1->cd_damage) ? $value1->cd_damage : '';?>
                <option value="No" <?php if($cd_damage  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($cd_damage  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>   
                                                                                 
               </select>            
               </span>           
               </div>
               </div>
               <?php $g=1; foreach($app_veh32 as $value2)  { 
			   
			   if($value2 ->parent_id === $value1 ->vehicle_id){
			    ?>
               
           <!--    <input type="hidden" value="<?php echo  isset($value2->vehicle_id) ? $value2->vehicle_id : ''; ?>" name="tr_vehicle_id[]"/>-->
               <div class="row_added_trailer<?php echo  $k; ?> vehicle_schedule trailer_divs_here">
               <div class="row-fluid ">				
               <div class="span12" style="text-align:center; font-weight:bold; background-color:yellow; color:black;">				TRAILER #<?php echo  $g; ?></div>
               
          
             <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="tr_select_vehicle_year[]" class="select_new">
                <?php $select_vehicle_year= isset($value2->vehicle_year) ? $value2->vehicle_year : ''; ?>
                 <option value="1985"  <?php if($select_vehicle_year  == '1985') echo 'selected="selected"'; ?>>1985</option>
                 <option value="1986"  <?php if($select_vehicle_year  == '1986') echo 'selected="selected"'; ?>>1986</option>
                                                                <option value="1987"  <?php if($select_vehicle_year  == '1987') echo 'selected="selected"'; ?>>1987</option>
                                                                <option value="1988"  <?php if($select_vehicle_year  == '1988') echo 'selected="selected"'; ?>>1988</option>
                                                                <option value="1989"  <?php if($select_vehicle_year  == '1989') echo 'selected="selected"'; ?>>1989</option>
																<option value="1990"  <?php if($select_vehicle_year  == '1990') echo 'selected="selected"'; ?>>1990</option>
                                                                <option value="1991"  <?php if($select_vehicle_year  == '1991') echo 'selected="selected"'; ?>>1991</option>
                                                                <option value="1992"  <?php if($select_vehicle_year  == '1992') echo 'selected="selected"'; ?>>1992</option>
                                                                <option value="1993"  <?php if($select_vehicle_year  == '1993') echo 'selected="selected"'; ?>>1993</option>
                                                                <option value="1994"  <?php if($select_vehicle_year  == '1994') echo 'selected="selected"'; ?>>1994</option>
                                                                <option value="1995"  <?php if($select_vehicle_year  == '1995') echo 'selected="selected"'; ?>> 1995</option>
                                                                <option value="1996"  <?php if($select_vehicle_year  == '1996') echo 'selected="selected"'; ?>>1996</option>
                                                                <option value="1997"  <?php if($select_vehicle_year  == '1997') echo 'selected="selected"'; ?>>1997</option>
                                                                <option value="1998"  <?php if($select_vehicle_year  == '1998') echo 'selected="selected"'; ?>>1998</option>
                                                                <option value="1999"  <?php if($select_vehicle_year  == '1999') echo 'selected="selected"'; ?>>1999</option>
                                                                <option value="2000"  <?php if($select_vehicle_year  == '2000') echo 'selected="selected"'; ?>>2000</option>
                                                                <option value="2001"  <?php if($select_vehicle_year  == '2001') echo 'selected="selected"'; ?>>2001</option>
                                                                <option value="2002"  <?php if($select_vehicle_year  == '2002') echo 'selected="selected"'; ?>>2002</option>
                                                                <option value="2003"  <?php if($select_vehicle_year  == '2003') echo 'selected="selected"'; ?>>2003</option>
                                                                <option value="2004"  <?php if($select_vehicle_year  == '2004') echo 'selected="selected"'; ?>>2004</option>
                                                                <option value="2005"  <?php if($select_vehicle_year  == '2005') echo 'selected="selected"'; ?>>2005</option>
                                                                <option value="2006"  <?php if($select_vehicle_year  == '2006') echo 'selected="selected"'; ?>>2006</option>
                                                                <option value="2007"  <?php if($select_vehicle_year  == '2007') echo 'selected="selected"'; ?>>2007</option>
                                                                <option value="2008"  <?php if($select_vehicle_year  == '2008') echo 'selected="selected"'; ?>>2008</option>
                                                                <option value="2009"  <?php if($select_vehicle_year  == '2009') echo 'selected="selected"'; ?>>2009</option>
                                                                <option value="2010"  <?php if($select_vehicle_year  == '2010') echo 'selected="selected"'; ?>>2010</option>
                                                                <option value="2011"  <?php if($select_vehicle_year  == '2011') echo 'selected="selected"'; ?>>2011</option>
                                                                <option value="2012"  <?php if($select_vehicle_year  == '2012') echo 'selected="selected"'; ?>>2012</option>
                                                                <option value="2013"  <?php if($select_vehicle_year  == '2013') echo 'selected="selected"'; ?>>2013</option>
																<option value="2014"  <?php if($select_vehicle_year  == '2014') echo 'selected="selected"'; ?>>2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="tr_select_model[]" class="select_new">
            <?php $select_model= isset($value2->make_model) ? $value2->make_model : ''; ?>
                <option value="">Select</option>
				<option value="Freightliner" <?php if($select_model  == 'Freightliner') echo 'selected="selected"'; ?>>Freightliner</option>
                <option value="International" <?php if($select_model  == 'International') echo 'selected="selected"'; ?>>International</option>
                 <option value="Isuzu" <?php if($select_model  == 'Isuzu') echo 'selected="selected"'; ?>>Isuzu</option>
                 <option value="Kenworth" <?php if($select_model  == 'Kenworth') echo 'selected="selected"'; ?>>Kenworth</option>
                 <option value="Mack" <?php if($select_model  == 'Mack') echo 'selected="selected"'; ?>>Mack</option>
                 <option value="Peterbilt" <?php if($select_model  == 'Peterbilt') echo 'selected="selected"'; ?>>Peterbilt</option>
                 <option value="Sterling" <?php if($select_model  == 'Sterling') echo 'selected="selected"'; ?>>Sterling</option>
                 <option value="Western Star" <?php if($select_model  == 'Western Star') echo 'selected="selected"'; ?>>Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="tr_select_GVW[]" class="select_new">
               <?php $select_GVW= isset($value2->gvw) ? $value2->gvw : ''; ?>
                   <option value="0-10,000 pounds" <?php if($select_model  == '0-10,000 pounds') echo 'selected="selected"'; ?>>0-10,000 pounds</option>
                                                                <option value="10,001-26,000 pounds" <?php if($select_GVW  == '10,001-26,000 pounds') echo 'selected="selected"'; ?>>10,001-26,000 pounds</option>
                                                                <option value="26,001-40,000 pounds" <?php if($select_GVW  == '26,001-40,000 pounds') echo 'selected="selected"'; ?>>26,001-40,000 pounds</option>
                                                                <option value="40,001-80,000 pounds" <?php if($select_GVW  == '40,001-80,000 pounds') echo 'selected="selected"'; ?>>40,001-80,000 pounds</option>
                                                                <option value="Over 80,000 pounds" <?php if($select_GVW  == 'Over 80,000 pounds') echo 'selected="selected"'; ?>>Over 80,000 pounds</option>
             
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value2->vin) ? $value2->vin : ''; ?>" name="tr_select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" value="<?php echo  isset($value2->select_Operation) ? $value2->select_Operation : ''; ?>" placeholder="" name="tr_select_Operation[]">
            </span>
          </div>
		  <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="tr_select_Radius[]" class="select_new">
               <?php $select_Radius= isset($value2->radius_of_operation) ? $value2->radius_of_operation : ''; ?>
			    <option value="">Please Select</option>
                 <option value="0-100 miles" <?php if($select_Radius  == '0-100 miles') echo 'selected="selected"'; ?>>0-100 miles</option>
                                                                <option value="101-500 miles" <?php if($select_Radius  == '101-500 miles') echo 'selected="selected"'; ?>>101-500 miles</option>           	
                                                                <option value="501+ miles" <?php if($select_Radius  == '501+ miles') echo 'selected="selected"'; ?>>501+ miles</option>
                                                              	<option value="other" <?php if($select_Radius  == 'other') echo 'selected="selected"'; ?>>Other</option>
            
              </select>
            </span>
            <span class="span2">
              <label class="control-label">States driving</label>
            										
				 <?php
									$attr = "class='span12 ui-state-valid state_driv' id='state_driving'" ;
									$broker_state = isset($value2->state_driving) ? $value2->state_driving : '';
									
									?>
									<?php echo get_state_dropdown('tr_state_driving[]', $broker_state, $attr); ?>
            
            
            </span>
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value2->select_Cities) ? $value2->select_Cities : ''; ?>" name="tr_select_Cities[]">
            </span>

          <span class="span5">              <label class="control-label">Applicants business</label>              
          <select name="tr_select_applicant[]" id="tr_select_App<?php echo $g;?>" onchange="tr_business_app(<?php echo $g;?>,this.value);" class="select_new">    
          
          <script>
                 var e=1;
																 $(document).ready(function(){
																	
																     tr_business_app(e,$('#tr_select_App'+e).val());
																	 e++;
																 });
																 </script>	             
         <?php $select_applicant= isset($value2->select_applicant) ? $value2->select_applicant : ''; ?> 
               <option value="">Please Select</option>		
              <option value="Tow truck driver" <?php if($select_applicant  == 'Tow truck driver') echo 'selected="selected"'; ?>>Tow truck driver</option>
              <option value="Other" <?php if($select_applicant  == 'Other') echo 'selected="selected"'; ?>> Other </option>	
               </select>
           <input class="textinput span5" type="text" style="display:none;" placeholder="Please specify" id="tr_select_app_other<?php echo $g; ?>" value="<?php isset($value2->select_applicant_driver) ? $value2->select_applicant_driver : '';?>" name="tr_select_applicant_driver[]">            
           </span>
           </div>
           <div class="row-fluid">           
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($value2->value_amount_vh) ? $value2->value_amount_vh : '';?>" name="tr_select_special[]">
            </span>
         <!--   <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="tr_select_Axels[]" class="select_new">
               <?php $select_Axels= isset($value2->number_of_Axels) ? $value2->number_of_Axels : '';?>
                 <option value="">Please Select</option>
				 <option value="1 Axel" <?php if($select_Axels  == '1 Axel') echo 'selected="selected"'; ?> >1 Axel</option>
				 <option value="2 Axel" <?php if($select_Axels  == '2 Axel') echo 'selected="selected"'; ?> >2 Axel</option>
				 <option value="3 Axel" <?php if($select_Axels  == '3 Axel') echo 'selected="selected"'; ?> >3 Axel</option>
            
              </select>
            </span>-->
    <!--        <span class="span2">
              <label class="control-label">Number of drivers?&nbsp;</label>
              <input class="textinput input-mini" type="text" value="<?php echo  isset($value2->select_num_drivers) ? $value2->select_num_drivers : '';?>" placeholder="" name="tr_select_num_drivers[]"  pattern="[0-9]+" mAxelngth="2">
            </span>-->
            <span class="span3">
              <label class="control-label"></label>
            </span>
          </div></div>
		  </div>
		  
		  
		  <?php $g++; }  }     ?>
          
          <span id="add_trailer_div<?php echo $k;?>"></span>
		  <span id="add_pd_tractor_div<?php echo $k;?>" style="">
		  <h5 class="heading sh sh-2">Physical Damage</h5>
		  <a class="btn btn-mini" style="" onclick="t_pd_add(<?php  echo $k;?>);" id="t_pd_add_button<?php  echo $k;?>"> <i class="icon icon-plus"></i> Add</a>			                 
           <div class="row-fluid" id="t_pd_add_val<?php  echo $k;?>">   
           
         <?php  $n=1; foreach($app_veh_pd1 as $value3){ 
		
		 if($value3->vehicle_id == $value1->vehicle_id){
															  
						 ?>   
        
              <input type="hidden" value="<?php echo  isset($value3->v_id) ? $value3->v_id : ''; ?>" name="t_v_id<?php  echo $k;?>[]"/>
               <input type="hidden" value="<?php echo  isset($value3->vehicle_id) ? $value3->vehicle_id : ''; ?>" name="t_veh_id<?php  echo $k;?>[]"/>
               <div class="row-fluid" id=<?php echo "row_add_pd".$k?>>                                               
            <span class="span1">             
             <label class="control-label">Rate</label>              
             <select name="t_pd_rate<?php echo $k;?>[]" class="select_new_per">                
               <option value="">select</option>                                                  
                                                      <?php $pd_rate = isset($value3->pd_rate) ? $value3->pd_rate : '';?>
                <option value="5%" <?php if($pd_rate  == '5%') echo 'selected="selected"'; ?> >5%</option>
                 <option value="10%" <?php if($pd_rate  == '10%') echo 'selected="selected"'; ?>>10%</option>     </select>           
                </span>           
                 <span class="span3">              
                 <label class="control-label">Loss payee and full address</label>              
                 <select name="t_pd_loss_payee<?php echo $k;?>[]" class="span6" id="t_pd_loss_payee<?php echo $n ;?>"  onchange="t_loss_payee(<?php echo $n ;?>)">			
                 <?php $pd_loss_payee = isset($value3->pd_loss_payee) ? $value3->pd_loss_payee : '';?>
               <option value="No" <?php if($pd_loss_payee  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($pd_loss_payee  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>   
                                            </select>            
                   </span>			  
                                  
                                                                           <script>
												  var vk=1
												  $(document).ready(function(){
													 
													   t_loss_payee(vk);
													  vk++;
													  });
												  
											
												  </script>	  
                    <span class="span3" id="t_coverage<?php echo $n ;?>">              
                    <label class="control-label">Additional optional coverage</label>              
                    <select name="t_pd_coverage<?php echo $k;?>[]" class="span8" onchange="t_pd_coverage1(<?php echo $n ;?>,this.value)" id="t_pd_coverage1<?php echo $n ;?>">			  
                   <?php $pd_coverage = isset($value3->pd_coverage) ? $value3->pd_coverage : '';?>
         
               <option value="Debris removal"  <?php if($pd_coverage  == 'Debris removal') echo 'selected="selected"'; ?> >Debris removal</option>
         <option value="Towing &amp; storage" <?php if($pd_coverage  == 'Towing &amp; storage') echo 'selected="selected"'; ?>>Towing &amp; storage</option>  
        <option value="Tarpaulin coverage" <?php if($pd_coverage  == 'Tarpaulin coverage') echo 'selected="selected"'; ?>>Tarpaulin coverage</option>  
           <option value="Other" <?php if($pd_coverage  == 'Other') echo 'selected="selected"'; ?>>Other</option> 
                                   </select>   
                                   
                                      <script>
												  var b=1
												  $(document).ready(function(){
													
													   t_pd_coverage1(b,$('#t_pd_coverage1'+b).val());
													  b++;
													  });
												  
											
												  </script>          
                       <input class="textinput  span4" type="text" placeholder="Specify" id="Specify_o1<?php echo $n ;?>" style="display:none;" value="<?php echo  isset($value3->pd_coverage_specify) ? $value3->pd_coverage_specify : '';?>" name="t_pd_coverage_specify<?php echo $k;?>[]">            
                       </span>			<span class="span2">              
                       <label class="control-label">Premium</label>              
                       <input class="textinput span9" type="text" placeholder="" value="<?php echo  isset($value3->pd_premium) ? $value3->pd_premium : '';?>" name="t_pd_premium<?php echo $k;?>[]"    >           
                        </span>            <span class="span2">             
                         <label class="control-label">Deductible</label>             
                          <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value3->pd_deductible) ? $value3->pd_deductible : '';?>" name="t_pd_deductible<?php echo $k;?>[]">           
                           </span>    
                   <span class="span1"> <label class="control-label">Remove</label><a class="btn btn-mini btn-1   " onclick="t_remove_pd_row1(<?php echo  $k; ?>);" id="remove_pd_row"><i class="icon icon-remove "></i></a></span>              
 <div class="row-fluid" style="" id="t_pd_amount<?php echo k; ?>">            
          <span class="span7">              
          <label class="control-label"></label>              
          <table class="table">               
           <thead>                  
           <tr>                    
           <th></th>                    
           <th>Amount or Limit</th>                   
            <th>Deductible</th>                    
            <th>Premium</th>                 
             </tr>               
              </thead>                
              <tbody>                  
              <tr>                    
              <td id="t_removal_name<?php echo $k;?>">Debris removal</td>                    
              <td>                      
              <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value3->pd_amount_limit) ? $value3->pd_amount_limit : '';?>" name="t_pd_amount_limit<?php echo $k;?>[]">                   
               </td>                    <td>                     
                <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value3->pd_deductible_limit) ? $value3->pd_deductible_limit : '';?>" name="t_pd_deductible_limit<?php echo $k;?>[]">                   
                 </td>                    
                 <td>                      
                 <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value3->pd_premium_limit) ? $value3->pd_premium_limit : '';?>" name="t_pd_premium_limit<?php echo $k;?>[]">                   
                  </td>                  
                  </tr>               
                   </tbody>             
                    </table>           
                     </span>           
                      <span class="span5">             
                       <label class="control-label"></label>           
                        </span>          
                        </div>                             
                           
                           
                            </div>
                            
        <?php $n++; }  }  ?>
        </div>
		        <label class="control-label control-label-2">Loss Payee Address(es)</label>
          <div class="row-fluid">
            <span class="span3">
              <label class="control-label">Entity name</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->pd_loss_name) ? $value1->pd_loss_name : '';?>" name="t_pd_loss_name[]">
            </span>
            <span class="span3">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->pd_loss_address) ? $value1->pd_loss_address : '';?>" name="t_pd_loss_address[]">
            </span>
            <span class="span1">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder=""  value="<?php echo  isset($value1->pd_loss_city) ? $value1->pd_loss_city : '';?>" name="t_pd_loss_city[]">
            </span>
            <span class="span1">
              <label class="control-label">State</label>
              								<?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = isset($value1->pd_loss_state) ? $value1->pd_loss_state : '';;
									
									?>
									<?php echo get_state_dropdown('t_pd_loss_state[]', $broker_state, $attr); ?>
	</select>
            
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" value="<?php echo  isset($value1->pd_loss_zip) ? $value1->pd_loss_zip : '';?>" placeholder="" name="t_pd_loss_zip[]" pattern="[0-9]+" mAxelngth="9">
            </span>
            <span class="span3">
              <label class="control-label">Remarks</label>
              <input class="textinput span12" type="text" value="<?php echo  isset($value->pd_loss_remarks) ? $value->pd_loss_remarks : '';?>" placeholder="" name="t_pd_loss_remarks[]">
            </span>
          </div>
		</span>
        
        <span id="add_cd_tractor_div<?php echo $k;?>" style=""><h5 class="heading sh sh-3">Cargo</h5><div class="row-fluid">           
                         <span class="span3" style="display:none;" id="cargo_damages<?php echo $k;?>">             
                          <label class="control-label">Average value per load &nbsp;</label>             
                           <input class="textinput span6 price_format" type="text" placeholder="$" value="<?php echo  isset($value1->cargo_avg_val) ? $value1->cargo_avg_val : '';?>" name="t_cargo_avg_val[]">             
                            <input class="textinput span6" type="text" placeholder="%" value="<?php echo  isset($value1->cargo_avg_per) ? $value1->cargo_avg_per : '';?>" name="t_cargo_avg_per[]">           
                             </span>            
                             <span class="span3" style="margin-left:10px;">              
                             <label class="control-label">Maximum values per load &nbsp;</label>             
                              <input class="textinput span6 price_format" type="text" placeholder="$" value="<?php echo  isset($value1->cargo_max_val) ? $value1->cargo_max_val : '';?>" name="t_cargo_max_val[]">              
                              <input class="textinput span6" type="text" placeholder="%" value="<?php echo  isset($value1->cargo_max_per) ? $value1->cargo_max_per : '';?>" name="t_cargo_max_per[]">           
                               </span>            <span class="span2">             
                               <label class="control-label">% Factor</label>              
                               <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->cargo_factor) ? $value1->cargo_factor : '';?>" name="t_cargo_factor[]">           
                                </span>            <span class="span2">             
                                 <label class="control-label">Premium</label>             
                                  <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->cargo_prem) ? $value1->cargo_prem : '';?>" name="t_cargo_prem[]">            
                                  </span>            
                                  <span class="span2">             
                                   <label class="control-label">Deductible</label>              
                                   <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value1->cargo_deduct) ? $value1->cargo_deduct : '';?>" name="t_cargo_deduct[]">            
                                   </span>         
                                    </div>  <div class="row-fluid">            
                                    <span class="span6">              
                                    <label class="control-label">Additional optional coverage</label>             
                                     <select name="t_coverage_name[]" class="select-15" onchange="t_coverage_name1(<?php echo $k; ?>,this.value)" id="t_coverage_name1<?php echo $k;?>">			   
                                    <?php $coverage_name = isset($value1->coverage_name) ? $value1->coverage_name : '';?>		 
                              <option value="">Please Select</option>               
                               <option <?php if($coverage_name  == 'Earned freight clause') echo 'selected="selected"'; ?> value="Earned freight clause">Earned freight clause</option>               
                                <option <?php if($coverage_name  == 'Refrigeration breakdown deductible') echo 'selected="selected"'; ?> value="Refrigeration breakdown deductible">Refrigeration breakdown deductible</option>               
                                 <option <?php if($coverage_name  == 'Debris removal') echo 'selected="selected"'; ?> value="Debris removal">Debris removal</option>               
                                  <option <?php if($coverage_name  == 'Excess cargo') echo 'selected="selected"'; ?> value="Excess cargo">Excess cargo</option>             
                                    </select>          
                                          </span> 
                                          <script>
												  var s=1
												  $(document).ready(function(){
													 
													   t_coverage_name1(s,$('#t_coverage_name1'+s).val());
													  s++;
													  });
												  
											
												  </script>   
                                     
                                                    
                                  </div></span><span id="t_cargo_dis<?php echo $k; ?>" style="display:none;"> <div class="row-fluid" id="t_cargo_amount<?php echo $k; ?>">          
                                           <span class="span8">              <table class="table">               
                                            <thead>                  <tr>                    <th></th>                  
                                              <th>Amount or Limit</th>                   
                                               <th>Deductible</th>                    
                                               <th>Premium</th>                  
                                               </tr>               
                                                </thead>               
                                                 <tbody>                 
                                                  <tr>                    
                                                  <td>Earned freight clause</td>                   
             <td>                     
         <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value1->cargo_amount_limit) ? $value1->cargo_amount_limit : '';?>" name="t_cargo_amount_limit[]">                  
        </td>                    
         <td>                      
       <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value1->cargo_amount_deduct) ? $value1->cargo_amount_deduct : '';?>" name="t_cargo_amount_deduct[]">                   
      </td>                   
      <td>                      
            <input class="textinput input-mini" type="text" placeholder="" value="<?php echo  isset($value1->cargo_amount_premium) ? $value1->cargo_amount_limit : '';?>" name="t_cargo_amount_premium[]">                   
                </td>                 
                </tr>               
                          
                   </tbody>             
                  </table>           
                      </span>          
                    <span class="span4">     
                     <label class="control-label"></label>           
                 </span>          
                  </div>
                  </span>
                  
															   
	<span id="add_drivers_tractor_div<?php echo $k;?>" style="display:none;">
      <h5 class="heading sh sh-5">Drivers Schedule</h5>
          
          <table class="table">
            <thead>
              <tr>
                <th>First name</th>
                <th>Middle name</th>
                <th>Last name</th>
                <th>Driver License
                  <br>Number</th>
                <th>License class</th>
                <th>Date hired</th>
                <th>How many years
                  <br>Class A license</th>
                <th>License issue state</th>
                <th>Remarks</th>
                <th>Surcharge</th>
              </tr>
            </thead>
            <tbody id="<?php echo $k; ?>t_add_drivered_row1">
                              
         <?php 


if (!empty($app_driver1)) { ?> 	
                <?php   foreach ($app_driver1 as $drivers) {
					  if ($drivers->vehicle_id == $value1->vehicle_id) {
					
					 ?>
                      <input type="hidden" value="<?php echo  isset($drivers->vehicle_id) ? $drivers->vehicle_id : ''; ?>" name="<?php echo $k; ?>t_dri_id[]"/>
      <input type="hidden" value="<?php echo (isset($drivers->driver_id) ? $drivers->driver_id : ''); ?>" name="<?php echo $k; ?>t_driver_id[]" />
              <tr>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_fname)){ echo (isset($drivers->driver_fname) ? $drivers->driver_fname : ''); }else{ echo (isset($drivers->driver_name) ? $drivers->driver_name : ''); } ?>" name="<?php echo $k; ?>t_driver_fname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder=""  value="<?php if(!empty($drivers->driver_mname)){ echo (isset($drivers->driver_mname) ? $drivers->driver_mname : ''); }else{ echo (isset($drivers->driver_middle_name) ? $drivers->driver_middle_name : ''); } ?>" name="<?php echo $k; ?>t_driver_mname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_lname)){ echo (isset($drivers->driver_lname) ? $drivers->driver_lname : ''); }else{ echo (isset($drivers->driver_last_name) ? $drivers->driver_last_name : ''); } ?>" name="<?php echo $k; ?>t_driver_lname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_license_num)){ echo (isset($drivers->driver_license_num) ? $drivers->driver_license_num : ''); }else{ echo (isset($drivers->driver_licence_no) ? $drivers->driver_licence_no : ''); } ?>" name="<?php echo $k; ?>t_driver_license_num[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_license_class)){ echo (isset($drivers->driver_license_class) ? $drivers->driver_license_class : ''); }else{ echo (isset($drivers->driver_license_class) ? $drivers->driver_license_class : ''); } ?>" name="<?php echo $k; ?>t_driver_license_class[]">
                </td>
                <td>
                  <input class="textinput input-mini datepick" type="text" value="<?php if(!empty($drivers->driver_date_hired)){ echo (isset($drivers->driver_date_hired) ? $drivers->driver_date_hired : ''); }else{ echo (isset($drivers->driver_license_issue_date) ? date('m/d/Y', strtotime($drivers->driver_license_issue_date)) : ''); } ?>" required="required" placeholder="" name="<?php echo $k; ?>t_driver_date_hired[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" value="<?php if(!empty($drivers->driver_class_A)){  echo (isset($drivers->driver_class_A) ? $drivers->driver_class_A : ''); }else{ echo (isset($drivers->driver_class_a_years) ? $drivers->driver_class_a_years : ''); } ?>" placeholder="" name="<?php echo $k; ?>t_driver_class_A[]">
                </td>
                <td>
                   <?php
									$attr = "class='span12 width_td ui-state-valid'";
									$broker_state =   isset($drivers->driver_state) ? $drivers->driver_state : $drivers->driver_license_issue_state;
									
									?>
									<?php echo get_state_dropdown($k.'t_driver_state[]', $broker_state, $attr); ?>
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_remarks)){ echo  (isset($drivers->driver_remarks) ? $drivers->driver_remarks : ''); } else{ isset($drivers->driver_remarks) ? $drivers->driver_remarks : ''; } ?>" name="<?php echo $k; ?>t_driver_remarks[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" value="<?php if(!empty($drivers->driver_surcharge)){ echo (isset($drivers->driver_surcharge) ? $drivers->driver_surcharge : ''); } else{ isset($drivers->driver_surcharge) ? $drivers->driver_surcharge : ''; } ?>" name="<?php echo $k; ?>t_driver_surcharge[]">
                </td>
              </tr>
           <?php } } } ?>
            </tbody>
           </table>
		  </span></div>
                  
		  
			  
			  
			  
			  
			  
			  
			  
			  <?php  $k++; } } ?>
        
        
        
        
		<span id="add_vehicle_div"></span>
         
		
		<div id="row_vehicle" style="display:none; ">
          <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="select_vehicle_year[]" class="select_new">
               
                 <option value="1985"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1985</option>
                 <option value="1986"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1986</option>
                                                                <option value="1987"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1987</option>
                                                                <option value="1988"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1988</option>
                                                                <option value="1989"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1989</option>
																<option value="1990"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1990</option>
                                                                <option value="1991"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1991</option>
                                                                <option value="1992"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1992</option>
                                                                <option value="1993"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1993</option>
                                                                <option value="1994"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1994</option>
                                                                <option value="1995"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>> 1995</option>
                                                                <option value="1996"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1996</option>
                                                                <option value="1997"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1997</option>
                                                                <option value="1998"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1998</option>
                                                                <option value="1999"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>1999</option>
                                                                <option value="2000"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2000</option>
                                                                <option value="2001"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2001</option>
                                                                <option value="2002"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2002</option>
                                                                <option value="2003"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2003</option>
                                                                <option value="2004"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2004</option>
                                                                <option value="2005"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2005</option>
                                                                <option value="2006"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2006</option>
                                                                <option value="2007"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2007</option>
                                                                <option value="2008"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2008</option>
                                                                <option value="2009"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2009</option>
                                                                <option value="2010"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2010</option>
                                                                <option value="2011"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2011</option>
                                                                <option value="2012"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2012</option>
                                                                <option value="2013"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2013</option>
																<option value="2014"  <?php if($select_vehicle_year  == 'No') echo 'selected="selected"'; ?>>2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="select_model[]" class="select_new">
                <option value="">Select</option>
				<option value="Freightliner">Freightliner</option>
                <option value="International">International</option>
                 <option value="Isuzu">Isuzu</option>
                 <option value="Kenworth">Kenworth</option>
                 <option value="Mack">Mack</option>
                 <option value="">Peterbilt</option>
                 <option value="Peterbilt">Sterling</option>
                 <option value="Western Star">Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="select_GVW[]" class="select_new">
                   <option>0-10,000 pounds</option>
                                                                <option>10,001-26,000 pounds</option>
                                                                <option>26,001-40,000 pounds</option>
                                                                <option>40,001-80,000 pounds</option>
                                                                <option>Over 80,000 pounds</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" name="select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" placeholder="" name="select_Operation[]">
            </span>
          </div>
		 </div>
     
	     
		 <div id="liability_value" style="display:none;">
            <span class="span2" >
              <label class="control-label">Radius of Operation</label>
              <select name="select_Radius[]" id="select_Radius"  class="select_new check_Radius">
			    <option value="">Please Select</option>
                 <option value="0-100 miles">0-100 miles</option>
                                                                <option value="101-500 miles" >101-500 miles</option>           	
                                                                <option value="501+ miles">501+ miles</option>
                                                              	<option value="other">Other</option>
              </select>
            </span>
			</div>
			<div id="liability_value1" style="display:none;">
            <span class="span2" >
              <label class="control-label">States driving</label>
            <?php
									$attr = "class='span12 ui-state-valid state_driv' id='state_driving'" ;
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('state_driving[]', $broker_state, $attr); ?>
            </span>
			</div>
			<div id="liability_value2" style="display:none;">
            <span class="span3" >
              <label class="control-label">Cities</label>
              <input class="textinput span12 check_Cities" type="text" id="select_Cities" placeholder="" name="select_Cities[]">
            </span>
		</div>
      <!--      
              <label class="control-label">Applicant's business</label>
              <select name="select_applicant[]" class="select_new check_App" id="select_App" onChange="business_app(this.value);">
			  <option value="">Please Select</option>
                <option value="Tow truck driver">Tow truck driver</option>
				<option value="Other">Other</option>				
              </select>
              <input class="textinput span5 check_app_other" type="text" placeholder="Please specify" style="display:none;" id="select_app_other" name="select_applicant_driver[]">
            -->
         </span>
		
       <div id="request_specail" style="display:none;">
          <div class="row-fluid">
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" placeholder="" name="select_special[]">
            </span>
            <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="select_Axels[]" class="select_new">
                 <option value="">Please Select</option>
				 <option value="1 Axel">1 Axel</option>
				 <option value="2 Axel">2 Axel</option>
				 <option value="3 Axel">3 Axel</option>
              </select>
            </span>
            <span class="span2">
              <label>Number of drivers ?</label>
              <input <?php echo $less_100_limit; ?> required type="text" id="num_of_drivers" on1 name="select_num_drivers[]" class="span4"  value="<?php echo (!empty($drivers))? count($drivers):''; ?>"/>
                                    </span>
            <span class="span3">
              <label class="control-label"></label>
            </span>
          </div>
		 </div>
         <!-- <div class="row-fluid">
            <span class="span12">
              <label class="control-label pull-left">Do you pull equipment with this vehicle? &nbsp;</label>
              <select name="" class="select_new">
                <option value="Yes">Yes</option>
              </select>
			  <select type="text" name="vehicle_trailer_no[]" id="vehicle_trailer" onchange="add_trailer1(this.value,1 );" class="span3 ui-state-valid">
			  <option value="0">None</option>
			  <option value="1">Single</option>	
			  <option value="2">Double</option>	
			  <option value="3">Triple</option>	
			  </select>
              <select name="" class="select-7">
                <option value="Single">Single</option>
                <option value="Double">Double</option>
                <option value="Other">Triple</option>
              </select>
            </span>
          </div>
          <div class="row-fluid">
            <span class="span6">
              <label class="control-label pull-left">Do you need Physical Damage Coverage? &nbsp;</label>
              <select name="">
                <option value="Yes">Yes</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label pull-left">Do you need Cargo Coverage? &nbsp;</label>
              <select name="" class="select_new">
                <option value="Yes">Yes</option>
              </select>
            </span>
           </div>-->
		  
		  <!--end truck-->
		  <!--start tractor-->
		
		
		<div id="row_tractor" style="display:none; ">
           <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="t_select_vehicle_year[]" class="select_new">
                 <option value="1985">1985</option>
                 <option value="1986">1986</option>
                                                                <option value="1987">1987</option>
                                                                <option value="1988">1988</option>
                                                                <option value="1989">1989</option>
																<option value="1990">1990</option>
                                                                <option value="1991">1991</option>
                                                                <option value="1992">1992</option>
                                                                <option value="1993">1993</option>
                                                                <option value="1994">1994</option>
                                                                <option value="1995"> 1995</option>
                                                                <option value="1996">1996</option>
                                                                <option value="1997">1997</option>
                                                                <option value="1998">1998</option>
                                                                <option value="1999">1999</option>
                                                                <option value="2000">2000</option>
                                                                <option value="2001">2001</option>
                                                                <option value="2002">2002</option>
                                                                <option value="2003">2003</option>
                                                                <option value="2004">2004</option>
                                                                <option value="2005">2005</option>
                                                                <option value="2006">2006</option>
                                                                <option value="2007">2007</option>
                                                                <option value="2008">2008</option>
                                                                <option value="2009">2009</option>
                                                                <option value="2010">2010</option>
                                                                <option value="2011">2011</option>
                                                                <option value="2012">2012</option>
                                                                <option value="2013">2013</option>
																<option value="2014">2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="t_select_model[]" class="select_new">
                <option value="">Select</option>
				<option value="Freightliner">Freightliner</option>
                <option value="International">International</option>
                 <option value="Isuzu">Isuzu</option>
                 <option value="Kenworth">Kenworth</option>
                 <option value="Mack">Mack</option>
                 <option value="">Peterbilt</option>
                 <option value="Peterbilt">Sterling</option>
                 <option value="Western Star">Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="t_select_GVW[]" class="select_new">
                   <option>0-10,000 pounds</option>
                                                                <option>10,001-26,000 pounds</option>
                                                                <option>26,001-40,000 pounds</option>
                                                                <option>40,001-80,000 pounds</option>
                                                                <option>Over 80,000 pounds</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" name="t_select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" placeholder="" name="t_select_Operation[]">
            </span>
          </div>
		  
    </div>
	
	   <div class="row-fluid" id="liability_value_tractor" style="display:none;">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="t_select_Radius[]" id="t_s_Radius" class="select_new t_check_Radius" onchange="">
			  <option value="">Please Select</option>
                 <option value="0-100 miles">0-100 miles</option>
                        <option value="101-500 miles" >101-500 miles</option>           	
                         <option value="501+ miles">501+ miles</option>
                         <option value="other">Other</option>                                       	                                                                             
              </select>
            </span>
            <span class="span2">
              <label class="control-label">States driving</label>
            <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('t_state_driving[]', $broker_state, $attr); ?>
            </span>
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12 t_check_Cities" type="text" placeholder="" id="t_select_Cities" name="t_select_Cities[]">
            </span>
       
          </div>
		  

          <div class="row-fluid" id="request_specail1" style="display:none;">
		   <div class="row-fluid">
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" placeholder="" name="t_select_special[]">
            </span>
            <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="t_select_Axels[]" class="select_new">
                <option value="">Please Select</option>
				 <option value="1 Axel">1 Axel</option>
				 <option value="2 Axel">2 Axel</option>
				 <option value="3 Axel">3 Axel</option>
              </select>
            </span>
            <span class="span2">
                <label>Number of drivers ?</label>
           <input <?php echo $less_100_limit; ?> required type="text" id="num_of_drivers" on1 name="t_select_num_drivers[]" class="span4"  value=""/>
                                  
            </span>
            <span class="span3">
              <label class="control-label"></label>
            </span>
			</div>
          </div>
		
		  
		  
		  
		  
       
      
		 
		 
         <!-- <div class="row-fluid">
            <span class="span12">
              <label class="control-label pull-left">Do you pull equipment with this vehicle? &nbsp;</label>
              <select name="" class="select_new">
                <option value="Yes">Yes</option>
              </select>
			  <select type="text" name="vehicle_trailer_no[]" id="vehicle_trailer" onchange="add_trailer1(this.value,1 );" class="span3 ui-state-valid">
			  <option value="0">None</option>
			  <option value="1">Single</option>	
			  <option value="2">Double</option>	
			  <option value="3">Triple</option>	
			  </select>
              <select name="" class="select-7">
                <option value="Single">Single</option>
                <option value="Double">Double</option>
                <option value="Other">Triple</option>
              </select>
            </span>
          </div>
          <div class="row-fluid">
            <span class="span6">
              <label class="control-label pull-left">Do you need Physical Damage Coverage? &nbsp;</label>
              <select name="">
                <option value="Yes">Yes</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label pull-left">Do you need Cargo Coverage? &nbsp;</label>
              <select name="" class="select_new">
                <option value="Yes">Yes</option>
              </select>
            </span>
           </div>-->
		  
		  <!--end tractor-->
		  <!--start trailer-->
		  
		  <span id="add_trailer_div"></span>
          
          

          
          <div id="license_state" style="display:none;" >
 <?php
									$attr = "class='span12 width_td ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('driver_state[]', $broker_state, $attr); ?>
</div>
          
          
            <div id="license_state1" style="display:none;" >
 <?php
									$attr = "class='span12 width_td ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('t_driver_state[]', $broker_state, $attr); ?>
</div>
          
          
			
	
		  <div id="pd_id_damage" style="display:none; ">
          
         
		        <label class="control-label control-label-2">Loss Payee Address(es)</label>
          <div class="row-fluid">
            <span class="span3">
              <label class="control-label">Entity name</label>
              <input class="textinput span12" type="text" placeholder="" name="pd_loss_name[]">
            </span>
            <span class="span3">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" name="pd_loss_address[]">
            </span>
            <span class="span1">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder="" name="pd_loss_city[]">
            </span>
            <span class="span1">
              <label class="control-label">State</label>
              <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('pd_loss_state[]', $broker_state, $attr); ?>
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" placeholder="" name="pd_loss_zip[]" pattern="[0-9]+" mAxelngth="9">
            </span>
            <span class="span3">
              <label class="control-label">Remarks</label>
              <input class="textinput span12" type="text" placeholder="" name="pd_loss_remarks[]">
            </span>
          </div>
        
		  </div>
		  <div id="cv_id_damage" style="display:none; ">
        
		  </div>
		  <div id="gd_id_damage" style="display:none; ">
          <h5 class="heading sh sh-4">Garaging Address</h5>
          <div class="row-fluid">
            <span class="span4">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" name="garage_address[]">
            </span>
            <span class="span2">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder="" name="garage_city[]">
            </span>
            <span class="span2">
              <label class="control-label">State</label>
              <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('garage_state[]', $broker_state, $attr); ?>
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" placeholder="" name="garage_zip[]">
            </span>
            <span class="span4">
              <label class="control-label">Remarks</label>
              <input class="textinput span12" type="text" placeholder="" name="garage_remarks[]">
            </span>
          </div>
		  </div>
		 <div id="ds_id_damage" style="display:none; ">
          
          <table class="table">
            <thead>
              <tr>
                <th>First name</th>
                <th>Middle name</th>
                <th>Last name</th>
                <th>Driver License
                  <br>Number</th>
                <th>License class</th>
                <th>Date hired</th>
                <th>How many years
                  <br>Class A license</th>
                <th>License issue state</th>
                <th>Remarks</th>
                <th>Surcharge</th>
              </tr>
            </thead>
            <tbody id="add_drivered_row1">
              <tr>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_fname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_mname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_lname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_license_num[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_license_class[]">
                </td>
                <td>
                  <input class="textinput input-mini datepick" type="text" placeholder="" name="driver_date_hired[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_class_A[]">
                </td>
                <td>
                   <?php
									$attr = "class='span12 width_td ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('driver_state[]', $broker_state, $attr); ?>
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_remarks[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_surcharge[]">
                </td>
              </tr>
            </tbody>
           </table>
		  </div>
		  	  <div id="t_pd_id_damage" style="display:none; ">
        
		        <label class="control-label control-label-2">Loss Payee Address(es)</label>
          <div class="row-fluid">
            <span class="span3">
              <label class="control-label">Entity name</label>
              <input class="textinput span12" type="text" placeholder="" name="t_pd_loss_name[]">
            </span>
            <span class="span3">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" name="t_pd_loss_address[]">
            </span>
            <span class="span1">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder="" name="t_pd_loss_city[]">
            </span>
            <span class="span1">
              <label class="control-label">State</label>
              <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('pd_loss_state[]', $broker_state, $attr); ?>
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" placeholder="" name="t_pd_loss_zip[]" pattern="[0-9]+" mAxelngth="9">
            </span>
            <span class="span3">
              <label class="control-label">Remarks</label>
              <input class="textinput span12" type="text" placeholder="" name="t_pd_loss_remarks[]">
            </span>
          </div>
		  </div>
		    <div id="t_cv_id_damage" style="display:none; ">
     
		  </div>
		  <div id="t_gd_id_damage" style="display:none; ">
          <h5 class="heading sh sh-4">Garaging Address</h5>
          <div class="row-fluid">
            <span class="span4">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" name="t_garage_address[]">
            </span>
            <span class="span2">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder="" name="t_garage_city[]">
            </span>
            <span class="span1">
              <label class="control-label">State</label>
              <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('t_garage_state[]', $broker_state, $attr); ?>
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" placeholder="" name="t_garage_zip[]">
            </span>
            <span class="span4">
              <label class="control-label">Remarks</label>
              <input class="textinput span12" type="text" placeholder="" name="t_garage_remarks[]">
            </span>
          </div>
		  </div>
		 <div id="t_ds_id_damage" style="display:none; ">
      
          <table class="table">
            <thead>
              <tr>
                <th>First name</th>
                <th>Middle name</th>
                <th>Last name</th>
                <th>Driver License
                  <br>Number</th>
                <th>License class</th>
                <th>Date hired</th>
                <th>How many years
                  <br>Class A license</th>
                <th>License issue state</th>
                <th>Remarks</th>
                <th>Surcharge</th>
              </tr>
            </thead>
            <tbody id="t_add_drivered_row1">
              <tr>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_fname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_mname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_lname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_license_num[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_license_class[]">
                </td>
                <td>
                  <input class="textinput input-mini datepick" type="text" placeholder="" name="t_driver_date_hired[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_class_A[]">
                </td>
                <td>
                   <?php
									$attr = "class='span12 width_td ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('t_driver_state[]', $broker_state, $attr); ?>
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_remarks[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_surcharge[]">
                </td>
              </tr>
            </tbody>
           </table>
		  </div>
          
          
          <div id="row_trailer" style="display:none; ">
          
             <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="tr_select_vehicle_year[]" class="select_new">
                 <option value="1985">1985</option>
                 <option value="1986">1986</option>
                                                                <option value="1987">1987</option>
                                                                <option value="1988">1988</option>
                                                                <option value="1989">1989</option>
																<option value="1990">1990</option>
                                                                <option value="1991">1991</option>
                                                                <option value="1992">1992</option>
                                                                <option value="1993">1993</option>
                                                                <option value="1994">1994</option>
                                                                <option value="1995"> 1995</option>
                                                                <option value="1996">1996</option>
                                                                <option value="1997">1997</option>
                                                                <option value="1998">1998</option>
                                                                <option value="1999">1999</option>
                                                                <option value="2000">2000</option>
                                                                <option value="2001">2001</option>
                                                                <option value="2002">2002</option>
                                                                <option value="2003">2003</option>
                                                                <option value="2004">2004</option>
                                                                <option value="2005">2005</option>
                                                                <option value="2006">2006</option>
                                                                <option value="2007">2007</option>
                                                                <option value="2008">2008</option>
                                                                <option value="2009">2009</option>
                                                                <option value="2010">2010</option>
                                                                <option value="2011">2011</option>
                                                                <option value="2012">2012</option>
                                                                <option value="2013">2013</option>
																<option value="2014">2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="tr_select_model[]" class="select_new">
                <option value="">Select</option>
				<option value="Freightliner">Freightliner</option>
                <option value="International">International</option>
                 <option value="Isuzu">Isuzu</option>
                 <option value="Kenworth">Kenworth</option>
                 <option value="Mack">Mack</option>
                 <option value="">Peterbilt</option>
                 <option value="Peterbilt">Sterling</option>
                 <option value="Western Star">Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="tr_select_GVW[]" class="select_new">
                   <option>0-10,000 pounds</option>

                                                                <option>10,001-26,000 pounds</option>
                                                                <option>26,001-40,000 pounds</option>
                                                                <option>40,001-80,000 pounds</option>
                                                                <option>Over 80,000 pounds</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" name="tr_select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" placeholder="" name="tr_select_Operation[]">
            </span>
          </div>
		  </div>
          <div class="row-fluid" id="radius_trailer" style="display:none;">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="tr_select_Radius[]" class="select_new">
                 <option value="0-100 miles">0-100 miles</option>
                                                                <option value="101-500 miles" >101-500 miles</option>           	
                                                                <option value="501+ miles">501+ miles</option>
                                                              	<option value="other">Other</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">States driving</label>
            <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('tr_state_driving[]', $broker_state, $attr); ?>
            </span>
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12" type="text" placeholder="" name="tr_select_Cities[]">
            </span>

          </div>
      
          <div class="row-fluid" id="request_trailer" style="display:none;">
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" placeholder="" name="tr_select_special[]">
            </span>
    <!--        <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="tr_select_Axels[]" class="select_new">
                  <option value="">Please Select</option>
				 <option value="1 Axel">1 Axel</option>
				 <option value="2 Axel">2 Axel</option>
				 <option value="3 Axel">3 Axel</option>
              </select>
            </span>-->
            <span class="span2">
       <!--       <label class="control-label">Number of drivers:&nbsp;</label>
              <input class="textinput input-mini" type="text" placeholder="" name="tr_select_num_drivers[]"  pattern="[0-9]+" mAxelngth="2">-->
            </span>
            <span class="span3">
              <label class="control-label"></label>
            </span>
          </div>
		
		  <!--end trailer-->	
          
          
                  <div id="state_trailer" style="display:none;">
             <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('tr_state_driving1[]', $broker_state, $attr); ?>
          </div>
          
               
               <?php if(!empty($app_veh4)){ $g=1; foreach($app_veh4 as $value2)  { 
			   
			 
			    ?>
               
               <input type="hidden" value="<?php echo  isset($value2->vehicle_id) ? $value2->vehicle_id : ''; ?>" name="tr_vehicle_id1[]"/>
               <div class="row_vehicle_trailer1 " >
               <div class="row-fluid ">				
               <div class="span12" style="text-align:center; font-weight:bold; background-color:yellow; color:black;">				TRAILER #<?php echo  $g; ?></div>
               
          
             <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="tr_select_vehicle_year1[]" class="select_new">
                <?php $select_vehicle_year= isset($value2->vehicle_year) ? $value2->vehicle_year : ''; ?>
                 <option value="1985"  <?php if($select_vehicle_year  == '1985') echo 'selected="selected"'; ?>>1985</option>
                 <option value="1986"  <?php if($select_vehicle_year  == '1986') echo 'selected="selected"'; ?>>1986</option>
                                                                <option value="1987"  <?php if($select_vehicle_year  == '1987') echo 'selected="selected"'; ?>>1987</option>
                                                                <option value="1988"  <?php if($select_vehicle_year  == '1988') echo 'selected="selected"'; ?>>1988</option>
                                                                <option value="1989"  <?php if($select_vehicle_year  == '1989') echo 'selected="selected"'; ?>>1989</option>
																<option value="1990"  <?php if($select_vehicle_year  == '1990') echo 'selected="selected"'; ?>>1990</option>
                                                                <option value="1991"  <?php if($select_vehicle_year  == '1991') echo 'selected="selected"'; ?>>1991</option>
                                                                <option value="1992"  <?php if($select_vehicle_year  == '1992') echo 'selected="selected"'; ?>>1992</option>
                                                                <option value="1993"  <?php if($select_vehicle_year  == '1993') echo 'selected="selected"'; ?>>1993</option>
                                                                <option value="1994"  <?php if($select_vehicle_year  == '1994') echo 'selected="selected"'; ?>>1994</option>
                                                                <option value="1995"  <?php if($select_vehicle_year  == '1995') echo 'selected="selected"'; ?>> 1995</option>
                                                                <option value="1996"  <?php if($select_vehicle_year  == '1996') echo 'selected="selected"'; ?>>1996</option>
                                                                <option value="1997"  <?php if($select_vehicle_year  == '1997') echo 'selected="selected"'; ?>>1997</option>
                                                                <option value="1998"  <?php if($select_vehicle_year  == '1998') echo 'selected="selected"'; ?>>1998</option>
                                                                <option value="1999"  <?php if($select_vehicle_year  == '1999') echo 'selected="selected"'; ?>>1999</option>
                                                                <option value="2000"  <?php if($select_vehicle_year  == '2000') echo 'selected="selected"'; ?>>2000</option>
                                                                <option value="2001"  <?php if($select_vehicle_year  == '2001') echo 'selected="selected"'; ?>>2001</option>
                                                                <option value="2002"  <?php if($select_vehicle_year  == '2002') echo 'selected="selected"'; ?>>2002</option>
                                                                <option value="2003"  <?php if($select_vehicle_year  == '2003') echo 'selected="selected"'; ?>>2003</option>
                                                                <option value="2004"  <?php if($select_vehicle_year  == '2004') echo 'selected="selected"'; ?>>2004</option>
                                                                <option value="2005"  <?php if($select_vehicle_year  == '2005') echo 'selected="selected"'; ?>>2005</option>
                                                                <option value="2006"  <?php if($select_vehicle_year  == '2006') echo 'selected="selected"'; ?>>2006</option>
                                                                <option value="2007"  <?php if($select_vehicle_year  == '2007') echo 'selected="selected"'; ?>>2007</option>
                                                                <option value="2008"  <?php if($select_vehicle_year  == '2008') echo 'selected="selected"'; ?>>2008</option>
                                                                <option value="2009"  <?php if($select_vehicle_year  == '2009') echo 'selected="selected"'; ?>>2009</option>
                                                                <option value="2010"  <?php if($select_vehicle_year  == '2010') echo 'selected="selected"'; ?>>2010</option>
                                                                <option value="2011"  <?php if($select_vehicle_year  == '2011') echo 'selected="selected"'; ?>>2011</option>
                                                                <option value="2012"  <?php if($select_vehicle_year  == '2012') echo 'selected="selected"'; ?>>2012</option>
                                                                <option value="2013"  <?php if($select_vehicle_year  == '2013') echo 'selected="selected"'; ?>>2013</option>
																<option value="2014"  <?php if($select_vehicle_year  == '2014') echo 'selected="selected"'; ?>>2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="tr_select_model1[]" class="select_new">
            <?php $select_model= isset($value2->make_model) ? $value2->make_model : ''; ?>
                <option value="">Select</option>
				<option value="Freightliner" <?php if($select_model  == 'Freightliner') echo 'selected="selected"'; ?>>Freightliner</option>
                <option value="International" <?php if($select_model  == 'International') echo 'selected="selected"'; ?>>International</option>
                 <option value="Isuzu" <?php if($select_model  == 'Isuzu') echo 'selected="selected"'; ?>>Isuzu</option>
                 <option value="Kenworth" <?php if($select_model  == 'Kenworth') echo 'selected="selected"'; ?>>Kenworth</option>
                 <option value="Mack" <?php if($select_model  == 'Mack') echo 'selected="selected"'; ?>>Mack</option>
                 <option value="Peterbilt" <?php if($select_model  == 'Peterbilt') echo 'selected="selected"'; ?>>Peterbilt</option>
                 <option value="Sterling" <?php if($select_model  == 'Sterling') echo 'selected="selected"'; ?>>Sterling</option>
                 <option value="Western Star" <?php if($select_model  == 'Western Star') echo 'selected="selected"'; ?>>Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="tr_select_GVW1[]" class="select_new">
               <?php $select_GVW= isset($value2->gvw) ? $value2->gvw : ''; ?>
                   <option value="0-10,000 pounds" <?php if($select_model  == '0-10,000 pounds') echo 'selected="selected"'; ?>>0-10,000 pounds</option>
                                                                <option value="10,001-26,000 pounds" <?php if($select_GVW  == '10,001-26,000 pounds') echo 'selected="selected"'; ?>>10,001-26,000 pounds</option>
                                                                <option value="26,001-40,000 pounds" <?php if($select_GVW  == '26,001-40,000 pounds') echo 'selected="selected"'; ?>>26,001-40,000 pounds</option>
                                                                <option value="40,001-80,000 pounds" <?php if($select_GVW  == '40,001-80,000 pounds') echo 'selected="selected"'; ?>>40,001-80,000 pounds</option>
                                                                <option value="Over 80,000 pounds" <?php if($select_GVW  == 'Over 80,000 pounds') echo 'selected="selected"'; ?>>Over 80,000 pounds</option>
             
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value2->vin) ? $value2->vin : ''; ?>" name="tr_select_VIN1[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" value="<?php echo  isset($value2->select_Operation) ? $value2->select_Operation : ''; ?>" placeholder="" name="tr_select_Operation1[]">
            </span>
          </div>
		  <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="tr_select_Radius1[]" class="select_new">
               <?php $select_Radius= isset($value2->radius_of_operation) ? $value2->radius_of_operation : ''; ?>
			    <option value="">Please Select</option>
                 <option value="0-100 miles" <?php if($select_Radius  == '0-100 miles') echo 'selected="selected"'; ?>>0-100 miles</option>
                                                                <option value="101-500 miles" <?php if($select_Radius  == '101-500 miles') echo 'selected="selected"'; ?>>101-500 miles</option>           	
                                                                <option value="501+ miles" <?php if($select_Radius  == '501+ miles') echo 'selected="selected"'; ?>>501+ miles</option>
                                                              	<option value="other" <?php if($select_Radius  == 'other') echo 'selected="selected"'; ?>>Other</option>
            
              </select>
            </span>
            <span class="span2">
              <label class="control-label">States driving</label>
            										
				 <?php
									$attr = "class='span12 ui-state-valid state_driv' id='state_driving'" ;
									$broker_state = isset($value2->state_driving) ? $value2->state_driving : '';
									
									?>
									<?php echo get_state_dropdown('tr_state_driving1[]', $broker_state, $attr); ?>
            
            
            </span>
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($value2->select_Cities) ? $value2->select_Cities : ''; ?>" name="tr_select_Cities1[]">
            </span>

          <span class="span5">              <label class="control-label">Applicants business</label>              
          <select name="tr_select_applicant1[]" id="tr_select_App<?php echo $g;?>" onchange="tr_business_app(<?php echo $g;?>,this.value);" class="select_new">    
          
          <script>
                 var e=1;
																 $(document).ready(function(){
																	
																     tr_business_app(e,$('#tr_select_App'+e).val());
																	 e++;
																 });
																 </script>	             
         <?php $select_applicant= isset($value2->select_applicant) ? $value2->select_applicant : ''; ?> 
               <option value="">Please Select</option>		
              <option value="Tow truck driver" <?php if($select_applicant  == 'Tow truck driver') echo 'selected="selected"'; ?>>Tow truck driver</option>
              <option value="Other" <?php if($select_applicant  == 'Other') echo 'selected="selected"'; ?>> Other </option>	
               </select>
           <input class="textinput span5" type="text" style="display:none;" placeholder="Please specify" id="tr_select_app_other<?php echo $g; ?>" value="<?php isset($value2->select_applicant_driver) ? $value2->select_applicant_driver : '';?>" name="tr_select_applicant_driver1[]">            
           </span>
           </div>
           <div class="row-fluid">           
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($value2->select_special) ? $value2->select_special : '';?>" name="tr_select_special1[]">
            </span>
    <!--        <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="tr_select_Axels1[]" class="select_new">
               <?php $select_Axels= isset($value2->number_of_Axels) ? $value2->number_of_Axels : '';?>
                 <option value="">Please Select</option>
				 <option value="1 Axel" <?php if($select_Axels  == '1 Axel') echo 'selected="selected"'; ?> >1 Axel</option>
				 <option value="2 Axel" <?php if($select_Axels  == '2 Axel') echo 'selected="selected"'; ?> >2 Axel</option>
				 <option value="3 Axel" <?php if($select_Axels  == '3 Axel') echo 'selected="selected"'; ?> >3 Axel</option>
            
              </select>
            </span>-->
    <!--        <span class="span2">
              <label class="control-label">Number of drivers?&nbsp;</label>
              <input class="textinput input-mini" type="text" value="<?php echo  isset($value2->select_num_drivers) ? $value2->select_num_drivers : '';?>" placeholder="" name="tr_select_num_drivers[]"  pattern="[0-9]+" mAxelngth="2">
            </span>-->
            <span class="span3">
              <label class="control-label"></label>
            </span>
          </div>
		     </div>
		   </div>
		  <?php $g++; }  }    ?>   
          
  
          
          <div id="add_vehicle_trailer_div"> </div>
          <div class="row-fluid">
            <span class="span12">
              <label class="control-label pull-left">Please attach MVR's for all driver(s) &nbsp;</label>
			  <input type="hidden" name="uploaded_files_values_div" id="uploaded_files_values_second_div" value="<?php echo  isset($app_load[0]->uploaded_files_values_div) ? $app_load[0]->uploaded_files_values_div : '';?>">
			  
			  <input type="button" id="first_upload_second_div" value="Upload"><br/>
             <?php $file=explode(",",$app_load[0]->uploaded_files_values_div); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
              <ul id="upload_file_name_second_div"></ul>
            </span>
          </div>
          
         
		</div>
		<div style="" id="">
	<!--	  <span class="badge">Cargo</span>-->
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">30.Are loaded trucks ever left unattended? &nbsp;</label>
            <select name="loaded_trucks_name" class="width_option" id="loaded_trucks_id" onchange="load_trucks();" required>
              <?php $loaded_trucks_name= isset($app_load[0]->loaded_trucks_name) ? $app_load[0]->loaded_trucks_name : '';?>
                 <option value="">Please Select</option>
              <option value="Yes" <?php if($loaded_trucks_name=="Yes"){ echo 'selected';}  ?>>Yes</option>
              <option value="No" <?php if($loaded_trucks_name=="No"){ echo 'selected';}  ?>>No</option>
            </select>
          </span>
        </div>
        <!--loaded_id-->
		<div  id="" style="display:none;">
		    <label class="control-label mt_15">Indicate whether the following additional coverages are required.</label>
        <div class="row-fluid mt_15">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th></th>
                  <th style="margin-left: 8px;float: left;">Amount or Limit</th>
                  <th></th>
                  <th style="margin-left: 8px;float: left;">Amount or Limit</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Earned freight clause</td>
                  <td >
                    <input class="textinput span4" type="text"  value="<?php echo  isset($app_load[0]->freight_amount) ? $app_load[0]->freight_amount : '';?>" placeholder="" name="freight_amount">
                  </td>
                  <td>Other</td>
                  <td>
                    <input class="textinput span4" type="text"  value="<?php echo  isset($app_load[0]->other_amount) ? $app_load[0]->other_amount : '';?>" placeholder="" name="other_amount">
                  </td>
                </tr>
                <tr>
                  <td>Refrigeration breakdown clause</td>
                  <td>
                    <input class="textinput span4" type="text" value="<?php echo  isset($app_load[0]->clause_amount) ? $app_load[0]->clause_amount : '';?>" placeholder="" name="clause_amount">
                  </td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
	  </div>
      
	</div>
    
		<div style="display:none" id="badge_id_4">
	<!--	 <span class="badge">Cargo, Liability</span>-->
        <div class="row-fluid mt_15">
         <input type="hidden" value="<?php echo isset($app_cov[0]->cover_id) ? $app_cov[0]->cover_id : ''; ?>" name="cover_id" /> 
          
            <label class="control-label pull-left">31.Is this a primary coverage? &nbsp;</label>
            <select class="width_option" name="primary_cover_name" id="primary_coverage_id" onchange="primary_coverage(this.value);" required>
             <?php $primary_cover_name = isset($app_cov[0]->primary_cover_name) ? $app_cov[0]->primary_cover_name : '';?>
             <option value="">Please Select</option>
                <option value="No" <?php if($primary_cover_name  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($primary_cover_name  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          
          <script>
		   $(document).ready(function() {
		  primary_coverage($('#primary_coverage_id').val());
		   });
		  
		  </script>
        </div>
		<div  id="coverage_id">
		  <div class="row-fluid">
          <span class="span3">
            <select name="combined_name" class="select-13" id="combine_func1" onchange="combine_func(this.value);">
            <?php $combined_name=  isset($app_cov[0]->combined_name) ? $app_cov[0]->combined_name : '';?>
            <option value="">Please Select</option>
              <option value="Combined Single Limit" <?php if($combined_name=="Combined Single Limit") {echo 'selected';}?>>Combined Single Limit</option>
              <option value="Split Limit" <?php if($combined_name=="Split Limit") {echo 'selected';}?>>Split Limit</option>
            </select>
          </span>
          <span class="span9" style="display:none;" id="Combined_limit">
            <input class="textinput span4" type="text" name="combined_amount" value="<?php echo  isset($app_cov[0]->combined_amount) ? $app_cov[0]->combined_amount : '';?>" placeholder="Amount">
            <input class="textinput" type="text"  value="<?php echo  isset($app_cov[0]->combined_deductible) ? $app_cov[0]->combined_deductible : '';?>" placeholder="Deductible" name="combined_deductible">
          </span>
              <span class="span6" style="display:none;" id="Split_limit">
            <input class="textinput price_format span4" type="text"  value="<?php echo  isset($app_cov[0]->each_person) ? $app_cov[0]->each_person : '';?>" name="each_person" placeholder="$ each person">
            <input class="textinput price_format span4" type="text"  value="<?php echo  isset($app_cov[0]->each_accident) ? $app_cov[0]->each_accident : '';?>" name="each_accident" placeholder="$ each accident">
            <input class="textinput price_format span4" type="text"  value="<?php echo  isset($app_cov[0]->each_property) ? $app_cov[0]->each_property : '';?>" name="each_property" placeholder="$ each property">
          </span>
          <span class="span3" style="display:none;" id="Split_limit1">
            <input class="textinput" type="text" placeholder="Deductible"  value="<?php echo  isset($app_cov[0]->split_deductible) ? $app_cov[0]->split_deductible : '';?>" name="split_deductible">
          </span>
          
        </div>
        <script>
		  $(document).ready(function(){
			//  function combine_func($('#combine_func1').val()){
				  
				  	  if($('#combine_func1').val()=="Combined Single Limit"){
															//  alert();
															  $('#Combined_limit').show();
															  $('#Split_limit').hide();
															  $('#Split_limit1').hide();
															  }
														  if($('#combine_func1').val()=="Split Limit"){
															  $('#Combined_limit').hide();
															  $('#Split_limit').show();
															  $('#Split_limit1').show();
															  }
				  
				//  }
			  
		  });
		</script>
		
        
		 <!--    <div class="row-fluid">
          <span class="span3">
            <select name="split_limit" class="select-13">
              <option value="Split Limit">Split Limit</option>
            </select>
          </span>
          <span class="span6">
            <input class="textinput price_format span4" type="text"  value="<?php echo  isset($app_cov[0]->each_person) ? $app_cov[0]->each_person : '';?>" name="each_person" placeholder="$ each person">
            <input class="textinput price_format span4" type="text"  value="<?php echo  isset($app_cov[0]->each_accident) ? $app_cov[0]->each_accident : '';?>" name="each_accident" placeholder="$ each accident">
            <input class="textinput price_format span4" type="text"  value="<?php echo  isset($app_cov[0]->each_property) ? $app_cov[0]->each_property : '';?>" name="each_property" placeholder="$ each property">
          </span>
          <span class="span3">
            <input class="textinput" type="text" placeholder="Deductible"  value="<?php echo  isset($app_cov[0]->split_deductible) ? $app_cov[0]->split_deductible : '';?>" name="split_deductible">
          </span>
        </div>  -->
		</div>
	</div>
		 <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">32.Do you need UM or UIM or PIP? &nbsp;</label>
            <select class="width_option" name="split_PIP" required>
		     <?php $split_PIP = isset($app_cov[0]->split_PIP) ? $app_cov[0]->split_PIP : '';?>
             <option value="">Please Select</option>
                <option value="No" <?php if($split_PIP  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($split_PIP  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
		<input type="hidden" name="uploaded_files_values" id="uploaded_files_values" value="">
		<p>This is an application not a binding confirmation. Agent / Broker has no authority to bind. Policy will be effective only when confirmed by GHINS in writing with a Binder or Policy number.</p>
        <p>Applicant understands that there is a minimum deductible of $1,000 or more for all sand and gravel, flat bed, open trailer, or tow truck policies or as quoted.</p>
        <p>Any person who knowingly engages in fraudulent practices, concealment of facts, false information on any application or statement of claim with the intent to defraud and mislead the insurance company is a felony, punishable by fines and or/ imprisonment.</p>
      
                          <!--      <div class="row-fluid ">  	
                                    <div class="span12 lightblue">
                                        <label>Number of drivers ?</label>
                                        <input pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" mAxelngth="2" required="" type="text" id="num_of_drivers" onchange="add_driver(this.value)" name="driver_count" class="span1" value="">
                                    </div>
                                </div>

                                
                                <div id="row_driver" style="display:none;">
						<div class="row-fluid">  
							<div class="span12 lightblue">
								<table id="driver_table" class="table table-bordered table-striped table-hover">
									<thead>
									  <tr>
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Driver License number</th>
										<th>License Class</th>
										<th>Date Hired</th>
										<th>DOB</th>
										<th>How Many Years Class A license</th>
										<th>License issue state</th>
									
									  </tr>
									  
									</thead>
									<tbody id="add_driver_row">
									</tbody>
								</table>
							</div>
						</div>
					</div>
                                                                <!--span id="add_driver_div"></span-->

                            </fieldset>

                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                       <!--    <fieldset id="section4" class="number_of_owner_box">
                                <legend>Number of Owner</legend>

                                <div class="row-fluid ">  	
                                    <div class="span12 lightblue">
                                        MAYANK
                                        <label>Number of owner of the vehicle or entity?</label>
										<!--END MAYANK
                                        <input required="" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" mAxelngth="2" type="text" id="number_of_owner" name="number_of_owner" onchange="add_owner(this.value)" class="span1" value="">
                                    </div>
                                </div>

  

                                    <div id="row_owner" style="display:none;">
                                        <div class="row-fluid">  
                                            <div class="span12 lightblue">
                                                <table id="owner_table" class="table table-bordered table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Middle Name</th>
                                                            <th>Last Name</th>
                                                            <th> Driver ?</th>
                                                            <th>Driver License number</th>
                                                            <th>License issue state</th>

                                                        </tr>

                                                    </thead>
                                                    <tbody id="add_owner_row">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> 

                                <!--span id="add_driver_div"></span

                            </fieldset>



                       
                       
                       
                       
                       
                       
                       
                            <fieldset id="section5" class="fillings_section_box">
                                <legend>Filing Section</legend>
						<!--MAYANK-->
  
                                    <div class="row-fluid">  
                                        
										<script type="text/javascript">
											$(document).ready(function(){
												$("#filings_need").change(function(){
													if($(this).val()=="yes") {
														$("#number_of_filings_div").show();
														$('#number_of_filings').attr('required', 'required'); 
														
													}else{
														$("#number_of_filings_div").hide();
														$('#number_of_filings').removeAttr('required'); 
														}
												});
											});
											
										/*	function add_filing(val){
													if(val >=0 ){
														if(val == 0)
														{
															$("#filing_table").hide();
														}
														//$("#filing_table").show();
														$("#row_filing").show();
														//$("#add_filing_row").html('');
														var i =1;
														var row = '<tr>\
														<td><input type="text" name="filing_type[]" class="span12 filing_type" required="required" /></td> \
														<td><input type="text" name="filing_numbers[]" class="span12 filing_numbers"  required="required" /></td> \
														</tr>';
														var currcount = $("#add_filing_row tr").length;
														var diff = currcount - val;
														
														 if(diff < 0) {
																while(diff < 0){
																	var v = $('#add_filing_row').append(row) ; 
																	diff++;
																	
																} 
															 }
															 else if(diff > 0)
															 {
																
																 while(diff > 0){
																
																	 var v = $('#add_filing_row tr:last').remove() ; 
																	 diff--;
																 }
															 }	
															
														apply_on_all_elements();
													}
												}*/
										</script>
									<!--	<div class="span12 lightblue">
											<label>Do you need Filing / Filings :</label>
											<select id="filings_need" name="filings_need" required="required">
												<option value="">--Select--</option>
												<option value="yes">YES</option>
												<option value="no">NO</option>
											</select>
                                        </div>
										
										<div class="span12 lightblue" id="number_of_filings_div" style="display:none;">
											<label>Number of Filings :</label>
											<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" mAxelngth="2" id="number_of_filings" name="number_of_filings" class="span1 filing_numbers" onchange="add_filing(this.value)">
										</div>
										
										<div id="row_filing" style="display:none;">
											<div class="row-fluid">  
												<div class="span6 lightblue">
													<table id="filing_table" class="table table-bordered table-striped table-hover">
														<thead>
															<tr>
																<th>Filing Type:</th>
																<th>Filing Num.</th>
															</tr>
														</thead>
														<tbody id="add_filing_row">
														</tbody>
													</table>
												</div>
											</div>
										</div> 
									</div>							
		                                <!--END OF MAYANK-->


                            </fieldset>

							
						
						
				
				
				
				
				
				

                    
                            <div class="row-fluid">  
                                <div class="span lightblue">
                                    <div class="form-actions">
                                   <!-- <input type="submit" name="save" id="save" class="btn btn-primary" value="Submit">
                                        button class="btn btn-primary" type="submit" onclick="return check_vehicle_entry();">Submit Quote</button--
                                         <button class="btn btn-primary" type="submit" onclick="" style="float:right; margin-left:3px;padding:5px 5px;">Submit</button>
									     <!--<button class="btn btn-primary" type="submit" name="saved2" id="" onclick="" style="float:right; margin-left: 5px; padding:5px 5px;">Save</button> 
                                          <button  type="submit" name="back2" class="btn btn-primary" type="" style="float:right;padding:5px 5px;" onclick="" >Back</button>-->

							<!--		<a href="<?php echo base_url();?>application_form/back" class="btn btn-primary" type="" style="float:right;padding:5px 5px;" onclick="">Back</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						</div>
                        <?php $this->load->view('application_commodity'); ?>
 <!--<script src="<?php echo base_url('js');?>/jquery.knob.js"></script>

		<!-- jQuery File Upload Dependencies 
		<script src="<?php echo base_url('js');?>/jquery.ui.widget.js"></script>
		<script src="<?php echo base_url('js');?>/jquery.iframe-transport.js"></script>
		<script src="<?php echo base_url('js');?>/jquery.fileupload.js"></script>
		
		<!-- Our main JS file 
		<script src="<?php echo base_url('js');?>/scripts_file.js"></script>	 -->

<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	//$this->load->view('upload_file');
//	$this->load->view('includes/footer');
}
?>
<script>
	 $(document).ready(function() {
		 
		 $(document).ready(function() { 

$('#app_id_chzn').click(function(){
var v=	$('#app_id').val();
//alert(v);
if(!v){}else{
	$( "#app_id_chzn" ).removeClass( "ui-state-error" );
	}
	});
});

$(document).ready(function() { 

$('.dd').click(function(){
var v=	$('#app_id').val();
//alert(v);
if(!v){}else{
	$( ".dd" ).removeClass( "ui-state-error" );
	}
	});
});


		 
		 
		 if($('#Vehicle_declined').val()=="Yes"){
		$('#addrenew').show();
		}
	  var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		 var _startDate1 = new Date(); //todays date
		var _endDate1 = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		$('#date_from_effective').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to_expiration').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to_expiration').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate1 = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from_effective').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		
		
      $('#date_from_effective1').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate1 = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to_expiration1').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to_expiration1').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from_effective1').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		});
	/*	$("#click_add_phone").click(function(){
          // $("#add_phone").show();
		   $( "#add_phone" ).slideToggle( "slow", function() {
    // Animation complete.
	if ($.trim($('input.textinput.span1').val()) != '')
{
      $('input.textinput.span1.phone').val('');
	  }
	  if ($.trim($('#select_home_p').val()) != '')
{
	   $('#select_home_p').val('Home');
	   }
	   if ($.trim($('#textinput_att_p').val()) != '')
{
	  $('#textinput_att_p').val('');
}
  });
          });
		  	$("#click_add_email").click(function(){
             $( "#add_email1" ).slideToggle( "slow", function() {
    // Animation complete.
	if ($.trim($('input.textinput.email').val()) != '')
{
      $('input.textinput.email').val('');
	  }
	  	if ($.trim($('#select_home_e').val()) != '')
{
	   $('#select_home_e').val('Home');
	   }	if ($.trim($('#textinput_att_e').val()) != '')
{
	  $('#textinput_att_e').val('');
}
  });
          });*/
		  
$("#id_text_app_val").val();
		var arr=$('#id_text_app_val').val();
				myarray = arr.split(',');
		  
		  	if(($.inArray( "Liability", myarray )> -1) || ($.inArray( "Cargo", myarray )> -1) ){
				//$("#losses_lia").show();
				$("#badge_id_2").show();
				$("#badge_id_4").show();
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_2").hide();
				$("#badge_id_4").hide();
				}
				if($.inArray( "Cargo", myarray )> -1){
				//$("#losses_lia").show();
				$("#badge_id_3").show();
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_3").hide();
				}
		  
        </script>
        <script>
	$(document).ready(function(){
		$("#id_text_app_val").val();
		var arr=$('#id_text_app_val').val();
				myarray = arr.split(',');
		  
		  	if(($.inArray( "Liability", myarray )> -1) || ($.inArray( "Cargo", myarray )> -1) ){
				//$("#losses_lia").show();
				$("#badge_id_2").show();
				$("#badge_id_4").show();
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_2").hide();
				$("#badge_id_4").hide();
				}
				if($.inArray( "Cargo", myarray )> -1){
				//$("#losses_lia").show();
				$("#badge_id_3").show();
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_3").hide();
				}
		//when the Add Filed button is clicked
	/*	$("#add_phone_row").click(function (e) {
			//alert();
			//Append a new row of code to the "#items" div
			$("#table_phone").append('<tr><td><input class="textinput span1" type="text" placeholder="Area" id="insured_telephone1" name="insured_telephone_add[]" mAxelngth="3"  pattern="[0-9]+"  value=""  required="required">&nbsp;<input class="textinput span1" id="insured_telephone2" type="text" placeholder="Prefix" name="insured_telephone_add[]" mAxelngth="4"  pattern="[0-9]+"  value=""  required="">&nbsp;<input class="textinput span1" id="insured_telephone3" type="text" placeholder="Sub" name="insured_telephone_add[]" mAxelngth="3"  pattern="[0-9]+"  value=""  required="">&nbsp;<input class="textinput span1" type="text" placeholder="Ext" insured_tel_ext_add[]></td><td><select name="insured_telephone_type[]" class="select-1"><option value="Home">Home</option><option value="Business">Business</option></select></td><td><input class="textinput" type="text" placeholder="" name="insured_telephone_attention[]"></td><td> <button class="btn btn-mini btn-1 remove_phone_row" id="remove_phone_row"><i class="icon icon-remove"></i></button></td></tr>');
		});

	$("body").on("click", ".remove_phone_row", function (e) {
		//alert($(this).parent("div"));
		$(this).closest('tr').remove();
	});*/
	});
	
	/*$(document).ready(function(){
		//when the Add Filed button is clicked
	/*	$("#add_email_row").click(function (e) {
			//alert();
			//Append a new row of code to the "#items" div
			$("#table_email").append('<tr><td><input class="textinput" type="text" placeholder="" id="email" name="insured_email_add[]" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" mAxelngth="30"></td><td><select name="insured_email_type[]" class="select-1"><option value="Home">Home</option><option value="Business">Business</option></select></td><td><input class="textinput" type="text" placeholder="" name="insured_email_attention[]"></td><td> <button class="btn btn-mini btn-1 remove_phone_row" id="remove_phone_row"><i class="icon icon-remove"></i></button></td></tr>');
		});

	$("body").on("click", ".remove_email_row", function (e) {
		//alert($(this).parent("div"));
		$(this).closest('tr').remove();
	});
	});*/
		function audit() {
    var val=$("#audit_name").val();
	
	if(val === "Other"){
			$("#Specify").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify").hide();
		}
		}
	function audit1() {
    var val=$("#audit_name1").val();
	
	if(val === "Other"){
		
			$("#Specify_other").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify_other").hide();
		}
		}
		
		
		function application_select(){
		//remarks_id_show
		

				//alert(myarray[0]);
				//alert($.inArray( "Liability", arr ));
				

// alert(foo);
	/*	var foo = []; 
$('#app_id :selected').map(function(i, selected){ 
  foo.push($(this).val()); 
 
});
var val_app1=foo;
		$("#id_remarks_text").val(val_app1);
	
	var val_app=$("#app_id").val();
		//alert(val_app);
		var val_remark=$("#id_remarks_text").val();
		// var myarray=[];
        // myarray.push(val_app);
		 // alert($(this).attr("selected",true));
		if(val_remark === ""){
		$("#id_remarks_text").val(val_app);		
		}else
		{
		  myarray = val_remark.split(',');
		 //alert(myarray[0]);
	
		//alert(myarray.indexOf( val_app ));
		
		if(myarray.indexOf( val_app )>=0){
			//alert(2);				
		 }else{
		// alert(3);
	//var val_app1=val_remark+","+val_app;
		//$("#id_remarks_text").val(val_app1);
		 }
		}*/
		
		
		}

</script>
<script>
										 function trans_val(){
										 var n=$("#transported_id").val();
										 if(n==="No"){
										 $("#text_no").show();										 
										 }else{
										 $("#text_no").hide();	
										 
										 }
										 }
										 </script>
										 <script>
										 function Company_trucking(){
										 if($("#trucking_comp_id").val() ==="Other"){
										 $("#trucking_specify").show();
										 }else{
										 $("#trucking_specify").hide();
										 }
										 }
										 function certificate_app_val(){
										// alert($("#certificate_id_val").val());
										 if($("#certificate_id_val").val() ==="Yes"){
										 //alert(1);
										 $("#certificate_app_id").show();
										 }else{
										 $("#certificate_app_id").hide();
										// alert(2);
										 }
										 }
										 function owner_vehicle1(){
										 if($("#owner_id").val() ==="No"){
										 
										 $("#insurance_vehicle").show();
										// $("#Policy_id").show();
										 }else{
										 $("#insurance_vehicle").hide();
										// $("#Policy_id").hide();
										// alert(2);
										 }
										 }
										 function insurance_val_func(){
										  if($("#insurance_val_id").val() ==="Yes"){
										 //alert(1);
										 $("#Policy_id").show();
										 }else{
										 $("#Policy_id").hide();
										// alert(2);
										 }
										 
										 }
										 	

function load_trucks(){

	 var n=$("#loaded_trucks_id").val();
										 if(n==="Yes"){
										 $("#loaded_id").show();										 
										 }else{
										 $("#loaded_id").hide();	
										 
										 }

}						
          function primary_coverage(n){
		 // var n=$("#primary_coverage_id").val();
										 if(n==="Yes"){
										 $("#coverage_id").show();										 
										 }else{
										 $("#coverage_id").hide();	
										 
										 }
		  
		  }
					function inspection_func(){
					if($("#inspection_id").val()=== "Yes"){
					  $("#if_yes").show();
					}else{
					  $("#if_yes").hide();
					}
					
					}
					 $(document).ready(function() {

        $('.fancybox').fancybox();

    });
	
	
		function fancybox_close(){
		parent.jQuery.fancybox.close();
		//location.reload(true);
		}
		function reload_close(){
	
		 var l=$('#addrenewed input').length;
		
		 for(i=1;i<l;i++){
		 $('#addrenewed input').val('');
		  $('#addrenewed textarea').val('');
		
		  $('#add_carriers').hide();
		 }
	
		parent.jQuery.fancybox.close();

			
		
		}
		
		function Vehicle_declined1(val){
		if(val=="Yes"){
		$('#addrenew').show();
		}else{
		$('#addrenew').hide();
		}
		
		}
		// var v1=
		var difference = [];
		
		
		
		
		
													  function pd_coverage1(id){
														 
																var val=$("#pd_coverage1"+id).val(); 
		//	alert(val)
			var value='';
			//$('#cargo_amounts'+id).html('');
			if(val==null){
				
				$('#pd_amount'+id).hide();
				}else{
					$('#pd_amount'+id).show();
			for(var i=0; i<val.length;i++){
			if(val[i]==''){}else{
			  value+=' <tr>\
                    <td >'+val[i]+'</td>\
                    <td>\
                      <input class="textinput input-mini" type="text" placeholder="" name="pd_amount_limit'+id+'[]">\
                    </td>\
                    <td>\
                      <input class="textinput input-mini" type="text" placeholder="" name="pd_deductible_limit'+id+'[]">\
                    </td>\
                    <td>\
                      <input class="textinput input-mini" type="text" placeholder="" name="pd_premium_limit'+id+'[]">\
                    </td>\
                  </tr>';
			if(val[i]=='Other'){
				$('#Specify_o1'+id).show();
				}else{
					$('#Specify_o1'+id).hide();
					}
			}
			}
			$('#pd_amounts'+id).html(value);
			}
			}
		
		
		
		
		
		
		
		
		
		/*	  function pd_coverage1(id){
				  
				//alert(val1[id]);  
				var val=[];
			//var	v= $('#removal_name'+id).html(value);
				 val=$("#pd_coverage1"+id).val(); 
      //  alert(val+'   ---   '+val1[id])

			var value='';
			//$('#cargo_amounts'+id).html('');
			var len=$("#pd_amount"+id+" tr").length-1;
        // var ver
			if(val==null){
				//removal_name
				$("#pd_amount"+id).hide();
				}else{
					$('#pd_amount'+id).show();
			for(var i=0; i<val.length;i++){
						if(jQuery.inArray( val[i], val1[id] )>-1){
							
							//$("#pd_amount"+id+" tr:last").remove();
						var ver=val1[id];
						
						for(var k=0; k < ver.length;k++){
							//alert(ver)
						if(jQuery.inArray(ver[k], val )>-1){
							
							
							} else {
								//alert(i);
								//var value12=$("#pd_remove"+i+" td").text();
								//alert(value12);
								//if(ver[k]==value12){
									alert(k);
							$("#pd_remove"+k).remove();
							   alert(ver[k]);	
								//}
						      }
							}
							//alert(val[i]);
							//$("#pd_remove"+id).remove();
							}else{
								
							
								if(val[i]==''){}else{
				  value=' <tr id="pd_remove'+len+'">\
                    <td >'+val[i]+'</td>\
                    <td>\
                      <input class="textinput input-mini" type="text" placeholder="" name="pd_amount_limit'+id+'[]">\
                    </td>\
                    <td>\
                      <input class="textinput input-mini" type="text" placeholder="" name="pd_deductible_limit'+id+'[]">\
                    </td>\
                    <td>\
                      <input class="textinput input-mini" type="text" placeholder="" name="pd_premium_limit'+id+'[]">\
                    </td>\
                  </tr>';
		          $('#pd_amounts'+id).append(value);
				  len++;
				}
								
			}

					/*alert(val[i]);
					
				//alert(compareArrays (val1[id],val));
				//  if(compareArrays (val1[id],val)){
				
			//	alert(val1[id]);

		//	}else{
		
				
			//	}
				
				
				
					if(val[i]=='Other'){
				$('#Specify_o'+id).show();
				}else{
					$('#Specify_o'+id).hide();
					}
				
				}
				
			val1[id]=val;
				
				//alert(val1[id]);
				//var	v= $('#removal_name'+i).text();
				
				
				//alert(valid)
				
			
		
			
			}
			*/


			
			 
			//	 var customerId = $('#mytable tr').find("td:first").html();
				 
							// alert(v);						
						/*	  if(value == "Other")
													 
													  { 
													    $('#Specify_o'+id).show();
														
													  }else{
														  
														 
														  $('#Specify_o'+id).hide();
														  }
													  
													  }*/
													  
													  function combine_func(val){
														//  alert(val);
														  if(val=="Combined Single Limit"){
															//  alert();
															  $('#Combined_limit').show();
															  $('#Split_limit').hide();
															  $('#Split_limit1').hide();
															  }
														  if(val=="Split Limit"){
															  $('#Combined_limit').hide();
															  $('#Split_limit').show();
															  $('#Split_limit1').show();
															  }
														  
														  }
										function loss_payee(id){
												if($("#pd_loss_payee"+id).val()=="Yes"){
												$("#coverage"+id).show();
												$("#pd_amount"+id).show()
												}else{
												$("#coverage"+id).hide();
												$("#pd_amount"+id).hide()
												}
												}
												
												function t_loss_payee(id){
												if($("#t_pd_loss_payee"+id).val()=="Yes"){
												$("#t_coverage"+id).show();
												$("#t_pd_amount"+id).show();
												}else{
												$("#t_coverage"+id).hide();
												$("#t_pd_amount"+id).hide();
												}
												}
												
												
													  function t_pd_coverage1(id){
														 
																var val=$("#t_pd_coverage1"+id).val(); 
		//	alert(val)
			var value='';
			//$('#cargo_amounts'+id).html('');
			if(val==null){
				
				$('#t_pd_amount'+id).hide();
				}else{
					$('#t_pd_amount'+id).show();
			for(var i=0; i<val.length;i++){
			if(val[i]==''){}else{
			  value+=' <tr>\
                    <td >'+val[i]+'</td>\
                    <td>\
                      <input class="textinput input-mini" type="text" placeholder="" name="t_pd_amount_limit'+id+'[]">\
                    </td>\
                    <td>\
                      <input class="textinput input-mini" type="text" placeholder="" name="t_pd_deductible_limit'+id+'[]">\
                    </td>\
                    <td>\
                      <input class="textinput input-mini" type="text" placeholder="" name="t_pd_premium_limit'+id+'[]">\
                    </td>\
                  </tr>';
			if(val[i]=='Other'){
				$('#Specify_o1'+id).show();
				}else{
					$('#Specify_o1'+id).hide();
					}
			}
			}
			$('#t_pd_amounts'+id).html(value);
			}
														
										
												/*		
														 var	v= $('tr  #t_removal_name'+id).html(value);
														  
														  		 if(value == "Other")
													 
													  { 
													    $('#Specify_o1'+id).show();
														
													  }else{
														  
														 
														  $('#Specify_o1'+id).hide();
														  }*/
														  }
		
										 </script>
<script src="<?php echo base_url('js'); ?>/edit_application.js"></script>

