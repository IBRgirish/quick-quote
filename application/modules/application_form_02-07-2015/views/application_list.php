<?php 
	if (strtolower($this->input->server('HTTP_X_Pending_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
   //load_underwriter_list();
    $("#underwriterList").dataTable({
				 "sPaginationType": "full_numbers",
				 "aoColumnDefs" : [ {
					'bSortable' : false,
					'aTargets' : [ 0 ]
				} ]
			 });
});

function load_underwriter_list() {
    $("#underwriterList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo site_url(); ?>quote/ajax_quote_list/"+$("#Pending_by").val(),

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [{
                "sClass": "center"
            },

            //{"sClass": "center"},
            {
                "fnRender": function (oObj) {
                    var a = oObj.aData[1];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[4];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
					var a = oObj.aData[5];
					var d = new Date(a);
					var day = d.getDate();
					var month = d.getMonth() + 1;
					var year = d.getFullYear();
				
                    return (month + "/" + day + "/" + year);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[6];
                    return (a);
				}
			}, {
			"fnRender": function (oObj) {
				if(oObj.aData[8] == 'Released') {
					b = '<a class="btn btn-primary"" href="<?php echo base_url(); ?>ratesheet/rate_by_quote/'+oObj.aData[7]+'">View</a>';
					return (b);
					}	
					else
					{
						b = '-';
						return (b);
					}
				}
				
			}
			
        ]
    });

}
function viewQuotePopup(id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url('quote/QuoteView/');?>/'+id,
    	autoSize: true,
    	closeBtn: true,
    	width: '850',
    	//height: '150',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}
/**By Ashvin Patel popup for Quoteview 13/may/2014**/
</script>

<form action="<?php echo base_url();?>application_form/quote_request/" method="post" accept-charset="utf-8" id="main_form" enctype="multipart/form-data" novalidate="novalidate"/>
<button class="btn btn-primary" name="save"> Add New</button>
<input type="hidden" name="quote_id" value="<?php echo isset($broker_id[0]->quote_id) ? $broker_id[0]->quote_id :'';?>"/>
<input type="hidden" name="edit" value="edit"/>
<div id="data_table">
  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
       <!-- <th class="center" width="10%">Sr.</th>-->
        <th width="13%">Application No.</th>
        <th width="12%">Quote No.</th>
        <th width="15%">Company Name</th>
        <th width="15%">Application Name</th>
		
		<th width="12%">Effective Date</th>
        <th width="12%">Expiration Date</th>
   <!--     <th width="12%">Status</th>-->
         <th width="12%">Audit Name</th>
      <!--   <th>Status</th>-->
		<th width="30%">Action</th>
      </tr>
    </thead>
    <tbody>
								
					<!--<td>1</td>-->
				<?php $list0 = ''; $cnt= 0; krsort($list); foreach($list as $row)
		{
			//print_r($row);
			$cnt++;
			if($list0!=$row->general_id)
			{
				$list0==$row->general_id;
				?>	<tr>
					<td><?php echo $row->general_id; ?></td>
                    <td><?php if($row->quote_id==0) {} else { echo $row->quote_id; }; ?></td>
					<td><?php echo $row->company; ?></td>
					<td><?php echo $row->app_name; ?></td>
					<td><?php echo $row->effective_from; ?></td>
					<td><?php echo $row->expiration_from; ?></td>	
              <!--  <td><?php echo $row->status; ?></td>		-->		
                    <td><?php echo $row->audit_name; ?></td>
                   <!-- <td>Pending</td>-->
											
						<td>
						
                 <a class="btn btn-primary" href="<?php echo base_url(); ?>application_form/quote_request/edit/<?php echo $row->general_id; ?>">Edit</a>
             <!--   <a href="javascript:;" onclick="" class="btn btn">View Request</a>-->
          <!--     	<a class="btn btn-danger" href="javascript:;" onclick="">Delete</a>-->
                        </td>
</tr>
<?php }
}

?>
</tbody>
</table>
</div>