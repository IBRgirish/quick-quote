<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/head_style_script');	
} 
?>
<html lang="en">
  
  <head>
    <title>GHI-6</title>
  </head>
  
  <body>
    <div class="container-fluid">
      <div class="page-header">
        <h1>GHI-6 <small>Driver / Owner exclusion supplement form</small>
        </h1>
      </div>
      <div class="well">
        <label class="control-label control-label-1">Named Insured / DBA</label>
        <input class="textinput span11" type="text" name="" value="Hector Hernandez" placeholder="">
      </div>
      <p>
        <br>It is hereby understood and agreed that all coverages and our obligation to defend under this policy shall not apply nor accrue to the benefit of any INSURED or any third party claimant while any VEHICLE or MOBILE EQUIPMENT described in the policy or
        any other VEHICLE or MOBILE EQUIPMENT, to which the terms of the policy are extended, is being driven, used or operated by any person designated below.</p>
      <p>
        <br>The driver exclusion shall be binding upon every INSURED to whom such policy or endorsements provisions apply while such policy is in force and shall continue to be binding with respect to any continuation, renewal or replacement of such policy by the
        Named Insured or with respect to any reinstatement of such policy within 30 days of any lapse thereof. This DRIVER EXCLUSION provision shall conform to State statutes and laws.</p>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">How many additional persons?</label>
        </span>
        <span class="span1">
          <input class="textinput span12" type="text" name="" placeholder="">
        </span>
        <span class="span7">
          <button class="btn"> <i class="icon icon-plus"></i> Add</button>
        </span>
      </div>
      <div class="well">
        <div>
          <button class="btn pull-right">Delete</button>
        </div>
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label">Name of person excluded</label>
            <input class="textinput" type="text" placeholder="First name" name="">
            <input class="textinput" type="text" placeholder="Middle name" name="">
            <input class="textinput" type="text" placeholder="Last name" name="">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">Date of Birth &nbsp;</label>
            <input class="textinput span7" type="text" name="" placeholder="dd-mm-yyyy">
          </span>
          <span class="span4">
            <label class="control-label pull-left">SSN &nbsp;</label>
            <input class="textinput span10" type="text" name="" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">License number &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">Reason for exclusion</label>
            <textarea class="span12" placeholder="" name=""></textarea>
          </span>
        </div>
      </div>
      <div>
        <div class="form-actions">
          <button class="btn btn-primary">Save</button>
          <button class="btn">Cancel</button>
        </div>
      </div>
    </div>
  </body>

</html>