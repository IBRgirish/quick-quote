<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');	
} 
?>
<script src="<?php echo base_url('js'); ?>/msdropdown/jquery.dd.min.js" type="text/javascript"></script>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('css');?>/msdropdown/dd.css" />   
   <script language="javascript">
		$(document).ready(function(e) {
		try {
		$("#company").msDropDown();
		} catch(e) {
		alert(e.message);
		}
		});
		</script>
<link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
     	<script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
    	<script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
    	<script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script >
		<script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>

		<script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
		<!--script src="<?php echo base_url('js'); ?>/jquery.validateForm.js"></script-->
   

        <style>
            .table thead th {
                text-align: center;
                vertical-align: inherit;
            }

            .table th, .table td {
                font-size: 0.8em;
            }

            .row_added_vehicle{
                margin:0px 0px 5px 0px;
            }
            .status_msg {
                color: #ff0000;
            }
			
			#upload_file_name_second{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second input{
				display: none;
			}
			
			#upload_file_name_second li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second li span{
				width: 15px;
				height: 12px;
				background: url('images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second li.error p{
				color:red;
			}

				#upload_file_name_second_div{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second_div li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second_div input{
				display: none;
			}
			
			#upload_file_name_second_div li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second_div li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second_div li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second_div li span{
				width: 15px;
				height: 12px;
				background: url('images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second_div li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second_div li.error p{
				color:red;
			}

			
			
			#upload_file_name1{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name1 li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name1 input{
				display: none;
			}
			
			#upload_file_name1 li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name1 li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name1 li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name1 li span{
				width: 15px;
				height: 12px;
				background: url('images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name1 li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name1 li.error p{
				color:red;
			}
        </style>	 

<script type="text/javascript">
	
	
			

function apply_on_all_elements(){
		
		$('input').not('.datepick').autotab_magic().autotab_filter();
		$('input').not('.require').autotab_magic().autotab_filter();
		$('select').not('.require').autotab_magic().autotab_filter();
		
		//$('input').attr('required', 'required');
		
		$('.price_format').priceFormat({
		prefix: '$ ',
		centsLimit: 0,
		thousandsSeparator: ','
		});
		
		$(".license_issue_date").mask("99/99/9999");
		
		//  $('input').attr('required', 'required');  
		//$('select').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		
		$('#vehicle_tractor').removeAttr('required');
		$('#vehicle_truck').removeAttr('required');
		$('#zip2').removeAttr( 'required' );
		$('.zip2').removeAttr( 'required' );
		$('.filing_numbers').removeAttr('required');
		$('.filing_type').removeAttr('required');
		$('.last_name').removeAttr('required');
		$('.middle_name').removeAttr('required');
		
		//$('#vehicle_pd').removeAttr('required');
		//$('#vehicle_liability').removeAttr('required');
		//$('#vehicle_cargo').removeAttr('required');
		$('#cab').removeAttr('required');
		$('#tel_ext').removeAttr('required');
		$('#insured_email').removeAttr('required');
		$('#insured_tel_ext').removeAttr('required');
		//alert();
		
		// $('form').h5Validate(); 	
		//alert($(".chosen").val());
}

	$(document).ready(function() {

	
			$('#first_upload').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values').value = '';
				$('#drop a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});
			
			$('#first_upload_second').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values_second').value = '';
				$('#drop_second a').parent().find('input').click();
    		});
			
			$('#first_upload_second_div').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values_second_div').value = '';
				$('#drop_second_div a').parent().find('input').click();
    		});
			
			
		apply_on_all_elements();
		$("#coverage_date").mask("99/99/9999");
		
		$('form').h5Validate();
		
		//$('input').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		//$('input[type="checkbox"]').removeAttr( 'required' );
		$('input[type="file"]').removeAttr( 'required' ); 
		
/* 		$('#coverage_date').datepicker({
			format: 'mm/dd/yyyy'
		}); */
		//$("#coverage_date").mask("99/99/9999");
 	 	/* var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

		var checkin = $('#coverage_date').datepicker({
		onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
		}).on('changeDate', function(ev) {
		// $(this).blur();
		checkin.hide();		 
		}).data('datepicker');   */
		
		/* $('#coverage_date').blur(function() {
  			$('#coverage_date').datepicker("hide");
		});
		 */
		
		//Hide datepicker on Focus Out		
	/* 	$('#coverage_date').blur(function(){
			//setTimeout(function(){$('#coverage_date').datepicker('hide');},100)
		}); */
		
		var date = new Date();
		date.setDate(date.getDate()-0);
				
		 var inputs1 = $('#coverage_date').datepicker({
			startDate: date,
			format: 'mm/dd/yyyy',
			autoclose: true
		});  


		
		$('#dba').change(function(){
			//check_insured_dba_required();			
		});
		
		$('#insured_name').change(function(){
		//alert();
			//check_insured_dba_required();			
		});

		

    var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }

//$('.request_quote').validateForm();
/* 
		var hiddenEls = $("body").find(":hidden").not("script");

	$("body").find(":hidden").each(function(index, value) {
	selctor= this.id;
	if(selctor){
		$( '#'+selctor+' :input').removeAttr("required");
		}
		alert(this.id);
	}); */
	
	/*  $(".request_quote").validateForm({
		ignore: ":hidden",
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			
			$('#loss_report_upload1').click(function() {  
				//var up_id = $('.upload_select:last').attr('id');
				//alert(up_id);
				$('#fileup1_1').click();  
				//$('#'+up_id).click();  
			
			});
			 
			$('#mvr_upload').click(function() { 
			$('#fileup_1').click();  });
 


	});
	
	
			
		function form_sub(){
		/* $(".request_quote").validateForm({
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			//$('.request_quote').validateForm();
			
/* 			if($('#theid').css('display') == 'none'){ 
   $('#theid').show('slow'); 
} else { 
   $('#theid').hide('slow'); 
}
$("br[style$='display: none;']") */


			
	}
	
	function check_insured_dba_required(){
			dba_val = $('#dba').val().trim();
			insured_val = $('#insured_name').val().trim();
			if(dba_val){
				$('#insured_name').removeAttr('required'); 
				$('#insured_middle_name').removeAttr('required');
				$('#dba').attr('required', 'required'); 
			}else if(insured_val){
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').removeAttr('required'); 		
			} else {
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').attr('required', 'required'); 	
			}
		}
		
	
	<!-----updated by amit03-03-2014------>
	$("body").on("change",".rofo:first",function(){
				val = $('.rofo:first').val();
				$('.rofo:first').attr("readonly",false)
				$('.rofothers:first').attr("readonly",false)
				otherval = $('.rofothers:first').val();
				$(".tractor_radius_of_operation").val(val);
				$(".truck_radius_of_operation").val(val);
				if(val == 'other')
				{
					$('.rofothers').show();
					$('.rofothers').val(otherval);
					$('.rofothers').not(":first").attr("readonly",true);
					$(".show_v_others").show();
				}
				
				$(".truck_radius_of_operation").not(":first").attr("readonly",true);
				$(".tractor_radius_of_operation").not(":first").attr("readonly",true);
				$('.show_tractor_others').not(":first").attr("readonly",true);
				$('.show_truck_others').not(":first").attr("readonly",true);
			});
			$("body").on("keyup",".rofothers:first",function(){
				
				$(".rofothers").val($(this).val());
			});
	
			function rofoperation(val , id){
			
				if(id=='1tractor_radius_of_operation')
				{							
					$(".tractor_radius_of_operation").val(val);
					$(".tractor_radius_of_operation").not(":first").attr("disabled",true);
					if(val == 'other'){
						$('.show_tractor_others').show();
						$(".specify_other_cp").not(":first").attr("disabled",true);
					} else {
						$('.show_tractor_others').hide();
					}	
				}
				else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').show();
					} else {
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').hide();
					}	
					
				}
				
				
				
			/*  alert(id);
			alert(val);  */
			
			
			}
			
			
			
			function rofoperation2(val , id){
				
				if(id=='1truck_radius_of_operation')
				{
					$(".truck_radius_of_operation").val(val);
					if(val == 'other'){
						$('.show_truck_others').show();
					} else {
						$('.show_truck_others').hide();
					}
				}	
							else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').show();
					} else {
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').hide();
					}	
					
				}
			}

				
				
				
				
			
					
			
		function check_commodities_other(id){ 
			$("#"+id+" option:selected").each(function(){
			  val = $(this).text(); 
			  if(val == 'other'){ 
					$('.'+id+'_otr').show();
				} else { 
					$('.'+id+'_otr').hide();
				}
			});
		}
	
	
	$(document).ready(function(e) {
        $("body").on("keyup",".commhauled:first",function(){
			$(".commhauled").val($(this).val());
		});
		$("body").on("keyup",".commhauled2:first",function(){
			$(".commhauled2").val($(this).val());
		});
		
		
		 $("body").on("keyup",".cargo_class_price:first",function(){
			$(".cargo_class_price").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_price2:first",function(){
			$(".cargo_class_price2").val($(this).val());
		});
		
		
		$("body").on("keyup",".cargo_class_ded:first",function(){
			$(".cargo_class_ded").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_ded2:first",function(){
			$(".cargo_class_ded2").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp:first",function(){
			$(".specify_other_cp").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp2:first",function(){
			$(".specify_other_cp2").val($(this).val());
		});
			/**Dba required if name if blank or name required if dba is blank By Ashvin Patel 12/may/2014**/

		$('#insured_name').blur(function(){
			//alert();
			var f_name = $(this).val();
			var l_name = $('#insured_last_name').val();
			if(f_name==''||l_name=='')
			{
				$('#dba').attr('required', 'required');
				
			}
			else
			{
				$('#dba').removeAttr('required');
				$('#dba').removeClass('ui-state-error');
			}
		});
	
		$('#insured_last_name').blur(function(){
			//alert();
			var l_name = $(this).val();
			var f_name = $('#insured_name').val();
			if(f_name==''||l_name=='')
			{
				$('#dba').attr('required', 'required');
			}
			else
			{
				$('#dba').removeAttr('required');
				$('#dba').removeClass('ui-state-error');
			}
		});
		$('#dba').blur(function(){
			var dba = $(this).val();
			var f_name = $('#insured_name').val();
			if(dba=='')
			{
				$('#insured_name').attr('required', 'required');
				$('#insured_last_name').attr('required', 'required');
			}
			else
			{
				$('#insured_name').removeAttr('required');
				$('#insured_last_name').removeAttr('required');
				$('#insured_name').removeClass('ui-state-error');
				$('#insured_last_name').removeClass('ui-state-error');
			}
		});
    });
	

		
   
	
	
	
	<!-----updated by amit03-03-2014------>
</script>
       
       <script>
$(document).ready(function(){
	$("#agencies").change(function(){
		//window.location.href = window.location.href + "/" + $(this).val();
		$("#broker_email").val($(this).text());
		
		$.ajax({
			url: '<?php echo base_url();?>manualquote/setpublishersession',
			type: "POST",
			data: {
					action:'via_ajax',
					publisherid : $(this).val()
			},
			success: function(data) {
				
				window.location.reload(true);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(thrownError);
			}
		});
	});
});
function update_unknown(val){
	var owned_veh = $('#trailer_owned_vehicle'+val).val();


	if(owned_veh == 'no'){
		$("#vehicle_year_vh"+val).removeAttr("selected");
		$("#vehicle_year_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#make_model_vh"+val).removeAttr("selected");
		$("#make_model_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#trailer_type_veh"+val).removeAttr("selected");
		$("#trailer_type_veh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
	}else{
		$("#vehicle_year_vh"+val+" option[value='Unknown']").remove();
		$("#make_model_vh"+val+" option[value='Unknown']").remove();
		$("#trailer_type_veh"+val+" option[value='Unknown']").remove();
	}
	/* vehicle_year_vh1-2
	make_model_vh1-2
	trailer_type_veh1-2 */
	//alert();
}


	
	//$("#coverage_date").mask("99/99/9999");
	
	/* ('#dp3').datepicker({
			//format: 'mm/dd/yyyy'
		}); */

    /* $("input").each(function() {
      this.value = "";}) */
    //add_driver();
    //alert($(window).width()); 
	
/* 	$( ".number_limit" ).validate({
rules: {
field: {

range: [1, 99]
}
}
}); */
jQuery(document).ready(function(){
	$(".chzn-select").chosen({ allow_single_deselect: true });
	
	
	
});
</script>

<h2 style="text-align:center;">Application Sheet</h2>
<form action="http://localhost/quote_process/application_form/quote" method="post" accept-charset="utf-8" id="main_form" enctype="multipart/form-data" novalidate="novalidate"/>
<div class="well">

     <div class="row-fluid" style="margin-bottom:5px;">
              <span class="left_float" >
                <label class="control-label control-label-1 control-label-3 changecolor">This is</label>
				
              </span>
			  
		<!--	    <span class="span6 pull-left">
                        <h5 class="heading pull-left changecolor">Company</h5>
						<?php
						
							//echo get_catlog_dropdown_company($field_name ='company', $catlog_type='company_list', $attr = "class='span10 pull-right' required='required'", $other_val=0, $module_name ='', $selected = isset($meeting_details->company) ? $meeting_details->company : '',$otr_relative_field='', $otr_relative_field_attr='');	
						
						?>
						
                        <!--select id="company" name="company" class="span8 pull-right" required >
                            <option value="one" <?php echo ((isset($meeting_details->company) && $meeting_details->company != '' && $meeting_details->company == 'One') ? 'selected="selected"' : '');?>>One</option>
                            <option value="two" <?php echo ((isset($meeting_details->company) && $meeting_details->company != '' && $meeting_details->company == 'Two') ? 'selected="selected"' : '');?>>Two</option>
                            <option value="three" <?php echo ((isset($meeting_details->company) && $meeting_details->company != '' && $meeting_details->company == 'Three') ? 'selected="selected"' : '');?>>Three</option>
                        </select
                    </span>-->
              <span class="left_float span5">
			  <?php echo get_catlog_dropdown_company($field_name ='company', $catlog_type='company_list', $attr = "class='span10 pull-right' required='required'", $other_val=0, $module_name ='', $selected = isset($meeting_details->company) ? $meeting_details->company : '',$otr_relative_field='', $otr_relative_field_attr=''); ?>
                
              </span>
              <span class="">
              <select name="app_name[]" class="span3 chzn-select-deselect left_float " multiple onChange="application_select();" id="app_id" requierd="required">
             <option value="">Select</option>
			 <option value="Liability">Liability</option>
                  <option value="Physical Damage">Physical Damage</option>
                  <option value="Cargo">Cargo</option>
                  <option value="Bobtail">Bobtail</option>
                  <option value="Dead head">Dead head</option>
                  <option value="Non Trucking">Non Trucking</option>
				   <option value="Excess">Excess</option>
                </select>
                <label class="control-label pull-left  left_float">&nbsp;&nbsp;&nbsp;&nbsp;Applications &nbsp; </label>
              </span>
            </div>

                                <fieldset class="head_box">



                    <!-- Form Name -->
                    <legend>General Information</legend>

                    <div class="row-fluid">
                        <div class="span12">

                            <div class="row-fluid"> 
							<div class="span7">
                                <div class="span2 lightblue">
                                    <label>Quote</label>
                                     <input class="textinput input-mini require" type="text" name="Quote_num" value="" placeholder="Quote num." required="required">
                                       
							   </div>
                                <div class="span2 lightblue">
                                    <label></label>
                                   <select name="Quote_num_new" class="select-3 h5-active ui-state-valid" required="required" >
                                       <option value="">Please Select</option>
									   <option value="New">New</option>
                                        <option value="Renewal">Renewal</option>
                                       <option value="Rewritten">Rewritten</option>
                                        </select>
										</div>
                                <div class="span3 lightblue">
                                    <label>Effective Date</label>
                                    <input required="required" maxlength="10" type="text" name="effective_from" id="date_from_effective" class="span12  datepick  " pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}">
								</div>	
								 <div class="span3 lightblue">
                                    <label>Expiration Date</label>
                                   <input required="required" maxlength="10" type="text" name="expiration_from" id="date_to_expiration" class="span12  datepick   " pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}">
								 </div>
								 <div class="row-fluid">  
							
							     <div class="span4 lightblue">
                                    <label>Audit Period</label>
                          <select name="audit_name" class="select-4 error" id="audit_name" onChange="audit();" required="required">
                          <option value="">Please Select</option>
						  <option value="Anually">Anually</option>
                          <option value="Monthly">Monthly</option>
                          <option value="Quaterly">Quaterly</option>
                          <option value="Semi-Anually">Semi-Anually</option>
                          <option value="Other">Other</option>
                          </select>
                                </div>

                                <div class="span6 lightblue" style="display:none" id="Specify">
                                    <label>Specify other&nbsp;</label>
                                   <input class="textinput span12" type="text" name="Specify_oth" placeholder="">
                                </div>

                             <div class="row-fluid" id="remarks_id_show" style="display:none">  
							                                   
							 <div class="span10 lightblue" >
							 <label>Remarks</label>
                             <div class="span11">
							 <input class="textinput span12" type="text" placeholder="" name="remarks" id="id_remarks_text">
							 </div>
							 </div>
							 </div>
								
								 
								
							</div>	
							</div>
							
                                <div class="span4 lightblue">
                                    <label> Broker information</label>
								 <div class="row-fluid">
                  <span class="span12">
                    <label class="control-label pull-left">Name:&nbsp;</label>
                    <select name="" class="select-1">
                      <option value="Broker 1">Broker 1</option>
                    </select>
                  </span>
                </div>
                <label class="control-label">Dba: BROKER_NAME_HERE</label>
                <label class="control-label">Address: BROKER_ADD_HERE</label>
                <label class="control-label">Tel: BROKER_TELEPHONE_HERE</label>
                <label class="control-label">Email: BROKER_EMAIL_HERE</label>
                <label class="control-label">Remarks: BROKER_REMARKS_HERE</label>
								</div>
							

                            </div>	
							
						
							

                           
						



                         
								 
								

								
								
							</div>
							</div></fieldset>
							
						<fieldset class="insured_info_box">
							<legend>Applicant's Section </legend>
						<div class="row-fluid">  
			
								
								<div class="span6 lightblue">
                                    <label> Applicant's Name </label>
									<div class="span12">
                                   <input type="text" maxlength="255" value="" class="span4 first_name ui-state-valid" placeholder="First Name" name="insured_first_name" id="">
									<input type="text" value="" class="span4 middle_name" name="insured_middle_name" placeholder="Middle Name" id="">
									<input type="text" value="" class="span4 last_name" placeholder="Last Name" name="insured_last_name" id="">
                             
									</div>
								</div>
								
								
                                <div class="span6 lightblue">
                                    <label>DBA(if applicable)</label>
                                   <input id="dba" name="dba" type="text" class="span12" maxlength="255" value="">
                                </div>
								
								       
                               

                               

                            </div>	

                            <div class="row-fluid">  
							
							 <div class="span4 lightblue">
                                                                        <label>Address</label> 
									                                    <div class="span12">
										<input class="textinput span12" type="text" placeholder="Address" name="app_address">
                                    </div>
									
                                </div>
							
							  <div class="span3 lightblue">
                                    <label>City</label>
                                    <input id="garaging_city" name="app_city" type="text" class="span12" value="">
                                </div>
								
								 <div class="span2 lightblue">
                                    <label>State</label>
                            	<?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('state_app', $broker_state, $attr); ?>
            

                                </div>
							
								 <div class="span3 lightblue">
                                    <label>Zip</label>
                                    <div class="span12">
                                     <input id="zip1" name="insured_zip[]" type="text" class="span8" maxlength="9" value="" required="required">
                                        <!--<input id="zip2" name="insured_zip[]" type="text" class="span6 zip2" value="">-->
                                    </div>
                                </div>
								
								
							
					  </div>
					  
					<div class="row-fluid">  
					
                                <div class="span5 lightblue">
                                            <div class="row-fluid">
                  <span class="span12">
                    <label class="control-label pull-left">Phone</label>
                    <input type="button" id="click_add_phone" class="dom-link pull-right tdu click_add" value="Click for Additional phones">
                   <!-- <a class="dom-link pull-right tdu" href="#" id="click_add_phone">Click for Additional phones</a>-->
                  </span>
                </div>
				
                <div class="row-fluid">
            				                                    <div class="span12">
										 <input id="insured_telephone1" name="insured_telephone[1]" type="text" class="span3" maxlength="3"  pattern="[0-9]+"  value=""  >
                                        <input id="insured_telephone2" name="insured_telephone[2]" type="text" class="span3"   pattern="[0-9]+"  maxlength="3" value=""  >
                                        <input id="insured_telephone3" name="insured_telephone[3]" type="text" class="span3"   pattern="[0-9]+"  maxlength="4" value=""  >
									 <input id="insured_tel_ext" name="insured_tel_ext" placeholder="Ext" type="text"   class="span3" maxlength="5" value=""  >
                                    
                                    </div>
                </div>
									
                                </div>
                                <div class="span4 lightblue">
                               <div class="row-fluid">
                  <span class="span12">
                    <label class="control-label pull-left">Email</label>
                    <input type="button" id="click_add_email" class="dom-link pull-right tdu click_add" value="Click for Additional email" style="background: white;"/>
                    <!--<a class="dom-link pull-right tdu" href="#" id="click_add_email">Click for Additional email</a>-->
                  </span>
                </div>
				 
                <input class="textinput span12" type="text" placeholder="" id="email" name="insured_email" maxlength="100" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30">
								</div>
					   <div class="span3 lightblue">
                                    <label>Other contact info</label>
                                   <input class="textinput" type="text" placeholder="" name="Other_contact">
                                </div>
								
								
								
                 </div>
				 <div id="add_phone" style="display:none">
				      <h5 class="heading sh">Additional Phones</h5>
            <input type="button" class="icon icon-plus" style="background-color:white;" id="add_phone_row" value="+ Add"/> 
            <div id="items">
            <div>
            <table class="table" id="table_phone">
              <thead>
                <tr>
                  <th>Phone #</th>
                  <th>Type</th>
                  <th>Attention</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
								                                <!--    <div class="span12">
										 <input id="insured_telephone1" name="insured_telephone[1]" type="text" class="span3" maxlength="3"  pattern="[0-9]+"  value=""  required="required">
                                        <input id="insured_telephone2" name="insured_telephone[2]" type="text" class="span3"   pattern="[0-9]+"  maxlength="3" value="" required >
                                        <input id="insured_telephone3" name="insured_telephone[3]" type="text" class="span3"   pattern="[0-9]+"  maxlength="4" value="" required >
									 <input id="insured_tel_ext" name="insured_tel_ext" placeholder="Ext" type="text"   class="span3" maxlength="5" value=""  >
                                    
                                    </div>-->
                  <td>
                    <input class="textinput span1 phone" id="insured_telephone1" name="insured_telephone_add[]" type="text" placeholder="Area" maxlength="3"  pattern="[0-9]+"  value=""  >
                    <input class="textinput span1 phone" id="insured_telephone2" name="insured_telephone_add[]" type="text" placeholder="Prefix" pattern="[0-9]+"  maxlength="3" value="" >
                    <input class="textinput span1 phone" id="insured_telephone3" name="insured_telephone_add[]" type="text" placeholder="Sub" pattern="[0-9]+"  maxlength="4" value="" >
                    <input class="textinput span1 phone" name="insured_tel_ext_add[]" type="text" placeholder="Ext" name="" maxlength="5" value="">
                  </td>
                  <td>
                    <select name="insured_telephone_type[]" class="select-1" id="select_home_p">
					<option value="">Select</option>
                      <option value="Home">Home</option>
                      <option value="Business">Business</option>
                    </select>
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="insured_telephone_attention[]" id="textinput_att_p">
                  </td>
                  <td> <a  class="btn btn-mini btn-1" name="button" id="remove_phone_row"><i class="icon icon-remove"></i> </a>
                  </td>
                </tr>
              </tbody>
            </table>
			</div> 
            </div>
            </div>
			<div id="add_email1" style="display:none">
            <h5 class="heading sh">Additional Email</h5>
			<input type="button" class="icon icon-plus" style="background-color:white;" id="add_email_row" value="+ Add"/>
            
            <table class="table" id="table_email">
              <thead>
                <tr>
                  <th>Email</th>
                  <th>Type</th>
                  <th>Attention</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <input class="textinput email" type="text" placeholder="" name="insured_email_add[]" id="email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30">
                  </td>
                  <td>
                    <select name="insured_email_type[]" class="select-1" id="select_home_e">
					<option value="">Select</option>
                      <option value="Home">Home</option>
                      <option value="Business">Business</option>
                    </select>
                  </td>
                  <td>
                    <input class="textinput"  type="text" placeholder="" name="insured_email_attention[]" id="textinput_att_e">
                  </td>
                  <td> <a  class="btn btn-mini btn-1" name="button" id="remove_phone_row"><i class="icon icon-remove"></i> </a>
                  </td>
                </tr>
              </tbody>
            </table>
			</div>
				
				

</fieldset>

<!--LOSS REPORT -->

<fieldset class="loses_information_box">
	<legend>Loss(es) information</legend>
	<div class="row-fluid ">
		<div class="span10">
		<div class="span6">
		<label class="control-label pull-left pull-left">Does the application garage all vehicles at the above address? &nbsp;&nbsp;</label>
       </div>	
	   <div class="span6">
		<select class="pull-left pull-left-2" name="">
              <option value="">Please select</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select> <strong><label class="control-label pull-left pull-left-1">&nbsp; Edit</label> </strong>
			</div>
		</div>
	</div>
	
    <div class="row-fluid">
    <div class="span12 lightblue">
	<div class="span2">
        <label>Legal Business Status: &nbsp;</label>
		</div>
		<div class="span2">
        <select class="pull-left pull-left-3 select-4" name="audit_name_losses" id="audit_name1" onchange="audit1();">
              <option value="">Please select</option>
			  <option value="Individual">Individual</option>
              <option value="Partnership">Partnership</option>
              <option value="Corporation">Corporation</option>
              <option value="Other">Other</option>
            </select>
			</div>
			<div class="span3">
			 <input class="textinput span6" type="text" placeholder="Other" name="audit_name_other" id="Specify_other" style="display:none;">
			</div>
			<span class="span4">
            <label class="control-label lbl_qt pull-left">SSN or Tax I.D&nbsp;</label>
            <input class="textinput span6" type="text" name="audit_SSN" value="" placeholder="">
          </span>
     </div>
   <!--<div class="row-fluid">
	<div class="span12 lightblue">
	<label>Has the applicant, a business partner or any associate of the applicant ever filed for bankruptcy under any name in the past 10 years?</label>
	</div>
	<div class="row-fluid">
	 <select class="pull-left pull-left-4" name="">
              <option value="Please select">Please select</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
	</div>
	
	</div>-->
	   <div class="row-fluid">
	<div class="span12 lightblue">
	<label>Has the applicant, a business partner or any associate of the applicant ever filed for bankruptcy under any name in the past 10 years?</label>
	</div>
	<div class="row-fluid">
	 <select class="pull-left pull-left-4" name="applicant_ever">
              <option value="">Please select</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
	</div>
	
	</div>
	   <div class="row-fluid">
	<div class="span12 lightblue">
	<label>Has the applicant, a business partner or any associate of the applicant conducted business under any other name in the past five years?</label>
	</div>
	<div class="row-fluid">
	 <select class="pull-left pull-left-4" name="applicant_conducted">
              <option value="">Please select</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
			<label class="control-label pull-left pull-left-1">&nbsp; </label>
	</div>
	
	</div>
	  <div class="row-fluid">
	<div class="span4 lightblue">
        <label>Do you have any losses in the last three years ?</label>
		</div>
		<div class="span2 lightblue" style="margin: 0px;">
        <select name="losses_need" id="losses_need" required="required" class="h5-active ui-state-valid span10" style="margin-left: 0px;">
            <option value="">--Select--</option>
            <option value="yes">YES</option>
            <option value="no">NO</option>
        </select>
    </div>
			<div class="span2 lightblue">
	<label>Did you provide loss run? &nbsp;</label>
	</div>
	<div class="span3 lightblue">
	 <select class="pull-left pull-left-4 span7" id="pull_left_yes" name="losses_name ">
              <option value="">Please select</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
			</div>	
	</div>
	
		<input type="hidden" id="id_text_app_val" name="id_text_app_val[]" value=""/>
    <div id="losses_div" class="losses_div" style="display:none;">
		
    
		<div class="row-fluid " id="losses_lia" style="display:none">
			<div class="span6">
			
						<!--<button class="btn btn-primary" type="button" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >Click To Open</button>-->
			<div class="span4 lightblue losses_head"><p><b>Liability </b></p></div>
			<button class="label label-info" type="button" id="losses_liability" onclick="show_losses(this.id)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="losses_liability_text">Click To Open</label></button>
			<!--<a href="javascript:void(0);" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >
				<div id="< ?php echo $sec_name ?>_icon" class="icon-arrow-down"> </div>
			</a>-->
						
			<!--a href="javascript:void(0);" id="losses_liability" onclick="show_losses(this.id)" >
				<div class="span8 lightblue losses_head" ><p><b>Liability </b></p></div><div id="losses_liability_icon" class="icon-arrow-down"> </div>
			</a-->
			</div> 
			<div id="losses_liability_div" style="display:none;">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span4">
						Number of losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_liability')" class="ssss span3 number_limit" value="">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_liability_table" class="table table-bordered table-striped table-hover" style="display:none">
							<thead> 
							  <tr>
								<th colspan="2">Period of <br> losses</th>
								<th rowspan="2">Liability <br> Amount</th>
								<th rowspan="2">Company</th>
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_liability_row">
							</tbody>
						</table>
					</div>
				</div>
			</div> 
		</div> 
    
		<div class="row-fluid " id="losses_pd" style="display:none">
			<div class="span6">
			
						<!--<button class="btn btn-primary" type="button" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >Click To Open</button>-->
			<div class="span4 lightblue losses_head"><p><b>P.D. </b></p></div>
			<button class="label label-info" type="button" id="losses_pd" onclick="show_losses(this.id)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="losses_pd_text">Click To Open</label></button>
			<!--<a href="javascript:void(0);" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >
				<div id="< ?php echo $sec_name ?>_icon" class="icon-arrow-down"> </div>
			</a>-->
						
			<!--a href="javascript:void(0);" id="losses_pd" onclick="show_losses(this.id)" >
				<div class="span8 lightblue losses_head" ><p><b>P.D. </b></p></div><div id="losses_pd_icon" class="icon-arrow-down"> </div>
			</a-->
			</div> 
			<div id="losses_pd_div" style="display:none;">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span4">
						Number of losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_pd')" class="ssss span3 number_limit" value="">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_pd_table" class="table table-bordered table-striped table-hover" style="display:none">
							<thead> 
							  <tr>
								<th colspan="2">Period of <br> losses</th>
								<th rowspan="2">P.D. <br> Amount</th>
								<th rowspan="2">Company</th>
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_pd_row">
							</tbody>
						</table>
					</div>
				</div>
			</div> 
		</div> 
    
		<div class="row-fluid " id="losses_cargo" style="display:none">
			<div class="span6">
			
						<!--<button class="btn btn-primary" type="button" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >Click To Open</button>-->
			<div class="span4 lightblue losses_head"><p><b>Cargo </b></p></div>
			<button class="label label-info" type="button" id="losses_cargo" onclick="show_losses(this.id)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="losses_cargo_text">Click To Open</label></button>
			<!--<a href="javascript:void(0);" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >
				<div id="< ?php echo $sec_name ?>_icon" class="icon-arrow-down"> </div>
			</a>-->
						
			<!--a href="javascript:void(0);" id="losses_cargo" onclick="show_losses(this.id)" >
				<div class="span8 lightblue losses_head" ><p><b>Cargo </b></p></div><div id="losses_cargo_icon" class="icon-arrow-down"> </div>
			</a-->
			</div> 
			<div id="losses_cargo_div" style="display:none;">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span4">
						Number of losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_cargo')" class="ssss span3 number_limit" value="">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_cargo_table" class="table table-bordered table-striped table-hover" style="display:none">
							<thead> 
							  <tr>
								<th colspan="2">Period of <br> losses</th>
								<th rowspan="2">Cargo <br> Amount</th>
								<th rowspan="2">Company</th>
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_cargo_row">
							</tbody>
						</table>
					</div>
				</div>
			</div> 
		</div> 
			<div class="row-fluid " id="losses_bobtail" style="display:none">
			<div class="span6">
			
						<!--<button class="btn btn-primary" type="button" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >Click To Open</button>-->
			<div class="span4 lightblue losses_head"><p><b>Bobtail </b></p></div>
			<button class="label label-info" type="button" id="losses_bobtail" onclick="show_losses(this.id)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="losses_bobtail_text">Click To Open</label></button>
			<!--<a href="javascript:void(0);" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >
				<div id="< ?php echo $sec_name ?>_icon" class="icon-arrow-down"> </div>
			</a>-->
						
			<!--a href="javascript:void(0);" id="losses_cargo" onclick="show_losses(this.id)" >
				<div class="span8 lightblue losses_head" ><p><b>Cargo </b></p></div><div id="losses_cargo_icon" class="icon-arrow-down"> </div>
			</a-->
			</div> 
			<div id="losses_bobtail_div" style="display:none;">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span4">
						Number of losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_bobtail')" class="ssss span3 number_limit" value="">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_bobtail_table" class="table table-bordered table-striped table-hover" style="display:none">
							<thead> 
							  <tr>
								<th colspan="2">Period of <br> losses</th>
								<th rowspan="2">Bobtail <br> Amount</th>
								<th rowspan="2">Company</th>
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_bobtail_row">
							</tbody>
						</table>
					</div>
				</div>
			</div> 
		</div> 
			<div class="row-fluid " id="losses_dead" style="display:none">
			<div class="span6">
			
						<!--<button class="btn btn-primary" type="button" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >Click To Open</button>-->
			<div class="span4 lightblue losses_head"><p><b>Dead head </b></p></div>
			<button class="label label-info" type="button" id="losses_dead" onclick="show_losses(this.id)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="losses_dead_text">Click To Open</label></button>
			<!--<a href="javascript:void(0);" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >
				<div id="< ?php echo $sec_name ?>_icon" class="icon-arrow-down"> </div>
			</a>-->
						
			<!--a href="javascript:void(0);" id="losses_cargo" onclick="show_losses(this.id)" >
				<div class="span8 lightblue losses_head" ><p><b>Cargo </b></p></div><div id="losses_cargo_icon" class="icon-arrow-down"> </div>
			</a-->
			</div> 
			<div id="losses_dead_div" style="display:none;">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span4">
						Number of losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_dead')" class="ssss span3 number_limit" value="">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_dead_table" class="table table-bordered table-striped table-hover" style="display:none">
							<thead> 
							  <tr>
								<th colspan="2">Period of <br> losses</th>
								<th rowspan="2">Dead head <br> Amount</th>
								<th rowspan="2">Company</th>
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_dead_row">
							</tbody>
						</table>
					</div>
				</div>
			</div> 
		</div> 
    		<div class="row-fluid " id="losses_trunk" style="display:none">
			<div class="span6">
			
						<!--<button class="btn btn-primary" type="button" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >Click To Open</button>-->
			<div class="span4 lightblue losses_head"><p><b>Non Trucking</b></p></div>
			<button class="label label-info" type="button" id="losses_trunk" onclick="show_losses(this.id)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="losses_trunk_text">Click To Open</label></button>
			<!--<a href="javascript:void(0);" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >
				<div id="< ?php echo $sec_name ?>_icon" class="icon-arrow-down"> </div>
			</a>-->
						
			<!--a href="javascript:void(0);" id="losses_cargo" onclick="show_losses(this.id)" >
				<div class="span8 lightblue losses_head" ><p><b>Cargo </b></p></div><div id="losses_cargo_icon" class="icon-arrow-down"> </div>
			</a-->
			</div> 
			<div id="losses_trunk_div" style="display:none;">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span4">
						Number of losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_trunk')" class="ssss span3 number_limit" value="">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_trunk_table" class="table table-bordered table-striped table-hover" style="display:none">
							<thead> 
							  <tr>
								<th colspan="2">Period of <br> losses</th>
								<th rowspan="2">Non Trucking<br> Amount</th>
								<th rowspan="2">Company</th>
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_trunk_row">
							</tbody>
						</table>
					</div>
				</div>
			</div> 
		</div> 
				<div class="row-fluid " id="losses_excess" style="display:none">
			<div class="span6">
			
						<!--<button class="btn btn-primary" type="button" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >Click To Open</button>-->
			<div class="span4 lightblue losses_head"><p><b>Excess</b></p></div>
			<button class="label label-info" type="button" id="losses_excess" onclick="show_losses(this.id)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="losses_excess_text">Click To Open</label></button>
			<!--<a href="javascript:void(0);" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >
				<div id="< ?php echo $sec_name ?>_icon" class="icon-arrow-down"> </div>
			</a>-->
						
			<!--a href="javascript:void(0);" id="losses_cargo" onclick="show_losses(this.id)" >
				<div class="span8 lightblue losses_head" ><p><b>Cargo </b></p></div><div id="losses_cargo_icon" class="icon-arrow-down"> </div>
			</a-->
			</div> 
			<div id="losses_excess_div" style="display:none;">
				<div class="row-fluid ">
					<div class="span12">
						<div class="span4">
						Number of losses in last 3 years ? 
						<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" onchange="add_losses(this.value, 'losses_excess')" class="ssss span3 number_limit" value="">
						</div>
											</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="losses_excess_table" class="table table-bordered table-striped table-hover" style="display:none">
							<thead> 
							  <tr>
								<th colspan="2">Period of <br> losses</th>
								<th rowspan="2">Excess <br> Amount</th>
								<th rowspan="2">Company</th>
								<th rowspan="2">General Agent </th>
								<th rowspan="2">Date of Loss</th>
							  </tr>
							  <tr colspan="2">
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_losses_excess_row">
							</tbody>
						</table>
					</div>
				</div>
			</div> 
		</div> 


</div>
		
			


</fieldset>
 <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div> 

<!--
                            <div class="row-fluid " >  	
                                <div class="span12 lightblue">
                                    Number of losses in last 3 years ? 
                                    <input type="text" onchange="add_losses(this.value)" class="span1" value="" />
                                </div>
                            </div>	
                                                        
<div class="row-fluid">  
							<div class="span12 lightblue">
								<table id="losses_table" class="table table-bordered table-striped table-hover" style="display:none" >
									<thead>
									  <tr>
										<th colspan="2" >Period of <br> losses</th>
										<th rowspan="2" >Liability <br> Amount</th>
										<th rowspan="2" >Physical Damage <br> Amount</th>
										<th rowspan="2" >Cargo <br> Amount </th>
									  </tr>
									  <tr colspan="2" >
										<th>From (mm/dd/yyyy)</th>
										<th>To (mm/dd/yyyy)</th>
									  </tr>
									</thead>
									<tbody id="add_losses_row_old" >
									</tbody>
								</table>
							</div>
						</div>


--> 

                            <div class="row-fluid">  
                                <div class="span6 lightblue">
                                    <label class="loses_information_box" style="float:left;">Attach Loss Report(s) for all Accident(s)</label>
                                    <!--span class="btn btn-file">Upload<input type="file" multiple="multiple" name="file_sec_1[]" id="file_sec_1" /></span-->
									<!-- input type="button" value="Upload" id="loss_report_upload1" class="btn btn-primary" style="cursor:pointer;" -->
									
									<!--<label class="cabinet label label-info" >
										<div id="upload_bottom1"><input type="hidden" id="upload_count1" value="1" ></div>
										<input onChange="add_upload_1(this.id);" type="file" name="file_sec_1[]" id="fileup1_1" class="upload_browse_multi1 file" title="Select file to upload" />
									</label>-->
									<input type="button" id="first_upload" value="Upload">
									

									<input type="hidden" name="uploaded_files_values" id="uploaded_files_values" value="">
									<ul id="upload_file_name1"></ul>
                                </div>
                                <div class="span6 lightblue"></div>
                            </div>
                              <div class="row-fluid">
          <span class="span12">
            <label class="control-label control-label-1">Disclaimer HERE</label>
          </span>
        </div>
 

                            <!-- Vehicle Schedule Section Start -->	
                            <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div>
                            <fieldset class="section2 vehicle_schedule_box">
                                <legend>Commodities section</legend>

                           
                                <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div>
                                <div class="row-fluid ">  	
                                    <div class="span12 lightblue">
                                       <div class="span3">
                                            <label>Number of commodities hauled? &nbsp;</label>
											</div>
											<div class="span3">
                                            <input class="textinput input-mini" type="text" placeholder="" name="commodity_number">
											</div> 

                                         </div>
                                </div>	
                                 
                                <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div> 
<div class="row-fluid">
            <span class="span8">
              <table class="table">
                <thead>
                  <tr>
                    <th></th>
                    <th>Commodity</th>
                    <th>% (Cargo)</th>
                    <th>Value (Cargo)</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="td-1"></td>
                    <td>
                      <select name="aapli_name" id="appli_id" onchange="aapli_other();">
                        <option value="Appliances">Appliances</option>
						<option value="Other">Other</option>
                      </select>
                    </td>
                    <td>
                      <input class="textinput input-mini" type="text" placeholder="" name="commodity_cargo_per">
                    </td>
                    <td>
                      <input class="textinput input-mini" type="text" placeholder="" name="commodity_cargo_val">
                    </td>
                  </tr>
                  <tr id="other_tr" style="display:none" >
                    <td class="td-1"></td>
                    <td>
                      <label class="control-label pull-left">Other&nbsp;</label>
                      <input class="textinput span9" type="text" placeholder="" name="aapli_name_other">
                    </td>
                    <td>
                      <input class="textinput input-mini" type="text" placeholder="" name="commodity_other_per">
                    </td>
                    <td>
                      <input class="textinput input-mini" type="text" placeholder="" name="commodity_other_val">
                    </td>
                  </tr>
                </tbody>
              </table>
            </span>
           
          </div>
		  <div class="row-fluid ">  	
                                    <div class="span12 lightblue">
                                       <div class="span4">
                                            <label>Does the applicant own the cargo transported? &nbsp;&nbsp;</label>
											</div>
											<div class="span3">
                                             <select class="pull-left pull-left-3" name="transported_name" id="transported_id" onChange="trans_val();">
               <option value="">Please select</option>
			   <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
											</div> 

                                         </div>
										 
				 <textarea class="span12 span12-1" id="text_no" placeholder="If no, who owns it" name="transported_desc" style="display:none"></textarea>
                                </div>	
	
       <div class="row-fluid mt_15">
            <span class="span6">
              <label class="control-label pull-left" >Does the applicant haul any kind of Hazmat? &nbsp;</label>
              <select class="pull-left pull-left-4 span3" name="hazmat_name"  >
			  <option value="">Please select</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label pull-left">Does the applicant transport passengers? &nbsp;</label>
              <select class="pull-left pull-left-1 span3" name="passengers_name">
			  <option value="">Please select</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
          </div>
		    <div class="row-fluid" style="display:none" id="filings_id">
			<div class="row-fluid ">
			  <span class="badge">Liability</span>
			</div>
			<div class="row-fluid ">
            <span class="span3">
              <label class="control-label pull-left span8">Do you need any filings?</label>
              <select class="pull-left pull-left-5 span4" name="filings_name">
			  <option value="">Please select</option>
                <option value="Yes">Yes</option>
				<option value="Yes">No</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label pull-left span7">&nbsp; Number of filings : &nbsp;</label>
              <input class="textinput span3" type="text" placeholder="" name="filings_number">
            </span>
            <span class="span5">
              <div class="row-fluid">
                <span class="span5">
                  <label class="control-label span6">Filing Type</label>
                  <input class="textinput span5" type="text" placeholder="" name="filings_type">
                </span>
                <span class="span4">
                  <label class="control-label span6">Filing Num</label>
                  <input class="textinput span6" type="text" placeholder="" name="filings_num">
                </span>
                <span class="span3">
                  <label class="control-label span5">State</label>
                <?php
									$attr = "class='span7 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('state_fill', $broker_state, $attr); ?>
                </span>
              </div>
            </span>
			</div>
          </div>
         
		          <div class="row-fluid">
            <span class="span6">
              <label class="control-label pull-left span4" > Does the applicant haul for</label>
              <select class="pull-left pull-left-6 span4" name="trucking_comp_name" id="trucking_comp_id" onChange="Company_trucking();">
                <option value="Trucking Company">Trucking Company</option>
                <option value="Shippers">Shippers</option>
                <option value="Other">Other</option>
              </select>
              <input class="textinput span3" type="text" placeholder="Please specify" id="trucking_specify" name="trucking_comp_other" style="display:none">
            </span>
            <span class="span6">
              <label class="control-label pull-left span7"> &nbsp;Does the applicant need any certificate(s)? &nbsp;</label>
              <select class="pull-left pull-left-7 span3" name="certificate_name" id="certificate_id_val" onChange="certificate_app_val();">
               <option value="">Please select</option>
			   <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
          </div>
		  <div id="certificate_app_id" style="display:none;">
		  <div class="row-fluid">
            <span class="span6">
              <label class="control-label pull-left span4">How many certificates?&nbsp;</label>
              <input class="textinput span3" type="text" placeholder="" name="cert_num">
            </span>
          </div>
		  
		    <div class="row-fluid">
            <span class="span3">
              <label class="control-label pull-left span5">Company Name &nbsp;</label>
              <input class="textinput span7" type="text" placeholder="" name="cert_comp">
            </span>
            <span class="span3">
              <label class="control-label span3">Attention &nbsp;</label>
              <input class="textinput span7" type="text" placeholder="" name="cert_attent">
            </span>
            <span class="span4">
              <label class="control-label span3">Address</label>
              <input class="textinput span7" type="text" placeholder="" name="cert_add">
            </span>
            <span class="span2">
              <label class="control-label span3">City</label>
              <input class="textinput span7" type="text" placeholder="" name="cert_city">
            </span>
          </div>
		  
		 <div class="row-fluid">
            <span class="span2">
              <label class="control-label span3">Email</label>
              <input class="textinput span9" type="text" placeholder="" name="cert_email" id="email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30">
            </span>
            <span class="span4" >
              <label class="control-label span2">Phone</label>
              <input class="textinput span2" type="text" placeholder="Area" name="cert_telephone[1]" id="insured_telephone1" maxlength="3"  pattern="[0-9]+"  value=""  >
              <input class="textinput span2" type="text" placeholder="Prefix" name="cert_telephone[2]" id="insured_telephone2" maxlength="3"  pattern="[0-9]+"  value=""  >
              <input class="textinput span2" type="text" placeholder="Sub" name="cert_telephone[3]" id="insured_telephone3" maxlength="4"  pattern="[0-9]+"  value=""  >
              <input class="textinput span2" type="text" placeholder="Ext" name="cert_telephone_ext" maxlength="5">
            </span>
            <span class="span3" style="float:left; margin:0px;">
              <label class="control-label span2">Fax</label>
              <input class="textinput span3" type="text" placeholder="Area" name="cert_fax[1]" maxlength="3">
              <input class="textinput span3" type="text" placeholder="Prefix" name="cert_fax[2]" maxlength="3">
              <input class="textinput span3" type="text" placeholder="Sub" name="cert_fax[3]" maxlength="5">
            </span>
            <span class="span2" style="float:left; margin:0px;">
              <label class="control-label span3">State</label>
             <?php
									$attr = "class='span8 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('state_cert', $broker_state, $attr); ?>
            </span>
            <span class="span1" >
              <label class="control-label span4">Zip</label>
              <input class="textinput span8" type="text" placeholder="" name="cert_zip" >
            </span>
          </div>
		  
		  <div class="row-fluid">
            <span class="span6">
              <label class="control-label span5">Any Additional insured(s)?</label>
              <select class="pull-left pull-left-5 span3" name="Additional_ins" style="float:left; margin:0px;">
               <option value="">Please select</option>
			   <option value="Yes">Yes</option>
				<option value="No">No</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label"></label>
            </span>
          </div>
         </div>
		   <div class="row-fluid mt_15">
            <span class="span6">
              <label class="control-label pull-left span8">Is the scheduled vehicle(s) driven by the owner(s)?&nbsp;</label>
              <select class="pull-left pull-left-2 span4" name="owner_vehicle" onChange="owner_vehicle1();" id="owner_id" >
               <option value="">Please select</option>
			   <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>              
            </span>
			<div id="insurance_vehicle" style="display:none;">
            <span class="span6" >
              <label class="control-label pull-left span8">Do you have workmen &nbsp;compensation insurance?&nbsp;</label>
              <select class="pull-left pull-left-2 span4" name="insurance_val" onChange="insurance_val_func(); " id="insurance_val_id">
                <option value="">Please select</option>
				<option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
			</div>
          </div>
		
		     <div class="row-fluid" id="Policy_id" style="display:none;">
            <span class="span2">
              <label class="control-label">Policy Number</label>
              <input class="textinput span12" type="text" placeholder="" name="ins_Policy_num">
            </span>
            <span class="span5">
              <label class="control-label">Insurance company for workmen compensation</label>
              <input class="textinput span12" type="text" placeholder="" name="ins_Policy_comp">
            </span>
            <span class="span2">
              <label class="control-label">Effective Date</label>
              <input  maxlength="10" type="text" name="effective_from_ins" id="date_from_effective1" class="span12  datepick  " pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}">
            </span>
            <span class="span2">
              <label class="control-label">Expiring Date</label>
              <input maxlength="10" type="text" name="expiration_from_ins" id="date_to_expiration1" class="span12  datepick   " pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}">
            </span>
          </div>
		
		 
		  
		      <div class="row-fluid">
            
              
             
			   <div class="span6 lightblue">
                                    <label class="" style="float:left;">Please send the certificates &nbsp;</label>
                                    <!--span class="btn btn-file">Upload<input type="file" multiple="multiple" name="file_sec_1[]" id="file_sec_1" /></span-->
									<!-- input type="button" value="Upload" id="loss_report_upload1" class="btn btn-primary" style="cursor:pointer;" -->
									
									<!--<label class="cabinet label label-info" >
										<div id="upload_bottom1"><input type="hidden" id="upload_count1" value="1" ></div>
										<input onChange="add_upload_1(this.id);" type="file" name="file_sec_1[]" id="fileup1_1" class="upload_browse_multi1 file" title="Select file to upload" />
									</label>-->
									<input type="button" id="first_upload_second" value="Upload">
									

									<input type="hidden" name="uploaded_files_values_second" id="uploaded_files_values_second" value="">
									<ul id="upload_file_name_second"></ul>
                                </div>
            
          </div>
		     <div class="row-fluid mt_15">
            <span class="span12">
              <label class="control-label pull-left span4"> Does the owner(s) have a commercial license(s)? &nbsp;</label>
              <select class="pull-left pull-left-8 span2" name="owner_license">
                <option value="">Please select</option>
				<option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
          </div>
		      <div class="row-fluid">
            <span class="span12">
              <label class="control-label span8">Is applicant the registered owner(s) of all the vehicle(s) listed on this application, other than the unidentified trailer(s)? &nbsp;</label>
              <select class="pull-left pull-left-1 span2" name="owner_applicant">
                <option value="">Please select</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
          </div>
		     <div class="row-fluid ">
            <span class="span12">
              <label class="control-label span6">Does the applicant own any vehicle(s) not scheduled on this application?</label>
              <select class="pull-left pull-left-1 span2" name="applicant_scheduled">
                <option value="">Please select</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
          </div>
		    <div class="row-fluid mt_15">
            <span class="span12">
              <label class="control-label span6">Does the applicant rent, lease, or sub haul vehicle(s) to others? &nbsp;</label>
              <select class="pull-left pull-left-1 span2" name="applicant_rent">
                <option value="">Please select</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
          </div>
		  
		  
          <div class="row-fluid " style="display:none;" id="badge_id_2">
		  <span class="badge">Liability, Cargo</span> 
            <div class="span12" style="margin: 0px;">
              <label class="control-label span7">Does the applicant hire vehicle(s), owner operator(s) or vehicle(s) owned by other parties? &nbsp;</label>
              <select class="pull-left pull-left-1 span2" name="owner_operator">
                <option value="">Please select</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </div>
          </div>
		  
		     <div class="row-fluid ">
            <span class="span12">
              <label class="control-label pull-left span4">Does the applicant broker loads out to others? &nbsp;</label>
              <select class="pull-left pull-left-1 span2" name="broker_loads">
                <option value="">Please select</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
          </div>
		  
		    <div class="row-fluid ">
            <span class="span12">
              <label class="control-label pull-left">Does the applicant participate in a formal safety inspection? &nbsp;&nbsp;</label>
              <select class="pull-left pull-left-1" name="inspection__name" id="inspection_id" onchange="inspection_func();">
                <option value="">Please select</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
			  &nbsp;
              <input class="textinput" type="text" id="if_yes" style="display:none;" placeholder="If yes" name="if_yes_name">
            </span>
          </div>
		     <div class="row-fluid">
            <span class="span4">
              <label class="control-label"> Estimated financial worth</label>
              <input class="textinput price_format" type="text" name="Estimated_financial" placeholder="$"  maxlength="15">
            </span>
            <span class="span4">
              <label class="control-label">Gross receipts last year</label>
              <input class="textinput price_format" type="text" name="Gross_receipts" placeholder="$"  maxlength="15">
            </span>
            <span class="span4">
              <label class="control-label">Estimated next year</label>
              <input class="textinput price_format" type="text" name="Estimated_next" placeholder="$"  maxlength="15">
            </span>
          </div>

                                <!--div class="row-fluid " >  	
                                <div class="span12 lightblue">
                                <label>How Many Vehicles to Schedule ?</label>
                                <input type="text" onchange="add_vehicle(this.value)" class="span1" />
                                </div>
                                </div-->

                               <!-- <span id="add_vehicle_div"></span>


                                <span id="add_tractor_div"></span>


                          

                             Vehicle Schedule Section End -->	


                            <!-- Trailer Schedule Section Start -->	




                            <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div>

                            <!--div class="row-fluid">  
                                <div class="span6 lightblue">
                                    <label >Owner Driven</label>

                                    <select id="owner_driven_tr" name="owner_driven_tr" >
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                </div>
                                <div class="span6 lightblue"></div>
                            </div-->

                          <!--  <div class="row-fluid">  
                                <div class="span12 lightblue">
                                    <label style="float:left">Attach MVR's for all Driver(s) and Owner(s) no more than 30 days old</label>
									
									
									
		input type="button" value="Upload" id="mvr_upload" class="btn btn-primary" style="cursor:pointer;" -->
		<!--<label class="cabinet label label-info" >
			<div id="upload_bottom"><input type="hidden" id="upload_count" value="1" ></div>
			<input  style="cursor:pointer" onChange="add_upload_3(this.id);" type="file" name="file_sec_3[]" id="fileup_1" class="upload_browse_multi file" title="Select file to upload" />
		</label>
									<input type="button" id="first_upload_second" value="Upload">
									

									<input type="hidden" name="uploaded_files_values_second" id="uploaded_files_values_second" value="">
									<ul id="upload_file_name_second"></ul>        
									
									<!--span class="label label-info" style="width:42px; margin-bottom:5px; cursor:pointer;" > Upload
									<input  style="cursor:pointer" onchange="add_upload_3(this.id);" type="file" name="file_sec_3[]" id="fileup_1" class="upload_browse_multi file" title="Select file to upload" />
																	
									<div id="upload_bottom"><input type="hidden" id="upload_count" value="1" ></div>
									</span>
									
									<div id="upload_file_name"></div>
									
									
								    <!--span class="btn btn-file">Upload<input type="file" multiple="multiple" name="file_sec_3[]" id="file_sec_3" /></span-->
                                    <!--MAYANK
									<label><b>Disclaimer :</b> Please attach the MVR for each driver, otherwise the quote is an indication only and not a formal quote. </label>
									<!--END MAYANK
                                </div>
                            </div> -->
                            </fieldset>


                            <fieldset id="section4" class="schedule_of_drivers_box">
                                <legend>Vehicle schedule section</legend>

								
								<div class="row-fluid">
          <span class="span12 ">
            <label class="control-label pull-left span8">Has the applicant ever have a risk declined, non-renewed, or cancelled in the past five years &nbsp;</label>
            <select class="pull-left pull-left-1 " name="Vehicle_declined">
              <option value="">Please select</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
            <label class="control-label pull-left control-label-1">&nbsp; Edit</label>
          </span>
		  		  
        </div>
		
		   <div class="row-fluid">
          <span class="span6">
            <label class="control-label pull-left span3"> Vehicles Schedule &nbsp;</label>
         <!--   <label class="control-label pull-left span4"> Number of vehicles type &nbsp;</label>
            <input class="textinput span2" type="text" placeholder="" name="vehicles_type">-->
          </span>
        </div>
		     <div class="row-fluid">
          <span class="span1">
            <label class="control-label pull-left span3">Type&nbsp;</label>
         
          </span>
          <span class="span2">
            <label class="control-label pull-left span5"># Truck &nbsp;</label>
             <input pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" type="text" name="vehicle_count" id="vehicle_truck" onchange="add_truck1(this.value)" class="span4 ui-state-valid" value="">
          </span>
     
          <span class="span2">
            <label class="control-label pull-left span5"># Tractor &nbsp;</label>
            <input required  pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2"  type="text"  name="vehicler_count" id="vehicle_tractor" onChange="add_tractor1(this.value)" class="span4" value="" />
          </span>
		            <span class="span4">
					 <label class="control-label pull-left span7">Are you mono hauling trailers? &nbsp;</label>
					  <select class="pull-left pull-left-1 span5" name="Vehicle_hauling">
                       <option value="">Please select</option>
                       <option value="Yes">Yes</option>
                       <option value="No">No</option>
                    </select>
					</span>
					<span class="span3">
					 <label class="control-label pull-left span7">How many trailers? &nbsp;</label>
					   <input class="textinput span3" type="text" placeholder="" name="vehicles_trailers">
					</span>
        </div>
		

		<div class="well">
		<!--start truck-->
		<span id="add_tractor_div"></span>
		<span id="add_vehicle_div"></span>
		
		<div id="row_vehicle" style="display:none; ">
          <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="select_vehicle_year[]" class="select_new">
                 <option value="1985">1985</option>
                 <option value="1986">1986</option>
                                                                <option value="1987">1987</option>
                                                                <option value="1988">1988</option>
                                                                <option value="1989">1989</option>
																<option value="1990">1990</option>
                                                                <option value="1991">1991</option>
                                                                <option value="1992">1992</option>
                                                                <option value="1993">1993</option>
                                                                <option value="1994">1994</option>
                                                                <option value="1995"> 1995</option>
                                                                <option value="1996">1996</option>
                                                                <option value="1997">1997</option>
                                                                <option value="1998">1998</option>
                                                                <option value="1999">1999</option>
                                                                <option value="2000">2000</option>
                                                                <option value="2001">2001</option>
                                                                <option value="2002">2002</option>
                                                                <option value="2003">2003</option>
                                                                <option value="2004">2004</option>
                                                                <option value="2005">2005</option>
                                                                <option value="2006">2006</option>
                                                                <option value="2007">2007</option>
                                                                <option value="2008">2008</option>
                                                                <option value="2009">2009</option>
                                                                <option value="2010">2010</option>
                                                                <option value="2011">2011</option>
                                                                <option value="2012">2012</option>
                                                                <option value="2013">2013</option>
																<option value="2014">2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="select_model[]" class="select_new">
                <option value="">Select</option>
				<option value="Freightliner">Freightliner</option>
                <option value="International">International</option>
                 <option value="Isuzu">Isuzu</option>
                 <option value="Kenworth">Kenworth</option>
                 <option value="Mack">Mack</option>
                 <option value="">Peterbilt</option>
                 <option value="Peterbilt">Sterling</option>
                 <option value="Western Star">Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="select_GVW[]" class="select_new">
                   <option>0-10,000 pounds</option>
                                                                <option>10,001-26,000 pounds</option>
                                                                <option>26,001-40,000 pounds</option>
                                                                <option>40,001-80,000 pounds</option>
                                                                <option>Over 80,000 pounds</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" name="select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" placeholder="" name="select_Operation[]">
            </span>
          </div>
      
          <div class="row-fluid">
            <span class="span4">
              <label class="checkbox">
                <input type="checkbox" value="Yes" name="select_ch_radius[]">
                <span>Same Radius of Operation for all vehicles</span>
              </label>
            </span>
            <span class="span3">
              <label class="checkbox">
                <input type="checkbox" value="Yes" name="select_ch_cities[]">
                <span>Same Cities for all vehicles</span>
              </label>
            </span>
            <span class="span5">
              <label class="checkbox">
                <input type="checkbox" value="Yes" name="select_ch_applicant[]">
                <span>Same Applicant's Business for all vehicles</span>
              </label>
            </span>
          </div>
          <div class="row-fluid">
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" placeholder="" name="select_special[]">
            </span>
            <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="select_Axels[]" class="select_new">
                <option value="Select">Select</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">Number of drivers:&nbsp;</label>
              <input class="textinput input-mini" type="text" placeholder="" name="select_num_drivers[]">
            </span>
            <span class="span3">
              <label class="control-label"></label>
            </span>
          </div>
		 </div>
         <!-- <div class="row-fluid">
            <span class="span12">
              <label class="control-label pull-left">Do you pull equipment with this vehicle? &nbsp;</label>
              <select name="" class="select_new">
                <option value="Yes">Yes</option>
              </select>
			  <select type="text" name="vehicle_trailer_no[]" id="vehicle_trailer" onchange="add_trailer1(this.value,1 );" class="span3 ui-state-valid">
			  <option value="0">None</option>
			  <option value="1">Single</option>	
			  <option value="2">Double</option>	
			  <option value="3">Triple</option>	
			  </select>
              <select name="" class="select-7">
                <option value="Single">Single</option>
                <option value="Double">Double</option>
                <option value="Other">Triple</option>
              </select>
            </span>
          </div>
          <div class="row-fluid">
            <span class="span6">
              <label class="control-label pull-left">Do you need Physical Damage Coverage? &nbsp;</label>
              <select name="">
                <option value="Yes">Yes</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label pull-left">Do you need Cargo Coverage? &nbsp;</label>
              <select name="" class="select_new">
                <option value="Yes">Yes</option>
              </select>
            </span>
           </div>-->
		  
		  <!--end truck-->
		  <!--start tractor-->
		
		
		<div id="row_tractor" style="display:none; ">
           <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="t_select_vehicle_year[]" class="select_new">
                 <option value="1985">1985</option>
                 <option value="1986">1986</option>
                                                                <option value="1987">1987</option>
                                                                <option value="1988">1988</option>
                                                                <option value="1989">1989</option>
																<option value="1990">1990</option>
                                                                <option value="1991">1991</option>
                                                                <option value="1992">1992</option>
                                                                <option value="1993">1993</option>
                                                                <option value="1994">1994</option>
                                                                <option value="1995"> 1995</option>
                                                                <option value="1996">1996</option>
                                                                <option value="1997">1997</option>
                                                                <option value="1998">1998</option>
                                                                <option value="1999">1999</option>
                                                                <option value="2000">2000</option>
                                                                <option value="2001">2001</option>
                                                                <option value="2002">2002</option>
                                                                <option value="2003">2003</option>
                                                                <option value="2004">2004</option>
                                                                <option value="2005">2005</option>
                                                                <option value="2006">2006</option>
                                                                <option value="2007">2007</option>
                                                                <option value="2008">2008</option>
                                                                <option value="2009">2009</option>
                                                                <option value="2010">2010</option>
                                                                <option value="2011">2011</option>
                                                                <option value="2012">2012</option>
                                                                <option value="2013">2013</option>
																<option value="2014">2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="t_select_model[]" class="select_new">
                <option value="">Select</option>
				<option value="Freightliner">Freightliner</option>
                <option value="International">International</option>
                 <option value="Isuzu">Isuzu</option>
                 <option value="Kenworth">Kenworth</option>
                 <option value="Mack">Mack</option>
                 <option value="">Peterbilt</option>
                 <option value="Peterbilt">Sterling</option>
                 <option value="Western Star">Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="t_select_GVW[]" class="select_new">
                   <option>0-10,000 pounds</option>
                                                                <option>10,001-26,000 pounds</option>
                                                                <option>26,001-40,000 pounds</option>
                                                                <option>40,001-80,000 pounds</option>
                                                                <option>Over 80,000 pounds</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" name="t_select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" placeholder="" name="t_select_Operation[]">
            </span>
          </div>
          <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="t_select_Radius[]" class="select_new">
                 <option value="0-100 miles">0-100 miles</option>
                                                                <option value="101-500 miles" >101-500 miles</option>           	
                                                                <option value="501+ miles">501+ miles</option>
                                                              	<option value="other">Other</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">States driving</label>
            <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('t_state_driving[]', $broker_state, $attr); ?>
            </span>
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12" type="text" placeholder="" name="t_select_Cities[]">
            </span>
            <span class="span5">
              <label class="control-label">Applicant's business</label>
              <select name="t_select_applicant[]" class="select_new">
                <option value="Tow truck driver">Tow truck driver</option>
              </select>
              <input class="textinput span5" type="text" placeholder="Please specify" name="t_select_applicant_driver[]">
            </span>
          </div>
          <div class="row-fluid">
            <span class="span4">
              <label class="checkbox">
                <input type="checkbox" value="Yes" name="t_select_ch_radius[]">
                <span>Same Radius of Operation for all vehicles</span>
              </label>
            </span>
            <span class="span3">
              <label class="checkbox">
                <input type="checkbox" value="Yes" name="t_select_ch_cities[]">
                <span>Same Cities for all vehicles</span>
              </label>
            </span>
            <span class="span5">
              <label class="checkbox">
                <input type="checkbox" value="Yes" name="t_select_ch_applicant[]">
                <span>Same Applicant's Business for all vehicles</span>
              </label>
            </span>
          </div>
          <div class="row-fluid">
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" placeholder="" name="t_select_special[]">
            </span>
            <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="t_select_Axels[]" class="select_new">
                <option value="">Select</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">Number of drivers:&nbsp;</label>
              <input class="textinput input-mini" type="text" placeholder="" name="t_select_num_drivers[]">
            </span>
            <span class="span3">
              <label class="control-label"></label>
            </span>
          </div>
		 </div>
		 
         <!-- <div class="row-fluid">
            <span class="span12">
              <label class="control-label pull-left">Do you pull equipment with this vehicle? &nbsp;</label>
              <select name="" class="select_new">
                <option value="Yes">Yes</option>
              </select>
			  <select type="text" name="vehicle_trailer_no[]" id="vehicle_trailer" onchange="add_trailer1(this.value,1 );" class="span3 ui-state-valid">
			  <option value="0">None</option>
			  <option value="1">Single</option>	
			  <option value="2">Double</option>	
			  <option value="3">Triple</option>	
			  </select>
              <select name="" class="select-7">
                <option value="Single">Single</option>
                <option value="Double">Double</option>
                <option value="Other">Triple</option>
              </select>
            </span>
          </div>
          <div class="row-fluid">
            <span class="span6">
              <label class="control-label pull-left">Do you need Physical Damage Coverage? &nbsp;</label>
              <select name="">
                <option value="Yes">Yes</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label pull-left">Do you need Cargo Coverage? &nbsp;</label>
              <select name="" class="select_new">
                <option value="Yes">Yes</option>
              </select>
            </span>
           </div>-->
		  
		  <!--end tractor-->
		  <!--start trailer-->
		  
		  <span id="add_trailer_div"></span>
		<div id="row_trailer" style="display:none; ">
          
             <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Vehicle year</label>
              <select name="tr_select_vehicle_year[]" class="select_new">
                 <option value="1985">1985</option>
                 <option value="1986">1986</option>
                                                                <option value="1987">1987</option>
                                                                <option value="1988">1988</option>
                                                                <option value="1989">1989</option>
																<option value="1990">1990</option>
                                                                <option value="1991">1991</option>
                                                                <option value="1992">1992</option>
                                                                <option value="1993">1993</option>
                                                                <option value="1994">1994</option>
                                                                <option value="1995"> 1995</option>
                                                                <option value="1996">1996</option>
                                                                <option value="1997">1997</option>
                                                                <option value="1998">1998</option>
                                                                <option value="1999">1999</option>
                                                                <option value="2000">2000</option>
                                                                <option value="2001">2001</option>
                                                                <option value="2002">2002</option>
                                                                <option value="2003">2003</option>
                                                                <option value="2004">2004</option>
                                                                <option value="2005">2005</option>
                                                                <option value="2006">2006</option>
                                                                <option value="2007">2007</option>
                                                                <option value="2008">2008</option>
                                                                <option value="2009">2009</option>
                                                                <option value="2010">2010</option>
                                                                <option value="2011">2011</option>
                                                                <option value="2012">2012</option>
                                                                <option value="2013">2013</option>
																<option value="2014">2014</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Make model</label>
              <select name="tr_select_model[]" class="select_new">
                <option value="">Select</option>
				<option value="Freightliner">Freightliner</option>
                <option value="International">International</option>
                 <option value="Isuzu">Isuzu</option>
                 <option value="Kenworth">Kenworth</option>
                 <option value="Mack">Mack</option>
                 <option value="">Peterbilt</option>
                 <option value="Peterbilt">Sterling</option>
                 <option value="Western Star">Western Star</option>
                 
              </select>
            </span>
            <span class="span3">
              <label class="control-label">GVW</label>
              <select name="tr_select_GVW[]" class="select_new">
                   <option>0-10,000 pounds</option>
                                                                <option>10,001-26,000 pounds</option>
                                                                <option>26,001-40,000 pounds</option>
                                                                <option>40,001-80,000 pounds</option>
                                                                <option>Over 80,000 pounds</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">VIN</label>
              <input class="textinput span12" type="text" placeholder="" name="tr_select_VIN[]">
            </span>
            <span class="span2">
              <label class="control-label">Operation</label>
              <input class="textinput span12" type="text" placeholder="" name="tr_select_Operation[]">
            </span>
          </div>
          <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Radius of Operation</label>
              <select name="tr_select_Radius[]" class="select_new">
                 <option value="0-100 miles">0-100 miles</option>
                                                                <option value="101-500 miles" >101-500 miles</option>           	
                                                                <option value="501+ miles">501+ miles</option>
                                                              	<option value="other">Other</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">States driving</label>
            <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('tr_state_driving[]', $broker_state, $attr); ?>
            </span>
            <span class="span3">
              <label class="control-label">Cities</label>
              <input class="textinput span12" type="text" placeholder="" name="tr_select_Cities[]">
            </span>
            <span class="span5">
              <label class="control-label">Applicant's business</label>
              <select name="tr_select_applicant[]" class="select_new">
                <option value="Tow truck driver">Tow truck driver</option>
              </select>
              <input class="textinput span5" type="text" placeholder="Please specify" name="tr_select_applicant_driver[]">
            </span>
          </div>
          <div class="row-fluid">
            <span class="span4">
              <label class="checkbox">
                <input type="checkbox" value="Yes" name="tr_select_ch_radius[]">
                <span>Same Radius of Operation for all vehicles</span>
              </label>
            </span>
            <span class="span3">
              <label class="checkbox">
                <input type="checkbox" value="Yes" name="tr_select_ch_cities[]">
                <span>Same Cities for all vehicles</span>
              </label>
            </span>
            <span class="span5">
              <label class="checkbox">
                <input type="checkbox" value="Yes" name="tr_select_ch_applicant[]">
                <span>Same Applicant's Business for all vehicles</span>
              </label>
            </span>
          </div>
          <div class="row-fluid">
            <span class="span5">
              <label class="control-label">Any special request?</label>
              <input class="textinput span12" type="text" placeholder="" name="tr_select_special[]">
            </span>
            <span class="span2">
              <label class="control-label">Number of Axels</label>
              <select name="tr_select_Axels[]" class="select_new">
                <option value="">Select</option>
              </select>
            </span>
            <span class="span2">
              <label class="control-label">Number of drivers:&nbsp;</label>
              <input class="textinput input-mini" type="text" placeholder="" name="tr_select_num_drivers[]">
            </span>
            <span class="span3">
              <label class="control-label"></label>
            </span>
          </div>
		</div>
		  <!--end trailer-->	
	
		  <div id="pd_id_damage" style="display:none; ">
          
          <div class="row-fluid" >
             <span class="span1">
              <label class="control-label">Rate</label>
              <select name="pd_rate[]" class="select_new_per">
                <option value="5%">5%</option>
                <option value="10%">10%</option>
              </select>
            </span>
            <span class="span3">
              <label class="control-label">Loss payee and full address</label>
              <select name="pd_loss_payee[]">
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
			     <span class="span4">
              <label class="control-label">Additional optional coverage</label>
              <select name="pd_coverage[]" class="span6">
                <option value="Debris removal">Debris removal</option>
                <option value="Towing &amp; storage">Towing &amp; storage</option>
                <option value="Tarpaulin coverage">Tarpaulin coverage</option>
                <option value="Other">Other</option>
              </select>
              <input class="textinput  span4" type="text" placeholder="Specify" name="pd_coverage_specify[]">
            </span>
			<span class="span2">
              <label class="control-label">Premium</label>
              <input class="textinput span9" type="text" placeholder="" name="pd_premium[]">
            </span>
            <span class="span2">
              <label class="control-label">Deductible</label>
              <input class="textinput span12" type="text" placeholder="" name="pd_deductible[]">
            </span>
           </div>
		        <label class="control-label control-label-2">Loss Payee Address(es)</label>
          <div class="row-fluid">
            <span class="span3">
              <label class="control-label">Entity name</label>
              <input class="textinput span12" type="text" placeholder="" name="pd_loss_name[]">
            </span>
            <span class="span3">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" name="pd_loss_address[]">
            </span>
            <span class="span1">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder="" name="pd_loss_city[]">
            </span>
            <span class="span1">
              <label class="control-label">State</label>
              <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('pd_loss_state[]', $broker_state, $attr); ?>
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" placeholder="" name="pd_loss_zip[]">
            </span>
            <span class="span3">
              <label class="control-label">Remarks</label>
              <input class="textinput span12" type="text" placeholder="" name="pd_loss_remarks[]">
            </span>
          </div>
        
		  </div>
		  <div id="cv_id_damage" style="display:none; ">
          <div class="row-fluid">
            <span class="span6">
              <label class="control-label">Additional optional coverage</label>
              <select name="" class="select-15">
                <option value="Earned freight clause">Earned freight clause</option>
                <option value="Refrigeration breakdown deductible">Refrigeration breakdown deductible</option>
                <option value="Debris removal">Debris removal</option>
                <option value="Excess cargo">Excess cargo</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label"></label>
            </span>
          </div>
		  </div>
		  <div id="gd_id_damage" style="display:none; ">
          <h5 class="heading sh sh-4">Garaging Address</h5>
          <div class="row-fluid">
            <span class="span4">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" name="garage_address[]">
            </span>
            <span class="span2">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder="" name="garage_city[]">
            </span>
            <span class="span2">
              <label class="control-label">State</label>
              <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('garage_state[]', $broker_state, $attr); ?>
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" placeholder="" name="garage_zip[]">
            </span>
            <span class="span4">
              <label class="control-label">Remarks</label>
              <input class="textinput span12" type="text" placeholder="" name="garage_remarks[]">
            </span>
          </div>
		  </div>
		 <div id="ds_id_damage" style="display:none; ">
          <h5 class="heading sh sh-5">Drivers Schedule</h5>
          <table class="table">
            <thead>
              <tr>
                <th>First name</th>
                <th>Middle name</th>
                <th>Last name</th>
                <th>Driver License
                  <br>Number</th>
                <th>License class</th>
                <th>Date hired</th>
                <th>How many years
                  <br>Class A license</th>
                <th>License issue state</th>
                <th>Remarks</th>
                <th>Surcharge</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_fname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_mname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_lname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_license_num[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_license_class[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_date_hired[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_class_A[]">
                </td>
                <td>
                   <?php
									$attr = "class='span12 width_td ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('driver_state[]', $broker_state, $attr); ?>
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_remarks[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="driver_surcharge[]">
                </td>
              </tr>
            </tbody>
           </table>
		  </div>
		  	  <div id="t_pd_id_damage" style="display:none; ">
          <h5 class="heading sh sh-2">Physical Damage</h5>
          <div class="row-fluid" >
            <span class="span1">
              <label class="control-label">Value 1</label>
              <input class="textinput span12" type="text" placeholder="" name="t_pd_val1[]">
            </span>
            <span class="span1">
              <label class="control-label">Value 2</label>
              <input class="textinput span12" type="text" placeholder="" name="t_pd_val2[]">
            </span>
            <span class="span1">
              <label class="control-label">Rate 1</label>
              <select name="t_pd_rate1[]" class="select_new_per">
                <option value="5%">5%</option>
                <option value="10%">10%</option>
              </select>
            </span>
            <span class="span1">
              <label class="control-label">Rate 2</label>
              <select name="t_pd_rate2[]" class="select_new_per">
                <option value="5%">5%</option>
                <option value="10%">10%</option>
              </select>
            </span>
            <span class="span2">
             
              <label class="control-label">Amount 1</label>
              <input class="textinput span12" type="text" placeholder="" name="t_pd_amount1[]">
            </span>
            <span class="span2">
              <label class="control-label">Amount 2</label>
              <input class="textinput span12" type="text" placeholder="" name="t_pd_amount2[]">
            </span>
            <span class="span4">
             
              <label class="control-label">Stated Amount</label>
              <input class="textinput span5" type="text" placeholder="Amount" name="t_pd_amount[]">
              <input class="textinput span5" type="text" placeholder="Deductible" name="t_pd_deductible[]">
            </span>
          </div>
          <div class="row-fluid">
            <span class="span2">
              <label class="control-label">Premium</label>
              <input class="textinput span12" type="text" placeholder="" name="t_pd_premium[]">
            </span>
            <span class="span4">
              <label class="control-label">Loss payee and full address</label>
              <select name="t_pd_loss_payee[]">
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label"></label>
            </span>
          </div>
		  </div>
		    <div id="t_cv_id_damage" style="display:none; ">
          <h5 class="heading sh sh-3">Cargo</h5>
          <div class="row-fluid">
            <span class="span3">
              <label class="control-label">Average value per load &nbsp;</label>
              <input class="textinput input-mini" type="text" placeholder="$" name="t_cargo_avg_val[]">
              <input class="textinput input-mini" type="text" placeholder="%" name="t_cargo_avg_val1[]">
            </span>
            <span class="span3">
              <label class="control-label">Maximum values per load &nbsp;</label>
              <input class="textinput input-mini" type="text" placeholder="$" name="t_cargo_max_val[]">
              <input class="textinput input-mini" type="text" placeholder="%" name="t_cargo_max_val1[]">
            </span>
            <span class="span3">
              <label class="control-label">Stated Amount</label>
              <input class="textinput input-mini" type="text" placeholder="Amount" name="t_cargo_amount[]">
              <input class="textinput input-mini" type="text" placeholder="Deductible" name="t_cargo_deductible[]">
            </span>
            <span class="span3">
              <label class="control-label">% Factor</label>
              <input class="textinput input-mini" type="text" placeholder="" name="t_cargo_factor[]">
            </span>
           </div>
		  </div>
		  <div id="t_gd_id_damage" style="display:none; ">
          <h5 class="heading sh sh-4">Garaging Address</h5>
          <div class="row-fluid">
            <span class="span4">
              <label class="control-label">Address</label>
              <input class="textinput span12" type="text" placeholder="" name="t_garage_address[]">
            </span>
            <span class="span2">
              <label class="control-label">City</label>
              <input class="textinput span12" type="text" placeholder="" name="t_garage_city[]">
            </span>
            <span class="span1">
              <label class="control-label">State</label>
              <?php
									$attr = "class='span12 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('t_garage_state[]', $broker_state, $attr); ?>
            </span>
            <span class="span1">
              <label class="control-label">Zip</label>
              <input class="textinput input-mini" type="text" placeholder="" name="t_garage_zip[]">
            </span>
            <span class="span4">
              <label class="control-label">Remarks</label>
              <input class="textinput span12" type="text" placeholder="" name="t_garage_remarks[]">
            </span>
          </div>
		  </div>
		 <div id="t_ds_id_damage" style="display:none; ">
          <h5 class="heading sh sh-5">Drivers Schedule</h5>
          <table class="table">
            <thead>
              <tr>
                <th>First name</th>
                <th>Middle name</th>
                <th>Last name</th>
                <th>Driver License
                  <br>Number</th>
                <th>License class</th>
                <th>Date hired</th>
                <th>How many years
                  <br>Class A license</th>
                <th>License issue state</th>
                <th>Remarks</th>
                <th>Surcharge</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_fname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_mname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_lname[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_license_num[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_license_class[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_date_hired[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_class_A[]">
                </td>
                <td>
                   <?php
									$attr = "class='span1 ui-state-valid'";
									$broker_state = 'CA';
									
									?>
									<?php echo get_state_dropdown('t_driver_state', $broker_state, $attr); ?>
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_remarks[]">
                </td>
                <td>
                  <input class="textinput input-mini" type="text" placeholder="" name="t_driver_surcharge[]">
                </td>
              </tr>
            </tbody>
           </table>
		  </div>
          <div class="row-fluid">
            <span class="span12">
              <label class="control-label pull-left">Please attach MVR's for all driver(s) &nbsp;</label>
			  <input type="hidden" name="uploaded_files_values_div" id="uploaded_files_values_second_div" value="">
			  
			  <input type="button" id="first_upload_second_div" value="Upload">
              <ul id="upload_file_name_second_div"></ul>
            </span>
          </div>
         
		</div>
		<div style="display:none" id="badge_id_3">
		  <span class="badge">Cargo</span>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">Are loaded trucks ever left unattended? &nbsp;</label>
            <select name="loaded_trucks_name" class="select-12" id="loaded_trucks_id" onchange="load_trucks();">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
		<div  id="loaded_id">
		    <label class="control-label mt_15">Indicate whether the following additional coverages are required.</label>
        <div class="row-fluid mt_15">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th></th>
                  <th style="margin-left: 8px;float: left;">Amount or Limit</th>
                  <th></th>
                  <th style="margin-left: 8px;float: left;">Amount or Limit</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Earned freight clause</td>
                  <td >
                    <input class="textinput span4" type="text" placeholder="" name="freight_amount">
                  </td>
                  <td>Other</td>
                  <td>
                    <input class="textinput span4" type="text" placeholder="" name="other_amount">
                  </td>
                </tr>
                <tr>
                  <td>Refrigeration breakdown clause</td>
                  <td>
                    <input class="textinput span4" type="text" placeholder="" name="clause_amount">
                  </td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
	</div>
	</div>
		<div style="display:none" id="badge_id_4">
		 <span class="badge">Cargo, Liability</span>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">Is this a primary coverage? &nbsp;</label>
            <select class="pull-left select-11" name="primary_cover_name" id="primary_coverage_id" onchange="primary_coverage();">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
		<div  id="coverage_id">
		  <div class="row-fluid">
          <span class="span3">
            <select name="combined_name" class="select-13">
              <option value="Combined Single Limit">Combined Single Limit</option>
            </select>
          </span>
          <span class="span9">
            <input class="textinput span4" type="text" name="combined_amount" placeholder="Amount">
            <input class="textinput" type="text" placeholder="Deductible" name="combined_deductible">
          </span>
        </div>
		     <div class="row-fluid">
          <span class="span3">
            <select name="split_limit" class="select-13">
              <option value="Split Limit">Split Limit</option>
            </select>
          </span>
          <span class="span6">
            <input class="textinput price_format span4" type="text" name="each_person" placeholder="$ each person">
            <input class="textinput price_format span4" type="text" name="each_accident" placeholder="$ each accident">
            <input class="textinput price_format span4" type="text" name="each_property" placeholder="$ each property">
          </span>
          <span class="span3">
            <input class="textinput" type="text" placeholder="Deductible" name="split_deductible">
          </span>
        </div>
		</div>
	</div>
		 <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">Do you need UM or UIM or PIP? &nbsp;</label>
            <select class="pull-left select-11" name="split_PIP">
              <option value="Yes">Yes</option>
              <option value="NO">NO</option>
            </select>
          </span>
        </div>
		<p>This is an application not a binding confirmation. Agent / Broker has no authority to bind. Policy will be effective only when confirmed by GHINS in writing with a Binder or Policy number.</p>
        <p>Applicant understands that there is a minimum deductible of $1,000 or more for all sand and gravel, flat bed, open trailer, or tow truck policies or as quoted.</p>
        <p>Any person who knowingly engages in fraudulent practices, concealment of facts, false information on any application or statement of claim with the intent to defraud and mislead the insurance company is a felony, punishable by fines and or/ imprisonment.</p>
      
                          <!--      <div class="row-fluid ">  	
                                    <div class="span12 lightblue">
                                        <label>Number of drivers ?</label>
                                        <input pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" required="" type="text" id="num_of_drivers" onchange="add_driver(this.value)" name="driver_count" class="span1" value="">
                                    </div>
                                </div>

                                
                                <div id="row_driver" style="display:none;">
						<div class="row-fluid">  
							<div class="span12 lightblue">
								<table id="driver_table" class="table table-bordered table-striped table-hover">
									<thead>
									  <tr>
										<th>First Name</th>
										<th>Middle Name</th>
										<th>Last Name</th>
										<th>Driver License number</th>
										<th>License Class</th>
										<th>Date Hired</th>
										<th>DOB</th>
										<th>How Many Years Class A license</th>
										<th>License issue state</th>
									
									  </tr>
									  
									</thead>
									<tbody id="add_driver_row">
									</tbody>
								</table>
							</div>
						</div>
					</div>
                                                                <!--span id="add_driver_div"></span-->

                            </fieldset>

                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                       <!--    <fieldset id="section4" class="number_of_owner_box">
                                <legend>Number of Owner</legend>

                                <div class="row-fluid ">  	
                                    <div class="span12 lightblue">
                                        MAYANK
                                        <label>Number of owner of the vehicle or entity?</label>
										<!--END MAYANK
                                        <input required="" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" type="text" id="number_of_owner" name="number_of_owner" onchange="add_owner(this.value)" class="span1" value="">
                                    </div>
                                </div>

  

                                    <div id="row_owner" style="display:none;">
                                        <div class="row-fluid">  
                                            <div class="span12 lightblue">
                                                <table id="owner_table" class="table table-bordered table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Middle Name</th>
                                                            <th>Last Name</th>
                                                            <th> Driver ?</th>
                                                            <th>Driver License number</th>
                                                            <th>License issue state</th>

                                                        </tr>

                                                    </thead>
                                                    <tbody id="add_owner_row">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> 

                                <!--span id="add_driver_div"></span

                            </fieldset>



                       
                       
                       
                       
                       
                       
                       
                            <fieldset id="section5" class="fillings_section_box">
                                <legend>Filing Section</legend>
						<!--MAYANK-->
  
                                    <div class="row-fluid">  
                                        
										<script type="text/javascript">
											$(document).ready(function(){
												$("#filings_need").change(function(){
													if($(this).val()=="yes") {
														$("#number_of_filings_div").show();
														$('#number_of_filings').attr('required', 'required'); 
														
													}else{
														$("#number_of_filings_div").hide();
														$('#number_of_filings').removeAttr('required'); 
														}
												});
											});
											
											function add_filing(val){
													if(val >=0 ){
														if(val == 0)
														{
															$("#filing_table").hide();
														}
														//$("#filing_table").show();
														$("#row_filing").show();
														//$("#add_filing_row").html('');
														var i =1;
														var row = '<tr>\
														<td><input type="text" name="filing_type[]" class="span12 filing_type" required="required" /></td> \
														<td><input type="text" name="filing_numbers[]" class="span12 filing_numbers"  required="required" /></td> \
														</tr>';
														var currcount = $("#add_filing_row tr").length;
														var diff = currcount - val;
														
														 if(diff < 0) {
																while(diff < 0){
																	var v = $('#add_filing_row').append(row) ; 
																	diff++;
																	
																} 
															 }
															 else if(diff > 0)
															 {
																
																 while(diff > 0){
																
																	 var v = $('#add_filing_row tr:last').remove() ; 
																	 diff--;
																 }
															 }	
															
														apply_on_all_elements();
													}
												}
										</script>
									<!--	<div class="span12 lightblue">
											<label>Do you need Filing / Filings :</label>
											<select id="filings_need" name="filings_need" required="required">
												<option value="">--Select--</option>
												<option value="yes">YES</option>
												<option value="no">NO</option>
											</select>
                                        </div>
										
										<div class="span12 lightblue" id="number_of_filings_div" style="display:none;">
											<label>Number of Filings :</label>
											<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" id="number_of_filings" name="number_of_filings" class="span1 filing_numbers" onchange="add_filing(this.value)">
										</div>
										
										<div id="row_filing" style="display:none;">
											<div class="row-fluid">  
												<div class="span6 lightblue">
													<table id="filing_table" class="table table-bordered table-striped table-hover">
														<thead>
															<tr>
																<th>Filing Type:</th>
																<th>Filing Num.</th>
															</tr>
														</thead>
														<tbody id="add_filing_row">
														</tbody>
													</table>
												</div>
											</div>
										</div> 
									</div>							
		                                <!--END OF MAYANK-->


                            </fieldset>

				

                            <div class="row-fluid">  
                                <div class="span lightblue">
                                    <div class="form-actions">
                                        <!--button class="btn btn-primary" type="submit" onclick="return check_vehicle_entry();">Submit Quote</button-->
                                        <button class="btn btn-primary" type="submit" onclick="">Submit Quote</button>
									
                                        <button class="btn" type="reset">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
						</div>
</form>
 <script src="<?php echo base_url('js');?>/jquery.knob.js"></script>

		<!-- jQuery File Upload Dependencies -->
		<script src="<?php echo base_url('js');?>/jquery.ui.widget.js"></script>
		<script src="<?php echo base_url('js');?>/jquery.iframe-transport.js"></script>
		<script src="<?php echo base_url('js');?>/jquery.fileupload.js"></script>
		
		<!-- Our main JS file -->
		<script src="<?php echo base_url('js');?>/script.js"></script>	 
<div style="display:none;">
		<form id="upload" method="post" action="http://localhost/quote_process/upload.php" enctype="multipart/form-data">
			<div id="drop">
				Drop Here

				<a>Browse</a>
				<input type="file" name="upl" multiple />
			</div>

			<ul>
				<!-- The file uploads will be shown here -->
			</ul>

		</form>
        
        
        <form id="upload_second" method="post" action="http://localhost/quote_process/upload.php" enctype="multipart/form-data">
			<div id="drop_second">
				Drop Here

				<a>Browse</a>
				<input type="file" name="upl" multiple />
			</div>

			<ul>
				<!-- The file uploads will be shown here -->
			</ul>

		</form>
		<form id="upload_second_div" method="post" action="http://localhost/quote_process/upload.php" enctype="multipart/form-data">
			<div id="drop_second_div">
				Drop Here

				<a>Browse</a>
				<input type="file" name="upl" multiple />
			</div>

			<ul>
				<!-- The file uploads will be shown here -->
			</ul>

		</form>
     </div>  
       
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>
<script>
	 $(document).ready(function() {
	  var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		 var _startDate1 = new Date(); //todays date
		var _endDate1 = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		$('#date_from_effective').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to_expiration').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to_expiration').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate1 = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from_effective').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		
		
      $('#date_from_effective1').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate1 = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to_expiration1').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to_expiration1').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from_effective1').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		});
		$("#click_add_phone").click(function(){
          // $("#add_phone").show();
		   $( "#add_phone" ).slideToggle( "slow", function() {
    // Animation complete.
	if ($.trim($('input.textinput.span1').val()) != '')
{
      $('input.textinput.span1.phone').val('');
	  }
	  if ($.trim($('#select_home_p').val()) != '')
{
	   $('#select_home_p').val('Home');
	   }
	   if ($.trim($('#textinput_att_p').val()) != '')
{
	  $('#textinput_att_p').val('');
}
  });
          });
		  	$("#click_add_email").click(function(){
             $( "#add_email1" ).slideToggle( "slow", function() {
    // Animation complete.
	if ($.trim($('input.textinput.email').val()) != '')
{
      $('input.textinput.email').val('');
	  }
	  	if ($.trim($('#select_home_e').val()) != '')
{
	   $('#select_home_e').val('Home');
	   }	if ($.trim($('#textinput_att_e').val()) != '')
{
	  $('#textinput_att_e').val('');
}
  });
          });
		  

		  
		  
        </script>
        <script>
	$(document).ready(function(){
		//when the Add Filed button is clicked
		$("#add_phone_row").click(function (e) {
			//alert();
			//Append a new row of code to the "#items" div
			$("#table_phone").append('<tr><td><input class="textinput span1" type="text" placeholder="Area" id="insured_telephone1" name="insured_telephone_add[]" maxlength="3"  pattern="[0-9]+"  value=""  required="required">&nbsp;<input class="textinput span1" id="insured_telephone2" type="text" placeholder="Prefix" name="insured_telephone_add[]" maxlength="4"  pattern="[0-9]+"  value=""  required="">&nbsp;<input class="textinput span1" id="insured_telephone3" type="text" placeholder="Sub" name="insured_telephone_add[]" maxlength="3"  pattern="[0-9]+"  value=""  required="">&nbsp;<input class="textinput span1" type="text" placeholder="Ext" insured_tel_ext_add[]></td><td><select name="insured_telephone_type[]" class="select-1"><option value="Home">Home</option><option value="Business">Business</option></select></td><td><input class="textinput" type="text" placeholder="" name="insured_telephone_attention[]"></td><td> <button class="btn btn-mini btn-1 remove_phone_row" id="remove_phone_row"><i class="icon icon-remove"></i></button></td></tr>');
		});

	$("body").on("click", ".remove_phone_row", function (e) {
		//alert($(this).parent("div"));
		$(this).closest('tr').remove();
	});
	});
	
	$(document).ready(function(){
		//when the Add Filed button is clicked
		$("#add_email_row").click(function (e) {
			//alert();
			//Append a new row of code to the "#items" div
			$("#table_email").append('<tr><td><input class="textinput" type="text" placeholder="" id="email" name="insured_email_add[]" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30"></td><td><select name="insured_email_type[]" class="select-1"><option value="Home">Home</option><option value="Business">Business</option></select></td><td><input class="textinput" type="text" placeholder="" name="insured_email_attention[]"></td><td> <button class="btn btn-mini btn-1 remove_phone_row" id="remove_phone_row"><i class="icon icon-remove"></i></button></td></tr>');
		});

	$("body").on("click", ".remove_email_row", function (e) {
		//alert($(this).parent("div"));
		$(this).closest('tr').remove();
	});
	});
		function audit() {
    var val=$("#audit_name").val();
	
	if(val === "Other"){
			$("#Specify").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify").hide();
		}
		}
	function audit1() {
    var val=$("#audit_name1").val();
	
	if(val === "Other"){
		
			$("#Specify_other").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify_other").hide();
		}
		}
		
		
		function application_select(){
		//remarks_id_show
		

				//alert(myarray[0]);
				//alert($.inArray( "Liability", arr ));
				
		var foo = []; 
$('#app_id :selected').map(function(i, selected){ 
  foo.push($(this).val()); 
 
});
var val_app1=foo;




		$("#id_text_app_val").val(val_app1);
		var arr=$('#id_text_app_val').val();
				myarray = arr.split(',');
		
		if($('#losses_need').val()=="yes")
			{
				$("#losses_div").show();
				

				//alert(myarray[0]);
				//alert($.inArray( "Liability", arr ));
				if($.inArray( "Liability", myarray )> -1){
				$("#losses_lia").show();
				}else{
				$("#losses_lia").hide();
				}
				if($.inArray( "Physical Damage", myarray )> -1){
				$("#losses_pd").show();
				}else{
				$("#losses_pd").hide();
				}
				if($.inArray( "Cargo", myarray )> -1){
				$("#losses_cargo").show();
				}else{
				$("#losses_cargo").hide();
				}
				if($.inArray( "Bobtail", myarray )> -1){
				$("#losses_bobtail").show();
				}else{
				$("#losses_bobtail").hide();
				}
				if($.inArray( "Dead head", myarray )> -1){
				$("#losses_dead").show();
				}else{
				$("#losses_dead").hide();
				}
				if($.inArray( "Non Trucking", myarray )> -1){
				$("#losses_trunk").show();
				}else{
				$("#losses_trunk").hide();
				}
				if($.inArray( "Excess", myarray )> -1){
				$("#losses_excess").show();
				}else{
				$("#losses_excess").hide();
				}				
				}
				if($.inArray( "Liability", myarray )> -1){
				//$("#losses_lia").show();
				$("#filings_id").show();
				 
				}else{
				//$("#losses_lia").hide();
				$("#filings_id").hide();
				}
				if(($.inArray( "Liability", myarray )> -1) || ($.inArray( "Cargo", myarray )> -1) ){
				//$("#losses_lia").show();
				$("#badge_id_2").show();
				$("#badge_id_4").show();
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_2").hide();
				$("#badge_id_4").hide();
				}
				if($.inArray( "Cargo", myarray )> -1){
				//$("#losses_lia").show();
				$("#badge_id_3").show();
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_3").hide();
				}
if(($.inArray( "Liability", myarray )> -1) || ($.inArray( "Cargo", myarray )> -1) || ($.inArray( "Physical Damage", myarray )> -1) ){

$("#remarks_id_show").show();
}else{
$("#remarks_id_show").hide();
}
// alert(foo);
	/*	var foo = []; 
$('#app_id :selected').map(function(i, selected){ 
  foo.push($(this).val()); 
 
});
var val_app1=foo;
		$("#id_remarks_text").val(val_app1);
	
	var val_app=$("#app_id").val();
		//alert(val_app);
		var val_remark=$("#id_remarks_text").val();
		// var myarray=[];
        // myarray.push(val_app);
		 // alert($(this).attr("selected",true));
		if(val_remark === ""){
		$("#id_remarks_text").val(val_app);		
		}else
		{
		  myarray = val_remark.split(',');
		 //alert(myarray[0]);
	
		//alert(myarray.indexOf( val_app ));
		
		if(myarray.indexOf( val_app )>=0){
			//alert(2);				
		 }else{
		// alert(3);
	//var val_app1=val_remark+","+val_app;
		//$("#id_remarks_text").val(val_app1);
		 }
		}*/
		
		
		}
		function aapli_other(){
		var val=$("#appli_id").val();
		if(val === "Other"){
		
			$("#other_tr").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#other_tr").hide();
		}
		}
	
</script>
<script>
										 function trans_val(){
										 var n=$("#transported_id").val();
										 if(n==="No"){
										 $("#text_no").show();										 
										 }else{
										 $("#text_no").hide();	
										 
										 }
										 }
										 </script>
										 <script>
										 function Company_trucking(){
										 if($("#trucking_comp_id").val() ==="Other"){
										 $("#trucking_specify").show();
										 }else{
										 $("#trucking_specify").hide();
										 }
										 }
										 function certificate_app_val(){
										// alert($("#certificate_id_val").val());
										 if($("#certificate_id_val").val() ==="Yes"){
										 //alert(1);
										 $("#certificate_app_id").show();
										 }else{
										 $("#certificate_app_id").hide();
										// alert(2);
										 }
										 }
										 function owner_vehicle1(){
										 if($("#owner_id").val() ==="No"){
										 
										 $("#insurance_vehicle").show();
										// $("#Policy_id").show();
										 }else{
										 $("#insurance_vehicle").hide();
										// $("#Policy_id").hide();
										// alert(2);
										 }
										 }
										 function insurance_val_func(){
										  if($("#insurance_val_id").val() ==="Yes"){
										 //alert(1);
										 $("#Policy_id").show();
										 }else{
										 $("#Policy_id").hide();
										// alert(2);
										 }
										 
										 }
										 	

function load_trucks(){

	 var n=$("#loaded_trucks_id").val();
										 if(n==="Yes"){
										 $("#loaded_id").show();										 
										 }else{
										 $("#loaded_id").hide();	
										 
										 }

}						
          function primary_coverage(){
		  var n=$("#primary_coverage_id").val();
										 if(n==="Yes"){
										 $("#coverage_id").show();										 
										 }else{
										 $("#coverage_id").hide();	
										 
										 }
		  
		  }
					function inspection_func(){
					if($("#inspection_id").val()=== "Yes"){
					  $("#if_yes").show();
					}else{
					  $("#if_yes").hide();
					}
					
					}
										 </script>
<script src="http://localhost/quote_process/js/request_quote.js"></script>
<script src="http://localhost/quote_process/js/chosen.jquery.min.js"></script>

