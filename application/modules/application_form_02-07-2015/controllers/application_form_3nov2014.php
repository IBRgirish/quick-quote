<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Application_form extends RQ_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct()
	{
		parent::__construct();
		
		   		if(!$this->require_login_user() && !$this->require_login_underwriter())

		{

			$base = base_url('login');

			redirect($base);

		}
		
			$this->load->model('application_model');
			$this->load->model('edit_model');
	       // $this->load->library('session');
		    $this->load->model('quote/quote_model');
	}
	 
	public function index()
	{
		
		$data['user'] = 'member';

		if ($this->session->userdata('underwriter_id')) {

			$data['user'] = 'underwriter';

		}

	    
	    $data['broker_id'] = $this->application_model->get_broker_quote($id='');
		$data['list']=$this->application_model->get_data();
		//print_r($data);
		$this->load->view('application_list',$data);
		//$this->load->view('application_applicant');
		
		//redirect(base_url('application_form'));
	}
	
	/*public function genrate_quote(){
	
	
	}*/
	
  
  function quote_request($edit='',$id=''){
	if($id!=''){
	  $data['broker_inform'] = $this->application_model->get_broker_inform();
	  if($edit=='edit'){
		$data['app']=$this->application_model->get_data($id);
		$data['app_applicant']=$this->application_model->get_data_applicant($id);
		$data['app_tel_add']=$this->application_model->get_data_tel_add($id);
		$data['adresses']=$this->application_model->get_database_inform($id,'adresses');
		$data['app_email_add']=$this->application_model->get_data_email_add($id);
		$data['broker_information'] = $this->application_model->get_broker_information($id);
	  }
	  if($edit=='app'){
		$data['Quote_num']=$id;	
		$rq_rqf_quote=$this->application_model->get_database_quote($id,'rq_rqf_quote');
		$data['broker_inform1'] = $this->application_model->get_broker_inform($rq_rqf_quote[0]->email);
		$data['rq_rqf_quote_losses']=$this->application_model->get_database_quote($id,'rq_rqf_quote_losses');
		$data['broker_insured'] = $this->application_model->get_broker_insured($id);
		$data['app']=$this->application_model->get_database_quote($id,'application_general');
		$data['app_applicant']=$this->application_model->get_database_quote($id,'application_applicant');
		$data['app_tel_add']=$this->application_model->get_database_quote($id,'application_tel_add');
		$data['adresses']=$this->application_model->get_database_quote($id,'adresses');
		$data['app_email_add']=$this->application_model->get_database_quote($id,'application_email_add');
		$data['broker_information'] = $this->application_model->get_database_quote($id,'app_broker_inform');
	  }
	  //$this->load->view('application_applicant',$data);
	}else{
	 $data['broker_inform1'] = $this->application_model->get_broker_inform();	
	 $data['quote_id'] = $this->input->post('quote_id');
	 $data['broker_inform'] = $this->application_model->get_broker_inform();
	}
	$this->load->view('application_applicant',$data);
	//$this->load->view('application_losses',$data);
  }
	/*public function request_quote()
	{
		if(!$this->session->userdata('member_id'))
		{
			redirect('user/login');
		}
		else
		{
			$data['headre']='';
			$this->load->view('frontend/request_quote',$data);
			
		}
		
	}*/
	
  //Submit application form Ashvin Patel 3/nov/2014
  public function submit_applicant(){
	  
  }
	function applicant($id='',$save=''){
		
	if($_POST){  //print_r($_POST); die;
			//print_r($_POST['tractor_number_of_axles']);
			//$myarray = array_values($myarray);
			//print_r($_FILES); die;
			//error_reporting(E_ALL);
			/*	if($id!=''){
			 
			
			 $data['losses'] = $this->application_model->get_losses_db($id);
             $data['quote_losses'] = $this->application_model->get_quote_losses_db($id);	
             $this->load->view('application_losses',$data);				 
			 } else {
		
			  if(isset($_POST['save1'])){
			  
			  
			  // $this->session->set_userdata("id_text_app_val",$data1);
			   $data['app_val']=$this->session->userdata("id_text_app_val");
			  // print_r($data);
			   //$data['app_val'] = $this->input->post('id_text_app_val');
			   $data['quote_id']=$id;
			   $this->load->view('application_losses',$data);	
			  
			  }else{*/
			
			  if(isset($_POST['save'])){
				 $id = $this->input->post('insert_id');

				   if($id==''){
					   $result = $this->application_model->app_applicant_db($id);
					 //  print_r( $result);
					        $getid = $this->application_model->getid();
							//print_r($getid);
							$id= $getid[0]->general_id;
							 $this->quote_request('edit',$id);
					   }else{
						   
						   $result = $this->application_model->app_applicant_db($id);
						   $this->quote_request('edit',$id);
						   }
					  
				//  echo $id;
			     // die;
               	   
				  
				  }else{
			  
			  if($id!=''){
			    $data['app_val'] = $this->input->post('id_text_app_val');			 
			    $data['quote_id'] = $this->input->post('quote_id');	
				$data['app']=$this->application_model->get_data($id);
			    $data['app_loss']=$this->application_model->get_database_inform($id,'application_losses');
				
			    $data['app_loss1']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_liability');
				//print_r($data['app_loss']);
				//die;
			    $data['app_loss2']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_pd');
	    	    $data['app_loss3']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_cargo');
				$data['app_loss4']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_bobtail');
				$data['app_loss5']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_dead');
				$data['app_loss6']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_trunk');
				$data['app_loss7']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_excess');
				$data['app_ghi8']=$this->application_model->get_database_inform($id,'application_ghi8');
                $data['los_id']='value';
			   
			     if(!empty($data['quote_id'])){
					 if(empty($data['app_loss1'])){
			         $data['app_loss1']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_liability');
					  $data['upload_file']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote');
					 }
					 if(empty($data['app_loss2'])){
			    $data['app_loss2']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_pd');
					 }
					 if(empty($data['app_loss3'])){
	    	    $data['app_loss3']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_cargo');
					 }
					
			   }
			   
			   
			   if($save=='save'){
				  
				   }else{
                  
			    $result = $this->application_model->app_applicant_db($id);
			   }
			     $this->load->view('application_losses',$data);
			//int_r($data);
			 //  die;
			 //  $this->load->view('application_losses',$data);	
			  }else{
			  
			 
			   
			   $data['app_val'] = $this->input->post('id_text_app_val');
			 //  print_r($data['app_val']);		 
			   $data['quote_id'] = $this->input->post('quote_id');	   
			   $result = $this->application_model->app_applicant_db($id);
		       $data['ref_id']=$this->application_model->getid();
		      // $data['rqf_quote_drivers']=$this->application_model->get_database_quote($id,'rq_rqf_quote_drivers'); 
			   if(!empty($data['quote_id'])){
			    $data['app_loss1']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_liability');
			    $data['app_loss2']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_pd');
	    	    $data['app_loss3']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_cargo');
				$data['upload_file']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote');
				
			   }
				   $this->load->view('application_losses',$data);
				  
			       }
				  }
			
			} else{
				
			}
			}
			function losses($id='',$save=''){
				//echo $id;
				
	if($_POST){  //print_r($_POST); die;
			//print_r($_POST['tractor_number_of_axles']);
			//$myarray = array_values($myarray);
			//print_r($_FILES); die;
			//error_reporting(E_ALL);
		
					  if(isset($_POST['saved'])){
				 $insert_id   =$this->input->post('insert_id');
				  $ref_id   = $this->input->post('ref_id');
				  $id=  !empty($insert_id)? $insert_id: $ref_id;
				
				   if($id==''){
					  
					        $result = $this->application_model->app_losses_db($id);
		                    $this->edit_model->app_edit_db($id);
							$id= $getid[0]->general_id;
							 $this->applicant($id,'save');
					   }else{
						   
						    $result = $this->application_model->app_losses_db($id);
		                    $this->edit_model->app_edit_db($id);
						   $this->applicant($id,'save');
						   }
					  
				//  echo $id;
			     // die;
               	   
				  
				  }else{
			
			
			if(isset($_POST['back'])){
	      $ref_id = $this->input->post('ref_id');
		  $id=!empty($id)? $id : $ref_id;
		   // $this->applicant($id);
	    $data['quote_id'] = $this->input->post('quote_id');
        $data['app_val'] = $this->input->post('id_text_app_val');
			   
	    $data['app']=$this->application_model->get_data($id);	
	    $data['app_applicant']=$this->application_model->get_data_applicant($id);
	    $data['app_tel_add']=$this->application_model->get_data_tel_add($id);
	    $data['adresses']=$this->application_model->get_database_inform($id,'adresses');
	    $data['app_email_add']=$this->application_model->get_data_email_add($id);
	    $data['broker_inform'] = $this->application_model->get_broker_inform();
        $data['broker_information'] = $this->application_model->get_broker_information($id);
		//$data['ref_id']=$this->application_model->getid($id);
	    $result = $this->application_model->app_losses_db($id);
		$this->edit_model->app_edit_db($id);
	  //  $this->application_model->app_losses_db($id);

	    $this->load->view('application_applicant',$data);
			
			
			
			}else{
			   
			    if($id!=''){
			/*	 $data['quote_id'] = $this->input->post('quote_id');	
				$data['app_val'] = $this->input->post('id_text_app_val');*/
			   $data['app_com']=$this->application_model->get_database_inform($id,'application_commodity');
			 // print_r($data);
			  // die;
			   $data['app_trans']=$this->application_model->get_database_inform($id,'application_transported');
			   $data['app_cert']=$this->application_model->get_database_inform($id,'application_certificate');
			   $data['app_add']=$this->application_model->get_database_inform($id,'application_additional');
			   $data['app_own']=$this->application_model->get_database_inform($id,'application_owner_operator');
			   $data['app_fil']=$this->application_model->get_database_inform($id,'application_filings');
			   
			  // print_r($data['app_fil']);
			   $data['edit_app']=$this->application_model->get_database_inform($id,'application_ghi11');
			   $data['app_ghi6']=$this->application_model->get_database_inform($id,'application_ghi6');
			   $data['app_ghi5a']=$this->application_model->get_database_inform($id,'application_ghi5a');
			   $data['app_ghi5b']=$this->application_model->get_database_inform($id,'application_ghi5b');
			   $data['app_ghi5c']=$this->application_model->get_database_inform($id,'application_ghi5c');
			   $data['app_ghi5d']=$this->application_model->get_database_inform($id,'application_ghi5d');
			   $data['app_ghi5e']=$this->application_model->get_database_inform($id,'application_ghi5e');
			   $data['app_ghi5f']=$this->application_model->get_database_inform($id,'application_ghi5f');
			   $data['app_ghi9']=$this->application_model->get_database_inform($id,'application_ghi9');
			 
			    $data['quote_id'] = $this->input->post('quote_id');
			    $data['app_val'] = $this->input->post('id_text_app_val');
			    $data['app']=$this->application_model->get_data($id);
			   	$result = $this->application_model->app_losses_db($id);
				$this->edit_model->app_edit_db($id); 
			    $data['filing_type_show'] = $this->filing_type_show();
			 if(!empty($data['quote_id'])){
				 if(empty($data['app_fil'])){
			      $data['quote_filings']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote_filings');
			   }
			 }
		 		$this->load->view('application_commodity',$data);	  
			   }else{
			    $data['quote_id'] = $this->input->post('quote_id');
			    $data['app_val'] = $this->input->post('id_text_app_val');
			    $data['ref_id']=$this->application_model->getid();
				
				if($save=='save'){}else{
				$result = $this->application_model->app_losses_db();
				$this->edit_model->app_edit_db();
				
				}
			  	$data['filing_type_show'] = $this->filing_type_show();
			//	print_r($data['filing_type_show']);
			//	die;
			 if(!empty($data['quote_id'])){
			    $data['quote_filings']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote_filings');
			 }
			
 // print_r($data['quote_filings']);
 // die;
                $this->load->view('application_commodity',$data);	
							
			   }//redirect('application_commodity','refresh');				
				//$mail_sent = $this->GenerateQuickQuoteMail($result);
				//print_r($result);
			   }	//$this->session->set_flashdata('success', '<strong>Success</strong> Quote Request Sent Successfully');
			} 
	}     else{
				//$this->session->set_flashdata('error', '<strong>Error</strong> In Sending Quote Request ');
			}
	
			}
			function commodity($id='',$save=''){
	if($_POST){  //print_r($_POST); die;
			//print_r($_POST['tractor_number_of_axles']);
			//$myarray = array_values($myarray);
			//print_r($_FILES); die;
			//error_reporting(E_ALL);
					  if(isset($_POST['saved1'])){
				   $insert_id   =$this->input->post('insert_id');
				  $ref_id   = $this->input->post('ref_id');
				   $id=  !empty($insert_id)? $insert_id: $ref_id;
				   
				   if($id==''){
					$result = $this->application_model->app_commodity_db($id);
			  $this->edit_model->app_edit_ghi6($id);
			  $this->edit_model->app_edit_ghi9($id);
			  $this->edit_model->app_edit_ghi11($id);
			  $this->edit_model->app_edit_ghi5($id);
							$id= $getid[0]->general_id;
							 $this->losses($id,'save');
					   }else{
						   
						   $result = $this->application_model->app_commodity_db($id);
			                      $this->edit_model->app_edit_ghi6($id);
			                       $this->edit_model->app_edit_ghi9($id);
			                    $this->edit_model->app_edit_ghi11($id);
			                      $this->edit_model->app_edit_ghi5($id);
						   $this->losses($id,'save');
						   }
					  
				//  echo $id;
			     // die;
               	   
				  
				  }else{
			if(isset($_POST['back1'])){
		     $ref_id = $this->input->post('ref_id');
		 	 $id=!empty($id)? $id : $ref_id;
		     $data['los_id']='value';
		   	   $data['app_val'] = $this->input->post('id_text_app_val');
			   
			   			 
			    $data['quote_id'] = $this->input->post('quote_id');	
				//print_r($data);
				//die;
				$data['app']=$this->application_model->get_data($id);
			    $data['app_loss']=$this->application_model->get_database_inform($id,'application_losses');
			    $data['app_loss1']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_liability');
			    $data['app_loss2']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_pd');
	    	    $data['app_loss3']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_cargo');
				$data['app_loss4']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_bobtail');
				$data['app_loss5']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_dead');
				$data['app_loss6']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_trunk');
				$data['app_loss7']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_excess');
				$data['app_ghi8']=$this->application_model->get_database_inform($id,'application_ghi8');
             
            //  $data['ref_id']=$this->application_model->getid($id);
			 // $data['app']=$this->application_model->get_data($id);
			//  print_r($data);
			 // die;
			  $data['quote_id'] = $this->input->post('quote_id');
			  $data['app_val'] = $this->input->post('id_text_app_val');
			  $result = $this->application_model->app_commodity_db($id);
			  $this->edit_model->app_edit_ghi6($id);
			  $this->edit_model->app_edit_ghi9($id);
			  $this->edit_model->app_edit_ghi11($id);
			  $this->edit_model->app_edit_ghi5($id);
             		  
			 // $this->load->view('application_vehicle',$data);
              $this->load->view('application_losses',$data);
			
			// $this->load->view('application_losses');
			}else{
				//$vehicle_id = $this->input->post('vehicle_id');
				//$vehicle_id=$this->application_model->get_database_inform($id,'application_vehicle');
			
				if($id!=''){
			    $data['quote_id'] = $this->input->post('quote_id');
			   $data['app_load']=$this->application_model->get_database_inform($id,'application_loaded');
			   $data['app_cov']=$this->application_model->get_database_inform($id,'application_coverage');
               $data['app_veh1']=$this->application_model->get_database_inform2($id,'application_vehicle','TRUCK');
			   $data['app_veh2']=$this->application_model->get_database_inform2($id,'application_vehicle','TRACTOR');
	    	   $data['app_veh3']=$this->application_model->get_database_inform2($id,'application_vehicle','TRAILER');
			   $data['app_veh4']=$this->application_model->get_database_inform2($id,'application_vehicle','TRAILER_SEP');			  
					
					
					//print_r($data['app_veh4']);	
					//die;
			   $data['app_veh_pd']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRUCK');
			   $data['app_veh_pd1']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRACTOR');
			   $data['app_ghi13']=$this->application_model->get_database_inform($id,'application_ghi13');
		       $data['app_driver']=$this->application_model->get_database_inform($id,'application_driver');
			   
			   
			   
			  // print_r($data['app_driver']);
			   
			  // $data['app_driver']=$this->application_model->get_database_quote(1,'rq_rqf_quote_drivers');
			//print_r($data['app_driver']);
		//	die;
	/*		if(empty($data['quote_id'])){
				if(empty($data['app_veh1'])){
			   $data['app_veh12']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRUCK');
				}
				if(empty($data['app_veh2'])){
			  
			   $data['app_veh22']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRACTOR');
				}
				if(empty($data['app_veh3'])){
			  
			   $data['app_veh32']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRAILER');
			   
				}
				
			if(empty($data['app_driver'])){
			   $data['app_driver1']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote_drivers');
			 // print_r($data['app_driver1']);
			  //die;
			   }
			}*/
				if(!empty($data['quote_id'])){
				if(empty($data['app_veh1'])){
			   $data['app_veh12']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRUCK');
				}
				if(empty($data['app_veh2'])){
			   $data['app_veh22']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRACTOR');
				}
				if(empty($data['app_veh3'])){
	    	   $data['app_veh32']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRAILER');
				}
			   
			  // $data['app_driver1']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote_drivers');
			  
			}
		    	////print_r($data['app_veh12']);
				//die;
			  $data['app']=$this->application_model->get_data($id);
			 
			  $data['app_val'] = $this->input->post('id_text_app_val');
			  
		      if($save=='save'){}else{
			  $result = $this->application_model->app_commodity_db($id);
			  
			  $this->edit_model->app_edit_ghi6($id);
			  $this->edit_model->app_edit_ghi9($id);
			  $this->edit_model->app_edit_ghi11($id);
			  $this->edit_model->app_edit_ghi5($id);
			  }
			  $this->load->view('application_vehicle',$data);
			  			  
			   } else {
			  $data['ref_id']=$this->application_model->getid();	
			//  $data['app']=$this->application_model->get_data($id);
			  $data['quote_id'] = $this->input->post('quote_id');
			  $data['app_val'] = $this->input->post('id_text_app_val');
			
			
				 //  $data['app_driver']=$this->application_model->get_database_quote(1,'rq_rqf_quote_drivers');
		
			if(!empty($data['quote_id'])){
				
			   $data['app_veh12']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRUCK');
			   $data['app_veh22']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRACTOR');
	    	   $data['app_veh32']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRAILER');
			   
			 //  $data['app_driver1']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote_drivers');
			  
			}
				
			  $result = $this->application_model->app_commodity_db($id);
			  $this->edit_model->app_edit_ghi6($id);
			  $this->edit_model->app_edit_ghi9($id);
			  $this->edit_model->app_edit_ghi11($id);
			//  die;
			  $this->edit_model->app_edit_ghi5($id);
             		  
			  $this->load->view('application_vehicle',$data);
			   }
			}
                //redirect('application_vehicle','refresh');				
				//$mail_sent = $this->GenerateQuickQuoteMail($result);
				//print_r($result);
				//$this->session->set_flashdata('success', '<strong>Success</strong> Quote Request Sent Successfully');
			} 
			
	}else{
				//$this->session->set_flashdata('error', '<strong>Error</strong> In Sending Quote Request ');
			}
			}
			function vehicle($id=''){
	if($_POST){  //print_r($_POST); die;
			//print_r($_POST['tractor_number_of_axles']);
			//$myarray = array_values($myarray);
			//print_r($_FILES); die;
			//error_reporting(E_ALL);
							  if(isset($_POST['saved2'])){
				   $insert_id   =$this->input->post('insert_id');
				  $ref_id   = $this->input->post('ref_id');
				  $id=  !empty($insert_id)? $insert_id: $ref_id;
				   
				   if($id==''){
				$result = $this->application_model->app_vehicle_db($id);	
				$this->edit_model->app_edit_ghi13($id);
							$id= $getid[0]->general_id;
							 $this->commodity($id,'save');
					   }else{
						   
						$result = $this->application_model->app_vehicle_db($id);	
				$this->edit_model->app_edit_ghi13($id);
						   $this->commodity($id,'save');
						   }
					  
				//  echo $id;
			     // die;
               	   
				  
				  }else{
				if(isset($_POST['back2'])){
					
		    	 $ref_id = $this->input->post('ref_id');
		 	   $id=!empty($id)? $id : $ref_id;
			   $data['app']=$this->application_model->get_data($id);
			   $data['app_com']=$this->application_model->get_database_inform($id,'application_commodity');
			   $data['app_trans']=$this->application_model->get_database_inform($id,'application_transported');
			   $data['app_cert']=$this->application_model->get_database_inform($id,'application_certificate');
			   $data['app_add']=$this->application_model->get_database_inform($id,'application_additional');
			   $data['app_own']=$this->application_model->get_database_inform($id,'application_owner_operator');
			   $data['app_fil']=$this->application_model->get_database_inform($id,'application_filings');
			   $data['edit_app']=$this->application_model->get_database_inform($id,'application_ghi11');
			   $data['app_ghi6']=$this->application_model->get_database_inform($id,'application_ghi6');
			   $data['app_ghi5a']=$this->application_model->get_database_inform($id,'application_ghi5a');
			   $data['app_ghi5b']=$this->application_model->get_database_inform($id,'application_ghi5b');
			   $data['app_ghi5c']=$this->application_model->get_database_inform($id,'application_ghi5c');
			   $data['app_ghi5d']=$this->application_model->get_database_inform($id,'application_ghi5d');
			   $data['app_ghi5e']=$this->application_model->get_database_inform($id,'application_ghi5e');
			   $data['app_ghi5f']=$this->application_model->get_database_inform($id,'application_ghi5f');
			   $data['app_ghi9']=$this->application_model->get_database_inform($id,'application_ghi9');
		       
			   $data['quote_id'] = $this->input->post('quote_id');
			   $data['app_val'] = $this->input->post('id_text_app_val');
			 
			  //  $data['quote_id'] = $this->input->post('quote_id');
			    $data['broker_id'] = $this->application_model->get_broker_inform($id);
		      //  $data['list']=$this->application_model->get_data();
				$result = $this->application_model->app_vehicle_db($id);	
				$this->edit_model->app_edit_ghi13($id);
				
				$data['filing_type_show'] = $this->filing_type_show();
				
				$this->load->view('application_commodity',$data);
			 
			}else{
			
			
			    $data['quote_id'] = $this->input->post('quote_id');
			    $data['broker_id'] = $this->application_model->get_broker_inform($id);
		        $data['list']=$this->application_model->get_data();
				$result = $this->application_model->app_vehicle_db($id);	
				$this->edit_model->app_edit_ghi13($id);
			    $pdf_id = $this->input->post('ref_id');
				redirect('/pdf/GeneratePDF2/'.$pdf_id);
		 	   // $this->GeneratePDF2($pdf_id);
				
				//base_url();
			//die;
				//$this->load->view('application_list',$data);
				
				}
               // redirect('application_applicant','refresh');				
				//$mail_sent = $this->GenerateQuickQuoteMail($result);
				//print_r($result);
				//$this->session->set_flashdata('success', '<strong>Success</strong> Quote Request Sent Successfully');
		    	} 
	         } else{
				//$this->session->set_flashdata('error', '<strong>Error</strong> In Sending Quote Request ');
			}
			
			
			}




		
		public function newMembers(){		
		$id = $this->input->post('ids');
		$data['broker_inform'] = $this->application_model->get_broker_inform_mem($id);
       // $data['broker_information'] = $this->application_model->get_broker_information(ids;
	echo 	json_encode($data);
		//print_r($data);
		//$this->load->view('edit_owner', $data);	
	}
	public function app_form($id=""){		
	echo $id;
    $request = $this->input->post('data');
	 	echo 	json_encode($request);
	$this->application_model->app_losses_db($id);
	$this->edit_model->app_edit_db($id);
	  //  $this->load->view('application_applicant',$data);
		//$this->load->view('application_applicant', $data);	
	}
			public function applicant_scheduled($id){		
		$data['quote_id'] = $id;
		//print_r($data);
		$this->load->view('edit_scheduled', $data);	
	}
	
	
	function filing_type_show() {
		$catlog ='';
		$catlog .='<select id="filing_type_otherABC123" class="span12 filing_type filing_type_add" required="required" name="filings_type[]"  onChange="filling_other(this.value,this.id)" >';
		$catlog .='<option value="">Select</option>';
		$catlog_data = $this->quote_model->get_catlog_options(25);
		//print_r($catlog_data);die;
		if($catlog_data)
		{
			foreach ($catlog_data as $catlogData)
				$catlog .='<option value="'.$catlogData->catlog_value.'">'.$catlogData->catlog_value.'</option>';	
		}
		$catlog .='<option value="Other">Other</option>';
		$catlog .='</select>';
			return $catlog;
    }
	
	
    /* public function GeneratePDF2($id = '')
	{
	$data['app']=$this->application_model->get_data($id);
	$data['app_applicant']=$this->application_model->get_data_applicant($id);
	$data['app_tel_add']=$this->application_model->get_data_tel_add($id);
	
	$data['app_email_add']=$this->application_model->get_data_email_add($id);
	//$data['broker_inform'] = $this->application_model->get_broker_inform();
	
    $data['broker_insured'] = $this->application_model->get_broker_insured($id);
	//print_r($data['broker_insured']);
	
	    // print_r($data['app_loss']);
	 //	die;	
				$this->load->view('MPDF53/mpdf.php');	
				$mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0);
 
				$mpdf->SetDisplayMode('fullpage');
				 
				$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
				 
				/* $php = base_url().'pdfreport.php';
									;
				$stylesheet = base_url('css').'/style.css';
				$stylesheetcont = file_get_contents($stylesheet);
				
				$data['ajax_req'] = TRUE;
				
				$filecont = $this->load->view('application_applicant',$data, TRUE);
				//$stylesheetcont = file_get_contents($stylesheet);
				//$path_to_view = get_view_path('pdfapp');
			
				//$filecont = file_get_contents($path_to_view);
				
				//$mpdf->WriteHTML($stylesheetcont,1);			
				//$mpdf->WriteHTML(file_get_contents(base_url().'application/modules/view/pdf/pdfreport.php'));
			
				$mpdf->WriteHTML($filecont);
				 
				$mpdf->Output();
							
		/*	}
	}*/
	
	public function getState()
	{
		 $id = $_POST['id'];
	    $name = $_POST['name'];
	  	$attr = $_POST['attr'];
	    getStateDropDown($id, $name, $attr='',$selected='');
		
		//die;
		
		
	}

}




/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */