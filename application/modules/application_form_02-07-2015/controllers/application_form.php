<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Application_form extends RQ_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct()
	{
		parent::__construct();
		
	/*	if(!$this->require_login_user() && !$this->require_login_underwriter())

		{

			$base = base_url('login');

			redirect($base);

		}*/
		
			$this->load->model('application_model');
			$this->load->model('edit_model');
	       // $this->load->library('session');
		    $this->load->model('quote/quote_model');
	}
	 
	public function index()
	{
		
		
      if(!$this->require_login_user() && !$this->require_login_underwriter())

		{

			$base = base_url('login');

			redirect($base);

		}
		
		$data['user'] = 'member';

		if ($this->session->userdata('underwriter_id')) {

			$data['user'] = 'underwriter';

		}
       
	    
	    $data['broker_id'] = $this->application_model->get_broker_quote($id='');
		$data['list']=$this->application_model->get_data();
		//print_r($data);
		$this->load->view('application_list',$data);
		//$this->load->view('application_applicant');
		
		//redirect(base_url('application_form'));
	}
	
	/*public function genrate_quote(){
	
	
	}*/
	

  function quote_request($edit='',$id='',$print=''){
  $data['print']=$print; 
  $data['broker_inform'] = $this->application_model->get_broker_inform();
  
	if($id!=''){
	
	  if($edit=='edit'){
		$data['app']=$this->application_model->get_data($id);
		$data['app_applicant']=$this->application_model->get_data_applicant($id);
		$data['app_tel_add']=$this->application_model->get_data_tel_add($id);
		$data['adresses']=$this->application_model->get_database_inform($id,'adresses');
		$data['app_email_add']=$this->application_model->get_data_email_add($id);
		$data['broker_information'] = $this->application_model->get_broker_information($id);
		
		
		
	    //for losses section ashvin patel 4/nov/2014
		//$data['app_val'] = $this->input->post('id_text_app_val');			 
		$data_loss['quote_id'] = $this->input->post('quote_id');	
		$loss = $this->application_model->get_database_inform($id,'application_losses');
		
		$data_loss['app_loss']= $loss;
	//	print_r($data_loss['app_loss']);
		$data_loss['app_loss1']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_liability');
		$data_loss['app_loss2']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_pd');
		$data_loss['app_loss3']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_cargo');
		//print_r($data_loss['app_loss3']);
		$data_loss['app_loss4']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_bobtail');
		$data_loss['app_loss5']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_dead');
		$data_loss['app_loss6']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_trunk');
		$data_loss['app_loss7']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_excess');
		$data_loss['app_ghi8']=$this->application_model->get_database_inform($id,'application_ghi8');
		
		$data_loss['app_ghi1a']=$this->application_model->get_database_inform($id,'application_ghi1a');
		$data_loss['app_ghi1b']=$this->application_model->get_database_inform($id,'application_ghi1b');
		
		$data_loss['los_id']='value';
		if(!empty($data_loss['quote_id'])){
		 if(empty($data_loss['app_loss1'])){
		  $data_loss['app_loss1']=$this->application_model->get_database_quote1($data_loss['quote_id'],'rq_rqf_quote_losses','losses_liability');
		  $data_loss['upload_file']=$this->application_model->get_database_quote($data_loss['quote_id'],'rq_rqf_quote');
		 }
		 if(empty($data_loss['app_loss2'])){
		  $data_loss['app_loss2']=$this->application_model->get_database_quote1($data_loss['quote_id'],'rq_rqf_quote_losses','losses_pd');
		 }
		 if(empty($data_loss['app_loss3'])){
		  $data_loss['app_loss3']=$this->application_model->get_database_quote1($data_loss['quote_id'],'rq_rqf_quote_losses','losses_cargo');
		 }
		}
		$data['data_loss'] = $data_loss;
		//for losses section end ashvin patel 4/nov/2014
		
		//for commodity section start ashvin patel 4/nov/2014
		 $data['app_com']=$this->application_model->get_database_inform($id,'application_commodity');
	  	//print_r($data['app_com']);
		 $data['app_trans']=$this->application_model->get_database_inform($id,'application_transported');
		// print_r($data['app_trans']);
		 $data['app_cert']=$this->application_model->get_database_inform($id,'application_certificate');
		 // print_r($data['app_cert']);
		
		 $data['app_add']=$this->application_model->get_database_inform($id,'application_additional');
		// print_r($data['app_add']);
		 $data['app_own']=$this->application_model->get_database_inform($id,'application_owner_operator');
		// print_r($data['app_own']);
		 $data['app_fil']=$this->application_model->get_database_inform($id,'application_filings');
		 
		
		 $data['edit_app']=$this->application_model->get_database_inform($id,'application_ghi11');
		 $data['app_ghi6']=$this->application_model->get_database_inform6($id,'application_ghi6',18);
         $data['app_ghi61']=$this->application_model->get_database_inform6($id,'application_ghi6',20);
         $data['app_ghi62']=$this->application_model->get_database_inform6($id,'application_ghi6',21);

						   
						 
		 $data['app_ghi5a']=$this->application_model->get_database_inform8($id,'application_ghi5a',24);


		 $data['app_ghi5b']=$this->application_model->get_database_inform8($id,'application_ghi5b',24);
		 $data['app_ghi5c']=$this->application_model->get_database_inform8($id,'application_ghi5c',24);
		 $data['app_ghi5d']=$this->application_model->get_database_inform8($id,'application_ghi5d',24);
		 $data['app_ghi5e']=$this->application_model->get_database_inform8($id,'application_ghi5e',24);
		 $data['app_ghi5f']=$this->application_model->get_database_inform8($id,'application_ghi5f',24);
		 $data['app_ghi5g']=$this->application_model->get_database_inform8($id,'application_ghi5g',24);
		 
		 
		 $data['app_ghi51a']=$this->application_model->get_database_inform8($id,'application_ghi5a',23);


		 $data['app_ghi51b']=$this->application_model->get_database_inform8($id,'application_ghi5b',23);
		 $data['app_ghi51c']=$this->application_model->get_database_inform8($id,'application_ghi5c',23);
		 $data['app_ghi51d']=$this->application_model->get_database_inform8($id,'application_ghi5d',23);
		 $data['app_ghi51e']=$this->application_model->get_database_inform8($id,'application_ghi5e',23);
		 $data['app_ghi51f']=$this->application_model->get_database_inform8($id,'application_ghi5f',23);
		 		 $data['app_ghi51g']=$this->application_model->get_database_inform8($id,'application_ghi5g',23);
		
		
		 
		 $data['app_ghi9']=$this->application_model->get_database_inform($id,'application_ghi9');
		 
		 
	   
		 $data['quote_id'] = $this->input->post('quote_id');
		 $data['app_val'] = $this->input->post('id_text_app_val');
		 
		 $data['filing_type_show'] = $this->filing_type_show();
		 
			   $data['app_load']=$this->application_model->get_database_inform($id,'application_loaded');
			   $data['app_cov']=$this->application_model->get_database_inform($id,'application_coverage');
               $data['app_veh1']=$this->application_model->get_database_inform2($id,'application_vehicle','TRUCK');
			   $data['app_veh2']=$this->application_model->get_database_inform2($id,'application_vehicle','TRACTOR');
	    	   $data['app_veh3']=$this->application_model->get_database_inform2($id,'application_vehicle','TRAILER');
			   $data['app_veh4']=$this->application_model->get_database_inform2($id,'application_vehicle','TRAILER_SEP');			  
					
				
			   $data['app_veh_pd']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRUCK');
			   $data['app_veh_pd1']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRACTOR');
			   $data['app_ghi13']=$this->application_model->get_database_inform($id,'application_ghi13');
		       $data['app_driver']=$this->application_model->get_database_inform($id,'application_driver');
			   
			
				if(!empty($data['quote_id'])){
				if(empty($data['app_veh1'])){
			   $data['app_veh12']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRUCK');
				}
				if(empty($data['app_veh2'])){
			   $data['app_veh22']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRACTOR');
				}
				if(empty($data['app_veh3'])){
	    	   $data['app_veh32']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRAILER');
				}
				}
		 if(!empty($data['quote_id'])){
			 if(empty($data['app_fil'])){
			  $data['quote_filings']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote_filings');
		   }
		 }
		//for commodity section end ashvin patel 4/nov/2014	  
			   
	  }
	  if($edit=='app'){
		$data['Quote_num']=$id;	
		$rq_rqf_quote=$this->application_model->get_database_quote($id,'rq_rqf_quote');
		$data['broker_inform1'] = $this->application_model->get_broker_inform($rq_rqf_quote[0]->email);
		$data['rq_rqf_quote_losses']=$this->application_model->get_database_quote($id,'rq_rqf_quote_losses');
		$data['broker_insured'] = $this->application_model->get_broker_insured($id);
		$data['app']=$this->application_model->get_database_quote($id,'application_general');
		$data['app_applicant']=$this->application_model->get_database_quote($id,'application_applicant');
		$data['app_tel_add']=$this->application_model->get_database_quote($id,'application_tel_add');
		$data['adresses']=$this->application_model->get_database_quote($id,'adresses');
		$data['app_email_add']=$this->application_model->get_database_quote($id,'application_email_add');
		
		
		$data['app_loss1']=$this->application_model->get_database_quote1($id,'rq_rqf_quote_losses','losses_liability');
		$data['app_loss2']=$this->application_model->get_database_quote1($id,'rq_rqf_quote_losses','losses_pd');
		$data['app_loss3']=$this->application_model->get_database_quote1($id,'rq_rqf_quote_losses','losses_cargo');
		$data['upload_file']=$this->application_model->get_database_quote($id,'rq_rqf_quote');
		
		
				
	    $data['app_veh12']=$this->application_model->get_database_quote2($id,'rq_rqf_quote_vehicle','TRUCK');
	    $data['app_veh22']=$this->application_model->get_database_quote2($id,'rq_rqf_quote_vehicle','TRACTOR');
	    $data['app_veh32']=$this->application_model->get_database_quote2($id,'rq_rqf_quote_vehicle','TRAILER');
			   
	
		
	  }
	
	}else{
	
	$member_id=$this->session->userdata('member_id');
    $rq_rqf_quote=$this->application_model->get_database_members($member_id,'rq_members');
	$rq_rqf_quote[0]->email;
	$data['broker_inform1'] = $this->application_model->get_broker_inform($rq_rqf_quote[0]->email);

	 $data['quote_id'] = $this->input->post('quote_id');
	 
	}
	if ($this->session->userdata('member_id')) {
	 $data['underwriter'] = get_member_underwriter();

	 }
	 $data['filing_type_show'] = $this->filing_type_show();
	$this->load->view('application_applicant',$data);
	
  }
	
	
  //Submit application form Ashvin Patel 3/nov/2014
  public function submit_applicant($id=''){
 	
	  $ref_id = $this->applicant();
	  $this->losses($ref_id);
	  $this->commodity($ref_id);
	  $this->vehicle($ref_id);

  }
public function generate_pdf($html_page, $file_name){

 $fh = fopen('uploads/application_pdf/application_pdf.html', 'w') or die("can't open file");

 fwrite($fh,'');

 fwrite($fh,$html_page);

 fclose($fh);

 
  echo $cmd = "/usr/local/bin/wkhtmltopdf file:///opt/lampp/htdocs/quote_process/uploads/application_pdf/application_pdf.html file:///opt/lampp/htdocs/quote_process/uploads/application_pdf/".$file_name."";

  $t = shell_exec($cmd);

 
 //$cmd = "/usr/local/bin/wkhtmltopdf  --page-size A4 --viewport-size 1480x864 uploads/application_pdf/application_pdf.html uploads/application_pdf/".$file_name."";

 // $t = shell_exec($cmd);
   
   
	
	// passthru("D:/xampp/htdocs/quote-process/application/third_party/wkhtmltopdf/bin/wkhtmltopdf.exe --page-size A4 --viewport-size 1480x864 uploads/application_pdf/application_pdf.html uploads/application_pdf/".$file_name."",$err); 
	 //return $err; 
  }
  public function send_application_pdf_mail($filename){
	$filename = 'uploads/application_pdf/'.$filename;
	$to = get_member_underwriter();
	$sub = 'Application Request from '.$this->session->userdata('member_name');
	$msg = 'PFA';
	$data = array(
		'address' => $to,
		'sub' => $sub,
		'msg' => $msg,
		'filename' => $filename
	);
	$result = sendEmail($data);
	//print_r($result);
  }
  public function get_pdf_test($ref_id){

	$html_data = $this->application_pdf($ref_id);	
	$file_name = $ref_id.'_'.time().'.pdf';
	$this->generate_pdf($html_data, $file_name);
  }
  function applicant($id='',$save=''){
	if($_POST){ 
	  if(isset($_POST['save'])){
	   $id = $this->input->post('insert_id');
		 if($id==''){
		   $result = $this->application_model->app_applicant_db($id);
		   $getid = $this->application_model->getid();
		   $id= $getid[0]->general_id;
		   return $id;
		
		 }else{
		   $result = $this->application_model->app_applicant_db($id);
		   return $id;
		 
		 }
	  }else{ 
		if($id!=''){
		  $data['app_val'] = $this->input->post('id_text_app_val');			 
		  $data['quote_id'] = $this->input->post('quote_id');	
		  $data['app']=$this->application_model->get_data($id);
		  $data['app_loss']=$this->application_model->get_database_inform($id,'application_losses');
		  $data['app_loss1']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_liability');
		  $data['app_loss2']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_pd');
		    print_r($data['app_veh2']);
			  die;
		  $data['app_loss3']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_cargo');
		  $data['app_loss4']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_bobtail');
		  $data['app_loss5']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_dead');
		  $data['app_loss6']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_trunk');
		  $data['app_loss7']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_excess');
		  $data['app_ghi8']=$this->application_model->get_database_inform($id,'application_ghi8');
		  $data['los_id']='value';
		  if(!empty($data['quote_id'])){
		   if(empty($data['app_loss1'])){
			$data['app_loss1']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_liability');
			$data['upload_file']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote');
		   }
		   if(empty($data['app_loss2'])){
			$data['app_loss2']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_pd');
		   }
		   if(empty($data['app_loss3'])){
			$data['app_loss3']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_cargo');
		   }
		  }
		  if($save=='save'){
		  }else{
			$result = $this->application_model->app_applicant_db($id);
		  }
		}else{
		 $data['app_val'] = $this->input->post('id_text_app_val');
		 $data['quote_id'] = $this->input->post('quote_id');	   
		 $result = $this->application_model->app_applicant_db($id);
		 $data['ref_id']=$this->application_model->getid();
		 if(!empty($data['quote_id'])){
		  $data['app_loss1']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_liability');
		  $data['app_loss2']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_pd');
		  $data['app_loss3']=$this->application_model->get_database_quote1($data['quote_id'],'rq_rqf_quote_losses','losses_cargo');
		  $data['upload_file']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote');
		 }
		}
	  }
	}
  }
  
			function losses($id='',$save=''){
				
				
	if($_POST){  
							
					  if(isset($_POST['saved'])){
				 $insert_id = $id;
				  $ref_id   = $id;
				  $id=  !empty($insert_id)? $insert_id: $ref_id;
				
				
				  
				  }else{
			
			
			if(isset($_POST['back'])){
	      $ref_id = $id;
		  $id=!empty($id)? $id : $ref_id;
		
	    $data['quote_id'] = $this->input->post('quote_id');
        $data['app_val'] = $this->input->post('id_text_app_val');
			   
	    $data['app']=$this->application_model->get_data($id);	
	    $data['app_applicant']=$this->application_model->get_data_applicant($id);
	    $data['app_tel_add']=$this->application_model->get_data_tel_add($id);
	    $data['adresses']=$this->application_model->get_database_inform($id,'adresses');
	    $data['app_email_add']=$this->application_model->get_data_email_add($id);
	    $data['broker_inform'] = $this->application_model->get_broker_inform();
        $data['broker_information'] = $this->application_model->get_broker_information($id);
		
	    $result = $this->application_model->app_losses_db($id);
		$this->edit_model->app_edit_db($id);

			
			}else{
			   
			    if($id!=''){
			
			   $data['app_com']=$this->application_model->get_database_inform($id,'application_commodity');
		
			   $data['app_trans']=$this->application_model->get_database_inform($id,'application_transported');
			   $data['app_cert']=$this->application_model->get_database_inform($id,'application_certificate');
			   $data['app_add']=$this->application_model->get_database_inform($id,'application_additional');
			   $data['app_own']=$this->application_model->get_database_inform($id,'application_owner_operator');
			   $data['app_fil']=$this->application_model->get_database_inform($id,'application_filings');
			   
			
			   $data['edit_app']=$this->application_model->get_database_inform($id,'application_ghi11');
			   $data['app_ghi6']=$this->application_model->get_database_inform($id,'application_ghi6');
			   $data['app_ghi5a']=$this->application_model->get_database_inform($id,'application_ghi5a');
			   $data['app_ghi5b']=$this->application_model->get_database_inform($id,'application_ghi5b');
			   $data['app_ghi5c']=$this->application_model->get_database_inform($id,'application_ghi5c');
			   $data['app_ghi5d']=$this->application_model->get_database_inform($id,'application_ghi5d');
			   $data['app_ghi5e']=$this->application_model->get_database_inform($id,'application_ghi5e');
			   $data['app_ghi5f']=$this->application_model->get_database_inform($id,'application_ghi5f');
			   $data['app_ghi9']=$this->application_model->get_database_inform($id,'application_ghi9');
			 
			    $data['quote_id'] = $this->input->post('quote_id');
			    $data['app_val'] = $this->input->post('id_text_app_val');
			    $data['app']=$this->application_model->get_data($id);
			   	$result = $this->application_model->app_losses_db($id);
				$this->edit_model->app_edit_db($id); 
				$this->edit_model->application_ghi1a($id); 
				$this->edit_model->application_ghi1b($id); 
			    $data['filing_type_show'] = $this->filing_type_show();
			 if(!empty($data['quote_id'])){
				 if(empty($data['app_fil'])){
			      $data['quote_filings']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote_filings');
			   }
			 }
		 	
			   }else{
			    $data['quote_id'] = $this->input->post('quote_id');
			    $data['app_val'] = $this->input->post('id_text_app_val');
			    $data['ref_id']= $this->application_model->getid();
				
				if($save=='save'){}else{
				$result = $this->application_model->app_losses_db();
				$this->edit_model->app_edit_db();
				
				}
			  	$data['filing_type_show'] = $this->filing_type_show();
		
			 if(!empty($data['quote_id'])){
			    $data['quote_filings']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote_filings');
			 }
			

							
			   }
			   }	
			} 
	}     else{
				
			}
	
			}
			function commodity($id='',$save=''){
	if($_POST){  
					  if(isset($_POST['saved1'])){
				   $insert_id = $id;
				      $ref_id = $id;
				   $id=  !empty($insert_id)? $insert_id: $ref_id;
				   
				   if($id==''){
					$result = $this->application_model->app_commodity_db($id);
			  $this->edit_model->app_edit_ghi6($id);
			  $this->edit_model->app_edit_ghi9($id);
			  $this->edit_model->app_edit_ghi11($id);
			  $this->edit_model->app_edit_ghi5($id);
			  $this->edit_model->app_edit_ghi51($id);
							$id= $getid[0]->general_id;
							 
					   }else{
						   
						   $result = $this->application_model->app_commodity_db($id);
			               $this->edit_model->app_edit_ghi6($id);
			               $this->edit_model->app_edit_ghi9($id);
			               $this->edit_model->app_edit_ghi11($id);
			               $this->edit_model->app_edit_ghi5($id);
					
						   }
			
				  
				  }else{
			if(isset($_POST['back1'])){
		     $ref_id = $id;
		 	 $id=!empty($id)? $id : $ref_id;
		     $data['los_id']='value';
		   	   $data['app_val'] = $this->input->post('id_text_app_val');
			   
			   			 
			    $data['quote_id'] = $this->input->post('quote_id');	
		
				$data['app']=$this->application_model->get_data($id);
			    $data['app_loss']=$this->application_model->get_database_inform($id,'application_losses');
			    $data['app_loss1']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_liability');
			    $data['app_loss2']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_pd');
	    	    $data['app_loss3']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_cargo');
				$data['app_loss4']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_bobtail');
				$data['app_loss5']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_dead');
				$data['app_loss6']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_trunk');
				$data['app_loss7']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_excess');
				$data['app_ghi8']=$this->application_model->get_database_inform($id,'application_ghi8');
 
			  $data['quote_id'] = $this->input->post('quote_id');
			  $data['app_val'] = $this->input->post('id_text_app_val');
			  $result = $this->application_model->app_commodity_db($id);
			  $this->edit_model->app_edit_ghi6($id);
			  $this->edit_model->app_edit_ghi9($id);
			  $this->edit_model->app_edit_ghi11($id);
			  $this->edit_model->app_edit_ghi5($id);
             $this->edit_model->app_edit_ghi51($id);		  
		
			}else{
			
			
			if($id!=''){
			   $data['quote_id'] = $this->input->post('quote_id');
			   $data['app_load']=$this->application_model->get_database_inform($id,'application_loaded');
			   $data['app_cov']=$this->application_model->get_database_inform($id,'application_coverage');
               $data['app_veh1']=$this->application_model->get_database_inform2($id,'application_vehicle','TRUCK');
			   $data['app_veh2']=$this->application_model->get_database_inform2($id,'application_vehicle','TRACTOR');
			
	    	   $data['app_veh3']=$this->application_model->get_database_inform2($id,'application_vehicle','TRAILER');
			   $data['app_veh4']=$this->application_model->get_database_inform2($id,'application_vehicle','TRAILER_SEP');			  
					
					
				
			   $data['app_veh_pd']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRUCK');
			   $data['app_veh_pd1']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRACTOR');
			   $data['app_ghi13']=$this->application_model->get_database_inform($id,'application_ghi13');
		       $data['app_driver']=$this->application_model->get_database_inform($id,'application_driver');
			   
			   
			 
				if(!empty($data['quote_id'])){
				if(empty($data['app_veh1'])){
			   $data['app_veh12']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRUCK');
				}
				if(empty($data['app_veh2'])){
			   $data['app_veh22']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRACTOR');
				}
				if(empty($data['app_veh3'])){
	    	   $data['app_veh32']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRAILER');
				}
		
			}
		    	
			  $data['app']=$this->application_model->get_data($id);
			 
			  $data['app_val'] = $this->input->post('id_text_app_val');
			  
		      if($save=='save'){}else{
			  $result = $this->application_model->app_commodity_db($id);
			  
			  $this->edit_model->app_edit_ghi6($id);
			  $this->edit_model->app_edit_ghi9($id);
			  $this->edit_model->app_edit_ghi11($id);
			  $this->edit_model->app_edit_ghi5($id);
			  $this->edit_model->app_edit_ghi51($id);
			  }
					  
			   } else {
			  $data['ref_id']=$this->application_model->getid();	
		
			  $data['quote_id'] = $this->input->post('quote_id');
			  $data['app_val'] = $this->input->post('id_text_app_val');
			
		
			if(!empty($data['quote_id'])){
				
			   $data['app_veh12']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRUCK');
			   $data['app_veh22']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRACTOR');
	    	   $data['app_veh32']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRAILER');
			  
			}
			
			  $result = $this->application_model->app_commodity_db($id);
			  $this->edit_model->app_edit_ghi6($id);
			  $this->edit_model->app_edit_ghi9($id);
			  $this->edit_model->app_edit_ghi11($id);
		
			  $this->edit_model->app_edit_ghi5($id);
              $this->edit_model->app_edit_ghi51($id);    		  
			
			   }
			}
              
			} 
			
	}else{
				
			}
			}
	function vehicle($id=''){
	    if($_POST){ 
				if(isset($_POST['saved2'])){
				  $insert_id = $id;
				  $ref_id   = $id;
				  $id=  !empty($insert_id)? $insert_id: $ref_id;
				   
				   if($id==''){
				$result = $this->application_model->app_vehicle_db($id);	
				$this->edit_model->app_edit_ghi13($id);
				$id= $getid[0]->general_id;
							
					   }else{
						   
				$result = $this->application_model->app_vehicle_db($id);
				$this->edit_model->app_edit_ghi13($id);
						 
						   }
			
				  
				  }else{
				if(isset($_POST['back2'])){
					
		    	 $ref_id = $id;
		 	   $id=!empty($id)? $id : $ref_id;
			   $data['app']=$this->application_model->get_data($id);
			   $data['app_com']=$this->application_model->get_database_inform($id,'application_commodity');
			   $data['app_trans']=$this->application_model->get_database_inform($id,'application_transported');
			   $data['app_cert']=$this->application_model->get_database_inform($id,'application_certificate');
			   $data['app_add']=$this->application_model->get_database_inform($id,'application_additional');
			   $data['app_own']=$this->application_model->get_database_inform($id,'application_owner_operator');
			   $data['app_fil']=$this->application_model->get_database_inform($id,'application_filings');
			   $data['edit_app']=$this->application_model->get_database_inform($id,'application_ghi11');
			
			   $data['app_ghi5a']=$this->application_model->get_database_inform($id,'application_ghi5a');
			   $data['app_ghi5b']=$this->application_model->get_database_inform($id,'application_ghi5b');
			   $data['app_ghi5c']=$this->application_model->get_database_inform($id,'application_ghi5c');
			   $data['app_ghi5d']=$this->application_model->get_database_inform($id,'application_ghi5d');
			   $data['app_ghi5e']=$this->application_model->get_database_inform($id,'application_ghi5e');
			   $data['app_ghi5f']=$this->application_model->get_database_inform($id,'application_ghi5f');
			   $data['app_ghi9']=$this->application_model->get_database_inform($id,'application_ghi9');
		       
			   $data['quote_id'] = $this->input->post('quote_id');
			   $data['app_val'] = $this->input->post('id_text_app_val');
			 
			
			    $data['broker_id'] = $this->application_model->get_broker_inform($id);
		 
				$result = $this->application_model->app_vehicle_db($id);	
				$this->edit_model->app_edit_ghi13($id);
				
				$data['filing_type_show'] = $this->filing_type_show();
				
				
			 
			}else{
			

            
			
			    $data['quote_id'] = $this->input->post('quote_id');
			    $data['broker_id'] = $this->application_model->get_broker_inform($id);
		        $data['list']=$this->application_model->get_data();
				$result = $this->application_model->app_vehicle_db($id);	
				$this->edit_model->app_edit_ghi13($id);
				$data['broker_id'] = $this->application_model->get_broker_quote($id);
		        $data['list']=$this->application_model->get_data();
		        $this->load->view('application_list',$data);
		
				
				}
              } 
	         } else{
					}
			
			
			}
		
		public function newMembers(){		
		$id = $this->input->post('ids');
		$data['broker_inform'] = $this->application_model->get_broker_inform_mem($id);
      
	echo 	json_encode($data);
	
	}
	public function app_form($id=""){		
	
    $request = $this->input->post('data');
	 	echo 	json_encode($request);
	$this->application_model->app_losses_db($id);
	$this->edit_model->app_edit_db($id);

	}
			public function applicant_scheduled($id){		
		$data['quote_id'] = $id;
		
		$this->load->view('edit_scheduled', $data);	
	}
	
	
	function filing_type_show() {
		$catlog ='';
		$catlog .='<select id="filing_type_otherABC123" class="span12 filing_type filing_type_add" required="required" name="filings_type[]"  onChange="filling_other1(this.value,this.id)" >';
		$catlog .='<option value="">Select</option>';
		$catlog_data = $this->quote_model->get_catlog_options(25);
		
		if($catlog_data)
		{
			foreach ($catlog_data as $catlogData)
				$catlog .='<option value="'.$catlogData->catlog_value.'">'.$catlogData->catlog_value.'</option>';	
		}
		$catlog .='<option value="Other">Other</option>';
		$catlog .='</select>';
			return $catlog;
    }
	

	
	public function getState(){
		$id = $_POST['id'];
	    $name = $_POST['name'];
	  	$attr = $_POST['attr'];
		$selected="";
		
	    getStateDropDown($id, $name, $attr='',$selected='');
	}
	public function application_pdf($id=''){
		
	  if($id){		 
		echo $mail_message = $this->get_form_pdf_data('edit', $id);

	  }
	}
	function get_form_pdf_data($edit='edit', $id=''){
		
	
	if($id){
	  $data['broker_inform'] = $this->application_model->get_broker_inform();
	  if($edit=='edit'){
		$data['app']=$this->application_model->get_data($id);
		$data['app_applicant']=$this->application_model->get_data_applicant($id);
		$data['app_tel_add']=$this->application_model->get_data_tel_add($id);
		$data['adresses']=$this->application_model->get_database_inform($id,'adresses');
		$data['app_email_add']=$this->application_model->get_data_email_add($id);
		$data['broker_information'] = $this->application_model->get_broker_information($id);
		
		
		//for losses section ashvin patel 4/nov/2014
		//$data['app_val'] = $this->input->post('id_text_app_val');			 
		$data_loss['quote_id'] = $this->input->post('quote_id');	
		$data_loss['app_loss']=$this->application_model->get_database_inform($id,'application_losses');
		$data_loss['app_loss1']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_liability');
		$data_loss['app_loss2']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_pd');
		$data_loss['app_loss3']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_cargo');
		$data_loss['app_loss4']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_bobtail');
		$data_loss['app_loss5']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_dead');
		$data_loss['app_loss6']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_trunk');
		$data_loss['app_loss7']=$this->application_model->get_database_inform1($id,'application_quote_losses','losses_excess');
		$data_loss['app_ghi8']=$this->application_model->get_database_inform($id,'application_ghi8');
		
		$data_loss['app_ghi1a']=$this->application_model->get_database_inform($id,'application_ghi1a');
		$data_loss['app_ghi1b']=$this->application_model->get_database_inform($id,'application_ghi1b');
		
		$data_loss['los_id']='value';
		if(!empty($data_loss['quote_id'])){
		 if(empty($data_loss['app_loss1'])){
		  $data_loss['app_loss1']=$this->application_model->get_database_quote1($data_loss['quote_id'],'rq_rqf_quote_losses','losses_liability');
		  $data_loss['upload_file']=$this->application_model->get_database_quote($data_loss['quote_id'],'rq_rqf_quote');
		 }
		 if(empty($data_loss['app_loss2'])){
		  $data_loss['app_loss2']=$this->application_model->get_database_quote1($data_loss['quote_id'],'rq_rqf_quote_losses','losses_pd');
		 }
		 if(empty($data_loss['app_loss3'])){
		  $data_loss['app_loss3']=$this->application_model->get_database_quote1($data_loss['quote_id'],'rq_rqf_quote_losses','losses_cargo');
		 }
		}
		$data['data_loss'] = $data_loss;
		//print_r($data_loss);
		//die;
		//for losses section end ashvin patel 4/nov/2014
		
		//for commodity section start ashvin patel 4/nov/2014
		 $data['app_com']=$this->application_model->get_database_inform($id,'application_commodity');
	  
		 $data['app_trans']=$this->application_model->get_database_inform($id,'application_transported');
		 $data['app_cert']=$this->application_model->get_database_inform($id,'application_certificate');
		 $data['app_add']=$this->application_model->get_database_inform($id,'application_additional');
		 $data['app_own']=$this->application_model->get_database_inform($id,'application_owner_operator');
		 $data['app_fil']=$this->application_model->get_database_inform($id,'application_filings');
		 
		
		 $data['edit_app']=$this->application_model->get_database_inform($id,'application_ghi11');


		   $data['app_ghi6']=$this->application_model->get_database_inform6($id,'application_ghi6',18);
                           $data['app_ghi61']=$this->application_model->get_database_inform6($id,'application_ghi6',20);
                           $data['app_ghi62']=$this->application_model->get_database_inform6($id,'application_ghi6',21);

						   
						 
		 $data['app_ghi5a']=$this->application_model->get_database_inform8($id,'application_ghi5a',24);


		 $data['app_ghi5b']=$this->application_model->get_database_inform8($id,'application_ghi5b',24);
		 $data['app_ghi5c']=$this->application_model->get_database_inform8($id,'application_ghi5c',24);
		 $data['app_ghi5d']=$this->application_model->get_database_inform8($id,'application_ghi5d',24);
		 $data['app_ghi5e']=$this->application_model->get_database_inform8($id,'application_ghi5e',24);
		 $data['app_ghi5f']=$this->application_model->get_database_inform8($id,'application_ghi5f',24);
		 
		 
		 $data['app_ghi51a']=$this->application_model->get_database_inform8($id,'application_ghi5a',23);


		 $data['app_ghi51b']=$this->application_model->get_database_inform8($id,'application_ghi5b',23);
		 $data['app_ghi51c']=$this->application_model->get_database_inform8($id,'application_ghi5c',23);
		 $data['app_ghi51d']=$this->application_model->get_database_inform8($id,'application_ghi5d',23);
		 $data['app_ghi51e']=$this->application_model->get_database_inform8($id,'application_ghi5e',23);
		 $data['app_ghi51f']=$this->application_model->get_database_inform8($id,'application_ghi5f',23);
		 
		 
		 
		 
		 $data['app_ghi9']=$this->application_model->get_database_inform($id,'application_ghi9');
		 
		 
	   
		 $data['quote_id'] = $this->input->post('quote_id');
		 $data['app_val'] = $this->input->post('id_text_app_val');
		 
		 $data['filing_type_show'] = $this->filing_type_show();
		 
	
			   $data['app_load']=$this->application_model->get_database_inform($id,'application_loaded');
			   $data['app_cov']=$this->application_model->get_database_inform($id,'application_coverage');
               $data['app_veh1']=$this->application_model->get_database_inform2($id,'application_vehicle','TRUCK');
			   $data['app_veh2']=$this->application_model->get_database_inform2($id,'application_vehicle','TRACTOR');
			  
	    	   $data['app_veh3']=$this->application_model->get_database_inform2($id,'application_vehicle','TRAILER');
			   $data['app_veh4']=$this->application_model->get_database_inform2($id,'application_vehicle','TRAILER_SEP');			  
			
			   $data['app_veh_pd']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRUCK');
			   $data['app_veh_pd1']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRACTOR');
			   $data['app_ghi13']=$this->application_model->get_database_inform($id,'application_ghi13');
		       $data['app_driver']=$this->application_model->get_database_inform($id,'application_driver');
			   
			
				if(!empty($data['quote_id'])){
				if(empty($data['app_veh1'])){
			   $data['app_veh12']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRUCK');
				}
				if(empty($data['app_veh2'])){
			   $data['app_veh22']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRACTOR');
				}
				if(empty($data['app_veh3'])){
	    	   $data['app_veh32']=$this->application_model->get_database_quote2($data['quote_id'],'rq_rqf_quote_vehicle','TRAILER');
				}
				}
		 if(!empty($data['quote_id'])){
			 if(empty($data['app_fil'])){
			  $data['quote_filings']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote_filings');
		   }
		 }
		//for commodity section end ashvin patel 4/nov/2014	  
			   
	  }
	  if($edit=='app'){
		$data['Quote_num']=$id;	
		$rq_rqf_quote=$this->application_model->get_database_quote($id,'rq_rqf_quote');
		$data['broker_inform1'] = $this->application_model->get_broker_inform($rq_rqf_quote[0]->email);
		$data['rq_rqf_quote_losses']=$this->application_model->get_database_quote($id,'rq_rqf_quote_losses');
		$data['broker_insured'] = $this->application_model->get_broker_insured($id);
		$data['app']=$this->application_model->get_database_quote($id,'application_general');
		$data['app_applicant']=$this->application_model->get_database_quote($id,'application_applicant');
		$data['app_tel_add']=$this->application_model->get_database_quote($id,'application_tel_add');
		$data['adresses']=$this->application_model->get_database_quote($id,'adresses');
		$data['app_email_add']=$this->application_model->get_database_quote($id,'application_email_add');
		$data['broker_information'] = $this->application_model->get_database_quote($id,'app_broker_inform');
		
		$data['app_loss1']=$this->application_model->get_database_quote1($id,'rq_rqf_quote_losses','losses_liability');
		$data['app_loss2']=$this->application_model->get_database_quote1($id,'rq_rqf_quote_losses','losses_pd');
		$data['app_loss3']=$this->application_model->get_database_quote1($id,'rq_rqf_quote_losses','losses_cargo');
		$data['upload_file']=$this->application_model->get_database_quote($id,'rq_rqf_quote');
		
		//	if(!empty($data['quote_id'])){
				
			   $data['app_veh12']=$this->application_model->get_database_quote2($id,'rq_rqf_quote_vehicle','TRUCK');
			   $data['app_veh22']=$this->application_model->get_database_quote2($id,'rq_rqf_quote_vehicle','TRACTOR');
	    	   $data['app_veh32']=$this->application_model->get_database_quote2($id,'rq_rqf_quote_vehicle','TRAILER');
			   
			 //  $data['app_driver1']=$this->application_model->get_database_quote($data['quote_id'],'rq_rqf_quote_drivers');
			  
		//	}
		
	  }
	 //$this->load->view('application_applicant',$data);
	}else{
	 $data['broker_inform1'] = $this->application_model->get_broker_inform();	
	 $data['quote_id'] = $this->input->post('quote_id');
	 $data['broker_inform'] = $this->application_model->get_broker_inform();
	}
	 $data['filing_type_show'] = $this->filing_type_show();
	 $data['show_hed'] = 1;
	 $pdf_data = $this->load->view('application_applicant',$data, true);
	//$this->load->view('application_losses',$data);
	return $pdf_data;
  	
	}
	
	public function Get_ids(){		
		$sql = "SELECT general_id,underwriter_eamil FROM application_general where flag_pdf != 1 ORDER BY general_id ASC";
        $stmt = $this->db->query($sql);
		$users =$stmt->result();
		if(isset($users)){
		echo json_encode($users);
		}
	} 
    public function flag_set(){
        $table="application_general";
		$pdf_name = $this->input->post('pdf_name');
        $id = $this->input->post('id');
        $data= array(
            "flag_pdf" => 1,
			"pdf_name" => $pdf_name,
	        );
        $this->db->where('general_id', $id);
        if($this->db->update($table, $data) !== FALSE){
            return TRUE;
        }		
    }

}




/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
