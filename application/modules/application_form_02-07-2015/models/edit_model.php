<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class edit_model extends CI_Model {




    public function &__get($key) {
        $CI = & get_instance();
        return $CI->$key;
        $this->load->database();
		$this->load->model('application_model');
    }
	
	
	public function application_ghi1a($id=""){
	$insert_id=$this->application_model->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	
	$mail_id = $this->input->post('mail_id');
	

	$data_mail['quote_id'] =  $quote_id;	
	$data_mail['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_mail['mail_address'] = $this->input->post('mail_address');
	$data_mail['mail_city'] = $this->input->post('mail_city');
	$data_mail['mail_country'] = $this->input->post('mail_country');
	$data_mail['mail_state'] = $this->input->post('mail_state');
	$data_mail['mail_zip'] = $this->input->post('mail_zip');
	$data_mail['contact_fname'] = $this->input->post('contact_fname');
	$data_mail['contact_mname'] = $this->input->post('contact_mname');
	
	$data_mail['contact_lname'] = $this->input->post('contact_lname');
	$data_mail['contact_tel'] = $this->input->post('contact_tel');
	$data_mail['contact_cell'] = $this->input->post('contact_cell');
	
	$data_mail['contact_fax'] = $this->input->post('contact_fax');
	$data_mail['contact_email'] = $this->input->post('contact_email');

		if($mail_id!=''){
					$this->application_model->_updateapp($id, $data_mail,'application_ghi1a');
				
					}else{
					
					$this->db->insert('application_ghi1a', $data_mail);
					
				}
	
	
	
	
	}
	
public function application_ghi1b($id=""){
	$insert_id=$this->application_model->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	$garage_id = $this->input->post('garage_id');
	$garage_location = $this->input->post('garage_location');
	$garage_city = $this->input->post('garage_city');
	$garage_country = $this->input->post('garage_country');
	$garage_state = $this->input->post('garage_state');
	$garage_zip = $this->input->post('garage_zip');
	$contacts_fname = $this->input->post('contacts_fname');
	
	$contacts_mname = $this->input->post('contacts_mname');
	$contacts_lname = $this->input->post('contacts_lname');
	
	$contacts_tel = $this->input->post('contacts_tel');
	$contacts_cell = $this->input->post('contacts_cell');
	$contacts_fax = $this->input->post('contacts_fax');
	$contacts_email = $this->input->post('contacts_email');
	
	$a1=1;
    $garage_old_id=array();
	$garage_new_id=array();
      for ($i = 0; $i < count($garage_location); $i++) 
				{
					if ($garage_location[$i] != '') 
					 { 
	$contacts_checkbox1 = $this->input->post($a1.'contacts_checkbox');
	$contacts_checkbox=!empty($contacts_checkbox1)? implode(',',$contacts_checkbox1):'';	
		
	$data_garage['quote_id'] =  $quote_id;	
	$data_garage['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_garage['garage_location'] = isset($garage_location[$i])? $garage_location[$i]:'';
	$data_garage['garage_city'] = isset($garage_city[$i]) ? $garage_city[$i]:'';
	$data_garage['garage_country'] = isset($garage_country[$i]) ? $garage_country[$i]:'';
	$data_garage['garage_state'] = isset($garage_state[$i]) ? $garage_state[$i]:'';
	$data_garage['garage_zip'] = isset($garage_zip[$i]) ? $garage_zip[$i]:'';
	$data_garage['contacts_fname'] = isset($contacts_fname[$i]) ? $contacts_fname[$i]:'';	
	$data_garage['contacts_mname'] = isset($contacts_mname[$i]) ? $contacts_mname[$i]:'';	
	$data_garage['contacts_lname'] = isset($contacts_lname[$i]) ? $contacts_lname[$i]:'';
	$data_garage['contacts_tel'] = isset($contacts_tel[$i]) ? $contacts_tel[$i]:'';
	$data_garage['contacts_cell'] = isset($contacts_cell[$i]) ? $contacts_cell[$i]:'';
	$data_garage['contacts_fax'] = isset($contacts_fax[$i]) ? $contacts_fax[$i]:'';
	$data_garage['contacts_email'] = isset($contacts_email[$i]) ? $contacts_email[$i]:'';
	$data_garage['contacts_checkbox'] = isset($contacts_checkbox) ? $contacts_checkbox:'';

						
						
						if(!empty($garage_id[$i])){
					$this->application_model->_updateapp1($id, $data_garage,'application_ghi1b',$garage_id[$i],'garage_id');
					$garage_old_id[]=$garage_id[$i];
					}else{
				
					$this->db->insert('application_ghi1b', $data_garage);
					$garage_new_id[]=$this->db->insert_id();
				}
						
					$a1++;	
						
					}
					
				}
		$this->is_delete($garage_old_id,$garage_new_id,'application_ghi1b','garage_id',$id);

	
	}
	
	
	
	public function app_edit_db($id=""){
	$insert_id=$this->application_model->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	
	$app_edit_id = $this->input->post('app_edit_id');
	

	$zip=$this->input->post('applicant_zip1').'-'.$this->input->post('applicant_zip2');
	$data_applicant['quote_id'] =  $quote_id;	
	$data_applicant['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_applicant['applicant_fname'] = $this->input->post('applicant_fname');
	$data_applicant['applicant_mname'] = $this->input->post('applicant_mname');
	$data_applicant['applicant_lname'] = $this->input->post('applicant_lname');
	$data_applicant['applicant_dba'] = $this->input->post('applicant_dba');
	$data_applicant['applicant_address'] = $this->input->post('applicant_address');
	$data_applicant['applicant_city'] = $this->input->post('applicant_city');
	$data_applicant['applicant_country'] = $this->input->post('applicant_country');
	
	$data_applicant['applicant_state'] = $this->input->post('applicant_state');
	$data_applicant['applicant_zip'] = $zip ;
	$data_applicant['applicant_from'] = $this->input->post('applicant_from');
	$data_applicant['applicant_to'] = $this->input->post('applicant_to');
	$data_applicant['applicant_ca'] = $this->input->post('applicant_ca');
	
	$data_applicant['applicant_ma'] = $this->input->post('applicant_ma');
	$data_applicant['applicant_dot'] = $this->input->post('applicant_dot');
	$data_applicant['applicant_entity'] = $this->input->post('applicant_entity');
	$data_applicant['applicant_role'] = $this->input->post('applicant_role');
	$data_applicant['applicant_explain'] = $this->input->post('applicant_explain');
	
	
		if($app_edit_id!=''){
					$this->application_model->_updateapp($id, $data_applicant,'application_ghi8');
					
					}else{
					
					$this->db->insert('application_ghi8', $data_applicant);
					
				}
	
	
	
	
	}
	
	
	public function app_edit_ghi6($id=""){
	
	$insert_id=$this->application_model->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	
	$supplement_id = $this->input->post('supplement_id');
	$supplement_dba= $this->input->post('supplement_dba');
	$supplement_fname = $this->input->post('supplement_fname');
	$supplement_mname = $this->input->post('supplement_mname');
	$supplement_lname = $this->input->post('supplement_lname');
	$supplement_dfrom = $this->input->post('supplement_dfrom');
	$supplement_dto = $this->input->post('supplement_dto');
	

	$supplement_lnum = $this->input->post('supplement_lnum');
	$supplement_Reason = $this->input->post('supplement_Reason');
    $supplement_parent_id = $this->input->post('supplement_parent_id');
	$supplement_old_id=array();
	$supplement_new_id=array();
	
	

      for ($i = 0; $i < count($supplement_fname); $i++) 
				{
		if ($supplement_fname[$i] != '') 
					{ 
					
	$data_supplement['quote_id'] =  $quote_id;	
	$data_supplement['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_supplement['supplement_fname'] = $supplement_fname[$i];
	$data_supplement['supplement_mname'] = $supplement_mname[$i];
	$data_supplement['supplement_lname'] = $supplement_lname[$i];
	$data_supplement['supplement_dfrom'] = $supplement_dfrom[$i];
	$data_supplement['supplement_dto'] = $supplement_dto[$i];
	$data_supplement['supplement_lnum'] = $supplement_lnum[$i];
	$data_supplement['supplement_Reason'] = $supplement_Reason[$i];
    $data_supplement['supplement_parent_id'] = $supplement_parent_id[$i];
						
				
					if(!empty($supplement_id[$i])){
					$this->application_model->_updateapp1($id, $data_supplement,'application_ghi6',$supplement_id[$i],'supplement_id');
					$supplement_old_id[]=$supplement_id[$i];
					}else{
					
					$this->db->insert('application_ghi6', $data_supplement);
					
					$supplement_new_id[]=$this->db->insert_id();
				}
					
						
					}
					
				}
				
	$data_supplement1['supplement_dba'] = isset($supplement_dba) ? implode(',',$supplement_dba):'';

	$this->db->where('general_id',$id);
	$this->db->update('application_general',$data_supplement1);
	if(!empty($supplement_old_id)){
	 $result=array();
	 $adresses=$this->application_model->get_id_inform($id,'application_ghi6');
	   foreach($adresses as $values){
	
		   $result[]=$values->supplement_id;
		
	   }
	$flag_set= array_diff($result,$supplement_old_id,$supplement_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->application_model->_updateapp1($id, $insert_data130,'application_ghi6',$vlaues,'supplement_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->application_model->get_id_inform($id,'application_ghi6');
	   foreach($array as $values){
	  
		   $result[]=$values->supplement_id;
		
	   }
	
	$flag_set= array_diff($result,$supplement_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->application_model->_updateapp1($id, $insert_data130,'application_ghi6',$vlaues,'supplement_id');
	
	     }
	    }
	  
	  }
	
	
	}	
				
				
				
	}
	
	public function app_edit_ghi9($id=""){
	$insert_id=$this->application_model->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	
	$reg_id = $this->input->post('reg_id');
	$reg_fname = $this->input->post('reg_fname');
	$reg_mname = $this->input->post('reg_mname');
	$reg_lname = $this->input->post('reg_lname');
	$reg_address = $this->input->post('reg_address');
	$reg_city = $this->input->post('reg_city');
	
	$reg_country = $this->input->post('reg_country');
	$reg_state = $this->input->post('reg_state');
	
	$reg_zip = $this->input->post('reg_zip');
	$reg_zip1 = $this->input->post('reg_zip1');
	$reg_lnum = $this->input->post('reg_lnum');
	$reg_ssn = $this->input->post('reg_ssn');
	
	$MVR_file = $this->input->post('MVR_file');
	$reg_explain = $this->input->post('reg_explain');
	 
    $reg_r1 = $this->input->post('reg_r0');

	$reg_old_id=array();
	$reg_new_id=array();	        
      for ($i = 0; $i < count($reg_fname); $i++) 
				{
					if ($reg_fname[$i] != '') 
					{ 
		
	$data_reg['quote_id'] =  $quote_id;	
	$data_reg['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_reg['reg_fname'] = $reg_fname[$i];
	$data_reg['reg_mname'] = $reg_mname[$i];
	$data_reg['reg_lname'] = $reg_lname[$i];
	$data_reg['reg_address'] = $reg_address[$i];
	$data_reg['reg_city'] = $reg_city[$i];
	$data_reg['reg_country'] = $reg_country[$i];	
	$data_reg['reg_state'] = $reg_state[$i];	
	$data_reg['reg_zip'] = $reg_zip[$i];
	$data_reg['reg_zip1'] = isset($reg_zip1[$i])?$reg_zip1[$i]:'';
	$data_reg['reg_lnum'] = $reg_lnum[$i];
	$data_reg['reg_ssn'] = $reg_ssn[$i];
	$data_reg['reg_r1'] = $reg_r1[$i];;
	$data_reg['MVR_file'] = $MVR_file[$i];
	$data_reg['reg_explain'] = $reg_explain[$i];
						
						
						if(!empty($reg_id[$i])){
					$this->application_model->_updateapp1($id, $data_reg,'application_ghi9',$reg_id[$i],'reg_id');
					$reg_old_id[]=$reg_id[$i];
					}else{
					
					$this->db->insert('application_ghi9', $data_reg);
				    $reg_new_id[]=$this->db->insert_id();
				}
						
						
						
					}
					
				}

				
				$this->is_delete($reg_old_id,$reg_new_id,'application_ghi9','reg_id',$id);
	
	
	
	
	
	}
	
	
	public function app_edit_ghi11($id=""){
	$insert_id=$this->application_model->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	$vehicles_id = $this->input->post('vehicles_id');

	$vehicles_VIN = $this->input->post('vehicles_VIN');
	$vehicles_GVW = $this->input->post('vehicles_GVW');
	$vehicles_Year = $this->input->post('vehicles_Year');
	$vehicles_make = $this->input->post('vehicles_make');
	$vehicles_body = $this->input->post('vehicles_body');
	$vehicles_model = $this->input->post('vehicles_model');
	$vehicles_reason = $this->input->post('vehicles_reason');
	
	$vehicles_old_id=array();
	$vehicles_new_id=array();
      for ($i = 0; $i < count($vehicles_VIN); $i++) 
				{
					if ($vehicles_VIN[$i] != '') 
					{ 
					
	$data_vehicles['quote_id'] =  $quote_id;	
	$data_vehicles['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_vehicles['vehicles_VIN'] = $vehicles_VIN[$i];
	$data_vehicles['vehicles_GVW'] = $vehicles_GVW[$i];
	$data_vehicles['vehicles_Year'] = $vehicles_Year[$i];
	$data_vehicles['vehicles_make'] = $vehicles_make[$i];
	$data_vehicles['vehicles_body'] = $vehicles_body[$i];
	$data_vehicles['vehicles_model'] = $vehicles_model[$i];	
	$data_vehicles['vehicles_reason'] = $vehicles_reason[$i];	

								if(!empty($vehicles_id[$i])){
					$this->application_model->_updateapp1($id, $data_vehicles,'application_ghi11',$vehicles_id[$i],'vehicles_id');
					$vehicles_old_id[]=$vehicles_id[$i];
					}else{
			
					$this->db->insert('application_ghi11', $data_vehicles);
				    $vehicles_new_id[]=$this->db->insert_id();
				         }
					
						
					}
					
				}

	$this->is_delete($vehicles_old_id,$vehicles_new_id,'application_ghi11','vehicles_id',$id);
	
	
	
	
	}
	
	
public function app_edit_ghi13($id=""){
	$carriers_tele=array();
	$retail_tele=array();
	$insert_id=$this->application_model->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	
	$carriers_id = $this->input->post('carriers_id');
	
	$carriers_name = $this->input->post('carriers_name');
	$carriers_tel = $this->input->post('carriers_tel');
	$carriers_ga = $this->input->post('carriers_ga');

	

	
	//$carriers_tele[] =	isset($carriers_tele1)? implode("-", $carriers_tele1) : '';
	$retail_broker = $this->input->post('retail_broker');
	
	//$retail_tele[] = isset($retail_tele1)? implode("-", $retail_tele1) : '';
	
	$retail_reason = $this->input->post('retail_reason');

    $carriers_old_id=array();
	$carriers_new_id=array();
	$b=1;
      for ($i = 0; $i < count($carriers_name);$i++) 
				{
					if ($carriers_name[$i] != '') 
					{
						
	$data_carriers['quote_id'] =  $quote_id;	
	$data_carriers['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_carriers['carriers_name'] = $carriers_name[$i];
	$data_carriers['carriers_tel'] = $carriers_tel[$i];
	$data_carriers['carriers_ga'] = $carriers_ga[$i];
	$carriers_tele=$this->input->post($b.'carriers_telephone');
	$data_carriers['carriers_telephone'] = isset($carriers_tele)? implode("-", $carriers_tele) : '';
	$data_carriers['retail_broker'] = $retail_broker[$i];
	$retail_tele=$this->input->post($b.'retail_telephone');
	$data_carriers['retail_telephone'] = isset($retail_tele)? implode("-", $retail_tele) : '';
	$data_carriers['retail_reason'] = $retail_reason[$i];	
		if(!empty($carriers_id[$i])){
					$this->application_model->_updateapp1($id, $data_carriers,'application_ghi13',$carriers_id[$i],'carriers_id');
					
					$carriers_old_id[]=$carriers_id[$i];
					}else{
					
					$this->db->insert('application_ghi13', $data_carriers);
					$carriers_new_id[]=$this->db->insert_id();
				}
						
					$b++;	
						
					}
					
				}

	        $this->is_delete($carriers_old_id,$carriers_new_id,'application_ghi13','carriers_id',$id);
	
	
	
	
	}
	
	
	
		
	public function app_edit_ghi51($id=""){		
	$insert_id=$this->application_model->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');		
	
	$hauled_n_id1 = $this->input->post('hauled_n_id1');
	$ghi5_parent_id1=$this->input->post('ghi5_parent_id1');
	
	$data_hauler1['quote_id'] =  $quote_id;	
	$data_hauler1['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_hauler1['hauler_MVR'] = $this->input->post('hauler_MVR1');
	$data_hauler1['hauler_Roa'] = $this->input->post('hauler_Roa1');
	$data_hauler1['hauler_Wri'] = $this->input->post('hauler_Wri1');
	$data_hauler1['hauler_Phy'] = $this->input->post('hauler_Phy1');
	$data_hauler1['hauler_Drug'] = $this->input->post('hauler_Drug1');
	$data_hauler1['hauler_Ref'] = $this->input->post('hauler_Ref1');
	$data_hauler1['hauler_Ver'] = $this->input->post('hauler_Ver1');
	
	$data_hauler1['ghi5_parent_id'] = $this->input->post('ghi5_parent_id1');
	
	$data_hauler1['hauler_hir_name'] = $this->input->post('hauler_hir_name1');
	$data_hauler1['hauler_Own'] = $this->input->post('hauler_Own1');
	$data_hauler1['hauler_Rev'] = $this->input->post('hauler_Rev1');
	$data_hauler1['hauler_team'] = $this->input->post('hauler_team1');
	$data_hauler1['hauler_Num'] = $this->input->post('hauler_Num1');
	$data_hauler1['hauler_Num_y'] = $this->input->post('hauler_Num_y1');
	$data_hauler1['hauler_Vehi'] = $this->input->post('hauler_Vehi1');
	
	
	$data_hauler1['hauler_often'] = $this->input->post('hauler_often1');
	$data_hauler1['hauler_attach'] = $this->input->post('hauler_attach1');
	$data_hauler1['hauler_drivers'] = $this->input->post('hauler_drivers1');
	$data_hauler1['hauler_ins'] = $this->input->post('hauler_ins1');
	$data_hauler1['hauler_Pol'] = $this->input->post('hauler_Pol1');
	$data_hauler1['hauler_Effect']= $this->input->post('hauler_Effect1');
	$data_hauler1['hauler_Exp'] = $this->input->post('hauler_Exp1');
	
	
	
	$data_hauler1['hauler_Hir'] = $this->input->post('hauler_Hir1');
	$data_hauler1['hauler_Ter'] = $this->input->post('hauler_Ter1');
	$data_hauler1['hauler_Quit'] = $this->input->post('hauler_Quit1');
	$data_hauler1['hauler_Oth'] = $this->input->post('hauler_Oth1');
	$data_hauler1['hauler_Max'] = $this->input->post('hauler_Max1');
	$data_hauler1['hauler_Per']= $this->input->post('hauler_Per1');
	$data_hauler1['hauler_day'] = $this->input->post('hauler_day1');
	
	
	$data_hauler1['hauler_comp'] = $this->input->post('hauler_comp1');
	$data_hauler1['hauler_Others'] = $this->input->post('hauler_Others1');
	$data_hauler1['hauler_hours'] = $this->input->post('hauler_hours1');
	$data_hauler1['hauler_hour'] = $this->input->post('hauler_hour1');
	$data_hauler1['hauler_hou'] = $this->input->post('hauler_hou1');
	$data_hauler1['hauler_sleep']= $this->input->post('hauler_sleep1');
	//$data_hauler['hauler_Motel'] = $this->input->post('hauler_Motel');
	
	$data_hauler1['hauler_hother'] = $this->input->post('hauler_hother1');	
	$data_hauler1['hauler_rest'] = $this->input->post('hauler_rest1');
	$data_hauler1['hauler_roth']= $this->input->post('hauler_roth1');
	$data_hauler1['hauler_haul'] = $this->input->post('hauler_haul1');
	
	$data_hauler1['haul_name'] = $this->input->post('haul_name1');
	$data_hauler1['haul_agree'] = $this->input->post('haul_agree1');
	$data_hauler1['haul_attach'] = $this->input->post('haul_attach1');
	$data_hauler1['haul_Last'] = $this->input->post('haul_Last1');
	$data_hauler1['haul_Next'] = $this->input->post('haul_Next1');
	$data_hauler1['haul_attach_file'] = $this->input->post('haul_attach_file1');

	
	
	$data_hauler1['haul_equi'] = $this->input->post('haul_equi1');
	$data_hauler1['haul_rent'] = $this->input->post('haul_rent1');
	$data_hauler1['haul_lease'] = $this->input->post('haul_lease1');
	$data_hauler1['haul_agreement'] = $this->input->post('haul_agreement1');
	$data_hauler1['haul_agree_file'] = $this->input->post('haul_agree_file1');
	$data_hauler1['haul_month']= $this->input->post('haul_month1');
	$data_hauler1['haul_year'] = $this->input->post('haul_year1');
	$data_hauler1['haul_bor']= $this->input->post('haul_bor1');
	$data_hauler1['haul_ope'] = $this->input->post('haul_ope1');
	
	
	$data_hauler1['haul_yagree'] = $this->input->post('haul_yagree1');
	$data_hauler1['haul_yagree_file'] = $this->input->post('haul_yagree_file1');
	$data_hauler1['haul_yins'] = $this->input->post('haul_yins1');
	
	
	
	$data_hauler1['subcon_y'] = $this->input->post('subcon_y1');
	$data_hauler1['subcon_name'] = $this->input->post('subcon_name1');
	$data_hauler1['subcon_cert'] = $this->input->post('subcon_cert1');
	$data_hauler1['subcon_lia'] = $this->input->post('subcon_lia1');
	$data_hauler1['subcon_duties'] = $this->input->post('subcon_duties1');
	$data_hauler1['subcon_cost'] = $this->input->post('subcon_cost1');
	$data_hauler1['subcon_emp_y'] = $this->input->post('subcon_emp_y1');
	
	
	$data_hauler1['sub_haul_y'] = $this->input->post('sub_haul_y1');
	$data_hauler1['audit_y'] = $this->input->post('audit_y1');
	$data_hauler1['vehicles_y'] = $this->input->post('vehicles_y1');
	$data_hauler1['ride_y'] = $this->input->post('ride_y1');
	$data_hauler1['driver_y'] = $this->input->post('driver_y1');
	$data_hauler1['activity_y'] = $this->input->post('activity_y1');
	
	$data_hauler1['prior_y'] = $this->input->post('prior_y1');
	$data_hauler1['procedure_y'] = $this->input->post('procedure_y1');
	$data_hauler1['Drug_y'] = $this->input->post('Drug_y1');
	$data_hauler1['hiring_file'] = $this->input->post('hiring_file1');
	$data_hauler1['employees_y'] = $this->input->post('employees_y1');
	$data_hauler1['explain_no'] = $this->input->post('explain_no1');
	
	$data_hauler1['safety_y'] = $this->input->post('safety_y1');
	$data_hauler1['details_y'] = $this->input->post('details_y1');
	$data_hauler1['program_detail'] = $this->input->post('program_detail1');
	$data_hauler1['load_y'] = $this->input->post('load_y1');
	$data_hauler1['ins_pol'] = $this->input->post('ins_pol1');
	$data_hauler1['safety_attach_file'] = $this->input->post('safety_attach_file1');
//echo '----------------------------------------------';

			
				
			  if($hauled_n_id1!=''){
					  
				
					$this->application_model->_updateapp1($id,$data_hauler1,'application_ghi5a',$hauled_n_id1,'haul_id');
				  }else{
					  $this->db->insert('application_ghi5a',$data_hauler1);
					  $insert_h_id1=$this->db->insert_id();
					  }
				
					
	   
	  
	   
	   
	$haul_id= $this->input->post('haul_ins_id');   
	$add_ins_id1= $this->input->post('add_ins_id1'); 
	   
	$add_ins_name1= $this->input->post('add_ins_name1');
	$add_ins_carrier1= $this->input->post('add_ins_carrier1');
	$ins_attach_y1= $this->input->post('ins_attach_y1');
	$add_ins_attach1 = $this->input->post('add_ins_attach1');
	$add_ins_old_id=array();
	$add_ins_new_id=array();
	
		
      for ($i = 0; $i < count($add_ins_name1); $i++) 
				{
					//if ($add_ins_name1[$i] != '') 
					//{ 
					
	$data_insert121['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	$data_insert121['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : '' ;
    $data_insert121['quote_id'] =  $quote_id;	
	$data_insert121['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_insert121['add_ins_name'] = !empty($add_ins_name1[$i]) ? $add_ins_name1[$i] :'';
	$data_insert121['add_ins_carrier'] = !empty($add_ins_carrier1[$i]) ? $add_ins_carrier1[$i]:'';
	$data_insert121['ins_attach_y'] = !empty($ins_attach_y1[$i]) ? $ins_attach_y1[$i]:'';
	$data_insert121['add_ins_attach'] = !empty($add_ins_attach1[$i]) ? $add_ins_attach1[$i] :'';
	
				
					if(!empty($add_ins_id1[$i])){
					$this->application_model->_updateapp1($id, $data_insert121,'application_ghi5b',$add_ins_id1[$i],'add_ins_id');
					$add_ins_old_id[]=$add_ins_id1[$i];
					}else{
					
					$this->db->insert('application_ghi5b', $data_insert121);
					$add_ins_new_id[]=$this->db->insert_id();
				         }
					
						
						
					//}
					
				}
				
	$this->is_delete($add_ins_old_id,$add_ins_new_id,'application_ghi5b','add_ins_id',$id,$ghi5_parent_id1);	
	
	
	$add_owner_id1= $this->input->post('add_owner_id1'); 
				   
	$add_owner_name1= $this->input->post('add_owner_name1');
	$add_owner_y1= $this->input->post('add_owner_y1');
	$add_owner_attach1= $this->input->post('add_owner_attach1');
	$add_owner_old_id=array();
	$add_owner_new_id=array();
		
      for ($j = 0; $j < count($add_owner_name1); $j++) 
				{
				//	if ($add_owner_name1[$j] != '') 
				//	{ 
	$data_insert131['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	$data_insert131['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : '' ;
    $data_insert131['quote_id'] =  $quote_id;	
	$data_insert131['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_insert131['add_owner_name'] = !empty($add_owner_name1[$j]) ? $add_owner_name1[$j]:'';
	$data_insert131['add_owner_y'] = !empty($add_owner_y1[$j]) ? $add_owner_y1[$j]:'';
	$data_insert131['add_owner_attach'] = !empty($add_owner_attach1[$j]) ? $add_owner_attach1[$j]:'';
	
	
			if(!empty($add_owner_id1[$j])){
					$this->application_model->_updateapp1($id, $data_insert131,'application_ghi5c',$add_owner_id1[$j],'add_owner_id');
					$add_owner_old_id[]=$add_owner_id1[$j];
					}else{
					
					$this->db->insert('application_ghi5c', $data_insert131);
					$add_owner_new_id[]=$this->db->insert_id();
				}
	     //	}
					
	    }
				
	$this->is_delete($add_owner_old_id,$add_owner_new_id,'application_ghi5c','add_owner_id',$id,$ghi5_parent_id1);
		
	
	$subcon_add_id1= $this->input->post('subcon_add_id1'); 
				   
	$subcon_add_name1= $this->input->post('subcon_add_name1');
	$subcon_add_vehicle1= $this->input->post('subcon_add_vehicle1');
	$subcon_add_y1= $this->input->post('subcon_add_y1');
	$subcon_add_attach1 = $this->input->post('subcon_add_attach1');
	$subcon_add_old_id=array();	
	$subcon_add_new_id=array();	
	
      for ($k = 0; $k < count($subcon_add_name1); $k++) 
				{
					//if ($subcon_add_name1[$k] != '') 
					//{ 
	$data_insert171['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	$data_insert171['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : '' ;	
    $data_insert171['quote_id'] =  $quote_id;	
	$data_insert171['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert171['subcon_add_name'] = !empty($subcon_add_name1[$k]) ? $subcon_add_name1[$k]:'';
	$data_insert171['subcon_add_vehicle'] = !empty($subcon_add_vehicle1[$k])?$subcon_add_vehicle1[$k] :'';
	$data_insert171['subcon_add_y'] = !empty($subcon_add_y1[$k])? $subcon_add_y1[$k]:'';
	$data_insert171['subcon_add_attach'] = !empty($subcon_add_attach1[$k]) ? $subcon_add_attach1[$k]:'';
	
						
					
			
				if(!empty($subcon_add_id1[$k])){
					$this->application_model->_updateapp1($id, $data_insert171,'application_ghi5d',$subcon_add_id1[$k],'subcon_add_id');
					$subcon_add_old_id[]=$subcon_add_id1[$k];
					}else{
					
					$this->db->insert('application_ghi5d', $data_insert171);
				    $subcon_add_new_id[]=$this->db->insert_id(); 
				      }
						
					//}
					
				}
	$this->is_delete($subcon_add_old_id,$subcon_add_new_id,'application_ghi5d','subcon_add_id',$id,$ghi5_parent_id1);		
	
	

	$sub_haul_id1= $this->input->post('sub_haul_id1');

	$sub_haul_add_name1= $this->input->post('sub_haul_add_name1');
	$sub_haul_add_carrier1= $this->input->post('sub_haul_add_carrier1');
	$sub_haul_add_limit1= $this->input->post('sub_haul_add_limit1');
	$sub_haul_add_y1 = $this->input->post('sub_haul_add_y1');
	$sub_haul_add_attach1= $this->input->post('sub_haul_add_attach1');
	$sub_haul_old_id=array();	
	$sub_haul_new_id=array();	
	
      for ($l = 0; $l < count($sub_haul_add_name1); $l++) 
				{
					//if ($sub_haul_add_name1[$l] != '') 
					//{ 
		$data_insert141['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	 $data_insert141['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : '' ;			
    $data_insert141['quote_id'] =  $quote_id;	
	$data_insert141['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert141['sub_haul_add_name'] = !empty($sub_haul_add_name1[$l]) ? $sub_haul_add_name1[$l]:'' ;
	$data_insert141['sub_haul_add_carrier'] = !empty($sub_haul_add_carrier1[$l]) ? $sub_haul_add_carrier1[$l] : '';
	$data_insert141['sub_haul_add_limit'] = !empty($sub_haul_add_limit1[$l]) ? $sub_haul_add_limit1[$l] :'' ;
	$data_insert141['sub_haul_add_y'] = !empty($sub_haul_add_y1[$l])? $sub_haul_add_y1[$l]:'';
	$data_insert141['sub_haul_add_attach'] = !empty($sub_haul_add_attach1[$l]) ? $sub_haul_add_attach1[$l]:'';
	
				
						
							if(!empty($sub_haul_id1[$l])){
				    	$this->application_model->_updateapp1($id, $data_insert141,'application_ghi5e',$sub_haul_id1[$l],'sub_haul_id');
						$sub_haul_old_id[]=$sub_haul_id1[$l];	
	
					}else{
				
					$this->db->insert('application_ghi5e', $data_insert141);
				     	
	                   $sub_haul_new_id[]=$this->db->insert_id();
				}
						
					}
					
				//}
				
	$this->is_delete($sub_haul_old_id,$sub_haul_new_id,'application_ghi5e','sub_haul_id',$id,$ghi5_parent_id1);			

	
	
	$add_mem_id1= $this->input->post('add_mem_id1');
				
	$add_mem_name1= $this->input->post('add_mem_name1');
	$add_mem_rel1= $this->input->post('add_mem_rel1');

	$add_mem_old_id=array();
	$add_mem_new_id=array();	
      for ($m = 0; $m < count($add_mem_name1); $m++) 
				{
					//if ($add_mem_name1[$m] != '') 
					//{ 
	$data_insert151['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	$data_insert151['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : '' ;			
    $data_insert151['quote_id'] =  $quote_id;	
	$data_insert151['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert151['add_mem_name'] = !empty($add_mem_name1[$m])? $add_mem_name1[$m] :'';
	$data_insert151['add_mem_rel'] = !empty($add_mem_rel1[$m]) ? $add_mem_rel1[$m]:'';

	
	
							if(!empty($add_mem_id1[$m])){
					$this->application_model->_updateapp1($id, $data_insert151,'application_ghi5f',$add_mem_id1[$m],'add_mem_id');
					$add_mem_old_id[]=$add_mem_id1[$m];
					}else{
					
					$this->db->insert('application_ghi5f', $data_insert151);
					$add_mem_new_id[]=$this->db->insert_id();
				//}
						
						
					}
					
				}
				
	$this->is_delete($add_mem_old_id,$add_mem_new_id,'application_ghi5f','add_mem_id',$id,$ghi5_parent_id1);
	
	
	$add_list_id1= $this->input->post('add_list_id1'); 
    $hauled_n_id1= $this->input->post('hauled_n_id1'); 				
	$add_list_name1= $this->input->post('add_list_name1');
	$add_list_length1= $this->input->post('add_list_length1');
	$add_list_year1= $this->input->post('add_list_year1');
	$add_list_old_id=array();
	$add_list_new_id=array();	
		
      for ($n = 0; $n < count($add_list_name1); $n++) 
				{
					//if ($add_list_name1[$n] != '') 
					//{ 
    $data_insert161['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	$data_insert161['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : '' ;				
		
    $data_insert161['quote_id'] =  $quote_id;	
	$data_insert161['ref_id'] = !empty($id) ? $id : $ref_id;		
	$data_insert161['add_list_name'] = !empty($add_list_name1[$n]) ? $add_list_name1[$n]:'';
	$data_insert161['add_list_length'] = !empty($add_list_length1[$n]) ? $add_list_length1[$n]:'';
	$data_insert161['add_list_year'] = !empty($add_list_year1[$n]) ? $add_list_year1[$n]:'';
	
	
						if(!empty($add_list_id1[$n])){
					$this->application_model->_updateapp1($id, $data_insert161,'application_ghi5g',$add_list_id1[$n],'add_list_id');
					$add_list_old_id[]=$add_list_id1[$n];
					}else{
			
					$this->db->insert('application_ghi5g', $data_insert161);
				    $add_list_new_id[]=$this->db->insert_id();
				         }
							
						
						
					//}
					
				}
	$this->is_delete($add_list_old_id,$add_list_new_id,'application_ghi5g','add_list_id',$id,$ghi5_parent_id1);
				
	}
	
	
	
	
	public function app_edit_ghi5($id=""){
		
		
	$insert_id=$this->application_model->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	
	$hauled_n_id = $this->input->post('hauled_n_id');
	
	$insert_h_id='';
	
	$ghi5_parent_id=$this->input->post('ghi5_parent_id');
	
	$data_hauler['quote_id'] =  $quote_id;	
	$data_hauler['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_hauler['hauler_MVR'] = $this->input->post('hauler_MVR');
	$data_hauler['hauler_Roa'] = $this->input->post('hauler_Roa');
	$data_hauler['hauler_Wri'] = $this->input->post('hauler_Wri');
	$data_hauler['hauler_Phy'] = $this->input->post('hauler_Phy');
	$data_hauler['hauler_Drug'] = $this->input->post('hauler_Drug');
	$data_hauler['hauler_Ref'] = $this->input->post('hauler_Ref');
	$data_hauler['hauler_Ver'] = $this->input->post('hauler_Ver');
	
	$data_hauler['ghi5_parent_id'] = $ghi5_parent_id;;
	
	
	$data_hauler['hauler_hir_name'] = $this->input->post('hauler_hir_name');
	$data_hauler['hauler_Own'] = $this->input->post('hauler_Own');
	$data_hauler['hauler_Rev'] = $this->input->post('hauler_Rev');
	$data_hauler['hauler_team'] = $this->input->post('hauler_team');
	$data_hauler['hauler_Num'] = $this->input->post('hauler_Num');
	$data_hauler['hauler_Num_y'] = $this->input->post('hauler_Num_y');
	$data_hauler['hauler_Vehi'] = $this->input->post('hauler_Vehi');
	
	
	$data_hauler['hauler_often'] = $this->input->post('hauler_often');
	$data_hauler['hauler_attach'] = $this->input->post('hauler_attach');
	$data_hauler['hauler_drivers'] = $this->input->post('hauler_drivers');
	$data_hauler['hauler_ins'] = $this->input->post('hauler_ins');
	$data_hauler['hauler_Pol'] = $this->input->post('hauler_Pol');
	$data_hauler['hauler_Effect']= $this->input->post('hauler_Effect');
	$data_hauler['hauler_Exp'] = $this->input->post('hauler_Exp');
	
	
	
	$data_hauler['hauler_Hir'] = $this->input->post('hauler_Hir');
	$data_hauler['hauler_Ter'] = $this->input->post('hauler_Ter');
	$data_hauler['hauler_Quit'] = $this->input->post('hauler_Quit');
	$data_hauler['hauler_Oth'] = $this->input->post('hauler_Oth');
	$data_hauler['hauler_Max'] = $this->input->post('hauler_Max');
	$data_hauler['hauler_Per']= $this->input->post('hauler_Per');
	$data_hauler['hauler_day'] = $this->input->post('hauler_day');
	
	
	$data_hauler['hauler_comp'] = $this->input->post('hauler_comp');
	$data_hauler['hauler_Others'] = $this->input->post('hauler_Others');
	$data_hauler['hauler_hours'] = $this->input->post('hauler_hours');
	$data_hauler['hauler_hour'] = $this->input->post('hauler_hour');
	$data_hauler['hauler_hou'] = $this->input->post('hauler_hou');
	$data_hauler['hauler_sleep']= $this->input->post('hauler_sleep');
	//$data_hauler['hauler_Motel'] = $this->input->post('hauler_Motel');
	
	$data_hauler['hauler_hother'] = $this->input->post('hauler_hother');	
	$data_hauler['hauler_rest'] = $this->input->post('hauler_rest');
	$data_hauler['hauler_roth']= $this->input->post('hauler_roth');
	$data_hauler['hauler_haul'] = $this->input->post('hauler_haul');
	
	$data_hauler['haul_name'] = $this->input->post('haul_name');
	$data_hauler['haul_agree'] = $this->input->post('haul_agree');
	$data_hauler['haul_attach'] = $this->input->post('haul_attach');
	$data_hauler['haul_Last'] = $this->input->post('haul_Last');
	$data_hauler['haul_Next'] = $this->input->post('haul_Next');
	$data_hauler['haul_attach_file'] = $this->input->post('haul_attach_file');

	
	
	$data_hauler['haul_equi'] = $this->input->post('haul_equi');
	$data_hauler['haul_rent'] = $this->input->post('haul_rent');
	$data_hauler['haul_lease'] = $this->input->post('haul_lease');
	$data_hauler['haul_agreement'] = $this->input->post('haul_agreement');
	$data_hauler['haul_agree_file'] = $this->input->post('haul_agree_file');
	$data_hauler['haul_month']= $this->input->post('haul_month');
	$data_hauler['haul_year'] = $this->input->post('haul_year');
	$data_hauler['haul_bor']= $this->input->post('haul_bor');
	$data_hauler['haul_ope'] = $this->input->post('haul_ope');
	
	
	$data_hauler['haul_yagree'] = $this->input->post('haul_yagree');
	$data_hauler['haul_yagree_file'] = $this->input->post('haul_yagree_file');
	$data_hauler['haul_yins'] = $this->input->post('haul_yins');
	
	
	
	$data_hauler['subcon_y'] = $this->input->post('subcon_y');
	$data_hauler['subcon_name'] = $this->input->post('subcon_name');
	$data_hauler['subcon_cert'] = $this->input->post('subcon_cert');
	$data_hauler['subcon_lia'] = $this->input->post('subcon_lia');
	$data_hauler['subcon_duties'] = $this->input->post('subcon_duties');
	$data_hauler['subcon_cost'] = $this->input->post('subcon_cost');
	$data_hauler['subcon_emp_y'] = $this->input->post('subcon_emp_y');
	
	
	$data_hauler['sub_haul_y'] = $this->input->post('sub_haul_y');
	$data_hauler['audit_y'] = $this->input->post('audit_y');
	$data_hauler['vehicles_y'] = $this->input->post('vehicles_y');
	$data_hauler['ride_y'] = $this->input->post('ride_y');
	$data_hauler['driver_y'] = $this->input->post('driver_y');
	$data_hauler['activity_y'] = $this->input->post('activity_y');
	
	$data_hauler['prior_y'] = $this->input->post('prior_y');
	$data_hauler['procedure_y'] = $this->input->post('procedure_y');
	$data_hauler['Drug_y'] = $this->input->post('Drug_y');
	$data_hauler['hiring_file'] = $this->input->post('hiring_file');
	$data_hauler['employees_y'] = $this->input->post('employees_y');
	$data_hauler['explain_no'] = $this->input->post('explain_no');
	
	$data_hauler['safety_y'] = $this->input->post('safety_y');
	$data_hauler['details_y'] = $this->input->post('details_y');
	$data_hauler['program_detail'] = $this->input->post('program_detail');
	$data_hauler['load_y'] = $this->input->post('load_y');
	$data_hauler['ins_pol'] = $this->input->post('ins_pol');
	$data_hauler['safety_attach_file'] = $this->input->post('safety_attach_file');



				  if($hauled_n_id!=''){
						$this->application_model->_updateapp1($id,$data_hauler,'application_ghi5a',$hauled_n_id,'haul_id');
				  }else{
					  $this->db->insert('application_ghi5a',$data_hauler);
					  $insert_h_id=$this->db->insert_id();
					  }
			
	$add_ins_id= $this->input->post('add_ins_id'); 
	   
	$add_ins_name= $this->input->post('add_ins_name');
	$add_ins_carrier= $this->input->post('add_ins_carrier');
	$ins_attach_y= $this->input->post('ins_attach_y');
	$add_ins_attach = $this->input->post('add_ins_attach');
	
    $add_ins_old_id1=array();
	$add_ins_new_id1=array();
		
      for ($i = 0; $i < count($add_ins_name); $i++) 
				{
					//if ($add_ins_name[$i] != '') 
				//	{ 
					
	$data_insert12['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;
	$data_insert12['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id : '' ;
    $data_insert12['quote_id'] =  $quote_id;	
	$data_insert12['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_insert12['add_ins_name'] = !empty($add_ins_name[$i]) ? $add_ins_name[$i] :'';
	$data_insert12['add_ins_carrier'] = !empty($add_ins_carrier[$i]) ? $add_ins_carrier[$i]:'';
	$data_insert12['ins_attach_y'] = !empty($ins_attach_y[$i]) ? $ins_attach_y[$i]:'';
	$data_insert12['add_ins_attach'] = !empty($add_ins_attach[$i]) ? $add_ins_attach[$i]:'';
	
						
						
						
					if(!empty($add_ins_id[$i])){
					$this->application_model->_updateapp1($id, $data_insert12,'application_ghi5b',$add_ins_id[$i],'add_ins_id');
					$add_ins_old_id1[]=$add_ins_id[$i];
	
					}else{
					
					$this->db->insert('application_ghi5b', $data_insert12);
					$add_ins_new_id1[]=$this->db->insert_id();
				}
					
						
						
					//}
					
				}
				
	$this->is_delete($add_ins_old_id1,$add_ins_new_id1,'application_ghi5b','add_ins_id',$id,$ghi5_parent_id);

	
	$add_owner_id= $this->input->post('add_owner_id'); 
				   
	$add_owner_name= $this->input->post('add_owner_name');
	$add_owner_y= $this->input->post('add_owner_y');
	$add_owner_attach= $this->input->post('add_owner_attach');
	$add_owner_old_id1=array();
	$add_owner_new_id1=array();
		
      for ($j = 0; $j < count($add_owner_name); $j++) 
				{
					//if ($add_owner_name[$j] != '') 
					//{ 
	$data_insert13['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;
	$data_insert13['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id : '' ;
    $data_insert13['quote_id'] =  $quote_id;	
	$data_insert13['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_insert13['add_owner_name'] = !empty($add_owner_name[$j]) ?  $add_owner_name[$j] :'';
	$data_insert13['add_owner_y'] = !empty($add_owner_y[$j]) ? $add_owner_y[$j]:'' ;
	$data_insert13['add_owner_attach'] = !empty($add_owner_attach[$j]) ? $add_owner_attach[$j]:'';
	
	
											if(!empty($add_owner_id[$j])){
					$this->application_model->_updateapp1($id, $data_insert13,'application_ghi5c',$add_owner_id[$j],'add_owner_id');
					$add_owner_old_id1[]=$add_owner_id[$j];
	
					}else{
					
					$this->db->insert('application_ghi5c', $data_insert13);
					$add_owner_new_id1[]=$this->db->insert_id();
				          }
					//	}
					
				}
	$this->is_delete($add_owner_old_id1,$add_owner_new_id1,'application_ghi5c','add_owner_id',$id,$ghi5_parent_id);		
				
	
	$subcon_add_id= $this->input->post('subcon_add_id'); 
				   
	$subcon_add_name= $this->input->post('subcon_add_name');
	$subcon_add_vehicle= $this->input->post('subcon_add_vehicle');
	$subcon_add_y= $this->input->post('subcon_add_y');
	$subcon_add_attach = $this->input->post('subcon_add_attach');
	$subcon_old_id1=array();
	$subcon_new_id1=array();
	
      for ($k = 0; $k < count($subcon_add_name); $k++) 
				{
				//	if ($subcon_add_name[$k] != '') 
				//	{ 
    $data_insert17['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;
	$data_insert17['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id : '' ;
    $data_insert17['quote_id'] =  $quote_id;	
	$data_insert17['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert17['subcon_add_name'] = !empty($subcon_add_name[$k])? $subcon_add_name[$k] :'';
	$data_insert17['subcon_add_vehicle'] = !empty($subcon_add_vehicle[$k])? $subcon_add_vehicle[$k] :'';
	$data_insert17['subcon_add_y'] = !empty($subcon_add_y[$k]) ? $subcon_add_y[$k] :'';
	$data_insert17['subcon_add_attach'] = !empty($subcon_add_attach[$k]) ? $subcon_add_attach[$k] :'' ;
	
						
					
				if(!empty($subcon_add_id[$k])){
					$this->application_model->_updateapp1($id, $data_insert17,'application_ghi5d',$subcon_add_id[$k],'subcon_add_id');
					$subcon_old_id1[]=$subcon_add_id[$k];
					}else{
					
					$this->db->insert('application_ghi5d', $data_insert17);
					$subcon_new_id1[]=$this->db->insert_id();
				        }
						
				//	}
					
				}
				
	$this->is_delete($subcon_old_id1,$subcon_new_id1,'application_ghi5d','subcon_add_id',$id,$ghi5_parent_id);
	
	 
	$sub_haul_id= $this->input->post('sub_haul_id');

	$sub_haul_add_name= $this->input->post('sub_haul_add_name');
	$sub_haul_add_carrier= $this->input->post('sub_haul_add_carrier');
	$sub_haul_add_limit= $this->input->post('sub_haul_add_limit');
	$sub_haul_add_y = $this->input->post('sub_haul_add_y');
	$sub_haul_add_attach= $this->input->post('sub_haul_add_attach');
	$sub_haul_old_id1=array();	
	$sub_haul_new_id1=array();
      for ($l = 0; $l < count($sub_haul_add_name); $l++) 
				{
					//if ($sub_haul_add_name[$l] != '') 
				//	{ 
	$data_insert14['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;
	
	$data_insert14['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id : '' ;
	
    $data_insert14['quote_id'] =  $quote_id;	
	$data_insert14['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert14['sub_haul_add_name'] = !empty($sub_haul_add_name[$l]) ? $sub_haul_add_name[$l] :'';
	$data_insert14['sub_haul_add_carrier'] = !empty($sub_haul_add_carrier[$l]) ? $sub_haul_add_carrier[$l]:'';
	$data_insert14['sub_haul_add_limit'] = !empty($sub_haul_add_limit[$l]) ? $sub_haul_add_limit[$l] :'';
	$data_insert14['sub_haul_add_y'] = !empty($sub_haul_add_y[$l]) ? $sub_haul_add_y[$l] :'';
	$data_insert14['sub_haul_add_attach'] = !empty($sub_haul_add_attach[$l]) ? $sub_haul_add_attach[$l] :'';
	
		if(!empty($sub_haul_id[$l])){
					$this->application_model->_updateapp1($id, $data_insert14,'application_ghi5e',$sub_haul_id[$l],'sub_haul_id');
					$sub_haul_old_id1[]=$sub_haul_id[$l];
					}else{
					
					$this->db->insert('application_ghi5e', $data_insert14);
					$sub_haul_new_id1[]=$this->db->insert_id();
				}
						
				//	}
					
				}
	$this->is_delete($sub_haul_old_id1,$sub_haul_new_id1,'application_ghi5e','sub_haul_id',$id,$ghi5_parent_id);			
				
	
	$add_mem_id= $this->input->post('add_mem_id');
				
	$add_mem_name= $this->input->post('add_mem_name');
	$add_mem_rel= $this->input->post('add_mem_rel');
    $add_mem_old_id1=array();
	$add_mem_new_id1=array();
	
		
      for ($m = 0; $m < count($add_mem_name); $m++) 
				{
				//	if ($add_mem_name[$m] != '') 
				//	{ 
	$data_insert15['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;	
    $data_insert15['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id : '' ;	
    $data_insert15['quote_id'] =  $quote_id;	
	$data_insert15['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert15['add_mem_name'] = !empty($add_mem_name[$m]) ? $add_mem_name[$m]:'';
	$data_insert15['add_mem_rel'] = !empty($add_mem_rel[$m]) ? $add_mem_rel[$m] :'';
	
	
	
	
							if(!empty($add_mem_id[$m])){
					$this->application_model->_updateapp1($id, $data_insert15,'application_ghi5f',$add_mem_id[$m],'add_mem_id');
					$add_mem_old_id1[]=$add_mem_id[$m];
					}else{
					
					$this->db->insert('application_ghi5f', $data_insert15);
					$add_mem_new_id1[]=$this->db->insert_id();
				         }
						
						
				//	}
					
				}
	$this->is_delete($add_mem_old_id1,$add_mem_new_id1,'application_ghi5f','add_mem_id',$id,$ghi5_parent_id);			
				
	$haul_id= $this->input->post('haul_list_id');
	$add_list_id= $this->input->post('add_list_id'); 
						
	$add_list_name= $this->input->post('add_list_name');
	$add_list_length= $this->input->post('add_list_length');
	$add_list_year= $this->input->post('add_list_year');
	$add_list_old_id1=array();
	$add_list_new_id1=array();
		
      for ($n = 0; $n < count($add_list_name); $n++) 
				{
				//	if ($add_list_name[$n] != '') 
					//{ 
	$data_insert16['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;	
    $data_insert16['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id : '' ;	
    $data_insert16['quote_id'] =  $quote_id;	
	$data_insert16['ref_id'] = !empty($id) ? $id : $ref_id;		
	$data_insert16['add_list_name'] = !empty($add_list_name[$n]) ? $add_list_name[$n] : '';
	$data_insert16['add_list_length'] = !empty($add_list_length[$n]) ? $add_list_length[$n] :'';
	$data_insert16['add_list_year'] = !empty($add_list_year[$n]) ? $add_list_year[$n] :'' ;
	
	
						if(!empty($add_list_id[$n])){
					    $this->application_model->_updateapp1($id, $data_insert16,'application_ghi5g',$add_list_id[$n],'add_list_id');
					    $add_list_old_id1[]=$add_list_id[$n];
					}else{
				      	$this->db->insert('application_ghi5g', $data_insert16);
				        $add_list_new_id1[]=$this->db->insert_id();
				          }
							
						
						
					//}
					
				}
				
			$this->is_delete($add_list_old_id1,$add_list_new_id1,'application_ghi5g','add_list_id',$id,$ghi5_parent_id);	
				
		 }
public function is_delete($old_id='',$new_id='',$table='',$table_id='',$ref_id='',$condition=''){		 
if(!empty($old_id)){
	 $result=array();
	 $adresses=$this->application_model->get_id_inform1($ref_id,$table,$condition);
	   foreach($adresses as $values){
	
		   $result[]=$values->$table_id;
		
	   }
	$flag_set= array_diff($result,$old_id,$new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->application_model->_updateapp1($ref_id, $insert_data130,$table,$vlaues,$table_id);
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->application_model->get_id_inform1($ref_id,$table,$condition);
	
	   foreach($array as $values){

		   $result[]=$values->$table_id;
		
	   }
	
	$flag_set= array_diff($result,$new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->application_model->_updateapp1($ref_id, $insert_data130,$table,$vlaues,$table_id);
	
	     }
	    }
	  
	  }
	
	
	}
}	
		 
}
?>
