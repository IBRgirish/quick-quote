<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class application_model extends CI_Model {


    public function &__get($key) {
        $CI = & get_instance();
        return $CI->$key;
        $this->load->database();
    }
	public function getAmount($money)
	{
		$cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
		$onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);
	
		$separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;
	
		$stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
		$removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);
	
		return (float) str_replace(',', '.', $removedThousendSeparator);
	}
	
	protected function _update_phone($id, $data, $table)
	{
		$this->db->where('quote_id', $id); 
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}
        
		return FALSE;
		
	}
		protected function _update_email($id, $data, $table)
	{
		$this->db->where('quote_id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
		}
		
	
		protected function _updatequote($id, $data, $table)
	{
		$this->db->where('general_id', $id);
		
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
		public function _updateapp1($id, $data, $table,$id1,$table_id)
	{
	
	
		$this->db->where('ref_id', $id);
		if($table_id!=''){
		$this->db->where($table_id, $id1);
	
			}	
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
			protected function _updateapp2($id, $data, $table,$id1,$table_id,$type_loss)
	{
	
	
		$this->db->where('ref_id', $id);
		if($table_id!=''){
		$this->db->where($table_id, $id1);
		     
			}	
			
			if($type_loss=='losses_liability'){
			$this->db->where('losses_liability', $type_loss);
			
			}
			
			
			if($type_loss=='losses_pd'){
			$this->db->where('losses_pd', $type_loss);
			
			}
			if($type_loss=='losses_cargo'){
			$this->db->where('losses_cargo', $type_loss);
			
			}
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
			public function _updateapp($id, $data, $table)
	{
	
	
		$this->db->where('ref_id', $id);
	
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	

	
		public function get_database_inform($id='',$table){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('ref_id', $id);
		$this->db->where('is_delete',null);
     	}
		
	     $query = $this->db->get();
   
	     $result = $query->result();
		//echo $this->db->last_query();
	return $result;
		
		}
		
	public function get_id_inform($id='',$table,$condition=''){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	      {
		    $this->db->where('ref_id', $id);
     	  }
		   if($condition!='')
	      {
		    $this->db->where('vehicle_type', $condition);
     	  }
	
	       $query = $this->db->get();
   
	       $result = $query->result();
	
	     return $result;
		
		}
		
	public function get_id_inform1($id='',$table,$condition=''){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	      {
		    $this->db->where('ref_id', $id);
     	  }
		   if($condition!='')
	      {
		    $this->db->where('ghi5_parent_id', $condition);
     	  }
	
	       $query = $this->db->get();
   
	       $result = $query->result();
	
	     return $result;
		
		}
		
		
		
		public function get_database_inform6($id='',$table,$id1=''){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		   $this->db->where('ref_id', $id);
		   $this->db->where('is_delete', null);
     	}
		

        if($id1!='')
	    {
		   $this->db->where('supplement_parent_id', $id1);
     	}

	      $query = $this->db->get();
   
	      $result = $query->result();
	
	return $result;
		
		}
		
		
	public function get_database_inform8($id='',$table,$id1=''){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('ref_id', $id);
		$this->db->where('is_delete', null);
     	}
	
     if($id1!='')
	    {
		$this->db->where('ghi5_parent_id', $id1);
     	}

	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		
				public function get_database_quote($id='',$table){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('quote_id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		
				public function get_database_members($id='',$table){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		public function get_database_quote1($id='',$table,$condition){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('quote_id', $id);
		$this->db->where('losses_type', $condition);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
	
		
			public function get_database_inform3($id='',$table,$id1=''){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('ref_id', $id);
     	}
		    if($id1!='')
	    {
		$this->db->where('vehicle_id', $id1);
     	}
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
	public function get_database_inform1($id='',$table,$condition){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('ref_id', $id);
		$this->db->where('losses_type', $condition);
		$this->db->where('is_delete', null);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
	public function get_database_quote2($id='',$table,$condition){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('quote_id', $id);
		$this->db->where('vehicle_type', $condition);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
	
			public function get_database_inform2($id='',$table,$condition){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('ref_id', $id);
		$this->db->where('vehicle_type', $condition);
		$this->db->where('is_delete', null);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}

		
		public function get_broker_inform($email=''){
		
		  $this->db->select('*');
	      $this->db->from('rq_members');
	     if($email!='')
	    {
		$this->db->where('email', $email);
     	}
	$this->db->where('is_deleted', 'FALSE');
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
	public function get_broker_inform_mem($id=''){
		
		  $this->db->select('address,email,phone_number,phone_ext,city,country,state,zip_code,zip_code_extd,fax_number,sec_email,website');
	      $this->db->from('rq_members');
	     if($id!='')
	    {
		$this->db->where('id', $id);
     	}
		
		
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		
		function get_broker_information($id=''){
		
		
		  $this->db->select('*');
	      $this->db->from('app_broker_inform');
	     if($id!='')
	    {
		$this->db->where('broker_id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		
				public function get_broker_quote($id=''){
		
		  $this->db->select('*');
	      $this->db->from('rq_rqf_quote');
	     if($id!='')
	    {
		$this->db->where('quote_id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		
	public function get_broker_insured($id=''){
		
		  $this->db->select('*');
	      $this->db->from('rq_rqf_insured_info');
	     if($id!='')
	    {
		$this->db->where('quote_id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		function replaceArrayToString($arr)
 {

 $array['quote'] = $this->input->post($arr);
	//print_r($array['quote']);
	if(empty($array['quote'])){
		
	}else{
foreach($array['quote'] as $value)
{
    $array_temp[] =  $value;
	
}
$arr = implode(',', $array_temp);

 return $arr;
 }
 }
 
 function replaceArrayToString1($arr)
 {

 $array['quote'] = $this->input->post($arr);
	//print_r($array['quote']);
	if(empty($array['quote'])){
		
	}else{
foreach($array['quote'] as $value)
{
    $array_temp[] =  $value;
	
}
$arr = implode('-', $array_temp);

 return $arr;
 }
 }
 
function convertToObject($array=''){
 
}
 
/*	function quote_id()
 {
    $this->db->select('*');
	$this->db->from('application_general');
		
	$this->db->order_by('applicant_id', 'desc');
	$query = $this->db->get();
   
	$result = $query->result();
	return $result;
 }*/
	
	function app_applicant_db($id='') {
    $app_name = $this->input->post('app_name');
	
	$quote_id1 = $this->input->post('quote_id1');
	$quote_id = $this->input->post('quote_id');
	
	
	if ($_POST) 
		{
    $insert_data_general['quote_id'] = $quote_id1;
	$insert_data_general['company'] = $this->input->post('company');	
	$insert_data_general['app_name']=isset($app_name) ? implode(',',$app_name) : '';
	$insert_data_general['Quote_num'] = $this->input->post('Quote_num');
	$insert_data_general['Quote_num_new'] = $this->input->post('Quote_num_new');	
	$insert_data_general['effective_from'] = $this->input->post('effective_from');
	$insert_data_general['expiration_from'] = $this->input->post('expiration_from');
	$insert_data_general['audit_name'] = $this->input->post('audit_name');
	$insert_data_general['Specify_oth'] = $this->input->post('Specify_oth');
	$insert_data_general['remarks'] = $this->input->post('remarks');
	$insert_data_general['underwriter_eamil'] = $this->input->post('underwriter');
	
	if($id!='')
	{
	//$insert_status['status'] = 'Draft';	
	$this->_updatequote($id, $insert_data_general,'application_general');
	//$this->db->insert('application_general',$insert_data_general);
	
	} else {
		
		
	$this->db->insert('application_general',$insert_data_general);
	
	$insert_id=$this->db->insert_id();
     }
	 
	$brokr_fax_number= $this->input->post('brokr_fax_number');	
	$brker_tel= $this->input->post('brokr_tel');	 
	$insert_data_broker['ref_id'] = isset($insert_id) ? $insert_id : $id;
	$insert_data_broker['quote_id'] = $quote_id;
	$insert_data_broker['broker_name'] = $this->input->post('broker_name');	
	$insert_data_broker['broker_name_dba'] = $this->input->post('broker_name_dba');
	$insert_data_broker['broker_address'] = $this->input->post('broker_address');	
	$insert_data_broker['brokr_tel'] = isset($brker_tel) ? implode('-',$brker_tel) : ''; 
	$insert_data_broker['broker_tel_ext'] = $this->input->post('broker_tel_ext');
	$insert_data_broker['broker_email'] = $this->input->post('broker_email');
	$insert_data_broker['broker_remarks'] = $this->input->post('broker_remarks');
	
	
	$insert_data_broker['brokr_fax_number'] = isset($brokr_fax_number) ? implode('',$brokr_fax_number) : '';
	$insert_data_broker['broker_country'] = $this->input->post('broker_country');	
	$insert_data_broker['broker_state'] = $this->input->post('broker_state');
	$insert_data_broker['broker_city'] = $this->input->post('broker_city');	
	
	
	$insert_data_broker['broker_zip_code'] = $this->input->post('broker_zip_code');
	$insert_data_broker['broker_zip_code_extd'] = $this->input->post('broker_zip_code_extd');
	$insert_data_broker['broker_website'] = $this->input->post('broker_website');
	$insert_data_broker['broker_sec_email'] = $this->input->post('broker_sec_email');
	
	 if($id!='')
	{
	 $this->_updateapp($id, $insert_data_broker,'app_broker_inform');
	
	} else {
	 $this->db->insert('app_broker_inform',$insert_data_broker);
     }
	 
	 
	//$up_insert_id=$this->input->post('up_insert_id', true);
	
	
	$insert_data_applicant['ref_id'] = isset($insert_id) ? $insert_id : $id;
	$insert_data_applicant['quote_id'] = $quote_id;	
	$insert_data_applicant['insured_first_name'] = $this->input->post('insured_first_name');
	$insert_data_applicant['insured_middle_name'] = $this->input->post('insur_middle_name');
	$insert_data_applicant['insured_last_name'] = $this->input->post('insured_last_name');
	$insert_data_applicant['dba'] = $this->input->post('dba');
	$insert_data_applicant['app_address'] = $this->input->post('app_address');
	$insert_data_applicant['app_city'] = $this->input->post('app_city');
	$insert_data_applicant['state_app'] = $this->input->post('state_app');
	$insert_data_applicant['country_app'] = $this->input->post('country_app');
	$insert_data_applicant['add_add_yn'] = $this->input->post('add_add_yn');
	$insured_zip1 = $this->input->post('insured_zip1', true);
    $insured_zip2 = $this->input->post('insured_zip2', true);
	$insured_zip=$insured_zip1.'-'.$insured_zip2;
//	die;
	$insert_data_applicant['insured_zip']=isset($insured_zip) ? $insured_zip  : '';
    $insured_telephone = $this->input->post('insured_telephone', true);
	$insert_data_applicant['insured_telephone']=isset($insured_telephone) ? implode('-',$insured_telephone) : '';	
	//$insert_data_applicant['insured_zip'] = $this->replaceArrayToString1('insured_zip');
	//$insert_data_applicant['insured_telephone'] = $this->replaceArrayToString1('insured_telephone');	
	$insert_data_applicant['insured_tel_ext'] = $this->input->post('insured_tel_ext');
	$insert_data_applicant['insured_email'] = $this->input->post('insured_email');
	$insert_data_applicant['other_contact'] = $this->input->post('Other_contact');	

	
	


	// print_r($insert_data_applicant);
	 if($id!='')
	{
	//$apid = $this->input->post('applicant_id', true);
	
	$this->_updateapp($id, $insert_data_applicant,'application_applicant');
	} else {
	//$this->db->insert('application_general',$insert_data_general);
	$this->db->insert('application_applicant',$insert_data_applicant);
	
     }
	 
	 
	 $insured_telephone_add1 = $this->input->post('insured_telephone_add1', true);
	 $insured_telephone_add2 = $this->input->post('insured_telephone_add2', true);
	 $insured_telephone_add3 = $this->input->post('insured_telephone_add3', true);
	 $insured_telephone_type = $this->input->post('insured_telephone_type', true);
	 $insured_tel_ext_add = $this->input->post('insured_tel_ext_add', true);
	 
	 
	 $insured_telephone_attention = $this->input->post('insured_telephone_attention', true);
	
	 $insured_email_add = $this->input->post('insured_email_add', true);
	
	 $insured_email_type = $this->input->post('insured_email_type', true);
		
	 $insured_email_attention = $this->input->post('insured_email_attention', true);
	 
	 
	 $tel_id = $this->input->post('tel_id', true);
	 $email_id = $this->input->post('email_id', true);
	
	 $add_id=$this->input->post('add_id', true);
	
	
	
		 
	 $type = $this->input->post('add_type', true);	
	 $address = $this->input->post('add_address', true);	
	 $city = $this->input->post('add_city', true);	
	 $country = $this->input->post('add_country', true);	
	 
	 $state = $this->input->post('add_state', true);
	 $zip = $this->input->post('add_zip', true);
	 $ext_zip = $this->input->post('add_ext_zip', true);
	 
	
	
	 if(!empty($add_id)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'adresses');
	   foreach($adresses as $values){
	  // print_R($values);
	  
		   $result[]=$values->id;
		
		 
	   }
	$flag_set= array_diff($result,$add_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'adresses',$vlaues,'id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'adresses');
	   foreach($array as $values){
	  // print_R($values);
	  
		   $result[]=$values->id;
		
		 
	   }
	//$flag_set= array_diff($result,$tel_id);
 
	foreach ($result as $Keys=>$vlaues) {
	
	
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'adresses',$vlaues,'id');
	
	     }
	    }
	  
	  }
	
	
	}
	 
	for ($a=0; $a < count($zip); $a++) {
    
	if($zip[$a]!=''){
	
    $insert_data30['ref_id'] = isset($insert_id) ? $insert_id : $id;
	$insert_data30['quote_id'] = $quote_id;	
	$insert_data30['country']=isset($country[$a]) ? $country[$a] : '';
	$insert_data30['type']=isset($type[$a]) ? $type[$a] : '';	
	$insert_data30['address']=isset($address[$a]) ? $address[$a] : '';
	$insert_data30['city']=isset($city[$a]) ? $city[$a] : '';
    $insert_data30['state']=isset($state[$a]) ? $state[$a] : '';		
    $insert_data30['zip']=isset($zip[$a]) ? $zip[$a].'-'.$ext_zip[$a]  : '';
   
   
 
	if(!empty($add_id[$a])){
		
	$this->_updateapp1($id, $insert_data30,'adresses',$add_id[$a],'id');
	
	}else{
	
	$this->db->insert('adresses',$insert_data30);
	
		}
	
	 }
	}
	   
	
	
	
	if(!empty($tel_id)){
	 $result=array();
	 $array=$this->get_id_inform($id,'application_tel_add');
	   foreach($array as $values){
	  // print_R($values);
	  
		   $result[]=$values->tel_id;
		
		 
	   }
	$flag_set= array_diff($result,$tel_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_tel_add',$vlaues,'tel_id');
	
	     }
	    }
	   }
	  }
	}else{
	
	
	$result=array();
	 $array=$this->get_id_inform($id,'application_tel_add');
	   foreach($array as $values){
	  // print_R($values);
	  
		   $result[]=$values->tel_id;
		
		 
	   }
	//$flag_set= array_diff($result,$tel_id);
 
	foreach ($result as $Keys=>$vlaues) {
	
	
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_tel_add',$vlaues,'tel_id');
	
	     }
	    }
	  
	  }
	
	
	}
	
	
	
	
	for ($m=0; $m < count($insured_telephone_add1); $m++) {
    
	if($insured_telephone_add1[$m]!=''){
	
    $insert_data_applicant1['ref_id'] = isset($insert_id) ? $insert_id : $id;
	$insert_data_applicant1['quote_id'] = $quote_id;	
	$insert_data_applicant1['insured_telephone_add']=isset($insured_telephone_add1[$m]) ? $insured_telephone_add1[$m].'-'.$insured_telephone_add2[$m].'-'.$insured_telephone_add3[$m] : '';	
	$insert_data_applicant1['insured_telephone_type']=isset($insured_telephone_type[$m]) ? $insured_telephone_type[$m] : '';	
	$insert_data_applicant1['insured_telephone_attention']=isset($insured_telephone_attention[$m]) ? trim($insured_telephone_attention[$m]) : '';
    $insert_data_applicant1['insured_tel_ext_add']=isset($insured_tel_ext_add[$m]) ? $insured_tel_ext_add[$m] : '';		
   
   
   
	if(!empty($tel_id[$m])){
		
	$this->_updateapp1($id, $insert_data_applicant1,'application_tel_add',$tel_id[$m],'tel_id');
	
	}else{
	
	$this->db->insert('application_tel_add',$insert_data_applicant1);
	
		}
	
	 }
	}
	
	
	
	
	if(!empty($email_id)){
	 $result=array();
	 $array=$this->get_id_inform($id,'application_email_add');
	   foreach($array as $values){
	  // print_R($values);
	  
		   $result[]=$values->email_id;
		
		 
	   }
	$flag_set= array_diff($result,$email_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_email_add',$vlaues,'email_id');
	
	     }
	    }
	   }
	  }
	}else{
	
	 $result=array();
	 $array=$this->get_id_inform($id,'application_email_add');
	   foreach($array as $values){
	
	  
		   $result[]=$values->email_id;
		
		 
	   }
	
 
	foreach ($result as $Keys=>$vlaues) {
	
	
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_email_add',$vlaues,'email_id');
	
	     }
	    }
	  
	  }
	
	}
	
	
	for ($n=0 ; $n < count($insured_email_add); $n++) {
 
	if($insured_email_add[$n]!=''){
	
	$insert_data_applicant12['ref_id'] = isset($insert_id) ? $insert_id : $id;
	$insert_data_applicant12['quote_id'] = $quote_id;
	$insert_data_applicant12['insured_email_add']=isset($insured_email_add[$n]) ? $insured_email_add[$n] : '';	
	$insert_data_applicant12['insured_email_type']=isset($insured_email_type[$n]) ? $insured_email_type[$n] : '';
	$insert_data_applicant12['insured_email_attention']=isset($insured_email_attention[$n]) ? $insured_email_attention[$n] : '';
	
   if(!empty($email_id[$n])){
	$this->_updateapp1($id, $insert_data_applicant12,'application_email_add',$email_id[$n],'email_id');
	
	}else{
	$this->db->insert('application_email_add',$insert_data_applicant12);
	
		}

	  }
	}

	
	 }

	 
	}

	
	
	
function app_losses_db($id='') {

  $insert_id=$this->getid();


 $quote_id = $this->input->post('quote_id');
 $losses_id = $this->input->post('losses_id');

	if ($_POST) 
		{
	$insert_data_losses = array(
	'ref_id' => !empty($id) ?$id : $insert_id[0]->general_id,
	'quote_id'=>$quote_id,
	'app_garage'=>$this->input->post('app_garage'),
	'audit_name_losses' => $this->input->post('audit_name_losses'),
	'audit_name_other' => $this->input->post('audit_name_other'),
	'audit_SSN' => $this->input->post('audit_SSN'),
	'applicant_ever' => $this->input->post('applicant_ever'),
	'explain_business' => $this->input->post('explain_business'),
	'applicant_conducted' => $this->input->post('applicant_conducted'),
	'explain_conducted' => $this->input->post('explain_conducted'),
	'applicant_transportation' => $this->input->post('applicant_transportation'),	
	'losses_need' => $this->input->post('losses_need'),
	'losses_name' => $this->input->post('losses_name'),
	'losses_attached' => $this->input->post('losses_attached'),
	'exp_year' => $this->input->post('exp_year'),
	'exp_case' => $this->input->post('exp_case'),
	'exp_number' => $this->input->post('exp_number'),
	'exp_county' => $this->input->post('exp_county'),
	'exp_state' => $this->input->post('exp_state'),
	'SSN' => $this->input->post('SSN'),
	'loss_report_filename' => $this->input->post('loss_report_filename')
	
	);
	
	
	
	
	
	if($losses_id!=''){
	$this->_updateapp($id, $insert_data_losses,'application_losses');
	   
	 }else{
	$this->db->insert('application_losses',$insert_data_losses);
	}
	

		$losses_fields = array('losses_liability','losses_pd','losses_cargo','losses_bobtail','losses_dead','losses_trunk','losses_excess');
 
		
		$insert_new_id=array();
		$lossess_old_id=array();
		
		 foreach($losses_fields as $heading)
			{
			        
			    $lossess_id = $this->input->post($heading.'_id');
			
				$losses_type = $this->input->post($heading.'_type');
				
				$losses_from = $this->input->post($heading.'_from');
				$losses_to = $this->input->post($heading.'_to');
				//start by Ashvin patel 14nov2014
				$losses_claim = $this->input->post($heading.'_cliam');
				$losses_ac_city = $this->input->post($heading.'_ac_city');
				
				$losses_ac_state = $this->input->post($heading.'_ac_state');
				//end by Ashvin patel 14nov2014
                
     			$losses_amount = $this->input->post($heading.'_amount');
				$losses_company = $this->input->post($heading.'_company');
				$losses_general_agent = $this->input->post($heading.'_general_agent');
				$losses_date_of_loss = $this->input->post($heading.'_date_of_loss');
				$uploaded_files_values = $this->input->post($heading.'_values1');
				
	
	
			
	
				for ($i = 0; $i < count($losses_from); $i++) 
				{
					if ($losses_from[$i] != '') 
					{ 
						if($heading=='losses_other1' || $heading=='losses_other2')
						{
							$heading = $heading.'_specify_other';
						}
					    $insert_data1['ref_id'] = !empty($id) ? $id : $insert_id[0]->general_id;
						$insert_data1['quote_id'] = $quote_id;
						
						$insert_data1['losses_from_date'] = isset($losses_from[$i]) ? date("Y-m-d",strtotime($losses_from[$i])) : '';
						$insert_data1['losses_to_date'] =  isset($losses_to[$i]) ? date("Y-m-d", strtotime($losses_to[$i])) :'';
						
						//start by Ashvin patel 14nov2014
						$insert_data1['losses_claim'] = isset($losses_claim[$i]) ? $losses_claim[$i] :'';
						$insert_data1['losses_ac_city'] = isset($losses_amount[$i]) ? $losses_ac_city[$i] :'';
						$insert_data1['losses_ac_state'] = isset($losses_amount[$i]) ? $losses_ac_state[$i] :'';
						//end by Ashvin patel 14nov2014
					
						
						
						$insert_data1['losses_amount'] = isset($losses_amount[$i]) ?$losses_amount[$i] :'';
						$insert_data1['losses_company'] = isset($losses_company[$i]) ?$losses_company[$i] :'';
						$insert_data1['losses_general_agent'] = isset($losses_general_agent[$i]) ?$losses_general_agent[$i] :'';
						$insert_data1['date_of_loss'] = isset($losses_date_of_loss[$i]) ? date("Y-m-d", strtotime($losses_date_of_loss[$i])) :'';
						$insert_data1['uploaded_files_values'] = isset($uploaded_files_values[$i]) ? $uploaded_files_values[$i] :'';
			           

					   if(!empty($lossess_id[$i])){
					    //print_r($insert_data1);
						//die;
						$this->_updateapp1($id, $insert_data1,'application_quote_losses',$lossess_id[$i],'losses_id');
						$lossess_old_id[]=$lossess_id[$i];
						//print_r($lossess_old_id);
						}else{
					     
	                     $insert_data1['losses_type']=$heading;
                         $this->db->insert('application_quote_losses',$insert_data1);
						 $insert_new_id[]=$this->db->insert_id();
	                     }
					   }
					     
		   }
		  
    	}
		//print_r($lossess_old_id);
			if(!empty($lossess_old_id)){
	               $result=array();
				
	                 $array=$this->get_id_inform($id,'application_quote_losses');
	                  foreach($array as $values){
	                   // print_R($values);
	  
		                $result[]=$values->losses_id;
		
		 
	                    }
	                    $flag_set= array_diff($result,$lossess_old_id,$insert_new_id);
						
               
	                    foreach ($flag_set as $Keys=>$vlaues) {
	
	                    if(isset($flag_set)){
	                    if($vlaues!=''){
	
                        $insert_data130['is_delete'] = 1;
	
	 
                        if(!empty($vlaues)){
		                 $this->_updateapp1($id, $insert_data130,'application_quote_losses',$vlaues,'losses_id');
	                 //   $this->_updateapp1($id, $insert_data130,'adresses',$vlaues,'id');
	
	                }
	             }
	          }
	       }
	    }else{
		            $result=array();
	                $array=$this->get_id_inform($id,'application_quote_losses');
	               foreach($array as $values){
	                // print_R($values);
	  
		           $result[]=$values->losses_id;
		
		 
	               }
	            
 
	             $flag_set= array_diff($result,$insert_new_id);
						
               
	             foreach ($flag_set as $Keys=>$vlaues) {
	
	
	              if($vlaues!=''){
	
                  $insert_data130['is_delete'] = 1;
	
	 
                  if(!empty($vlaues)){
		
	              $this->_updateapp1($id, $insert_data130,'application_quote_losses',$vlaues,'losses_id');
	
	            }
	          }
	       }
		
		
		}
		
		
		
	
	   
}
	if(isset($update_loss_ids)){
		if(is_array($update_loss_ids)){
		
		}
	}
}//End Losses Section
	
function app_commodity_db($id='') {
$insert_id=$this->getid();
$quote_id = $this->input->post('quote_id');
$in_id=$insert_id[0]->general_id;

  $application_commodity_id = $this->input->post('application_commodity_id');

	if ($_POST) 
		{
	$commodity_cargo_per = $this->input->post('commodity_cargo_per');	
	$commodity_cargo_val = $this->input->post('commodity_cargo_val');
	$aapli_name_other =$this->input->post('aapli_name_other');
	$aapli_name=$this->input->post('aapli_name');
	//$insert_data_commodity = array(
	$insert_data_commodity['ref_id'] =!empty($id) ? $id: $insert_id[0]->general_id;
	$insert_data_commodity['quote_id']=$quote_id;
	$insert_data_commodity['commodity_number']=$this->input->post('commodity_number');
	$insert_data_commodity['aapli_name']=isset($aapli_name) ? is_array($aapli_name) ? implode(',',$aapli_name) : '' : '';
	$insert_data_commodity['commodity_cargo_per']=isset($commodity_cargo_per) ? is_array($commodity_cargo_per) ? implode(',',$commodity_cargo_per) : '' : '';
	$insert_data_commodity['commodity_cargo_val']=isset($commodity_cargo_val) ? is_array($commodity_cargo_val) ? implode(',',$commodity_cargo_val) : '' : '';
	$insert_data_commodity['aapli_name_other']=isset($aapli_name_other) ? is_array($aapli_name_other) ? implode(',',$aapli_name_other) : '' : '';
	$insert_data_commodity['commodity_other_per']=$this->input->post('commodity_other_per');
	$insert_data_commodity['commodity_other_val']=$this->input->post('commodity_other_val');
	
	
	
	 if($application_commodity_id!='')
	{
	$this->_updateapp($id, $insert_data_commodity,'application_commodity');
	
	} else {
	
	$this->db->insert('application_commodity',$insert_data_commodity);
     }
	 
	
	$application_transported_id = $this->input->post('application_transported_id');
  
    $insured_zip11 = $this->input->post('insured_zip11', true);
	$insured_zip21 = $this->input->post('insured_zip12', true);
	$insured_zip12=$insured_zip11.'-'.$insured_zip21;

    $insured_telephone21 = $this->input->post('insured_telephone1', true);
	$insured_telephone12=isset($insured_telephone21) ? implode('-',$insured_telephone21) : '';	


	$insert_data_transported = array(
	'ref_id' =>!empty($id) ? $id: $insert_id[0]->general_id,
	'quote_id'=>$quote_id,
	'transported_name' => $this->input->post('transported_name'),
	'transported_desc' => $this->input->post('transported_desc'),
	'hazmat_name' => $this->input->post('hazmat_name'),	
	'passengers_name' => $this->input->post('passengers_name'),
	
	
	'insured_first_name' => $this->input->post('insured_first_name1'),
	'insured_middle_name' => $this->input->post('ins_middle_name1'),
	'insured_last_name' => $this->input->post('insured_last_name1'),	
	'dba' => $this->input->post('dba1'),
	
	
	'app_address' => $this->input->post('app_address1'),
	'app_city' => $this->input->post('app_city1'),
	'state_app' => $this->input->post('state_app1'),	
	'country_app' => $this->input->post('country_app1'),
	
	
	'insured_zip' => $insured_zip12,
	'insured_telephone' => $insured_telephone12,
	'insured_tel_ext' => $this->input->post('insured_tel_ext1'),	
	'insured_email' => $this->input->post('insured_email1'),
	);


	
		 if($application_commodity_id!='')
	{
	$this->_updateapp($id, $insert_data_transported,'application_transported');
	
	} else {
	
	$this->db->insert('application_transported',$insert_data_transported);
     }
	
	    
		$app_fill_id = $this->input->post('app_fill_id');
		

		$filings_num = $this->input->post('filings_num');
		
		$filings_type = $this->input->post('filings_type');
		$filing_type_other = $this->input->post('filing_type_other');
		$trucking_comp_name = $this->input->post('trucking_comp_name');
	
	
	$insert_data_filings = array(
	 'ref_id' =>!empty($id) ? $id: $insert_id[0]->general_id,
	'quote_id'=>$quote_id,
	'filings_name' => $this->input->post('filings_name'),
	'filings_number' => $this->input->post('filings_number'),		
	'filings_type' => !empty($filings_type) ? implode(',',$filings_type) : '',
	'filings_num' => !empty($filings_num) ? implode(',',$filings_num) : '',	
	'state_fill' => !empty($filing_type_other) ? implode(',',$filing_type_other) : '',
	'trucking_comp_name' =>!empty($trucking_comp_name) ? implode(',',$trucking_comp_name) : '',
	'trucking_comp_other' => $this->input->post('trucking_comp_other'),
	'certificate_name' => $this->input->post('certificate_name'),
	'cert_num' => $this->input->post('cert_num'),
	);

 if($app_fill_id!='')
	{
	$this->_updateapp($id, $insert_data_filings,'application_filings');
	
	} else {
	
	$this->db->insert('application_filings',$insert_data_filings);
     }
	
	
	$cert_id = $this->input->post('cert_id');		
	$cert_comp = $this->input->post('cert_comp');
	$cert_attent = $this->input->post('cert_attent');	
	$cert_add = $this->input->post('cert_add');
	$cert_city = $this->input->post('cert_city');
	$cert_email = $this->input->post('cert_email');	
	$cert_telephone = $this->input->post('cert_telephone');
	$cert_telephone_ext = $this->input->post('cert_telephone_ext');
	$cert_fax = $this->input->post('cert_fax');
    $state_cert = $this->input->post('state_cert');	
		

	$cert_zip = $this->input->post('cert_zip');
    $Additional_ins = $this->input->post('Additional_ins');

	$cert_old_id=array();
	$cert_new_id=array();
	
	for ($p = 0; $p < count($Additional_ins); $p++) {

	if ($Additional_ins[$p] != '') {
	$insert_data_certificate['ref_id'] =!empty($id) ? $id: $insert_id[0]->general_id;	
	$insert_data_certificate['quote_id']=$quote_id;
	$insert_data_certificate['cert_comp']=isset($cert_comp[$p]) ? $cert_comp[$p] : '';
	$insert_data_certificate['cert_attent']=isset($cert_attent[$p]) ? $cert_attent[$p] : '';
	$insert_data_certificate['cert_add']=isset($cert_add[$p]) ? $cert_add[$p] : '';
	$insert_data_certificate['cert_city']=isset($cert_city[$p]) ? $cert_city[$p] : '';
	$insert_data_certificate['cert_email']=isset($cert_email[$p]) ? $cert_email[$p] : '';
	$insert_data_certificate['cert_telephone']=isset($cert_telephone[$p]) ? implode('-',$cert_telephone) : '';
	$insert_data_certificate['cert_telephone_ext']=isset($cert_telephone_ext[$p]) ? $cert_telephone_ext[$p] : '';
	$insert_data_certificate['cert_fax']=isset($cert_fax[$p]) ? implode('-',$cert_fax) : '';
	$insert_data_certificate['state_cert']=isset($state_cert[$p]) ? $state_cert[$p] : '';
	$insert_data_certificate['cert_zip']=isset($cert_zip[$p]) ? $cert_zip[$p] : '';
	$insert_data_certificate['Additional_ins']=isset($Additional_ins[$p]) ? $Additional_ins[$p] : '';
	
	
	 if(!empty($cert_id[$p]))
	{

	$this->_updateapp1($id, $insert_data_certificate,'application_certificate',$cert_id[$p],'application_certificate_id');
	$cert_old_id[]=$cert_id[$p];
	} else {

	$this->db->insert('application_certificate',$insert_data_certificate);
     
	 $cert_new_id[]=$this->db->insert_id();
	  }
    }
  }
	
	
	
	if(!empty($cert_old_id)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'application_certificate');
	   foreach($adresses as $values){
	
		   $result[]=$values->application_certificate_id;
		
	   }
	$flag_set= array_diff($result,$cert_old_id,$cert_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_certificate',$vlaues,'application_certificate_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'application_certificate');
	   foreach($array as $values){
	  
		   $result[]=$values->application_certificate_id;
		
	   }
	
	$flag_set= array_diff($result,$cert_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_certificate',$vlaues,'application_certificate_id');
	
	     }
	    }
	  
	  }
	
	
	}
	
	$insert_data_Additional = array(
	'ref_id' =>!empty($id) ? $id: $insert_id[0]->general_id,
	'quote_id'=>$quote_id,		
	'owner_vehicle' => $this->input->post('owner_vehicle'),
	'insurance_val' => $this->input->post('insurance_val'),
	'ins_Policy_num' => $this->input->post('ins_Policy_num'),
	'ins_Policy_comp' => $this->input->post('ins_Policy_comp'),	
	'effective_from_ins' => $this->input->post('effective_from_ins'),
	'expiration_from_ins' => $this->input->post('expiration_from_ins'),
	'uploaded_files_values_second' => $this->input->post('uploaded_files_values_second'),	
	'owner_license' => $this->input->post('owner_license'),
	'owner_applicant' => $this->input->post('owner_applicant'),
	'applicant_scheduled' => $this->input->post('applicant_scheduled'),
	'applicant_rent' => $this->input->post('applicant_rent'),
	
	);
		
		print_r($insert_data_Additional);
		die;
	 if($application_commodity_id!='')
	{
	$this->_updateapp($id, $insert_data_Additional,'application_additional');
	
	} else {
	//$this->db->insert('application_losses',$insert_data_losses);
	$this->db->insert('application_additional',$insert_data_Additional);
     }
	
	$application_owner_op_id = $this->input->post('application_owner_op_id');
	
	$insert_data_owner_operator = array(
	'ref_id' =>!empty($id) ? $id: $insert_id[0]->general_id,
	'quote_id'=>$quote_id,
	'owner_operator' => $this->input->post('owner_operator'),	
	'broker_loads' => $this->input->post('broker_loads'),
	'inspection__name' => $this->input->post('inspection__name'),
	'if_yes_name' => $this->input->post('if_yes_name'),
	'Estimated_financial' => $this->input->post('Estimated_financial'),	
	'Gross_receipts' => $this->input->post('Gross_receipts'),
	'Estimated_next' => $this->input->post('Estimated_next'),
	);
	
				 if($application_commodity_id!='')
	{
	$this->_updateapp($id, $insert_data_owner_operator,'application_owner_operator');
	
	} else {
	//$this->db->insert('application_losses',$insert_data_losses);
	//print_r($insert_data_owner_operator);
	//die;
	$this->db->insert('application_owner_operator',$insert_data_owner_operator);
     }

	
	}
}
	 // print_r($insert_data_general);
	  
	  //print_r($insert_data_applicant);
		//$this->db->insert('application_Additional',$insert_data_Additional);
function app_vehicle_db($id='') {
	
$insert_id=$this->getid();

$insert_t_id=0;
$quote_id = $this->input->post('quote_id');
	if ($_POST) 
		{
	/*-------------------------------------------------------------------------------------------*/		
			//Truck Section End	
			
			
			$vehicle_id = $this->input->post('vehicle_id');
			
			
			$t_dri_id = $this->input->post('t_dri_id');
			
		
			$select_vehicle_year = $this->input->post('select_vehicle_year');
			$select_model = $this->input->post('select_model');
			$select_GVW = $this->input->post('select_GVW');
			$select_VIN = $this->input->post('select_VIN');
			
			$select_Operation = $this->input->post('select_Operation');
			$select_Radius = $this->input->post('select_Radius');
			$state_driving = $this->input->post('state_driving');
			$select_applicant = $this->input->post('select_applicant');
			
			$select_applicant_driver = $this->input->post('select_applicant_driver');
			$select_ch_radius = $this->input->post('select_ch_radius');
			$select_ch_cities = $this->input->post('select_ch_cities');
			$select_ch_applicant = $this->input->post('select_ch_applicant');
			//print_r($select_ch_radius);
			$select_Cities = $this->input->post('select_Cities');
			
			$select_special = $this->input->post('select_special');
			$select_Axels = $this->input->post('select_Axels');
			$select_num_drivers = $this->input->post('select_num_drivers');
			
			
			$select_equipment = $this->input->post('select_equipment');
			$put_on_truck = $this->input->post('put_on_truck');
		    $pd_damage = $this->input->post('pd_damage');
			$cd_damage = $this->input->post('cd_damage');
	
		
			

			
			
			$coverage_name1 = $this->input->post('coverage_name');	
         
            $coverage_name=!empty($coverage_name1)?  implode(',',$coverage_name1) :'';			
			
			$cargo_avg_val = $this->input->post('cargo_avg_val');
			$cargo_avg_per = $this->input->post('cargo_avg_per');
			$cargo_max_val = $this->input->post('cargo_max_val');
			$cargo_max_per = $this->input->post('cargo_max_per');
			
			
				
			$cargo_factor = $this->input->post('cargo_factor');
			$cargo_prem = $this->input->post('cargo_prem');
			$cargo_deduct = $this->input->post('cargo_deduct');
			
			
			$cargo_amount_limit1 = $this->input->post('cargo_amount_limit');
			$cargo_amount_limit=!empty($cargo_amount_limit1)?  implode(',',$cargo_amount_limit1) :'';
			
			$cargo_amount_deduct1 = $this->input->post('cargo_amount_deduct');
			$cargo_amount_deduct=!empty($cargo_amount_deduct1)?  implode(',',$cargo_amount_deduct1) :'';
			
			$cargo_amount_premium1 = $this->input->post('cargo_amount_premium');
			$cargo_amount_premium=!empty($cargo_amount_premium1)?  implode(',',$cargo_amount_premium1) :'';
		
			$coverage_specify = $this->input->post('coverage_specify');
			$cargo_breakdown_deduct = $this->input->post('cargo_breakdown_deduct');
			$cargo_breakdown_premium = $this->input->post('cargo_breakdown_premium');
			
			
			$garage_address = $this->input->post('garage_address');
			$garage_city = $this->input->post('garage_city');
			$garage_zip = $this->input->post('garage_zip');
			$garage_remarks = $this->input->post('garage_remarks');
			
			
			$truck_new_id=array();
			
		    $truck_old_id=array();
			
			$truck_pd_new_id=array();
			
		    $truck_pd_old_id=array();
			
			$truck_driver_new_id=array();
			
		    $truck_driver_old_id=array();
			
			$insert_v_id=0;
		    $p=1;		
			
			
		
			
			for ($j = 0; $j < count($select_GVW)-1; $j++) {
				
		
			$driver_date_hired = $this->input->post($p.'driver_date_hired');
			$driver_fname = $this->input->post($p.'driver_fname');			
			$driver_mname = $this->input->post($p.'driver_mname');
			$driver_lname = $this->input->post($p.'driver_lname');
			$driver_license_num = $this->input->post($p.'driver_license_num');
			
			$driver_license_class = $this->input->post($p.'driver_license_class');
			$driver_state = $this->input->post($p.'driver_state');
		
			$driver_class_A = $this->input->post($p.'driver_class_A');
			$driver_remarks = $this->input->post($p.'driver_remarks');
			$driver_surcharge = $this->input->post($p.'driver_surcharge');
			$driver_id=$this->input->post($p.'driver_id');	
			$dri_id = $this->input->post($p.'dri_id');	

				if (!empty($select_GVW[$j])) {
					$insert_data2['quote_id'] = $quote_id;
					$insert_data2['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
					$insert_data2['vehicle_type'] = 'TRUCK';
			
					$insert_data2['select_vehicle_year'] = $select_vehicle_year[$j];
					$insert_data2['select_model'] = $select_model[$j];
					$insert_data2['select_GVW'] = $select_GVW[$j];
					$insert_data2['select_VIN'] = $select_VIN[$j];
					$insert_data2['select_Operation'] = $select_Operation[$j];
					
					$insert_data2['select_Radius'] = isset($select_Radius[$j]) ? $select_Radius[$j] : '' ;
					$insert_data2['state_driving'] = isset($state_driving[$j]) ? $state_driving[$j] : '' ;
					$insert_data2['select_applicant'] = isset($select_applicant[$j]) ? $select_applicant[$j] : '' ;
					$insert_data2['select_applicant_driver'] = isset($select_applicant_driver[$j]) ? $select_applicant_driver[$j] : '' ;
					$insert_data2['select_Cities'] = isset($select_Cities[$j]) ? $select_Cities[$j] : '' ;
					
					$insert_data2['select_ch_radius'] = isset($select_ch_radius[$j]) ? $select_ch_radius[$j] : '' ;
					$insert_data2['select_ch_cities'] = isset($select_ch_cities[$j]) ? $select_ch_cities[$j] : '' ;
					$insert_data2['select_ch_applicant'] = isset($select_ch_applicant[$j]) ? $select_ch_applicant[$j] : '' ;
					$insert_data2['select_special'] = isset($select_special[$j]) ? $select_special[$j] : '' ;
					
					$insert_data2['select_Axels'] = isset($select_Axels[$j]) ? $select_Axels[$j] : '' ;
					$insert_data2['select_num_drivers'] = isset($select_num_drivers[$j]) ? $select_num_drivers[$j] : '' ;
					$insert_data2['select_equipment'] = isset($select_equipment[$j]) ? $select_equipment[$j] : '';
					$insert_data2['put_on_truck'] = isset($put_on_truck[$j]) ? $put_on_truck[$j] : '';
					
					$insert_data2['pd_damage'] = isset($pd_damage[$j]) ? $pd_damage[$j] : '';
					
					$insert_data2['cd_damage'] = isset($cd_damage[$j]) ? $cd_damage[$j] : '';
					
					
					$insert_data2['cargo_prem'] = isset($cargo_prem[$j]) ? $cargo_prem[$j] : '';
				
					$insert_data2['coverage_name'] = isset($coverage_name) ? $coverage_name : '';					
					$insert_data2['cargo_avg_val'] = isset($cargo_avg_val[$j])? $cargo_avg_val[$j] : '';
					$insert_data2['cargo_avg_per'] = isset($cargo_avg_per[$j])? $cargo_avg_per[$j] : '';
					$insert_data2['cargo_max_val'] = isset($cargo_max_val[$j])? $cargo_max_val[$j] : '';					
					$insert_data2['cargo_max_per'] = isset($cargo_max_per[$j])? $cargo_max_per[$j] : '';
					
								
					$insert_data2['cargo_deduct'] = isset($cargo_deduct[$j])? $cargo_deduct[$j] : '';
					$insert_data2['cargo_factor'] = isset($cargo_factor[$j])? $cargo_factor[$j] : '';			
					
					
					
					
					$insert_data2['cargo_amount_limit'] = isset($cargo_amount_limit)? $cargo_amount_limit : '';
					$insert_data2['cargo_amount_deduct'] = isset($cargo_amount_deduct)? $cargo_amount_deduct : '';			
					$insert_data2['cargo_amount_premium'] = isset($cargo_amount_premium)? $cargo_amount_premium : '';
					$insert_data2['coverage_specify'] = isset($coverage_specify[$j])? $coverage_specify[$j] : '';
					$insert_data2['cargo_breakdown_deduct'] = isset($cargo_breakdown_deduct[$j])? $cargo_breakdown_deduct[$j] : '';			
					$insert_data2['cargo_breakdown_premium'] = isset($cargo_breakdown_premium[$j])? $cargo_breakdown_premium[$j] : '';
					
					$insert_data2['garage_address'] = isset($garage_address[$j])? $garage_address[$j] : '';
					$insert_data2['garage_city'] = isset($garage_city[$j])? $garage_city[$j] : '';				
					$insert_data2['garage_zip'] = isset($garage_zip[$j])? $garage_zip[$j] : '';
					$insert_data2['garage_remarks'] = isset($garage_remarks[$j])? $garage_remarks[$j] : '';					
					
   			
				if(!empty($vehicle_id[$j])){
					 $this->_updateapp1($id, $insert_data2,'application_vehicle',$vehicle_id[$j],'vehicle_id');
					//echo $vehicle_id[$j].'<pre>';
					
					//die;
		            $truck_old_id[]=$vehicle_id[$j];
					}else{
				
					$this->db->insert('application_vehicle', $insert_data2);
				    $insert_v_id=$this->db->insert_id();
					$truck_new_id[]=$insert_v_id;
					
				}
			}
				
				
				
					

			
			for ($z= 0; $z < count($driver_date_hired); $z++) {
	
			
				if (!empty($driver_date_hired[$z])) {
			        $insert_data25['quote_id'] = $quote_id;
					$insert_data25['vehicle_type'] = "TRUCK";
					$insert_data25['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
		        	$insert_data25['driver_fname'] = isset($driver_fname[$z])? $driver_fname[$z] : '';
					$insert_data25['vehicle_id']=!empty($vehicle_id[$j]) ? $vehicle_id[$j] : $insert_v_id;
					$insert_data25['driver_mname'] = isset($driver_mname[$z])? $driver_mname[$z] : '';				
					$insert_data25['driver_lname'] = isset($driver_lname[$z])? $driver_lname[$z] : '';
					$insert_data25['driver_license_num'] = isset($driver_license_num[$z])? $driver_license_num[$z] : '';				
					$insert_data25['driver_license_class'] = isset($driver_license_class[$z])? $driver_license_class[$z] : '';	
					$insert_data25['driver_state'] = isset($driver_state[$z])? $driver_state[$z] : '';	
					
					$insert_data25['driver_date_hired'] = isset($driver_date_hired[$z])? $driver_date_hired[$z] : '';					
					$insert_data25['driver_class_A'] = isset($driver_class_A[$z])? $driver_class_A[$z] : '';
					$insert_data25['driver_remarks'] = isset($driver_remarks[$z])? $driver_remarks[$z] : '';			
					$insert_data25['driver_surcharge'] = isset($driver_surcharge[$z])? $driver_surcharge[$z] : '';	
			
			
			
					if(!empty($driver_id[$z])){
					$this->_updateapp1($id, $insert_data25,'application_driver',$driver_id[$z],'driver_id');
					$truck_driver_old_id[]=$driver_id[$z];
					}else{
					
					$this->db->insert('application_driver', $insert_data25);
					
					$truck_driver_new_id[]=$this->db->insert_id();
					
					}
				  }
			
				}
			
			
			
			$pd_rate = $this->input->post('pd_rate'.$p);
			
			
			$pd_loss_payee = $this->input->post('pd_loss_payee'.$p);
			
			
			$pd_coverage_specify = $this->input->post('pd_coverage_specify'.$p);
	
			$pd_premium = $this->input->post('pd_premium'.$p);
			$pd_deductible = $this->input->post('pd_deductible'.$p);
		
	
			
		    $pd_loss_name = $this->input->post('pd_loss_name'.$p);
			$pd_loss_address = $this->input->post('pd_loss_address'.$p);
			
			$pd_loss_city = $this->input->post('pd_loss_city'.$p);
			$pd_loss_state = $this->input->post($p.'pd_loss_state');				
			$pd_loss_zip = $this->input->post('pd_loss_zip'.$p);
			$pd_loss_remarks = $this->input->post('pd_loss_remarks'.$p);
			$veh_id = $this->input->post('veh_id'.$p);
			$v_id = $this->input->post('v_id'.$p);
			
			$q=1;
			for ($t = 0; $t < count($pd_rate); $t++) {
			
			
			$pd_coverage11 = $this->input->post('pd_coverage'.$p.$q);
			
			 $pd_coverage = !empty($pd_coverage11)?implode(',',$pd_coverage11):'';
			
			
		
	
		

			
			$pd_amount_limit1 = $this->input->post('pd_amount_limit'.$p.$q);	
    
	        $pd_amount_limit = !empty($pd_amount_limit1)?implode(',',$pd_amount_limit1):'';
			 
			$pd_deductible_limit1 = $this->input->post('pd_deductible_limit'.$p.$q);
			
			$pd_deductible_limit = !empty($pd_deductible_limit1)?implode(',',$pd_deductible_limit1):'';
			
		
			
			$pd_premium_limit1 = $this->input->post('pd_premium_limit'.$p.$q);
	
			
			$pd_premium_limit = !empty($pd_premium_limit1)?implode(',',$pd_premium_limit1):'';
			
				if (!empty($pd_rate[$t])) {
				   $insert_data21['vehicle_id'] =isset($vehicle_id[$j]) ? $vehicle_id[$j] : $insert_v_id; 
					$insert_data21['quote_id'] = $quote_id;
					$insert_data21['vehicle_type'] = 'TRUCK';
					$insert_data21['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
				    $insert_data21['pd_rate'] = isset($pd_rate[$t]) ? $pd_rate[$t] : '';
					
					$insert_data21['pd_coverage'] = isset($pd_coverage) ? $pd_coverage : '';
					
				
					$insert_data21['pd_coverage_specify'] = isset($pd_coverage_specify[$t]) ? $pd_coverage_specify[$t] : '';				
					$insert_data21['pd_deductible'] = isset($pd_deductible[$t]) ? $pd_deductible[$t] : '';
					$insert_data21['pd_premium'] = isset($pd_premium[$t]) ? $pd_premium[$t] : '';
					$insert_data21['pd_loss_payee'] = isset($pd_loss_payee[$t]) ? $pd_loss_payee[$t] : '';	
					
				
					
					$insert_data21['pd_loss_name'] = isset($pd_loss_name[$t]) ? $pd_loss_name[$t] : '';
					$insert_data21['pd_loss_address'] = isset($pd_loss_address[$t]) ? $pd_loss_address[$t] : '';			
					$insert_data21['pd_loss_city'] = isset($pd_loss_city[$t]) ? $pd_loss_city[$t] : '';
					$insert_data21['pd_loss_state'] = isset($pd_loss_state[$t]) ? $pd_loss_state[$t] : '';
					$insert_data21['pd_loss_zip'] = isset($pd_loss_zip[$t]) ? $pd_loss_zip[$t] : '';	
					
					
					$insert_data21['pd_loss_remarks'] = isset($pd_loss_remarks[$t]) ? $pd_loss_remarks[$t] : '';
								
					$insert_data21['pd_amount_limit'] = isset($pd_amount_limit) ? $pd_amount_limit : '';
					$insert_data21['pd_deductible_limit'] = isset($pd_deductible_limit) ? $pd_deductible_limit : '';			
					$insert_data21['pd_premium_limit'] = isset($pd_premium_limit) ? $pd_premium_limit : '';
				
					if(!empty($v_id[$t])){
					$this->_updateapp1($id, $insert_data21,'application_vehicle_pd',$v_id[$t],'v_id');
					$truck_pd_old_id[]=$v_id[$t];
					}else{
					
					$this->db->insert('application_vehicle_pd', $insert_data21);
					$truck_pd_new_id[]=$this->db->insert_id();
					}
					
					  $q++;
				  }
				
				}
				
		
			$p++;
		}	
	
		
if(!empty($truck_old_id)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'application_vehicle','TRUCK');
	   foreach($adresses as $values){
	
	  
		   $result[]=$values->vehicle_id;
		
		 
	   }
	$flag_set= array_diff($result,$truck_old_id,$truck_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle',$vlaues,'vehicle_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'application_vehicle','TRUCK');
	   foreach($array as $values){
	  // print_R($values);
	  
		   $result[]=$values->vehicle_id;
		
		 
	   }
	//$flag_set= array_diff($result,$tel_id);
 
	foreach ($result as $Keys=>$vlaues) {
	
	$flag_set= array_diff($result,$truck_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle',$vlaues,'vehicle_id');
	
	      }
		 }
	    }
	  
	  }
	
	
	}
		
			
	if(!empty($truck_pd_old_id)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'application_vehicle_pd','TRUCK');
	   foreach($adresses as $values){
	
	  
		   $result[]=$values->v_id;
		
		 
	   }
	$flag_set= array_diff($result,$truck_pd_old_id,$truck_pd_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle_pd',$vlaues,'v_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'application_vehicle_pd','TRUCK');
	   foreach($array as $values){
	  // print_R($values);
	  
		   $result[]=$values->v_id;
		
		 
	   }
	//$flag_set= array_diff($result,$tel_id);
 
	$flag_set= array_diff($result,$truck_pd_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle_pd',$vlaues,'v_id');
	
	     }
	    }
	  
	  }
	
	
	}
	
    if(!empty($truck_driver_old_id)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'application_driver','TRUCK');
	   foreach($adresses as $values){
	
	  
		   $result[]=$values->driver_id;
		
		 
	   }
	$flag_set= array_diff($result,$truck_driver_old_id,$truck_driver_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_driver',$vlaues,'driver_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'application_driver','TRUCK');
	   foreach($array as $values){
	  // print_R($values);
	  
		   $result[]=$values->driver_id;
		
		 
	   }
	//$flag_set= array_diff($result,$tel_id);
 
	$flag_set= array_diff($result,$truck_driver_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_driver',$vlaues,'driver_id');
	
	     }
	    }
	  
	  }
	
	
	}
	
		
		
		
				//print_r($insert_data21);
			//Truck Section End
		/*-------------------------------------------------------------------------------------------*/	
		
	    // For TRACTOR
			$t_vehicle_id = $this->input->post('t_vehicle_id');
			
			
		
			$t_select_vehicle_year = $this->input->post('t_select_vehicle_year');
			$t_select_model = $this->input->post('t_select_model');
			$t_select_GVW = $this->input->post('t_select_GVW');
			$t_select_VIN = $this->input->post('t_select_VIN');
			
			$t_select_Operation = $this->input->post('t_select_Operation');
			$t_select_Radius = $this->input->post('t_select_Radius');
			$t_state_driving = $this->input->post('t_state_driving');
			$t_select_applicant = $this->input->post('t_select_applicant');
			
			$t_select_applicant_driver = $this->input->post('t_select_applicant_driver');
			$t_select_ch_radius = $this->input->post('t_select_ch_radius');
			$t_select_ch_cities = $this->input->post('t_select_ch_cities');
			$t_select_ch_applicant = $this->input->post('t_select_ch_applicant');
			
			$t_select_special = $this->input->post('t_select_special');
			$t_select_Axels = $this->input->post('t_select_Axels');
			$t_select_num_drivers = $this->input->post('t_select_num_drivers');
			$t_select_Cities = $this->input->post('t_select_Cities');
			
			$t_select_equipment = $this->input->post('t_select_equipment');
			$t_put_on_truck = $this->input->post('t_put_on_truck');
			$t_pd_damage = $this->input->post('t_pd_damage');
			$t_cd_damage = $this->input->post('t_cd_damage');
		
			
			$t_coverage_name1 = $this->input->post('t_coverage_name');	
			//print_r($t_coverage_name1);
			//die;
            $t_coverage_name=!empty($t_coverage_name1)?  implode(',',$t_coverage_name1) :'';
			
			$t_cargo_avg_val = $this->input->post('t_cargo_avg_val');
			$t_cargo_avg_per = $this->input->post('t_cargo_avg_per');
			$t_cargo_max_val = $this->input->post('t_cargo_max_val');
			$t_cargo_max_per = $this->input->post('t_cargo_max_per');
			
			
				
			$t_cargo_factor = $this->input->post('t_cargo_factor');
			$t_cargo_prem = $this->input->post('t_cargo_prem');
			$t_cargo_deduct = $this->input->post('t_cargo_deduct');
			
			
			$t_cargo_amount_limit1 = $this->input->post('t_cargo_amount_limit');
			$t_cargo_amount_limit=!empty($t_cargo_amount_limit1)?  implode(',',$t_cargo_amount_limit1) :'';
			
			$t_cargo_amount_deduct1 = $this->input->post('t_cargo_amount_deduct');
			$t_cargo_amount_deduct=!empty($t_cargo_amount_deduct1)?  implode(',',$t_cargo_amount_deduct1) :'';
			
			$t_cargo_amount_premium1 = $this->input->post('t_cargo_amount_premium');
			$t_cargo_amount_premium=!empty($t_cargo_amount_premium1)?  implode(',',$t_cargo_amount_premium1) :'';
			
			$t_coverage_specify = $this->input->post('t_coverage_specify');
			
		
			$t_garage_address = $this->input->post('t_garage_address');			
			$t_garage_city = $this->input->post('t_garage_city');
			$t_garage_zip = $this->input->post('t_garage_zip');
			$t_garage_remarks = $this->input->post('t_garage_remarks');
		
			
			
			
			$b=1;
			$tractor_old_id=array();
			$tractor_new_id=array();
			
			$tractor_pd_new_id=array();
			
		    $tractor_pd_old_id=array();
			
			$tractor_driver_new_id=array();
			
		    $tractor_driver_old_id=array();
			
	        $trailer_old_id=array();
			$trailer_new_id=array();
		
			for ($k = 0; $k < count($t_select_GVW); $k++) {
   
				if ($t_select_GVW[$k] != '') {
					$insert_data3['quote_id'] = $quote_id;
					$insert_data3['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
					$insert_data3['vehicle_type'] = 'TRACTOR';
			
					$insert_data3['select_vehicle_year'] = $t_select_vehicle_year[$k];
					$insert_data3['select_model'] = $t_select_model[$k];
					$insert_data3['select_GVW'] = $t_select_GVW[$k];
					$insert_data3['select_VIN'] = $t_select_VIN[$k];
					$insert_data3['select_Operation'] = $t_select_Operation[$k];
					
					$insert_data3['select_Radius'] = $t_select_Radius[$k];
					$insert_data3['state_driving'] = $t_state_driving[$k];
					$insert_data3['select_applicant'] = $t_select_applicant[$k];
					$insert_data3['select_applicant_driver'] = $t_select_applicant_driver[$k];
					$insert_data3['cargo_prem'] = $t_cargo_prem[$k];
					
					$insert_data3['select_ch_radius'] = isset($t_select_ch_radius[$k]) ? $t_select_ch_radius[$k] : '' ;
					$insert_data3['select_ch_cities'] = isset($t_select_ch_cities[$k]) ? $t_select_ch_cities[$k] : '' ;
					$insert_data3['select_ch_applicant'] = isset($t_select_ch_applicant[$k]) ? $t_select_ch_applicant[$k] : '' ;
					$insert_data3['select_special'] = isset($t_select_special[$k]) ? $t_select_special[$k] : '' ;
					$insert_data3['select_Cities'] = isset($t_select_Cities[$k]) ? $t_select_Cities[$k] : '' ;
					$insert_data3['select_Axels'] = isset($t_select_Axels[$k]) ? $t_select_Axels[$k] : '' ;
					$insert_data3['select_num_drivers'] = isset($t_select_num_drivers[$k]) ? $t_select_num_drivers[$k] : '' ;
					$insert_data3['select_equipment'] = isset($t_select_equipment[$k]) ? $t_select_equipment[$k] : '';
					$insert_data3['put_on_truck'] = isset($t_put_on_truck[$k]) ? $t_put_on_truck[$k] : '';
			
					$insert_data3['pd_damage'] = isset($t_pd_damage[$k]) ? $t_pd_damage[$k] : '';
					$insert_data3['cd_damage'] = isset($t_cd_damage[$k]) ? $t_cd_damage[$k] : '';
					
					$insert_data3['coverage_name'] = isset($t_coverage_name) ? $t_coverage_name : '';					
					$insert_data3['cargo_avg_val'] = isset($t_cargo_avg_val[$k])? $t_cargo_avg_val[$k] : '';
					$insert_data3['cargo_avg_per'] = isset($t_cargo_avg_per[$k])? $t_cargo_avg_per[$k] : '';
					$insert_data3['cargo_max_val'] = isset($t_cargo_max_val[$k])? $t_cargo_max_val[$k] : '';					
					$insert_data3['cargo_max_per'] = isset($t_cargo_max_per[$k])? $t_cargo_max_per[$k] : '';
					
								
					$insert_data3['cargo_deduct'] = isset($t_cargo_deduct[$k])? $t_cargo_deduct[$k] : '';
					$insert_data3['cargo_factor'] = isset($t_cargo_factor[$k])? $t_cargo_factor[$k] : '';			
		
					
					
					
					$insert_data3['cargo_amount_limit'] = isset($t_cargo_amount_limit)? $t_cargo_amount_limit : '';
					$insert_data3['cargo_amount_deduct'] = isset($t_cargo_amount_deduct)? $t_cargo_amount_deduct : '';			
					$insert_data3['cargo_amount_premium'] = isset($t_cargo_amount_premium)? $t_cargo_amount_premium : '';
					$insert_data3['coverage_specify'] = isset($t_coverage_specify[$k])? $t_coverage_specify[$k] : '';
			

					
					$insert_data3['garage_address'] = isset($t_garage_address[$k]) ? $t_garage_address[$k] : '';
					$insert_data3['garage_city'] = isset($t_garage_city[$k]) ? $t_garage_city[$k] : '';					
					$insert_data3['garage_zip'] = isset($t_garage_zip[$k]) ? $t_garage_zip[$k] : '';
					$insert_data3['garage_remarks'] = isset($t_garage_remarks[$k]) ? $t_garage_remarks[$k] : '';					
			
					if(!empty($t_vehicle_id[$k])){
					$this->_updateapp1($id, $insert_data3,'application_vehicle',$t_vehicle_id[$k],'vehicle_id');
					//print_r($insert_data3);
					$tractor_old_id[]=$t_vehicle_id[$k];
					}else{
					
					$this->db->insert('application_vehicle', $insert_data3);
					$insert_t_id=$this->db->insert_id();
					$tractor_new_id[]=$insert_t_id;
			          	}
					
					}
				
						
			
			$t_driver_date_hired = $this->input->post($b.'t_driver_date_hired');
			
			$t_driver_fname = $this->input->post($b.'t_driver_fname');
			
				
			$t_driver_mname = $this->input->post($b.'t_driver_mname');
			$t_driver_lname = $this->input->post($b.'t_driver_lname');
			$t_driver_license_num = $this->input->post($b.'t_driver_license_num');
			
			$t_driver_license_class = $this->input->post($b.'t_driver_license_class');
			
			$t_driver_class_A = $this->input->post($b.'t_driver_class_A');
			$t_driver_remarks = $this->input->post($b.'t_driver_remarks');
			$t_driver_surcharge = $this->input->post($b.'t_driver_surcharge');
			$t_driver_id=$this->input->post($b.'t_driver_id');
			$t_driver_state=$this->input->post($b.'t_driver_state');
			
			for ($z= 0; $z < count($t_driver_date_hired); $z++) {
			
				if ($t_driver_date_hired[$z] != '') {
			        $insert_data26['quote_id'] = $quote_id;
					$insert_data26['vehicle_type'] = "TRACTOR";
					$insert_data26['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
		        	$insert_data26['driver_fname'] = isset($t_driver_fname[$z])? $t_driver_fname[$z] : '';
					$insert_data26['vehicle_id']=!empty($t_vehicle_id[$k]) ? $t_vehicle_id[$k] : $insert_t_id;
					$insert_data26['driver_mname'] = isset($t_driver_mname[$z])? $t_driver_mname[$z] : '';				
					$insert_data26['driver_lname'] = isset($t_driver_lname[$z])? $t_driver_lname[$z] : '';
					$insert_data26['driver_license_num'] = isset($t_driver_license_num[$z])? $t_driver_license_num[$z] : '';				
					$insert_data26['driver_license_class'] = isset($t_driver_license_class[$z])? $t_driver_license_class[$z] : '';	
					$insert_data26['driver_state'] = isset($t_driver_state[$z])? $t_driver_state[$z] : '';	
				
					$insert_data26['driver_date_hired'] = isset($t_driver_date_hired[$z])? $t_driver_date_hired[$z] : '';					
					$insert_data26['driver_class_A'] = isset($t_driver_class_A[$z])? $t_driver_class_A[$z] : '';
					$insert_data26['driver_remarks'] = isset($t_driver_remarks[$z])? $t_driver_remarks[$z] : '';			
					$insert_data26['driver_surcharge'] = isset($t_driver_surcharge[$z])? $t_driver_surcharge[$z] : '';	
			
					if(!empty($t_driver_id[$z])){
					$this->_updateapp1($id, $insert_data26,'application_driver',$t_driver_id[$z],'driver_id');
					$tractor_driver_old_id[]=$t_driver_id[$z];
					}else{
					
					$this->db->insert('application_driver', $insert_data26);
					$tractor_driver_new_id[]=$this->db->insert_id();
					
					}
				  }
				}
				
				
			$t_pd_loss_payee = $this->input->post('t_pd_loss_payee'.$b);
					
			$a=1;
			
			$t_pd_rate = $this->input->post('t_pd_rate'.$b);		
			$t_pd_premium = $this->input->post('t_pd_premium'.$b);
			$t_pd_deductible = $this->input->post('t_pd_deductible'.$b);

			
			$t_pd_loss_name = $this->input->post('t_pd_loss_name'.$b);
			$t_pd_loss_address = $this->input->post('t_pd_loss_address'.$b);
			
			$t_pd_loss_city = $this->input->post('t_pd_loss_city'.$b);
			$t_pd_loss_state = $this->input->post($b.'t_pd_loss_state');				
			$t_pd_loss_zip = $this->input->post('t_pd_loss_zip'.$b);
			$t_pd_loss_remarks = $this->input->post('t_pd_loss_remarks'.$b);
			$t_pd_coverage_specify = $this->input->post('t_pd_coverage_specify'.$b);		
		
			$t_v_id = $this->input->post('t_v_id'.$b);	
			$t_veh_id = $this->input->post('t_veh_id'.$b);	
			
			for ($q = 0; $q < count($t_pd_rate); $q++) {
			
		
			$t_pd_coverage11 = $this->input->post('t_pd_coverage'.$b.$a);
			
			$t_pd_coverage = !empty($t_pd_coverage11)?implode(',',$t_pd_coverage11):'';
			
			$t_pd_amount_limit1 = $this->input->post('t_pd_amount_limit'.$b.$a);	
     
			 $t_pd_amount_limit = !empty($t_pd_amount_limit1)?implode(',',$t_pd_amount_limit1):'';
			 
			$t_pd_deductible_limit1 = $this->input->post('t_pd_deductible_limit'.$b.$a);
			
			$t_pd_deductible_limit = !empty($t_pd_deductible_limit1)?implode(',',$t_pd_deductible_limit1):'';
		
			$t_pd_premium_limit1 = $this->input->post('t_pd_premium_limit'.$b.$a);
			
	     	$t_pd_premium_limit = !empty($t_pd_premium_limit1)?implode(',',$t_pd_premium_limit1):'';
			
				if ($t_pd_rate[$q] != '') {
					$insert_data31['vehicle_id'] =isset($t_vehicle_id[$k]) ? $t_vehicle_id[$k] : $insert_t_id;
					$insert_data31['quote_id'] = $quote_id;
					$insert_data31['vehicle_type'] = 'TRACTOR';
					$insert_data31['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
				    $insert_data31['pd_rate'] = isset($t_pd_rate[$q]) ? $t_pd_rate[$q] : '';
					$insert_data31['pd_coverage'] = isset($t_pd_coverage) ? $t_pd_coverage : '';				
					$insert_data31['pd_coverage_specify'] = isset($t_pd_coverage_specify[$q]) ? $t_pd_coverage_specify[$q] : '';				
					$insert_data31['pd_deductible'] = isset($t_pd_deductible[$q]) ? $t_pd_deductible[$q] : '';
					$insert_data31['pd_premium'] = isset($t_pd_premium[$q]) ? $t_pd_premium[$q] : '';
					$insert_data31['pd_loss_payee'] = isset($t_pd_loss_payee[$q]) ? $t_pd_loss_payee[$q] : '';	
					$insert_data31['pd_amount_limit'] = isset($t_pd_amount_limit) ? $t_pd_amount_limit : '';
					$insert_data31['pd_deductible_limit'] = isset($t_pd_deductible_limit) ? $t_pd_deductible_limit : '';			
					$insert_data31['pd_premium_limit'] = isset($t_pd_premium_limit) ? $t_pd_premium_limit : '';
					
					$insert_data31['pd_loss_name'] = isset($t_pd_loss_name[$q]) ? $t_pd_loss_name[$q] : '';
					$insert_data31['pd_loss_address'] = isset($t_pd_loss_address[$q]) ? $t_pd_loss_address[$q] : '';			
					$insert_data31['pd_loss_city'] = isset($t_pd_loss_city[$q]) ? $t_pd_loss_city[$q] : '';
					$insert_data31['pd_loss_state'] = isset($t_pd_loss_state[$q]) ? $t_pd_loss_state[$q] : '';
					$insert_data31['pd_loss_zip'] = isset($t_pd_loss_zip[$q]) ? $t_pd_loss_zip[$q] : '';	
					$insert_data31['pd_loss_remarks'] = isset($t_pd_loss_remarks[$q]) ? $t_pd_loss_remarks[$q] : '';
					
			
					if(!empty($t_v_id[$q])){
					$this->_updateapp1($id, $insert_data31,'application_vehicle_pd',$t_v_id[$q],'v_id');
					$tractor_pd_old_id[]=$t_v_id[$q];
					}else{
					
					$this->db->insert('application_vehicle_pd', $insert_data31);
					
					$tractor_pd_new_id[]=$this->db->insert_id();
					}
				
				  }
				  $a++;
				}
			
						 
			$tr_vehicle_id = $this->input->post('tr_vehicle_id');
			$tr_select_vehicle_year = $this->input->post('tr_select_vehicle_year');
			$tr_select_model = $this->input->post('tr_select_model');
			$tr_select_GVW = $this->input->post('tr_select_GVW');
			$tr_select_VIN = $this->input->post('tr_select_VIN');
			
			$tr_select_Operation = $this->input->post('tr_select_Operation');
			$tr_select_Radius = $this->input->post('tr_select_Radius');
			$tr_state_driving = $this->input->post('tr_state_driving');
			$tr_select_applicant = $this->input->post('tr_select_applicant');
			$tr_select_Cities = $this->input->post('tr_select_Cities');
			$tr_select_applicant_driver = $this->input->post('tr_select_applicant_driver');
			$tr_select_ch_radius = $this->input->post('tr_select_ch_radius');
			$tr_select_ch_cities = $this->input->post('tr_select_ch_cities');
			$tr_select_ch_applicant = $this->input->post('tr_select_ch_applicant');
			
			$tr_select_special = $this->input->post('tr_select_special');
			$tr_select_Axels = $this->input->post('tr_select_Axels');
			$tr_select_num_drivers = $this->input->post('tr_select_num_drivers');
			$tr_select_app_other = $this->input->post('tr_select_app_other');
			
			

			
			for ($m = 0; $m < count($tr_select_GVW)-1; $m++) {

				if ($tr_select_vehicle_year[$m] != '') {
					$insert_data4['quote_id'] = $quote_id;
					$insert_data4['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
					$insert_data4['vehicle_type'] = 'TRAILER';
			
					$insert_data4['select_vehicle_year'] = $tr_select_vehicle_year[$m];
					$insert_data4['select_model'] = $tr_select_model[$m];
					$insert_data4['select_GVW'] = $tr_select_GVW[$m];
					$insert_data4['select_VIN'] = $tr_select_VIN[$m];
					$insert_data4['select_Operation'] = $tr_select_Operation[$m];
					
					$insert_data4['select_Radius'] = isset($tr_select_Radius[$m]) ? $tr_select_Radius[$m] : '' ;
					$insert_data4['state_driving'] = isset($tr_state_driving[$m]) ? $tr_state_driving[$m] : '' ;
					$insert_data4['select_applicant'] = isset($tr_select_applicant[$m]) ? $tr_select_applicant[$m] : '' ;
					$insert_data4['select_applicant_driver'] =isset($tr_select_applicant_driver[$m]) ? $tr_select_applicant_driver[$m] : '' ;
					$insert_data4['select_Cities'] = isset($tr_select_Cities[$m]) ? $tr_select_Cities[$m] : '' ;
					
					$insert_data4['select_ch_radius'] = isset($tr_select_ch_radius[$m]) ? $tr_select_ch_radius[$m] : '' ;
					$insert_data4['select_ch_cities'] = isset($tr_select_ch_cities[$m]) ? $tr_select_ch_cities[$m] : '' ;
					$insert_data4['select_ch_applicant'] = isset($tr_select_ch_applicant[$m]) ? $tr_select_ch_applicant[$m] : '' ;
					$insert_data4['select_special'] = isset($tr_select_special[$m]) ? $tr_select_special[$m] : '' ;
					
					$insert_data4['select_Axels'] = isset($tr_select_Axels[$m]) ? $tr_select_Axels[$m] : '' ;
					$insert_data4['select_num_drivers'] = isset($tr_select_num_drivers[$m]) ? $tr_select_num_drivers[$m] : '' ;
					$insert_data4['tr_select_app_other'] = isset($tr_select_app_other[$m]) ? $tr_select_app_other[$m] : '' ;
					
					
					$insert_data4['parent_id'] = isset($t_vehicle_id[$k]) ? $t_vehicle_id[$k] : $insert_t_id;
		
			
							if(!empty($tr_vehicle_id[$m])){
					$this->_updateapp1($id, $insert_data4,'application_vehicle',$tr_vehicle_id[$m],'vehicle_id');
					
					$trailer_old_id[]=$tr_vehicle_id[$m];
					}else{
				
					$this->db->insert('application_vehicle', $insert_data4);
					$trailer_old_id[]=$this->db->insert_id();
				         }
					
				
					
					}
				}   
					$b++;
			}

			
		
    if(!empty($tractor_old_id)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'application_vehicle','TRACTOR');
	   foreach($adresses as $values){
	
		   $result[]=$values->vehicle_id;
		
	   }
	$flag_set= array_diff($result,$tractor_old_id,$tractor_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle',$vlaues,'vehicle_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'application_vehicle','TRACTOR');
	   foreach($array as $values){
	  
		   $result[]=$values->vehicle_id;
		
	   }
	
	$flag_set= array_diff($result,$tractor_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle',$vlaues,'vehicle_id');
	
	     }
	    }
	  
	  }
	
	
	}
	
	
	 if(!empty($trailer_old_id)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'application_vehicle','TRAILER');
	   foreach($adresses as $values){
	
	  
		   $result[]=$values->vehicle_id;
		
		 
	   }
	$flag_set= array_diff($result,$trailer_old_id,$trailer_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle',$vlaues,'vehicle_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'application_vehicle','TRAILER');
	   foreach($array as $values){
	  // print_R($values);
	  
		   $result[]=$values->vehicle_id;
		
		 
	   }
	//$flag_set= array_diff($result,$tel_id);
 
	$flag_set= array_diff($result,$trailer_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle',$vlaues,'vehicle_id');
	
	     }
	    }
	  
	  }
	
	
	}


	
	 if(!empty($tractor_pd_old_id)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'application_vehicle_pd','TRACTOR');
	   foreach($adresses as $values){
	
	  
		   $result[]=$values->v_id;
		
		 
	   }
	$flag_set= array_diff($result,$tractor_pd_old_id,$tractor_pd_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle_pd',$vlaues,'v_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'application_vehicle_pd','TRACTOR');
	   foreach($array as $values){
	  // print_R($values);
	  
		   $result[]=$values->v_id;
		
		 
	   }
	//$flag_set= array_diff($result,$tel_id);
 
	$flag_set= array_diff($result,$tractor_pd_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle_pd',$vlaues,'v_id');
	
	     }
	    }
	  
	  }
	
	
	}
	
    if(!empty($truck_driver_old_id)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'application_driver','TRACTOR');
	   foreach($adresses as $values){
	
	  
		   $result[]=$values->driver_id;
		
		 
	   }
	$flag_set= array_diff($result,$tractor_driver_old_id,$tractor_driver_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_driver',$vlaues,'driver_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'application_driver','TRACTOR');
	   foreach($array as $values){
	 
	  
		   $result[]=$values->driver_id;
		
		 
	   }
	
 
	$flag_set = array_diff($result,$tractor_driver_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_driver',$vlaues,'driver_id');
	
	     }
	    }
	  
	  }
	
	
	}
	

			
			$tr_vehicle_id1 = $this->input->post('tr_vehicle_id1');	
			
			
			
		 	$tr_select_vehicle_year = $this->input->post('tr_select_vehicle_year1');
			$tr_select_model = $this->input->post('tr_select_model1');
			$tr_select_GVW = $this->input->post('tr_select_GVW1');
			$tr_select_VIN = $this->input->post('tr_select_VIN1');
			
			
			$tr_select_Operation = $this->input->post('tr_select_Operation1');
			$tr_select_Radius = $this->input->post('tr_select_Radius1');
			$tr_state_driving = $this->input->post('tr_state_driving1');
			$tr_select_applicant = $this->input->post('tr_select_applicant1');
			$tr_select_Cities = $this->input->post('tr_select_Cities1');
			$tr_select_applicant_driver = $this->input->post('tr_select_applicant_driver1');
			$tr_select_ch_radius = $this->input->post('tr_select_ch_radius1');
			$tr_select_ch_cities = $this->input->post('tr_select_ch_cities1');
			$tr_select_ch_applicant = $this->input->post('tr_select_ch_applicant1');
			
			$tr_select_special = $this->input->post('tr_select_special1');
			$tr_Pre_drivers = $this->input->post('tr_Pre_drivers1');
			$tr_select_num_drivers = $this->input->post('tr_select_num_drivers1');
			
			$tr_price_drivers = $this->input->post('tr_price_drivers1');
			
			$tr_select_Axels = $this->input->post('tr_select_Axels1');
			
            $trailer_old_id1=array();
			$trailer_new_id1=array();
			
			for ($d = 0; $d < count($tr_select_GVW); $d++) {

				if ($tr_select_vehicle_year[$d] != '') {
					$insert_data41['quote_id'] = $quote_id;
					$insert_data41['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
					$insert_data41['vehicle_type'] = 'TRAILER_SEP';
			
			     	$insert_data41['select_vehicle_year'] = $tr_select_vehicle_year[$d];
					$insert_data41['select_model'] = $tr_select_model[$d];
					$insert_data41['select_GVW'] = $tr_select_GVW[$d];
					$insert_data41['select_VIN'] = $tr_select_VIN[$d];
					$insert_data41['select_Operation'] = $tr_select_Operation[$d];
					
					$insert_data41['select_Radius'] = isset($tr_select_Radius[$d]) ? $tr_select_Radius[$d] : '' ;
					$insert_data41['state_driving'] = isset($tr_state_driving[$d]) ? $tr_state_driving[$d] : '' ;
					$insert_data41['select_applicant'] = isset($tr_select_applicant[$d]) ? $tr_select_applicant[$d] : '' ;
					$insert_data41['select_applicant_driver'] =isset($tr_select_applicant_driver[$d]) ? $tr_select_applicant_driver[$d] : '' ;
					$insert_data41['select_Cities'] = isset($tr_select_Cities[$d]) ? $tr_select_Cities[$d] : '' ;
					
					$insert_data41['select_ch_radius'] = isset($tr_select_ch_radius[$d]) ? $tr_select_ch_radius[$d] : '' ;
					$insert_data41['select_ch_cities'] = isset($tr_select_ch_cities[$d]) ? $tr_select_ch_cities[$d] : '' ;
					$insert_data41['select_ch_applicant'] = isset($tr_select_ch_applicant[$d]) ? $tr_select_ch_applicant[$d] : '' ;
					$insert_data41['select_special'] = isset($tr_select_special[$d]) ? $tr_select_special[$d] : '' ;
					
					$insert_data41['Pre_drivers'] = isset($tr_Pre_drivers[$d]) ? $tr_Pre_drivers[$d] : '' ;
					$insert_data41['price_drivers'] = isset($tr_price_drivers[$d]) ? $tr_price_drivers[$d] : '' ;
					$insert_data41['select_Axels'] = isset($tr_select_Axels[$d]) ? $tr_select_Axels[$d] : '' ;
		
							if(!empty($tr_vehicle_id1[$d])){
					$this->_updateapp1($id, $insert_data41,'application_vehicle',$tr_vehicle_id1[$d],'vehicle_id');
			        $trailer_old_id1[]=$tr_vehicle_id1[$d];
					}else{
					
					$this->db->insert('application_vehicle', $insert_data41);
					$trailer_new_id1[]=$this->db->insert_id();
				        }
		
					}
				}   
				
 if(!empty($trailer_old_id1)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'application_vehicle','TRAILER_SEP');
	   foreach($adresses as $values){
	
	  
		   $result[]=$values->vehicle_id;
		
		 
	   }
	$flag_set= array_diff($result,$trailer_old_id1,$trailer_new_id1);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle',$vlaues,'vehicle_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'application_vehicle','TRAILER_SEP');
	   foreach($array as $values){
	  // print_R($values);
	  
		   $result[]=$values->vehicle_id;
		
		 
	   }
	//$flag_set= array_diff($result,$tel_id);
 
	$flag_set= array_diff($result,$trailer_new_id1);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'application_vehicle',$vlaues,'vehicle_id');
	
	     }
	    }
	  
	  }
	
	
	}
	
				
				
   $loaded__id = $this->input->post('loadedid');	
  
	//die;		
	$insert_data['Vehicle_declined'] = $this->input->post('Vehicle_declined');
	$insert_data['Vehicle_hauling'] = $this->input->post('Vehicle_hauling');
	
	$insert_data['quote_id'] = $quote_id;
	$insert_data['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;			
	$insert_data['loaded_trucks_name'] = $this->input->post('loaded_trucks_name');
	$insert_data['freight_amount'] = $this->input->post('freight_amount');
	$insert_data['other_amount'] = $this->input->post('other_amount');
	$insert_data['clause_amount'] = $this->input->post('clause_amount');
	$insert_data['uploaded_files_values_div'] = $this->input->post('uploaded_files_values_div');
	
	
	if($loaded__id!=''){
	  $this->_updateapp($id, $insert_data,'application_loaded');
	  
		}else{
			
			$this->db->insert('application_loaded', $insert_data);
			
			}
	
	$cover_id = $this->input->post('cover_id');	
	
	$insert_data5['quote_id'] = $quote_id;
	$insert_data5['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;		
	$insert_data5['primary_cover_name'] = $this->input->post('primary_cover_name');
	$insert_data5['combined_amount'] = $this->input->post('combined_amount');
	$insert_data5['combined_deductible'] = $this->input->post('combined_deductible');
	$insert_data5['each_person'] = $this->input->post('each_person');
	$insert_data5['each_accident'] = $this->input->post('each_accident');
	$insert_data5['each_property'] = $this->input->post('each_property');
	$insert_data5['split_deductible'] = $this->input->post('split_deductible');
	$insert_data5['split_PIP'] = $this->input->post('split_PIP');
	$insert_data5['combined_name'] = $this->input->post('combined_name');
 
	if($cover_id!=''){
	  $this->_updateapp($id, $insert_data5,'application_coverage');
		}else{
			
			$this->db->insert('application_coverage', $insert_data5);
			
			}
		
	
	}
		
	}	
	
	
	
	
	
	public function get_broker_losses($id=''){
	$this->db->select('*');
	$this->db->from('rq_rqf_quote_losses');
	if($id!='')
	{
		$this->db->where('quote_id', $id);
	}
	
	$this->db->order_by('quote_id', 'desc');
	//$this->db->limit('1');
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
	
	}
	public function getid($id='')
{
	//echo "hello";
	
	$this->db->select('*');
	$this->db->from('application_general');
	if($id!='')
	{
		$this->db->where('general_id', $id);
	}
	
	$this->db->order_by('general_id', 'desc');
	$this->db->limit('1');
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
}
function get_data($id=''){
    $this->db->select('*');
	$this->db->from('application_general');
	if($id!='')
	{
		$this->db->where('general_id', $id);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;

}
public function get_data_applicant($id=''){

  $this->db->select('*');
	$this->db->from('application_applicant');
	if($id!='')
	{
		$this->db->where('ref_id', $id);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;

}
public function get_data_tel_add($id=''){

  $this->db->select('*');
	$this->db->from('application_tel_add');
	if($id!='')
	{
		$this->db->where('ref_id', $id);
		$this->db->where('is_delete', null);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;

}


public function get_data_email_add($id=''){

  $this->db->select('*');
	$this->db->from('application_email_add');
	if($id!='')
	{
		$this->db->where('ref_id', $id);
		$this->db->where('is_delete', null);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;

}
	//print_r($insert_data_applicant);
	
	
 function get_losses_db($id=''){
	$this->db->select('*');
	$this->db->from('application_losses');
	if($id!='')
	{
		$this->db->where('ref_id', $id);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
	
	}
	
function get_quote_losses_db($id=''){
	$this->db->select('*');
	$this->db->from('application_quote_losses');
	if($id!='')
	{
		$this->db->where('ref_id', $id);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
	
	}
	
	
}


	













/* End of file quote_model.php */
/* Location: ./application/models/quote_model.php */
