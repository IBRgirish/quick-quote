<fieldset class="loses_information_box">
<div ng-controller="lossesCtrl">
	<legend >Loss(es) information</legend>
	<div class="row-fluid " >
		<div class="span12" >
		<label >Disclaimer: You do not need to fill in the loss info if you are uploading the loss runs.</label>
		</div>
	</div>
	<div class="row-fluid">
    <div class="span12 lightblue">
    <span class="pull-left">
        <label>Loss/Losses in the last three years ?</label>
        </span>
       
       <span class="pull-left">
        <select  required="required" ng-change="losses_need_value(losses_need)" ng-model="losses_need" class="hear_about_us myclass span12" >
            <option value="">--Select--</option>
            <option value="yes" >YES</option>
            <option value="no">NO</option>
        </select>
        </span>
    </div>

<div class="row-fluid " style="display:none;" id="losses_inform">
   <div class="row-fluid">
		<div class="row-fluid" id="lia_losses">
			<div class="span6" id="lia_losses_div">
		

			<div class="span4 lightblue losses_head" ><p><b> Liability </b></p></div>
			<button class="label label-info" type="button" id="liability_open" lia_item="open" onclick="show_losses_lia(this)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;" >
			<label class="open_btn" id="liability_text" >Click To Open</label></button>


			
			</div> 
           
			<div >
				<div class="row-fluid "  id="liability_div" style="display:none">
					<div class="span12"  >
						<div class="span4">
						Number of losses in last 3 years ? 
                       
						<input type="text"  ng-change="add_losses('liability')"  ng-model="liability" class="ssss span2 number_limit myclass" value="" />
						
                        </div>
						
						
						
					</div>
				</div>
				                           
				<div class="row-fluid" style="display:none;" id="liabilty_show">  
					<div class="span12 lightblue">
						<table id="_table" class="table table-bordered table-striped table-hover"  >
							<thead> 
							  <tr>
								<th colspan="2" >Period of <br> losses</th>
								<th rowspan="2" > Liability <br> Amount</th>
								<th rowspan="2" >Company</th>
								<th rowspan="2" >General Agent </th>
								<th rowspan="2" >Date of Loss</th>
							  </tr>
							  <tr colspan="2" >
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
					    <tbody id="add__row" ng-repeat="row in rows">                                            
                                                <tr>                
												<td>
												<input type="text" maxlength="10" class="span12 datepick date_from_losses_liability myclass" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" onclick="date12('date_from_losses_liability','date_to_losses_liability')" id="date_from_losses_liability" name="losses_liability_from[]" value=""></td>
                                                <td><input type="text" maxlength="10" id="date_to_losses_liability" onclick="date12('date_from_losses_liability','date_to_losses_liability')" class="span12 datepicker date_to_losses_liability myclass" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" name="losses_liability_to[]" value=""></td>
                                                <td><input type="text" maxlength="15" class="span12 price_format myclass" name="losses_liability_amount[]" value="" ></td>
                                                <td><input type="text" maxlength="15" class="span12 myclass " name="losses_liability_company[]" value=""></td>
                                                <td><input type="text" maxlength="15" class="span12 myclass" name="losses_liability_general_agent[]" value="" ></td>
                                                <td><input type="text" maxlength="15" class="span12 date_from_losses_liability myclass" name="losses_liability_date_of_loss[]" value="" ></td>
                                                </tr> 
                                                
                                               
                                                
   
	</tbody>
                                        </table>
						
					</div>
				</div>
			</div> 
            </div>
		<div class="row-fluid">
	 <div class="span6">
		

			<div class="span4 lightblue losses_head"><p><b>P.D. </b></p></div>
			<button class="label label-info" type="button" id="pd_open" onclick="show_losses_pd(this)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="pd_text">Click To Open</label></button>


			
			</div>
            
			<div id="_div"  >
				<div class="row-fluid " id="pd_div" style="display:none">
					<div class="span12"  >
						<div class="span4">
						Number of losses in last 3 years ? 
                       
						<input type="text" ng-change="add_losses('pd')"  ng-model="pd" class="ssss span2 number_limit myclass" value="" />
						
                        </div>
					
					
					
					</div>
				</div>
				                           
				<div class="row-fluid" style="display:none;" id="pd_show">  
					<div class="span12 lightblue">
						<table id="_table" class="table table-bordered table-striped table-hover"  >
							<thead> 
							  <tr>
								<th colspan="2" >Period of <br> losses</th>
								<th rowspan="2" >P.D.<br> Amount</th>
								<th rowspan="2" >Company</th>
								<th rowspan="2" >General Agent </th>
								<th rowspan="2" >Date of Loss</th>
							  </tr>
							  <tr colspan="2" >
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
					    <tbody id="add__row" ng-repeat="pd_row in pd_rows">
	
             
                                                <tr>                
												<td>
												<input type="text" maxlength="10" class="span12 datepick date_from_losses_pd myclass" onclick="date12('date_from_losses_pd','date_to_losses_pd')" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" id="date_from_losses_pd" name="losses_pd_from[]" value=""></td>
                                                <td><input type="text" maxlength="10" id="date_to_losses_pd" class="span12 datepick date_to_losses_pd" onclick="date12('date_from_losses_pd','date_to_losses_pd')" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" name="losses_pd_to[]" value=""></td>
                                                <td><input type="text" maxlength="15" class="span12 price_format" name="losses_pd_amount[]" value="" ></td>
                                                <td><input type="text" maxlength="15" class="span12 " name="losses_pd_company[]" value=""></td>
                                                <td><input type="text" maxlength="15" class="span12 " name="losses_pd_general_agent[]" value="" >
                                                </td>
                                               <td><input type="text" maxlength="15" class="span12 datepick date_from_losses_pd" name="losses_pd_date_of_loss[]" value="" ></td>
                                                </tr> 
                                                
                                               
  
	 </tbody>
                                        </table>
						
					</div>
				</div>
			</div> 
            </div>
             <div class="row-fluid">
		<div class="span6">
		

			<div class="span4 lightblue losses_head"><p><b>Cargo </b></p></div>
			<button class="label label-info" type="button" id="cargo_open" onclick="show_losses_cargo(this)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="cargo_text">Click To Open</label></button>


			
			</div>
			<div id="_div" >
				<div class="row-fluid " id="cargo_div" style="display:none">
					<div class="span12"  >
						<div class="span4">
						Number of losses in last 3 years ? 
                       
						<input type="text"  ng-change="add_losses('cargo')"  ng-model="cargo" class="ssss span2 number_limit" value="" />
						
                        </div>			
					
						
					</div>
				</div>
				                           
				<div class="row-fluid" style="display:none;" id="cargo_show">  
					<div class="span12 lightblue">
						<table id="_table" class="table table-bordered table-striped table-hover"   >
							<thead> 
							  <tr>
								<th colspan="2" >Period of <br> losses</th>
								<th rowspan="2" > Crgo<br> Amount</th>
								<th rowspan="2" >Company</th>
								<th rowspan="2" >General Agent </th>
								<th rowspan="2" >Date of Loss</th>
							  </tr>
							  <tr colspan="2" >
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
					    <tbody id="add__row" ng-repeat="cargo_row in cargo_rows">
               
                                                <tr>                
												<td>
												<input type="text" maxlength="10" class="span12 datepick date_from_losses_cargo" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" id="date_from_losses_cargo" name="losses_cargo_from[]" onclick="date12('date_from_losses_cargo','date_from_losses_cargo')" value=""></td>
                                                <td><input type="text" maxlength="10" id="date_to_losses_cargo" class="span12 datepick date_to_losses_cargo" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" name="losses_cargo_to[]" value="" onclick="date12('date_from_losses_cargo','date_from_losses_cargo')"></td>
                                                <td><input type="text" maxlength="15" class="span12 price_format" name="losses_cargo_amount[]" value="" ></td>
                                                <td><input type="text" maxlength="15" class="span12 " name="losses_cargo_company[]" value=""></td>
                                                <td><input type="text" maxlength="15" class="span12 " name="losses_cargo_general_agent[]" value="" ></td>
                                                <td><input type="text" maxlength="15" class="span12 datepick date_from_losses_cargo" name="losses_cargo_date_of_loss[]" value="" ></td>
                                                </tr> 
                                                
                                               
                                                
   
	
	                                        </tbody>
                                        </table>
						
					</div>
				</div>
			</div> 
	
			</div>
			 
		
		</div> 
    </div>
 </div>



		
	  


                            
                            	
</fieldset>