<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
?>
<?php
$agency_name = $this->session->userdata('member_name'); 
$characters_only = ' pattern="^[a-zA-Z0-9][a-zA-Z0-9-_\.\ \;\,\*\@\&\)\(\-\=\+\:/]{0,50}$"  ';

//placeholder="MM/DD/YYYY" 
//$date_pattern = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" ';

$date_pattern_js = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ';

/*
  $date_pattern = ' placeholder="MM/DD/YYYY" class="date"  pattern="^([0-9]{2,2})/([0-9]{2,2})/([0-9]{4,4})" ';
 */

$email_pattern = 'pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$"';
$number_pattern = ' pattern="[0-9]+" ';
//$less_100_limit = ' pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" ';
$less_100_limit = ' pattern="([0-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" ';
?>
<?php
//echo includes_dir(); die;
//include(base_url().'includes/functions.php');
//echo date_default_timezone_get();
$date = date('m/d/Y', time());
$time = date('h:i:s a', time());

$quote_id = 1;
//echo '<pre>';
//print_r($user_quote_data);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title> Quick Quote </title>
    <link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
    <script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
    <script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
    <script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script>
    <script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
    <script src="<?= base_url('js/angular.min.js')?>" type="text/javascript"></script>
    <script src="<?= base_url('js/qq_angular.js')?>" type="text/javascript"></script>
  </head>
<body ng-app="qq">
  <h2 style="text-align:center;" >Quick Quote Sheet</h2>
  <div class="container-fluid"> 
	<?php 
    $editid = "";
    $attributes = array('class' => 'request_quote', 'id' => 'main_form');
    echo form_open_multipart(base_url('quote/QuickQuote/'.$editid), $attributes);
    ?> 
    
    <div class="">
	  <?php if (!empty($message)) { ?>
      <div id="message">
		<?php echo $message; ?>
      </div>
      <?php } ?>
      <?php 
	    $this->load->view('quotes_view'); 
		$this->load->view('insured_info'); 
		$this->load->view('coverage_info'); 
      ?>
    </div>
    <?php echo form_close(); ?>
  </div>
<script src="<?php echo base_url('js');?>/jquery.knob.js"></script>
<!-- jQuery File Upload Dependencies -->
<script src="<?php echo base_url('js');?>/jquery.ui.widget.js"></script>
<script src="<?php echo base_url('js');?>/jquery.iframe-transport.js"></script>
<script src="<?php echo base_url('js');?>/jquery.fileupload.js"></script>
<!-- Our main JS file -->
<script src="<?php echo base_url('js');?>/script.js"></script>
</body>
</html>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
$this->load->view('includes/footer');
}
?>


