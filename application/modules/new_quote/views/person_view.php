<fieldset class="insured_info_box ">



                    <!-- Form Name -->
                    <legend>Person submitting business info</legend>

                    <div class="row-fluid" ng-controller="personCtrl">
                    <div class="span12">
                       <div ="row-fluid">
                          <label>Person submitting business</label>
                           <select name="person_state" class="span5">
                                   <option value=""> Select</option>                                 
                                   </select>
                          </div>
                            <div class="row-fluid">  
                                <div class="span3 lightblue">
                                    <label >First name</label>
                                    <input id="" name="fistname" type="text" class="span12 myclass" value=""  maxlength="10"  >
                                </div>
                                <div class="span3 lightblue">
                                    <label >Middle name</label>
                                    <input  name="middlename" type="text" class="span12 myclass"   value="" maxlength="11" >
                                </div>
                                <div class="span3 lightblue">
                                    <label >Last Name</label>
                                    <input  pattern="" id="lastname" name="lastname" type="text" class="span12 myclass" maxlength="10" value="">
                                </div>	
                           </div>
                           <div class="row-fluid">
                                <table id="table" class="tables">
                                    <label >  </label>	
                                    <tr>		
									 <td>Address</td>	
                                     <td>City</td>	
                                     <td>State</td>	
                                     <td>County</td>
                                     <td>Zip</td>	
                                     <td>Country</td>	
                                     
                                     </tr>
                                     <tr >
                                     <td>  <input  id="name" maxlength="80" name="person_address"  type="text"  class="span12 first_name myclass" value="" > </td>
                                     <td><input id="middle_name" maxlength="80"  name="person_city" type="text"  class="span12  myclass" value=""> </td>
                                     <td> 
                                      <select name="person_state" class="span12">
                                   <option value=""> Select</option>                                 
                                   </select> </td>
                                     <td><input id="county" maxlength="80" name="person_county" placeholder="" type="text"  class="span12  myclass" value="" > </td>
                                     <td>  <input id="zip" maxlength="80" name="person_zip" placeholder="" type="text"  class="span12  myclass" value="" ></td>
                                     <td>
                                       <select name="person_country" class="span12">
                                        <option value="">Select</option>                                 
                                        </select>   
                                     </td>
                                    
                                     </tr>
                                     </table>                        
                                    

                            </div>	
							
						
							

                           <div class="row-fluid">
                           
                             <table id="table" class="tables">
                                    <label >  </label>	
                                    <tr>		
									 <td>Phone Type</td>	
                                     <td>Code Type</td>	
                                     <td>Area code</td>	
                                     <td>Prefix</td>
                                     <td>Suffix</td>	
                                     <td>Priority sequence</td>	
                                     <td>	</td>
                                     </tr>
                                     <tr ng-repeat="person_phone in person_phones">
                                     <td>
                                       <select name="person_phone_type" class="span12">
                                       <option value=""> Select</option>                                 
                                       </select>  </td>
                                     <td>
                                     <select name="person_code_type" class="span12">
                                    <option value=""> Select</option>                                 
                                   </select>  
                                     <td> 
                                      <input  id="name" maxlength="80" name="person_area_code"  type="text"  class="span12 first_name myclass" value="" > </td>
                                     <td>
                                     <input id="" maxlength="80"  name="person_prefix" type="text"  class="span12  myclass" value="">
                                  </td>
                                     <td> 
                                   <input id="" maxlength="80"  name="person_suffix" type="text"  class="span12  myclass" value="">
                                     </td>
                                     <td>
                                       <select name="person_country" class="span12">
                                        <option value="">Select</option>                                 
                                        </select>   
                                     </td>                                    
                                   
                                    <td>
                                        <a href="javscript:;" ng-click="addRowPhone('person_phone')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                     <a href="javscript:;" ng-click="remove_row_phone($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                    </tr>
                                    </table>

                            </div>
                            
                            
                            
                            <div class="row-fluid">  
							<table id="table" class="tables">
                                  
                                    <tr>		
									 <td>Email Type</td>	
                                     <td>Email address</td>	
                                     <td>Priority sequence</td>	
                                     <td></td>
                                     </tr>
                                     <tr ng-repeat="person_email in person_emails">
                                      <td>
                                    <select name="person_email_type" class="span12">
                                    <option value=""> Select</option>                                 
                                   </select>    </td>	
                                     <td>
                                     <input id="agency" name="person_email" type="text" class="span12 myclass" maxlength="15" value=""></td>	
                                     <td> 
                                     <select name="person_priority_email" class="span8">
                                     <option value=""> Select</option>                                 
                                     </select>  
                                      </td>	
                                    
                                      <td>
                                             <a href="javscript:;" ng-click="addRowEmail('person_email')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                           <a href="javscript:;" ng-click="remove_row_email($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                     </tr>
                                  </table>
							   
									
                                </div>   

                            <div class="row-fluid">
                              <table id="table" class="tables">
                                    <label >  </label>	
                                    <tr>		
									 <td>Fax Type</td>	
                                     <td>Code Type</td>	
                                     <td>Area code</td>	
                                     <td>Prefix</td>
                                     <td>Suffix</td>	
                                     <td>Priority sequence</td>	
                                     <td>	</td>
                                     </tr>
                                     <tr ng-repeat="person_fax in person_faxs">
                                     <td>
                                       <select name="person_fax_type" class="span12">
                                       <option value=""> Select</option>                                 
                                       </select>  
                                       </td>
                                     <td>
                                     <select name="person_code_type_fax" class="span12">
                                    <option value=""> Select</option>                                 
                                   </select>  
                                     <td> 
                                      <input  id="name" maxlength="80" name="person_area_code_fax"  type="text"  class="span12 first_name myclass" value="" > </td>
                                     <td>
                                     <input id="" maxlength="80"  name="person_prefix_fax" type="text"  class="span12  myclass" value="">
                                  </td>
                                     <td> 
                                   <input id="" maxlength="80"  name="person_suffix_fax" type="text"  class="span12  myclass" value="">
                                     </td>
                                     <td>
                                       <select name="person_country_fax" class="span12">
                                        <option value="">Select</option>                                 
                                        </select>   
                                     </td>                                    
                                   
                                    <td>
                                             <a href="javscript:;" ng-click="addRowFax('person_fax')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                           <a href="javscript:;" ng-click="remove_row_fax($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                    </tr>
                                    </table>
                             							

                            </div>

                            <div class="row-fluid">  
                            
								  <label > Specail remarks </label>
                                     <input id="" maxlength="80"  name="person_special_remark" type="text"  class="span12 middle_name myclass" value="">
							</div>
                          </div>
                        
                       </div>
							</fieldset>