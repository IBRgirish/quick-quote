<fieldset class="insured_info_box">
<legend >Insured info </legend>
<div class="well" ng-controller="insuredCtrl">  	
  <div class="row-fluid" > 
  	<div class="row-fluid">
    	<label>This is a</label>
        <select class="span2 no_margin in_this_is" ng-model="in_this_is" name="in_this_is">
        	<option>Corporation</option>
            <option>Individual</option>
        </select>
    </div>
  	<div class="row-fluid top20">
    	<label ng-if="in_this_is=='Corporation'">Person in charge</label>
        <label ng-if="in_this_is=='Individual'">Insured name</label>
        <label ng-if="!in_this_is">Person in charge / Insured name</label>
    </div>
    
    <div class="row-fluid">
        <div class="span2">
          <label>First name</label>
          <input type="text" maxlength="255" class="span12 first_name myclass" placeholder="First Name" name="insured_name" id="insured_name" required>
        </div>
        <div class="span2 span_10">
          <label>Middle name</label>
          <input type="text"  class="span12 middle_name myclass" name="insured_middle_name" placeholder="Middle Name" id="insured_middle_name" >
        </div>
        <div class="span2">
          <label>last Name</label>
          <input type="text" class="span12 last_name myclass" placeholder="Last Name" name="insured_last_name" id="insured_last_name">
        </div>
        <div class="span3">
          <label>DBA</label>
          <input id="dba" name="dba" type="text" class="span12 myclass" maxlength="255" value=""  >
        </div>
      </div>
      
      <div class="row-fluid">
      	<div class="span2">
        	<label>Address</label>
            <input type="text" name="in_address" class="span12">
        </div>
        <div class="span1">
        	<label>City</label>
            <input type="text" name="in_city" class="span12">
        </div>
        <div class="span1">
        	<label>State</label>
            <select name="in_state" class="span12">
            	<option value="">Select</option>
            </select>
        </div>
        <div class="span2 span_10">
        	<label>County</label>
            <input type="text" name="in_county" class="span12">
        </div>
        <div class="span1">
        	<label>Zip</label>
            <input type="text" name="in_zip" class="span12">
        </div>
        <div class="span2 span_10">
        	<label>Country</label>
            <select name="in_country" class="span12">
            	<option value="">Select</option>
            </select>
        </div>
      </div>
      
      <div class="row-fluid">
      	<label>All vehicle garage in the same address?</label>
        <select name="in_garag_in" class="span1" ng-model="in_garag_in">
        	<option>Yes</option>
            <option selected="selected">No</option>
        </select>
      </div>
      <div class="row-fluid" ng-if="in_garag_in=='No'">      	
          <div class="row-fluid">
              <label>Please fill in all the addresses where the vehicles are garaging:</label>
              <a href="javascript:;" ng-click="add_row('in_garags')"><img src="<?= base_url('images/add.png'); ?>"></a>
          </div>
          <div class="row-fluid" ng-repeat="in_garag_ad in rows.in_garags">
            <div class="row-fluid">
                <div class="span1">
                    <label>Sequence</label>
                    <input type="text" name="in_ga_sequence[]" class="span12">
                </div>
               <div class="span2">
                    <label>Address</label>
                    <input type="text" name="in_ga_address[]" class="span12">
                </div>
                <div class="span1">
                    <label>City</label>
                    <input type="text" name="in_ga_city[]" class="span12">
                </div>
                <div class="span1">
                    <label>State</label>
                    <select name="in_ga_state[]" class="span12">
                        <option value="">Select</option>
                    </select>
                </div>
                <div class="span2 span_10">
                    <label>County</label>
                    <input type="text" name="in_ga_county[]" class="span12">
                </div>
                <div class="span1">
                    <label>Zip</label>
                    <input type="text" name="in_ga_zip[]" class="span12">
                </div>
                <div class="span2 span_10">
                    <label>Country</label>
                    <select name="in_ga_country[]" class="span12">
                        <option value="">Select</option>
                    </select>
                </div>
                <div class="span1">
                	<label class="span12 no_margin">&nbsp;</label>
                	<a href="javascript:;" ng-click="remove_row($index, 'in_garags')"><img src="<?= base_url('images/remove.png')?>"></a>
                </div>
            </div>
        </div>
      </div>
      
      <div class="row-fluid">
        <div class="span2">
          <label>Nature of Business</label>
          <select class="span12">
          
          </select>
        </div>
        <div class="span2 nature_of_business_otr" >
          <label>Specify Other</label>
          <input type="text" name="nature_of_business_other" value="" class="span12" >
        </div>
        <div class="span2 lightblue">
          <label>Commodities Hauled</label>
          <?php echo $commodities_haulted; ?>
        </div>
        <div class="span2 commodities_haulted_otr" >
          <label>Specify Other</label>
          <input type="text" id="commodities_haulted" name="commodities_haulted[]" value="" class="span12" >
        </div>
        <div class="span3 lightblue">
          <label>Years in Business </label>
          <!--change by sulbha -->
          <select id="business_years" name="business_years" class="myclass" onChange="businessYears(this.value);" >
            <option  value="New venture">New venture</option>
            <option  value="1 year" >1 year</option>
            <option   value="2 years">2 years</option>
            <option  value="3 years">3 years</option>
            <option  value="4+ years" >4+ years</option>
          </select>
        </div>
      </div>
    </div>
    
    <!-- Insured filling Section Ashvin Patel 26/April/2015-->
    <legend class="filing_ribbon">
    	<div class="span9 text-center"><div class="row-fluid ">Filing Section</div></div>
        <div class="span3">
          <label>Do you need Filing / Filings :</label>
          <select id="filings_need1" name="filings_need" ng-model="filings_need" class="span1 no_margin">
            <option>Yes</option>
            <option>No</option>
          </select>
        </div>
    </legend>
    <div class="well">
    <div class="row-fluid" ng-if="filings_need=='Yes'">	     
      <div class="span12 lightblue" id="number_of_filings_div">
        <label><a href="javascript:;" ng-click="add_row('filing')"><img src="<?= base_url('images/add.png')?>"></a></label>
      
      </div>
      <div class="row-fluid" id="filings_need_update">  
        <div class="span12 lightblue">
          <table id="filing_table" class="table table-bordered table-striped table-hover filing_table" ng-repeat="filing in rows.filing">
            <thead>
              <tr>
                <th>Filing Type:</th>
                <th >Filing Num</th>
              </tr>
            </thead>
            <tbody id="add_filing_row" >
              <tr > 
                <td> 
				  <?php  echo filing_type_show('', 0);?>
                <!--  <input  id="filing_type_other" type="text" name="filing_type_other[]" class="span12 filing_numbers" value=""/> 	-->	
                </td>
                <td>
                    <input type="text" maxlength="20" class="span6" name="filing_numbers[]" value="">
                    <a href="javascript:;" ng-click="remove_row($index, 'filing')"><img src="<?= base_url('images/remove.png')?>"></a>
                </td>
              </tr>					
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="row-fluid" ng-if="filings_need=='No'">
    	<div class="row-fluid">
        	<label>Under who`s authority you are driving?</label>
        </div>
        <div class="row-fluid" ng-repeat="filing_no in rows.filing_nos">
        	<div class="span2">
            	<label>Filing Number</label>
                <input type="text" name="in_fi_filing_number" class="span12">
            </div>
            <div class="span3">
            	<label>DBA</label>
                <input type="text" name="in_fi_dba" class="span12">
            </div>
            <div class="span2">
            	<label>Address</label>
                <input type="text" name="in_fi_address" class="span12">
            </div>
            <div class="span1 span_10">
            	<label>City</label>
                <input type="text" name="in_fi_city" class="span12">
            </div>
            <div class="span1">
            	<label>State</label>
                <input type="text" name="in_fi_state" class="span12">
            </div>
            <div class="span1 span_8">
            	<label>Zip</label>
                <input type="text" name="in_fi_zip" class="span12">
            </div>
            <div class="span1">
            	<label class="span12 no_margin">&nbsp;</label>
            	<a href="javscript:;" ng-click="add_row('filing_nos')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                <a href="javscript:;" ng-click="remove_row($index, 'filing_nos')"><img src="<?= base_url('images/remove.png'); ?>"></a>
            </div>
        </div>
    </div>
    </div>
<!-- Insured filling Section End Ashvin Patel 26/April/2015-->

<!-- Insured Owner Section Start Ashvin Patel 26/April/2015-->
<legend class="top10 in_owner_ribbon">
	<div class="span9 no_margin">Number of Owner</div>
    <div class="span4 top5 no_margin on_number">
      <label class="top5 pull-left">Number of owner of the vehicle or entity?</label>
      <input required <?php echo $less_100_limit; ?> type="text" id="number_of_owner" name="number_of_owner"  class="span1 pull-right no_margin" ng-change="add_row_('owners')" ng-model="n_owners"/>
    </div>
</legend>
<div class="well">
  <div id="row_owner">
    <div class="row-fluid">  
      <div class="row-fluid">
        <table id="owner_table" class="table table-bordered table-striped table-hover" >
          <thead>
            <tr>
              <th>First Name</th>
              <th>Middle Name</th>
              <th>Last Name</th>
              <th>Driver ?</th>
              <th>Driver License number</th>
              <th>License issue state</th>
            </tr>
          </thead> 
          <tbody id="add_owner_row">
            <tr ng-repeat="owner in rows.owners">
              <td><input type="text" maxlength="20" class="span12 first_name" name="owner_name[]" value=""></td>
              <td><input type="text" maxlength="20" class="span12 middle_name" name="owner_middle_name[]" value=""></td>
              <td><input type="text" maxlength="20" class="span12 last_name" name="owner_last_name[]" value=""></td>            
              <td>
                <select name="owner_is_driver[]" class="span12" >
                  <option value="">SELECT</option>
                  <option value="Y">Yes</option>
                  <option value="N">NO</option>
                </select>
              </td> 
              <td><input type="text" maxlength="20" class="span12" name="owner_license[]" value=""></td> 
              <td>
			  <?php  echo get_state_dropdown('license_issue_state[]', 'CA', 'style="width:70px;" '); ?>
              <a href="javscript:;" ng-click="add_row('owners')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
              <a href="javscript:;" ng-click="remove_row($index, 'owners')"><img src="<?= base_url('images/remove.png'); ?>"></a>
              </td>
            </tr>
          </tbody> 
        </table>
      </div>    
    </div>
  </div>
</div>
<div class="row-fluid">
    <label>Attach MVR`s for all Driver(s) and Owner(s) no mare than 30 days old</label>
    <input type="file" name="driv_own_mvr_file" class="span1 span_8">
</div>
<div class="row-fluid top5">
  <label><b>Disclaimer : </b> Please attach the MVR for each driver, otherwise the quote is an indication only and not a formal quote.</label>
</div>
<!-- Insured Owner Section End Ashvin Patel 26/April/2015-->
</div>
</fieldset>