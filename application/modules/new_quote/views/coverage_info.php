<!-- Coverage Section End Ashvin Patel 26/April/2015-->
<fieldset class="coverage_info_box top10" ng-controller="coverageCtrl">
<legend >Coverage info </legend>
<div class="well">
  <div class="row-fluid">
  	<div class="span2 span_19 top27">
    	<label>Coverage</label>
        <select name="coverage[]" chosen multiple="multiple" ng-model="coverage" class="span8 no_margin" id="coverage">        	
            <option>Liability</option>
            <option>PD</option>
            <option>Cargo</option>
        </select>
    </div>
    <div class="span2 span_19 top27">
    	<label>Vehicle Type</label>
        <select name="vh_type" chosen multiple="multiple"  class="span7 no_margin" id="vh_type">        	
            <option>Truck</option>
            <option>Tractor</option>
            <option>Trailer</option>
            <option>Other</option>
        </select>
    </div>
    <div class="span1 truck_box" style="display:none">
    	<label>Truck</label>
        <input type="text" name="truck_no" class="span12" ng-change="add_row_('{{n_trucks}}', 'trucks')" ng-model="n_trucks">
    </div>
    <div class="span1 tractor_box" style="display:none">
    	<label>Tractor</label>
        <input type="text" name="tractor_no" class="span12" ng-change="add_row_('{{n_tractors}}', 'tractors')" ng-model="n_tractors">
    </div>
    <div class="span1 trailer_box" style="display:none">
    	<label>Trailer</label>
        <input type="text" name="trailer_no" class="span12" ng-change="add_row_('{{n_trailers}}', 'trailers')" ng-model="n_trailers">
    </div>
    <div class="span1 other_box" style="display:none">
    	<label>Other</label>
        <input type="text" name="other_no" class="span12" ng-change="add_row_('{{n_others}}', 'others')" ng-model="n_others">
    </div>
    <div class="span2 other_box" style="display:none">
    	<label>Please specify Other: </label>
        <input type="text" name="other_vh_name" class="span12">
    </div>
  </div> 
  
  <div class="row-fluid">
  	<div class="row-fluid">
      <?php $this->load->view('truck_coverage'); ?>      
      <?php $this->load->view('tractor_coverage'); ?>
      <?php $this->load->view('trailer_coverage'); ?>
      <?php $this->load->view('other_coverage'); ?>
    </div>
  </div>
</div>
</fieldset>

<!-- Coverage Section End Ashvin Patel 26/April/2015-->
<fieldset id="section4" class="schedule_of_drivers_box top20" ng-controller="driverCtrl">
<legend>
<div class="span10 no_margin">Schedule of Drivers</div>
<div class="span2 span_18">
  <label>Number of drivers ?</label>
  <input <?php echo $less_100_limit; ?> required type="text" id="num_of_drivers" name="driver_count" class="span1 no_margin" ng-model="n_drivers" ng-change="add_row_()"  />
  </div>
</legend>
<div class="well">
  <div id="row_driver">
    <div class="row-fluid">  
      <div class="span12 lightblue">
        <table id="driver_table" class="table table-bordered table-striped table-hover" >
          <thead>
            <tr>
              <th >First Name</th>
              <th >Middle Name</th>
              <th >Last Name</th>
              <th >Driver License number</th>
              <th >License Class</th>
              <th >Date Hired</th>
              <th >DOB</th>
              <th >How Many Years Class A license</th>
              <th >License issue state</th>
              <th></th>
            </tr>
          </thead>
          <tbody id="add_driver_row" ng-repeat="row in rows">
            <tr>
              <td><input type="text" maxlength="20" class="span12 first_name" name="driver_name[]" ></td>   
              <td><input type="text" maxlength="20" class="span12 middle_name" name="driver_middle_name[]" ></td>   
              <td><input type="text" maxlength="20" class="span12 last_name" name="driver_last_name[]" ></td>   
              <td><input type="text" maxlength="20" class="span12" name="driver_license[]"  ></td> 
              <td><input type="text" maxlength="20" class="span12" name="driver_license_class[]"  ></td>
              <td><input type="text" maxlength="20" class="span12 datepick date_from_license_issue" name="driver_license_issue_date[]" ></td>
              <td><input type="text" maxlength="20" class="span12 datepick date_from_dob" name="driver_dob[]"  ></td>
              <td><input type="text" maxlength="20" class="span12" name="driver_class_a_years[]"  ></td>
              <td><?php  echo get_state_dropdown('driver_license_issue_state[]', 'CA', 'style="width:70px;" '); ?></td>
              <td>
              	<a href="javscript:;" ng-click="add_row()" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                <a href="javscript:;" ng-click="remove_row($index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
              </td>
            </tr> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</fieldset>