<div class="row-fluid bottom5 other_box" ng-repeat="other in rows.others" ng-init="othersIndex=$index">
		<div class="well">
      	<div class="row-fluid">
        	<div class="row-fluid">
            	<label>Other #{{$index+1}}</label>
            </div>
            <div class="row-fluid">
            	<label>Vehicle Info</label>
            </div>
        </div>
      	<div class="row-fluid">  	
          <div class="span1 span_8">      
            <label>Vehicle Year</label>          
            <select class="span12 myclass" name="vehicle_year_vh[]" id="vehicle_year_vh">
              <?php 
              $year=$vehicle->vehicle_year;
              $current_year = date("Y");
              for($i = 1985; $i <= $current_year + 1; $i++){
                echo "<option value='$i'>$i</option>";
              } 
              ?>
            </select>
          </div>
          <div class="span2 span_8">
            <label> Make Model</label>
            <?php echo $make_model_truck;?>
          </div>
          <div class="span2 lightblue">
            <label>GVW</label>
            <select class="span12 myclass" name="gvw_vh[]" id="gvw_vh">
              <option value="0-10,000 pounds">0-10,000 pounds</option>
              <option value="10,001-26,000 pounds">10,001-26,000 pounds</option>
              <option value="26,001-40,000 pounds">26,001-40,000 pounds</option>
              <option value="40,001-80,000 pounds">40,001-80,000 pounds</option>
              <option value="Over 80,000 pounds">Over 80,000 pounds</option>
            </select>
          </div>
          <div class="span2 lightblue">
            <label>VIN</label>
            <input type="text" maxlength="19" class="span12 myclass" name="vin_vh[]" id="vin_vh" value="">
          </div>
          <!--<div class="span2 lightblue">
            <label>Special req.</label>
            <input type="text" class="span12 myclass" name="value_amount_vh[]" id="value_amount_vh" value="">
          </div>-->
          <div class="span2 span_10">
              <label>Number of Axles</label>
              <?php echo $truck_number_of_axles; ?>             
          </div>
          <div class="span1">
          	<label>Salvage</label>
            <select name="vh_salvage" class="span12">
            	<option value="">Select</option>
            </select>
          </div>
          <div class="span2 lightblue">
            <label>Radius of Operation</label>            
            <select class="span12 rofo truck_radius_of_operation myclass" name="truck_radius_of_operation[]" id="radius_of_operation" onChange="rofoperation2(this.value);" >
                <option value="0-100 miles" >0-100 miles</option>
                <option value="101-500 miles"  >101-500 miles</option>           	
                <option value="501+ miles"  >501+ miles</option>
                <option value="other"  >Other</option>
            </select>    
          </div>
          <div class="span2">
          	<label>Nature of business</label>
            <input type="text" name="vh_nature_of_business" class="span12">
          </div>          
        </div>
        <div class="row-fluid">
        	<div class="span2 span_8">
              <label>Owned Vehicle</label>
              <select name="truck_trailer_owned_vehicle[]" id="trailer_owned_vehicle1-1" ng-model="other.truck_trailer_owned_vehicle" class="span12">
                  <option>Yes</option>
                  <option>No</option>
              </select>
          	</div>
            <div class="span2" ng-if="other.truck_trailer_owned_vehicle=='No'">
            	<label>&nbsp;</label>
            	<label class="span12">Please provide the owner</label>
            </div>
        </div>
        <div class="row-fluid">
        	<div class="row-fluid" ng-if="other.truck_trailer_owned_vehicle=='No'">
            	<div class="span6">
                  <label>Sequence</label>
                  <input type="text" name="vh_owner_sq" class="span1" ng-change="add_loss_row('others', $index)" ng-model="other.lessor_n" >
                </div>
            </div>
            <div class="row-fluid bottom5" ng-repeat="lessor in other.lessor" ng-init="lessorIndex=$index">
            	<div class="well">
            	<div class="row-fluid">
                	<div class="span2 span_10">
                    	<label>Lessor name</label>
                        <input type="text" name="vh_ls_name" class="span12">
                    </div>
                    <div class="span2 span_8">
                    	<label>Middle name</label>
                        <input type="text" name="vh_ls_m_name" class="span12">
                    </div>
                    <div class="span2 span_10">
                    	<label>Last name</label>
                        <input type="text" name="vh_ls_l_name" class="span12">
                    </div>
                    <div class="span3">
                    	<label>Lessor DBA</label>
                        <input type="text" name="vh_ls_dba" class="span12">
                    </div>
                </div>
                <div class="row-fluid">
                	<div class="span3">
                    	<label>Address</label>
                        <input type="text" name="vh_ls_address" class="span12">
                    </div>
                    <div class="span1 span_8">
                    	<label>City</label>
                        <input type="text" name="vh_ls_city" class="span12">
                    </div>
                    <div class="span1">
                    	<label>State</label>
                        <input type="text" name="vh_ls_state" class="span12">
                    </div>
                    <div class="span2 span_10">
                    	<label>County</label>
                        <input type="text" name="vh_ls_county" class="span12">
                    </div>
                    <div class="span1">
                    	<label>Zip</label>
                        <input type="text" name="vh_ls_zip" class="span12">
                    </div>
                    <div class="span2">
                    	<label>Country</label>
                        <input type="text" name="vh_ls_country" class="span12">
                    </div>
                </div>
                <div class="row-fluid">
                	<div class="span2 span_10">
                    	<label>Phone type</label>
                        <select name="vh_ls_ph_type" class="span12">
                        	<option value="">Select</option>
                        </select>
                    </div>
                    <div class="span2 span_10">
                    	<label>Country code</label>
                        <select name="vh_ls_country_code" class="span12">
                        	<option value="">Select</option>
                        </select>
                    </div>
                    <div class="span2 span_8">
                    	<label>Area code</label>
                        <input type="text" name="vh_ls_areacode" class="span12">
                    </div> 
                    <div class="span1">
                    	<label>Prefix</label>
                        <input type="text" name="vh_ls_prefix" class="span12">
                    </div>
                    <div class="span1">
                    	<label>Suffix</label>
                        <input type="text" name="vh_ls_suffixe" class="span12">
                    </div>
                    <div class="span2 span_10">
                    	<label>Priority sequence</label>
                        <select name="vh_ls_ph_pri_sequence" class="span12">
                        	<option value="">Select</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid" ng-repeat="email in lessor.emails">
                	<div class="span2 span_10">
                    	<label>Email Type</label>
                        <select name="vh_ls_email_type" class="span12">
                        	<option value="">Select</option>
                        </select>
                    </div>
                    <div class="span2">
                    	<label>Email Address</label>
                        <input type="text" name="vh_ls_email" class="span12">
                    </div>
                    <div class="span2 span_10">
                    	<label>Priority sequence</label>
                        <select name="vh_ls_em_pri_sequence" class="span12">
                        	<option value="">Select</option>
                        </select>
                    </div>
                    <div class="span1">
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_email('others', othersIndex, lessorIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                		<a href="javscript:;" ng-click="remove_email('others', othersIndex, lessorIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
                </div>
                <div class="row-fluid" ng-repeat="fax in lessor.faxs">
                	<div class="span2 span_10">
                    	<label>Fax Type</label>
                        <select name="vh_ls_fx_type" class="span12">
                        	<option value="">Select</option>
                        </select>
                    </div>
                    <div class="span2 span_10">
                    	<label>Country code</label>
                        <select name="vh_ls_fx_country_code" class="span12">
                        	<option value="">Select</option>
                        </select>
                    </div>
                    <div class="span2 span_8">
                    	<label>Area code</label>
                        <input type="text" name="vh_ls_fx_areacode" class="span12">
                    </div>
                    <div class="span1">
                    	<label>Prefix</label>
                        <input type="text" name="vh_ls_fx_prefix" class="span12">
                    </div>
                    <div class="span1">
                    	<label>Suffix</label>
                        <input type="text" name="vh_ls_fx_suffix" class="span12">
                    </div>
                    <div class="span2 span_10">
                    	<label>Priority sequence</label>
                        <select name="vh_ls_fx_pri_sequence" class="span12">
                        	<option value="">Select</option>
                        </select>
                    </div>
                    <div class="span1">
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_fax('others', othersIndex, lessorIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                		<a href="javscript:;" ng-click="remove_fax('others', othersIndex, lessorIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
                </div>
                </div>
            </div>
            <div class="row-fluid top20">
            	<div class="row-fluid">
                	<label>Do you pull any equipment with this vehicle?</label>
                    <select name="vh_equipment_pull" class="span1 no_margin" ng-model="other.vh_equipment_pull">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                </div>
                <div class="row-fluid top10" ng-if="other.vh_equipment_pull=='Yes'"> 
                	<div class="row-fluid" ng-repeat="vh_eqpmnt in other.vh_eqpmnts">              	
                	<div class="span2 span_10">
                    	<label>Equipment Type</label>
                        <select name="vh_eqp_type" class="span12">
                        	
                        </select>
                    </div>
                    <div class="span1 span_8">
                    	<label>Vehicle Year</label>
                        <select name="vh_eqp_year" class="span12">
                        <?php 
						  $year=$vehicle->vehicle_year;
						  $current_year = date("Y");
						  for($i = 1985; $i <= $current_year + 1; $i++){
							echo "<option value='$i'>$i</option>";
						  } 
						?>
                        </select>
                    </div>
                    <div class="span2 span_10">
                    	<label>Make</label>
						<?php echo $make_model_truck;?>
                    </div>
                    <div class="span2 span_10">
                    	<label>Model</label>
                        <select name="vh_eqp_model" class="span12">
                        	
                        </select>
                    </div>
                    <div class="span2">
                    	<label>GVW</label>
                        <select class="span12" name="gvw_vh[]" id="gvw_vh">
                          <option value="0-10,000 pounds">0-10,000 pounds</option>
                          <option value="10,001-26,000 pounds">10,001-26,000 pounds</option>
                          <option value="26,001-40,000 pounds">26,001-40,000 pounds</option>
                          <option value="40,001-80,000 pounds">40,001-80,000 pounds</option>
                          <option value="Over 80,000 pounds">Over 80,000 pounds</option>
                        </select>
                    </div>
                    <div class="span2">
                    	<label>VIN</label>
                        <input type="text" name="vh_eqp_vin" class="span12">
                    </div>
                    <div class="span2 span_10">
                    	<label>Salvaga</label>
                        <select name="vh_eqp_salvage" class="span12">
                        
                        </select>
                    </div>
                    <div class="span1">
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_eqp('others', othersIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                		<a href="javscript:;" ng-click="remove_eqp('others', othersIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid top20">
            	<div class="row-fluid">
                	<label>Do you need Liability coverage</label>
                    <select name="vh_lib_coverage" class="span1 no_margin" ng-model="other.vh_lib_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                </div>
                <div class="row-fluid top10" ng-if="other.vh_lib_coverage=='Yes'">
                	<div class="span2 span_10">
                    	<label>Liability</label>
                        <select class="span12" name="liability_vh[]" id="liability_vh">
                  			<option value="$1,000,000">$1,000,000</option>
                            <option value="$750,000">$750,000</option>
                            <option value="$500,000">$500,000</option>
                        </select>
                    </div>
                    <div class="span2 span_8">
                        <label>Liability Ded</label>
                        <select class="span12" name="liability_ded_vh[]" id="liability_ded_vh">                        
                            <option value="$1,000">$1,000</option>
                            <option value="$1,500">$1,500</option>
                            <option value="$2,000">$2,000</option>
                            <option value="$2,500">$2,500</option>
                        </select>
                    </div>
                    <div class="span2 span_10">
                     <label>UM</label>
                     <?php
					   $um = '';                                                               
					   $state = 'CA';
					   $att = 'class="span12 ui-state-valid myclass um_changes_option"'; 
					   echo getumcoverages('truck_um[]',$att,$um,$state);
                     ?>
                   </div>
                  <div class="span2 span_10">
                    <label>PIP</label>
                    <?php   
					  $pip = '';                                                              
					  $state = 'CA';
					  $att = 'class="span12 ui-state-valid myclass pip_changes_option"'; 
					  echo getpipcoverages('truck_pip[]',$att,$pip,$state);
                    ?>
                  </div>                                  
                  <div class="span2 span_10">
                    <label>UIM</label>
                    <?php   
					  $uim = '';
					  $state = 'CA';
					  $att = 'class="span12 ui-state-valid myclass uim_changes_option"'; 
					  echo getuimcoverages('truck_uim[]',$att,$uim,$state);
                    ?>
                  </div>
                </div>
            </div>
            <div class="row-fluid top20">
            	<div class="row-fluid">
                	<label>Do you need PD coverage</label>
                    <select name="vh_pd_coverage" class="span1 no_margin" ng-model="other.vh_pd_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                </div>
                <div class="row-fluid top10" ng-if="other.vh_pd_coverage=='Yes'">
                	 <div class="span2 span_10">
                        <label>ACV</label>
                        <input type="text" class="span12 price_format" name="pd_vh[]" id="pd_vh" value="">
                    </div>
                    <div class="span2 span_10">
                        <label>PD Ded</label>
                        <input type="text" class="span12 price_format" name="ph_ded_vh[]" id="ph_ded_vh" value="">
                    </div>
                    <div class="span3">
                    	<label>Remark</label>
                        <input type="text" name="vh_pd_remark" class="span12">
                    </div>
                </div>
            </div>
            <div class="row-fluid top20">
            	<div class="row-fluid">
                	<label>Do you need Cargo coverage</label>
                    <select name="vh_cargo_coverage" class="span1 no_margin" ng-model="other.vh_cargo_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                </div>
                <div class="row-fluid top10" ng-if="other.vh_cargo_coverage=='Yes'">
                	<div class="span2 span_10">
                      <label>Cargo limit</label>
                      <input type="text" class="span12 price_format myclass" name="cargo_vh[]" id="cargo_vh" value="">
                    </div>
                    <div class="span2 span_10">
                        <label>Cargo Ded</label>
                        <input type="text" class="span12 price_format myclass" name="cargo_ded_vh[]" id="cargo_ded_vh" value="">
                    </div>
                    <div class="span3 truck_ _cargo"> 
                    	<label>Refrigeration breakdown:</label>	
                        <input type="checkbox" name="refrigerated_breakdown[]" id="refrigerated_breakdown1" onClick="check_deduct(this.id) ">
                        <div class="refrigerated_breakdown span12">
                         	<label>Deductible : </label>
                            <input type="text" class="span6 price_format" name="deductible[]" id="deductible">
                         </div>
                    </div>
                </div>
                 <div class="row-fluid">
                   <div class="span9">
                     <label>Commodities Hauled</label>
                     <input type="text" name="commodities_haulted_truck[]"  class="span12 commhauled2 ui-state-valid myclass">  
                     <label>if you are requesting cargo coverage, please be as specific as possible, For Example, Do not list Dry Goods, General Freight or Dry Freight </label>
                   </div>   
                 </div>
             </div>
        </div>
        </div>
      </div>