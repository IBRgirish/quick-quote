<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
	$flash_msg = '';
	if ($this->session->flashdata('success')){    
		$flash_msg = '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; 
	} elseif ($this->session->flashdata('error')) {
		$flash_msg = '<div class="alert alert-error">'.$this->session->flashdata('error').'</div>';
	}
	
	
?>
<html>
<body>

<div class="row">
    <div class="span12">
		<?php 
		echo $flash_msg;
		//echo $flash_msg; ?>
    </div>
</div>


</body>
</html>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>