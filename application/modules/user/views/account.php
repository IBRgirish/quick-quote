<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');
	
$flash_success = '';
if ($this->session->flashdata('error')){    
		//echo '<div class="error"><p>'.$this->session->flashdata('error').'</p></div>';
		$flash_error = $this->session->flashdata('error');
	}
	if ($this->session->flashdata('success')){    
		$flash_success = '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; 
	}
	// Validation Errors
	  $error_message = (validation_errors()?'<div class="error">'.validation_errors().'</div>':'');
	  
    if(!empty ($error_message)) {
		$error_message_div = '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$error_message.'</div>';
	} else {
		$error_message_div = '';
	}	
	
    if(!empty ($flash_error)) {
		$error_message_div .= '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$flash_error.'</div>';
	} 

}
?>

<div class="content">		 
	<div class="row">
		<div class="span6 offset4">

  <?php
if($this->session->userdata('member_id')){		
?>

  <div id="center-column" style="font-size: 13px;">
    
	
<?php echo $flash_success ?>
<?php echo $error_message_div ?>
  <div class="page-header">
    <h1>Profile Info</h1>
  </div>
  
  <table class="table table-bordered table-striped table-hover">
    
    <tbody>
      <tr>
        <td><strong>Email:</strong></td>
        <td><?php echo $member->email?></td>
      </tr>
      <tr>
        <td><strong>Agency Name:</strong></td>
        <td><?php echo $member->first_name.' '.$member->middle_name.' '.$member->last_name?></td>
      </tr>
      <tr>
        <td><strong>DBA Name:</strong></td>
        <td><?php echo $member->dba_name?></td>
      </tr>
      <tr>
        <td><strong>Address:</strong></td>
        <td><?php echo $member->address?></td>
      </tr>
      <tr>
        <td><strong>Country:</strong></td>
        <td><?php echo $member->country?></td>
      </tr>
      <tr>
        <td><strong>State:</strong></td>
        <td><?php echo $member->state?></td>
      </tr>
      <tr>
        <td><strong>City:</strong></td>
        <td><?php echo $member->city?></td>
      </tr>
	  <tr>
        <td><strong>Zip Code:</strong></td>
        <td><?php echo $member->zip_code ?></td>
      </tr>
      <tr>
        <td><strong>Phone:</strong></td>
        <td><?php echo $member->phone_number.' - '.$member->phone_ext; ?>
		  <?php $add_num=isset($member->add_phone_number)?explode(',',$member->add_phone_number):''; 
		  if(!empty($add_num[0])){			
		  for($i=0;$i<count($add_num);$i++){
		  echo ','.$add_num[$i].' - '.$member->phone_ext; 
		   }
		  }
		  ?></td>
      </tr>
      <tr>
        <td><strong>Cell Number:</strong></td>
        <td><?php echo $member->cell_number ?></td>
      </tr>
	  <tr>
        <td><strong>Fax Number:</strong></td>
        <td><?php echo $member->fax_number.' - '.$member->fax_ext ?></td>
      </tr>
	  <tr>
        <td><strong>Secondary Email:</strong></td>
        <td><?php echo $member->sec_email ?></td>
      </tr>
	  <tr>
        <td><strong>Website:</strong></td>
        <td><?php echo $member->website ?></td>
      </tr>
      </tbody>
  </table>
	
		<a href="<?php echo base_url('account/profile_edit');?>" style="margin:auto;" class="btn btn-primary">Edit Profile</a>
	
  </div>
  <?php 
}else{
	$this->load->view('access_denied');
}
?>
		</div>
	</div>
</div>
<?php
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>
