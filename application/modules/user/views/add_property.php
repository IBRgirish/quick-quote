<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');
}
//$header = 'All Property Listings';
$header = 'Add Property';

$id	= isset($listing->id ) ?	$listing->id : 0; 
$property_title = (isset($_POST['property_title']) ? $_POST['property_title'] : NULL);
$pictures = (isset($_POST['pictures']) ? $_POST['pictures'] : NULL);
$property_description = isset($listing->property_description)?$listing->property_description :(isset($_POST['property_description']) ? $_POST['property_description'] : NULL);

$num_rooms = isset($listing->num_rooms)?$listing->num_rooms :(isset($_POST['num_rooms']) ? $_POST['num_rooms'] : NULL);
$toilet = isset($listing->toilet)?$listing->toilet :(isset($_POST['toilet']) ? $_POST['toilet'] : NULL);
$bath = isset($listing->bath)?$listing->bath :(isset($_POST['bath']) ? $_POST['bath'] : NULL);
$garage = isset($listing->garage)?$listing->garage :(isset($_POST['garage']) ? $_POST['garage'] : NULL);
$size_of_land = isset($listing->size_of_land)?$listing->size_of_land :(isset($_POST['size_of_land']) ? $_POST['size_of_land'] : NULL);
$size_of_floor = isset($listing->size_of_floor)?$listing->size_of_floor :(isset($_POST['size_of_floor']) ? $_POST['size_of_floor'] : NULL);
$selling_price = isset($listing->selling_price)?$listing->selling_price :(isset($_POST['selling_price']) ? $_POST['selling_price'] : NULL);

$location = isset($listing->location)?$listing->location :(isset($_POST['location']) ? $_POST['location'] : NULL);
$house_num = isset($listing->house_num)?$listing->house_num :(isset($_POST['house_num']) ? $_POST['house_num'] : NULL);
$street = isset($listing->street)?$listing->street :(isset($_POST['street']) ? $_POST['street'] : NULL);
$zip_code = isset($listing->zip_code)?$listing->zip_code :(isset($_POST['zip_code']) ? $_POST['zip_code'] : NULL);

$state = isset($listing->state)?$listing->state :(isset($_POST['state']) ? $_POST['state'] : NULL);
$city = isset($listing->city)?$listing->city :(isset($_POST['city']) ? $_POST['city'] : NULL);
$country = isset($listing->country)?$listing->country :(isset($_POST['country']) ? $_POST['country'] : NULL);

$current_market_value = isset($listing->current_market_value)?$listing->current_market_value :(isset($_POST['current_market_value']) ? $_POST['current_market_value'] : NULL);

//$google_map = isset($listing->google_map)?$listing->google_map :(isset($_POST['google_map']) ? $_POST['google_map'] : NULL);

$google_map = '';

$design_concept = isset($listing->design_concept)?$listing->design_concept :(isset($_POST['design_concept']) ? $_POST['design_concept'] : NULL);
$quality_of_finish = isset($listing->quality_of_finish)?$listing->quality_of_finish :(isset($_POST['quality_of_finish']) ? $_POST['quality_of_finish'] : NULL);
$neighborhood = isset($listing->neighborhood)?$listing->neighborhood :(isset($_POST['neighborhood']) ? $_POST['neighborhood'] : NULL);
$property_type = isset($listing->property_type)?$listing->property_type :(isset($_POST['property_type']) ? $_POST['property_type'] : NULL);
$available_for = isset($listing->available_for)?$listing->available_for :(isset($_POST['available_for']) ? $_POST['available_for'] : NULL);
$annual_earnings = isset($listing->annual_earnings)?$listing->annual_earnings :(isset($_POST['annual_earnings']) ? $_POST['annual_earnings'] : NULL);
$year_built = isset($listing->year_built)?$listing->year_built :(isset($_POST['year_built']) ? $_POST['year_built'] : NULL);
$currency = isset($listing->currency)?$listing->currency :(isset($_POST['currency']) ? $_POST['currency'] : NULL);
$property_category = isset($listing->property_category)?$listing->property_category :(isset($_POST['property_category']) ? $_POST['property_category'] : NULL);
$apprisal_date = isset($listing->apprisal_date)?$listing->apprisal_date :(isset($_POST['apprisal_date']) ? $_POST['apprisal_date'] : NULL);
$apprisal_value = isset($listing->apprisal_value)?$listing->apprisal_value :(isset($_POST['apprisal_value']) ? $_POST['apprisal_value'] : NULL);

$floor_image = isset($listing->floor_image)?$listing->floor_image :(isset($_POST['floor_image']) ? $_POST['floor_image'] : NULL);


$realestate_taxes = isset($listing->realestate_taxes)?$listing->realestate_taxes :(isset($_POST['realestate_taxes']) ? $_POST['realestate_taxes'] : NULL);
$to_purchased_in = isset($listing->to_purchased_in)?$listing->to_purchased_in :(isset($_POST['to_purchased_in']) ? $_POST['to_purchased_in'] : NULL);
$posted_by_user = isset($listing->posted_by)?$listing->posted_by :(isset($_POST['posted_by']) ? $_POST['posted_by'] : 'admin');
$request_status = isset($listing->request_status)?$listing->request_status :(isset($_POST['request_status']) ? $_POST['request_status'] : NULL);

$to_purchased_incremental ='';
$to_purchased_upshares ='';
$to_purchased_in =  isset($listing->to_purchased_in)?$listing->to_purchased_in :(isset($_POST['to_purchased_in']) ? $_POST['to_purchased_in'] : NULL);

if($to_purchased_in == 'incremental'){
	$to_purchased_incremental = 'checked="checked"';
}elseif($to_purchased_in == '10_per_share'){
	$to_purchased_upshares = 'checked="checked"';
}


if ($this->session->flashdata('error')){    
		echo '<div class="error"><p>'.$this->session->flashdata('error').'</p></div>';
	}
	if ($this->session->flashdata('success')){    
		echo '<div class="success">'.$this->session->flashdata('success').'</div>';
	}
	// Validation Errors
	  echo (validation_errors()?'<div class="error">'.validation_errors().'</div>':'');

?>
	<?php echo $tiny_head;  ?>

<br>
<script>
function change_currency(value)
{
		
	if(value =='NewZealand'){
	var currency_sign = "<?=currency_prefix('NZD') ?>";
	$('.inpCurrencey').html(currency_sign);
	$('#currency').attr('value','NZD');
	}
	
	if(value=='Australia'){
		var currency_sign = "<?=currency_prefix('AUD') ?>";
		$('.inpCurrencey').html(currency_sign);
		$('#currency').attr('value','AUD');
	}
	
	if(value=='Philippines'){
		var currency_sign = "<?=currency_prefix('PHP') ?>";
		$('.inpCurrencey').html(currency_sign);
		$('#currency').attr('value','PHP');
	}
}
$(document).ready(function(){

	change_currency($('#country').val());
	/*
	var currency_sign = "<?=currency_prefix('AUD') ?>";
	$('.inpCurrencey').html(currency_sign);
	$('#currency').attr('value','AUD');*/
	$("#apprisal_date").datepicker({ dateFormat: 'yy-mm-dd' });
});
</script>

<div class="table" style="padding-left:20px; font-size: 13px;" > 
 <form action="<?=base_url('user/add_property')?>" method="post" id="managemembers_add_edit" name="managemembers_add_edit" enctype="multipart/form-data" >
	<table cellspacing="0" cellpadding="0" class="listing form">
	<thead>
	<tr>
		<th class="full" colspan="2"><?=$header?></th>
	</tr>
	</thead>
	   <tbody>		  
		  <tr>
			 <td width="172" class="first"><strong>Property Title:</strong></td>
			 <td class="last"><input type="text" value="<?=$property_title?>" id="property_title" name="property_title"></td>
			   
		  </tr>	
		   
		  <tr>
			 <td width="172" class="first"><strong>Pictures :</strong></td>
			 <td class="last"> <input type="file" name="main_image"></td>
			 <tr>
			  <td></td>
			 <td><img src="<?=base_url('uploads/mainimage').'/'.$pictures ?>" height="150" width="150" >  
			 <?php if($pictures) { ?> <br><a href="<?=base_url(); ?>/administration/property/delete_mainimage/<?=$id ?>?name=<?=$pictures ?>">Delete Image</a>
			 <?php } 
			
			 ?>
			 </td>
			 </tr>
		  </tr>  
		  
		  <tr>
			 <td width="172" class="first"><strong>Property Description:</strong></td>
			 <td class="last"> <textarea rows="5" cols="70" name="property_description" id="property_description" style="width:70px; height:550px;border:2px solid blue;"><?php echo set_value('property_description',$property_description);?></textarea>
			</td>
			   
		  </tr>

		  
		 <tr>
			 <td width="172" class="first"><strong> Rooms :</strong></td>
			 <td class="last"><input type="text" value="<?=$num_rooms?>" id="num_rooms" name="num_rooms"></td>
			   
		  </tr>	
		  <tr>
			 <td width="172" class="first"><strong> Toilet:</strong></td>
			 <td class="last"><input type="text" value="<?=$toilet?>" id="toilet" name="toilet"></td>
			   
		  </tr>
		  		  <tr>
			 <td width="172" class="first"><strong> Bath:</strong></td>
			 <td class="last"><input type="text" value="<?=$bath?>" id="bath" name="bath"></td>
			   
		  </tr>	
		  <tr>
			 <td width="172" class="first"><strong>Garage:</strong></td>
			 <td class="last"><input type="text" value="<?=$garage?>" id="garage" name="garage"></td>
			   
		  </tr>		 

		  <tr>
			 <td width="172" class="first"><strong> Land Size:</strong></td>
			 <td class="last"><input type="text" value="<?=$size_of_land?>" id="size_of_land" name="size_of_land"></td>
			   
		  </tr>
		  
		  <tr>
			 <td width="172" class="first"><strong> Floor Size:</strong></td>
			 <td class="last"><input type="text" value="<?=$size_of_floor?>" id="size_of_floor" name="size_of_floor"></td>
			   
		  </tr>		  		  
		  
		  <tr>
			 <td width="172" class="first"><strong> Location:</strong></td>
			 <td class="last"><input type="text" value="<?=$location?>" id="location" name="location"></td>
			   
		  </tr>	
		 
		  <tr>
			 <td width="172" class="first"><strong>House No.:</strong></td>
			 <td class="last"><input type="text" value="<?=$house_num?>" id="house_num" name="house_num"></td>
			   
		  </tr>
		  
		    <tr>
			 <td width="172" class="first"><strong>Street:</strong></td>
			 <td class="last"><input type="text" value="<?=$street?>" id="street" name="street"></td>
			   
		  </tr>  
		  
		    <tr>
			 <td width="172" class="first"><strong>City:</strong></td>
			 <td class="last"><input type="text" value="<?=$city?>" id="city" name="city"></td>
		 </tr>	
		  
		    <tr>
			 <td width="172" class="first"><strong>State:</strong></td>
			 <td class="last"><input type="text" value="<?=$state?>" id="state" name="state"></td>
			   
		  </tr>
		  
		  <tr>
			 <td width="172" class="first"><strong>Country:</strong></td>
			 <!--td class="last">
			 <?php /*
			  $country_table = $this->config->item('country_table');
						$country=form_simple_dropdown_from_db_valuebyname('country', "SELECT id,country_name FROM ".$country_table." where status=1", $country_id, 'Choose');
						echo $country;*/
					//$country=form_simple_dropdown_from_db('country', "SELECT id,country_name FROM acms_country where status=1",$country_id);
					//echo $country;
			 ?></td-->
			 <td class="last">
				 <select name="country" id="country" onchange="change_currency(this.value);">
					 <option value="Australia" <?php if($country == 'Australia') echo 'selected=selected'; ?> >Australia</option>
					 <option value="NewZealand" <?php if($country == 'NewZealand') echo 'selected=selected'; ?> >New Zealand</option>
					 <option value="Philippines" <?php if($country == 'Philippines') echo 'selected=selected'; ?> >Philippines</option>
					
				</select>
					<input type="hidden" name="currency" id="currency" value="">
			</td>
			
		<!--tr>
			 <td width="172" class="first"><strong>Currency :</strong></td>
			 <td class="last">
			 <select name="currency">
			 <option value="USD" <?php if($currency == 'USD') echo 'selected=selected'; ?> >USD</option>
			 <option value="NZD" <?php if($currency == 'NZD') echo 'selected=selected'; ?> >NZD</option>
			 <option value="PHP" <?php if($currency == 'PHP') echo 'selected=selected'; ?> >PHP</option>
			 <option value="AUD" <?php if($currency == 'AUD') echo 'selected=selected'; ?> >AUD</option>
			</select>
			</td>
		</tr-->
		
			  <tr>
			 <td width="172" class="first"><strong>Selling Price:</strong><span style="color:red;" class="inpCurrencey"></span></td>
			 <td class="last"><input type="text" value="<?=$selling_price?>" id="selling_price" name="selling_price"></td>
			   
		  </tr>	
		  
		  
		  <tr>
			 <td width="185" class="first"><strong>Current Market Value:</strong><span style="color:red;" class="inpCurrencey"></span></td>
			 <td class="last"><input type="text" value="<?=$current_market_value?>" id="current_market_value" name="current_market_value"></td>
			   
		  </tr>	
		 
		  <tr>
			 <td width="172" class="first"><strong>Annual Earnings:</strong><span style="color:red;" class="inpCurrencey"></span></td>
			 <td class="last"><input type="text" value="<?=$annual_earnings?>" id="annual_earnings" name="annual_earnings"></td>
			   
		  </tr>	
			   
		  </tr>
		  
		  <tr>
			 <td width="172" class="first"><strong>Zip Code:</strong></td>
			 <td class="last"><input type="text" value="<?=$zip_code?>" id="zip_code" name="zip_code"></td>
			   
		  </tr>	
		   <!--tr>
			 <td width="172" class="first"><strong>Google Map:</strong></td>
			 <td class="last"><input type="text" value="<?=$google_map?>" id="google_map" name="google_map"></td>
			   
		  </tr-->
		 
		  <tr>
			 <td width="172" class="first"><strong>Design Concept:</strong></td>
			 <td class="last"><input type="text" value="<?=$design_concept?>" id="design_concept" name="design_concept"></td>
			   
		  </tr>		 

		  <tr>
			 <td width="172" class="first"><strong>Quality Of Finish:</strong></td>
			 <td class="last"><input type="text" value="<?=$quality_of_finish?>" id="quality_of_finish" name="quality_of_finish"></td>
			   
		  </tr>		 

		  <tr>
			 <td width="172" class="first"><strong>Neighborhood:</strong></td>
			 <td class="last"><input type="text" value="<?=$neighborhood?>" id="neighborhood" name="neighborhood"></td>
			   
		  </tr>		 

		<tr>
			 <td width="172" class="first"><strong>Property Type:</strong></td>
			 <td class="last">
			 <select name="property_type">
			 <option value="commercial" <?php if($property_type == 'commercial') echo 'selected=selected'; ?> >Commercial</option>
			 <option value="residential" <?php if($property_type == 'residential') echo 'selected=selected'; ?> >Residential</option>
			</select>
			</td>
		</tr>
		
		<tr>
			 <td width="172" class="first"><strong>Property Category :</strong></td>
			 <td class="last">
			 <select name="property_category">
			 <option value="second_board" <?php if($property_category == 'second_board') echo 'selected=selected'; ?> >Second Board</option>
			 <option value="prime_board" <?php if($property_category == 'prime_board') echo 'selected=selected'; ?> >Prime Board</option>
			</select>
			</td>
		</tr>

		<tr>
			 <td width="172" class="first"><strong>Available for :</strong></td>
			 <td class="last">
			 <select name="available_for">
			 <option value="sale" <?php if($available_for == 'sale') echo 'selected=selected'; ?> >Sale</option>
			 <option value="rent" <?php if($available_for == 'rent') echo 'selected=selected'; ?> >Rent</option>
			</select>
			</td>
			  
		  </tr>	
		  
		  <tr>
			 <td width="172" class="first"><strong>Year Of Built:</strong></td>
			 <td class="last"><input type="text" value="<?=$year_built?>" id="year_built" name="year_built"></td>
			   
		  </tr>		
		  
	

		  <tr>
			 <td width="172" class="first"><strong>Realestate Taxes:</strong></td>
			 <td class="last"><input type="text" value="<?=$realestate_taxes?>" id="realestate_taxes" name="realestate_taxes"></td>
			   
		  </tr>		  

		  <tr>
			   <td width="172" class="label"><strong>Floor Layout :</strong></td>
			   <td class="last"><input name="floor_image" type="file" id="floor_image" class="box"></td>
		  </tr>
		  
		  <tr>
			 <td width="172" class="first"><strong>Property Purchased in:</strong></td>
			  <td></td>   
		  </tr>
		  <?php 
		  

		  ?>
		  
		  <tr><td></td>
		      <td width="172" class="first"><strong>
		  <input type="radio" name="to_purchased_in" value="incremental" <?=$to_purchased_incremental; ?> />Incremental <br>
		 <input type="radio" name="to_purchased_in" value="10_per_share" <?=$to_purchased_upshares; ?> /> Share of 10% Up 
		  </strong></td>
		  
		  </tr>
		  <?php 
		  if( !empty($posted_by_user) && $posted_by_user != 'admin' ){
			$member_info =$object->get_member_info($posted_by_user); 
			$username =  $member_info->first_name.' '. $member_info->last_name;
			$user = '<a  target="_blank" href="'.base_url().'administration/members/details/'.$posted_by_user.'">'.$username.'</a>';
		  } else { $user = 'Admin';	  }
		  ?>
		 
		  <tr>
			 <td class="first" colspan="4">&nbsp;</td>
		  </tr>
		  <tr class="bg">
		  <td class="first">&nbsp;</td>
			 <td colspan="4">
				<input type="submit" value="Save" class="button" name="save_member">
				&nbsp;
				<input type="button" onclick="closeWindow('<?=base_url('user/account')?>');" value="Cancel" class="button">
				<input type="hidden" name="property_id" id="property_id" value="<?=$id?>" >
				
			</td>
		  </tr>
	   </tbody>
	</table>
	<input type="hidden" value="insert" name="type">
	
 </form>
</div>

<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>