<?php

class Members_model extends CI_Model {
	


	function Members_model() {
		parent::__construct();		
	}
	
		
	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);		
		return $this->db->delete($table); 
	}


	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{		
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}


	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
		


	/**
	 * insert_update_articles function
	 *
	 * @access	public
	 * @params	array data, integer id
	 * @return	boolean
	 */
	
	public function insert_update_members($data, $id=NULL)
	{		
		
		if($id){
			$this->_update($id, $data, $this->config->item('member_table'));
			return $id;
		}
		else{
			$this->_insert($data, $this->config->item('member_table'));
			return $lastinsertid =$this->db->insert_id();
		}

		
	}
	
	public function checkuniquemember($email)
	{
		 $member_table = $this->config->item('member_table');
		 $this->db->select("$member_table.email");	
		 $this->db->where("$member_table.email", $email);
		  $this->db->where("$member_table.is_deleted", 'FALSE');
		 $query = $this->db->get($this->config->item('member_table'));
		 
		 if($query->num_rows> 0) 
			return 1;
		 else 
			return 0;
	}
	
	public function get_members($id = '')
	{
		
		$member_table = $this->config->item('member_table');
		 $this->db->select("$member_table.email");	
		 $this->db->where("$member_table.status", 'ACTIVE');
		  $this->db->where("$member_table.is_deleted", 'FALSE');
		  $query = $this->db->get($this->config->item('member_table'));//echo $this->db->last_query();
		 
	  if($query->num_rows> 0) 
	  {
			return $query->result();
	  }
	  else 
	  {
			return FALSE;
	  }
		
	}
	
	public function get_underwriters($id = '')
	{
		
		$member_table = $this->config->item('underwriter_table');
		 $this->db->select("$member_table.id");	
		 $this->db->select("$member_table.email");	
		 $this->db->select("$member_table.name");	
		 $this->db->where("$member_table.status", '1');
		  $query = $this->db->get($this->config->item('underwriter_table'));//echo $this->db->last_query();
		
		 if($query->num_rows> 0) 
		  {
				return $query->result();
		  }
		  else 
		  {
				return FALSE;
		  }
		
	}

	public function get_member_info($id,$countryid=7)
	{	
	  $member_table = $this->config->item('member_table');
	  $this->db->select("$member_table.*");	
	 // $this->db->select('acms_country.id as countryid,acms_country.country_name');		
	  //$this->db->from($this->config->item('country_table'));
	  //$this->db->where('acms_country.id',$countryid);
	  $this->db->where("$member_table.id", $id);	  
	  $this->db->where("$member_table.status", 'ACTIVE');
	  $query = $this->db->get($this->config->item('member_table'));//echo $this->db->last_query();
	  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else 
	  {
			return FALSE;
	  }
	}


	public function change_password($data, $id=NULL)
	{		
		
		if($id){
			$this->_update($id, $data, $this->config->item('member_table'));
			return true;
		}
		else{
			return FALSE;
		}

		
	}

	public function forgot_password($data, $email=NULL)
	{		
		
		if($email){
			$this->db->where('email',$email);			
			if($this->db->update($this->config->item('member_table'), $data) !== FALSE){
				return true;
			}
		}
		else{
			return FALSE;
		}

		
	}


	public function get_member_info_via_email($email)
	{	
		
	  $this->db->where('email', $email);	  	 
	  $query = $this->db->get($this->config->item('member_table'));
	  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else 
	  {
			return FALSE;
	  }
	}


	public function delete($id)
	{
		return $this->_delete($id, $this->config->item('members_company_list'));
	}
	
			public function insert_update_order($data,$id=NULL)
	{
		
		if($id){
			$this->_update($id, $data, 'acms_orders');
			return true;
		}
		else{
			$this->_insert($data, 'acms_orders');
			return $lastinsertid =$this->db->insert_id();
		}
	}
	
	
	
	
	public function update_lastlogin($id)
	{
		$data = array('last_login' =>date("Y-m-d : H:i:s", time()));
		$this->db->where('id', $id);
		$this->db->update($this->config->item('member_table'), $data); 
	}
	
	public function insert_update_property_listings($data, $id=NULL)
	{
		if($id)
			return $this->_update($id, $data, $this->config->item('property_listing_table'));
		else
			return $this->_insert($data, $this->config->item('property_listing_table'));
	}
	
	public function active_underwriter_list()
	{
		$sql = 'SELECT * FROM '.$this->config->item('underwriter_table').' WHERE status=1';
		$query = $this->db->query($sql);
		$count = $query->num_rows();
		if($count >= 1)
		{
			$data['flag'] = 1;
			$data['resultData'] = $query->result_array();
		}
		else
		{
			$data['flag'] = 0;
		}
		return $data;
	}
	
}

