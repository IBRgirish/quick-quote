<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Login_model extends CI_Model {

	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * validate_admin function
	 *
	 * @access	public
	 * @return	array
	 */
	public function validate_member()
	{		
	 // echo $this->input->post('username');
	  $this->db->where('email', $this->input->post('username'));
	  $this->db->where('password', $this->input->post('password'));
	  //$this->db->where('status', 'ACTIVE');
	  $query = $this->db->get($this->config->item('member_table'));
	  //echo $this->db->last_query();
	  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else 
	  {
	  		return FALSE;
	  }
	}
	
	public function update_failed_login_count()
	{
		$this->db->where('email', $this->input->post('username'));
		$query = $this->db->get($this->config->item('member_table'));
		if($query->num_rows == 1) 
	  	{
			$val=$query->row();
			$this->db->set('failed_attempt', 'failed_attempt+1', FALSE);
			$this->db->where('email', $val->email);
			$this->db->update($this->config->item('member_table'));
	  	}
		else
		{
			return FALSE;
		}
	}
	
	public function get_failed_login_count()
	{
		$this->db->where('email', $this->input->post('username'));
		$query = $this->db->get($this->config->item('member_table'));
		if($query->num_rows == 1) 
	  	{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}
	
	public function block_member_account()
	{
		
		$this->db->set('status', 'BLOCKED');
		$this->db->set('failed_attempt_timestamp',date("Y-m-d H:i:s", time()));
		$this->db->where('email', $this->input->post('username'));
		$this->db->update($this->config->item('member_table'));
	}
	
	public function unblock_member_account()
	{
		
		$this->db->set('status', 'ACTIVE');
		$this->db->set('failed_attempt_timestamp', NULL);
		$this->db->set('failed_attempt', 0);
		$this->db->where('email', $this->input->post('username'));
		$this->db->update($this->config->item('member_table'));
	}
	
	public function get_failed_attemps()
	{
		$this->db->where('key','failed_attemps');
		$query = $this->db->get($this->config->item('config_table'));
		if($query->num_rows == 1) 
	  	{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_auto_unblock_period()
	{
		$this->db->where('key','auto_unblock_period');
		$query = $this->db->get($this->config->item('config_table'));
		if($query->num_rows == 1) 
	  	{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_support_email()
	{
		$this->db->where('key','support_email');
		$query = $this->db->get($this->config->item('config_table'));
		if($query->num_rows == 1) 
	  	{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}
}