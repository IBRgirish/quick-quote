<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends RQ_Controller {


		public function __construct()
		{


			parent::__construct();
			$this->load->model('members_model');
			$this->lang->load('member');	
			$this->load->helper('string');
			$this->load->helper('my_helper');
			$this->load->model('login_model');
			$this->load->helper('my_helper');
			$this->load->helper('text');
			$this->load->library('lotencrypt');	
			$this->lang->load('common');
		}

		public function index($val='')
		{
			
			$support_msg = 'support ';
			$support_mail = $this->login_model->get_support_email();
			if ($support_mail) {
				if ($support_mail->value!='') {
					$support_msg .= $support_mail->value;
				}
			}
			
			$auto_unblock_period = $this->login_model->get_auto_unblock_period();
			$unblock_period=24;
			if ($auto_unblock_period) {
				if ($auto_unblock_period->value!='') {
					$unblock_period=$auto_unblock_period->value;
				}
			}
		
			$failed_attempts = $this->login_model->get_failed_attemps();
			$failed_attempt=3;
			if ($failed_attempts) {
				if ($failed_attempts->value!='') {
					$failed_attempt = $failed_attempts->value;
				}
			}
							
			if(($val == "1") && (base_url(uri_string()) != $this->config->item('prevous_url'))) {				
				$prev_url = $this->config->item('prevous_url');
				$this->session->set_userdata('prev_url', "$prev_url");		
			}		
			
			if($this->session->userdata('member_id'))
			{
				//$this->load->view('frontend/index/view_detail/viewdetail/');
				redirect(base_url('user'));
			}
			else
			{
				$this->form_validation->set_rules('username', 'Username', 'required|valid_email');
				$this->form_validation->set_rules('password', 'Password', 'required');
				if($this->form_validation->run() === FALSE)
				{				
					//$this->template->write('title', "Member Login");
					$data['metaKeywords'] = "Member Login";
					$this->load->view('user/login', $data);
					//$this->template->write_view('content', 'user/login', $data);		
					//$this->template->render();
				
				}	
				else
				{
					$login_flag=0;
					$valid_user = $this->login_model->validate_member();
					if($valid_user) 
					{
						if ($valid_user->status=='BLOCKED')
						{
							$current_date=date("Y-m-d H:i:s", time());
							$last_login=date('Y-m-d H:i:s', strtotime('+'.$unblock_period.' hour', strtotime($valid_user->failed_attempt_timestamp)));
							
							if ($current_date>=$last_login)
							{
								$this->login_model->unblock_member_account();
								$login_flag=1;
							}
							else
							{
								$login_flag=0;
							}
						
						}elseif($valid_user->status=='REQUESTED'){
							$login_flag=2;
						}else
						{
							$login_flag=1;
						}
						
						if ($login_flag==1)
						{ 
							$data = array(
								'username' => $valid_user->email,
								'member_name' => $valid_user->first_name,
								'member_id' => $valid_user->id,
								);
							
							$this->session->set_userdata($data);
							// set_subscription_session();
							$this->members_model->update_lastlogin($valid_user->id);
							$this->session->set_flashdata('success', 'You have successfully logged into your account');
							if($val == "1")
							{
								$prev_url = $this->session->userdata('prev_url');
								$this->session->unset_userdata('prev_url');
								redirect($prev_url);
							}
							else
							{
								if($this->input->post('login_type') == 'fancybox')
								{
									echo '<script type="text/javascript">
											parent.$.fancybox.close();
											parent.location.href = "'.base_url('user').'";
										</script>';
								}
								else
								{
									redirect(base_url('user'));
								}
							}
						}
						elseif($login_flag === 2){
							$this->session->set_flashdata('error', 'Account is not active. Please contact admin or wait for activation.');
							if($this->input->post('login_type') == 'base')
							{
							   redirect(base_url());
							  
							}
							else
							{
							   redirect(base_url());
							}
						}else
						{ 
							$this->session->set_flashdata('error', 'Account is blocked contact '.$support_msg.' or wait for '.$unblock_period.' hours to get unblock.');
							if($this->input->post('login_type') == 'base')
							{
							   redirect(base_url());
							  
							}
							else
							{
							   redirect(uri_string());
							}
						}
				}
				else 
				{
					$failed_login_count = $this->login_model->get_failed_login_count();
					
					if ($failed_login_count)
					{
						if ($failed_login_count->failed_attempt < $failed_attempt)
						{
							$this->login_model->update_failed_login_count();
							$count = $this->login_model->get_failed_login_count();
							if ($count->failed_attempt == $failed_attempt)
							{
								$this->login_model->block_member_account();
								$this->session->set_flashdata('error', 'Account is blocked contact '.$support_msg.' or wait for '.$unblock_period.' hours to get unblock.');
							}
							else
							{
								$this->session->set_flashdata('error', 'Information submitted is incorrect.');
							}
						}
						else
						{
							$this->session->set_flashdata('error', 'Account is blocked contact '.$support_msg.' or wait for '.$unblock_period.' hours to get unblock.');
						}
					
					}
					else
					{
						$this->session->set_flashdata('error', 'Information submitted is incorrect.');
					}
					
					
					
					if($this->input->post('login_type') == 'base')
					{
					   redirect(base_url());
					  
					}
					else
					{
					   redirect(uri_string());
					}
				}
			  }

		}}
		
		
} 
/* End of file user.php */ 