	<?php  
		if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
		{	
			if($this->session->userdata('admin_id')){		
				$this->load->view('includes/admin_header');	
			} else if($this->session->userdata('underwriter_id')){	
				$this->load->view('includes/header');	
			}  else if($this->session->userdata('member_id')){	
				$this->load->view('includes/header');	
			}
		} 
		function getAmount($money)
		{
			$cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
			$onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);
		
			$separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;
		
			$stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
			$removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);
		
			return (float) str_replace(',', '.', $removedThousendSeparator);
		}
	?>
<?php $segement = $this->uri->segment(2); ?>
    <?php $data['segement'] = $segement;?>
	<style>
			label.cabinet {
				width: 79px;
				background: url(images/upload_icon.png) 0 0 no-repeat;
				display: block;
				overflow: hidden;
				cursor: pointer;
				width:62px; margin-bottom:5px; cursor:pointer;height:23px;
			}
	 
			label.cabinet input.file {
				position: relative;
				cursor: pointer;
				height: 100%;
				width: 120px;
				opacity: 0;
				-moz-opacity: 0;
				filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
			
			}
			.container-fluid {
				display:table;
			}
	</style>

	<?php
		$ratesheet_id = 1;
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html>
	<head>
		<title> Rate Sheet </title>
		<link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
		<link href="<?php echo base_url('css');?>/common.css" rel="stylesheet">
		<link href="<?php echo base_url('css');?>/meetingminute.css" rel="stylesheet">
		<script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
		<script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
		<script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script>
		<script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
        <script src="<?php echo base_url('js'); ?>/ratesheet.js"></script>
		<script src="<?php echo base_url('js'); ?>/cargo.js"></script>
        <script src="<?php echo base_url('js'); ?>/pd.js"></script>
        <script src="<?php echo base_url('js'); ?>/liability.js"></script>
		
		<!--script src="<?php echo base_url('js'); ?>/jquery.validateForm.js"></script-->
		
		<script type='text/javascript'>
			$(document).ready(function(e) {
                $('.price_format_decimal').priceFormat({
					prefix: '$ ',
					centsLimit: 1,
					thousandsSeparator: ','
				});
            });
			function return_numeric(no)
			{
				
				//return no.substring(1));
				return Number(no.replace(/[^0-9\.]+/g,""));
			}
			function apply_on_all_elements(){
		
				$('input').not('.datepick').autotab_magic().autotab_filter();
				$('.price_format').priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});
				 $('.price_format_decimal').priceFormat({
					prefix: '$ ',
					centsLimit: 1,
					thousandsSeparator: ','
				});
			}
			
			
			
			$(document).ready(function() {
				<!----disabled system---> 
				apply_on_all_elements();
			
				//$('.show_tractor_others').hide();
				$(".mkread").attr("disabled","true"); 
				<?php /*if($segement=='generate_ratesheet'){*/?>
				$(".mkread1").attr("disabled","true"); 
				<?php /*} */?>
				//$(".total_cargo").attr("disabled","true");		
				/*$('.efl,.ou').change(function(){
					
					if (($(this).val()=="None") || ($(this).val()=="0") || ($(this).val()=="No"))
					{
						
						$(this).parents('.row-efl').find('.mkread').attr("disabled",true); 		
						$(this).parents('.row-efl').find('.mkread').val(0); 
						calctotalcargo();
					}
					else
					{
						$(this).parents('.row-efl').find('.mkread').attr("disabled",false);
					}
				});*/
				
				
				
				/*$("#bsr,.efp").keyup(function(){
					calctotalcargo();
				});*/
			
				
				
				$('body').on('change','.number_of_trailer',function(e){
					e.preventDefault();
					var total_no = 0;
					var val = $(this).val();
					var currcount = $(".trailerrw").length;
					var diff = currcount - val;
					
					if(diff < 0) {
						while(diff < 0){
							var num     = $('.trailerrw').length+1;
							var newNum  = new Number(num - 1);  
							var newElem = '<tr id="trailerrw'+num+'" class="trailerrw"><td class="vehicle_name">Trailer '+num+'<input type="hidden" name="trailername_'+num+'" value="Trailer '+num+'"></td><td style="text-align:center"><input type="text" class="trailer_value vehicle_value span6 ui-state-valid price_format" value="" name="trailer_val'+num+'"></td><td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="" name="trailer_ded'+num+'"></td><td style="text-align:center"><input type="text" name="trailer_rate'+num+'" class="span6 vehicle_rate ui-state-valid"></td><td style="text-align:center"><input type="text" name="trailer_flat_rate'+num+'" class="span6 numr_valid vehicle_frate ui-state-valid"></td><td style="text-align:center"><input type="text" name="trailer_premium'+num+'" class="span6 trailer_prem vehicle_rp price_format_decimal ui-state-valid"></td></tr>';
							
							if(currcount == 0)
							{
								if($(".tractorrw").length != 0)
									$(".tractorrw:last").after(newElem);
								else if($(".truckrw").length != 0)
									$(".truckrw:last").after(newElem);
								else
									$("#add_owner_row").prepend(newElem);
								currcount = 1;
							}
							else
							{
								$('.trailerrw:last').after(newElem);	
							}
							$('#trailerrw' + num +' .vehicle_name').html('Trailer '+num);
							diff++;
						} 
					 }
					 else if(diff > 0)
					 {
						while(diff > 0){
						
							 var v = $('tr.trailerrw:last').remove() ; 
							 diff--;
						 }
					 }	
					apply_on_all_elements();
				});
				$('body').on('change','.no_of_truck',function(e){
					e.preventDefault();
					var total_no = 0;
					var val = $(this).val();
					var currcount = $(".truckrw").length;
					var diff = currcount - val;
					
					if(diff < 0) {
						while(diff < 0){
							var num     = $('.truckrw').length+1;
							var newNum  = new Number(num - 1);  
							var newElem = '<tr id="truckrw'+num+'" class="truckrw"><td class="vehicle_name">Truck '+num+'<input type="hidden" name="truckname_'+num+'" value="Truck '+num+'"></td><td style="text-align:center"><input type="text" class="truck_value vehicle_value span6 ui-state-valid price_format" value="" name="truck_val'+num+'"></td><td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="" name="truck_ded'+num+'"></td><td style="text-align:center"><input type="text" name="truck_rate'+num+'" class="span6 vehicle_rate ui-state-valid"></td><td style="text-align:center"><input type="text" name="truck_flat_rate'+num+'" class="span6 numr_valid vehicle_frate ui-state-valid"></td><td style="text-align:center"><input type="text" name="truck_premium'+num+'" class="span6 truck_prem1 vehicle_rp price_format_decimal ui-state-valid"></td></tr>';
							if(currcount == 0)
							{
								if($(".tractorrw").length == 0)
									$("#add_owner_row").prepend(newElem);
								else
									$(".tractorrw:last").after(newElem);
								currcount = 1;
							}
							else
							{
								$('.truckrw:last').after(newElem);	
							}
							$('#truckrw' + num +' .vehicle_name').html('Truck '+num);
							diff++;
						} 
					 }
					 else if(diff > 0)
					 {
						while(diff > 0){
						
							 var v = $('tr.truckrw:last').remove() ; 
							 diff--;
						 }
					 }	
					apply_on_all_elements();
				});
				$('body').on('change','.no_of_tractor',function(e){
					
					e.preventDefault();
					var total_no = 0;
					var val = $(this).val();
					var currcount = $(".tractorrw").length;
					var diff = currcount - val;
					
					if(diff < 0) {
						while(diff < 0){
							var num     = $('.tractorrw').length+1;
							var newNum  = new Number(num - 1);  
							var newElem = '<tr id="tractorrw'+num+'" class="tractorrw"><td class="vehicle_name">Tractor '+num+'<input type="hidden" name="tractorname_'+num+'" value="Tractor '+num+'"></td><td style="text-align:center"><input type="text" class="tractor_value vehicle_value span6 ui-state-valid price_format" value="" name="tractor_val'+num+'"></td><td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="" name="tractor_ded'+num+'"></td><td style="text-align:center"><input type="text" name="tractor_rate'+num+'" class="span6 vehicle_rate ui-state-valid"></td><td style="text-align:center"><input type="text" name="tractor_flat_rate'+num+'" class="span6 numr_valid vehicle_frate ui-state-valid"></td><td style="text-align:center"><input type="text" name="tractor_premium'+num+'" class="span6 tractor_prem vehicle_rp price_format_decimal ui-state-valid"></td></tr>';
							
							if(currcount == 0)
							{
								$("#add_owner_row").prepend(newElem);
								currcount = 1;
							}
							else
							{
								$('.tractorrw:last').after(newElem);	
							}
							$('#tractorrw' + num +' .vehicle_name').html('Tractor '+num);
							diff++;
						} 
					 }
					 else if(diff > 0)
					 {
						while(diff > 0){
						
							 var v = $('tr.tractorrw:last').remove() ; 
							 diff--;
						 }
					 }	
					apply_on_all_elements();
				});
				
				
				
				
				
				
				
				//* PD Script ends */
				
	<!--integer validation---->
				$(".numr_valid").keypress(function(evt){				
					var charCode = (evt.which) ? evt.which : event.keyCode;
					if (charCode != 46 && charCode > 31
					&& (charCode < 48 || charCode > 57))
					{
						//alert('Please enter numeric value');
						$(this).css("border", "1px solid #FAABAB");
						$(this).css("box-shadow", "0 0 5px rgba(204, 0, 0, 0.5");
						
						$(this).focus();
						return false;
					}			
					//return true;
					
	else {
						$(this).css("border", "1px solid #CCCCCC");
						$(this).css("box-shadow", "none");
					}
				});				
	<!--integer validation---->		
					
									
			});			


</script>
		
</head>
<body>
<div class="container-fluid">  

 <?php  $this->load->view('admin_cargo_ratesheet', $data);  ?>
 <?php $this->load->view('admin_pd_ratesheet', $data); ?>
	<div id="bundle_message"></div>
    <div id='loader3' style="display:none">
    	<img src="<?php echo base_url(); ?>images/spinner.gif"/>
    </div>
    <div id="cargo_sheet_message"></div>
	<button class="btn btn-primary" id="ratesheet_bundle_submit" type="submit" style="display:">Submit Bundle</button>
</div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script>
function changeCarrier(val, type){
	var url = '<?php echo base_url('ratesheet'); ?>/get_carrier_by_state';
	changeCarrier1(val, type, url)
}
function updatePolicyValue(type, val){
	var url = '<?php echo base_url('ratesheet'); ?>/get_cargo_detail';
	updatePolicyValue1(type, val, url);	
}
$(document).ready(function(){
  $("#assign_producer").change(function(){
	  if($(this).val() == '-1')
	  {
		  $("#new_producer").show();
	  }
	  else
	  {
		  var producer = $(this).val();
		  $(".producer_email").val(producer);
		  $("#new_producer").hide();
		  
		  var baseurl = "<?php echo base_url('ratesheet'); ?>/get_underwriter_by_producer";
		  $.ajax({
			url: baseurl,
			type: "post",
			data: {'producer' : producer},
			datatype: 'json',
			success: function(data){
			   var obj = $.parseJSON(data);  
			   //alert(obj['underwriter']);	
			   var underwriter = obj['underwriter'];
			   $('.underwriter_ids').val(underwriter);				
			},
			error:function(){
			  
			}   
		  });
	  }
  });
	$(".sheet-content").hide();
	$(".sheet-header").click(function(){	
		$(this).parent().parent().parent().find(".sheet-content").slideToggle();
	});
});

	function rofoperation(val , id){
				
			
			if(val == 'other'){
				$('.show_tractor_others').show();
			} else {
				$('.show_tractor_others').hide();
			}	
			
		}
		
		
		
	/*  alert(id);
	alert(val);  */

<!--ajax form submit-->

$("#cargo_submit").click(function(){
	
	var baseurl = "<?php echo base_url('ratesheet'); ?>/save_cargo";

	$.ajax({
		  url: baseurl,
		  type: "post",
		  data: $("#main_form2").serialize(),
	 	 datatype: 'json',
		 beforeSend: function() {
			 $('#loader').show();
		  },
		  complete: function(){
			 $('#loader').hide();
		  },
		  success: function(data){
				// $("#main_form2 input,#main_form2 select").attr("disabled",true);
				 $("#cargo_sheet_message").html("Cargo Rate Sheet Draft Saved");
				 if(($(".bundleid").first().val() == ''))
				 {
					$("#ratesheet_bundle_submit").show();
					$(".bundle_mail").show();
				 }
				 $(".bundleid").val(data);
				 
		  },
		  error:function(){
			
		  }   
		}); 
});
$("#liability_submit").click(function(){
	var baseurl = "<?php echo base_url('ratesheet'); ?>/save_liability";

	$.ajax({
		  url: baseurl,
		  type: "post",
		  data: $("#main_form4").serialize(),
	 	 datatype: 'json',
		  beforeSend: function() {
			 $('#loader1').show();
		  },
		  complete: function(){
			 $('#loader1').hide();
		  },
		  success: function(data){
			//	 $("#main_form4 input,#main_form4 select").attr("disabled",true);
				 $("#liability_sheet_message").html("Auto Liability Rate Sheet Draft Saved"); 
				if(($(".bundleid").first().val() == ''))
				 {
					$("#ratesheet_bundle_submit").show();
					$(".bundle_mail").show();
				 }			 
				 $(".bundleid").val(data);
		  },
		  error:function(){
			
		  }   
		}); 
});

$("#ratesheet_bundle_submit").click(function(){
	var baseurl2 = "<?php echo base_url('ratesheet/submit_bundle'); ?>/";
	var quote_id = $("#quote_id1").val();
	var bundle_id = $(".bundleid").first().val();
	var producer_email = $(".producer_email").val();
	//alert(baseurl2);
	$.ajax({
		  url: "<?php echo base_url('ratesheet/submit_bundle'); ?>",
		  type: "post",
		  data: 'quote_id='+quote_id+'&bundle_id='+bundle_id+'&producer_email='+producer_email,
	 	 // datatype: 'json',
		  beforeSend: function() {
			 $('#loader3').show();
		  },
		  complete: function(){
			 $('#loader3').hide();
		  },
		  success: function(data){
		 // alert(data);
			$("#bundle_message").html("Bundle Submitted");
			
		  },
		  error: function (xhr, ajaxOptions, thrownError) {
			alert(xhr.status);
			alert(thrownError);
		  }
		}); 
});
$("#prod_passwd").on('click', validPass).on('keyup', validPass);

function validPass()
{
	var pass = $("#prod_passwd").val();
	if(pass.length>0)
	{
		var email = $("#prod_email").val();
		if(validateEmail(email))
		{
			if($("#email_message span").html()=='Available')
			{
				$("#add_producer").removeAttr('disabled');
			}
			else
			{
				$("#add_producer").attr('disabled', 'disabled');
			}
		}
		else
		{
			$("#add_producer").attr('disabled', 'disabled');
		}
	}
	else
	{
		$("#add_producer").attr('disabled', 'disabled');
	}
}
/*$('.underwrit').trigger('click');*/
$('.underwrit').click(function(){

check_underwriter();
});

check_underwriter();
produce_email_chekc();
function produce_email_chekc()
{
	var producer_email = $('#assign_producer').val();
	$('.producer_email').val(producer_email);
}
function check_underwriter(){
	var underwriter = [];
	var n= 0;
	
	$('[name="underwriter[]"]').each(function(index) {
		//  alert($(this).val());
		if($(this).is(':checked'))
		{
		  underwriter[n] = $(this).val();
		  n++;
		  }
		  // and of course depending on your element utilize $(this).doSomethingCool
		});
	$('.underwriter_ids').val(underwriter);
}
$("#add_producer").click(function(){
	var baseurl2 = "<?php echo base_url('user'); ?>/add_manual_member";
	var email = $("#prod_email").val();
	if(validateEmail(email))
	{
		var underwriter = [];
		var n= 0;
		$("#prod_email").removeClass('ui-state-error');
		$('[name="underwriter[]"]').each(function(index) {
			//  alert($(this).val());
			if($(this).is(':checked'))
			{
			  underwriter[n] = $(this).val();
			  n++;
			  }
			  // and of course depending on your element utilize $(this).doSomethingCool
			});
			//alert(underwriter);
		$.ajax({
			  url: baseurl2,
			  type: "post",
			  data: {'email':$("#prod_email").val(),'password':$("#prod_passwd").val(),'underwriter':underwriter},
			  beforeSend: function() {
				 $('#prod_userloader3').show();
			  },
			  complete: function(){
				  $('#prod_userloader3').hide();
			  },
			  success: function(data){
			 //alert(data);
				$(".producer_email").val($("#prod_email").val());
				
					$("#prod_user_msg").html("<span style='color:green'>Saved</span>");
					$("#assign_producer").append('<option value="'+email+'">'+email+'</option>');
				
			  },
			  error:function(){
			}   
		});
	}
	else
	{
		$("#prod_email").addClass('ui-state-error');
	}
});
function validateEmail(sEmail) {
    var filter =/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

$("#prod_email").change(function(){
	var baseurl2 = "<?php echo base_url('user'); ?>/checkmember";
	var email = $("#prod_email").val();
	if(validateEmail(email))
	{
		$("#prod_email").removeClass('ui-state-error');
		$.ajax({
			  url: baseurl2,
			  type: "post",
			  data: {'email':$(this).val()},
			  datatype: 'json',
			  beforeSend: function() {
			  	 $("#email_message").html("");
				 $('#emailloader3').show();
				 $("#add_producer").attr('disabled', 'disabled');
			  },
			  complete: function(){
				 $('#emailloader3').hide();
			  },
			  success: function(data){
				
				if(data==0)
				{
					$("#email_message").html("<span style='color:green'>Available</span>");
					validPass();
				}	
				else
				{
					$("#email_message").html("<span style='color:red'>Not Available</span>");
					$("#add_producer").attr('disabled', 'disabled');
					}
				
			  },
			  error:function(){
				
			  }   
		}); 
	}
	else
	{
		$("#prod_email").addClass('ui-state-error');
	}
});
$("#physical_damage_submit").click(function(){
	
	var baseurl2 = "<?php echo base_url('ratesheet'); ?>/save_physical_damage";
	
	$.ajax({
		  url: baseurl2,
		  type: "post",
		  data: $("#main_form3").serialize(),
	 	  datatype: 'json',
		  beforeSend: function() {
			 $('#loader2').show();
		  },
		  complete: function(){
			 $('#loader2').hide();
		  },
		  success: function(data){
		  //alert(data);
		//	$("#main_form3 input,#main_form3 select").attr("disabled",true);
			$("#physical_damage_sheet_message").html("Physical Damage Rate Sheet Draft Saved");
			if(($(".bundleid").first().val() == ''))
			 {
			 
				$("#ratesheet_bundle_submit").show();
				$(".bundle_mail").show();
			 }
			$(".bundleid").val(data);
		  },
		  error:function(){
			//alert('e');
		  }   
		}); 
});




	</script>
    
    
    
   
    
    
    
	</body>
	</html>
	<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{
		$this->load->view('includes/footer');
	}
	?>