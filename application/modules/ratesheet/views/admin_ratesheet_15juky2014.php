	<?php  
		if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
		{	
			
			if($this->session->userdata('admin_id')){		
				$this->load->view('includes/admin_header');	
			} else if($this->session->userdata('underwriter_id')){	
				$this->load->view('includes/header');	
			}
		} 
		function getAmount($money)
		{
			$cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
			$onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);
		
			$separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;
		
			$stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
			$removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);
		
			return (float) str_replace(',', '.', $removedThousendSeparator);
		}
	?>

	<style>
			label.cabinet {
				width: 79px;
				background: url(images/upload_icon.png) 0 0 no-repeat;
				display: block;
				overflow: hidden;
				cursor: pointer;
				width:62px; margin-bottom:5px; cursor:pointer;height:23px;
			}
			
			.sheet-header
			{
				cursor:pointer;
			}
	 
	 
			label.cabinet input.file {
				position: relative;
				cursor: pointer;
				height: 100%;
				width: 120px;
				opacity: 0;
				-moz-opacity: 0;
				filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
			
			}
			.container-fluid {
				display:table;
			}
	</style>

	<?php
		$ratesheet_id = 1;
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html>
	<head>
		<title> Rate Sheet </title>
		<link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
		<link href="<?php echo base_url('css');?>/common.css" rel="stylesheet">
		<link href="<?php echo base_url('css');?>/meetingminute.css" rel="stylesheet">
		<script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
		<script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
		<script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script >
		<script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
		<!--script src="<?php echo base_url('js'); ?>/jquery.validateForm.js"></script-->
		
		<script type='text/javascript'>
			function return_numeric(no)
			{
				
				//return no.substring(1));
				return Number(no.replace(/[^0-9\.]+/g,""));
			}
			function apply_on_all_elements(){
		
				$('input').not('.datepick').autotab_magic().autotab_filter();
				$('.price_format').priceFormat({
				prefix: '$ ',
				centsLimit: 0,
				thousandsSeparator: ','
				});
			}
			
			function calctotalliability()
			{
				total_cargo_premium = 0;
				
				$('.row-lefl').each(function(){
					
					if(($(this).find('.lefl').val() != 'No') && ($(this).find('.lefl').val() != '') && ($(this).find('.lefl').val() != 'Rejected') && ($(this).find('.lefl').val() != 'None') && ($(this).find('.lefl').val() != '0') && ($(this).find('.lefl').val() != undefined))
					{		
						if($(this).find(".lnvp").val() != undefined)
						{
							premium = return_numeric($(this).find(".lnvp").val());	
							total_cargo_premium = total_cargo_premium + parseInt(premium); 						
						}
					}
				});
				$('.row-efl2,.row-efl1').each(function(){
					if(($(this).find('.efl1').val() != '0')&& ($(this).find('.efl1').val() != undefined))
					{			
						premium = $(this).find(".nvp2").val();	
						total_cargo_premium = total_cargo_premium + parseInt(return_numeric(premium)); 						
					}
				});
				$('.row-efl3').each(function(){
					if(($(this).find('.umc').val() != '0')&& ($(this).find('.umc').val() != undefined))
					{			
						premium = $(this).find(".ump").val();	
						total_cargo_premium = total_cargo_premium + parseInt(return_numeric(premium)); 						
					}
				});
				
				var val1=return_numeric($('.unit_price').val());
				var val2=$('.quantity').val();
				var val3=return_numeric($('.unit_price2').val());	
				var val4=$('.quantity2').val();	
				total_cargo_premium = (parseInt(val1*val2)+parseInt(val3*val4)) + total_cargo_premium;
				total_unit = parseInt(val2) + parseInt(val4);
				total_cargo_premium = total_cargo_premium - parseInt(return_numeric($(".disc3").val()));
				if(total_unit != 0)
				{
					unit_price_breakdown = total_cargo_premium / total_unit;
				}
				
				$(".unit_price_breakdown").val(unit_price_breakdown);
				$(".total_liability_premium").val(total_cargo_premium);
				$('.price_format').priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});
			}
			function calctotalcargo(){
				var total_cargo_premium = 0;
				$('.row-efl').each(function(){
					if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != '0') && ($(this).find('.efl').val() != '') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined))
					{
						//alert($(this).find('.efl').val());
						premium=$(this).find('.efp').val();
						//console.log(premium);
						total_cargo_premium = total_cargo_premium + parseInt(return_numeric(premium)); 	
					}
					
				});	
				//alert(total_cargo_premium);
				base_unit=	$('.npu').val();
				base_rate = $("#bsr").val();
				if(base_rate == '') {base_rate = 0; }
				npu_br = 	parseInt(base_unit) * parseInt(return_numeric(base_rate));
				if(npu_br > 0)
				{
					total_cargo_premium = total_cargo_premium + parseInt(npu_br);
				}
				var old_unit_premium = $(".ofp").val();
				if(old_unit_premium != '')
				{
					total_cargo_premium = total_cargo_premium + parseInt(return_numeric(old_unit_premium));
				}
				
				$(".total_cargo").val(total_cargo_premium);
				$(this).parent().parent().find(".efp").priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});
			}
			$(document).ready(function() {
				<!----disabled system---> 
				apply_on_all_elements();
			
				//$('.show_tractor_others').hide();
				$(".mkread").attr("disabled","true"); 
				//$(".total_cargo").attr("disabled","true");		
				$('.efl,.ou').change(function(){
					
					if (($(this).val()=="None") || ($(this).val()=="0") || ($(this).val()=="No"))
					{
						
						$(this).parents('.row-efl').find('.mkread').attr("disabled",true); 		
						$(this).parents('.row-efl').find('.mkread').val(0); 
						calctotalcargo();
					}
					else
					{
						$(this).parents('.row-efl').find('.mkread').attr("disabled",false);
					}
				});
				
				$(".oup").change(function(){
					old_unit = $(".ou").val();
					val = $("#oup").val();
					total = old_unit * val;
					$(".ofp").val(total);
					$('.ofp').priceFormat({
							prefix: '$ ',
							centsLimit: 0,
							thousandsSeparator: ','
						});
					calctotalcargo();
				});
				
				$("#bsr,.efp").keyup(function(){
					calctotalcargo();
				});
				
				$('.npu').change(function(){
					base_unit=	$('.npu').val();
					total_cargo_premium = 0;
					$('.row-efl').each(function(){
						if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined) && ($(this).find('.efl').val() != '0') && ($(this).find('.efl').val() != ''))
						{
							var val=$(this).find('.efc').val();
							premium = base_unit * val;
							$(this).find(".efp").val(premium);	
						}
					});
					$('.price_format').priceFormat({
							prefix: '$ ',
							centsLimit: 0,
							thousandsSeparator: ','
						});
					calctotalcargo();
				});
				
				/*$('.efc').change(function(){
					field = $(this).attr('name');
					base_unit=	$('.no_of_power_unit').val();
					base_rate=	return_numeric($('.base_rate').val());					
					val = $(this).val();		
					//alert(field);
					if(field=='drivers_surcharge_percentage')
					{
						total =  (val * base_rate)/10;
						$(this).parent().parent().find(".efp").val(total);
						$(this).parent().parent().find(".efp").priceFormat({
								prefix: '$ ',
								centsLimit: 1,
								thousandsSeparator: ','
							});
					}else if(field=='lapse_in_coverage_percentage'||field=='new_venture_percentage'||field=='losses_percentage') {
						total =  base_unit * base_rate * val;
						total = total/10;
						$(this).parent().parent().find(".efp").val(total);
						$(this).parent().parent().find(".efp").priceFormat({
								prefix: '$ ',
								centsLimit: 1,
								thousandsSeparator: ','
							});
					}else {
						total =  base_unit * val;
						$(this).parent().parent().find(".efp").val(total);
						$(this).parent().parent().find(".efp").priceFormat({
								prefix: '$ ',
								centsLimit: 0,
								thousandsSeparator: ','
							});
					}
					
					
					calctotalcargo();
				});*/
				
				
				/* liability script starts */
				
					$('.lefl').change(function(){
						
						if (($(this).val()=="None") || ($(this).val()=="0") || ($(this).val()=="No"))
						{
							
							$(this).parents('.row-lefl').find('.mkread').attr("disabled",true); 		
							$(this).parents('.row-lefl').find('.mkread').val(0); 
							calctotalliability();
						}
						else
						{
							$(this).parents('.row-lefl').find('.mkread').attr("disabled",false);
						}
					});
					/*$('.etq').change(function(){
						var trailer_quantity=$(this).val();
						var trailer_cost=$(this).parents('.row-efl1').find('.etc').val();
						var total=parseInt(trailer_quantity)*parseInt(return_numeric(trailer_cost));
						$(this).parents('.row-efl1').find(".etp").val(total);	
						calctotalliability();						
					})	
					$('.etc').change(function(){
						var trailer_quantity=$(this).val();
						var trailer_cost=$(this).parents('.row-efl1').find('.etq').val();
						var total=parseInt(trailer_quantity)*parseInt(return_numeric(trailer_cost));
						$(this).parents('.row-efl1').find(".etp").val(total);
						calctotalliability();						
					})*/
					
					/*$('.lnvc').change(function(){
						var field = $(this).attr('name');
						var val1=return_numeric($('.unit_price').val());
						var val3=return_numeric($('.unit_price2').val());						
						var val2=$(this).val();
						if(field=='drivers_surcharge_a_percentage')
						{
							var total = val1*val2
						}
						else if(field=='drivers_surcharge_b_percentage')
						{
							var total = val3*val2
						}	
						$(this).parent().parent().find(".lnvp").val(total);						
						$('.price_format').priceFormat({
							prefix: '$ ',
							centsLimit: 0,
							thousandsSeparator: ','
						});
						calctotalliability();
					});*/
					/*$('.lnvc').change(function(){
						var field = $(this).attr('name');
						var unit_price=return_numeric($('.unit_price').val());
						var quantity=return_numeric($('.quantity').val());
						var unit_price2=return_numeric($('.unit_price2').val());	
						var quantity2=return_numeric($('.quantity2').val());	
										
						var val2=$(this).val();
						
						if(field=='drivers_surcharge_a_percentage')
						{
							var total = unit_price*val2
						}
						else if(field=='drivers_surcharge_b_percentage')
						{
							var total = unit_price2*val2
						}	
						else{
								var total = unit_price*quantity*unit_price2*quantity2*val2;
						}
						$(this).parent().parent().find(".lnvp").val(total);						
						$('.price_format').priceFormat({
							prefix: '$ ',
							centsLimit: 0,
							thousandsSeparator: ','
						});
						calctotalliability();
					});*/
					$(".umc").change(function(){
						$(this).parents('.row-efl3').find('.ump').val($('.lno_of_power_unit').val()*$(this).val());	
						$('.price_format').priceFormat({
							prefix: '$ ',
							centsLimit: 0,
							thousandsSeparator: ','
						});
						calctotalliability();
					});
					$('.efl1').change(function(){
						if (($(this).val()=="No") || ($(this).val()=="Rejected") || ($(this).val()=="None") || ($(this).val()=="0"))
						{
							$(this).parents('.row-efl1').find('.mkread').attr("disabled",true); 			
						}
						else
						{
							$(this).parents('.row-efl1').find('.mkread').attr("disabled",false);
						}
						calctotalliability();
					});
					$('.efl3').change(function(){
						if (($(this).val()=="No") || ($(this).val()=="Rejected") || ($(this).val()=="None") || ($(this).val()=="0"))
						{
							$(this).parents('.row-efl3').find('.mkread').attr("disabled",true); 			
						}
						else
						{
							$(this).parents('.row-efl3').find('.mkread').attr("disabled",false);
						}
					});
					$('.efl3').change(function(){			
						if ($(this).val()=="Rejected")
						{				
							$(this).parents('.row-efl').find('.nvc2').attr("disabled",true); 
							$(this).parents('.row-efl3').find('.nvc2').val("0");	
							$(this).parents('.row-efl3').find('.ump').val("0");	
						}
						else if ($(this).val()=="15/30")
						{
							$(this).parents('.row-efl3').find('.nvc2').val("50"); 
							$(this).parents('.row-efl3').find('.ump').val($('.lno_of_power_unit').val()*50);		
							
						}
						else if ($(this).val()=="30/60")
						{
							$(this).parents('.row-efl3').find('.nvc2').val("80");
							$(this).parents('.row-efl3').find('.ump').val($('.lno_of_power_unit').val()*80);	 	
							
						}
						else if ($(this).val()=="2500")
						{
						
							$(this).parents('.row-efl3').find('.nvc2').val("750"); 
							$(this).parents('.row-efl3').find('.ump').val($('.lno_of_power_unit').val()*750);		
							
						}			
						else
						{
							$(this).parents('.row-efl').find('.nvc2').attr("disabled",false);
						}
						$('.price_format').priceFormat({
							prefix: '$ ',
							centsLimit: 0,
							thousandsSeparator: ','
						});
						calctotalliability();
					});
					$('.ldsa').change(function(e) {
						if($(this).val()=='Yes'){
                        	$(this).parents('.row-lefl').find('.lnvc').attr("disabled",false);
						}else{
								$(this).parents('.row-lefl').find('.lnvc').attr("disabled",true);
						}
                    });
					/*$(".disc1").change(function(){
						if($(this).val() != 0)
						{
							$(".disc2").attr("disabled",false);
						}else
						{
							$(".disc2").attr("disabled",true);
							$(".disc2").val(0);
							calctotalliability();
						}
					});*/
					/*$(".disc2").change(function(){
						var val1=return_numeric($('.unit_price').val());
						var val2=$('.quantity').val();
						var val3=return_numeric($('.unit_price2').val());	
						var val4=$('.quantity2').val();	
						var val5=$(this).val();
						$(".disc3").val(((val1*val2)+(val3*val4))*val5);	
						calctotalliability();
					});*/

				/* liability script ends */
				/* PD script starts */
				
				
				
				$('.total_pd_pr').change(function(){
				
					//calculatepd();
						
				})	
				$('.tractor_trailer_qunt,.ttcc').change(function(){
					var val5=return_numeric($('.ttcc').val());			
					var val6=$('.tractor_trailer_qunt').val();
					$(".ttcp").val(val5 * val6);	
					$('.price_format').priceFormat({
						prefix: '$ ',
						centsLimit: 0,
						thousandsSeparator: ','
					});
				})
				/*$('.nvc').change(function(){
					
				    var field = $(this).attr('name');
					var val1=return_numeric($('.pd_base_rate').val());
					//var val2=$('.quantity').val();
					var val2=$(this).val();
					if((field=='losses_surcharge_cost')||(field=='miscellaneos_surcharge_cost'))
					{
						var tractor_p = return_numeric($("#total_tractor_premium_pd").val());
						var trailer_p = return_numeric($("#total_trailer_premium_pd").val());
						total = (tractor_p+trailer_p)*val2;
						
					}
					else if(field='driver_surcharge_cost')
					{
						var driver_surcharge = $(".driver_surcharge").val();
						total = driver_surcharge*val2;
					}
					else{
						total = val1*val2;
						alert(total);
					}
					
					$(this).parent().parent().find(".nvp").val(total);					
					$('.price_format').priceFormat({
						prefix: '$ ',
						centsLimit: 0,
						thousandsSeparator: ','
					});
					calculatepd();	
				});*/
				
				/*$(".drc").change(function(){
					$(".drp").val($('.no_of_tractor').val()*$(this).val());
					$('.price_format').priceFormat({
						prefix: '$ ',
						centsLimit: 0,
						thousandsSeparator: ','
					});
					calculatepd();	
				});*/
				
				/*$(".tls").change(function(){
					$(".tlp").val($('.no_of_tractor').val()*$(this).val());
					$('.price_format').priceFormat({
						prefix: '$ ',
						centsLimit: 0,
						thousandsSeparator: ','
					});
					calculatepd();	
				});*/
				
				$('body').on('change','.number_of_trailer',function(e){
					e.preventDefault();
					var total_no = 0;
					var val = $(this).val();
					var currcount = $(".trailerrw").length;
					var diff = currcount - val;
					
					if(diff < 0) {
						while(diff < 0){
							var num     = $('.trailerrw').length+1;
							var newNum  = new Number(num - 1);  
							var newElem = '<tr id="trailerrw'+num+'" class="trailerrw"><td class="vehicle_name">Trailer '+num+'<input type="hidden" name="trailername_'+num+'" value="Trailer '+num+'"></td><td style="text-align:center"><input type="text" class="trailer_value vehicle_value span6 ui-state-valid price_format" value="" name="trailer_val'+num+'"></td><td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="" name="trailer_ded'+num+'"></td><td style="text-align:center"><input type="text" name="trailer_rate'+num+'" class="span6 vehicle_rate ui-state-valid"></td><td style="text-align:center"><input type="text" name="trailer_flat_rate'+num+'" class="span6 numr_valid vehicle_frate ui-state-valid"></td><td style="text-align:center"><input type="text" name="trailer_premium'+num+'" class="span6 trailer_prem price_format_decimal vehicle_rp ui-state-valid"></td></tr>';
							
							if(currcount == 0)
							{
								if($(".tractorrw").length != 0)
									$(".tractorrw:last").after(newElem);
								/*else if($(".truckrw").length != 0)
									$(".truckrw:last").after(newElem);*/
								else
									$("#add_owner_row").prepend(newElem);
								currcount = 1;
							}
							else
							{
								$('.trailerrw:last').after(newElem);	
							}
							$('#trailerrw' + num +' .vehicle_name').html('Trailer '+num);
							diff++;
						} 
					 }
					 else if(diff > 0)
					 {
						while(diff > 0){
						
							 var v = $('tr.trailerrw:last').remove() ; 
							 diff--;
						 }
					 }	
					apply_on_all_elements();
				});
				$('body').on('change','.no_of_truck',function(e){
					e.preventDefault();
					var total_no = 0;
					var val = $(this).val();
					var currcount = $(".truckrw").length;
					var diff = currcount - val;
					
					if(diff < 0) {
						while(diff < 0){
							var num     = $('.truckrw').length+1;
							var newNum  = new Number(num - 1);  
							var newElem = '<tr id="truckrw'+num+'" class="truckrw"><td class="vehicle_name">Truck '+num+'<input type="hidden" name="truckname_'+num+'" value="Truck '+num+'"></td><td style="text-align:center"><input type="text" class="truck_value vehicle_value span6 ui-state-valid price_format" value="" name="truck_val'+num+'"></td><td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="" name="truck_ded'+num+'"></td><td style="text-align:center"><input type="text" name="truck_rate'+num+'" class="span6 vehicle_rate ui-state-valid"></td><td style="text-align:center"><input type="text" name="truck_flat_rate'+num+'" class="span6 numr_valid vehicle_frate ui-state-valid"></td><td style="text-align:center"><input type="text" name="truck_premium'+num+'" class="span6 truck_prem1 vehicle_rp price_format_decimal ui-state-valid"></td></tr>';
							if(currcount == 0)
							{
								if($(".tractorrw").length == 0)
									$("#add_owner_row").prepend(newElem);
								else
									$(".tractorrw:last").after(newElem);
								currcount = 1;
							}
							else
							{
								$('.truckrw:last').after(newElem);	
							}
							$('#truckrw' + num +' .vehicle_name').html('Truck '+num);
							diff++;
						} 
					 }
					 else if(diff > 0)
					 {
						while(diff > 0){
						
							 var v = $('tr.truckrw:last').remove() ; 
							 diff--;
						 }
					 }	
					apply_on_all_elements();
				});
				$('body').on('change','.no_of_tractor',function(e){
					
					e.preventDefault();
					var total_no = 0;
					var val = $(this).val();
					var currcount = $(".tractorrw").length;
					var diff = currcount - val;
					
					if(diff < 0) {
						while(diff < 0){
							var num     = $('.tractorrw').length+1;
							var newNum  = new Number(num - 1);  
							var newElem = '<tr id="tractorrw'+num+'" class="tractorrw"><td class="vehicle_name">Tractor '+num+'<input type="hidden" name="tractorname_'+num+'" value="Tractor '+num+'"></td><td style="text-align:center"><input type="text" class="tractor_value vehicle_value span6 ui-state-valid price_format" value="" name="tractor_val'+num+'"></td><td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="" name="tractor_ded'+num+'"></td><td style="text-align:center"><input type="text" name="tractor_rate'+num+'" class="span6 vehicle_rate ui-state-valid"></td><td style="text-align:center"><input type="text" name="tractor_flat_rate'+num+'" class="span6 numr_valid vehicle_frate ui-state-valid"></td><td style="text-align:center"><input type="text" name="tractor_premium'+num+'" class="span6 tractor_prem price_format_decimal vehicle_rp ui-state-valid"></td></tr>';
							
							if(currcount == 0)
							{
								$("#add_owner_row").prepend(newElem);
								currcount = 1;
							}
							else
							{
								$('.tractorrw:last').after(newElem);	
							}
							$('#tractorrw' + num +' .vehicle_name').html('Tractor '+num);
							diff++;
						} 
					 }
					 else if(diff > 0)
					 {
						while(diff > 0){
						
							 var v = $('tr.tractorrw:last').remove() ; 
							 diff--;
						 }
					 }	
					apply_on_all_elements();
				});
				
				$('body').on('blur','.tractor_value',function(e){
					totallimit = 0;
					$(".tractor_value").each(function(){
						totallimit = totallimit + parseInt(return_numeric($(this).val()));
					});
					
					$(".total_tractor_limit").val(totallimit);
					apply_on_all_elements();
				});
				$('body').on('blur','.trailer_value',function(e){
					totallimit = 0;
					$(".trailer_value").each(function(){
						totallimit = totallimit + parseInt(return_numeric($(this).val()));
					});
					
					$(".total_trailer_limit").val(totallimit);
					apply_on_all_elements();
				});
				$('body').on('blur','.tractor_value',function(e){
					totallimit = 0;
					$(".tractor_value").each(function(){
						totallimit = totallimit + parseInt(return_numeric($(this).val()));
					});
					
					$(".total_tractor_limit").val(totallimit);
					apply_on_all_elements();
				});
				/**By Ashvin Patel 12/may/2014**/
				$('body').on('blur','.truck_value',function(e){
					totallimit = 0;
					$(".truck_value").each(function(){
						totallimit = totallimit + parseInt(return_numeric($(this).val()));
					});
					
					$(".total_truck_limit").val(totallimit);
					var total_truck_limit = return_numeric($(".total_truck_limit").val());
					var total_truck_pre = return_numeric($("#total_truck_premium_pd").val());
					total_pd_pre = parseInt(total_truck_limit)+parseInt(total_truck_pre);
					$("#total_pd_pr").val(total_pd_pre);
					apply_on_all_elements();
				});
				
				
				
				//* PD Script ends */
				
	<!--integer validation---->
				$(".numr_valid").keypress(function(evt){				
					var charCode = (evt.which) ? evt.which : event.keyCode;
					if (charCode != 46 && charCode > 31
					&& (charCode < 48 || charCode > 57))
					{
						//alert('Please enter numeric value');
						$(this).css("border", "1px solid #FAABAB");
						$(this).css("box-shadow", "0 0 5px rgba(204, 0, 0, 0.5");
						
						$(this).focus();
						return false;
					}			
					//return true;
					
	else {
						$(this).css("border", "1px solid #CCCCCC");
						$(this).css("box-shadow", "none");
					}
				});				
	<!--integer validation---->		
					
									
			});
			function calculatepd(){
				var tot_nvp_pre = 0;
					$('.nvp').each(function() {
                        tot_nvp_pre = tot_nvp_pre + parseInt(return_numeric($(this).val()));
                    });
					var scp = return_numeric($(".scp").val());
					var drp = return_numeric($(".drp").val());
					var tlp = return_numeric($(".tlp").val());
					var total_tractor_pre = return_numeric($("#total_tractor_premium_pd").val());
					var total_trailer_pre = return_numeric($("#total_trailer_premium_pd").val());
					total_pd_pre = parseInt(tot_nvp_pre) + parseInt(scp) + parseInt(drp) + parseInt(tlp) + parseInt(total_tractor_pre) + parseInt(total_trailer_pre);
					
					$("#total_pd_pr").val(total_pd_pre);
					/**By Ashvin Patel 12/may/2014**/
					$('.price_format').priceFormat({
						prefix: '$ ',
						centsLimit: 0,
						thousandsSeparator: ','
					});
				}
				</script>
		
	</head>

	<body>

		   

		<div class="container-fluid">
			   <div class="sheet_container">
					<div class="row-fluid">
						
						
					</div>
                    
                    <br />
				   <div class="row-fluid">
						<span class="span12">
						  
						  <div class="well pull-center well-3 pga sheet-header">
							<h4 class="heading pull-center heading-3" style="">Cargo Rate sheet</h4>           
						  </div>
						</span>
					</div>
                    <div class="well well-3 well sheet-content">
						
							<?php
								$attributes = array('class' => 'request_ratesheet', 'id' => 'main_form2');
							
							
							?>   
						 
						   	<form class="request_ratesheet" id="main_form2" action="javascript:void(0);" method="post">
							<input type="hidden" name="producer_email" value="<?php echo $quote->email; ?>">
                            <input type="hidden" name="underwriter_ids" value="<?php echo $quote->assigned_underwriter;?>">
							<div class="row-fluid">
								<?php
									$ratesheet_broker_name = '';
									
								?>
							 
							  <span class="span6">
								<h5 class="heading  heading-4">Broker</h5>
							   <input type="text" class="span9 brokername broker" name="broker" placeholder="Full Name/ DBA" value="<?php echo $quote->contact_name.' '.$quote->contact_middle_name.' '.$quote->contact_last_name; ?>"  <?php    if (!empty($ratesheet_broker_name)) { echo "disabled";} ?> >
							  </span>
							  <span class="span6">
								<h5 class="heading ">Insured</h5>
								<input type="text" class="span9 insuredname insured" name="insured" placeholder="Full Name/ DBA" value="<?php echo $quote->insured_fname.' '.$quote->insured_mname.' '.$quote->insured_lname; ?>">
							  </span>
							</div>
							<div class="row-fluid">
							  <span class="span2">
								<h5 class="heading  heading-3 hed-3">Radius of Operation</h5>
								<!--<input class="textinput span10 pull-left textinput-3 textinput-4 radiusofoperation" type="text" name="radius_of_operation" placeholder="">-->                
								<select name="radius_of_operation" class="select-3 span12 tractor_radius_of_operation radius_of_operation" id="tractor_radius_of_operation" onChange="rofoperation(this.value);">
									<option value="0-100 miles" <?php if($quote_vehicle[0]->radius_of_operation == '0-100 miles') echo 'selected' ?> >0-100 miles</option>
									<option value="101-500 miles" <?php if($quote_vehicle[0]->radius_of_operation == '101-500 miles') echo 'selected' ?>>101-500 miles</option>           	
									<option value="501+ miles" <?php if($quote_vehicle[0]->radius_of_operation == '501+ miles') echo 'selected' ?>>501+ miles</option>
									<option value="other" <?php if($quote_vehicle[0]->radius_of_operation == 'other') echo 'selected' ?>>Other</option>            
								</select><br>
								<input class="textinput span12 pull-left show_tractor_others" type="text" name="claim_venue" value="<?php echo $quote_vehicle[0]->radius_of_operation_other	; ?>" placeholder="" <?php if($quote_vehicle[0]->radius_of_operation == 'other') echo 'style="display:block"'; else echo 'style="display:none"' ?>>
							  </span>
							  <span class="span1">		
								<h5 class="heading  heading-4 hed-3">State</h5>
								<div class="textinput span12 ">
									<?php
										$attr = "class='span12 ui-state-valid state' onChange='changeCarrier(this.value, &quot;cargo&quot;)'";
										//echo $quote->insured_state;
										if($quote->insured_state != '') $broker_state = $quote->insured_state; else $broker_state = 'CA';
									?>
									<?php echo get_state_dropdown('state', $broker_state, $attr); ?>
								</div>
							  </span>
							   <span class="span1">
								<h5 class="heading  heading-4 hed-3">Claim Venue</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4 claim_venue" type="text" name="claim_venue" placeholder="">
							   
							  </span>
							  <span class="span2">
								<h5 class="heading  heading-4 hed-3">Number of Power Units</h5>
								<input class="textinput span10 pull-left textinput-3 textinput-4 npu no_of_power_unit" type="text" name="no_of_power_unit" placeholder="" value="<?php echo count($quote_vehicle); ?>">
							   
							  </span>
							   <span class="span1">
								<h5 class="heading  heading-4 hed-3"> Limit</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4 limit" type="text" name="limit" placeholder="" value="<?php if($quote_vehicle[0]->cargo != '') echo $quote_vehicle[0]->cargo; ?>">
							   
							  </span>
							   <span class="span1">
								<h5 class="heading  heading-4 hed-3">Deductible</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4 deductible" type="text" name="deductible" placeholder="" value="<?php if($quote_vehicle[0]->cargo_ded != '') echo $quote_vehicle[0]->cargo_ded; ?>">
							   
							  </span>  
							   <span class="span2">
								<h5 class="heading  heading-4 hed-3"> Reffer Deductible</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4 reffer_deductible" type="text" name="reffer_deductible" placeholder="">
							   
							  </span>      
							   <span class="span2" >
								<h5 class="heading  heading-4 hed-3">Base rate</h5>
								<input class="textinput span10 pull-left textinput-3 textinput-4 price_format base_rate" type="text" name="base_rate" placeholder="" id="bsr">
							   
							  </span>              
							</div>
							<div class="mid-wrapper">	
								 <strong>Optional endorsement   &nbsp; &nbsp; &nbsp; *** Not affered to all clients***</strong><br /><br />
								  <div class="row-fluid pull-center row-efl" style="text-align:right"> 
									 <span class="span4">
									 <h5 class="heading pull-left heading-4" style="text-align:right">Earned Freight</h5>&nbsp;
									 <select name="earned_freight_limit" class="select-3 span6 efl" id="">
									  <option value="None">None</option>
									   <option value="2500">$ 2,500</option>
										<option value="3000">$ 3,000</option>
													
									</select>
								   
									  </span>
										<span style=" padding: 0px 0px 5px 14px;display: block;        float: left;    ">@</span> 
									   <span class="span2">           
									   <!-- <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
										<select name="earned_freight_cost" class="select-3 span12 mkread efc">
										  <option value="0">0</option>
										   <option value="100">$ 100 </option>
											<option value="200">$ 200</option>
														
										</select>
									   
									  </span>
									  <span class="span3">
										<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
									   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp price_format" type="text" name="earned_freight_premium" placeholder="" >
									   
									  </span>
								  </div>
								 <div class="row-fluid pull-center row-efl" style="text-align:right"> 
									 <span class="span4">
									<h5 class="heading pull-left heading-4" style="text-align:right">Debris removal</h5>&nbsp;
									 <select name="debris_removal_limit " class="select-3 span6 efl">
									  <option value="None">None</option>
									  <option value="2500">$ 2,500</option>              
									</select>
								   
								  </span>
									 <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
								   <span class="span2">           
									<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
								   <select name="debris_removal_cost" class="select-3 span12 mkread efc" id="drc">
									  <option value="0">0</option>
									   <option value="150">$ 150 </option>                                
									</select>
								  </span>
								  <span class="span3">
									<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
								   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp price_format" type="text" name="debris_removal_premium" placeholder="" >
								   
								  </span>
								 </div>
								  <div class="row-fluid pull-center row-efl" style="text-align:right"> 
									 <span class="span4">
									<h5 class="heading pull-left heading-4" style="text-align:right">Tourpolin coverage</h5>&nbsp;
									 <select name="tarpauling_coverage_limit" class="select-3 span6 efl" >
									  <option value="None">None</option>
									   <option value="5000">$ 5,000</option>              
									</select>
								   
									  </span>
										 <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
									   <span class="span2">           
										<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
										<select name="tarpauling_coverage_cost" class="select-3 span12 mkread efc" id="tcc">
										  <option value="0">0</option>
										   <option value="1000">$ 1,000 </option>                                
										</select>
									   
									  </span>
									  <span class="span3">
										<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
									   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp price_format" type="text" name="tarpauling_coverage_premium" placeholder="" >
									  </span>
								  </div>
								 
								 
								  <div class="row-fluid pull-center" style="text-align:right"> 
										 <span class="span6">
										<h5 class="heading pull-left heading-4" style="text-align:right">Reefer breakdown</h5>&nbsp;
											<!-- <input class="textinput span8 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
											 <select name="reefer_breakdown_limit" class="select-3 span6 ">
										  <option value="None">None</option>
										  <option value="Included in Base Rate">Included in Base Rate</option>
										</select>
											
									  </span>
									  
								  </div>
									<br />    
									<strong > Optional endoresment</strong>
									 <div class="ho">
										 <div class="row-fluid pull-center row-efl" style="text-align:right"> 
											 <span class="span4">
											<h5 class="heading pull-left heading-4" style="text-align:right">Lapse in coverage</h5>&nbsp;
											 <select name="lapse_in_coverage_surcharge" class="select-3 span6 efl">
											  <option value="No">No</option>
												<option value="Yes">Yes</option>           
											</select>
										   
										  </span>
											 <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
										   <span class="span2">           
											<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
										   <select name="lapse_in_coverage_percentage" class="select-3 span12 mkread efc" id="lcc">
											  <option value="0">0</option>
												<option value="5">5%</option>    
												 <option value="10">10%</option>         
											</select>
										  </span>
										  <span class="span3">
											<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
										   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp price_format_decimal" type="text" name="lapse_in_coverage_premium" placeholder="" >
										   
										  </span>
										 </div>
										 <div class="row-fluid pull-center row-efl" style="text-align:right"> 
											 <span class="span4">
											<h5 class="heading pull-left heading-4" style="text-align:right">New venture</h5>&nbsp;
											 <select name="new_venture_surcharge" class="select-3 span6 efl">
											  <option value="No">No</option>
											   <option value="Yes">Yes</option>             
											</select>
										   
										  </span>
											 <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
										   <span class="span2">           
											<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
											<select name="new_venture_percentage" class="select-3 span12 mkread efc" id="nvc">
											  <option value="0">0</option>
												<option value="5">5%</option>    
												 <option value="10">10%</option>         
											</select>
										   
										  </span>
										  <span class="span3">
											<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
										   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp price_format_decimal" type="text" name="new_venture_premium" placeholder="" >
										   
										  </span>
										 </div>
										  <div class="row-fluid pull-center row-efl" style="text-align:right"> 
											 <span class="span4">
											<h5 class="heading pull-left heading-4" style="text-align:right">Losess</h5>&nbsp;
											 <select name="losses_surcharge" class="select-3 span6 efl">
											  <option value="None">None</option>
											   <option value="Yes">Yes</option>             
											</select>
										   
											  </span>
												 <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
											   <span class="span2">           
												<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="losses_percentage" placeholder="">    -->           	
												<select name="losses_percentage" class="select-3 span12 mkread efc" id="lc">
												   <option value="0">0</option>
												   <option value="5">5%</option>
												   <option value="10">10%</option>
												   <option value="15">15%</option>
												   <option value="20">20%</option>
												   <option value="25">25%</option>
												   <option value="30">30%</option>
												   <option value="35">35%</option>
																
												</select>
											  </span>
											  <span class="span3">
												<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
											   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp price_format_decimal" type="text" name="losses_premium" placeholder="" >
											   
											  </span>
										  </div>
										 <div class="row-fluid pull-center row-efl" style="text-align:right"> 
											 <span class="span4">
											<h5 class="heading pull-left heading-4" style="text-align:right">Drivers</h5>&nbsp;
											 <select name="drivers_surcharge" class="select-3 span6 efl">
											  <option value="None">None </option>
											   <option value="Yes">Yes</option>                                
											</select>
										   
										  </span>
											 <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
										   <span class="span2">           
										   <!-- <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
											<select name="drivers_surcharge_percentage" class="select-3 span12 mkread efc" id="dsc">
											   <option value="0">0</option>
											   <option value="5">5%</option>
											   <option value="10">10%</option>
											   <option value="15">15%</option>
											   <option value="20">20%</option>
											   <option value="25">25%</option>
											   <option value="30">30%</option>
											   <option value="35">35%</option>                                
											</select>
										   
										  </span>
										  <span class="span3">
											<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
										   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp price_format_decimal" type="text" name="driver_surcharge_premium" placeholder="" >
										   
										  </span>
										  </div>
									 
											<div class="row-fluid pull-center row-efl" style="text-align:right"> 
												 <span class="span4">
												<h5 class="heading pull-left heading-4" style="text-align:right">Old units</h5>&nbsp;
												 <input class="textinput span6 pull-left textinput-3 textinput-4 ou numr_valid" type="text" name="old_units" value='0'>
												 
												 <!--<select name="old_units" class="select-3 span6 efl efl2">
												  <option value="No">No</option>
												   <option value="Yes">Yes</option>                                
												</select>-->
											   
											  </span>
												 <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
											   <span class="span2">           
												<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
												<select name="old_units_surcharge" class="select-3 span12 mkread oup" id="oup">                  
												   <option value="0">0</option>     
												   <option value="200">$200</option>                           
												</select>
											  </span>
											  <span class="span3">
												<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
											   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread ofp price_format" type="text" name="old_units_premium" placeholder="" >
											   
											  </span>
											</div>
									</div>
							 </div>
							  <div class="row-fluid">
								  <span class="custom_span2">
									<h5 class="heading  heading-4 total_cargo">Total cargo premium</h5>
									<input class="textinput span12 pull-left textinput-3 textinput-4 total_cargo price_format" type="text" name="total_cargo_premium" placeholder="" id="total_cargo_premium1" readonly>
								   
								  </span>
								  <span class="span3">
									<h5 class="heading  heading-4">Carrier</h5>
									<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="carrier" placeholder="">-->
                                    <div class="cargo_carrier_drop_down">
										<?php
                                            $attr1 = "id='carrier_cargo' class='span12 ui-state-valid select-required' onchange='updatePolicyValue(&quot;cargo&quot;, this.value)'";
                                            $broker_state = '';
                                             if($carriers!=''){ $carriers = $carriers; } else {$carriers = array();}
                                        ?>
                                        <?php echo form_dropdown('carriers', $carriers,$broker_state, $attr1); ?>
                                    </div>
								  </span>
								   <span class="span2">
									<h5 class="heading  heading-4">Cargo police fee</h5>
									<input class="textinput span10 pull-left textinput-3 textinput-4 price_format " type="text" name="cargo_policy_fee" placeholder="" id="cargo_policy_fee">
								   
								  </span>
								   <span class="span2">
									<h5 class="heading  heading-4">Cargo filing fee</h5>
									<input class="textinput span10 pull-left textinput-3 textinput-4 numr_valid price_format text-required" type="text" name="cargo_filing_fee" placeholder="" value="0" maxlength="" ><!--onKeyPress="return isNumberKey(event)"-->
								   
								  </span>
								  <span class="span2">
									<h5 class="heading  heading-4"> Cargo SLA tax</h5>
									<input class="textinput span10 pull-left textinput-3 textinput-4 price_format text-required" type="text" name="cargo_sla_tax" placeholder="" id="cargo_sla_tax">
								   
								  </span>
								   
							</div>
							  <div class="row-fluid">
								  <span class="span7">
									<h5 class="heading ">Commodities</h5>
									<input class="textinput span15" type="text" placeholder="" name="commodities" value="<?php echo $quote_vehicle[0]->commodities_haulted; ?>">
								  </span>
								   <span class="span4">
									<h5 class="heading  heading-4">Required For FIRM quote</h5>
									<input class="textinput span8 pull-left textinput-3 textinput-4" type="text" name="required_for_firm_quote" placeholder="">
								  </span>
							 </div>
							  <div class="row-fluid">
								  <span class="span12">
									<h5 class="heading pull-left">Comments &nbsp;&nbsp;</h5>
									<input class="textinput span9" type="text" placeholder="" name="comments">
								  </span>
							 </div>
							<!-- <input type="hidden" class="cargo_id" value="" name="cargo_id">-->
							 <input type="hidden" name="bundleid" class="bundleid" value="">
								<div id='loader' style="display:none"><img src="<?php echo base_url(); ?>images/spinner.gif"/></div><div id="cargo_sheet_message"></div>
								 <button class="btn btn-primary" id="cargo_submit" type="submit" >Save Draft</button>
						<!--onClick="form_sub()"-->
							   <input type="hidden" name="for_submit" value="1" />
                                <input type="hidden" name="quote_id" value="<?php echo $quote_id; ?>" id="quote_id"/>
                                
                                <input type="hidden" id="base_url" name="" value="<?php echo base_url();?>" />
								<input type="hidden" name="action" value="<?php echo $action; ?>" />
								
							</form>
							
					   
					   </div>
			   </div>
			   
			   <script>
			  
			   </script>
			   <div class="sheet_container">
					<div class="row-fluid">
						<span class="span12">
						  
						  <div class="well pull-center well-3 bga sheet-header">
							<h4 class="heading pull-center heading-3">Physical Damage Rate Sheet</h4>           
						  </div>
						</span>
				    </div>
					<div class="well well-3 well sheet-content">
						<?php
							//$attributes = array('class' => 'request_ratesheet_physical_damage', 'id' => 'main_form4');
							//echo form_open_multipart(base_url('ratesheet_physical_damage'), $attributes);
							
						?> 
                        <?php
								$attributes = array('class' => 'request_ratesheet', 'id' => 'main_form3');					
							
						?>  
                        <form class="request_ratesheet" id="main_form3" action="javascript:void(0);" method="post">
						<input type="hidden" name="producer_email" value="<?php echo $quote->email; ?>">
							<div class="row-fluid">
							
                        
                        
						<div class="row-fluid">
						  <span class="span6">
							<h5 class="heading  heading-4">Broker</h5>
						     <input type="text" class="span9" name="broker" placeholder="Full Name/ DBA	" value="<?php echo $quote->contact_name.' '.$quote->contact_middle_name.' '.$quote->contact_last_name; ?>">
						  </span>
						  <span class="span6">
							<h5 class="heading ">Insured</h5>
							<input type="text" class="span9" name="insured" placeholder="Full Name/ DBA	" value="<?php echo $quote->insured_fname.' '.$quote->insured_mname.' '.$quote->insured_lname; ?>">
						  </span>
						</div>
						<div class="row-fluid">
						  <span class="custom_span3">
						   <h5 class="heading  heading-3 hed-3">Radius of Operation</h5>
								<!--<input class="textinput span10 pull-left textinput-3 textinput-4 radiusofoperation" type="text" name="radius_of_operation" placeholder="">-->                
								<select name="radius_of_operation" class="select-3 span12 tractor_radius_of_operation" id="tractor_radius_of_operation" onChange="rofoperation(this.value);">
									<option value="0-100 miles" <?php if($quote_vehicle[0]->radius_of_operation == '0-100 miles') echo 'selected' ?> >0-100 miles</option>
									<option value="101-500 miles" <?php if($quote_vehicle[0]->radius_of_operation == '101-500 miles') echo 'selected' ?>>101-500 miles</option>           	
									<option value="501+ miles" <?php if($quote_vehicle[0]->radius_of_operation == '501+ miles') echo 'selected' ?>>501+ miles</option>
									<option value="other" <?php if($quote_vehicle[0]->radius_of_operation == 'other') echo 'selected' ?>>Other</option>                      
								</select><br>
									
							
						   
						  </span>
						  <span class="span2" <?php if($quote_vehicle[0]->radius_of_operation == 'other') echo 'style="display:block"'; else echo 'style="display:none"' ?>>
							<h5 class="heading  heading-4 hed-3">Others</h5>
						   <input class="textinput span12 pull-left show_tractor_others" type="text" name="claim_venue" value="<?php echo $quote_vehicle[0]->radius_of_operation_other	; ?>" placeholder="" >
						   
						  </span>
						   <span class="span2">
								<h5 class="heading  heading-4 hed-3">State</h5>
							
									<?php
									$attr = "class='span12 ui-state-valid' onchange='changeCarrier(this.value, &quot;pd&quot;)'";
									
									if($quote->insured_state != '') $broker_state = $quote->insured_state; else $broker_state = 'CA';
									?>
									<?php echo get_state_dropdown('state', $broker_state, $attr); ?>
						   
						  </span>
						   <span class="span2" >
							<h5 class="heading  heading-4 hed-3">Base rate</h5>
							<input class="textinput span10 pull-left textinput-3 textinput-4 price_format pd_base_rate" type="text" name="base_rate" placeholder="">
						   
						  </span>              
						</div>
						<div class="hold">
							 <div class="row-fluid pull-center row-efl" style="text-align:right"> 
								<span class="span5">
									<h5 class="heading pull-left heading-4" style="text-align:right">Losses</h5>&nbsp;
									 <select name="losses_surcharge" class="select-3 span6 efl">
									  <option value="None">None</option>
									   <option value="Yes">Yes</option>              
									</select> 
							   </span>
								<span style="padding: 0px 0px 5px 14px;display: block;float: left;">@</span> 
							  
							   <span class="span2">           
									<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
									<select name="losses_surcharge_cost" class="select-3 span12 mkread loses_cost nvc">
										<option value="0">0</option>	
										<option value="5">5%</option> 
										<option value="10">10%</option> 
										<option value="15">15%</option> 
										<option value="20">20%</option> 
										<option value="25">25%</option> 
										<option value="30">30%</option>             
									</select>
								   
							  </span>
							  <span class="span3">
								<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
							   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread lsp nvp price_format_decimal" type="text" name="losses_surcharge_premium" placeholder="" >
							   
							  </span>
							</div>
							 <div class="row-fluid pull-center row-efl" style="text-align:right"> 
								<span class="span5">
									<h5 class="heading pull-left heading-4" style="text-align:right">Misc.surcharges</h5>&nbsp;
									 <select name="miscellaneos_surcharge" class="select-3 span6 efl">
									  <option value="None">None</option>
									   <option value="Yes">Yes</option>                             
									</select>
							   
								</span>
								<span style="padding: 0px 0px 5px 14px;display: block;  float: left;">@</span> 
							  
							  <span class="span2">           
								<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
										<select name="miscellaneos_surcharge_cost" class="select-3 span12 mkread mis_cost nvc">
											<option value="0">0</option>	
											<option value="5">5%</option> 
											<option value="10">10%</option> 
											<option value="15">15%</option> 
											<option value="20">20%</option> 
											<option value="25">25%</option> 
											<option value="30">30%</option>             
										</select>
							  </span>
							  <span class="span3">
								<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
							   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread msp nvp price_format_decimal" type="text" name="miscellaneos_surcharge_premium" placeholder="" >
							   
							  </span>
							</div>
							<div class="row-fluid pull-center row-efl" style="text-align:right"> 
								 <span class="span5">
								<h5 class="heading pull-left heading-4" style="text-align:right">Driver surcharges(value)</h5>&nbsp;
										  
								<input class="textinput span6 pull-left textinput-3 textinput-4 select-3 span4 driver_surcharge numr_valid efl" type="text" name="driver_surcharge" placeholder="">
							   
							  </span>
								<span style="   padding: 0px 0px 5px 14px;    display: block;    float: left;">@</span> 
							  <span class="span2">           
								<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							   <select name="driver_surcharge_cost" class="select-3 span12 driver_cost mkread mis_cost nvc" >
								  <option value="0">0</option>	
								  <option value="5">5%</option> 
								   <option value="10">10%</option> 
									<option value="15">15%</option> 
									 <option value="20">20%</option> 
									  <option value="25">25%</option> 
									   <option value="30">30%</option>             
								</select>
							  </span>
							  <span class="span3">
								<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
							   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread dsp nvp price_format_decimal" type="text" name="driver_surcharge_premium" placeholder="" >
							   
							  </span>
							</div>
							<div class="row-fluid pull-center row-efl" style="text-align:right"> 
						  <span class="span5">
							<h5 class="heading pull-left heading-4" style="text-align:right">15 year tractor/trailer quantity</h5>&nbsp;
							<input class="select-3 span3 tractor_trailer_qunt numr_valid efl" type="text" name="tractor/trailer_quantity" placeholder="">
						  </span>
						  <span style="padding: 0px 0px 5px 14px;display: block;float: left;">@</span> 
						  
						  <span class="span2">           
							<input class="textinput span12 pull-left textinput-3 textinput-4 ttcc mkread mis_cost price_format" type="text" name="tractor/trailer_surcharge_cost" placeholder="$200" value="200" >
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 ttcp scp price_format mkread" type="text" name="tractor/trailer_surcharge_premium" placeholder="" >
						  </span>
						</div>
						</div>
						<strong > Optional endrosements:</strong>
						<div class="row-fluid pull-center row-efl" style="text-align:right"> 
							<span class="span4">
								<h5 class="heading pull-left heading-4" style="text-align:right">Debris removal</h5>&nbsp;
								<select name="debris_removal_limit" class="select-3 span4 efl">
								  <option value="None">None</option>
								   <option value="2,500">$2,500</option>                            
								</select> 
							</span>
							<span style="    padding: 0px 0px 5px 14px;    display: block;    float: left;">@</span> 
							  <span class="span2">           
								<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
								<select name="debris_removal_cost" class="select-3 span12 mkread drc">
								  <option value="0">0</option>
								   <option value="150">$150</option>                            
								</select> 
							  </span>
							  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread drp price_format" type="text" name="debris_removal_premium" placeholder="" >
						  </span>
						</div>
						<div class="row-fluid pull-center row-efl" style="text-align:right"> 
							 <span class="span4">
							<h5 class="heading pull-left heading-4" style="text-align:right">Towing labor storage</h5>&nbsp;
							 <select name="towing_labor_storage" class="select-3 span4 efl">
							  <option value="None">None</option>
							  <option value="7500">$7,500</option>                             
							</select> 
						  
						  </span>
						  <span style="padding: 0px 0px 5px 14px; display: block; float: left;">@</span> 
						  
						   <span class="span2">           
							<select name="towing_labor_storage_cost" class="select-3 span12 mkread tls">
							  <option value="None">None</option>
							  <option value="200">$200</option>                             
							</select> 
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread tlp price_format" type="text" name="towing_labor_storage_premium" placeholder="" >
						   
						  </span>
						</div>
						<div class="row-fluid pull-center" style="text-align:right"> 
						 <span class="span4">
							<h5 class="heading pull-left heading-4" style="text-align:right">Number of Tractor</h5>&nbsp;
								 <input class="textinput span5 pull-left textinput-3 textinput-4 select-3 span4 no_of_tractor" type="text" name="number_of_tractor" placeholder="" onChange="add_vehicle(this.value)" id="no_of_vehicle" value="<?php echo count($tractor_vehicle); ?>" data-min="<?php echo count($tractor_vehicle); ?>">
						   
						  </span>
						   <span class="span3">  
							<h5 class="heading pull-left heading-4" style="text-align:right">Number of Truck</h5>&nbsp;         
							<input class="textinput span5 pull-left textinput-3 textinput-4 select-3 span4 trkcnt no_of_truck" type="text" name="no_of_truck" placeholder="" id="no_of_truck" value="<?php echo count($truck_vehicle); ?>">
						  </span>
						   <span class="span3">  
							<h5 class="heading pull-left heading-4" style="text-align:right">Number of Trailer</h5>&nbsp;         
							<input class="textinput span5 pull-left textinput-3 textinput-4 select-3 span4 trailercnt number_of_trailer" type="text" name="number_of_trailer" placeholder="" id="no_of_trailer" value="<?php echo count($trailer_vehicle); ?>">
						  </span>
						</div>
						
						<div class="tb">
								<table width="100%" border="1">
									<thead>
										  <tr style="background-color:#CCCCCC; color:#000000; text-weight:bold;">
											<th width="16%">Vehicles</th>
											<th width="19%">value/limit</th>
											<th width="15%">Deductible</th>
											<th width="16%">% Rate</th>
											<th width="18%">or Rate</th>
											<th width="16%">Premium</th>
										  </tr>
									</thead>
									<tbody id="add_owner_row">
										<?php 
										$tractorcnt = 0;
										$tot_tractor_value = 0;
										$tot_liability_ded = 0;
										//print_r($tractor_vehicle);
										foreach($tractor_vehicle as $tractor)
										{
											
											$tractorcnt++;
											$tot_tractor_value += getAmount($tractor->pd);
											$tot_liability_ded += getAmount($tractor->liability_ded);
											?>
												<tr id="tractorrw<?php echo $tractorcnt; ?>" class="tractorrw">
													<td class="vehicle_name">Tractor <?php echo $tractorcnt; ?><input type="hidden" name="tractorname_<?php echo $tractorcnt; ?>" value="Truck <?php echo $tractorcnt; ?>"></td>
													<td style="text-align:center"><input type="text" class="tractor_value vehicle_value span6 ui-state-valid price_format" value="<?php echo $tractor->pd; ?>" name="tractor_val<?php echo $tractorcnt; ?>"></td>
													<td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="<?php echo $tractor->pd_ded; ?>" name="tractor_ded<?php echo $tractorcnt; ?>"></td>
													<td style="text-align:center"><input type="text" name="tractor_rate<?php echo $tractorcnt; ?>" class="span6 vehicle_rate ui-state-valid"></td>
													<td style="text-align:center"><input type="text" name="tractor_flat_rate<?php echo $tractorcnt; ?>" class="span6 numr_valid vehicle_frate ui-state-valid"></td>
													<td style="text-align:center"><input type="text" name="tractor_premium<?php echo $tractorcnt; ?>" class="span6 tractor_prem vehicle_rp price_format_decimal ui-state-valid"></td>
												</tr>
												
											<?php
										}
										?>
										
										<?php 
										$truckcnt = 0;
										$tot_truc_value = 0;
										$truc_liability_ded = 0;
										foreach($truck_vehicle as $truck)
										{
											$truckcnt++;
											$tot_truc_value += getAmount($truck->pd);
											$truc_liability_ded += getAmount($truck->liability_ded);
											?>
												<tr id="truckrw<?php echo $truckcnt; ?>" class="truckrw">
													<td class="vehicle_name">Truck <?php echo $truckcnt; ?><input type="hidden" name="truckname_<?php echo $truckcnt; ?>" value="Truck <?php echo $truckcnt; ?>"></td>
													<td style="text-align:center"><input type="text" class="truck_value vehicle_value span6 ui-state-valid price_format" value="<?php echo $truck->pd; ?>" name="truck_val<?php echo $truckcnt; ?>"></td>
													<td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="<?php echo $truck->pd_ded; ?>" name="truck_ded<?php echo $truckcnt; ?>"></td>
													<td style="text-align:center"><input type="text" name="truck_rate<?php echo $truckcnt; ?>" class="span6 vehicle_rate ui-state-valid"></td>
													<td style="text-align:center"><input type="text" name="truck_flat_rate<?php echo $truckcnt; ?>" class="span6 numr_valid vehicle_frate ui-state-valid"></td>
													<td style="text-align:center"><input type="text" name="truck_premium<?php echo $truckcnt; ?>" class="span6 truck_prem1 vehicle_rp price_format_decimal ui-state-valid"></td>
												</tr>
											<?php
										}
										?>
										
										<?php 
										$tot_trailer_value = 0;
										$trailercnt = 0;
										$trail_liability_ded = 0;
										foreach($trailer_vehicle as $trailer)
										{
											
											$trailercnt++;
											$tot_trailer_value += getAmount($trailer->pd);
											$trail_liability_ded += getAmount($tractor->liability_ded);
											?>
												<tr id="trailerrw<?php echo $trailercnt; ?>" class="trailerrw">
													<td class="vehicle_name">Trailer <?php echo $trailercnt; ?><input type="hidden" name="trailername_<?php echo $trailercnt; ?>" value="Trailer <?php echo $trailercnt; ?>"></td>
													<td style="text-align:center"><input type="text" class="trailer_value vehicle_value span6 ui-state-valid price_format" value="<?php echo $trailer->pd; ?>" name="trailer_val<?php echo $trailercnt; ?>"></td>
													<td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="<?php echo $trailer->pd_ded; ?>" name="trailer_ded<?php echo $trailercnt; ?>"></td>
													<td style="text-align:center"><input type="text" name="trailer_rate<?php echo $trailercnt; ?>" class="span6 vehicle_rate ui-state-valid"></td>
													<td style="text-align:center"><input type="text" name="trailer_flat_rate<?php echo $trailercnt; ?>" class="span6 numr_valid vehicle_frate ui-state-valid"></td>
													<td style="text-align:center"><input type="text" name="trailer_premium<?php echo $trailercnt; ?>" class="span6 trailer_prem vehicle_rp price_format_decimal ui-state-valid"></td>
												</tr>
												
											<?php
										}
										?>
			
									</tbody>
								</table>
						</div>
						   <?php $deductible = $tot_liability_ded+$trail_liability_ded+$truc_liability_ded?>
							<div class="tdd">
							 <div class="row-fluid pull-center" style="text-align:left;"> 
								 <span class="span4">
								<h5 class="heading pull-left heading-4 span6" style="text-align:left">Total Tractor Limit</h5>&nbsp;
									 <input class="textinput span5 pull-left textinput-3 textinput-4 price_format total_tractor_limit" type="text" name="total_tractor_limit" placeholder="" value="<?php echo $tot_tractor_value; ?>">
							   
							  </span>
							   <span class="span4">  
								<h5 class="heading pull-left heading-4 span6" style="text-align:left;width:58%;">Total Tractor premium</h5>&nbsp;         
								<input class="textinput span5 pull-left textinput-3 textinput-4 price_format_decimal" id="total_tractor_premium_pd" type="text" name="total_tractor_premium" placeholder="" value="" style="margin:0px 0px 0px -4px;">
							  </span><br />
							  
							</div>
							 <div class="row-fluid pull-center" style="text-align:left; "> 
								 <span class="span4">
								<h5 class="heading pull-left heading-4 span6" style="text-align:left">Total Trailer Limit</h5>&nbsp;
									 <input class="textinput span5 pull-left textinput-3 textinput-4 price_format total_trailer_limit" type="text" name="total_trailer_limit" placeholder="" value="<?= $tot_trailer_value; ?>">
							   
							  </span>
							   <span class="span4">  
								<h5 class="heading pull-left heading-4" style="text-align:left;width:57%;">Total Trailer premium</h5>&nbsp;         
								<input class="textinput span5 pull-left textinput-3 textinput-4 price_format_decimal" type="text" name="total_trailer_premium" placeholder="" id="total_trailer_premium_pd">
							  </span><br />
							  
							</div>
                             <div class="row-fluid pull-center" style="text-align:left;"> 
								 <span class="span4">
								<h5 class="heading pull-left heading-4 span6" style="text-align:left">Total Truck Limit</h5>&nbsp;
									 <input class="textinput span5 pull-left textinput-3 textinput-4 price_format total_truck_limit" type="text" name="total_truck_limit" placeholder="" value="<?php echo $tot_truc_value; ?>">
							   
							  </span>
							   <span class="span4">  
								<h5 class="heading pull-left heading-4" style="text-align:left;width:57%;">Total Truck premium</h5>&nbsp;         
								<input class="textinput span5 pull-left textinput-3 textinput-4 price_format_decimal" id="total_truck_premium_pd" type="text" name="total_truck_premium" placeholder="" value="">
							  </span><br />
							  
							</div>
							</div>
						 
							 <div class="row-fluid">
							  <span class="span2">
								<h5 class="heading  heading-4">Total PD premium</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4 total_pd_pr price_format" type="text" id="total_pd_pr" name="total_pd_premium" placeholder="" readonly>
							   
							  </span>
							  <span class="span3">
								<h5 class="heading  heading-4"> Carrier</h5>
								<div class="pd_carrier_drop_down">
										<?php
                                            $attr1 = "id='carrier_cargo2' class='span12 ui-state-valid select-required' onchange='updatePolicyValue(&quot;pd&quot;, this.value)' ";
                                            $broker_state = '';
                                             if($carriers!=''){ $carriers = $carriers; } else {$carriers = array();}
                                        ?>
                                        <?php echo form_dropdown('carriers', $carriers,$broker_state, $attr1); ?>
                                    </div>
								
							  </span>	
							   <span class="span2">
								<h5 class="heading  heading-4">PD police fee</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4 price_format" type="text" name="pd_policy_fee" id="pd_policy_fee" placeholder="">
							   
							  </span>
							   <span class="span2">
								<h5 class="heading  heading-4">PD SLA tax</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4 price_format_decimal" type="text" name="pd_sla_tax" id="pd_sla_tax" placeholder="">
							   
							  </span>
							  
							   <span class="span3">
								<h5 class="heading  heading-4">Required for FIRM quote</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="required_for_firm_quote" placeholder="">
							  </span>
							</div>
							 <div class="row-fluid">
							  <span class="span12">
								<h5 class="heading pull-left">Comments</h5>
								<input class="textinput span9" type="text" placeholder="" name="comments">
							  </span>
							 </div>
								<div id='loader2' style="display:none"><img src="<?php echo base_url(); ?>images/spinner.gif"/></div><div id="cargo_sheet_message"></div>
								<div id="physical_damage_sheet_message" style="color:green"></div>
								<!--<input type="hidden" class="pd_id" value="" name="pd_id">-->
								 <input type="hidden" name="bundleid" class="bundleid" value="">
								<button class="btn btn-primary" id="physical_damage_submit" type="submit" onClick="form_sub()">Save Draft</button>
								<input type="hidden" name="for_submit1" value="1" />
                                  <input type="hidden" name="quote_id" value="<?php echo $quote_id; ?>" id="quote_id1"/>
                                <input type="hidden" id="base_url" name="" value="<?php echo base_url();?>" />
								<input type="hidden" name="action" value="<?php echo $action; ?>" />
							</div>
							
								
							</form>
					</div>
				
				</div>
			   <div class="sheet_container">
					<div class="row-fluid">
						<span class="span12">
						  
						  <div class="well pull-center well-3 vell-1 sheet-header">
							<h4 class="heading pull-center heading-3">Auto Liability Rate Sheet</h4>           
						  </div>
						</span>
					</div>
					<div class="well well-3 well sheet-content">
						<?php
							$attributes = array('class' => 'request_ratesheet_liability', 'id' => 'main_form4');
							
							
						?>   
                         <form class="request_ratesheet" id="main_form4" action="javascript:void(0);" method="post">
						 <input type="hidden" name="producer_email" value="<?php echo $quote->email; ?>">
						<div class="row-fluid">
						  <span class="span6">
							<h5 class="heading  heading-4">Broker</h5>
						   <input type="text" class="span9" name="broker" placeholder="Full Name/ DBA" value="<?php echo $quote->contact_name.' '.$quote->contact_middle_name.' '.$quote->contact_last_name; ?>">
						  </span>
						  <span class="span6">
							<h5 class="heading ">Insured</h5>
							<input type="text" class="span9" name="insured" placeholder="Full Name/ DBA	" value="<?php echo $quote->insured_fname.' '.$quote->insured_mname.' '.$quote->insured_lname; ?>">
						  </span>
						</div>
						<div class="row-fluid">
						  <span class="custom_span2">
							<h5 class="heading  heading-4 hed-3">Radius of Operation</h5>
							<select name="radius_of_operation" class="select-3 span12 tractor_radius_of_operation" id="tractor_radius_of_operation" onChange="rofoperation(this.value);">
								<option value="0-100 miles" <?php if($quote_vehicle[0]->radius_of_operation == '0-100 miles') echo 'selected' ?> >0-100 miles</option>
									<option value="101-500 miles" <?php if($quote_vehicle[0]->radius_of_operation == '101-500 miles') echo 'selected' ?>>101-500 miles</option>           	
									<option value="501+ miles" <?php if($quote_vehicle[0]->radius_of_operation == '501+ miles') echo 'selected' ?>>501+ miles</option>
									<option value="other" <?php if($quote_vehicle[0]->radius_of_operation == 'other') echo 'selected' ?>>Other</option>                        
							</select>
						   
						  </span>
						   <span class="span2 " <?php if($quote_vehicle[0]->radius_of_operation == 'other') echo 'style="display:block"'; else echo 'style="display:none"' ?>>
							<h5 class="heading  heading-4 hed-3">Others</h5>
						   <input class="textinput span12 pull-left show_tractor_others" type="text" name="claim_venue" value="<?php echo $quote_vehicle[0]->radius_of_operation_other	; ?>" placeholder="" >
						   
						  </span>
						   <span class="span1">
							<h5 class="heading  heading-4 hed-3">State</h5>
							<?php
							$attr = "class='span12 ui-state-valid' onchange='changeCarrier(this.value, &quot;liability&quot;)'";
							
							if($quote->insured_state != '') $broker_state = $quote->insured_state; else $broker_state = 'CA';
							
							?>
							<?php echo get_state_dropdown('state', $broker_state, $attr); ?>
						   
						  </span>
						   <span class="span1">
							<h5 class="heading  heading-4 hed-3">Claim Venue Rate</h5>
							<input class="textinput span11 pull-left textinput-3 textinput-4" type="text" name="claim_venue_rate" placeholder="">
						   
						  </span>
						  <span class="span2">
							<h5 class="heading  heading-4 hed-3">Number of Power Units</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 lno_of_power_unit" type="text" name="no_of_power_unit" placeholder="" value="<?php echo count($quote_vehicle); ?>">
						   
						  </span>
							<?php 
								$combined_limit = 0;$combined_ded = 0;
								foreach($quote_vehicle as $vehicle) { 
									$combined_limit = $combined_limit + getAmount($vehicle->liability,1);
									$combined_ded = $combined_ded + getAmount($vehicle->liability_ded);
								}
							?>
						   <span class="span2">
							<h5 class="heading  heading-4 hed-3">Combined Single Limit</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 price_format" type="text" name="combined_single_limit" placeholder="" value="<?php echo $combined_limit; ?>">
						   
						  </span>
						   <span class="span2">
							<h5 class="heading  heading-4 hed-3">Deductible</h5>
                             <!-- <select class="span12" name="tractor_liability_ded_vh" id="tractor_liability_ded_vh">
                                <option>$1,000</option>
                                <option>$1,500</option>
                                <option>$2,000</option>
                                <option>$2,500</option>
                            </select>-->
							<input class="textinput span12 pull-left textinput-3 textinput-4" value="<?= $deductible; ?>" type="text" name="deductible" placeholder="">    
                         </span>         
						</div>
						
						<div class="row-fluid">
						  <span class="span3 int_val">
							<h5 class="heading pull-left heading-4">Unit price(a) &nbsp;</h5>
							<input class="textinput span6 pull-left textinput-3 textinput-4 unit_price numr_valid price_format" type="text" name="unit_price_a" placeholder="">
						   
						  </span>
						   <span class="span3">
							<h5 class="heading pull-left heading-4">Quantity&nbsp;</h5>
							<input class="textinput span6 pull-left textinput-3 textinput-4 quantity numr_valid" type="text" name="quantity_a" placeholder="">
						   
						  </span>
						  <span class="span3 int_val">
							<h5 class="heading pull-left heading-4">Unit price(b)&nbsp;</h5>
							<input class="textinput span6 pull-left textinput-3 textinput-4 unit_price2 numr_valid price_format" type="text" name="unit_price_b" placeholder="">
						   
						  </span>
						   <span class="span3">
							<h5 class="heading pull-left heading-4">Quantity&nbsp;</h5>
							<input class="textinput span6 pull-left textinput-3 textinput-4 quantity2 numr_valid" type="text" name="quantity_b" placeholder="">
						   
						  </span>                  
						</div> 
						<div class="center">
						<div class="row-fluid pull-center row-lefl" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;width:300px;
				width: 297px;">New Venture</h5>&nbsp;
							 <select name="new_venture_surcharge" class="select-3 span4 lefl">
							  <option value="No">No</option>
							  <option value="Yes">Yes</option>              
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="new_venture_cost" class="select-3 span12 mkread lnvc">
							  <option value="0">0</option>	
							  <option value="5">5%</option>
							  <option value="10">10%</option>              
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread lnvp price_format_decimal" type="text" name="new_venture_premium" placeholder="" >
						   
						  </span>
						</div>
						<div class="row-fluid pull-center row-lefl" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">Lapsed in coverage</h5>&nbsp;
							 <select name="lapse_in_coverage_surcharge" class="select-3 span4 lefl">
							  <option value="No">No</option>
							  <option value="Yes">Yes</option>              
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="lapse_in_coverage_percentage" class="select-3 span12 mkread lnvc">
							  <option value="0">0</option>	
							  <option value="5">5%</option>
							  <option value="10">10%</option>              
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread lnvp price_format_decimal" type="text" name="lapse_in_coverage_premium" placeholder="" >
						   
						  </span>
						  
						</div>
						<div class="row-fluid pull-center row-lefl" style="text-align:right;"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">Auto hauler</h5>&nbsp;
							 <select name="auto_hauler_surcharge" class="select-3 span4 lefl">
							  <option value="No">No</option>
							  <option value="Yes">Yes</option>              
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
						   <select name="auto_hauler_cost" class="select-3 span12 mkread lnvc">
							  <option value="0">0</option>	
							  <option value="5">5%</option>             
							</select>
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread lnvp price_format_decimal" type="text" name="auto_hauler_premium" placeholder="" >
						   
						  </span>
						</div>
						  <div class="row-fluid pull-center row-efl3" style="text-align:right"> 
							 <span class="span6">
								<h5 class="heading pull-left heading-4" style="text-align:right;width: 297px;">UM coverages</h5>&nbsp;
								 <select name="um_coverage_surcharge" class="select-3 span4 efl3 ">
								  <option value="Rejected">Rejected</option>
								  <option value="15/30"> 15/30  </option>   
								   <option value="30/60"> 30/60 </option>        
								</select>
						   
							</span>
						   <span style="padding: 0px 0px 5px 14px;display: block;float: left;">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="um_coverage_cost" class="select-3 span12 mkread nvc2 umc">
							  <option value="0"> 0 </option>
							  <option value="50"> $50 </option>	
							  <option value="80"> $80 </option>                          
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right;">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread ump price_format" type="text" name="um_coverage_premium" placeholder="" >
						   
						  </span>
						</div>
						 <div class="row-fluid pull-center row-efl3" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">PIP coverages</h5>&nbsp;
							 <select name="pip_coverage_surcharge" class="select-3 span4 efl3 ">
							  <option value="Rejected">Rejected</option>
							  <option value="2500">$2,500</option>                            
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="pip_coverage_cost" class="select-3 span12 mkread  nvc2 umc">
							  <option value="0">0</option>
							  <option value="750">$750</option>                            
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread ump price_format" type="text" name="pip_coverage_premium" placeholder="" >
						   
						  </span>
						</div>
						  <div class="row-fluid pull-center row-lefl" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">Losess</h5>&nbsp;
							 <select name="losses_surcharge" class="select-3 span4 lefl">
							  <option value="None">None</option>
							  <option value="Yes">Yes</option>              
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="losses_percentage" class="select-3 span12 mkread lnvc" >
							  <option value="0">0</option>	
							  <option value="5">5%</option> 
							   <option value="10">10%</option> 
								<option value="15">15%</option> 
								 <option value="20">20%</option> 
								  <option value="25">25%</option> 
								   <option value="30">30%</option>             
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right;text-align: right;">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread lnvp price_format_decimal" type="text" name="losses_premium" placeholder="" >
						   
						  </span>
						</div>
						 <div class="row-fluid pull-center row-lefl" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">Misc.surcharges</h5>&nbsp;
							 <select name="miscellaneos_surcharge" class="select-3 span4 lefl">
							  <option value="None">None</option>
							  <option value="Yes">Yes</option>              
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="miscellaneos_surcharge_cost" class="select-3 span12 mkread lnvc">
							  <option value="0">0</option>	
							  <option value="5">5%</option> 
							   <option value="10">10%</option> 
								<option value="15">15%</option> 
								 <option value="20">20%</option> 
								  <option value="25">25%</option> 
								   <option value="30">30%</option>             
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread lnvp price_format_decimal" type="text" name="miscellaneos_surcharge_premium" placeholder="" >
						   
						  </span>
						</div>
				  <div class="row-fluid pull-center row-lefl" style="text-align:left"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;width: 297px;">Driver surcharges (unit price a) </h5>&nbsp;
							 <select name="drivers_surcharge_a" class="select-3 span4 pull-right ldsa">
							  <option value="None">None</option>
							  <option value="Yes">Yes</option>                            
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="drivers_surcharge_a_percentage" class="select-3 span12 mkread lnvc">
							  <option value="0">0</option>	
							  <option value="5">5%</option> 
							   <option value="10">10%</option> 
								<option value="15">15%</option> 
								 <option value="20">20%</option> 
								  <option value="25">25%</option> 
								   <option value="30">30%</option>   
									<option value="35">35%</option>            
							</select>
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-right textinput-3 textinput-4 mkread lnvp price_format_decimal" type="text" name="drivers_surcharge_a_premium" placeholder="" >
						   
						  </span>
						</div>
						  <div class="row-fluid pull-center row-lefl" style="text-align:left"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;width: 297px;">Driver surcharges (unit price b) </h5>&nbsp;
							 <select name="drivers_surcharge_b" class="select-3 span4 pull-right ldsa">
							  <option value="None">None</option>
							  <option value="Yes">Yes</option>                            
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-right textinput-3 textinput-4" type="text" name="" placeholder="">-->
						   <select name="drivers_surcharge_b_percentage" class="select-3 span12 mkread lnvc">
							  <option value="0">0</option>	
							  <option value="5">5%</option> 
							   <option value="10">10%</option> 
								<option value="15">15%</option> 
								 <option value="20">20%</option> 
								  <option value="25">25%</option> 
								   <option value="30">30%</option>      
									<option value="35">35%</option>       
							</select>
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-right textinput-3 textinput-4 mkread lnvp price_format_decimal" type="text" name="drivers_surcharge_b_premium" placeholder="" >
						   
						  </span>
						</div>
						 <div class="row-fluid pull-center row-lefl row-efl1" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">Extra trailer quantity</h5>&nbsp;
								<input class="textinput span4 pull-left textinput-3 textinput-4 numr_valid efl1 etq" type="text" name="extra_trailer_quantity" placeholder="" value="0">
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="extra_trailer_cost" class="select-3 span12 mkread etc">
							  <option value="420">$420 </option>  
							  <option value="456">$456 </option>
							  <option value="540">$540 </option>   
							</select>
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-right textinput-3 textinput-4 mkread nvp2 etp price_format" type="text" name="extra_trailer_premium" placeholder="" >
						   
						  </span>
						</div>
						<div class="row-fluid pull-center row-lefl row-efl1" style="text-align:left"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;width:297px;">15 year tractor/tralier quantity</h5>&nbsp;
								<input class="textinput span4 pull-right textinput-3 textinput-4 numr_valid efl1 etq" type="text" name="tractor/trailer_quantity" placeholder="" value="0">
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<input class="textinput span12 pull-left textinput-3 textinput-4 mkread lnvc etc price_format" type="text" name="tractor/trailer_surcharge_cost" placeholder="" value="200">
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-right textinput-3 textinput-4 nvp2 etp price_format mkread" type="text" name="tractor/trailer_surcharge_premium" placeholder="" >
						   
						  </span>
						</div>
						 <div class="row-fluid pull-center row-lefl" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;width:297px;">Discount</h5>&nbsp;
							 <!--<select name="" class="select-3 span4">
							  <option value="">None</option>
											
							</select>-->
							<span class="span2 pull-right">           
							 <select class=" span12 pull-left numr_valid disc1" name="discount">
                            	<option <?php echo isset($liability['discount']) ? ($liability['discount']=='No') ? 'selected' : '' : ''; ?>>No</option>
                                <option <?php echo isset($liability['discount']) ? ($liability['discount']=='Yes') ? 'selected' : '' : ''; ?>>Yes</option>
                            </select>
						  </span>
						   
						  </span>
							<span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">                        
							<select name="drivers_surcharge_b_percentage" class="select-3 span12 mkread disc2">
							  <option value="0">0</option>	
							  <option value="-5">-5%</option> 
							   <option value="-10">-10%</option> 
								<option value="-15">-15%</option> 
								 <option value="-20">-20%</option> 
								  <option value="-25">-25%</option> 
								   <option value="-30">-30%</option>     
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 price_format_decimal disc3 mkread" type="text" name="discounted_premium" placeholder="" >
						   
						  </span>
						</div>
						</div>
						  <div class="row-fluid">
						  <span class="span4">
							<h5 class="heading  heading-4">Total Liability Premium</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 total_liability_premium price_format " type="text" name="total_liability_premium" placeholder="" readonly>
						   
						  </span>
						   <span class="custom_span3">
							<h5 class="heading  heading-4">Unit Price breakdown</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 unit_price_breakdown" type="text" name="unit_price_breakdown" placeholder="" readonly>
						   
						  </span>
						   <span class="span2">
							<h5 class="heading  heading-4">Liability police fee</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 numr_valid price_format text-required" type="text" name="liability_policy_fee" id="liability_policy_fee" placeholder="" value="200">
						   
						  </span>
						  <span class="custom_span1">
							<h5 class="heading  heading-4">Filling fee</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 numr_valid price_format text-required" type="text" name="liablity_filing_fee" placeholder="" value="50">
						   
						  </span>
						   <span class="span2">
							<h5 class="heading  heading-4">Carrier</h5>
							<!--<input class="textinput span9 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<div class="liability_carrier_drop_down">
										<?php
                                            $attr1 = "id='carrier_cargo3' class='span12 ui-state-valid select-required' onchange='updatePolicyValue(&quot;liability&quot;, this.value)' ";
                                            $broker_state = '';
                                             if($carriers!=''){ $carriers = $carriers; } else {$carriers = array();}
                                        ?>
                                        <?php echo form_dropdown('carriers', $carriers,$broker_state, $attr1); ?>
                                    </div>
						  </span>
						</div>
						<div class="row-fluid">
						  <span class="span12">
							<h5 class="heading pull-left">Comments &nbsp;</h5>
							<input class="textinput span9" type="text" placeholder="" name="comments">
						  </span>
						 
				 
						</div>
							<div id='loader1' style="display:none"><img src="<?php echo base_url(); ?>images/spinner.gif"/></div><div id="cargo_sheet_message"></div>
							<div id="liability_sheet_message"></div>
							<!--<input type="hidden" class="liability_id" value="" name="liability_id">-->
							 <input type="hidden" name="bundleid" class="bundleid" value="">
							<button class="btn btn-primary" id="liability_submit" type="submit" onClick="form_sub()">Save Draft</button>                            
                              <input type="hidden" name="quote_id" value="<?php echo $quote_id; ?>" id="quote_id2"/>
							<input type="hidden" name="for_submit2" value="1" />
								<input type="hidden" name="action" value="<?php echo $action; ?>" />
							</form>
					</div>
				</div>
				<div id="bundle_message"></div>
				
				<div id='loader3' style="display:none">
				<img src="<?php echo base_url(); ?>images/spinner.gif"/></div><div id="cargo_sheet_message"></div>
				<button class="btn btn-primary" id="ratesheet_bundle_submit" type="submit" style="display:none">Submit Bundle</button>
				
				</div>
		</div>
	
				<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script>
updatePolicyValue('cargo', '');
updatePolicyValue('pd', '');
updatePolicyValue('liability', '');
function changeCarrier(val, type){
	var url = '<?php echo base_url('ratesheet'); ?>/get_carrier_by_state';
	changeCarrier1(val, type, url)
}
function updatePolicyValue(type, val){
	var url = '<?php echo base_url('ratesheet'); ?>/get_cargo_detail';
	updatePolicyValue1(type, val, url);	
}
	$(document).ready(function(){
	
	
		$("#assign_producer").change(function(){
			if($(this).val() == 100)
			{
				$("#new_producer").show();
			}
			else
			{
				$("#new_producer").hide();
			}
		});
		$(".sheet-content").hide();
		$(".sheet-header").click(function(){	
			$(this).parent().parent().parent().find(".sheet-content").slideToggle();
		});
		$("#carrier_cargo").change(function(){
			var val = $( "#carrier_cargo option:selected" ).val();
			var val1 = $("#quote_id").val();
			//alert(val1);
			var baseurl = "<?php echo base_url('ratesheet'); ?>/get_cargo_detail";
				$.ajax({
				  url: baseurl,
				  type: "post",
				  data: {'name' : val,'id':val1},
				 datatype: 'json',
				  success: function(data){
					 var obj = $.parseJSON(data);  
					// alert(data);
					 $("#cargo_policy_fee").val(obj['cargo_policy_fee']);
					 
					 $("#cargo_sla_tax").val(return_numeric($("#total_cargo_premium1").val()) * return_numeric(obj['slatax']));
					 $('.price_format').priceFormat({
						prefix: '$ ',
						centsLimit: 0,
						thousandsSeparator: ','
					});
				  },
				  error:function(){
					
				  }   
				});
		});
		$("#carrier_cargo1").change(function(){
			var val = $( "#carrier_cargo1 option:selected" ).val();
			var val1 = $("#quote_id1").val();
			
			var baseurl = "<?php echo base_url('ratesheet'); ?>/get_cargo_detail";
				$.ajax({
				  url: baseurl,
				  type: "post",
				  data: {'name' : val,'id':val1},
				 datatype: 'json',
				  success: function(data){
					 var obj = $.parseJSON(data);  
					 $("#pd_policy_fee").val(obj['cargo_policy_fee']);
					 $("#pd_sla_tax").val(return_numeric($("#total_pd_pr").val()) * return_numeric(obj['slatax']));
					  $('.price_format').priceFormat({
						prefix: '$ ',
						centsLimit: 0,
						thousandsSeparator: ','
					});
				  },
				  error:function(){
					
				  }   
				});
		});
	});

	function rofoperation(val , id){
				
			
			if(val == 'other'){
				$('.show_tractor_others').show();
			} else {
				$('.show_tractor_others').hide();
			}	
			
		}
		
		
		
	/*  alert(id);
	alert(val);  */

<!--ajax form submit-->

$("#cargo_submit").click(function(){
	valchk = true;
	$("#main_form2").find(".text-required").each(function(){
		if($(this).val() == '' || $(this).val() == '$ 0')
		{
			$(this).css("border","1px solid red");
			valchk = false;
		}
	});
	$("#main_form2").find(".select-required").each(function(){
		
		if($(this).val() == null || $(this).val() == undefined || $(this).val() == '')
		{
			$(this).css("border","1px solid red");
			valchk = false;
		}
	});
	if(valchk == false)
	return false; 
	var baseurl = "<?php echo base_url('ratesheet'); ?>/save_cargo";

	$.ajax({
		  url: baseurl,
		  type: "post",
		  data: $("#main_form2").serialize(),
	 	 datatype: 'json',
		 beforeSend: function() {
			 $('#loader').show();
		  },
		  complete: function(){
			 $('#loader').hide();
		  },
		  success: function(data){
		 // alert(data);
				// $("#main_form2 input,#main_form2 select").attr("disabled",true);
				 $("#cargo_sheet_message").html("Cargo Rate Sheet Draft Saved");
				 if(($(".bundleid").first().val() == ''))
				 {
					$("#ratesheet_bundle_submit").show();
					$(".bundle_mail").show();
				 }
				 $(".bundleid").val(data);
				 
		  },
		  error:function(){
			
		  }   
		}); 
});
$("#liability_submit").click(function(){
	valchk = true;
	$("#main_form4").find(".text-required").each(function(){
		if($(this).val() == '' || $(this).val() == '$ 0')
		{
			$(this).css("border","1px solid red");
			valchk = false;
		}
	});
	$("#main_form4").find(".select-required").each(function(){
		
		if($(this).val() == null || $(this).val() == undefined || $(this).val() == '')
		{
			$(this).css("border","1px solid red");
			valchk = false;
		}
	});
	if(valchk == false)
	return false; 
	var baseurl = "<?php echo base_url('ratesheet'); ?>/save_liability";

	$.ajax({
		  url: baseurl,
		  type: "post",
		  data: $("#main_form4").serialize(),
	 	 datatype: 'json',
		  beforeSend: function() {
			 $('#loader1').show();
		  },
		  complete: function(){
			 $('#loader1').hide();
		  },
		  success: function(data){
			//	 $("#main_form4 input,#main_form4 select").attr("disabled",true);
				 $("#liability_sheet_message").html("Auto Liability Rate Sheet Draft Saved"); 
				if(($(".bundleid").first().val() == ''))
				 {
					$("#ratesheet_bundle_submit").show();
					$(".bundle_mail").show();
				 }			 
				 $(".bundleid").val(data);
		  },
		  error:function(){
			
		  }   
		}); 
});

$("#ratesheet_bundle_submit").click(function(){
	var baseurl2 = "<?php echo base_url('ratesheet'); ?>/submit_bundle";
	$.ajax({
		  url: baseurl2,
		  type: "post",
		  data: {'quote_id':$("#quote_id1").val(),'bundle_id':$(".bundleid").first().val()},
	 	  datatype: 'json',
		  beforeSend: function() {
			 $('#loader3').show();
		  },
		  complete: function(){
			 $('#loader3').hide();
		  },
		  success: function(data){
		  //alert(data);
			$("#bundle_message").html("Bundle Submitted");
			
		  },
		  error:function(){
			
		  }   
		}); 
});

$("#physical_damage_submit").click(function(){
	
	var baseurl2 = "<?php echo base_url('ratesheet'); ?>/save_physical_damage";
	
	$.ajax({
		  url: baseurl2,
		  type: "post",
		  data: $("#main_form3").serialize(),
	 	  datatype: 'json',
		  beforeSend: function() {
			 $('#loader2').show();
		  },
		  complete: function(){
			 $('#loader2').hide();
		  },
		  success: function(data){
		  //alert(data);
		//	$("#main_form3 input,#main_form3 select").attr("disabled",true);
			$("#physical_damage_sheet_message").html("Physical Damage Rate Sheet Draft Saved");
			if(($(".bundleid").first().val() == ''))
			 {
			 
				$("#ratesheet_bundle_submit").show();
				$(".bundle_mail").show();
			 }
			$(".bundleid").val(data);
		  },
		  error:function(){
			alert('e');
		  }   
		}); 
});




	</script>
    
    
    
   
    
    
    
	</body>
	</html>
	<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{
		$this->load->view('includes/footer');
	}
	?>