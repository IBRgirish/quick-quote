<div class="sheet_container">
  <div class="row-fluid">
      
      
  </div>
  <?php if($segement=='manualrate'){?>
  <table>
    <tr>
        <td style="width: 103px;">Producer : </td>
        <td>
            <select id="assign_producer" name="new_publisher">
                <?php foreach($publishers as $publisher) { ?>
                    <option value="<?php echo $publisher->email; ?>"><?php echo $publisher->email; ?></option>
                <?php }?>
                <option value="-1">
                 Add New
                </option>
            </select>
        </td>
    </tr>
    
<!--<div id="new_producer" style="display:none">-->
    <tr id="new_producer" style="display:none" >
        <td colspan="2">
        <table>
        <tr>                               
            <td>Email : </td>
            <td>
                <input type="email" name="email" id="prod_email"> <span id="email_message"></span><span id='emailloader3' style="display:none">
<img src="<?php echo base_url(); ?>images/spinner.gif"/></span>
            </td>
        </tr>
         <tr>
            <td>Password : </td> 
            <td>
                <input type="password" name="password" id="prod_passwd"><br>
            </td>
         </tr>
         <tr>
         
        <?php /*if($this->session->userdata('underwriter_id')){	
        } else*/ if($this->session->userdata('member_id')){	
        } else { ?>
        <td> Underwriters :</td>
            <td>
            <?php foreach($underwriters as $underwriter) { 
            
                $check = 'checked="checked"';
            ?>
                 &nbsp;<input type="checkbox" name="underwriter[]"  class="underwrit" <?php echo $check; ?>  value="<?php echo $underwriter->id; ?>"> <?php echo ' '.$underwriter->name; ?>
            <?php }?>
            </td>
        <?php } ?> 
        </tr>
        <tr>
            <td>
                <input type="button" class="btn btn-primary" value="Add Agency" id="add_producer" disabled="disabled">
                <div id='prod_userloader3' style="display:none">
                    <img src="<?php echo base_url(); ?>images/spinner.gif"/>
                </div>
                <div id="prod_user_msg"></div>
            </td>
        </tr>
        </table>
        </td>
    <br>
   
    <div style="clear:both"></div>
<!--</div>-->
</table>
<?php } ?>
  <br />
  <div class="row-fluid">
      <span class="span12">
        
        <div class="well pull-center well-3 pga sheet-header">
          <h4 class="heading pull-center heading-3" style="">Cargo Rate sheet</h4>           
        </div>
      </span>
  </div>
  <div class="well well-3 well sheet-content">
      
          <?php
              $attributes = array('class' => 'request_ratesheet', 'id' => 'main_form2');
			  $cargo = isset($cargo_rate['0']) ? (array)$cargo_rate['0'] : '';
			 // 
			
			 $bundleid= isset($bundleid) ? $bundleid : ''; 			
			 if($segement == 'edit_ratesheet'){
				  $broker = isset($cargo['broker']) ? $cargo['broker'] :$quote->contact_name.' '.$quote->contact_middle_name.' '.$quote->contact_last_name;	
			 	  $insured = isset($cargo['insured']) ? $cargo['insured'] : $quote->insured_fname.' '.$quote->insured_mname.' '.$quote->insured_lname;
				  if(str_replace(' ', '', $insured)==''){
						 $insured = $quote->insured_dba;
					}
			  }else{
				  if($segement != 'manualrate'){
				  	$broker = $quote->contact_name.' '.$quote->contact_middle_name.' '.$quote->contact_last_name; 
					$insured = $quote->insured_fname.' '.$quote->insured_mname.' '.$quote->insured_lname;
					if(str_replace(' ', '', $insured)==''){
						$insured = $quote->insured_dba;
					}
				  }else{
					  $broker = '';
					  $insured = '';
				  }
			  }
			 
			 $radius_of_operation = isset($cargo['radius_of_operation']) ? $cargo['radius_of_operation'] : '0-100 miles';	
			 $state = isset($cargo['state']) ? $cargo['state'] : 'CA';	
		 	 $claim_venue = isset($cargo['claim_venue']) ? $cargo['claim_venue'] : '';	
			 $no_of_power_unit = isset($cargo['no_of_power_unit']) ? $cargo['no_of_power_unit'] : '';	
			 $limit = isset($cargo['limit']) ? $cargo['limit'] : '';	
			 $deductible = isset($cargo['deductible']) ? $cargo['deductible'] : '';				 
			 $reffer_deductible = isset($cargo['reffer_deductible']) ? $cargo['reffer_deductible'] : '';			
			 $base_rate = isset($cargo['base_rate']) ? $cargo['base_rate'] : '';	
			 $earned_freight_premium = isset($cargo['earned_freight_premium']) ? $cargo['earned_freight_premium'] : '';
			 $debris_removal_premium = isset($cargo['debris_removal_premium']) ? $cargo['debris_removal_premium'] : '';	
			 $tarpauling_coverage_premium = isset($cargo['tarpauling_coverage_premium']) ? $cargo['tarpauling_coverage_premium'] : '';	
			 $lapse_in_coverage_premium = isset($cargo['lapse_in_coverage_premium']) ? $cargo['lapse_in_coverage_premium']: '';
			 $new_venture_premium = isset($cargo['new_venture_premium']) ? $cargo['new_venture_premium'] : '';				
			 $losses_premium = 	isset($cargo['losses_premium']) ? $cargo['losses_premium'] : '';
			 $driver_surcharge_premium = isset($cargo['driver_surcharge_premium']) ? $cargo['driver_surcharge_premium'] : '';
			 $old_units = isset($cargo['old_units']) ? $cargo['old_units'] : '';
			 $old_units_premium = isset($cargo['old_units_premium']) ? $cargo['old_units_premium'] : '';
			 $total_cargo_premium = isset($cargo['total_cargo_premium']) ? $cargo['total_cargo_premium'] : '';
			 $cargo_policy_fee = isset($cargo['cargo_policy_fee']) ? $cargo['cargo_policy_fee'] : '';
			 $cargo_filing_fee = isset($cargo['cargo_filing_fee']) ? $cargo['cargo_filing_fee'] : '$ 0';
			 $cargo_sla_tax = isset($cargo['cargo_sla_tax']) ? $cargo['cargo_sla_tax'] : '';			 
			 $required_for_firm_quote = isset($cargo['required_for_firm_quote']) ? $cargo['required_for_firm_quote'] : '';
			 $comments = isset($cargo['comments']) ? $cargo['comments'] : '';
			 $cargo_policy_fee = isset($cargo['cargo_policy_fee']) ? $cargo['cargo_policy_fee'] : '';
        	 $cargo_carrier = isset($cargo['carrier']) ? $cargo['carrier'] : '';
			 if(isset($quote->commodities_haulted)){
				$commodities = $quote->commodities_haulted;
			 }else{
				$commodities = isset($cargo['commodities']) ? $cargo['commodities'] : '';
			 }
			 $cargo_combined_limit = 0;$cargo_combined_ded = 0;
			 if(isset($quote_vehicle)){
				foreach($quote_vehicle as $vehicle) { 
					$cargo_combined_limit = $cargo_combined_limit + getAmount($vehicle->cargo,1);
					$cargo_combined_ded = $cargo_combined_ded + getAmount($vehicle->cargo_ded);
				}
			}else{
				$cargo_combined_limit = isset($cargo['limit']) ? $cargo['limit'] :'';
				$cargo_combined_ded = isset($cargo['deductible']) ? $cargo['deductible'] : '';
			}
          
          ?>   
       
   		<form class="request_ratesheet" id="main_form2" action="javascript:void(0);" method="post"> 	
          <input type="hidden" name="producer_email" class="producer_email" value="<?php echo isset($quote->email) ? $quote->email : ''; ?>">
          <input type="hidden" name="underwriter_ids" class="underwriter_ids" value="<?php echo isset($quote->assigned_underwriter) ? $quote->assigned_underwriter : '';?>">
           <?php if($segement == 'manualrate'){ ?>
          <input type="hidden" name="is_manual" class="is_manual" value="1">
          <?php }?>
          <div class="row-fluid">
              <?php
                  $ratesheet_broker_name = '';
                  
              ?>
           
            <span class="span6">
              <h5 class="heading  heading-4">Broker</h5>
             <input type="text" class="span9 brokername broker" name="broker" placeholder="Full Name/ DBA" value="<?php echo $broker ?>" >
            </span>
            <span class="span6">
              <h5 class="heading ">Insured</h5>
              <input type="text" class="span9 insuredname insured" name="insured" placeholder="Full Name/ DBA" value="<?php echo $insured; ?>">
            </span>
          </div>
          <div class="row-fluid">
            <span class="span2">
              <h5 class="heading  heading-3 hed-3">Radius of Operation</h5>
              <!--<input class="textinput span10 pull-left textinput-3 textinput-4 radiusofoperation" type="text" name="radius_of_operation" placeholder="">-->      
              <select name="radius_of_operation" class="select-3 span12 tractor_radius_of_operation radius_of_operation rocargo" id="tractor_radius_of_operation" >
                  <option value="0-100 miles" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == '0-100 miles') ? 'selected'  : '' :'';?> <?php echo isset($cargo['radius_of_operation']) ? ($cargo['radius_of_operation']=='0-100 miles') ? 'selected' : '' : '';?> >0-100 miles</option>
                  <option value="101-500 miles" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == '101-500 miles') ? 'selected'  : '' :''; ?> <?php echo isset($cargo['radius_of_operation']) ? ($cargo['radius_of_operation']=='101-500 miles') ? 'selected' : '' : '';?>>101-500 miles</option>           	
                  <option value="501+ miles" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == '501+ miles') ? 'selected'  : '' :''; ?> <?php echo isset($cargo['radius_of_operation']) ? ($cargo['radius_of_operation']=='501+ miles') ? 'selected' : '' : '';?>>501+ miles</option>
                  <option value="other" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == 'other') ? 'selected'  : '' :'';?> <?php echo isset($cargo['radius_of_operation']) ? ($cargo['radius_of_operation']=='other') ? 'selected' : '' : '';?>>Other</option>            
              </select><br>
              <?php  $cargoRoOther = isset($cargo['radius_of_operation']) ? ($cargo['radius_of_operation']=='other') ? 'style="display:block"' : 'style="display:none"' : 'style="display:none"'; ?> 
              <?php $cargoRoOtherValue = isset($cargo['other_rop']) ? $cargo['other_rop'] : '';	; ?>
			  <input class="textinput span12 pull-left cargo_show_tractor_others" id="cargo_show_tractor_others" type="text" name="others" value="<?php echo isset($quote_vehicle[0]->radius_of_operation_other) ? $quote_vehicle[0]->radius_of_operation_other  :  $cargoRoOtherValue;?>" placeholder="" 
		<?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == 'other') ? 'style="display:block"' : 'style="display:none"' : $cargoRoOther; ?>>
            </span>
            <span class="span1">		
              <h5 class="heading  heading-4 hed-3">State</h5>
              <div class="textinput span12 ">
                  <?php
                      $attr = "class='span12 ui-state-valid state' onChange='changeCarrier(this.value, &quot;cargo&quot;)'";
                      //echo $quote->insured_state;
                      $broker_state = isset($quote->insured_state) ? ($quote->insured_state!='') ? $quote->insured_state : 'CA' : '';
					  
					  if($segement=='edit_ratesheet'){
						$broker_state = $state ;
					  }					 
                  ?>
                  <?php echo get_state_dropdown('state', $broker_state, $attr); ?>
              </div>
            </span>
             <span class="span1">
              <h5 class="heading  heading-4 hed-3">Claim Venue</h5>
              <!--<input class="textinput span12 pull-left textinput-3 textinput-4 claim_venue" type="text" name="claim_venue" placeholder="">-->
              <select name="claim_venue" class="span12 pull-left textinput-3 textinput-4 claim_venue">
                  <option value="No" <?php echo isset($cargo['claim_venue']) ? ($cargo['claim_venue']=='No') ? 'selected' : '' : '';?>>No</option>
                  <option value="Yes" <?php echo isset($cargo['claim_venue']) ? ($cargo['claim_venue']=='Yes') ? 'selected' : '' : '';?>>Yes</option>
              </select>
             
            </span>
            <span class="span2">
              <h5 class="heading  heading-4 hed-3">Number of Power Units</h5>
              <input class="textinput span10 pull-left textinput-3 textinput-4 npu no_of_power_unit" type="text" name="no_of_power_unit" placeholder="" value="<?php echo isset($powerUnitGenerate) ? $powerUnitGenerate : $no_of_power_unit ; ?>">
             
            </span>
             <span class="span1">
              <h5 class="heading  heading-4 hed-3"> Limit</h5>
              <input class="textinput span12 pull-left textinput-3 textinput-4 limit" type="text" name="limit" placeholder="" value="<?php echo  $cargo_combined_limit; ?>">
             
            </span>
             <span class="span1">
              <h5 class="heading  heading-4 hed-3">Deductible</h5>
              <input class="textinput span12 pull-left textinput-3 textinput-4 deductible" type="text" name="deductible" placeholder="" value="<?php echo $cargo_combined_ded; ?>">
             
            </span>  
             <span class="span2">
              <h5 class="heading  heading-4 hed-3"> Reffer Deductible</h5>
              <input class="textinput span12 pull-left textinput-3 textinput-4 reffer_deductible" type="text" name="reffer_deductible" placeholder="" value="<?php echo $reffer_deductible; ?>">
             
            </span>      
             <span class="span2" >
              <h5 class="heading  heading-4 hed-3">Base rate</h5>
              <input class="textinput span10 pull-left textinput-3 textinput-4 price_format_decimal base_rate" type="text" name="base_rate" placeholder="" id="bsr" value="<?php echo $base_rate; ?>">
             
            </span>              
          </div>
          <div class="mid-wrapper">	
               <strong>Optional endorsement   &nbsp; &nbsp; &nbsp; *** Not affered to all clients***</strong><br /><br />
                <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                   <span class="span4">
                   <h5 class="heading pull-left heading-4" style="text-align:right">Earned Freight</h5>&nbsp;
                   <select name="earned_freight_limit" class="select-3 span6 efl" id="" >
                    <option value="None" <?php echo isset($cargo['earned_freight_limit']) ? ($cargo['earned_freight_limit']=='None') ? 'selected' : '' : '';?>>None</option>
                     <option value="2500" <?php echo isset($cargo['earned_freight_limit']) ? ($cargo['earned_freight_limit']=='2500') ? 'selected' : '' : '';?>>$ 2,500</option>
                      <option value="3000" <?php echo isset($cargo['earned_freight_limit']) ? ($cargo['earned_freight_limit']=='3000') ? 'selected' : '' : '';?>>$ 3,000</option>
                                  
                  </select>
                 
                    </span>
                      <span style=" padding: 0px 0px 5px 14px;display: block;        float: left;    ">@</span> 
                     <span class="span2">           
                     <!-- <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                      <select name="earned_freight_cost" class="select-3 span12 mkread1 efc" <?php echo isset($cargo['earned_freight_limit']) ? ($cargo['earned_freight_limit']=='Yes') ? '' : 'disabled' : 'disabled';?>>
                        <option value="0" <?php echo isset($cargo['earned_freight_cost']) ? ($cargo['earned_freight_cost']=='0') ? 'selected' : '' : '';?>>0</option>
                         <option value="100" <?php echo isset($cargo['earned_freight_cost']) ? ($cargo['earned_freight_cost']=='100') ? 'selected' : '' : '';?>>$ 100 </option>
                          <option value="200"  <?php echo isset($cargo['earned_freight_cost']) ? ($cargo['earned_freight_cost']=='200') ? 'selected' : '' : '';?>>$ 200</option>
                                      
                      </select>
                     
                    </span>
                    <span class="span3">
                      <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
                     &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  efp price_format" type="text" name="earned_freight_premium" placeholder="" value="<?php echo $earned_freight_premium; ?>" readonly>
                     
                    </span>
                </div>
               <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                   <span class="span4">
                  <h5 class="heading pull-left heading-4" style="text-align:right">Debris removal</h5>&nbsp;
                   <select name="debris_removal_limit " class="select-3 span6 efl">
                    <option value="None" <?php echo isset($cargo['debris_removal_limit']) ? ($cargo['debris_removal_limit']=='None') ? 'selected' : '' : '';?>>None</option>
                    <option value="2500"<?php echo isset($cargo['debris_removal_limit']) ? ($cargo['debris_removal_limit']=='2500') ? 'selected' : '' : '';?>>$ 2,500</option>              
                  </select>
                 
                </span>
                   <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
                 <span class="span2">           
                  <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                 <select name="debris_removal_cost" class="select-3 span12 mkread1 efc" id="drc" <?php echo isset($cargo['debris_removal_limit']) ? ($cargo['debris_removal_limit']=='Yes') ? '' : 'disabled' : 'disabled';?>>
                    <option value="0" <?php echo isset($cargo['debris_removal_cost']) ? ($cargo['debris_removal_cost']=='0') ? 'selected' : '' : '';?>>0</option>
                     <option value="150" <?php echo isset($cargo['debris_removal_cost']) ? ($cargo['debris_removal_cost']=='150') ? 'selected' : '' : '';?>>$ 150 </option>                                
                  </select>
                </span>
                <span class="span3">
                  <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
                 &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  efp price_format" type="text" name="debris_removal_premium" placeholder="" value="<?php echo $debris_removal_premium;?>" readonly>
                 
                </span>
               </div>
                <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                   <span class="span4">
                  <h5 class="heading pull-left heading-4" style="text-align:right">Tourpolin coverage</h5>&nbsp;
                   <select name="tarpauling_coverage_limit" class="select-3 span6 efl" >
                    <option value="None" <?php echo isset($cargo['tarpauling_coverage_limit']) ? ($cargo['tarpauling_coverage_limit']=='None') ? 'selected' : '' : '';?>>None</option>
                     <option value="5000" <?php echo isset($cargo['tarpauling_coverage_limit']) ? ($cargo['tarpauling_coverage_limit']=='5000') ? 'selected' : '' : '';?>>$ 5,000</option>              
                  </select>
                 
                    </span>
                       <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
                     <span class="span2">           
                      <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                      <select name="tarpauling_coverage_cost" class="select-3 span12 mkread1 efc" id="tcc" <?php echo isset($cargo['tarpauling_coverage_limit']) ? ($cargo['tarpauling_coverage_limit']=='Yes') ? '' : 'disabled' : 'disabled';?>>
                        <option value="0" <?php echo isset($cargo['tarpauling_coverage_cost']) ? ($cargo['tarpauling_coverage_cost']=='0') ? 'selected' : '' : '';?>>0</option>
                         <option value="1000" <?php echo isset($cargo['tarpauling_coverage_cost']) ? ($cargo['tarpauling_coverage_cost']=='1000') ? 'selected' : '' : '';?>>$ 1,000 </option>                                
                      </select>
                     
                    </span>
                    <span class="span3">
                      <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
                     &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  efp price_format" type="text" name="tarpauling_coverage_premium" placeholder="" value="<?php echo $tarpauling_coverage_premium; ?>" readonly>
                    </span>
                </div>
               
               
                <div class="row-fluid pull-center" style="text-align:right"> 
                       <span class="span6">
                      <h5 class="heading pull-left heading-4" style="text-align:right">Reefer breakdown</h5>&nbsp;
                          <!-- <input class="textinput span8 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                           <select name="reefer_breakdown_limit" class="select-3 span6 ">
                        <option value="None" <?php echo isset($cargo['reefer_breakdown_limit']) ? ($cargo['reefer_breakdown_limit']=='None') ? 'selected' : '' : '';?>>None</option>
                        <option value="Included in Base Rate" <?php echo isset($cargo['reefer_breakdown_limit']) ? ($cargo['reefer_breakdown_limit']=='Included in Base Rate') ? 'selected' : '' : '';?>>Included in Base Rate</option>
                      </select>
                          
                    </span>
                    
                </div>
                  <br />    
                  <strong > Optional endoresment</strong>
                   <div class="ho">
                       <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                           <span class="span4">
                          <h5 class="heading pull-left heading-4" style="text-align:right">Lapse in coverage</h5>&nbsp;
                           <select name="lapse_in_coverage_surcharge" class="select-3 span6 efl">
                            <option value="No" <?php echo isset($cargo['lapse_in_coverage_surcharge']) ? ($cargo['lapse_in_coverage_surcharge']=='No') ? 'selected' : '' : '';?>>No</option>
                              <option value="Yes" <?php echo isset($cargo['lapse_in_coverage_surcharge']) ? ($cargo['lapse_in_coverage_surcharge']=='Yes') ? 'selected' : '' : '';?>>Yes</option>           
                          </select>
                         
                        </span>
                           <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
                         <span class="span2">           
                          <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                         <select name="lapse_in_coverage_percentage" class="select-3 span12 mkread1 efc" id="lcc" <?php echo isset($cargo['lapse_in_coverage_surcharge']) ? ($cargo['lapse_in_coverage_surcharge']=='Yes') ? '' : 'disabled' : 'disabled';?>>
                            <option value="0" <?php echo isset($cargo['lapse_in_coverage_percentage']) ? ($cargo['lapse_in_coverage_percentage']=='0') ? 'selected' : '' : '';?>>0</option>
                              <option value="5" <?php echo isset($cargo['lapse_in_coverage_percentage']) ? ($cargo['lapse_in_coverage_percentage']=='5') ? 'selected' : '' : '';?>>5%</option>    
                               <option value="10" <?php echo isset($cargo['lapse_in_coverage_percentage']) ? ($cargo['lapse_in_coverage_percentage']=='10') ? 'selected' : '' : '';?>>10%</option>         
                          </select>
                        </span>
                        <span class="span3">
                          <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
                         &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  efp price_format_decimal" type="text" name="lapse_in_coverage_premium" placeholder="" value="<?php echo $lapse_in_coverage_premium; ?>" readonly>
                         
                        </span>
                       </div>
                       <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                           <span class="span4">
                          <h5 class="heading pull-left heading-4" style="text-align:right">New venture</h5>&nbsp;
                           <select name="new_venture_surcharge" class="select-3 span6 efl">
                            <option value="No" <?php echo isset($cargo['new_venture_surcharge']) ? ($cargo['new_venture_surcharge']=='No') ? 'selected' : '' : '';?>>No</option>
                             <option value="Yes" <?php echo isset($cargo['new_venture_surcharge']) ? ($cargo['new_venture_surcharge']=='Yes') ? 'selected' : '' : '';?>>Yes</option>             
                          </select>
                         
                        </span>
                           <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
                         <span class="span2">           
                          <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                          <select name="new_venture_percentage" class="select-3 span12 mkread1 efc" id="nvc" <?php echo isset($cargo['new_venture_surcharge']) ? ($cargo['new_venture_surcharge']=='Yes') ? '' : 'disabled' : 'disabled';?>>
                            <option value="0" <?php echo isset($cargo['new_venture_percentage']) ? ($cargo['new_venture_percentage']=='0') ? 'selected' : '' : '';?>>0</option>
                              <option value="5" <?php echo isset($cargo['new_venture_percentage']) ? ($cargo['new_venture_percentage']=='5') ? 'selected' : '' : '';?>>5%</option>    
                               <option value="10" <?php echo isset($cargo['new_venture_percentage']) ? ($cargo['new_venture_percentage']=='10') ? 'selected' : '' : '';?>>10%</option>         
                          </select>
                         
                        </span>
                        <span class="span3">
                          <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
                         &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  efp price_format_decimal" type="text" name="new_venture_premium" placeholder="" value="<?php echo $new_venture_premium; ?>" readonly>
                         
                        </span>
                       </div>
                        <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                           <span class="span4">
                          <h5 class="heading pull-left heading-4" style="text-align:right">Losess</h5>&nbsp;
                           <select name="losses_surcharge" class="select-3 span6 efl">
                            <option value="None" <?php echo isset($cargo['losses_surcharge']) ? ($cargo['losses_surcharge']=='None') ? 'selected' : '' : '';?>>None</option>
                             <option value="Yes" <?php echo isset($cargo['losses_surcharge']) ? ($cargo['losses_surcharge']=='Yes') ? 'selected' : '' : '';?>>Yes</option>             
                          </select>
                         
                            </span>
                               <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
                             <span class="span2">           
                              <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="losses_percentage" placeholder="">    -->           	
                              <select name="losses_percentage" class="select-3 span12 mkread1 efc" id="lc" <?php echo isset($cargo['losses_surcharge']) ? ($cargo['losses_surcharge']=='Yes') ? '' : 'disabled' : 'disabled';?>>
                                 <option value="0" <?php echo isset($cargo['losses_percentage']) ? ($cargo['losses_percentage']=='0') ? 'selected' : '' : '';?>>0</option>
                                 <option value="5"<?php echo isset($cargo['losses_percentage']) ? ($cargo['losses_percentage']=='5') ? 'selected' : '' : '';?>>5%</option>
                                 <option value="10" <?php echo isset($cargo['losses_percentage']) ? ($cargo['losses_percentage']=='10') ? 'selected' : '' : '';?>>10%</option>
                                 <option value="15" <?php echo isset($cargo['losses_percentage']) ? ($cargo['losses_percentage']=='15') ? 'selected' : '' : '';?>>15%</option>
                                 <option value="20" <?php echo isset($cargo['losses_percentage']) ? ($cargo['losses_percentage']=='20') ? 'selected' : '' : '';?>>20%</option>
                                 <option value="25" <?php echo isset($cargo['losses_percentage']) ? ($cargo['losses_percentage']=='25') ? 'selected' : '' : '';?>>25%</option>
                                 <option value="30" <?php echo isset($cargo['losses_percentage']) ? ($cargo['losses_percentage']=='30') ? 'selected' : '' : '';?>>30%</option>
                                 <option value="35" <?php echo isset($cargo['losses_percentage']) ? ($cargo['losses_percentage']=='35') ? 'selected' : '' : '';?>>35%</option>
                                              
                              </select>
                            </span>
                            <span class="span3">
                              <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
                             &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  efp price_format_decimal" type="text" name="losses_premium" placeholder="" value="<?php echo $losses_premium; ?>" readonly>
                             
                            </span>
                        </div>
                       <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                           <span class="span4">
                          <h5 class="heading pull-left heading-4" style="text-align:right">Drivers</h5>&nbsp;
                           <select name="drivers_surcharge" class="select-3 span6 efl">
                            <option value="None" <?php echo isset($cargo['drivers_surcharge']) ? ($cargo['drivers_surcharge']=='None') ? 'selected' : '' : '';?>>None </option>
                             <option value="Yes" <?php echo isset($cargo['drivers_surcharge']) ? ($cargo['drivers_surcharge']=='Yes') ? 'selected' : '' : '';?>>Yes</option>                                
                          </select>
                         
                        </span>
                           <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
                         <span class="span2">           
                         <!-- <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                          <select name="drivers_surcharge_percentage" class="select-3 span12 mkread1 efc" id="dsc" 
						  <?php echo isset($cargo['drivers_surcharge']) ? ($cargo['drivers_surcharge']=='Yes') ? '' : 'disabled' : 'disabled';?> >
                             <option value="0" <?php echo isset($cargo['drivers_surcharge_percentage']) ? ($cargo['drivers_surcharge_percentage']=='0') ? 'selected' : '' : '';?>>0</option>
                                 <option value="5"<?php echo isset($cargo['drivers_surcharge_percentage']) ? ($cargo['drivers_surcharge_percentage']=='5') ? 'selected' : '' : '';?>>5%</option>
                                 <option value="10" <?php echo isset($cargo['drivers_surcharge_percentage']) ? ($cargo['drivers_surcharge_percentage']=='10') ? 'selected' : '' : '';?>>10%</option>
                                 <option value="15" <?php echo isset($cargo['drivers_surcharge_percentage']) ? ($cargo['drivers_surcharge_percentage']=='15') ? 'selected' : '' : '';?>>15%</option>
                                 <option value="20" <?php echo isset($cargo['drivers_surcharge_percentage']) ? ($cargo['drivers_surcharge_percentage']=='20') ? 'selected' : '' : '';?>>20%</option>
                                 <option value="25" <?php echo isset($cargo['drivers_surcharge_percentage']) ? ($cargo['drivers_surcharge_percentage']=='25') ? 'selected' : '' : '';?>>25%</option>
                                 <option value="30" <?php echo isset($cargo['drivers_surcharge_percentage']) ? ($cargo['drivers_surcharge_percentage']=='30') ? 'selected' : '' : '';?>>30%</option>
                                 <option value="35" <?php echo isset($cargo['drivers_surcharge_percentage']) ? ($cargo['drivers_surcharge_percentage']=='35') ? 'selected' : '' : '';?>>35%</option>                                
                          </select>
                         
                        </span>
                        <span class="span3">
                          <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
                         &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  efp price_format_decimal" type="text" name="driver_surcharge_premium" placeholder="" value="<?php echo $driver_surcharge_premium ; ?>" readonly>
                         
                        </span>
                        </div>
                   
                          <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                               <span class="span4">
                              <h5 class="heading pull-left heading-4" style="text-align:right">Old units</h5>&nbsp;
                               <input class="textinput span6 pull-left textinput-3 textinput-4 ou numr_valid pdds" type="text" name="old_units"    value="<?php echo $old_units; ?>">
                               
                               <!--<select name="old_units" class="select-3 span6 efl efl2">
                                <option value="No">No</option>
                                 <option value="Yes">Yes</option>                                
                              </select>-->
                             
                            </span>
                               <span style="        padding: 0px 0px 5px 14px;        display: block;        float: left;    ">@</span> 
                             <span class="span2">           
                              <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                              <select name="old_units_surcharge" class="select-3 span12 mkread1 oup" id="oup" <?php echo ($old_units==''||$old_units==0||$old_units==null) ? 'disabled' : '';?>>                  
                                 <option value="0" <?php echo isset($cargo['old_units_surcharge']) ? ($cargo['old_units_surcharge']=='0') ? 'selected' : '' : '';?>>0</option>     
                                 <option value="200" <?php echo isset($cargo['old_units_surcharge']) ? ($cargo['old_units_surcharge']=='200') ? 'selected' : '' : '';?>>$200</option>                           
                              </select>
                            </span>
                            <span class="span3">
                              <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
                             &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  ofp price_format" type="text" name="old_units_premium" placeholder="" value="<?php echo $old_units_premium; ?>" readonly>
                             
                            </span>
                          </div>
                  </div>
           </div>
            <div class="row-fluid">
                <span class="custom_span2">
                  <h5 class="heading  heading-4 total_cargo">Total cargo premium</h5>
                  <input class="textinput span12 pull-left textinput-3 textinput-4 total_cargo price_format_decimal" type="text" name="total_cargo_premium" placeholder="" id="total_cargo_premium1" readonly value="<?php echo $total_cargo_premium; ?>">
                 
                </span>
                <span class="span3">
                  <h5 class="heading  heading-4">Carrier</h5>
                  <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="carrier" placeholder="">-->
                  <div class="cargo_carrier_drop_down">
                      <?php
					 // print_r($carriers);
                          $attr1 = "id='carrier_cargo' class='span12 ui-state-valid select-required' onchange='updatePolicyValue(&quot;cargo&quot;, this.value)'";
                         ;
                           if($carriers!=''){ $carriers = $carriers;  } else {$carriers = array();}
                      ?>
                      <?php echo form_dropdown('carriers', $carriers,$broker_state, $attr1); ?>
                  </div>
                </span>
                 <span class="span2">
                  <h5 class="heading  heading-4">Cargo police fee</h5>
                  <input class="textinput span10 pull-left textinput-3 textinput-4 price_format " type="text" name="cargo_policy_fee" placeholder=""  value="<?php echo $cargo_policy_fee ; ?>">
                 
                </span>
                 <span class="span2">
                  <h5 class="heading  heading-4">Cargo filing fee</h5>
                  <input class="textinput span10 pull-left textinput-3 textinput-4 price_format text-required" type="text" name="cargo_filing_fee" placeholder="" maxlength="" value="<?php echo isset($cargo_filing_fee) ? $cargo_filing_fee : '$ 0'; ?>">
                 
                </span>
                <span class="span2">
                  <h5 class="heading  heading-4"> Cargo SLA tax</h5>
                  <input class="textinput span10 pull-left textinput-3 textinput-4 price_format_decimal text-required" type="text" name="cargo_sla_tax" placeholder=""  value="<?php echo $cargo_sla_tax; ?>">
                 
                </span>
                 
          </div>
            <div class="row-fluid">
                <span class="span7">
                  <h5 class="heading ">Commodities</h5>
                  <input class="textinput span15" type="text" placeholder="" name="commodities"  value="<?php echo $commodities ; ?>">
                </span>
                 <span class="span4">
                  <h5 class="heading  heading-4">Required For FIRM quote</h5>
                  <input class="textinput span8 pull-left textinput-3 textinput-4" type="text" name="required_for_firm_quote" placeholder="" value="<?php echo $required_for_firm_quote; ?>">
                </span>
           </div>
            <div class="row-fluid">
                <span class="span12">
                  <h5 class="heading pull-left">Comments &nbsp;&nbsp;</h5>
                  <input class="textinput span9" type="text" placeholder="" name="comments" value="<?php echo $comments; ?>">
                </span>
           </div>
          <!-- <input type="hidden" class="cargo_id" value="" name="cargo_id">-->
           <input type="hidden" name="bundleid" class="bundleid" value="<?php echo $bundleid; ?>">
              <div id='loader' style="display:none"><img src="<?php echo base_url(); ?>images/spinner.gif"/></div><div id="cargo_sheet_message"></div>
               <button class="btn btn-primary" id="cargo_submit" type="submit" >Save Draft</button>
      <!--onClick="form_sub()"-->
             <input type="hidden" name="for_submit" value="1" />
             
              <input type="hidden" name="quote_id" value="<?php echo $quote_id; ?>" id="quote_id"/>
              <input type="hidden" id="base_url" name="" value="<?php echo base_url();?>" />
              <input type="hidden" name="action" value="<?php echo $action; ?>" />
              
          </form>
          
     
     </div>
  </div>
<script>
changeCarrierC();
function changeCarrierC(){
	var val = $('#main_form2 select[name=state]').val();
	var type = 'cargo';
	var selected = '<?php echo $cargo_carrier; ?>';
	var url = '<?php echo base_url('ratesheet'); ?>/get_carrier_by_state';
	changeCarrier1(val, type, url, selected)
}

</script>