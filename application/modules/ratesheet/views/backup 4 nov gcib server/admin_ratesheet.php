	<?php  
		if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
		{	
			
			if($this->session->userdata('admin_id')){		
				$this->load->view('includes/admin_header');	
			} else if($this->session->userdata('underwriter_id')){	
				$this->load->view('includes/header');	
			}
		} 
		function getAmount($money)
		{
			$cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
			$onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);
		
			$separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;
		
			$stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
			$removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);
		
			return (float) str_replace(',', '.', $removedThousendSeparator);
		}
	?>
	<?php $segement = $this->uri->segment(2); ?>
    <?php $data['segement'] = $segement;?>
	<style>
			label.cabinet {
				width: 79px;
				background: url(images/upload_icon.png) 0 0 no-repeat;
				display: block;
				overflow: hidden;
				cursor: pointer;
				width:62px; margin-bottom:5px; cursor:pointer;height:23px;
			}
			
			.sheet-header
			{
				cursor:pointer;
			}
	 
	 
			label.cabinet input.file {
				position: relative;
				cursor: pointer;
				height: 100%;
				width: 120px;
				opacity: 0;
				-moz-opacity: 0;
				filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
			
			}
			.container-fluid {
				display:table;
			}
	</style>

	<?php
		$ratesheet_id = 1;
	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html>
	<head>
		<title> Rate Sheet </title>
		<link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
		<link href="<?php echo base_url('css');?>/common.css" rel="stylesheet">
		<link href="<?php echo base_url('css');?>/meetingminute.css" rel="stylesheet">
		<script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
		<script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
		<script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script>
		<script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
         <script>
		$(document).ready(function(e) {            
       
		$('.price_format_decimal').priceFormat({
		  prefix: '$ ',
		  centsLimit: 2,
		  thousandsSeparator: ','
		});
		
		
		});
		function applyDecimal(element, value, decimal){
			element.val(value.toFixed(decimal));
			element.priceFormat({
				prefix: '$ ',
				centsLimit: 2,
				thousandsSeparator: ','
			});
			$('.price_format_decimal').priceFormat({
				prefix: '$ ',
				centsLimit: 2,
				thousandsSeparator: ','
			});
			$('.price_format').priceFormat({
				prefix: '$ ',
				centsLimit: 0,
				thousandsSeparator: ','
			});
		}
		function isInt(n) {
		   return n % 1 === 0;
		}
		</script>
        <script src="<?php echo base_url('js'); ?>/ratesheet.js"></script>
        <script src="<?php echo base_url('js'); ?>/cargo.js"></script>
        <script src="<?php echo base_url('js'); ?>/pd.js"></script>
        <script src="<?php echo base_url('js'); ?>/liability.js"></script>
       
        
        
		<!--script src="<?php echo base_url('js'); ?>/jquery.validateForm.js"></script-->
       
		
		<script type='text/javascript'>
			function return_numeric(no)
			{
				
				//return no.substring(1));
				return Number(no.replace(/[^0-9\.]+/g,""));
			}
			function apply_on_all_elements(){
		
				$('input').not('.datepick').autotab_magic().autotab_filter();
				$('.price_format').priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});
			}
			
			
			
			$(document).ready(function() {
				<!----disabled system---> 
				apply_on_all_elements();
			
				//$('.show_tractor_others').hide();
				$(".mkread").attr("disabled","true");
				<?php if($segement=='generate_ratesheet'){?>
				$(".mkread1").attr("disabled","true"); 
				<?php } ?>
				//$(".total_cargo").attr("disabled","true");		
				
				
				
				
				/*$("#bsr,.efp").keyup(function(){
					calctotalcargo();
				});*/
				
				/*$('.npu').change(function(){
					base_unit=	$('.npu').val();
					total_cargo_premium = 0;
					$('.row-efl').each(function(){
						if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined) && ($(this).find('.efl').val() != '0') && ($(this).find('.efl').val() != ''))
						{
							var val=$(this).find('.efc').val();
							premium = base_unit * val;
							$(this).find(".efp").val(premium);	
						}
					});
					$('.price_format').priceFormat({
							prefix: '$ ',
							centsLimit: 0,
							thousandsSeparator: ','
						});
					calctotalcargo();
				});*/
				
				
				
				
				/* liability script starts */
				
					
				
					
					
				
					
					
					
					
					
				
				
				
				$('.total_pd_pr').change(function(){
				
					//calculatepd();
						
				})	
				
				
				
				
				$('body').on('change','.number_of_trailer',function(e){
					e.preventDefault();
					var total_no = 0;
					var val = $(this).val();
					var currcount = $(".trailerrw").length;
					var diff = currcount - val;
					
					if(diff < 0) {
						while(diff < 0){
							var num     = $('.trailerrw').length+1;
							var newNum  = new Number(num - 1);  
							var newElem = '<tr id="trailerrw'+num+'" class="trailerrw"><td class="vehicle_name">Trailer '+num+'<input type="hidden" name="trailername_'+num+'" value="Trailer '+num+'"></td><td style="text-align:center"><input type="text" class="trailer_value vehicle_value span6 ui-state-valid price_format" value="" name="trailer_val'+num+'"></td><td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="" name="trailer_ded'+num+'"></td><td style="text-align:center"><input type="text" name="trailer_rate'+num+'" class="span6 vehicle_rate ui-state-valid"></td><td style="text-align:center"><input type="text" name="trailer_flat_rate'+num+'" class="span6 numr_valid vehicle_frate ui-state-valid"></td><td style="text-align:center"><input type="text" name="trailer_premium'+num+'" class="span6 trailer_prem price_format_decimal vehicle_rp ui-state-valid"></td></tr>';
							
							if(currcount == 0)
							{
								if($(".tractorrw").length != 0)
									$(".tractorrw:last").after(newElem);
								/*else if($(".truckrw").length != 0)
									$(".truckrw:last").after(newElem);*/
								else
									$("#add_owner_row").prepend(newElem);
								currcount = 1;
							}
							else
							{
								$('.trailerrw:last').after(newElem);	
							}
							$('#trailerrw' + num +' .vehicle_name').html('Trailer '+num);
							diff++;
						} 
					 }
					 else if(diff > 0)
					 {
						while(diff > 0){
						
							 var v = $('tr.trailerrw:last').remove() ; 
							 diff--;
						 }
					 }	
					apply_on_all_elements();
				});
				$('body').on('change','.no_of_truck',function(e){
					e.preventDefault();
					var total_no = 0;
					var val = $(this).val();
					var currcount = $(".truckrw").length;
					var diff = currcount - val;
					
					if(diff < 0) {
						while(diff < 0){
							var num     = $('.truckrw').length+1;
							var newNum  = new Number(num - 1);  
							var newElem = '<tr id="truckrw'+num+'" class="truckrw"><td class="vehicle_name">Truck '+num+'<input type="hidden" name="truckname_'+num+'" value="Truck '+num+'"></td><td style="text-align:center"><input type="text" class="truck_value vehicle_value span6 ui-state-valid price_format" value="" name="truck_val'+num+'"></td><td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="" name="truck_ded'+num+'"></td><td style="text-align:center"><input type="text" name="truck_rate'+num+'" class="span6 vehicle_rate ui-state-valid"></td><td style="text-align:center"><input type="text" name="truck_flat_rate'+num+'" class="span6 numr_valid vehicle_frate ui-state-valid"></td><td style="text-align:center"><input type="text" name="truck_premium'+num+'" class="span6 truck_prem1 vehicle_rp price_format_decimal ui-state-valid"></td></tr>';
							if(currcount == 0)
							{
								if($(".tractorrw").length == 0)
									$("#add_owner_row").prepend(newElem);
								else
									$(".tractorrw:last").after(newElem);
								currcount = 1;
							}
							else
							{
								$('.truckrw:last').after(newElem);	
							}
							$('#truckrw' + num +' .vehicle_name').html('Truck '+num);
							diff++;
						} 
					 }
					 else if(diff > 0)
					 {
						while(diff > 0){
						
							 var v = $('tr.truckrw:last').remove() ; 
							 diff--;
						 }
					 }	
					apply_on_all_elements();
				});
				$('body').on('change','.no_of_tractor',function(e){
					
					e.preventDefault();
					var total_no = 0;
					var val = $(this).val();
					var currcount = $(".tractorrw").length;
					var diff = currcount - val;
					
					if(diff < 0) {
						while(diff < 0){
							var num     = $('.tractorrw').length+1;
							var newNum  = new Number(num - 1);  
							var newElem = '<tr id="tractorrw'+num+'" class="tractorrw"><td class="vehicle_name">Tractor '+num+'<input type="hidden" name="tractorname_'+num+'" value="Tractor '+num+'"></td><td style="text-align:center"><input type="text" class="tractor_value vehicle_value span6 ui-state-valid price_format" value="" name="tractor_val'+num+'"></td><td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="" name="tractor_ded'+num+'"></td><td style="text-align:center"><input type="text" name="tractor_rate'+num+'" class="span6 vehicle_rate ui-state-valid"></td><td style="text-align:center"><input type="text" name="tractor_flat_rate'+num+'" class="span6 numr_valid vehicle_frate ui-state-valid"></td><td style="text-align:center"><input type="text" name="tractor_premium'+num+'" class="span6 tractor_prem price_format_decimal vehicle_rp ui-state-valid"></td></tr>';
							
							if(currcount == 0)
							{
								$("#add_owner_row").prepend(newElem);
								currcount = 1;
							}
							else
							{
								$('.tractorrw:last').after(newElem);	
							}
							$('#tractorrw' + num +' .vehicle_name').html('Tractor '+num);
							diff++;
						} 
					 }
					 else if(diff > 0)
					 {
						while(diff > 0){
						
							 var v = $('tr.tractorrw:last').remove() ; 
							 diff--;
						 }
					 }	
					apply_on_all_elements();
				});
				
				
				
				
				
				
				
				//* PD Script ends */
				
	<!--integer validation---->
				$(".numr_valid").keypress(function(evt){				
					var charCode = (evt.which) ? evt.which : event.keyCode;
					if (charCode != 46 && charCode > 31
					&& (charCode < 48 || charCode > 57))
					{
						//alert('Please enter numeric value');
						$(this).css("border", "1px solid #FAABAB");
						$(this).css("box-shadow", "0 0 5px rgba(204, 0, 0, 0.5");
						
						$(this).focus();
						return false;
					}			
					//return true;
					
	else {
						$(this).css("border", "1px solid #CCCCCC");
						$(this).css("box-shadow", "none");
					}
				});				
	<!--integer validation---->		
					
									
			});
			
				</script>
<script>
function updatePolicyValue(type, val){
	var url = '<?php echo base_url('ratesheet'); ?>/get_cargo_detail';
	updatePolicyValue1(type, val, url);	
}
</script>		
	</head>

	<body>

		   

<div class="container-fluid">
<?php $this->load->view('admin_cargo_ratesheet', $data); ?>
<?php $this->load->view('admin_pd_ratesheet', $data); ?>
  	<div id="bundle_message"></div>
    <div id='loader3' style="display:none">
      <img src="<?php echo base_url(); ?>images/spinner.gif"/>
    </div>
      <div id="cargo_sheet_message"></div>
      <button class="btn btn-primary" id="ratesheet_bundle_submit" type="submit" style="display:none">Submit Bundle</button>
  </div>
</div>
	
				<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script>
		var cargoresult = 'false';
		var pdresult = 'false';
		var libilityresult = 'false';
	$(document).ready(function(){
	
	
		$("#assign_producer").change(function(){
			if($(this).val() == 100)
			{
				$("#new_producer").show();
			}
			else
			{
				$("#new_producer").hide();
			}
		});
		//$(".sheet-content").hide();
		/*$(".sheet-header").click(function(){	
			$(this).parent().parent().parent().find(".sheet-content").slideToggle();
		});*/
		 $("#cargo-select").change(function () {
			 var show = this.value;
			 if(show=='yes'){ //alert('hello');
				 $(this).parent().parent().parent().parent().find(".sheet-content").show();
			 }else{
				 $(this).parent().parent().parent().parent().find(".sheet-content").hide();
			 }
		 });
		 
		 $("#pd-select").change(function () {
			 var show = this.value;
			 if(show=='yes'){ //alert('hello');
				 $(this).parent().parent().parent().parent().find(".sheet-content").show();
			 }else{
				 $(this).parent().parent().parent().parent().find(".sheet-content").hide();
			 }
		});
		
		$("#liability-select").change(function () {
			 var show = this.value;
			 if(show=='yes'){ //alert('hello');
				 $(this).parent().parent().parent().parent().find(".sheet-content").show();
			 }else{
				 $(this).parent().parent().parent().parent().find(".sheet-content").hide();
			 }
		});
		/*$("#carrier_cargo").change(function(){ 
			var val = $( "#carrier_cargo option:selected" ).val();
			var val1 = $("#quote_id").val();
			//alert(val1);
			var baseurl = "<?php echo base_url('ratesheet'); ?>/get_cargo_detail";
				$.ajax({
				  url: baseurl,
				  type: "post",
				  data: {'name' : val,'id':val1},
				 datatype: 'json',
				  success: function(data){
					 var obj = $.parseJSON(data);  
					// alert(data);
					 $("#cargo_policy_fee").val(obj['cargo_policy_fee']);
					 
					 $("#cargo_sla_tax").val(return_numeric($("#total_cargo_premium1").val()) * return_numeric(obj['slatax']));
					 $('.price_format').priceFormat({
						prefix: '$ ',
						centsLimit: 0,
						thousandsSeparator: ','
					});
				  },
				  error:function(){
					
				  }   
				});
		});*/
		/*$("#carrier_cargo1").change(function(){ 
			var val = $( "#carrier_cargo1 option:selected" ).val();
			var val1 = $("#quote_id1").val();
			
			var baseurl = "<?php echo base_url('ratesheet'); ?>/get_cargo_detail";
				$.ajax({
				  url: baseurl,
				  type: "post",
				  data: {'name' : val,'id':val1},
				 datatype: 'json',
				  success: function(data){
					 var obj = $.parseJSON(data);  
					 $("#pd_policy_fee").val(obj['cargo_policy_fee']);
					 $("#pd_sla_tax").val(return_numeric($("#total_pd_pr").val()) * return_numeric(obj['slatax']));
					  $('.price_format').priceFormat({
						prefix: '$ ',
						centsLimit: 0,
						thousandsSeparator: ','
					});
				  },
				  error:function(){
					
				  }   
				});
		});*/
	});

	function rofoperation(val , id){
				
			
			if(val == 'other'){
				$('.show_tractor_others').show();
			} else {
				$('.show_tractor_others').hide();
			}	
			
		}
		
		
		
	/*  alert(id);
	alert(val);  */

<!--ajax form submit-->

$("#cargo_submit").click(function(){
	
	valchk = true;
	$("#main_form2").find(".text-required").each(function(){
		
		if($(this).val() == '' )
		//if($(this).val() == '' || $(this).val() == '$ 0')
		{
			$(this).css("border","1px solid red");
			valchk = false;
		}
	});
	$("#main_form2").find(".select-required").each(function(){
		
		if($(this).val() == null || $(this).val() == undefined || $(this).val() == '')
		{
			$(this).css("border","1px solid red");
			valchk = false;
		}
	});
	if(valchk == false)
	return false; 
	var baseurl = "<?php echo base_url('ratesheet'); ?>/save_cargo";
	cargoresult = 'true';
	$.ajax({
		  url: baseurl,
		  type: "post",
		  data: $("#main_form2").serialize(),
	 	 datatype: 'json',
		 beforeSend: function() {
			 $('#loader').show();
		  },
		  complete: function(){
			 $('#loader').hide();
		  },
		  success: function(data){
		 // alert(data);
				// $("#main_form2 input,#main_form2 select").attr("disabled",true);
				 $("#cargo_sheet_message").html("Cargo Rate Sheet Draft Saved");
				 
				 $("#ratesheet_bundle_submit").show(); //22/8/2014 girish gnaguly
				 
				 
				 if(($(".bundleid").first().val() == ''))
				 {
					//$("#ratesheet_bundle_submit").show();
					$(".bundle_mail").show();
				 }
				 $(".bundleid").val(data);
				
				 
		  },
		  error:function(){
			
		  }   
		}); //alert(result);
		
});



$("#liability_submit").click(function(){
	valchk = true;
	$("#main_form4").find(".text-required").each(function(){
		if($(this).val() == '' || $(this).val() == '$ 0')
		{
			$(this).css("border","1px solid red");
			valchk = false;
		}
	});
	$("#main_form4").find(".select-required").each(function(){
		
		if($(this).val() == null || $(this).val() == undefined || $(this).val() == '')
		{
			$(this).css("border","1px solid red");
			valchk = false;
		}
	});
	if(valchk == false)
	return false; 
	var baseurl = "<?php echo base_url('ratesheet'); ?>/save_liability";
	libilityresult = 'true';
	$.ajax({
		  url: baseurl,
		  type: "post",
		  data: $("#main_form4").serialize(),
	 	 datatype: 'json',
		  beforeSend: function() {
			 $('#loader1').show();
		  },
		  complete: function(){
			 $('#loader1').hide();
		  },
		  success: function(data){
			//	 $("#main_form4 input,#main_form4 select").attr("disabled",true);
				 $("#liability_sheet_message").html("Auto Liability Rate Sheet Draft Saved"); 
				 $("#ratesheet_bundle_submit").show(); //22/8/2014 girish gnaguly
				if(($(".bundleid").first().val() == ''))
				 {
					//$("#ratesheet_bundle_submit").show();
					$(".bundle_mail").show();
				 }			 
				 $(".bundleid").val(data);
				 
		  },
		  
		  error:function(){
			
		  }   
		}); 
});

$("#ratesheet_bundle_submit").click(function(){
	//alert();
	var cargo 		= $('#cargo-select').val();
	var pd 			= $('#pd-select').val();
	var liability	= $('#liability-select').val();
	var set = 'true';
	
	if(cargo == 'yes'){
		 set1 = $("#cargo_submit").trigger("click");
	  	if(cargoresult != 'true'){ 
			set = 'false'; 
		}
	}
	
	if(pd == 'yes'){
		 $("#physical_damage_submit").trigger("click");
		 if( libilityresult != 'true' ){
			set = 'false'; 
		}
	}
	
	if(liability == 'yes'){
		$("#liability_submit").trigger("click");
		 if( pdresult != 'true'){
				set = 'false'; 
			}
	}
	/*alert(cargoresult);
	alert(libilityresult);
	alert(pdresult);
	alert(set);*/
if(set == 'true' ){
	var baseurl2 = "<?php echo base_url('ratesheet'); ?>/submit_bundle";
	$.ajax({
		  url: baseurl2,
		  type: "post",
		  data: {'quote_id':$("#quote_id1").val(),'bundle_id':$(".bundleid").first().val()},
	 	  datatype: 'json',
		  beforeSend: function() {
			 $('#loader3').show();
		  },
		  complete: function(){
			 $('#loader3').hide();
		  },
		  success: function(data){
		  //alert(data);
			$("#bundle_message").html("Bundle Submitted");
			
		  },
		  error:function(){
			
		  }   
		});
}
});

$("#physical_damage_submit").click(function(){
	
	var baseurl2 = "<?php echo base_url('ratesheet'); ?>/save_physical_damage";
	pdresult = 'true';
	$.ajax({
		  url: baseurl2,
		  type: "post",
		  data: $("#main_form3").serialize(),
	 	  datatype: 'json',
		  beforeSend: function() {
			 $('#loader2').show();
		  },
		  complete: function(){
			 $('#loader2').hide();
		  },
		  success: function(data){
		  //alert(data);
		//	$("#main_form3 input,#main_form3 select").attr("disabled",true);
			$("#physical_damage_sheet_message").html("Physical Damage Rate Sheet Draft Saved");
			
			$("#ratesheet_bundle_submit").show(); //22/8/2014 girish gnaguly
			
			if(($(".bundleid").first().val() == ''))
			 {
			 
				//$("#ratesheet_bundle_submit").show();
				$(".bundle_mail").show();
			 }
			$(".bundleid").val(data);
			
		  },
		  error:function(){
			//alert('e');
		  }   
		}); 
});

	$('.price_format_float').priceFormat({
						prefix: '$ ',
						centsLimit: 1,
						thousandsSeparator: ','
					});


	</script>
    
<script>
function changeCarrier(val, type){		
	var selected = '';
	var url = '<?php echo base_url('ratesheet'); ?>/get_carrier_by_state';
	changeCarrier1(val, type, url, selected)
}
/*function changeCarrier1(val, type, url, selected)
{
	var baseurl = url;
	var val1 = $("#quote_id").val();
	$.ajax({
	  url: baseurl,
	  type: "post",
	  data: {'state' : val,'id':val1,'type':type, 'selected':selected},
	 datatype: 'json',
	  success: function(data){
			//alert(data);	
			if(type=='cargo') { 
				$('.cargo_carrier_drop_down').html(data);	
				updatePolicyValue(type, '');	
			}else if(type=='pd') {
				$('.pd_carrier_drop_down').html(data);	
				updatePolicyValue(type, '');
			}else if(type=='liability'){					
				$('.liability_carrier_drop_down').html(data);	
				updatePolicyValue(type, '');
			}
	  },
	  error:function(){
		
	  }   
	});
}
function updatePolicyValue1(type, val, url){	
if(val==''){
	if(type=='cargo'){
		var val = $('#carrier_cargo option:selected').val();
	} else if(type=='pd'){
		var val = $('#carrier_cargo2 option:selected').val();
	}else if(type=='liability'){
		var val = $('#carrier_cargo3 option:selected').val();
	}
}*/
/*var val1 = $("#quote_id").val();
if(val!='')
{
	var baseurl = url;
		$.ajax({
		  url: baseurl,
		  type: "post",
		  data: {'name' : val,'id':val1,'type':type},
		 datatype: 'json',
		  success: function(data){
			 var obj = $.parseJSON(data);  
			 //alert(data);
			 if(type=='cargo'){
				 $("#cargo_policy_fee").val(obj['cargo_policy_fee']);					 
				// console.log(return_numeric(obj['slatax']));
				 //console.log(return_numeric($("#total_cargo_premium1").val()));
				 var sla_tax = return_numeric($("#total_cargo_premium1").val()) * return_numeric(obj['slatax']);	
				 sla_tax = sla_tax/100; 						 	 
				 var element = $("#cargo_sla_tax");
				 applyDecimal(element, sla_tax, 2);
			}else if(type=='pd') {
				 $("#pd_policy_fee").val(obj['cargo_policy_fee']);					 
				 var sla_tax = return_numeric($("#total_pd_pr").val()) * return_numeric(obj['slatax']);	
				 sla_tax = sla_tax/100; 						 			 
				 var element = $("#pd_sla_tax");
				 applyDecimal(element, sla_tax, 2);
			}else if(type=='liability') {
				 //$("#liability_policy_fee").val(obj['cargo_policy_fee']);					 
				 //$("#cargo_sla_tax").val(return_numeric($("#total_cargo_premium1").val()) * return_numeric(obj['slatax']));
			}
			 $('#cargo_policy_fee').priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});
				$('.price_format').priceFormat({
					prefix: '$ ',
					centsLimit: 0,
					thousandsSeparator: ','
				});
		  },
		  error:function(){
			
		  }   
		});
	}
	else
	{
		$('#cargo_policy_fee').val('');
		$('#cargo_sla_tax').val('');
		$('#pd_policy_fee').val('');
		$('#pd_sla_tax').val('');
	}
}*/
 </script>   
    
   
    
    
    
	</body>
	</html>
	<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{
		$this->load->view('includes/footer');
	}
	?>