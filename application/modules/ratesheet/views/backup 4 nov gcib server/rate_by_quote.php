<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
			if($this->session->userdata('admin_id')){		
				$this->load->view('includes/admin_header');	
			} else if($this->session->userdata('underwriter_id')){	
				$this->load->view('includes/header');	
			}  else if($this->session->userdata('member_id')){	
				$this->load->view('includes/header');	
			}
		
	} 
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>

<br>
<h1>Quote List</h1>
<div class="table"> 
<!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> 
<!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<!--<a class="fancybox-add btn btn-primary" onclick="javascript:void(0)" href="">Add New</a>-->

  <!-- Quote List -->
  <input type="hidden" name="requested_by" id="requested_by" value="<?php echo $user ?>" />
 
  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">Sr.</th>
        <th width="15%">Email</th>
        <th width="15%">Total Cargo Premium</th>
        <th width="15%">Total PD Premium</th>
		<th width="15%">Total Liability Premium</th>
		<th width="15%">Created On</th>
		<th width="15%">Status</th>
		<th width="15%">Action</th>
	</tr>
    </thead>
    <tbody>
    <? print_r($rows);?>
		<?php $cn = 0; foreach($rows as $row) {  $cn++; ?>
			<tr>
				<td class="center" width="10%"><?php echo $cn; ?></td>
				<td width="15%"><?php echo $row[0]['email']; ?></td>
				<td width="15%"><?php echo $row[0]['total_cargo_premium']; ?></td>
				<td width="15%"><?php echo $row[0]['total_pd_premium']; ?></td>
				<td width="15%"><?php echo $row[0]['total_liability_premium']; ?></td>
				<td width="15%"><?php echo $row[0]['creation_date']; ?></td>
				<td width="15%"><?php echo $row[0]['status']; ?></td>
				<?php if($this->session->userdata('underwriter_id') || $this->session->userdata('admin_id')  ){	?>
				<td width="15%"><a class="btn btn-primary" href="<?php echo base_url(); ?>ratesheet/edit_ratesheet/<?php echo $row[0]['quote_id']; ?>">Edit</a></td>
				<?php } else { ?>
				<td width="15%"><a class="btn btn-primary" href="<?php echo base_url(); ?>ratesheet/view_ratesheet/<?php echo $row[0]['quote_id']; ?>">Open</a></td>
				<?php } ?>
			  </tr>
		<?php } ?>
    </tbody>
  </table>
  <!-- Quote List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>
