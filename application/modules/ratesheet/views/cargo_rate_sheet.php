<?php  
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
?>

<style>
		label.cabinet {
			width: 79px;
			background: url(images/upload_icon.png) 0 0 no-repeat;
			display: block;
			overflow: hidden;
			cursor: pointer;
			width:62px; margin-bottom:5px; cursor:pointer;height:23px;
		}
 
		label.cabinet input.file {
			position: relative;
			cursor: pointer;
			height: 100%;
			width: 120px;
			opacity: 0;
			-moz-opacity: 0;
			filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
		
		}
		.container-fluid {
			display:table;
		}
</style>

<?php
	$ratesheet_id = 1;
?>





<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title> cargo_rate_sheet </title>
    <link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
    <link href="<?php echo base_url('css');?>/common.css" rel="stylesheet">
    <link href="<?php echo base_url('css');?>/meetingminute.css" rel="stylesheet">
    <script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
    <script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
    <script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script >
    <script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>
    
    <script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
    
    <script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
    <!--script src="<?php echo base_url('js'); ?>/jquery.validateForm.js"></script-->
    
    <script type='text/javascript'>
		$(document).ready(function() {
			<!----readonly system---> 

		$('.show_tractor_others').hide();
			$(".mkread").attr("readonly","true"); 
			$(".total_cargo").attr("readonly","true");		
			$('.efl,.ou').change(function(){
				
				if (($(this).val()=="None") || ($(this).val()=="0") || ($(this).val()=="No"))
				{
					$(this).parents('.row-efl').find('.mkread').attr("readonly",true); 			
				}
				else
				{
					$(this).parents('.row-efl').find('.mkread').attr("readonly",false);
				}
			});
			
			$(".oup").change(function(){
				old_unit = $(".ou").val();
				val = $("#oup").val();
				total = old_unit * val;
				$(".ofp").val(total);
				
				total_cargo_premium = 0;
				$('.row-efl').each(function(){
					if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined))
					{
						//alert($(this).find('.efl').val());
						premium=$(this).find('.efp').val();
						total_cargo_premium = total_cargo_premium + parseInt(premium); 	
					}
				});	
				total_cargo_premium = total_cargo_premium + parseInt(total);
				
				base_unit=	$('.npu').val();
				base_rate = $("#bsr").val();
				npu_br = 	parseInt(base_unit) * parseInt(base_rate);
				if(npu_br != '')
				{
					total_cargo_premium = total_cargo_premium + parseInt(npu_br);
				}
				$(".total_cargo").val(total_cargo_premium);
			});
			
			
			
			$("#bsr").change(function(){
				
				total_cargo_premium = 0;
				$('.row-efl').each(function(){
					if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined))
					{
						//alert($(this).find('.efl').val());
						premium=$(this).find('.efp').val();
						total_cargo_premium = total_cargo_premium + parseInt(premium); 	
					}
				});	
				base_unit=	$('.npu').val();
				base_rate = $("#bsr").val();
				npu_br = 	parseInt(base_unit) * parseInt(base_rate);
				if(npu_br != '')
				{
					total_cargo_premium = total_cargo_premium + parseInt(npu_br);
				}
				var old_unit_premium = $(".ofp").val();
				if(old_unit_premium != '')
				{
					total_cargo_premium = total_cargo_premium + parseInt(old_unit_premium);
				}
				$(".total_cargo").val(total_cargo_premium);
			});
			
			
			$('.npu').change(function(){
				base_unit=	$('.npu').val();
				total_cargo_premium = 0;
				$('.row-efl').each(function(){
					if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined))
					{
						var val=$(this).find('.efc').val();
						premium = base_unit * val;
						$(this).find(".efp").val(premium);	
						total_cargo_premium = total_cargo_premium + parseInt(premium); 	
					}
				});
				var old_unit_premium = $(".ofp").val();
				if(old_unit_premium != '')
				{
					total_cargo_premium = total_cargo_premium + parseInt(old_unit_premium);
				}
				base_rate = $("#bsr").val();
				npu_br = 	parseInt(base_unit) * parseInt(base_rate);
				if(npu_br != '')
				{
					total_cargo_premium = total_cargo_premium + parseInt(npu_br);
				}
				$(".total_cargo").val(total_cargo_premium);
				
					
			})
			
			
		
			$('.efc').change(function(){
				base_unit=	$('.npu').val();
				val = $(this).val();
				total =  base_unit * val;
				$(this).parent().parent().find(".efp").val(total);
				var total_cargo_premium = 0;		
				$('.row-efl').each(function(){
					if(($(this).find('.efl').val() != 'No') && ($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined))
					{
						//alert($(this).find('.efl').val());
						premium=$(this).find('.efp').val();
						total_cargo_premium = total_cargo_premium + parseInt(premium); 	
					}
				});	
				var old_unit_premium = $(".ofp").val();
				if(old_unit_premium != '')
				{
					total_cargo_premium = total_cargo_premium + parseInt(old_unit_premium);
				}
				base_rate = $("#bsr").val();
				npu_br = 	parseInt(base_unit) * parseInt(base_rate);
				if(npu_br != '')
				{
					total_cargo_premium = total_cargo_premium + parseInt(npu_br);
				}
				$(".total_cargo").val(total_cargo_premium);
				
			})
			
			
			
<!--integer validation---->
			$(".numr_valid").keypress(function(evt){				
				var charCode = (evt.which) ? evt.which : event.keyCode;
				if (charCode != 46 && charCode > 31
				&& (charCode < 48 || charCode > 57))
				{
					//alert('Please enter numeric value');
					$(this).css("border", "1px solid #FAABAB");
					$(this).css("box-shadow", "0 0 5px rgba(204, 0, 0, 0.5");
					
					$(this).focus();
					return false;
				}			
				//return true;
				
else {
					$(this).css("border", "1px solid #CCCCCC");
					$(this).css("box-shadow", "none");
				}
			});				
<!--integer validation---->		
				
								
        });
		


			</script>
    
</head>

<body>

	<?php
		$attributes = array('class' => 'request_ratesheet', 'id' => 'main_form2');
		echo form_open_multipart(base_url('ratesheet'), $attributes);
		
    ?>   
                

    <div class="container-fluid">
     <div class="row-fluid">
            <span class="span12">
              
              <div class="well pull-center well-3 pga">
                <h4 class="heading pull-center heading-3">Cargo Rate sheet</h4>           
              </div>
            </span>
          </div>
           <div class="well well-3 well">
           
           
            <div class="row-fluid">
            
            	<?php
					$ratesheet_broker_name = '';
					/* if(isset($user_quote_data) && !empty($user_quote_data)) {
						$broker_name_first = $user_quote_data->first_name;
						$broker_name_middle = $user_quote_data->middle_name;
						$broker_name_last = $user_quote_data->last_name;
					} else {
						$broker_name_first = isset($quote->contact_name) ? $quote->contact_name : '';
						$broker_name_middle = isset($quote->contact_middle_name) ? $quote->contact_middle_name : '';
						$broker_name_last = isset($quote->contact_last_name) ? $quote->contact_last_name : '';
					} */
				?>
             
              <span class="span6">
                <h5 class="heading  heading-4">Broker</h5>
               <input type="text" class="span9 brokername" name="broker" placeholder="Full Name/ DBA" value="<?php echo $ratesheet_broker_name; ?>"  <?php    if (!empty($ratesheet_broker_name)) { echo "readonly";} ?> >
              </span>
              <span class="span6">
                <h5 class="heading ">Insured</h5>
                <input type="text" class="span9 insuredname" name="insured" placeholder="Full Name/ DBA		">
              </span>
            </div>
            
            
            
              <div class="row-fluid">
              <span class="span2">
                <h5 class="heading  heading-3 hed-3">Radius of Operation</h5>
                <!--<input class="textinput span10 pull-left textinput-3 textinput-4 radiusofoperation" type="text" name="radius_of_operation" placeholder="">-->                
                <select name="radius_of_operation" class="select-3 span12 tractor_radius_of_operation" id="tractor_radius_of_operation" onChange="rofoperation(this.value);">
                    <option value="0-100 miles">0-100 miles</option>
                    <option value="101-500 miles" >101-500 miles</option>           	
                    <option value="501+ miles">501+ miles</option>
                    <option value="other">Other</option>            
                </select><br>
					 <input class="textinput span12 pull-left show_tractor_others" type="text" name="claim_venue" placeholder="">

              </span>
              
               <span class="span1">		
                <h5 class="heading  heading-4 hed-3">State</h5>
                <div class="textinput span12 ">
					
					<?php
					$attr = "class='span12 ui-state-valid'";
					$broker_state = 'CA';
					
					?>
					<?php echo get_state_dropdown('insured_state[]', $broker_state, $attr); ?>

				</div>
                
               
              </span>
               <span class="span1">
                <h5 class="heading  heading-4 hed-3">Claim Venue</h5>
                <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="claim_venue" placeholder="">
               
              </span>
              <span class="span2">
                <h5 class="heading  heading-4 hed-3">Number of Power Units</h5>
                <input class="textinput span10 pull-left textinput-3 textinput-4 npu" type="text" name="no_of_power_unit" placeholder="">
               
              </span>
               <span class="span1">
                <h5 class="heading  heading-4 hed-3"> Limit</h5>
                <input class="textinput span10 pull-left textinput-3 textinput-4" type="text" name="limit" placeholder="">
               
              </span>
               <span class="span1">
                <h5 class="heading  heading-4 hed-3">Deductible</h5>
                <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="deductible" placeholder="">
               
              </span>  
               <span class="span2">
                <h5 class="heading  heading-4 hed-3"> Reffer Deductible</h5>
                <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="reffer_deductible" placeholder="">
               
              </span>      
               <span class="span2" >
                <h5 class="heading  heading-4 hed-3">Base rate</h5>
                <input class="textinput span10 pull-left textinput-3 textinput-4" type="text" name="base_rate" placeholder="" id="bsr">
               
              </span>              
            </div>
            <div class="mid-wrapper">	
           <strong>Optional endorsement   &nbsp; &nbsp; &nbsp; *** Not affered to all clients***</strong><br /><br />
              <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                 <span class="span4">
                <h5 class="heading pull-left heading-4" style="text-align:right">Earned Freight</h5>&nbsp;
                 <select name="earned_freight_limit" class="select-3 span6 efl" id="">
                  <option value="None">None</option>
                   <option value="2500">$ 2,500</option>
                    <option value="3000">$ 3,000</option>
                                
                </select>
               
              </span>
                 <span style="
        padding: 0px 0px 5px 14px;
        display: block;
        float: left;
    ">@</span> 
               <span class="span2">           
               <!-- <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                <select name="earned_freight_cost" class="select-3 span12 mkread efc" id="efc">
                  <option value="0">0</option>
                   <option value="100">$ 100 </option>
                    <option value="200">$ 200</option>
                                
                </select>
               
              </span>
              <span class="span3">
                <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
               &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp" type="text" name="earned_freight_premium" placeholder="" readonly>
               
              </span>
            </div>
             <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                 <span class="span4">
                <h5 class="heading pull-left heading-4" style="text-align:right">Debris removal</h5>&nbsp;
                 <select name="debris_removal_limit " class="select-3 span6 efl">
                  <option value="None">None</option>
                  <option value="2500">$ 2,500</option>              
                </select>
               
              </span>
                 <span style="
        padding: 0px 0px 5px 14px;
        display: block;
        float: left;
    ">@</span> 
               <span class="span2">           
                <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
               <select name="debris_removal_cost" class="select-3 span12 mkread efc" id="drc">
                  <option value="0">0</option>
                   <option value="150">$ 150 </option>                                
                </select>
              </span>
              <span class="span3">
                <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
               &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp" type="text" name="debris_removal_premium" placeholder="" readonly>
               
              </span>
            </div>
              <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                 <span class="span4">
                <h5 class="heading pull-left heading-4" style="text-align:right">Tourpolin coverage</h5>&nbsp;
                 <select name="tarpauling_coverage_limit" class="select-3 span6 efl" >
                  <option value="None">None</option>
                   <option value="5000">$ 5,000</option>              
                </select>
               
              </span>
                 <span style="
        padding: 0px 0px 5px 14px;
        display: block;
        float: left;
    ">@</span> 
               <span class="span2">           
                <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                <select name="tarpauling_coverage_cost" class="select-3 span12 mkread efc" id="tcc">
                  <option value="0">0</option>
                   <option value="1000">$ 1,000 </option>                                
                </select>
               
              </span>
              <span class="span3">
                <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
               &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp" type="text" name="tarpauling_coverage_premium" placeholder="" readonly>
              </span>
            </div>
             
             
              <div class="row-fluid pull-center" style="text-align:right"> 
                 <span class="span6">
                <h5 class="heading pull-left heading-4" style="text-align:right">Reefer breakdown</h5>&nbsp;
                    <!-- <input class="textinput span8 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                     <select name="reefer_breakdown_limit" class="select-3 span6 ">
                  <option value="None">None</option>
                  <option value="Included in Base Rate">Included in Base Rate</option>
                </select>
                    
              </span>
              
      </div>
      <br />       <strong > Optional endoresment</strong>
      
      
      
      
      
      
      
      
      
      
      
           <div class="ho">
             <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                 <span class="span4">
                <h5 class="heading pull-left heading-4" style="text-align:right">Lapse in coverage</h5>&nbsp;
                 <select name="lapse_in_coverage_surcharge" class="select-3 span6 efl">
                  <option value="No">No</option>
                    <option value="Yes">Yes</option>           
                </select>
               
              </span>
                 <span style="
        padding: 0px 0px 5px 14px;
        display: block;
        float: left;
    ">@</span> 
               <span class="span2">           
                <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
               <select name="lapse_in_coverage_percentage" class="select-3 span12 mkread efc" id="lcc">
                  <option value="0">0</option>
                    <option value="5">5%</option>    
                     <option value="10">10%</option>         
                </select>
              </span>
              <span class="span3">
                <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
               &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp" type="text" name="lapse_in_coverage_premium" placeholder="" readonly>
               
              </span>
            </div>
             <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                 <span class="span4">
                <h5 class="heading pull-left heading-4" style="text-align:right">New venture</h5>&nbsp;
                 <select name="new_venture_surcharge" class="select-3 span6 efl">
                  <option value="No">No</option>
                   <option value="Yes">Yes</option>             
                </select>
               
              </span>
                 <span style="
        padding: 0px 0px 5px 14px;
        display: block;
        float: left;
    ">@</span> 
               <span class="span2">           
                <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                <select name="new_venture_percentage" class="select-3 span12 mkread efc" id="nvc">
                  <option value="0">0</option>
                    <option value="5">5%</option>    
                     <option value="10">10%</option>         
                </select>
               
              </span>
              <span class="span3">
                <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
               &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp" type="text" name="new_venture_premium" placeholder="" readonly>
               
              </span>
            </div>
              <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                 <span class="span4">
                <h5 class="heading pull-left heading-4" style="text-align:right">Losess</h5>&nbsp;
                 <select name="losses_surcharge" class="select-3 span6 efl">
                  <option value="None">None</option>
                   <option value="Yes">Yes</option>             
                </select>
               
              </span>
                 <span style="
        padding: 0px 0px 5px 14px;
        display: block;
        float: left;
    ">@</span> 
               <span class="span2">           
                <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="losses_percentage" placeholder="">    -->           	
                <select name="losses_percentage" class="select-3 span12 mkread efc" id="lc">
                   <option value="0">0</option>
               	   <option value="5">5%</option>
                   <option value="10">10%</option>
                   <option value="15">15%</option>
                   <option value="20">20%</option>
                   <option value="25">25%</option>
                   <option value="30">30%</option>
                   <option value="35">35%</option>
                                
                </select>
              </span>
              <span class="span3">
                <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
               &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp" type="text" name="losses_premium" placeholder="" readonly>
               
              </span>
            </div>
             <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                 <span class="span4">
                <h5 class="heading pull-left heading-4" style="text-align:right">Drivers</h5>&nbsp;
                 <select name="drivers_surcharge" class="select-3 span6 efl">
                  <option value="None">None </option>
                   <option value="Yes">Yes</option>                                
                </select>
               
              </span>
                 <span style="
        padding: 0px 0px 5px 14px;
        display: block;
        float: left;
    ">@</span> 
               <span class="span2">           
               <!-- <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                <select name="drivers_surcharge_percentage" class="select-3 span12 mkread efc" id="dsc">
                   <option value="0">0</option>
               	   <option value="5">5%</option>
                   <option value="10">10%</option>
                   <option value="15">15%</option>
                   <option value="20">20%</option>
                   <option value="25">25%</option>
                   <option value="30">30%</option>
                   <option value="35">35%</option>                                
                </select>
               
              </span>
              <span class="span3">
                <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
               &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread efp" type="text" name="driver_surcharge_premium" placeholder="" readonly>
               
              </span>
            </div>
         
            <div class="row-fluid pull-center row-efl" style="text-align:right"> 
                 <span class="span4">
                <h5 class="heading pull-left heading-4" style="text-align:right">Old units</h5>&nbsp;
                 <input class="textinput span6 pull-left textinput-3 textinput-4 ou" type="text" name="old_units" value='0'>
                 
                 <!--<select name="old_units" class="select-3 span6 efl efl2">
                  <option value="No">No</option>
                   <option value="Yes">Yes</option>                                
                </select>-->
               
              </span>
                 <span style="
        padding: 0px 0px 5px 14px;
        display: block;
        float: left;
    ">@</span> 
               <span class="span2">           
                <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
                <select name="old_units_surcharge" class="select-3 span12 mkread oup" id="oup">                  
                   <option value="0">0</option>     
                   <option value="200">$200</option>                           
                </select>
              </span>
              <span class="span3">
                <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
               &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread ofp" type="text" name="old_units_premium" placeholder="" readonly>
               
              </span>
            </div>
            </div>
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            </div>
              <div class="row-fluid">
              <span class="custom_span2">
                <h5 class="heading  heading-4 total_cargo">Total cargo premium</h5>
                <input class="textinput span12 pull-left textinput-3 textinput-4 total_cargo" type="text" name="total_cargo_premium" placeholder="">
               
              </span>
               <span class="span2">
                <h5 class="heading  heading-4">Cargo police fee</h5>
                <input class="textinput span10 pull-left textinput-3 textinput-4" type="text" name="cargo_policy_fee" placeholder="">
               
              </span>
               <span class="span2">
                <h5 class="heading  heading-4">Cargo filing free</h5>
                <input class="textinput span10 pull-left textinput-3 textinput-4 numr_valid" type="text" name="cargo_filing_fee" placeholder="" value="0" maxlength="" onkeypress="return isNumberKey(event)">
               
              </span>
              <span class="span2">
                <h5 class="heading  heading-4"> Cargo SLA tax</h5>
                <input class="textinput span10 pull-left textinput-3 textinput-4" type="text" name="cargo_sla_tax" placeholder="">
               
              </span>
               <span class="span3">
                <h5 class="heading  heading-4">Carrier</h5>
                <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="carrier" placeholder="">-->
                <select name="carrier" class="select-3 span6">
                  <option value="unknown">unknown</option>
                   <option value="unknown">unknown</option>                                
                </select>
              </span>
            </div>
               <div class="row-fluid">
              <span class="span7">
                <h5 class="heading ">Commodities</h5>
                <input class="textinput span15" type="text" placeholder="" name="commodities">
              </span>
               <span class="span4">
                <h5 class="heading  heading-4">Required For FIRM quote</h5>
                <input class="textinput span8 pull-left textinput-3 textinput-4" type="text" name="required_for_firm_quote" placeholder="">
              </span>
            </div>
             <div class="row-fluid">
              <span class="span12">
                <h5 class="heading pull-left">Comments &nbsp;&nbsp;</h5>
                <input class="textinput span9" type="text" placeholder="" name="comments">
              </span>
             
     
            </div>
          		 <button class="btn btn-primary" type="submit" onClick="form_sub()">Submit Sheet</button>
            </div>
               <input type="hidden" name="for_submit" value="1" />
                <input type="hidden" name="action" value="add" />
            </form>
            
            
            </div>
            
<script>


function rofoperation(val , id){
			
		
		if(val == 'other'){
			$('.show_tractor_others').show();
		} else {
			$('.show_tractor_others').hide();
		}	
		
	}
	
	
	
/*  alert(id);
alert(val);  */



</script>
</body>
</html>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>