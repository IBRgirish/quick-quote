<div class="sheet_container">
					<div class="row-fluid">
						<span class="span12">
						  
						  <div class="well pull-center well-3 vell-1 sheet-header">
							<h4 class="heading pull-center heading-3">Auto Liability Rate Sheet</h4>           
						  </div>
						</span>
					</div>
					<div class="well well-3 well sheet-content">
						<?php
							$attributes = array('class' => 'request_ratesheet_liability', 'id' => 'main_form4');
							$liability = isset($liability_rate['0']) ? (array)$liability_rate['0'] : '';
							//print_r($liability);
							$powerUnit = isset($liability['no_of_power_unit']) ? $liability['no_of_power_unit'] : '';	
							$unitPriceA = isset($liability['unit_price_a']) ? $liability['unit_price_a'] :'';
							$quantityA = isset($liability['quantity_a']) ? $liability['quantity_a'] :'';
							$unitPriceB = isset($liability['unit_price_b']) ? ($liability['unit_price_b']) :'';
							$quantityB = isset($liability['quantity_b']) ? $liability['quantity_b'] :'';
							if($segement == 'edit_ratesheet'){
								$broker = isset($liability['broker']) ? $liability['broker'] :$quote->contact_name.' '.$quote->contact_middle_name.' '.$quote->contact_last_name;
								$insured = isset($liability['insured']) ? $liability['insured'] :$quote->insured_fname.' '.$quote->insured_mname.' '.$quote->insured_lname;
								if(str_replace(' ', '', $insured)==''){
										$insured = $quote->insured_dba;
									}
							}else{
								if($segement != 'manualrate'){
									$broker = $quote->contact_name.' '.$quote->contact_middle_name.' '.$quote->contact_last_name; 
									$insured = $quote->insured_fname.' '.$quote->insured_mname.' '.$quote->insured_lname;
									if(str_replace(' ', '', $insured)==''){
										$insured = $quote->insured_dba;
									}
								}else{
									$broker = '';
									$insured = '';
								}
							}
													
						?>   
                         <form class="request_ratesheet" id="main_form4" action="javascript:void(0);" method="post">
						 <input type="hidden" name="producer_email" class="producer_email"  value="<?php echo isset($quote->email) ? $quote->email :''; ?>">
                          <input type="hidden" name="underwriter_ids" class="underwriter_ids" value="">
						<div class="row-fluid">
						  <span class="span6">
							<h5 class="heading  heading-4">Broker</h5>
						   <input type="text" class="span9" name="broker" placeholder="Full Name/ DBA" value="<?= $broker?>">
						  </span>
						  <span class="span6">
							<h5 class="heading ">Insured</h5>
							<input type="text" class="span9" name="insured" placeholder="Full Name/ DBA	" value="<?= $insured?>">
						  </span>
						</div>
						<div class="row-fluid">
						  <span class="custom_span2">
							<h5 class="heading  heading-4 hed-3">Radius of Operation</h5>
							<select name="radius_of_operation" class="select-3 span12 tractor_radius_of_operation" id="tractor_radius_of_operation" onChange="rofoperation(this.value);">
								<option value="0-100 miles" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == '0-100 miles') ? 'selected'  : '' :'';?> <?php echo isset($liability['radius_of_operation']) ? ($liability['radius_of_operation'] == '0-100 miles') ? 'selected' : '' :''; ?> >0-100 miles</option>
									<option value="101-500 miles" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == '101-500 miles') ? 'selected'  : '' :''; ?> <?php echo isset($liability['radius_of_operation']) ? ($liability['radius_of_operation'] == '101-500 miles') ? 'selected' : '' : ''; ?>>101-500 miles</option>           	
									<option value="501+ miles" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == '501+ miles') ? 'selected'  : '' :''; ?> <?php echo isset($liability['radius_of_operation']) ? ($liability['radius_of_operation'] == '501+ miles') ? 'selected' : '' : ''; ?>>501+ miles</option>
									<option value="other" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == 'other') ? 'selected'  : '' :'';?> <?php echo isset($liability['radius_of_operation']) ? ($liability['radius_of_operation'] == 'other') ? 'selected' : '' : '';?>>Other</option>                        
							</select>
						   
						  </span>
                          <?php $libilitOther = isset($liability['radius_of_operation']) ? ($liability['radius_of_operation'] == 'other') ? 'style="display:block"' : 'style="display:none"' : 'style="display:none"';?>
						   <span class="span1 show_tractor_others" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == 'other') ? 'style="display:block"' : 'style="display:none"' : $libilitOther;  ?> >
							<h5 class="heading  heading-4 hed-3">Others</h5>
                            <?php $libilityOtherValue = isset($liability['other_rop']) ? $liability['other_rop'] : '';?>
						   <input class="textinput span12 pull-left show_tractor_others" type="text" name="claim_venue" value="<?php echo isset($quote_vehicle[0]->radius_of_operation_other) ? $quote_vehicle[0]->radius_of_operation_other  :  $libilityOtherValue;?>" placeholder="" >
						   
						  </span>
						   <span class="span1">
							<h5 class="heading  heading-4 hed-3">State</h5>
							<?php
							$attr = "class='span12 ui-state-valid' onchange='changeCarrier(this.value, &quot;liability&quot;)'";	
												
							$broker_state = isset($quote->insured_state) ? ($quote->insured_state != '') ? $quote->insured_state : 'CA' : ''; 
							
							if($segement=='edit_ratesheet')	{
								$broker_state = isset($liability['state']) ? $liability['state'] : 'CA';	
							}							
							
							?>
							<?php echo get_state_dropdown('state', $broker_state, $attr); ?>
						   
						  </span>
						   <span class="span1">
							<h5 class="heading  heading-4 hed-3">Claim Venue Rate</h5>
							<!--<input class="textinput span11 pull-left textinput-3 textinput-4" type="text" name="claim_venue_rate" placeholder="">-->
						   <select name="claim_venue_rate" class="span12 pull-left textinput-3 textinput-4">
                           		<option value="No" <?php echo isset($liability['claim_venue_rate']) ? (strtolower($liability['claim_venue_rate'])==strtolower('No')) ? 'selected' : '' : ''; ?>>No</option>
                                <option value="Yes" <?php echo isset($liability['claim_venue_rate']) ? (strtolower($liability['claim_venue_rate'])==strtolower('Yes')) ? 'selected' : '' : ''; ?>>Yes</option>
                           </select>
						  </span>
						  <span class="span2">
							<h5 class="heading  heading-4 hed-3">Number of Power Units</h5>
                            
							<input class="textinput span12 pull-left textinput-3 textinput-4 lno_of_power_unit" type="text" name="no_of_power_unit" id="no_of_power_unit" placeholder="" value="<?php echo isset($powerUnitGenerate) ? $powerUnitGenerate : $powerUnit ; ?>">
						   
						  </span>
							<?php 
								$combined_limit = 0;$combined_ded = 0;
								if(isset($quote_vehicle)){
									foreach($quote_vehicle as $vehicle) { 
										/*$combined_limit = $combined_limit + getAmount($vehicle->liability,1);
										$combined_ded = $combined_ded + getAmount($vehicle->liability_ded);*/
										$combined_limit = $combined_limit + $vehicle->liability;
										$combined_ded = $combined_ded +$vehicle->liability_ded;
									}
								}else{
									$combined_limit = isset($liability['combined_single_limit']) ? $liability['combined_single_limit'] :'';
									$combined_ded = isset($liability['deductible']) ? $liability['deductible'] : '';
								}
							?>
						   <span class="span2">
							<h5 class="heading  heading-4 hed-3">Combined Single Limit</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 price_format" type="text" name="combined_single_limit" placeholder="" value="<?php echo $combined_limit; ?>">
						   
						  </span>
						   <span class="span2">
							<h5 class="heading  heading-4 hed-3">Deductible</h5>
                             <!-- <select class="span12" name="tractor_liability_ded_vh" id="tractor_liability_ded_vh">
                                <option>$1,000</option>
                                <option>$1,500</option>
                                <option>$2,000</option>
                                <option>$2,500</option>
                            </select>-->
							<input class="textinput span12 pull-left textinput-3 textinput-4 price_format" value="<?= $combined_ded; ?>" type="text" name="deductible" placeholder="">    
                         </span>         
						</div>
						
						<div class="row-fluid">
						  <span class="span3 int_val">
							<h5 class="heading pull-left heading-4">Unit price(a) &nbsp;</h5>
							<input class="textinput span6 pull-left textinput-3 textinput-4 unit_price numr_valid price_format" type="text" name="unit_price_a" placeholder="" value="<?= $unitPriceA; ?>">
						   
						  </span>
						   <span class="span3">
							<h5 class="heading pull-left heading-4">Quantity&nbsp;</h5>
							<input class="textinput span6 pull-left textinput-3 textinput-4 quantity numr_valid" type="text" name="quantity_a" placeholder="" value="<?= $quantityA; ?>">
						   
						  </span>
						  <span class="span3 int_val">
							<h5 class="heading pull-left heading-4">Unit price(b)&nbsp;</h5>
							<input class="textinput span6 pull-left textinput-3 textinput-4 unit_price2 numr_valid price_format" type="text" name="unit_price_b" placeholder="" value="<?= $unitPriceB; ?>">
						   
						  </span>
						   <span class="span3">
							<h5 class="heading pull-left heading-4">Quantity&nbsp;</h5>
							<input class="textinput span6 pull-left textinput-3 textinput-4 quantity2 numr_valid" type="text" name="quantity_b" placeholder="" value="<?= $quantityB; ?>">
						   
						  </span>                  
						</div> 
						<div class="center">
						<div class="row-fluid pull-center row-lefl" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;width:300px;
				width: 297px;">New Venture</h5>&nbsp;
							 <select name="new_venture_surcharge" class="select-3 span4 lefl">
							  <option value="No" <?php if(isset($liability['new_venture_surcharge'])){ if(($liability['new_venture_surcharge'] == 'No') || ($liability['new_venture_surcharge'] == '0.00' )) echo 'selected';} ?>>No</option>
							  <option value="Yes" <?php if(isset($liability['new_venture_surcharge'])){  if(($liability['new_venture_surcharge'] == 'Yes') || ($liability['new_venture_surcharge'] == '0.00' )) echo 'selected';} ?>>Yes</option>              
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="new_venture_cost" class="select-3 span12 mkread1 lnvc" <?php if(isset($liability['new_venture_surcharge'])){ if(($liability['new_venture_surcharge'] == 'No') || ($liability['new_venture_surcharge'] == '0.00' ) || $liability['new_venture_surcharge'] == '') echo 'disabled';}else{ echo 'disabled'; } ?> >
							  <option value="0" <?php if(isset($liability['new_venture_cost'])){ if(($liability['new_venture_cost'] == 'No') || ($liability['new_venture_cost'] == '0.00' )) echo 'selected'; }?>>0</option>	
							  <option value="5" <?php if(isset($liability['new_venture_cost'])){if(($liability['new_venture_cost'] == '5') || ($liability['new_venture_cost'] == '5.00' )) echo 'selected'; }?>>5%</option>
							  <option value="10" <?php if(isset($liability['new_venture_cost'])){if(($liability['new_venture_cost'] == '10') || ($liability['new_venture_cost'] == '10.00' )) echo 'selected';} ?>>10%</option>            
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  lnvp price_format_decimal" type="text" name="new_venture_premium" placeholder="" readonly value="<?php echo isset($liability['new_venture_premium']) ? $liability['new_venture_premium'] : ''; ?>" >
						   
						  </span>
						</div>
						<div class="row-fluid pull-center row-lefl" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">Lapsed in coverage</h5>&nbsp;
							 <select name="lapse_in_coverage_surcharge" class="select-3 span4 lefl">
							  <option value="No" <?php if(isset($liability['lapse_in_coverage_surcharge'])){ if(($liability['lapse_in_coverage_surcharge'] == 'No') || ($liability['lapse_in_coverage_surcharge'] == '0.00' )) echo 'selected';} ?>>No</option>
							  <option value="Yes" <?php if(isset($liability['lapse_in_coverage_surcharge'])){ if(($liability['lapse_in_coverage_surcharge'] == 'Yes') || ($liability['lapse_in_coverage_surcharge'] == '0.00' )) echo 'selected';} ?>>Yes</option>              
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="lapse_in_coverage_percentage" class="select-3 span12 mkread1 lnvc" <?php if(isset($liability['lapse_in_coverage_surcharge'])){ if(($liability['lapse_in_coverage_surcharge'] == 'No') || ($liability['lapse_in_coverage_surcharge'] == '0.00') || ($liability['lapse_in_coverage_surcharge'] == '')) echo 'disabled'; }else{ echo 'disabled';}?>>
							   <option value="0" <?php if(isset($liability['lapse_in_coverage_percentage'])){ if(($liability['lapse_in_coverage_percentage'] == 'No') || ($liability['lapse_in_coverage_percentage'] == '0.00' )) echo 'selected'; }?>>0</option>	
							  <option value="5" <?php if(isset($liability['lapse_in_coverage_percentage'])){ if(($liability['lapse_in_coverage_percentage'] == '5') || ($liability['lapse_in_coverage_percentage'] == '5.00' )) echo 'selected';} ?>>5%</option>
							  <option value="10" <?php if(isset($liability['lapse_in_coverage_percentage'])){ if(($liability['lapse_in_coverage_percentage'] == '10') || ($liability['lapse_in_coverage_percentage'] == '10.00' )) echo 'selected'; }?>>10%</option>             
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  lnvp price_format_decimal" type="text" name="lapse_in_coverage_premium" placeholder="" readonly value="<?php echo isset($liability['lapse_in_coverage_premium']) ? $liability['lapse_in_coverage_premium'] : ''; ?>">
						   
						  </span>
						  
						</div>
						<div class="row-fluid pull-center row-lefl" style="text-align:right;"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">Auto hauler</h5>&nbsp;
							 <select name="auto_hauler_surcharge" class="select-3 span4 lefl" >
							  <option value="No" <?php if(isset($liability['auto_hauler_surcharge'])){ if(($liability['auto_hauler_surcharge']=='No') || ($liability['auto_hauler_surcharge'] == '0.00' )) echo 'selected';} ?>>No</option>
							  <option value="Yes" <?php if(isset($liability['auto_hauler_surcharge'])){ if(($liability['auto_hauler_surcharge']=='Yes') || ($liability['auto_hauler_surcharge'] == '0.00' )) echo 'selected';} ?>>Yes</option>             
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
						   <select name="auto_hauler_cost" class="select-3 span12 mkread1 lnvc" <?php if(isset($liability['auto_hauler_surcharge'])){ if(($liability['auto_hauler_surcharge'] == 'No') || ($liability['auto_hauler_surcharge'] == '0.00') || ($liability['auto_hauler_surcharge'] == '')) echo 'disabled';}else{ echo 'disabled';} ?>>
							   <option value="0" <?php if(isset($liability['auto_hauler_cost'])){ if(($liability['auto_hauler_cost'] == 'No') || ($liability['lapse_in_coverage_percentage'] == '0.00' )) echo 'selected'; }?>>0</option>	
							  <option value="5" <?php if(isset($liability['auto_hauler_cost'])){ if(($liability['auto_hauler_cost'] == '5') || ($liability['lapse_in_coverage_percentage'] == '5.00' )) echo 'selected'; }?>>5%</option>             
							</select>
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  lnvp price_format_decimal" type="text" name="auto_hauler_premium" placeholder="" readonly value="<?php echo isset($liability['auto_hauler_premium']) ? $liability['auto_hauler_premium'] : ''; ?>">
						   
						  </span>
						</div>
						  <div class="row-fluid pull-center row-efl3" style="text-align:right"> 
							 <span class="span6">
								<h5 class="heading pull-left heading-4" style="text-align:right;width: 297px;">UM coverages</h5>&nbsp;
								 <select name="um_coverage_surcharge" class="select-3 span4 efl3 ">
								  <option value="Rejected" <?php if(isset($liability['um_coverage_surcharge'])){ if(($liability['um_coverage_surcharge'] == 'Rejected') || ($liability['um_coverage_surcharge'] == '0.00' )) echo 'selected';} ?> >Rejected</option>
								  <option value="15/30" <?php if(isset($liability['um_coverage_surcharge'])){ if(($liability['um_coverage_surcharge'] == '15/30')) echo 'selected';} ?>> 15/30  </option>   
								   <option value="30/60" <?php if(isset($liability['um_coverage_surcharge'])){ if(($liability['um_coverage_surcharge'] == '30/60')) echo 'selected';} ?>> 30/60 </option>        
								</select>
						   
							</span>
						   <span style="padding: 0px 0px 5px 14px;display: block;float: left;">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="um_coverage_cost" class="select-3 span12 mkread1 nvc2 umc" <?php if(isset($liability['um_coverage_surcharge'])){ if(($liability['um_coverage_surcharge'] == 'Rejected') || ($liability['um_coverage_surcharge'] == '0.00' ) || ($liability['um_coverage_surcharge'] == '' )) echo 'disabled'; }else{ echo 'disabled';}?>>
							   <option value="0" <?php if(isset($liability['um_coverage_cost'])){ if(($liability['um_coverage_cost'] == '0') || ($liability['um_coverage_cost'] == '0.00' )) echo 'selected';} ?> > 0 </option>
							  <option value="50" <?php if(isset($liability['um_coverage_cost'])){ if(($liability['um_coverage_cost'] == '50') || ($liability['um_coverage_cost'] == '50.00' )) echo 'selected';} ?> > $50 </option>	
							  <option value="80" <?php if(isset($liability['um_coverage_cost'])){ if(($liability['um_coverage_cost'] == '80') || ($liability['um_coverage_cost'] == '80.00' )) echo 'selected'; }?> > $80 </option>                         
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right;">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  ump price_format" type="text" name="um_coverage_premium" placeholder="" readonly value="<?php echo isset($liability['um_coverage_premium']) ? intval($liability['um_coverage_premium']) : ''; ?>">
						   
						  </span>
						</div>
						 <div class="row-fluid pull-center row-efl3" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">PIP coverages</h5>&nbsp;
							 <select name="pip_coverage_surcharge" class="select-3 span4 efl3 ">
							   <option value="Rejected" <?php if(isset($liability['pip_coverage_surcharge'])){ if(($liability['pip_coverage_surcharge'] == 'Rejected')) echo 'selected'; }?>>Rejected</option>
							  <option value="2500" <?php if(isset($liability['pip_coverage_surcharge'])){ if(($liability['pip_coverage_surcharge'] == '2500') || ($liability['pip_coverage_surcharge'] == '2500.00' )) echo 'selected';} ?>>$2,500</option>                            
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="pip_coverage_cost" class="select-3 span12 mkread1  nvc2 umc" <?php if(isset($liability['pip_coverage_surcharge'])){ if(($liability['pip_coverage_surcharge'] == 'Rejected') || ($liability['pip_coverage_surcharge'] == '0.00' ) || ($liability['pip_coverage_surcharge'] == '' )) echo 'disabled'; }else{ echo 'disabled';}?>>
							  <option value="0" <?php if(isset($liability['pip_coverage_cost'])){ if(($liability['pip_coverage_cost'] == '0') || ($liability['pip_coverage_cost'] == '0.00' )) echo 'selected';} ?>>0</option>
							  <option value="750" <?php if(isset($liability['pip_coverage_cost'])){ if(($liability['pip_coverage_cost'] == '750') ) echo 'selected';} ?>>$750</option>                               
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  ump price_format" type="text" name="pip_coverage_premium" placeholder="" readonly value="<?php echo isset($liability['pip_coverage_premium']) ?  intval($liability['pip_coverage_premium']) : ''; ?>">
						   
						  </span>
						</div>
						  <div class="row-fluid pull-center row-lefl" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">Losess</h5>&nbsp;
							 <select name="losses_surcharge" class="select-3 span4 lefl">
							  <option value="None" <?php if(isset($liability['losses_surcharge'])){ if(($liability['losses_surcharge'] == 'None') || ($liability['losses_surcharge'] == '0.00' )) echo 'selected';} ?>>None</option>
							  <option value="Yes" <?php if(isset($liability['losses_surcharge'])){ if(($liability['losses_surcharge'] == 'Yes') || ($liability['losses_surcharge'] == '0.00' )) echo 'selected';} ?>>Yes</option>            
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="losses_percentage" class="select-3 span12 mkread1 lnvc" <?php if(isset($liability['losses_surcharge'])){ if(($liability['losses_surcharge'] == 'None') || ($liability['losses_surcharge'] == '0.00' ) || ($liability['losses_surcharge'] == '' )) echo 'disabled';}else{ echo 'disabled'; } ?>>
							  <option value="0" <?php if(isset($liability['losses_percentage'])){ if(($liability['losses_percentage'] == 'None') || ($liability['losses_percentage'] == '0.00')) echo 'selected';} ?>>0</option>
							   <option value="5" <?php if(isset($liability['losses_percentage'])){ if(($liability['losses_percentage'] == '5') || ($liability['losses_percentage'] == '5.00')) echo 'selected';} ?>>5%</option>
							   <option value="10" <?php if(isset($liability['losses_percentage'])){ if(($liability['losses_percentage'] == '10') || ($liability['losses_percentage'] == '10.00')) echo 'selected';} ?>>10%</option>
							   <option value="15" <?php if(isset($liability['losses_percentage'])){ if(($liability['losses_percentage'] == '15') || ($liability['losses_percentage'] == '15.00')) echo 'selected'; }?>>15%</option>
							   <option value="20" <?php if(isset($liability['losses_percentage'])){ if(($liability['losses_percentage'] == '20') || ($liability['losses_percentage'] == '20.00')) echo 'selected';} ?>>20%</option>
							   <option value="25" <?php if(isset($liability['losses_percentage'])){ if(($liability['losses_percentage'] == '25') || ($liability['losses_percentage'] == '25.00')) echo 'selected';} ?>>25%</option>
							   <option value="30" <?php if(isset($liability['losses_percentage'])){ if(($liability['losses_percentage'] == '30') || ($liability['losses_percentage'] == '30.00')) echo 'selected';} ?>>30%</option>             
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right;text-align: right;">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  lnvp price_format_decimal" type="text" name="losses_premium" placeholder="" readonly value="<?php echo isset($liability['losses_premium']) ? $liability['losses_premium'] : ''; ?>">
						   
						  </span>
						</div>
						 <div class="row-fluid pull-center row-lefl" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">Misc.surcharges</h5>&nbsp;
							 <select name="miscellaneos_surcharge" class="select-3 span4 lefl">
							  <option value="None"  <?php if(isset($liability['miscellaneos_surcharge'])){ if(($liability['miscellaneos_surcharge'] == 'None') || ($liability['miscellaneos_surcharge'] == '0.00' )) echo 'selected'; }?> >None</option>
							  <option value="Yes"  <?php if(isset($liability['miscellaneos_surcharge'])){ if(($liability['miscellaneos_surcharge'] == 'Yes')) echo 'selected';} ?>>Yes</option>               
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="miscellaneos_surcharge_cost" class="select-3 span12 mkread1 lnvc" <?php if(isset($liability['miscellaneos_surcharge'])){ if(($liability['miscellaneos_surcharge'] == 'None') || ($liability['miscellaneos_surcharge'] == '0.00' ) || ($liability['miscellaneos_surcharge'] == '' )) echo 'disabled';} else{ echo 'disabled';}?>>
							  <option value="0" <?php if(isset($liability['miscellaneos_surcharge_cost'])){if(($liability['miscellaneos_surcharge_cost'] == 'None') || ($liability['miscellaneos_surcharge_cost'] == '0.00')) echo 'selected';} ?>>0</option>
							   <option value="5" <?php if(isset($liability['miscellaneos_surcharge_cost'])){if(($liability['miscellaneos_surcharge_cost'] == '5') || ($liability['miscellaneos_surcharge_cost'] == '5.00')) echo 'selected'; }?>>5%</option>
							   <option value="10" <?php if(isset($liability['miscellaneos_surcharge_cost'])){if(($liability['miscellaneos_surcharge_cost'] == '10') || ($liability['miscellaneos_surcharge_cost'] == '10.00')) echo 'selected'; }?>>10%</option>
							   <option value="15" <?php if(isset($liability['miscellaneos_surcharge_cost'])){ if(($liability['miscellaneos_surcharge_cost'] == '15') || ($liability['miscellaneos_surcharge_cost'] == '15.00')) echo 'selected'; }?>>15%</option>
							   <option value="20" <?php if(isset($liability['miscellaneos_surcharge_cost'])){ if(($liability['miscellaneos_surcharge_cost'] == '20') || ($liability['miscellaneos_surcharge_cost'] == '20.00')) echo 'selected'; }?>>20%</option>
							   <option value="25" <?php if(isset($liability['miscellaneos_surcharge_cost'])){ if(($liability['miscellaneos_surcharge_cost'] == '25') || ($liability['miscellaneos_surcharge_cost'] == '25.00')) echo 'selected';} ?>>25%</option>
							   <option value="30" <?php if(isset($liability['miscellaneos_surcharge_cost'])){ if(($liability['miscellaneos_surcharge_cost'] == '30') || ($liability['miscellaneos_surcharge_cost'] == '30.00')) echo 'selected';} ?>>30%</option>        
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  lnvp price_format_decimal" type="text" name="miscellaneos_surcharge_premium" placeholder="" readonly value="<?php echo isset($liability['miscellaneos_surcharge_premium']) ? $liability['miscellaneos_surcharge_premium'] : ''; ?>">
						   
						  </span>
						</div>
				  <div class="row-fluid pull-center row-lefl" style="text-align:left"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;width: 297px;">Driver surcharges (unit price a) </h5>&nbsp;
							 <select name="drivers_surcharge_a" class="select-3 span4 pull-right ldsa" >
							   <option value="None"  <?php if(isset($liability['drivers_surcharge_a'])){ if(($liability['drivers_surcharge_a'] == 'None') || ($liability['drivers_surcharge_a'] == '0.00' )) echo 'selected'; }?> >None</option>
							   <option value="Yes"  <?php if(isset($liability['drivers_surcharge_a'])){ if(($liability['drivers_surcharge_a'] == 'Yes')) echo 'selected';} ?> >Yes</option>                            
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="drivers_surcharge_a_percentage" class="select-3 span12 mkread1 lnvc" <?php if(isset($liability['drivers_surcharge_a'])){ if(($liability['drivers_surcharge_a'] == 'None') || ($liability['losses_surcharge'] == '0.00') || ($liability['losses_surcharge'] == '')) echo 'disabled';}else{ echo 'disabled';} ?>>
							 <option value="0" <?php if(isset($liability['drivers_surcharge_a_percentage'])){ if(($liability['drivers_surcharge_a_percentage'] == 'None') || ($liability['drivers_surcharge_a_percentage'] == '0.00')) echo 'selected'; }?>>0</option>
							   <option value="5" <?php if(isset($liability['drivers_surcharge_a_percentage'])){ if(($liability['drivers_surcharge_a_percentage'] == '5') || ($liability['drivers_surcharge_a_percentage'] == '5.00')) echo 'selected';} ?>>5%</option>
							   <option value="10" <?php if(isset($liability['drivers_surcharge_a_percentage'])){ if(($liability['drivers_surcharge_a_percentage'] == '10') || ($liability['drivers_surcharge_a_percentage'] == '10.00')) echo 'selected'; }?>>10%</option>
							   <option value="15" <?php if(isset($liability['drivers_surcharge_a_percentage'])){ if(($liability['drivers_surcharge_a_percentage'] == '15') || ($liability['drivers_surcharge_a_percentage'] == '15.00')) echo 'selected'; }?>>15%</option>
							   <option value="20" <?php if(isset($liability['drivers_surcharge_a_percentage'])){ if(($liability['drivers_surcharge_a_percentage'] == '20') || ($liability['drivers_surcharge_a_percentage'] == '20.00')) echo 'selected'; }?>>20%</option>
							   <option value="25" <?php if(isset($liability['drivers_surcharge_a_percentage'])){ if(($liability['drivers_surcharge_a_percentage'] == '25') || ($liability['drivers_surcharge_a_percentage'] == '25.00')) echo 'selected';} ?>>25%</option>
							   <option value="30" <?php if(isset($liability['drivers_surcharge_a_percentage'])){if(($liability['drivers_surcharge_a_percentage'] == '30') || ($liability['drivers_surcharge_a_percentage'] == '30.00')) echo 'selected';} ?>>30%</option>
							    <option value="35" <?php if(isset($liability['drivers_surcharge_a_percentage'])){ if(($liability['drivers_surcharge_a_percentage'] == '35') || ($liability['drivers_surcharge_a_percentage'] == '35.00')) echo 'selected'; }?>>35%</option>          
							</select>
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-right textinput-3 textinput-4  lnvp price_format_decimal" type="text" name="drivers_surcharge_a_premium" placeholder="" readonly value="<?php echo isset($liability['drivers_surcharge_a_premium']) ? $liability['drivers_surcharge_a_premium'] : ''; ?>">
						   
						  </span>
						</div>
						  <div class="row-fluid pull-center row-lefl" style="text-align:left"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;width: 297px;">Driver surcharges (unit price b) </h5>&nbsp;
							 <select name="drivers_surcharge_b" class="select-3 span4 pull-right ldsa">
							   <option value="None"  <?php if(isset($liability['drivers_surcharge_b'])){ if(($liability['drivers_surcharge_b'] == 'None') || ($liability['drivers_surcharge_b'] == '0.00' )) echo 'selected';} ?> >None</option>
							  <option value="Yes"  <?php if(isset($liability['drivers_surcharge_b'])){ if(($liability['drivers_surcharge_b'] == 'Yes')) echo 'selected'; }?> >Yes</option>                            
							</select>
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-right textinput-3 textinput-4" type="text" name="" placeholder="">-->
						   <select name="drivers_surcharge_b_percentage" class="select-3 span12 mkread1 lnvc" <?php if(isset($liability['drivers_surcharge_b'])){ if(($liability['drivers_surcharge_b'] == 'None') || ($liability['drivers_surcharge_b'] == '0.00') || ($liability['drivers_surcharge_b'] == '')) echo 'disabled'; }else{ }?>>
							 <option value="0" <?php if(isset($liability['drivers_surcharge_b_percentage'])){ if(($liability['drivers_surcharge_b_percentage'] == 'None') || ($liability['drivers_surcharge_b_percentage'] == '0.00')) echo 'selected';} ?>>0</option>
							   <option value="5" <?php if(isset($liability['drivers_surcharge_b_percentage'])){ if((str_replace('-', '', $liability['drivers_surcharge_b_percentage']) == '5') || ($liability['drivers_surcharge_b_percentage'] == '5.00')) echo 'selected'; }?>>5%</option>
							   <option value="10" <?php if(isset($liability['drivers_surcharge_b_percentage'])){ if((str_replace('-', '', $liability['drivers_surcharge_b_percentage']) == '10') || ($liability['drivers_surcharge_b_percentage'] == '10.00')) echo 'selected'; }?>>10%</option>
							   <option value="15" <?php if(isset($liability['drivers_surcharge_b_percentage'])){ if((str_replace('-', '', $liability['drivers_surcharge_b_percentage']) == '15') || ($liability['drivers_surcharge_b_percentage'] == '15.00')) echo 'selected';} ?>>15%</option>
							   <option value="20" <?php if(isset($liability['drivers_surcharge_b_percentage'])){ if((str_replace('-', '', $liability['drivers_surcharge_b_percentage']) == '20') || ($liability['drivers_surcharge_b_percentage'] == '20.00')) echo 'selected'; }?>>20%</option>
							   <option value="25" <?php if(isset($liability['drivers_surcharge_b_percentage'])){ if((str_replace('-', '', $liability['drivers_surcharge_b_percentage']) == '25') || ($liability['drivers_surcharge_b_percentage'] == '25.00')) echo 'selected';} ?>>25%</option>
							   <option value="30" <?php if(isset($liability['drivers_surcharge_b_percentage'])){ if((str_replace('-', '', $liability['drivers_surcharge_b_percentage']) == '30') || ($liability['drivers_surcharge_b_percentage'] == '30.00')) echo 'selected';} ?>>30%</option>
							    <option value="35" <?php if(isset($liability['drivers_surcharge_b_percentage'])){ if((str_replace('-', '', $liability['drivers_surcharge_b_percentage']) == '35') || ($liability['drivers_surcharge_b_percentage'] == '35.00')) echo 'selected';} ?>>35%</option>    
							</select>
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-right textinput-3 textinput-4  lnvp price_format_decimal" type="text" name="drivers_surcharge_b_premium" placeholder="" readonly value="<?php echo isset($liability['drivers_surcharge_b_premium']) ? $liability['drivers_surcharge_b_premium']: ''; ?>">
						   
						  </span>
						</div>
						 <div class="row-fluid pull-center row-lefl row-efl1" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;
				width: 297px;">Extra trailer quantity</h5>&nbsp;
								<input class="textinput span4 pull-left textinput-3 textinput-4 numr_valid efl1 etq " type="text" name="extra_trailer_quantity" placeholder="" value="<?php   echo isset($liability['extra_trailer_quantity']) ? $liability['extra_trailer_quantity'] : 0; ?>">
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<select name="extra_trailer_cost" class="select-3 span12 mkread1 etc" <?php echo isset($liability['extra_trailer_quantity']) ? ($liability['extra_trailer_quantity']=='0'||$liability['extra_trailer_quantity']=='0.00'||$liability['extra_trailer_quantity']=='$ 0') ? 'disabled' : '' : 'disabled'; ?>>
							  <option value="420" <?php echo isset($liability['extra_trailer_cost']) ? (intval($liability['extra_trailer_cost'])=='420') ? 'selected' : '' :'';?>>$420 </option>  
							  <option value="456" <?php echo isset($liability['extra_trailer_cost']) ? (intval($liability['extra_trailer_cost'])=='456') ? 'selected' : '' :'';?>>$456 </option>
							  <option value="540" <?php echo isset($liability['extra_trailer_cost']) ? (intval($liability['extra_trailer_cost'])=='540') ? 'selected' : '' :'';?>>$540 </option>    
							</select>
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-right textinput-3 textinput-4  nvp2 etp price_format" type="text" name="extra_trailer_premium" placeholder="" readonly value="<?php echo isset($liability['extra_trailer_premium']) ? $liability['extra_trailer_premium'] : ''; ?>">
						   
						  </span>
						</div>
						<div class="row-fluid pull-center row-lefl row-efl1" style="text-align:left"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;width:297px;">15 year tractor/tralier quantity</h5>&nbsp;
								<input class="textinput span4 pull-right textinput-3 textinput-4 numr_valid efl1 etq " type="text" name="tractor/trailer_quantity" placeholder="" value="<?php echo isset($liability['tractor/trailer_quantity']) ? $liability['tractor/trailer_quantity'] : 0; ?>">
						   
						  </span>
						   <span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">           
							<input class="textinput span12 pull-left textinput-3 textinput-4 mkread1 lnvc etc price_format" type="text" name="tractor/trailer_surcharge_cost" placeholder="" value="<?php echo isset($liability['tractor/trailer_surcharge_cost']) ? $liability['tractor/trailer_surcharge_cost'] : 200; ?>" <?php echo isset($liability['tractor/trailer_quantity']) ? ($liability['tractor/trailer_quantity']=='0'||$liability['tractor/trailer_quantity']=='0.00'||$liability['tractor/trailer_quantity']=='$ 0') ? 'disabled' : '' : 'disabled'; ?>>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-right textinput-3 textinput-4 nvp2 etp price_format " type="text" name="tractor/trailer_surcharge_premium" placeholder="" readonly value="<?php echo isset($liability['tractor/trailer_surcharge_premium']) ? intval($liability['tractor/trailer_surcharge_premium']) : ''; ?>">
						   
						  </span>
						</div>
						 <div class="row-fluid pull-center row-lefl" style="text-align:right"> 
							 <span class="span6">
							<h5 class="heading pull-left heading-4" style="text-align:right;width:297px;">Discount</h5>&nbsp;
							 <!--<select name="" class="select-3 span4">
							  <option value="">None</option>
											
							</select>-->
							<span class="span2 pull-right">           
							 <select class=" span12 pull-left numr_valid disc1" name="discount">
                            	<option <?php echo isset($liability['discount']) ? ($liability['discount']=='No') ? 'selected' : '' : ''; ?>>No</option>
                                <option <?php echo isset($liability['discount']) ? ($liability['discount']=='Yes') ? 'selected' : '' : ''; ?>>Yes</option>
                            </select>
						  </span>
						   
						  </span>
							<span style="
					padding: 0px 0px 5px 14px;
					display: block;
					float: left;
				">@</span> 
						   <span class="span2">                        
							<select name="discount_percentage" class="select-3 span12 mkread1 disc2" <?php echo isset($liability['discount']) ? ($liability['discount']=='Yes') ? '' : 'disabled' : 'disabled'; ?>>
							   <option value="0" <?php echo isset($liability['discount_percentage']) ? ($liability['discount_percentage']=='0') ? 'selected' : '' : '';?>>0</option>	
							  <option value="-5" <?php echo isset($liability['discount_percentage']) ? ($liability['discount_percentage']=='-5') ? 'selected' : '' : '';?>>-5%</option> 
							   <option value="-10" <?php echo isset($liability['discount_percentage']) ? ($liability['discount_percentage']=='-10') ? 'selected' : '' : '';?>>-10%</option> 
								<option value="-15" <?php echo isset($liability['discount_percentage']) ? ($liability['discount_percentage']=='-15') ? 'selected' : '' : '';?>>-15%</option> 
								 <option value="-20" <?php echo isset($liability['discount_percentage']) ? ($liability['discount_percentage']=='-20') ? 'selected' : '' : '';?>>-20%</option> 
								  <option value="-25" <?php echo isset($liability['discount_percentage']) ? ($liability['discount_percentage']=='-25') ? 'selected' : '' : '';?>>-25%</option> 
								   <option value="-30" <?php echo isset($liability['discount_percentage']) ? ($liability['discount_percentage']=='-30') ? 'selected' : '' : '';?>>-30%</option>     
							</select>
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 price_format_decimal disc3 " type="text" name="discounted_premium" placeholder="" readonly value="<?php echo isset($liability['discounted_premium']) ? $liability['discounted_premium'] : ''; ?>">
						   
						  </span>
						</div>
						</div>
						  <div class="row-fluid">
						  <span class="span4">
							<h5 class="heading  heading-4">Total Liability Premium</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 total_liability_premium price_format_decimal " type="text" name="total_liability_premium" placeholder="" readonly value="<?php echo isset($liability['total_liability_premium']) ? $liability['total_liability_premium'] : ''; ?>">
						   
						  </span>
						   <span class="custom_span3">
							<h5 class="heading  heading-4">Unit Price breakdown</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 unit_price_breakdown price_format_decimal" type="text" name="unit_price_breakdown" placeholder="" readonly value="<?php echo isset($liability['unit_price_breakdown']) ? $liability['unit_price_breakdown'] :''; ?>">
						   
						  </span>
						   <span class="span2">
							<h5 class="heading  heading-4">Liability police fee</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 numr_valid price_format text-required" type="text" name="liability_policy_fee" id="liability_policy_fee" placeholder="" value="<?php echo isset($liability['liability_policy_fee']) ? intval($liability['liability_policy_fee']) : 200; ?>">
						   
						  </span>
						  <span class="custom_span1">
							<h5 class="heading  heading-4">Filling fee</h5>
							<input class="textinput span12 pull-left textinput-3 textinput-4 numr_valid price_format text-required" type="text" name="liablity_filing_fee" placeholder="" value="<?php echo isset($liability['liablity_filing_fee']) ? intval($liability['liablity_filing_fee']) : 50; ?>">
						   
						  </span>
						   <span class="span2">
							<h5 class="heading  heading-4">Carrier</h5>
							<!--<input class="textinput span9 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							<div class="liability_carrier_drop_down">
										<?php
                                            $attr1 = "id='carrier_cargo3' class='span12 ui-state-valid select-required' onchange='updatePolicyValue(&quot;liability&quot;, this.value)' ";
                                            $broker_state = '';
                                             if($carriers!=''){ $carriers = $carriers; } else { }
											 $broker_state = isset($liability['carrier']) ? $liability['carrier'] : '';
											
                                        ?>
                                        <?php echo form_dropdown('carriers', $carriers, $broker_state, $attr1); ?>
                                    </div>
						  </span>
						</div>
						<div class="row-fluid">
						  <span class="span12">
							<h5 class="heading pull-left">Comments &nbsp;</h5>
							<input class="textinput span9" type="text" placeholder="" name="comments" value="<?php echo isset($liability['comments']) ? $liability['comments'] : ''; ?>">
						  </span>
						 
				 
						</div>
							<div id='loader1' style="display:none"><img src="<?php echo base_url(); ?>images/spinner.gif"/></div><div id="cargo_sheet_message"></div>
							<div id="liability_sheet_message"></div>
							<!--<input type="hidden" class="liability_id" value="" name="liability_id">-->
                                <input type="hidden" name="bundleid" class="bundleid" value="<?php echo isset($bundleid) ? $bundleid : ''; ?>">
                                <button class="btn btn-primary" id="liability_submit" type="submit">Save Draft</button>                            
                                <input type="hidden" name="quote_id" value="<?php echo $quote_id; ?>" id="quote_id2"/>
                                <input type="hidden" name="for_submit2" value="1" />
                                <input type="hidden" name="action" value="<?php echo $action; ?>" />
							</form>
					</div>
				</div>
<script>
changeCarrierL();
function changeCarrierL(){
	var val = $('#main_form4 select[name=state]').val();
	var type = 'liability';
	var selected = '<?php echo $broker_state; ?>';
	var url = '<?php echo base_url('ratesheet'); ?>/get_carrier_by_state';
	changeCarrier1(val, type, url, selected)
}
</script>