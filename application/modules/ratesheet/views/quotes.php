<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/header');
}
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
    //load_underwriter_list();
	
	$(".fancybox-add").fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url('administration/underwriter/underwriter_add') ?>',
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '400',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
});

function load_underwriter_list() {

    $("#underwriterList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo base_url(); ?>administration/quotes/ajax_list_quotes",

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [{
                "sClass": "center"
            },

            //{"sClass": "center"},
            {
                "fnRender": function (oObj) {
                    var a = oObj.aData[0] + ' ' + oObj.aData[1];
                    return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];;
                    return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];;
					
                    if (a == 'Active') {
                        a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Active)';
                    } else {
                        a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Disable)';
                    }
                    return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    // var a = oObj.aData[2];
                    var jobId = oObj.aData[4];
					
					url = "<?php echo base_url('ratesheet/generate_ratesheet');?>";
                    a = '<a href="'+url+'/'+jobId+'">Generate Ratesheet</a>';
                    return (a);

                }

            }
        ]
    });

}

function ajax_status(status,id)
{
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('administration/underwriter/ajax_status'); ?>",
		data:{status:status,id:id},
		success:function(data){
			
			if(data=="done")
			{
				load_underwriter_list(); 
			} 
			
	}});
}

function underwriter_delete(id)
{
	var r=confirm("Are you sure want to delete underwriter ..?");
	if (r==true)
  	{
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('administration/underwriter/ajax_underwriter_delete'); ?>",
		data:{id:id},
		success:function(data){
			
			if(data=="done")
			{
				load_underwriter_list(); 
			} 
			
	}});
	}
}

function edit_underwriter(id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url() ?>administration/underwriter/underwriter_edit/'+id,
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '450',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}
</script>
<script>
function changeStatus(action, id, id1){
	$.ajax({
		url: "<?php echo base_url('administration/quotes/changeStatus')?>",
		type: "post",
		data: "action="+action+"&id="+id,
		beforeSend: function() {
			$('#loader1').show();
		},
		complete: function(){
			$('#loader1').hide();
		},
		success: function(data){
			//alert(id);
			$("#status_"+id1).html('Rejected');
			$("#link_"+id1).hide();
		}
	});
}
</script>
<link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
<script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
<script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
<script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script>
<script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>

<script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>

<script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
<script>
$(document).ready(function(){
	$(".date").mask("99/99/9999");
		/*var date = new Date();
		date.setDate(date.getDate()-0);
				
		 var inputs1 = $('.date').datepicker({
			startDate: date,
			format: 'mm/dd/yyyy',
			autoclose: true
		});  */
		/** Filter Start and End Date **/
		var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		$('#date_f').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_t').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_t').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_f').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		
$('#checkAll').click(function () {    
     $('input:checkbox').prop('checked', this.checked);    
 });	

$("#under").click(function(){
    var checked = $("input:checked").length > 0;
    if (!checked){
        alert("Please check at least one checkbox");
        return false;
    }
});

 
});
</script>
<br>
<h1>Manage Requests</h1>
<?php
$agency_name = $this->session->userdata('member_name'); 
$characters_only = ' pattern="^[a-zA-Z0-9][a-zA-Z0-9-_\.\ \;\,\*\@\&\)\(\-\=\+\:/]{0,50}$"  ';

//placeholder="MM/DD/YYYY" 
//$date_pattern = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" ';

$date_pattern_js = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ';

/*
  $date_pattern = ' placeholder="MM/DD/YYYY" class="date"  pattern="^([0-9]{2,2})/([0-9]{2,2})/([0-9]{4,4})" ';
 */

$email_pattern = 'pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$"';
$number_pattern = ' pattern="[0-9]+" ';
$less_100_limit = ' pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" ';
?>
<div class="table"> <!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> <!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<!--<a class="fancybox-add btn btn-primary" onclick="javascript:void(0)" href="">Add New</a>-->
	<label style="font-size:13px; color:green"><?php echo $this->session->flashdata('success') ?></label>
  <!-- Underwriter List -->
   <div class="pull-right" class="sapn6">
  <!--onchange="sort_btwn_date()"-->
  	<input type="text" class="span2 date pull-left" style="float:left;margin-right: 5px;" name="date_f" id="date_f"  <?= $date_pattern_js; ?> />
    <input type="text" class="span2 date pull-left" style="float:left;margin-right: 5px;" name="date_t" id="date_t"  <?= $date_pattern_js; ?> />
    <input type="button" class="btn btn-primary pull-left" name="filter" value="Filter" onclick="sort_btwn_date()" />
  </div>
  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">Sr.</th>
		
        <th width="10%">Title <input type="checkbox" name="quote_re[]" value="" id="checkAll"></th>
        
        <th width="22%">Name</th>
		
        <th width="22%">Email</th>
		 <th width="22%">Requested On</th>
        <th width="22%">Status</th>
        <th width="22%">Action</th>
      </tr>
    </thead>
    <tbody id="tabe_body">
		<?php ?>
		<?php  $cnt= 0; krsort($quote); foreach($quote as $row)
		{
			$cnt++;
			 $ids[] = $row->quote_id;
				?>
				<tr>
					<td><?php echo $cnt; ?></td>
                    
                    <td><input type="checkbox" name="quote_re[]" value="<?= $row->quote_id; ?>"></td>
					
					<td><?php echo $row->contact_name; ?></td>
					<td><?php echo $row->email; ?></td>
					<td><?php echo date("m/d/Y",strtotime($row->date_added)); ?></td>
					<td id="status_<?= $cnt; ?>"><?php if($row->bundle_status == '') { if($row->perma_reject=='Reject'){ echo 'Rejected';}else{?>Requested<?php }} else { echo $row->bundle_status; } ?></td>
					<td ><?php if($row->bundle_id == 0) { ?><a href="<?php echo base_url('ratesheet/generate_ratesheet');?>/<?php echo $row->quote_id; ?>">Generate Ratesheet</a>
                    <?php
					if($row->perma_reject!='Reject'){				
					?>
					 <a href="javascript:;" onclick="changeStatus('reject', <?php echo $row->quote_id; ?>, <?= $cnt; ?>)" id="link_<?= $cnt; ?>">Reject</a><?php } } else { ?><a href="<?php echo base_url(); ?>administration/quotes/rate_by_quote/<?php echo $row->quote_id; ?>">View</a></a><?php } ?>
                   
                    </td>
				</tr>
				<?php 
		}
		?>
    </tbody>
  </table>
  <span id="export_div">
   <a class="btn btn-primary" target="_top" href="<?php echo base_url();?>administration/quotes/file_xls/<?php echo implode('.', $ids); ?>">Export</a>
  </span>
  <button class="btn btn-primary" id="under">Underwriter</button>
  <!-- Underwriter List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
<script>
function sort_btwn_date(){
	var date_f = $("#date_f").val();
	var date_t = $("#date_t").val();
	if(date_t!='')
	{
		$.ajax({
			url: "<?php echo base_url();?>administration/quotes/sort_date_quote1",
			type: "post",
			data: "date_f="+date_f+"&date_t="+date_t,
			success: function(json){
				//alert(json);
				
				var table_body = $(json).find('.table_data').html();
				var export_link = $(json).find('.export_link').html();
				//alert(table_data);
				var table_data = $(table_body).find('.table_body').html();
				$("#tabe_body").html(table_data);
				$("#export_div").html(export_link);
			}
		});
	}
}
</script>