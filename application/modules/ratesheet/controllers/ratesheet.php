<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ratesheet extends RQ_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcomef
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
 public function __construct()
	{
		parent::__construct();
		$this->load->model('ratesheet_model');
		$this->load->model('quote/quote_model');
		$this->load->model('user/members_model');
		$this->load->model('administration/underwriter_model');
		$this->load->model('administration/catlog_model');
		$this->load->library('pagination');  
	}
	
	
	public function get_cargo_detail($id1='', $name1='',$state='')
	{   
		$post_arr = array();
		$post_arr = $this->input->post();
		
		if($id1==''&&$name1=='')
		{
			$id = $post_arr['id'];
		    $name = !empty($post_arr['name'])?$post_arr['name']:'';
			$type = isset($post_arr['type']);
		    $states= !empty($post_arr['states']) ? $post_arr['states']:'';
		}
		else
		{
			$id = $id1;
			$name = $name1;
		}
		
		if($id!=''&&$id!=0)
		{
			$quote_vehicle = $this->quote_model->get_all_vehicle($id);
			
			
			$carrier_val = array();
			$carrier_val = $this->catlog_model->get_catlog_by_name($name,$states);
			
			$datacar = array();
			
			if(($quote_vehicle[0]->cargo != '') && ($quote_vehicle[0]->pd != ''))
			{
				//$datacar['cargo_policy_fee'] = !empty($carrier_val[0]->pd_cargo_policy_fee)?$carrier_val[0]->pd_cargo_policy_fee:'';
				
			}
			if($quote_vehicle[0]->cargo != '')
			{
				//$datacar['cargo_policy_fee'] = !empty($carrier_val[0]->unique_cargo_policy_fee)?$carrier_val[0]->unique_cargo_policy_fee:'';
			} 
			 if($quote_vehicle[0]->pd != '')
			{
				//$datacar['cargo_policy_fee'] = !empty($carrier_val[0]->unique_pd_policy_fee)?$carrier_val[0]->unique_pd_policy_fee:'';
	
			}
			if(!isset($datacar['cargo_policy_fee'])){
				//$datacar['cargo_policy_fee'] = !empty($carrier_val[0]->unique_cargo_policy_fee)?$carrier_val[0]->unique_cargo_policy_fee:'';
			}
			$datacar['slatax'] = !empty($carrier_val[0]->tax)?$carrier_val[0]->tax:'';
		}
		else
		{
			$carrier_val = $this->catlog_model->get_catlog_by_name($name);
			
			//$datacar['cargo_policy_fee'] = isset($carrier_val[0]->unique_cargo_policy_fee)? $carrier_val[0]->unique_cargo_policy_fee :'';
			$datacar['slatax'] = isset($carrier_val[0]->tax)?$carrier_val[0]->tax:'';
		}
		echo json_encode($datacar);
		
	}
	
	public function get_cargo_detail1($id1='', $name1='')
	{
		$post_arr = array();
		$post_arr = $this->input->post();
		//print_r($post_arr);
		if($id1==''&&$name1=='')
		{
			$id = $post_arr['id'];
			$name = $post_arr['name'];
		}
		else
		{
			$id = $id1;
			$name = $name1;
		}
		
		if($id!='')
		{
			//$quote_vehicle = $this->quote_model->get_all_vehicle($id);
			
			
			
			$carrier_val = array();
			$carrier_val = $this->catlog_model->get_catlog_by_name($name);
			
			$datacar = array();
			
			/*if(($quote_vehicle[0]->cargo != '') && ($quote_vehicle[0]->pd != ''))
			{*/
				$datacar['cargo_policy_fee'] = $carrier_val[0]->pd_cargo_policy_fee;
				
			/*}
			if($quote_vehicle[0]->cargo != '')
			{
				$datacar['cargo_policy_fee'] = $carrier_val[0]->unique_cargo_policy_fee;
			} else if($quote_vehicle[0]->pd != '')
			{
				$datacar['cargo_policy_fee'] = $carrier_val[0]->unique_pd_policy_fee;
	
			}*/
			$datacar['slatax'] = $carrier_val[0]->slatax;
		}
		else
		{
			$carrier_val = $this->catlog_model->get_catlog_by_name($name);
			$datacar['cargo_policy_fee'] = $carrier_val[0]->unique_cargo_policy_fee;
			$datacar['slatax'] = $carrier_val[0]->slatax;
			//$datacar['cargo_policy_fee'] = '';
			//$datacar['slatax'] = '';
		}
		echo json_encode($datacar);
		
	}
	
	public function view_quote_rate(){
		$total = 0;
		$limit = 0;
		
		$data['user'] = 'member';
		$data['object'] = $this;
		$data['quote_id'] = $this->uri->segment(3);
		$data['bundle_status'] = $this->ratesheet_model->get_bundle_status($data['quote_id']);
		
		$config['base_url'] = base_url('administration/quotes');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		$data['total'] = $total;
		$this->pagination->initialize($config);
		
		$this->load->view('view_quote_rate',$data);
	}
	
	/*Ashvin Patel 28/april/2014*/
	public function view_quote_rate1(){
		$total = 0;
		$limit = 0;
			
	  	$data['row'] = new stdClass();
		//$data['row'] = $this->admin_init_elements->set_post_vals($this->input->post());
		$data['row']->id = $this->uri->segment(3);	
		
		$data['quote_id']=$this->quote_model->get_bundle_quote($data['row']->id);
		
		//print_r($data['quote_id']);
		
	 	$quote_id= $data['quote_id'][0]->quote_id;
		//die;
		$data['user'] = 'member';
		$data['object'] = $this;		
		
		$data['bundleid'] = $this->uri->segment(3);
		
	
		//die;
		$data['quote'] = $this->quote_model->get_insured_info($quote_id);
	
		//$data['quote_id'] = $this->uri->segment(3);
		
		
		$data['all_full'] = $this->quote_model->get_quote($quote_id);
		//echo $this->db->last_query();
		//print_r($data['all_full']);
		$data['quote_full'] = $this->quote_model->get_quote_full($quote_id);
		
		//	print_r($data['quote_id']);
	//	die;
		
		//print_r($data['quote_bundle']);
		$data['quote_bundle'] = $this->quote_model->get_quote_bundle($data['row']->id);

		/**Get all vehicle**/
		$data['vehicles'] = $this->quote_model->get_all_vehicle($quote_id); 
		$vehicle_commodities_string = '';
		$vehicle_pull_string = '';
		$trailer_pull_count = 0;
		$vehicle_pull = '';
		if(!empty ($data['vehicles']) && isset($data['vehicles'][0])){ 
			foreach($data['vehicles'] as $single_vehicle){
				//Commodities Haulted
				if (isset($single_vehicle->commodities_haulted) && !empty($single_vehicle->commodities_haulted)) { 
					$vehicle_commodities[] = trim($single_vehicle->commodities_haulted); 
					// Remove Break to get values from all vehicles
					//break;
				}
				
				//Pull
				
				if (isset($single_vehicle->put_on_truck) && !empty($single_vehicle->put_on_truck)) { 
					$vehicle_pull[] = trim($single_vehicle->put_on_truck);  //print_r($vehicle_pull);
					// Remove to get values from all vehicles
					//break;
				}
				
				if  ($single_vehicle->vehicle_type == 'TRAILER'){
					$trailer_pull_count++;
				}
				
			
			}
			//Commodities Variable Set
			if (isset($vehicle_commodities) && !empty($vehicle_commodities)){
				$vehicle_commodities_string = implode(', ',$vehicle_commodities);
			}
			
			//echo $trailer_pull_count;
			
			if($trailer_pull_count){
				if($trailer_pull_count>0){
					$vehicle_pull[] = $trailer_pull_count.' Trailers';
					$vehicle_pull_string = implode(', ',$vehicle_pull);
				}else{
					//Pull Variable Set
					if (isset($vehicle_pull) && !empty($vehicle_pull)){
							$vehicle_pull_string = implode(', ',$vehicle_pull);
						
					}
				}
			}
		}
		
		$data['commodities_vehicles'] = $vehicle_commodities_string;
		$data['vehicle_pull_string'] = $vehicle_pull_string;
		
		

		$data['drivers'] = $this->quote_model->get_all_drivers_rate($quote_id);
		
		/**get losses**/
		$data['losses'] = $this->quote_model->get_losses($quote_id);
		
		/**get all fillings**/
		$data['fillings'] = $this->quote_model->get_all_fillings($quote_id);
		
		//$data['prodemail'] = isset($data['quote_bundle'][0]['producer_email']) ? $data['quote_bundle'][0]['producer_email'] : '';
		$data['prodemail'] = isset($data['quote_bundle'][0]['producer_email']) ? $data['quote_bundle'][0]['producer_email'] : '';
		//$q = $data['quote_bundle'][0];
		$q = isset($data['quote_bundle'][0]) ? $data['quote_bundle'][0] : array('cargo_id'=>'0','pd_id'=>'0','liability_id'=>'0','status'=>'',);
		$data['bundle_status'] = $q['status'];
		//echo $q['cargo_id'];  echo $this->db->last_query();
		$data['cargo_rate'] = $this->quote_model->get_sheet_rate($q['cargo_id'],'cargo');
		$data['excess_cargo_rate'] = $this->quote_model->get_sheet_rate($q['excess_id'],'excess');
		//echo $this->db->last_query(); die;
	//	print_r($data['cargo_rate']);
		$data['pd_rate'] = $this->quote_model->get_sheet_rate($q['pd_id'],'pd');
		$data['liability_rate'] = $this->quote_model->get_sheet_rate($q['liability_id'],'liability');
		
		$data['carriers'] = $this->catlog_model->get_carrier_by_state(isset($data['quote']->insured_state) ? $data['quote']->insured_state : 'CA');
		
		$config['base_url'] = base_url('administration/quotes');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		$data['total'] = $total;
		$this->pagination->initialize($config);
		
		$this->load->view('view_quote_rate',$data);
	}
	/*Ashvin Patel 28/april/2014*/
	public function changeStatus()
	{
			$status = $this->uri->segment(3);
			$id = $this->uri->segment(4);
			$this->ratesheet_model->change_status($status,$id);
			$url = base_url().'list_quote';
			header("Location : ".$url);
	}
	
	public function changeStatus1()
	{
			$status = $this->input->post('status');
			
			$id = $this->input->post('id'); 
			$response = $this->ratesheet_model->change_status($status,$id);
			
		  $agency_email='';
		  $quote_number = $response['quote_number'];
		  
		   $members = $this->session->userdata('member_id');
		   $member_id = (!empty($members) && isset($members)) ? $members : 0 ;
		
		  $agency=$this->ratesheet_model->get_broker_quote($member_id);
		  $agency_name='';
		  // print_r($agency);
		  if(!empty($agency)){
			 // $agency_email="girish@ibrinfotech.com";
			
	      $agency_email= $agency[0]->email;
		
		  
			 foreach($agency as $broker){
							    $broker_name1= isset($broker->first_name) ? $broker->first_name : '';
								$broker_name2= isset($broker->middle_name) ? $broker->middle_name : '';
								$broker_name3=isset($broker->last_name) ? $broker->last_name : '';
								$broker_name4='';
								$broker_name5='';
								if($broker_name2!=0 && $broker_name2!= 'null'){
								
								$broker_name4=$broker_name2;
								
								}
								if($broker_name3!=0 && $broker_name3!= 'null'){
								
								$broker_name5=$broker_name3;
								
								}
								
								
								
								$agency_name=$broker_name1.' '.$broker_name4.' '.$broker_name5;
		   
		 //die;
			 }
		  }else{
			  
			  }
			$config = array(           
            'charset' => 'utf-8',
            'wordwrap' => TRUE,
            'mailtype' => 'html'
       	 );
		 $mail_content ='';
		 //$mail_from = get_mail_from_id();
		 $mail_from = 'support@gcib.net';
		 if($status=='revise'){
		 $mail_content =  $agency_name." is requesting a revision for Quote #".$quote_number.".Please log in to check.";
		
		 }
		 
		  if($status=='deny'){
		 $mail_content = $agency_name." rejected the Quote #".$quote_number.".Please log in to check.";
		
		 }
		 
		 if($status=='accept'){
		  $mail_content =$agency_name." accepted the Quote #".$quote_number.".Please log in to check.";
		 }
		 //print_r($this->session->all_userdata());
		//$data['mail_content'] = "Hi, \n Quote Status is updated by Agency of Quote No. ".$quote_number.", Please login to check.";
	

		//Old Email Address
		$underwriter_mail =  $this->quote_model->get_underwriter_email_address($member_id);
		//print_r($underwriter_mail['email']);
	//$underwriter_mail['email'] = 'mayankg17@gmail.com';
	   $this->load->library('email', $config);
	   $this->email->from($mail_from , 'Gcib Quote Status');
	   $this->email->to($agency_email);
	   $this->email->subject('Quote Status Updated, Quote Number: '.$quote_number);
	   $this->email->message($mail_content);
       $this->email->send();

		echo $response['message'];	
			//$url = base_url().'list_quote';
			//header("Location : ".$url);
	}
	/*public function generate_ratesheet()
	{
		
		$data = array();
		
		$data['row'] = new stdClass();
		//$data['row'] = $this->admin_init_elements->set_post_vals($this->input->post());
		$data['row']->id = $this->uri->segment(3);	
		$data['action'] = 'add';
		$post_action =$data['action'];
		
		$data['quote'] = $this->quote_model->get_quote_full($data['row']->id);
		
		$data['quote_vehicle'] = $this->quote_model->get_all_vehicle($data['row']->id,'');
		$data['truck_vehicle'] = $this->quote_model->get_all_vehicle($data['row']->id,'TRUCK');
		$data['tractor_vehicle'] = $this->quote_model->get_all_vehicle($data['row']->id,'TRACTOR');
		$data['trailer_vehicle'] = $this->quote_model->get_all_vehicle($data['row']->id,'TRAILER');
		
		$data['carriers'] = $this->catlog_model->get_carrier_by_state($data['quote']->insured_state);

		
		if ($this->input->post('for_submit')){
			$post_array = $this->input->post();
			
			$action = ($post_action == 'add')?'inserted':'updated';
			$data['error_message'] = $this->ratesheet_model->insert_ratesheet($post_array,$action);
			if($data['error_message'] == 'Record '.$action.' successfully'){
				$data['row'] = new stdClass();
				$data['row']->id = $this->uri->segment(3);	
				$data['row']->status = 1;
			}
		}
		
		$data['quote_id'] = $this->uri->segment(3);
		if($data['quote_id'] > 0)
		{
			$data['cargo_rate'] = $this->ratesheet_model->get_cargo($data['quote_id']);
			
			$data['pd_rate'] = $this->ratesheet_model->get_pd($data['quote_id']);
			$data['liability_rate'] = $this->ratesheet_model->get_liability($data['quote_id']);
			
			
			//print_r($data['liability_rate']);			
			//echo $data['liability_rate'];
		}

		if($data['row']->id>0){
			$data['action'] = 'update';
			$data['heading'] = 'Edit';
			
		}
		//echo '<pre>';print_r($data);die;
		$this->load->view('ratesheet',$data); 
				
	}
	*/
	public function generate_ratesheet()
	{
		
		$data = array();
		
		$data['row'] = new stdClass();
		//$data['row'] = $this->admin_init_elements->set_post_vals($this->input->post());
		$data['row']->id = $this->uri->segment(3);	
		$data['action'] = 'add';
		$post_action =$data['action'];
		
		$data['quote'] = $this->quote_model->get_quote_full($data['row']->id);
		
		$data['quote_vehicle'] = $this->quote_model->get_all_vehicle($data['row']->id,'');
		
		$data['truck_vehicle'] = $this->quote_model->get_all_vehicle($data['row']->id,'TRUCK');
		$data['tractor_vehicle'] = $this->quote_model->get_all_vehicle($data['row']->id,'TRACTOR');
		$data['trailer_vehicle'] = $this->quote_model->get_all_vehicle($data['row']->id,'TRAILER');
		$data['powerUnitGenerate'] = count($data['truck_vehicle'])+count($data['tractor_vehicle']);
	 
		$data['carriers'] = $this->catlog_model->get_carrier_by_state($data['quote']->insured_state);

		
		if ($this->input->post('for_submit')){
			$post_array = $this->input->post();
			
			$action = ($post_action == 'add')?'inserted':'updated';
			$data['error_message'] = $this->ratesheet_model->insert_ratesheet($post_array,$action);
			if($data['error_message'] == 'Record '.$action.' successfully'){
				$data['row'] = new stdClass();
				$data['row']->id = $this->uri->segment(3);	
				$data['row']->status = 1;
			}
		}
		
		$data['quote_id'] = $this->uri->segment(3);
		if($data['quote_id'] > 0)
		{
			$data['cargo_rate'] = $this->ratesheet_model->get_cargo($data['quote_id']);
			
			$data['pd_rate'] = $this->ratesheet_model->get_pd($data['quote_id']);
			$data['liability_rate'] = $this->ratesheet_model->get_liability($data['quote_id']);
			
			
			//print_r($data['liability_rate']);			
			//echo $data['liability_rate'];
		}

		if($data['row']->id>0){
			$data['action'] = 'update';
			$data['heading'] = 'Edit';
			
		}
		//echo '<pre>';print_r($data);die;
		$this->load->view('admin_ratesheet',$data); 
				
	}
	

	/**By Ashvin patel 9/may/2014**/
	public function get_carrier_by_state(){	
	$state = $this->input->post('state');
	$id = $this->input->post('id');
	$name = $this->input->post('name');
	$type = $this->input->post('type');
	$selected = $this->input->post('selected');
	$data['carriers'] = $this->catlog_model->get_carrier_by_state($state);
	//print_r($data['carriers']);
	if($data['carriers']!=''){ $carriers = $data['carriers'];}else{ $carriers = array(); }
	if($type=='cargo'){
		$carrier="carriers";
		$ele_id = 	'carrier_cargo';
	}elseif($type=='pd') {
		$carrier="carriers";
		$ele_id = 	'carrier_cargo2';
	}elseif($type=='liability') {
		$ele_id = 	'carrier_cargo3';
		$carrier="carriers";
	}elseif($type=='excess_cargo') {
		$ele_id = 	'excess_cargo';
		$carrier="excess_carriers";
	}
	$attr1 = "id='".$ele_id."' class='span12 ui-state-valid select-required' onchange='updatePolicyValue(&quot;".$type."&quot;, this.value,&quot;".$state."&quot;)'";
    $broker_state = $selected;
	//$policy_detail = $this->get_cargo_detail($id, $name);
	//print_r($policy_detail);
	echo form_dropdown($carrier, $carriers,$broker_state, $attr1);
	
	}
	public function manualrate()
	{
		
		$data = array();
		
		$data['row'] = new stdClass();	
			
		$data['quote'] = $this->quote_model->get_insured_info();		
		$data['action'] = 'add';
		$post_action =$data['action'];
		$data['publishers'] = $this->members_model->get_members();
		$data['underwriters'] = $this->members_model->get_underwriters();
		
		$data['carriers'] = isset($data['quote']->insured_state) ? $this->catlog_model->get_carrier_by_state($data['quote']->insured_state) : '';
		if ($this->input->post('for_submit')){
			$post_array = $this->input->post();
			
			$action = ($post_action == 'add')?'inserted':'updated';
			$data['error_message'] = $this->ratesheet_model->insert_ratesheet($post_array,$action);
			if($data['error_message'] == 'Record '.$action.' successfully'){
				$data['row'] = new stdClass();
				$data['row']->id = $this->uri->segment(3);	
				$data['row']->status = 1;
			}
		}
		$data['quote_id'] = $this->uri->segment(3);
		if($data['quote_id'] > 0)
		{
			$data['cargo_rate'] = $this->ratesheet_model->get_cargo($data['quote_id']);
			
			$data['pd_rate'] = $this->ratesheet_model->get_pd($data['quote_id']);
			$data['liability_rate'] = $this->ratesheet_model->get_liability($data['quote_id']);
		}
		$this->load->view('admin_ratesheet',$data); 
//$this->load->view('manualrate',$data); 
	}
	public function edit_ratesheet()
	{
		
		$data = array();
		
		$data['row'] = new stdClass();
		//$data['row'] = $this->admin_init_elements->set_post_vals($this->input->post());
		$data['row']->id = $this->uri->segment(3);	
		$data['action'] = 'add';
		$post_action =$data['action'];
		$data['bundleid'] = $this->uri->segment(3);		
	   
		$data['quote_bundle'] = $this->quote_model->get_quote_bundle($data['row']->id);

		$quote_id = $data['quote_bundle'][0]['quote_id'];
		
		$data['quote'] = $this->quote_model->get_quote_full($quote_id);

	   $data['prodemail'] = $data['quote_bundle'][0]['producer_email'];
		$data['produnderwriters'] = $data['quote_bundle'][0]['underwriters'];
		$q = $data['quote_bundle'][0];
		if($q['cargo_id']==0){
			
			$data['quote'] = $this->quote_model->get_quote_full($quote_id);
			
		}else{
		$data['cargo_rate'] = ($q['cargo_id']>0) ? $this->quote_model->get_sheet_rate($q['cargo_id'],'cargo')  : '';
	
		}

		if($q['pd_id']==0){
			
			$data['quote'] = $this->quote_model->get_quote_full($quote_id);

			}else{
				
		     $data['pd_rate'] = $this->quote_model->get_sheet_rate($q['pd_id'],'pd');
			}
	//	print_r($data['pd_rate']);
	if($q['liability_id']==0){
		$data['quote'] = $this->quote_model->get_quote_full($quote_id);
		}else{
		$data['liability_rate'] = $this->quote_model->get_sheet_rate($q['liability_id'],'liability');
	}
	
	   	if($q['excess_id']==0){
			
			$data['quote'] = $this->quote_model->get_quote_full($quote_id);
			
		}else{
		$data['excess_cargo_rate'] = ($q['excess_id']>0) ? $this->quote_model->get_sheet_rate($q['excess_id'],'excess')  : '';
	
		}
	
		$data['carriers'] = $this->catlog_model->get_carrier_by_state(isset($data['quote']->insured_state) ? $data['quote']->insured_state : 'CA');
		
		$data['underwriters'] = $this->members_model->get_underwriters();
		
		//print_r($data['carriers']);
	    $data['quote_id'] = $q['quote_id'];

		
		$data['truck_vehicle'] = $this->ratesheet_model->get_ratesheet_vehicle($data['row']->id,'TRUCK');
		$data['tractor_vehicle'] = $this->ratesheet_model->get_ratesheet_vehicle($data['row']->id,'TRACTOR');
		$data['trailer_vehicle'] = $this->ratesheet_model->get_ratesheet_vehicle($data['row']->id,'TRAILER');
		
		//print_r($data['tractor_vehicle']);
		 $data['cargo_id'] = $q['cargo_id'];
		 $data['liability_id'] = $q['liability_id'];
		 $data['pd_id'] = $q['pd_id'];
		 
		if($data['quote_id'] > 0)
		{

		}
		
		if($data['row']->id>0){
			$data['action'] = 'update';
			$data['heading'] = 'Edit';
			
		}

		$this->load->view('admin_ratesheet',$data); 
				
	}
		
	/**By Ashvin Patel 10/may/2014**/	
	public function get_underwriter_by_producer()
	{
		$producer_email =  $this->input->post('producer');
		$this->db->select('underwriter,id');
		$this->db->from('rq_members');
		$this->db->where('email', $producer_email);
		$query = $this->db->get();
		$result = $query->row();
		//print_r($result);
		$data['underwriter'] = isset($result->underwriter) ? $result->underwriter : '';
		echo json_encode($data);
	}
	/**By Ashvin Patel 10/may/2014**/
	
	/**By Ashvin Patel**/
	public function edit_ratesheet_manual()
	{
		
		$data = array();
		
		$data['row'] = new stdClass();
		//$data['row'] = $this->admin_init_elements->set_post_vals($this->input->post());
		$data['row']->id = $this->uri->segment(3);	
		$data['action'] = 'add';
		$post_action =$data['action'];
		$data['bundleid'] = $this->uri->segment(3);
		
		//$data['quote'] = $this->quote_model->get_insured_info($data['row']->id);
		
		//print_r($data['quote_bundle']);
		$data['quote_bundle'] = $this->quote_model->get_quote_bundle($data['row']->id);
		//print_r($data['quote_bundle']);
		$data['prodemail'] = $data['quote_bundle'][0]['producer_email'];
		$data['produnderwriters'] = $data['quote_bundle'][0]['underwriters'];
		$data['is_manual'] = $data['quote_bundle'][0]['is_manual'];
		$q = $data['quote_bundle'][0];
		
		$data['cargo_rate'] = $this->quote_model->get_sheet_rate($q['cargo_id'],'cargo');
		//print_r($data['cargo_rate']);
		$data['pd_rate'] = $this->quote_model->get_sheet_rate($q['pd_id'],'pd');
		$data['liability_rate'] = $this->quote_model->get_sheet_rate($q['liability_id'],'liability');
		
		$data['carriers'] = $this->catlog_model->get_carrier_by_state('CA');
		
		$data['underwriters'] = $this->members_model->get_underwriters();
		
		//print_r($data['carriers']);
		$data['quote_id'] = $q['quote_id'];
		$data['truck_vehicle'] = $this->quote_model->get_all_vehicle($q['quote_id'],'TRUCK');
		$data['tractor_vehicle'] = $this->quote_model->get_all_vehicle($q['quote_id'],'TRACTOR');
		$data['trailer_vehicle'] = $this->quote_model->get_all_vehicle($q['quote_id'],'TRAILER');
		
		$data['cargo_id'] = $q['cargo_id'];
		$data['liability_id'] = $q['liability_id'];
		$data['pd_id'] = $q['pd_id'];
		if($data['quote_id'] > 0)
		{
			$data['cargo_rate'] = $this->ratesheet_model->get_cargo($data['quote_id']);
			$data['pd_rate'] = $this->ratesheet_model->get_pd($data['quote_id']);
			$data['liability_rate'] = $this->ratesheet_model->get_liability($data['quote_id']);
		}
		
	//	print_r($data['liability_rate']);

		if($data['row']->id>0){
			$data['action'] = 'update';
			$data['heading'] = 'Edit';
			
		}
		//echo '<pre>';print_r($data);die;
		$this->load->view('edit_manual_ratesheet',$data); 
				
	}
	/**By Ashvin Patel**/
	
	/*public function edit_generate_ratesheet()
	{
		
		$data = array();
		
		$data['row'] = new stdClass();
		//$data['row'] = $this->admin_init_elements->set_post_vals($this->input->post());
		$data['row']->id = $this->uri->segment(3);	
		$data['action'] = 'add';
		$post_action =$data['action'];
		$data['bundleid'] = $this->uri->segment(3);
		
		$data['quote_bundle'] = $this->quote_model->get_quote_bundle($data['row']->id);
		
		$q = $data['quote_bundle'][0];
		
		$data['cargo_rate'] = $this->quote_model->get_sheet_rate($q['cargo_id'],'cargo');
	
		$data['pd_rate'] = $this->quote_model->get_sheet_rate($q['pd_id'],'pd');
		$data['liability_rate'] = $this->quote_model->get_sheet_rate($q['liability_id'],'liability');
		
		$data['carriers'] = $this->catlog_model->get_carrier_by_state($data['quote']->insured_state);
		
		$data['quote_id'] = $q['quote_id'];
		$data['cargo_id'] = $q['cargo_id'];
		$data['liability_id'] = $q['liability_id'];
		$data['pd_id'] = $q['pd_id'];
		/*if($data['quote_id'] > 0)
		{
			$data['cargo_rate'] = $this->ratesheet_model->get_cargo($data['quote_id']);
			$data['pd_rate'] = $this->ratesheet_model->get_pd($data['quote_id']);
			$data['liability_rate'] = $this->ratesheet_model->get_liability($data['quote_id']);
		}

		if($data['row']->id>0){
			$data['action'] = 'update';
			$data['heading'] = 'Edit';
			
		}
		//echo '<pre>';print_r($data);die;
		$this->load->view('edit_ratesheet',$data); 
				
	}*/
	
	public function view_ratesheet()
	{
		
		$data = array();
		
		$data['row'] = new stdClass();
		//$data['row'] = $this->admin_init_elements->set_post_vals($this->input->post());
		$data['row']->id = $this->uri->segment(3);	
		$data['action'] = 'add';
		$post_action =$data['action'];
		$data['bundleid'] = $this->uri->segment(3);
		
		$data['quote'] = $this->quote_model->get_insured_info($data['row']->id);
		
		$data['quote_bundle'] = $this->quote_model->get_quote_bundle($data['row']->id);
		
		$q = $data['quote_bundle'][0];
		
		$data['cargo_rate'] = $this->quote_model->get_sheet_rate($q['cargo_id'],'cargo');
	
		$data['pd_rate'] = $this->quote_model->get_sheet_rate($q['pd_id'],'pd');
		$data['liability_rate'] = $this->quote_model->get_sheet_rate($q['liability_id'],'liability');
		
		$data['carriers'] = $this->catlog_model->get_carrier_by_state($data['quote']->insured_state);
		
		$data['quote_id'] = $q['quote_id'];
		$data['cargo_id'] = $q['cargo_id'];
		$data['liability_id'] = $q['liability_id'];
		$data['pd_id'] = $q['pd_id'];
		if($data['quote_id'] > 0)
		{
			$data['cargo_rate'] = $this->ratesheet_model->get_cargo($data['quote_id']);
			$data['pd_rate'] = $this->ratesheet_model->get_pd($data['quote_id']);
			$data['liability_rate'] = $this->ratesheet_model->get_liability($data['quote_id']);
		}

		if($data['row']->id>0){
			$data['action'] = 'update';
			$data['heading'] = 'Edit';
			
		}
		//echo '<pre>';print_r($data);die;
		$this->load->view('view_admin_ratesheet',$data); 
				
	}
	
	
	/*function save_cargo()
	{
		print_r($this->input->post());
		die();
	}*/
	
	public function rate_by_quote()
	{
			
			$limit = 25;
			$total = $this->underwriter_model->count_underwriter(); 
			
			$data['admins'] = $this->quote_model->get_quote();
			$data['object'] = $this;
			
			$data['rows'] = $this->quote_model->get_bundle_by_quote($this->uri->segment(3));
			
			
			$quote_id = $data['rows'][0];
			//$config['base_url'] = base_url('administration/quotes');
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			
			$data['total'] = $total;
			$this->pagination->initialize($config);

			$this->load->view('ratesheet/rate_by_quote', $data);
	}
	
	function save_cargo($post_array='',$action='')
	{	
		
		$post_array = $this->input->post();
		$action = ($post_array['bundleid'] == '')?'inserted':'updated'; 
		//print_r($action);
		if($post_array['bundleid'] != '') {
			$status = $this->ratesheet_model->get_bundle_status($post_array['bundleid']);
			$action = ($status == 'Released')?'inserted':'updated'; 
			
		}
		
		$data['error_message'] = $this->ratesheet_model->insert_ratesheet($post_array,$action);
		
		echo $data['error_message'];
		if($data['error_message'] == 'Record '.$action.' successfully'){
			$data['row'] = new stdClass();
			$data['row']->id = $this->uri->segment(3);	
			$data['row']->status = 1;
			
		}
	}	
	
		
	function save_excess_cargo($post_array='',$action='')
	{	
		
		$post_array = $this->input->post();
	
		$action = ($post_array['bundleid'] == '')?'inserted':'updated'; 
		//print_r($action);
		if($post_array['bundleid'] != '') {
			$status = $this->ratesheet_model->get_bundle_status($post_array['bundleid']);
			$action = ($status == 'Released')?'inserted':'updated'; 
			
		}
		
		$data['error_message'] = $this->ratesheet_model->excess_ratesheet($post_array,$action);
		
		echo $data['error_message'];
		if($data['error_message'] == 'Record '.$action.' successfully'){
			$data['row'] = new stdClass();
			$data['row']->id = $this->uri->segment(3);	
			$data['row']->status = 1;
			
		}
	}	
	
	function save_physical_damage()
	{	
		
		$post_array = $this->input->post();
		
		$action = ($post_array['bundleid'] == '')?'inserted':'updated'; 
		if($post_array['bundleid'] != '') {
			$status = $this->ratesheet_model->get_bundle_status($post_array['bundleid']);
			$action = ($status == 'Released')?'inserted':'updated'; 
		}
		$data['error_message'] = $this->ratesheet_model->insert_ratesheet_physical_damage($post_array,$action);
		echo $data['error_message'];
		if($data['error_message'] == 'Record '.$action.' successfully'){
			$data['row'] = new stdClass();
			$data['row']->id = $this->uri->segment(3);	
			$data['row']->status = 1;
		}
	}	
	
	
	
	public function submit_bundle()
	{
		$post_action = '';
		$post_array = $this->input->post();
	//	print_r($post_array);
		$action = ($post_action == 'add')?'inserted':'updated';
		$result = $this->ratesheet_model->submit_bundle($post_array);
		// $email = $this->input->post('producer_email');
		$data['email']=$this->session->userdata('underwriter_email');
		// $data['email'];
		
		$msg = ''; 
		
		$this->db->select('producer_email');
		$this->db->from('rq_rqf_quote_bundle');
		$this->db->where('id', $post_array['bundle_id']);
		$query = $this->db->get();
		$result = $query->row();
		$data['email_p'] = $result->producer_email;
		
		
		//ini_set('dispaly_errors', 1);
		//error_reporting(E_ALL);
		$this->db->select('id,first_name,middle_name,last_name');
		$this->db->from('rq_members');
		$this->db->where('email', $data['email_p']);
		$query = $this->db->get();
		$result = $query->row();
		 $data['name'] = $result->first_name;

		$this->send_mail($data, $msg);
	}
	public function send_mail($data, $msg) {
			$config = array(           
            'charset' => 'utf-8',
            'wordwrap' => TRUE,
            'mailtype' => 'html'
       	 );
		 
		 //$mail_from = get_mail_from_id();
		 $mail_from = 'support@gcib.net';
		 
		 $data['mail_content'] = getMailContent('quote_release');
		 
		/* $searchReplaceArray = array(
		  '<full_name>' => '', 
		  '<site_name>' => '',
		  '<user>' => $data['email'],
		  '<pass>' => '',
		  '<support_email>' => $mail_from
		);*/
		
		$result  =  nl2br(str_replace('<full_name>', $data['name'], $data['mail_content']->msg));
		$result1 =  str_replace('<site_name>', base_url(), $result);	
		$result2 =  str_replace('<user>', $data['email'], $result1);		
		$result3 =  str_replace('<pass>', '', $result2);
		$result4 =  str_replace('<support_email>', $mail_from, $result3);
		
		/*$result = str_replace(
		  array_keys($searchReplaceArray), 
		  array_values($searchReplaceArray), 
		  $data['mail_content']->msg
		);*/
		// echo '<pre>';
		//print_r($this->session->all_userdata());
		
		
		//$data['email'] = 'mayankg17@gmail.com';
		//print_r($data['email']);
		//die;
		   $this->load->library( 'email', $config);
		   $this->email->from($mail_from , 'Gcib Ratesheet' );
		   $this->email->to($data['email_p']);
		   $this->email->cc($data['email']);
		   $this->email->subject( $data['mail_content']->sub );
		   $this->email->message( $result4 );
		   $this->email->send();
	}
	
	function save_liability()
	{			
		$post_array = $this->input->post();
		//print_r($post_array['bundleid']);
		$action = ($post_array['bundleid'] == '')?'inserted':'updated'; 
		if($post_array['bundleid'] != '') {
			$status = $this->ratesheet_model->get_bundle_status($post_array['bundleid']);
			$action = ($status == 'Released')?'inserted':'updated'; 
		}
		$data['error_message'] = $this->ratesheet_model->insert_liability($post_array,$action);
		echo $data['error_message'];
		if($data['error_message'] == 'Record '.$action.' successfully'){
			$data['row'] = new stdClass();
			$data['row']->id = $this->uri->segment(3);	
			$data['row']->status = 1;
		}
	}	
	public function updateRatesheet_sh_flag(){
		$id = $this->input->post('id');	
		$type = $this->input->post('type');	
		$flag = $this->input->post('flag');	
		if($id!='') { 
			if($type=='cargo'){
				$table_name = 'rq_ratesheet';
				$column_name = 'cargo_rate_sheet_sh';	
			} elseif($type=='pd'){
				$table_name = 'rq_ratesheet_damage_ratesheet';	
				$column_name = 'pd_rate_sheet_sh';	
			} elseif($type=='liability'){
				$table_name = 'rq_ratesheet_liability';	
				$column_name = 'liability_rate_sheet_sh';	
			}
			if(isset($table_name)&&isset($column_name)) {
				$this->db->update($table_name, array($column_name => $flag), array('id' => $id));
				echo $this->db->affected_rows();
			}
		}
	}
	/*function GetAll(){
		$data['query']=	$this->ratesheet_model->show_ratesheet_data();
		$this->load->view('ratesheet',$data);
		$this->load->model('ratesheet_model');
		echo 'cont';
	}*/
	
	
	/*public function view()
        {
			echo 'hello1'; 
          //  $this->load->model('ratesheet_model');
            $data['records']= $this->ratesheet_model->getAll();
        }*/
	
	
	
	
	


	public $file_upload_path = './uploads/quote_request/';
	
    /*public function __construct()
	{
		parent::__construct();
		if(!$this->require_login_user() && !$this->require_login_underwriter())
		{
			$base = base_url('login');
			redirect($base);
		}
		$this->load->model('ratesheet_model');
		 
		
		// Your own constructor code
		 
		  
		
	}*/

	
  public function QuickQuote2($ratesheet_id = '')
	{
		$this->ratesheet_model->insert_ratesheet($ratesheet_id);
			$upload_path = $this->file_upload_path;
			
			if($_POST){  
				$result = $this->ratesheet_model->insert_ratesheet($upload_path);
				//$this->session->set_flashdata('success', '<strong>Success</strong> Quote Request Sent Successfully');
			} else{
				//$this->session->set_flashdata('error', '<strong>Error</strong> In Sending Quote Request ');
			}
			
			if($ratesheet_id){
				$this->load->view('cargo_rate_sheet', $this->data);
			}else {				
				$this->load->view('cargo_rate_sheet_sucess');
				/* $red_url = base_url('quote/response');
				redirect($red_url); */
			}
	}
	
	

	//$data['error_message'] = $this->ratesheet_model->insert_ratesheet_physical_damage($post_array,$action);
	
	
		
	public function get_um_coverages_detail(){
		 $name=$this->input->post('name');
		 $um_coverage='';
		 $state= $this->input->post('state');
		 $att='class="select-3 span4 efl3 um_changes_option" style="  margin-top: 15px;
"'; 
        echo getumcoverages('um_coverage_surcharge',$att,$um_coverage,$state,$name); 
		
		
		}
    public function get_pip_coverages_detail(){
		
		 $name=$this->input->post('name');
		 $pip_coverage='';
		 $state= $this->input->post('state');
		 $att='class="select-3 span4 efl3 umc pip_changes_option"'; 
         echo getpipcoverages('pip_coverage_surcharge',$att,$pip_coverage,$state,$name); 
		}
	public function get_uim_coverages_detail(){
		 $name=$this->input->post('name');
		 $uim_coverage='';
		 $state= $this->input->post('state');
		 $att='class="select-3 span4 efl3 um_changes_option" style="  margin-top: 15px;
"'; 
        echo getuimcoverages('uim_coverage_surcharge',$att,$uim_coverage,$state,$name); 
		
		
		}
	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */