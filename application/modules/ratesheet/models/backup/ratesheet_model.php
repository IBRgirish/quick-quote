<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ratesheet_model extends CI_Model {

    public function &__get($key) {
        $CI = & get_instance();
        return $CI->$key;
        $this->load->database();
    }
	public function getAmount($money)
	{
		$cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
		$onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);
	
		$separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;
	
		$stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
		$removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);
	
		return (float) str_replace(',', '.', $removedThousendSeparator);
	}
		
		function insert_liability($post_array,$action) {		
			$insert_data = array();
			$insert_data['quote_id'] = trim($post_array['quote_id']);
			$insert_data['liability_rate_sheet_sh'] = trim(isset($post_array['liability_rate_sheet']) ? $post_array['liability_rate_sheet'] : '');
			$insert_data['broker'] = trim(isset($post_array['broker']) ? $post_array['broker'] : '');
			$insert_data['insured'] = trim(isset($post_array['insured']) ? $post_array['insured'] :'');
			$insert_data['radius_of_operation'] = trim(isset($post_array['radius_of_operation']) ? $post_array['radius_of_operation'] :'');
			$insert_data['other_rop'] = trim(isset($post_array['claim_venue']) ? $post_array['claim_venue'] : '');
			$insert_data['state'] = trim(isset($post_array['state']) ? $post_array['state'] :'');
			$insert_data['claim_venue_rate'] = trim(isset($post_array['claim_venue_rate']) ? $post_array['claim_venue_rate'] : '');
			$insert_data['no_of_power_unit'] = trim(isset($post_array['no_of_power_unit']) ? $post_array['no_of_power_unit'] : '');
			$insert_data['combined_single_limit'] = isset($post_array['combined_single_limit']) ? $post_array['combined_single_limit'] : '';
			$insert_data['deductible'] = isset($post_array['deductible']) ? $post_array['deductible'] : '';
			$insert_data['unit_price_a'] = isset($post_array['unit_price_a']) ? $this->getAmount(trim($post_array['unit_price_a'])) : '';
			$insert_data['quantity_a'] = trim(isset($post_array['quantity_a']) ? $post_array['quantity_a'] :'');
			$insert_data['unit_price_b'] = isset($post_array['unit_price_b']) ? $this->getAmount(trim($post_array['unit_price_b'])) : '';
			$insert_data['quantity_b'] = trim(isset($post_array['quantity_b']) ? $post_array['quantity_b'] :'');
			$insert_data['new_venture_surcharge'] = trim(isset($post_array['new_venture_surcharge']) ? $post_array['new_venture_surcharge'] :'');
			$insert_data['new_venture_cost'] = trim(isset($post_array['new_venture_cost']) ? $post_array['new_venture_cost'] :'');
			$insert_data['new_venture_premium'] = isset($post_array['new_venture_premium']) ? $this->getAmount(trim($post_array['new_venture_premium'])) : '';
			$insert_data['lapse_in_coverage_surcharge'] = trim(isset($post_array['lapse_in_coverage_surcharge']) ? $post_array['lapse_in_coverage_surcharge'] : '');
			$insert_data['lapse_in_coverage_percentage'] = trim(isset($post_array['lapse_in_coverage_percentage']) ? $post_array['lapse_in_coverage_percentage'] : '');
			$insert_data['lapse_in_coverage_premium'] = isset($post_array['lapse_in_coverage_premium']) ? $this->getAmount(trim($post_array['lapse_in_coverage_premium'])) : '';
			$insert_data['auto_hauler_surcharge'] = isset($post_array['auto_hauler_surcharge']) ? $post_array['auto_hauler_surcharge'] :'';
			$insert_data['auto_hauler_cost'] = isset($post_array['auto_hauler_cost']) ? $post_array['auto_hauler_cost'] : '';
			$insert_data['auto_hauler_premium'] = isset($post_array['auto_hauler_premium']) ? $this->getAmount(trim($post_array['auto_hauler_premium'])) : '';
			$insert_data['um_coverage_surcharge'] = trim(isset($post_array['um_coverage_surcharge']) ? $post_array['um_coverage_surcharge'] :'');
			$insert_data['um_coverage_cost'] = trim(isset($post_array['um_coverage_cost']) ? $post_array['um_coverage_cost'] : '');
			$insert_data['um_coverage_premium'] = isset($post_array['um_coverage_premium']) ? $this->getAmount(trim($post_array['um_coverage_premium'])) : '';
			$insert_data['pip_coverage_surcharge'] = trim(isset($post_array['pip_coverage_surcharge']) ? $post_array['pip_coverage_surcharge'] : '');
			$insert_data['pip_coverage_cost'] = trim(isset($post_array['pip_coverage_cost']) ? $post_array['pip_coverage_cost'] : '');
			$insert_data['pip_coverage_premium'] = isset($post_array['pip_coverage_premium']) ? $this->getAmount(trim($post_array['pip_coverage_premium'])) : '';
			$insert_data['losses_surcharge'] = trim(isset($post_array['losses_surcharge']) ? $post_array['losses_surcharge'] : '');
			$insert_data['losses_percentage'] = trim(isset($post_array['losses_percentage']) ? $post_array['losses_percentage'] : '');
			$insert_data['losses_percentage_other'] = trim(isset($post_array['losses_percentage_other']) ? $post_array['losses_percentage_other'] : '');
			$insert_data['losses_premium'] = isset($post_array['losses_premium']) ? $this->getAmount(trim($post_array['losses_premium'])) : '';
			$insert_data['miscellaneos_surcharge'] = trim(isset($post_array['miscellaneos_surcharge']) ? $post_array['miscellaneos_surcharge'] : '');
			$insert_data['miscellaneos_surcharge_cost'] = trim(isset($post_array['miscellaneos_surcharge_cost']) ? $post_array['miscellaneos_surcharge_cost'] : '');
			$insert_data['miscellaneos_surcharge_premium'] = isset($post_array['miscellaneos_surcharge_premium']) ? $this->getAmount(trim($post_array['miscellaneos_surcharge_premium'])) : '';
			$insert_data['drivers_surcharge_a'] = trim(isset($post_array['drivers_surcharge_a']) ? $post_array['drivers_surcharge_a'] : '');
			$insert_data['drivers_surcharge_a'] = trim(isset($post_array['drivers_surcharge_a']) ? $post_array['drivers_surcharge_a'] : '');
			$insert_data['drivers_surcharge_a_percentage'] = trim(isset($post_array['drivers_surcharge_a_percentage']) ? $post_array['drivers_surcharge_a_percentage'] : '');
			$insert_data['drivers_surcharge_a_premium'] = isset($post_array['drivers_surcharge_a_premium']) ? $this->getAmount(trim($post_array['drivers_surcharge_a_premium'])) : '';
			$insert_data['drivers_surcharge_b'] = trim(isset($post_array['drivers_surcharge_b']) ? $post_array['drivers_surcharge_b'] : '');
			$insert_data['drivers_surcharge_b_percentage'] = isset($post_array['drivers_surcharge_b_percentage']) ? $post_array['drivers_surcharge_b_percentage'] : '';
			$insert_data['drivers_surcharge_b_premium'] = isset($post_array['drivers_surcharge_b_premium']) ? $this->getAmount(trim($post_array['drivers_surcharge_b_premium'])) : '';
			$insert_data['extra_trailer_quantity'] = isset($post_array['extra_trailer_quantity']) ? $post_array['extra_trailer_quantity'] : '';
			$insert_data['extra_trailer_cost'] = isset($post_array['extra_trailer_cost']) ? $post_array['extra_trailer_cost'] : '';
			$insert_data['extra_trailer_premium'] = isset($post_array['extra_trailer_premium']) ? $this->getAmount(trim($post_array['extra_trailer_premium'])) : '';
			$insert_data['tractor/trailer_quantity'] = isset($post_array['tractor/trailer_quantity']) ? $post_array['tractor/trailer_quantity'] : '';		
			$insert_data['tractor/trailer_surcharge_cost'] = isset($post_array['tractor/trailer_surcharge_cost']) ? $post_array['tractor/trailer_surcharge_cost'] : '';
			$insert_data['tractor/trailer_surcharge_premium'] = isset($post_array['tractor/trailer_surcharge_premium']) ? $this->getAmount(trim($post_array['tractor/trailer_surcharge_premium'])) : '';
			$insert_data['discount'] = isset($post_array['discount']) ? $post_array['discount'] : '';
			$insert_data['discount_percentage'] = isset($post_array['discount_percentage']) ? $post_array['discount_percentage'] : '';
			$insert_data['discounted_premium'] = isset($post_array['discounted_premium']) ? $this->getAmount(trim($post_array['discounted_premium'])) : '';
			$insert_data['total_liability_premium'] = isset($post_array['total_liability_premium']) ? $this->getAmount(trim($post_array['total_liability_premium'])) : '';
			$insert_data['unit_price_breakdown'] = isset($post_array['unit_price_breakdown']) ? $post_array['unit_price_breakdown'] : '';
			$insert_data['liability_policy_fee'] = isset($post_array['liability_policy_fee']) ? $this->getAmount(trim($post_array['liability_policy_fee'])) : '';
			$insert_data['liablity_filing_fee'] = isset($post_array['liablity_filing_fee']) ? $this->getAmount(trim($post_array['liablity_filing_fee'])) : '';
			$insert_data['carrier'] = isset($post_array['carriers']) ? $post_array['carriers'] : '';
			$insert_data['comments'] = trim(isset($post_array['comments']) ? $post_array['comments'] : '');
			$insert_data['status'] = 'Pending';
			if($post_array['bundleid'] != '')
			{
				$where_array = array('rq_rqf_quote_bundle.id' => $post_array['bundleid']);
				$this->db->select('rq_rqf_quote_bundle.liability_id');
				$this->db->from('rq_rqf_quote_bundle');
				$this->db->where($where_array);

				$query = $this->db->get();
				$cargo = $query->result();
				$liability_id = isset($cargo[0]->liability_id) ? $cargo[0]->liability_id : '';
				$action = ($liability_id != '0' ) ? 'updated' : 'inserted';
				
			} else 
			{
				$action = 'inserted';
			}
			//echo $action;
		if($action == 'inserted') {
			$insert_data['created_by'] = $this->session->userdata('admin_id');
			$insert_data['created_date'] = date("Y-m-d");
			$this->db->insert('rq_ratesheet_liability', $insert_data);
			$result_id_ratesheet = $this->db->insert_id();
			if($post_array['bundleid'] == '')
			{
				$insert_bundle_array =  array();
				$insert_bundle_array['quote_id'] = $post_array['quote_id'];
				$insert_bundle_array['liability_id'] = trim($result_id_ratesheet);
				if($this->session->userdata('member_id')){		
					$insert_bundle_array['created_by'] = $this->session->userdata('member_id');
				} elseif($this->session->userdata('underwriter_id')){
					$insert_bundle_array['created_by'] = $this->session->userdata('underwriter_id');
				}
				$insert_bundle_array['creation_date'] = date("Y-m-d");
				$insert_bundle_array['status'] = 'Draft';
				$insert_bundle_array['producer_email'] = $post_array['producer_email'];
				$insert_bundle_array['quote_ref'] = $this->generate_quote_code();;
				$this->db->insert('rq_rqf_quote_bundle', $insert_bundle_array);
				$result_bid_ratesheet = $this->db->insert_id();
				if($post_array['quote_id'] != '') {
					$this->db->query("update rq_rqf_quote set bundle_id = ".$result_bid_ratesheet.",bundle_status = 'Draft' where ((quote_id=".$post_array['quote_id'].") and (bundle_id = '0'))");
				}
			} 
			else {
				$this->db->query("update rq_rqf_quote_bundle set liability_id = ".$result_id_ratesheet." where id=".$post_array['bundleid']);
				$result_bid_ratesheet = $post_array['bundleid'];
			}
		}  else
		{
			//print_r($insert_data);
			$this->db->update('rq_ratesheet_liability',$insert_data, array('id'=>$liability_id));
			//echo $this->db->affected_rows();
			$result_id_ratesheet =  $liability_id;
			$result_bid_ratesheet = $post_array['bundleid'];
		}
		$result =  $result_bid_ratesheet;
		return $result;
	}
	
	function get_bundle_status($bundleid)
	{
		$query = $this->db->query("SELECT status from rq_rqf_quote_bundle where id=".$bundleid);
  		$result = $query->result();
		
		return isset($result[0]->status) ? $result[0]->status : '';
		
	}
	
	function generate_quote_code()
	{
		$characters = array(
		"A","B","C","D","E","F","G","H","J","K","L","M",
		"N","P","Q","R","S","T","U","V","W","X","Y","Z",
		"1","2","3","4","5","6","7","8","9");

		//make an "empty container" or array for our keys
		$keys = array();
		$random_chars = '';
		//first count of $keys is empty so "1", remaining count is 1-7 = total 8 times
		while(count($keys) < 8) {
			//"0" because we use this to FIND ARRAY KEYS which has a 0 value
			//"-1" because were only concerned of number of keys which is 32 not 33
			//count($characters) = 33
			$x = mt_rand(0, count($characters)-1);
			if(!in_array($x, $keys)) {
			   $keys[] = $x;
			}
		}

		foreach($keys as $key){
		   $random_chars .= $characters[$key];
		}
		
		return $random_chars;
	}
	
	function insert_ratesheet($post_array,$action) {
		$insert_data = array();
		$insert_data['quote_id'] = trim(isset($post_array['quote_id']) ? $post_array['quote_id'] : '');
		$insert_data['cargo_rate_sheet_sh'] = trim(isset($post_array['cargo_rate_sheet']) ? $post_array['cargo_rate_sheet'] : '');
		$insert_data['broker'] = trim(isset($post_array['broker']) ? $post_array['broker'] : '');
		$insert_data['insured'] = trim(isset($post_array['insured']) ? $post_array['insured'] : '');
		$insert_data['radius_of_operation'] = trim(isset($post_array['radius_of_operation']) ? $post_array['radius_of_operation'] : '');
		$insert_data['other_rop'] = trim(isset($post_array['others']) ? $post_array['others'] : '');
		$insert_data['state'] = trim(isset($post_array['state']) ? $post_array['state'] : '');
		$insert_data['claim_venue'] = trim(isset($post_array['claim_venue']) ? $post_array['claim_venue'] : '');
		$insert_data['no_of_power_unit'] = trim(isset($post_array['no_of_power_unit']) ? $post_array['no_of_power_unit'] : '');
		$insert_data['limit'] = trim(isset($post_array['limit']) ? $post_array['limit'] : '');
		$insert_data['deductible'] = trim(isset($post_array['deductible']) ? $post_array['deductible'] : '');
		$insert_data['reffer_deductible'] = trim(isset($post_array['reffer_deductible']) ? $post_array['reffer_deductible'] : '');
		$insert_data['base_rate'] = $this->getAmount(isset($post_array['base_rate']) ? $post_array['base_rate'] : '');
		$insert_data['earned_freight_limit'] = trim(isset($post_array['earned_freight_limit']) ? $post_array['earned_freight_limit'] : '');
		$insert_data['earned_freight_cost'] = trim(isset($post_array['earned_freight_cost']) ? $post_array['earned_freight_cost'] : '');
		$insert_data['earned_freight_premium'] = $this->getAmount(isset($post_array['earned_freight_premium']) ? $post_array['earned_freight_premium'] : '');
		$insert_data['debris_removal_limit'] = $this->getAmount(isset($post_array['debris_removal_limit_']) ? $post_array['debris_removal_limit_'] : '');
		$insert_data['debris_removal_cost'] = trim(isset($post_array['debris_removal_cost']) ? $post_array['debris_removal_cost'] : '');
		$insert_data['debris_removal_premium'] = $this->getAmount(isset($post_array['debris_removal_premium']) ? $post_array['debris_removal_premium'] : '');
		$insert_data['tarpauling_coverage_limit'] = trim(isset($post_array['tarpauling_coverage_limit']) ? $post_array['tarpauling_coverage_limit'] : '');
		$insert_data['tarpauling_coverage_cost'] = trim(isset($post_array['tarpauling_coverage_cost']) ? $post_array['tarpauling_coverage_cost'] : '');
		$insert_data['tarpauling_coverage_premium'] = $this->getAmount(isset($post_array['tarpauling_coverage_premium']) ? $post_array['tarpauling_coverage_premium'] : '');
		$insert_data['reefer_breakdown_limit'] = trim(isset($post_array['reefer_breakdown_limit']) ? $post_array['reefer_breakdown_limit'] : '');
		$insert_data['lapse_in_coverage_surcharge'] = trim(isset($post_array['lapse_in_coverage_surcharge']) ? $post_array['lapse_in_coverage_surcharge'] : '');
		$insert_data['lapse_in_coverage_percentage'] = trim(isset($post_array['lapse_in_coverage_percentage']) ? $post_array['lapse_in_coverage_percentage'] : '');
		$insert_data['lapse_in_coverage_premium'] = $this->getAmount(isset($post_array['lapse_in_coverage_premium']) ? $post_array['lapse_in_coverage_premium'] : '');
		$insert_data['new_venture_surcharge'] = trim(isset($post_array['new_venture_surcharge']) ? $post_array['new_venture_surcharge'] : '');
		$insert_data['new_venture_percentage'] = trim(isset($post_array['new_venture_percentage']) ? $post_array['new_venture_percentage'] : '');
		$insert_data['new_venture_premium'] = $this->getAmount(isset($post_array['new_venture_premium']) ? $post_array['new_venture_premium'] : '');
		$insert_data['losses_surcharge'] = trim(isset($post_array['losses_surcharge']) ? $post_array['losses_surcharge'] : '');
		$insert_data['losses_percentage'] = trim(isset($post_array['losses_percentage'] ) ? $post_array['losses_percentage'] : '');
		$insert_data['losses_premium'] = $this->getAmount(isset($post_array['losses_premium']) ? $post_array['losses_premium']: '');
		$insert_data['drivers_surcharge'] = trim(isset($post_array['drivers_surcharge']) ? $post_array['drivers_surcharge'] : '');
		$insert_data['drivers_surcharge_percentage'] = trim(isset($post_array['drivers_surcharge_percentage']) ? $post_array['drivers_surcharge_percentage'] : '');
		$insert_data['driver_surcharge_premium'] = $this->getAmount(isset($post_array['driver_surcharge_premium']) ? $post_array['driver_surcharge_premium'] : '');
		$insert_data['old_units'] = trim(isset($post_array['old_units']) ? $post_array['old_units'] : '');
		$insert_data['old_units_surcharge'] = trim(isset($post_array['old_units_surcharge']) ? $post_array['old_units_surcharge'] : '');
		$insert_data['old_units_premium'] = trim(isset($post_array['old_units_premium']) ? $post_array['old_units_premium'] : '');
		$insert_data['total_cargo_premium'] = $this->getAmount(isset($post_array['total_cargo_premium']) ? $post_array['total_cargo_premium'] : '');
		$insert_data['cargo_policy_fee'] = $this->getAmount(isset($post_array['cargo_policy_fee']) ? $post_array['cargo_policy_fee'] : '');
		$insert_data['cargo_filing_fee'] = trim(isset($post_array['cargo_filing_fee']) ? $post_array['cargo_filing_fee'] : '');
		$insert_data['cargo_sla_tax'] = trim(isset($post_array['cargo_sla_tax']) ? $post_array['cargo_sla_tax'] : '');
		$insert_data['carrier'] = trim(isset($post_array['carriers']) ? $post_array['carriers'] : '');
		$insert_data['commodities'] = trim(isset($post_array['commodities']) ? $post_array['commodities'] : '');
		$insert_data['required_for_firm_quote'] = trim(isset($post_array['required_for_firm_quote']) ? $post_array['required_for_firm_quote'] : '');
		$insert_data['comments'] = trim(isset($post_array['comments']) ? $post_array['comments'] : '');
		$insert_data['status'] = 'Pending';
		if($post_array['bundleid'] != '')
			{
				$where_array = array('rq_rqf_quote_bundle.id' => $post_array['bundleid']);
				$this->db->select('rq_rqf_quote_bundle.cargo_id,rq_rqf_quote_bundle.status,rq_rqf_quote_bundle.quote_ref');
				$this->db->from('rq_rqf_quote_bundle');
				$this->db->where($where_array);
				$query = $this->db->get();
				$cargo = $query->result();
				$cargo_id = $cargo[0]->cargo_id;
				$bundlestatus =  $cargo[0]->status;
				$bundlerefcode = $cargo[0]->quote_ref;
				$action = ($cargo_id != '0' && $bundlestatus != 'Released' ) ? 'updated' : 'inserted';
			} else 
			{
				$action = 'inserted';
				$bundlestatus = '';
				$bundlerefcode = '';
			}
		if($action == 'inserted') {
			$insert_data['created_by'] = $this->session->userdata('admin_id');
			$insert_data['created_date'] = date("Y-m-d");
			$this->db->insert('rq_ratesheet', $insert_data);
			$result_id_ratesheet = $this->db->insert_id();
			
			if($post_array['bundleid'] == '' || $bundlestatus == 'Released')
			{
				$insert_bundle_array =  array();
				$insert_bundle_array['quote_id'] = $post_array['quote_id'];
				$insert_bundle_array['cargo_id'] = trim($result_id_ratesheet);
				if($this->session->userdata('member_id')){		
					$insert_bundle_array['created_by'] = $this->session->userdata('member_id');
				} elseif($this->session->userdata('underwriter_id')){
					$insert_bundle_array['created_by'] = $this->session->userdata('underwriter_id');
				}
				$insert_bundle_array['creation_date'] = date("Y-m-d H:i:s");
				$insert_bundle_array['status'] = 'Draft';
				$insert_bundle_array['producer_email'] = $post_array['producer_email'];
				$insert_bundle_array['underwriters'] = $post_array['underwriter_ids'];
				
				if($bundlestatus == 'Released')
				{
					$insert_bundle_array['quote_ref'] = $bundlerefcode;
				}
				else
				{
					$insert_bundle_array['quote_ref'] = $this->generate_quote_code();
				}
				$insert_bundle_array['is_manual'] = isset($post_array['is_manual']) ? $post_array['is_manual'] : '';
				$insert_bundle_array['parent_id'] = isset($post_array['parentbundleid']) ? $post_array['parentbundleid'] : '';
				$this->db->insert('rq_rqf_quote_bundle', $insert_bundle_array);
				$result_bid_ratesheet = $this->db->insert_id();
				//echo $post_array['quote_id'];
				if($post_array['quote_id'] != '')
				{
					$this->db->query("update rq_rqf_quote set bundle_id = ".$result_bid_ratesheet.",bundle_status = 'Draft' where ((quote_id=".$post_array['quote_id'].") and (bundle_id = '0'))");
				}
			} else {
				$this->db->query("update rq_rqf_quote_bundle set cargo_id = ".$result_id_ratesheet." where id=".$post_array['bundleid']);
				$result_bid_ratesheet = $post_array['bundleid'];
			}
		} else
		{
			$this->db->update('rq_ratesheet',$insert_data, array('id'=>$cargo_id));
			$result_id_ratesheet =  $cargo_id;
			$result_bid_ratesheet = $post_array['bundleid'];
		}
		$result =  $result_bid_ratesheet;
		return $result;
	
	}
	
	
	
	function get_bundles($id = '')
	{
		$sql = "select * from rq_rqf_quote_bundle as q ";
		if($id > 0) 
	        $sql .= " and q.quote_id = ". $id;
		$query = $this->db->query($sql);
		//$this->data['quote'] = $query->result();
		return $query->result();
	}
	
			
				public function get_broker_quote($id=''){
		
		  $this->db->select('*');
	      $this->db->from('rq_members');
	     if($id!='')
	    {
		$this->db->where('id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
	
	
	function change_status($status,$id)
	{
		
		//echo $status;
		//die;
		if($status == 'accept') $status = 'Accepted';
		else if($status == 'deny') $status = 'Rejected';
		else if($status == 'revise') $status = 'Revision Requested';
		$sql = "select * from rq_rqf_quote_bundle where id=".$id;
		
		$query = $this->db->query($sql);
		$row = $query->row();
		$data['quote_number'] = '';
		if($row->quote_id != 0)
		{
			$sql = "update rq_rqf_quote set bundle_status = '".$status."' where quote_id=".$row->quote_id;
			$query = $this->db->query($sql);
			$data['quote_number'] = $row->quote_id;
		}
		$sql = "update rq_rqf_quote_bundle set status = '".$status."' where id=".$id;
		$query = $this->db->query($sql);
		

		if($status == 'Revise Request'){ $status = 'Revision Request Sent';}
		$data['message'] = 'Quote '.$status;
		return $data;
	}
	
	function submit_bundle($post_array)
	{
		$insert_data = array();
		$insert_data['quote_id'] = trim($post_array['quote_id']);
		$insert_data['bundle_id'] = trim($post_array['bundle_id']);
		/*$sql = "select * from rq_rqf_quote_bundle as q where id=".$post_array['bundle_id'];
		$query = $this->db->query($sql);
		$row = $query->row();*/
		//$email = $row->publisher_email;
		
		$this->db->query("update rq_rqf_quote_bundle set status = 'Released' where id=".$post_array['bundle_id']);
		
		if($post_array['quote_id']!='')
		{
			$this->db->query("update rq_rqf_quote set bundle_status = 'Released', bundle_id = '".$post_array['bundle_id']."', update_dt = '".date('Y-m-d')."' where quote_id=".$post_array['quote_id']);
		}
		
		/*$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		//$email="mail.girish@gmail.com"; //intercept
		$headers .= 'To: <'.$email.'>' . "\r\n";
		$headers .= 'From: Perfect Relations<no-reply@innonym.com>' . "\r\n";
		$rh="Hi there,<br><br>You request to share the contact details have been approved. Here are the details<br><br>$str<br><br>Cheers,<br>Perfect Relations Admin";
		//echo $rh;
		// Mail it
		try {
		mail($email, "Contact details released", $rh, $headers);
		$u='';
			}		catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "<br>";
		}*/
		//return $row;
	}
	
	function insert_ratesheet_physical_damage($post_array,$action) {
		
		$insert_data = array();
		$insert_data['quote_id'] = trim($post_array['quote_id']);
		$insert_data['pd_rate_sheet_sh'] = trim(isset($post_array['pd_rate_sheet']) ? $post_array['pd_rate_sheet'] :'');
		$insert_data['broker'] = trim($post_array['broker']);
		$insert_data['insured'] = trim($post_array['insured']);
		$insert_data['radius_of_operation'] = trim($post_array['radius_of_operation']);
		$insert_data['other_rop'] = trim(isset($post_array['pdRoOther']) ? $post_array['pdRoOther'] : '');
		$insert_data['state'] = trim(isset($post_array['state']) ? $post_array['state'] : '');
		
		/*$insert_data['claim_venue_rate'] = trim($post_array['claim_venue_rate']);
		$insert_data['no_of_power_unit'] = trim($post_array['no_of_power_unit']);
		$insert_data['limit'] = trim($post_array['limit']);
		$insert_data['reffer_deductible'] = trim($post_array['reffer_deductible']);
		$insert_data['deductible'] = trim($post_array['deductible']);*/
		$insert_data['base_rate'] = $this->getAmount(trim(isset($post_array['base_rate']) ? $post_array['base_rate'] : ''));
		
		
		$insert_data['losses_surcharge'] = trim(isset($post_array['losses_surcharge']) ? $post_array['losses_surcharge'] : '');
		$insert_data['losses_surcharge_cost'] = $this->getAmount(trim(isset($post_array['losses_surcharge_cost']) ? $post_array['losses_surcharge_cost'] : ''));
		$insert_data['losses_surcharge_premium'] = $this->getAmount(trim(isset($post_array['losses_surcharge_premium']) ? $post_array['losses_surcharge_premium'] : ''));
		$insert_data['miscellaneos_surcharge'] = trim(isset($post_array['miscellaneos_surcharge']) ? $post_array['miscellaneos_surcharge'] : '');
		$insert_data['miscellaneos_surcharge_cost'] = isset($post_array['miscellaneos_surcharge_cost']) ? $this->getAmount(trim($post_array['miscellaneos_surcharge_cost'])) : '';
		$insert_data['miscellaneos_surcharge_premium'] = isset($post_array['miscellaneos_surcharge_premium']) ? $this->getAmount(trim($post_array['miscellaneos_surcharge_premium'])) : '';
		$insert_data['driver_surcharge'] = trim(isset($post_array['driver_surcharge']) ? $post_array['driver_surcharge'] : '');
		$insert_data['driver_surcharge_cost'] = isset($post_array['driver_surcharge_cost']) ? $this->getAmount(trim($post_array['driver_surcharge_cost'])) : '';
		$insert_data['driver_surcharge_premium'] = isset($post_array['driver_surcharge_premium']) ? $this->getAmount(trim($post_array['driver_surcharge_premium'])) : '';
		$insert_data['tractor/trailer_quantity'] = trim(isset($post_array['tractor/trailer_quantity']) ? $post_array['tractor/trailer_quantity'] : '');
		$insert_data['tractor/trailer_surcharge_cost'] = isset($post_array['tractor/trailer_surcharge_cost']) ? $this->getAmount(trim($post_array['tractor/trailer_surcharge_cost'])) : '';
		$insert_data['tractor/trailer_surcharge_premium'] = isset($post_array['tractor/trailer_surcharge_premium']) ? $this->getAmount(trim($post_array['tractor/trailer_surcharge_premium'])) : '';
		$insert_data['debris_removal_limit'] = trim(isset($post_array['debris_removal_limit']) ? $post_array['debris_removal_limit'] : '');
		$insert_data['debris_removal_cost'] = isset($post_array['debris_removal_cost']) ? $this->getAmount(trim($post_array['debris_removal_cost'])) : '';
		$insert_data['debris_removal_premium'] =isset($post_array['debris_removal_premium']) ? $this->getAmount(trim($post_array['debris_removal_premium'])) : '';
		$insert_data['towing_labor_storage'] = trim(isset($post_array['towing_labor_storage']) ? $post_array['towing_labor_storage'] :'');
		$insert_data['towing_labor_storage_cost'] = isset($post_array['towing_labor_storage_cost']) ? $this->getAmount(trim($post_array['towing_labor_storage_cost'])) : '';
		$insert_data['towing_labor_storage_premium'] = isset($post_array['towing_labor_storage_premium']) ? $this->getAmount(trim($post_array['towing_labor_storage_premium'])) : '';
		
		$insert_data['number_of_tractor'] = trim(isset($post_array['number_of_tractor']) ? $post_array['number_of_tractor'] :'');
		$insert_data['number_of_trailer'] = trim(isset($post_array['number_of_trailer']) ? $post_array['number_of_trailer'] : '');
		$insert_data['total_tractor_limit'] = trim(isset($post_array['total_tractor_limit']) ? $post_array['total_tractor_limit'] : '');
		$insert_data['total_trailer_limit'] = trim(isset($post_array['total_trailer_limit']) ? $post_array['total_trailer_limit'] : '');
		$insert_data['total_truck_limit'] = isset($post_array['total_truck_limit']) ? $post_array['total_truck_limit'] : '';
		$insert_data['total_tractor_premium'] = isset($post_array['total_tractor_premium']) ? $this->getAmount(trim($post_array['total_tractor_premium'])) : '';
		$insert_data['total_trailer_premium'] = isset($post_array['total_trailer_premium']) ? $this->getAmount(trim($post_array['total_trailer_premium'])) : '';
		$insert_data['total_truck_premium'] = isset($post_array['total_truck_premium']) ? $this->getAmount(trim($post_array['total_truck_premium'])) : '';
		$insert_data['total_pd_premium'] = isset($post_array['total_pd_premium']) ? $this->getAmount(trim($post_array['total_pd_premium'])) : '';
		$insert_data['pd_policy_fee'] = isset($post_array['pd_policy_fee']) ? $this->getAmount(trim($post_array['pd_policy_fee'])) : '';
		$insert_data['pd_sla_tax'] = isset($post_array['pd_sla_tax']) ? $this->getAmount(trim($post_array['pd_sla_tax'])) : '';
		$insert_data['carrier'] = trim(isset($post_array['carriers']) ? $post_array['carriers'] : '');
		$insert_data['required_for_firm_quote'] = trim(isset($post_array['required_for_firm_quote']) ? $post_array['required_for_firm_quote'] : '');
		$insert_data['comments'] = trim(isset($post_array['comments']) ? $post_array['comments'] : '');
		$insert_data['status'] = 'Pending';
		if($post_array['bundleid'] != '')
			{
				$where_array = array('rq_rqf_quote_bundle.id' => $post_array['bundleid']);
				$this->db->select('rq_rqf_quote_bundle.pd_id');
				$this->db->from('rq_rqf_quote_bundle');
				$this->db->where($where_array);

				$query = $this->db->get();
				$cargo = $query->result();
				$pd_id = $cargo[0]->pd_id;
			
				$action = ($pd_id != '0' ) ? 'updated' : 'inserted';
			} else 
			{
				$action = 'inserted';
			}
		if($action == 'inserted') {
			$insert_data['created_by'] = $this->session->userdata('admin_id');
			$insert_data['created_date'] = date("Y-m-d");
			$this->db->insert('rq_ratesheet_damage_ratesheet', $insert_data);
			$result_id_ratesheet = $this->db->insert_id();
			if($post_array['bundleid'] == '')
			{
				$insert_bundle_array =  array();
				$insert_bundle_array['quote_id'] = $post_array['quote_id'];
				$insert_bundle_array['pd_id'] = trim($result_id_ratesheet);
				if($this->session->userdata('member_id')){		
					$insert_bundle_array['created_by'] = $this->session->userdata('member_id');
				} elseif($this->session->userdata('underwriter_id')){
					$insert_bundle_array['created_by'] = $this->session->userdata('underwriter_id');
				}
				$insert_bundle_array['creation_date'] = date("Y-m-d");
				$insert_bundle_array['status'] = 'Draft';
				$insert_bundle_array['producer_email'] = $post_array['producer_email'];
				$insert_bundle_array['quote_ref'] = $this->generate_quote_code();;
				$this->db->insert('rq_rqf_quote_bundle', $insert_bundle_array);
				$result_bid_ratesheet = $this->db->insert_id();
				if($post_array['quote_id']!=''){
					$this->db->query("update rq_rqf_quote set bundle_id = ".$result_bid_ratesheet.",bundle_status = 'Draft' where ((quote_id=".$post_array['quote_id'].") and (bundle_id = '0'))");
				}
			} 
			else {
				$this->db->query("update rq_rqf_quote_bundle set pd_id = ".$result_id_ratesheet." where id=".$post_array['bundleid']);
				$result_bid_ratesheet = $post_array['bundleid'];
			}
		}  else
		{
			$this->db->update('rq_ratesheet_damage_ratesheet',$insert_data, array('id'=>$pd_id));
			$result_id_ratesheet =  $pd_id;
			$result_bid_ratesheet = $post_array['bundleid'];
		}
		$result =  $result_bid_ratesheet;
		
		$this->insertVehicleData($result, $this->input->post());
		return $result;
	}
	function insertVehicleData($pd_id, $postArray = array()){
		
		$tracktorDelIds = array();
		$trailerDelIds = array();
		$truckDelIds = array();
		
		/***Tractor data***/
		if($this->input->post('number_of_tractor')){
			for($i=1;$i<=$this->input->post('number_of_tractor');$i++){
				$tblTractor = array(
					'vh_pd' => $this->input->post('tractor_val'.$i),
					'vh_pd_ded' => $this->input->post('tractor_ded'.$i),
					'vh_rate' => $this->input->post('tractor_rate'.$i),
					'vh_frate' => $this->input->post('tractor_flat_rate'.$i),
					'vh_primium' => $this->input->post('tractor_premium'.$i),
					'vh_type' => 'TRACTOR',
					'pd_id' => $pd_id,					
				);	
				if($this->input->post('tractor_id'.$i)){
					$this->db->update('rq_ratesheet_vehicle', $tblTractor, array('id' => $this->input->post('tractor_id'.$i)));
					$tracktorDelIds[] = $this->input->post('tractor_id'.$i);
				}else{
					$this->db->insert('rq_ratesheet_vehicle', $tblTractor);
					$tracktorDelIds[] = $this->db->insert_id();
				}
			}
		}
		
		//print_r($tracktorDelIds);
		$this->db->select('id');
		$this->db->where('vh_type', 'TRACTOR');
		$this->db->where('pd_id', $pd_id);
		$query = $this->db->get('rq_ratesheet_vehicle');
		$tractors = $query->result();
		foreach($tractors as $tractor){
			if(!in_array($tractor->id, $tracktorDelIds))	{
				$this->db->update('rq_ratesheet_vehicle', array('is_delete' => 1), array('id' => $tractor->id));
			}
		}
		/***Truck data***/
		if($this->input->post('no_of_truck')){
			for($i=1;$i<=$this->input->post('no_of_truck');$i++){
				$tblTruck = array(
					'vh_pd' => $this->input->post('truck_val'.$i),
					'vh_pd_ded' => $this->input->post('truck_ded'.$i),
					'vh_rate' => $this->input->post('truck_rate'.$i),
					'vh_frate' => $this->input->post('truck_flat_rate'.$i),
					'vh_primium' => $this->input->post('truck_premium'.$i),
					'vh_type' => 'TRUCK',
					'pd_id' => $pd_id,					
				);					
				
				if($this->input->post('truck_id'.$i)){
					$this->db->update('rq_ratesheet_vehicle', $tblTruck, array('id' => $this->input->post('truck_id'.$i)));
					$truckDelIds[] = $this->input->post('truck_id'.$i);
				}else{
					$this->db->insert('rq_ratesheet_vehicle', $tblTruck);
					$truckDelIds[] = $this->db->insert_id();
				}
				
			}
		}
		
		$this->db->select('id');
		$this->db->where('vh_type', 'TRUCK');
		$this->db->where('pd_id', $pd_id);
		$query = $this->db->get('rq_ratesheet_vehicle');
		$trucks = $query->result();
		foreach($trucks as $truck){
			if(!in_array($truck->id, $truckDelIds))	{
				$this->db->update('rq_ratesheet_vehicle', array('is_delete' => 1), array('id' => $truck->id));
			}
		}
		
		/***Trailer data***/
		if($this->input->post('number_of_trailer')){
			for($i=1;$i<=$this->input->post('number_of_trailer');$i++){
				$tblTrailer = array(
					'vh_pd' => $this->input->post('trailer_val'.$i),
					'vh_pd_ded' => $this->input->post('trailer_ded'.$i),
					'vh_rate' => $this->input->post('trailer_rate'.$i),
					'vh_frate' => $this->input->post('trailer_flat_rate'.$i),
					'vh_primium' => $this->input->post('trailer_premium'.$i),
					'vh_type' => 'TRAILER',
					'pd_id' => $pd_id,					
				);			
				
				if($this->input->post('trailer_id'.$i)){
					$this->db->update('rq_ratesheet_vehicle', $tblTrailer, array('id' => $this->input->post('trailer_id'.$i)));
					$trailerDelIds[] = $this->input->post('trailer_id'.$i);
				}else{
					$this->db->insert('rq_ratesheet_vehicle', $tblTrailer);
					$trailerDelIds[] = $this->db->insert_id();
				}
			}
		}		
		
		$this->db->select('id');
		$this->db->where('vh_type', 'TRAILER');
		$this->db->where('pd_id', $pd_id);
		$query = $this->db->get('rq_ratesheet_vehicle');
		$trailers = $query->result();
		foreach($trailers as $trailer){
			if(!in_array($trailer->id, $trailerDelIds))	{
				$this->db->update('rq_ratesheet_vehicle', array('is_delete' => 1), array('id' => $trailer->id));
			}
		}
	}

	/*function show_ratesheet_data() {
		/*$this->load->db();
		$query = $this->db->query("SELECT * from rq_ratesheet");
  		return $query->result();*/
		/*echo 'model';
   }*/
   
   /* function getAll() {
        echo 'md';

    }*/
   
   

    function get_cargo($id) {
		$this->db->where('quote_id',$id);
        $q = $this->db->get('rq_ratesheet');
		//echo $this->db->last_query();
        if($q->num_rows() > 0){
			return $q->result();
			
		}
    }   

    function get_pd($id) {
		$this->db->where('quote_id',$id);
        $q = $this->db->get('rq_ratesheet_damage_ratesheet');
		//echo $this->db->last_query();
        if($q->num_rows() > 0){
			return $q->result();
			
		}
    }    
   
     function get_liability($id) {
		$this->db->where('quote_id',$id);
        $q = $this->db->get('rq_ratesheet_liability');
		//echo $this->db->last_query();
        if($q->num_rows() > 0){
			return $q->result();
			
		}
    }   
   
 
 	function get_ratesheet_vehicle($id = '',$vehicle_type='') {
        //  $created_by=   $this->session->userdata['flexi_auth'] ['user_id']; 
        //$where_array = array('rq_rqf_quote_vehicle.quote_id' => $id);
		$this->db->where('pd_id',$id);
		$this->db->where('is_delete', 0);
		if($vehicle_type != '')
		{
			//$where_array = array('rq_rqf_quote_vehicle.vehicle_type' => $vehicle_type);
			$this->db->where('vh_type',$vehicle_type);
		}

        $this->db->select('*');
        $this->db->from('rq_ratesheet_vehicle');
       

        $query = $this->db->get();
        $this->data['vehicle'] = $query->result();
        return $this->data['vehicle'];
    }
   
	/* function ratesheet_view(){
	/*	$data = array();
		$this->load->database();
	
		$this->db->select('*');
		$this->db->from('rq_ratesheet');
		$query = $this->db->get();
		
		->select('*')
		 ->from('rq_rqf_quote')
		 ->where($array);
	
		return $query->row();	
		echo ratesheet_view();*/
		
		
 /*$sql = "SELECT * FROM rq_ratesheet";
            $sql=$this->db->query($sql);
			
			while($row = mysql_fetch_array($sql))
			  {
			  echo "<tr>";
			  echo "<td>" . $row['broker'] . "</td>";
			  echo "<td>" . $row['insured'] . "</td>";
			  echo "</tr>";
			  }
			
			

		
		
		
		
	}  */ 
  
   
/* function ratesheet_view2($a){
	$this->load->database();
	$query = $this->db->query("SELECT * FROM rq_ratesheet");
	return $query->result();    
} */ 
   
   
   
   

	/*function insert_ratesheet_physical_damage($post_array,$action) {
		echo 'a';
		print_r($this->input->post());
	
	}*/
}
















/* End of file quote_model.php */
/* Location: ./application/models/quote_model.php */