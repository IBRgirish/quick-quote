<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->model('order_model');
		$this->load->helper('my_helper');
		$this->load->library('pagination');  
		$this->load->helper('date');
	}
	
	public function index()
	{
		$limit = 25;
		$offset=0;
		$total = $this->order_model->count_order(); 
		$order = $this->load->order_model->get_user_order_list($limit,$offset);
		$config['base_url'] = base_url('administration/order');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		$data['listing'] = $order;
		$data['total'] = $total;
		$this->pagination->initialize($config);
		$this->load->view('administration/order',$data);
	}
	public function page($offset='')
    {
		$limit = 25;
		$total = $this->order_model->count_order(); 
		$offset = isset($offset)&& !empty($offset) ? $offset : '0';
		$order = $this->load->order_model->get_user_order_list($limit,$offset);
		$config['base_url'] = base_url('administration/order');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		$data['total'] = $total;
		$data['listing'] = $order;
		$this->pagination->initialize($config);
		$this->load->view('administration/order',$data);
      
    }
		
	public function order_detail($order_id)
	{
		$order = $this->load->order_model->get_order_info($order_id);
		//echo "<pre>"; print_r($order); die;
		$data['listing'] = $order;
		$this->load->view('administration/order_details',$data);
	}
	
	function details($id){
		 $members = $this->member_model->get_members_info($id);
         $data['member'] = $members;
		 $this->load->view('administration/members_details', $data);
	}
	
	 function delete($id)
	 {
			$this->member_model->delete_members($id);
			redirect($this->config->item('prevous_url'));
			exit;
	 }
	 
	  function member_status($id,$status)
	 {
			$this->member_model->member_status_update($id,$status);
			redirect($this->config->item('prevous_url'));
			exit;
	 }
}	
/* End of file test_post.php */
/* Location: ./application/controllers/test_post.php */