<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Getunderwriter  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/
	  <method_name> 
 	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		//$this->require_admin_login();
		$this->load->helper('my_helper');
		$this->load->model('catlog_model');
		$this->load->library('pagination');  
		$this->load->helper('date');
		$this->load->helper('url');
		//$this->load->library('customclass');
	}
	
	public function index()
	{
		$this->db->where('is_deleted','FALSE');
		$this->db->where('status','ACTIVE');
		$query = $this->db->get('rq_underwriter');
		$rows = $query->result();
		echo json_encode($rows);
		
	}
	
	
}