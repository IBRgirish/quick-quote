<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catlog  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/
	  <method_name> 
 	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->helper('my_helper');
		$this->load->model('catlog_model');
		$this->load->library('pagination');  
		$this->load->helper('date');
		$this->load->helper('url');
		//$this->load->library('customclass');
	}
	
			/**
	* Upload file using CI
	* @$data return uploaded file name
	* @$error return errors during file uploading
	*/
	public function do_upload($file_types='*',$name, $path, $file_name='') {
        $config['upload_path'] = 'uploads/'.$path;
        $config['allowed_types'] = $file_types;
    
		if($file_name){
			$config['file_name'] = $file_name;
		}
      
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($name)) {
            $error = array( 'error' => $this->upload->display_errors() );
            
            return $error;
        } else {
            $data = array( 'upload_data' => $this->upload->data() );
            return $data['upload_data'];
           
        }
    }
	
	
	public function browse($type, $type1='')
	{
		$limit = 25;
		$total = $this->catlog_model->count_catlog($type); 
		$data['admins'] = $this->catlog_model->list_catlog($limit, 0, $type, '');
		$data['object'] = $this;
		$data['type'] = $type1;
		$config['base_url'] = base_url('administration/catlog');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		$data['total'] = $total;
		$this->pagination->initialize($config);
		$this->load->view('administration/catlog_main', $data);
	}
	
	public function index($type='')
	{   
		$this->browse('trailer_type', $type);	
	}
	public function ajax_list_catlog1($type)
	{ 
		
		//$array = array('rq_rqf_quote_catlog_new.catlog_type' => $type);
		$this->datatables->select("rq_rqf_quote_catlog_new.id,'Carrier', rq_rqf_quote_catlog_new.catlog_name,rq_rqf_quote_catlog_new.state,rq_rqf_quote_catlog_new.slatax,rq_rqf_quote_catlog_new.unique_cargo_policy_fee, rq_rqf_quote_catlog_new.unique_pd_policy_fee,rq_rqf_quote_catlog_new.pd_cargo_policy_fee,rq_rqf_quote_catlog_new.association_fee, rq_rqf_quote_catlog_new.status", FALSE);
		$this->datatables->from('rq_rqf_quote_catlog_new');
		//$this->datatables->where($array);
		//$this->datatables->join('rq_rqf_quote_catlog_type', 'rq_rqf_quote_catlog_new.catlog_type = rq_rqf_quote_catlog_type.id', 'inner');
        echo $this->datatables->generate();
		
		
	}
	public function ajax_list_catlog($type)
	{ 
	  if($type==21){
		$array = array('rq_rqf_quote_catlog_type.id' => $type);
		$this->datatables
         ->select("rq_rqf_quote_catlogs.id, rq_rqf_quote_catlogs.catlog_type, rq_rqf_quote_catlogs.catlog_value, rq_rqf_quote_catlogs.status", FALSE)
		 ->join('rq_rqf_quote_catlog_type', 'rq_rqf_quote_catlog_type.catlog_type = rq_rqf_quote_catlogs.catlog_type')
		 ->from('rq_rqf_quote_catlogs')
		 ->where($array);
         echo $this->datatables->generate();
		
		}else if($type==26){	
		   
	   
	   $array = array('a.catlog_type' => $type,'a.is_delete'=>0);
		$this->datatables
         ->select("a.id_pip_value,a.catlog_type,g.state_name,d.carrier_value,a.value,a.status")
		 ->join('pip_carrier_value b', 'b.id_pip_value=a.id_pip_value')
		 ->join('pip_carrier c', 'c.id_pip_carrier=b.id_pip_carrier')
		 ->join('carriers d', 'd.carrier_id=c.id_carrier')
		 ->join('pip_state_carrier e', 'e.id_pip_carrier=c.id_pip_carrier')
		 ->join('pip_state f', 'f.id_pip_state=e.id_pip_state')
		 ->join('real_state g', 'g.id=f.id_state')
		 ->from('pip_value a')
		 ->where($array);
        echo $this->datatables->generate();
						
	
			
		}else if($type==27){
			
			  
	  $array = array('a.catlog_type' => $type,'a.is_delete'=>0);
		$this->datatables
         ->select("a.id_pip_value,a.catlog_type,g.state_name,d.carrier_value,a.value,a.status")
		 ->join('pip_carrier_value b', 'b.id_pip_value=a.id_pip_value')
		 ->join('pip_carrier c', 'c.id_pip_carrier=b.id_pip_carrier')
		 ->join('carriers d', 'd.carrier_id=c.id_carrier')
		 ->join('pip_state_carrier e', 'e.id_pip_carrier=c.id_pip_carrier')
		 ->join('pip_state f', 'f.id_pip_state=e.id_pip_state')
		 ->join('real_state g', 'g.id=f.id_state')
		 ->from('pip_value a')
		 ->where($array);
        echo $this->datatables->generate();
			
		}else if($type==28){
			
			  
	   $array = array('a.catlog_type' => $type,'a.is_delete'=>0);
		$this->datatables
         ->select("a.id_pip_value,a.catlog_type,g.state_name,d.carrier_value,a.value,a.status")
		 ->join('pip_carrier_value b', 'b.id_pip_value=a.id_pip_value')
		 ->join('pip_carrier c', 'c.id_pip_carrier=b.id_pip_carrier')
		 ->join('carriers d', 'd.carrier_id=c.id_carrier')
		 ->join('pip_state_carrier e', 'e.id_pip_carrier=c.id_pip_carrier')
		 ->join('pip_state f', 'f.id_pip_state=e.id_pip_state')
		 ->join('real_state g', 'g.id=f.id_state')
		 ->from('pip_value a')
		 ->where($array);
        echo $this->datatables->generate();
			
		}
		else if($type==29){
			
			  
	    $array = array('a.catlog_type' => $type,'a.is_delete'=>0);
		$this->datatables
         ->select("a.id_slatax,a.catlog_type,g.state_name,d.carrier_value,a.tax,a.status")		
		 ->join('carriers d', 'd.carrier_id=a.id_carrier')		 
		 ->join('real_state g', 'g.id=a.id_state')
		 ->from('slatax a')
		 ->where($array);
        echo $this->datatables->generate();
			
		}else if($type==30){
		$array = array('rq_rqf_quote_catlogs.catlog_type' => $type);
		$this->datatables
         ->select("rq_rqf_quote_catlogs.id, rq_rqf_quote_catlogs.catlog_type, rq_rqf_quote_catlogs.catlog_value, rq_rqf_quote_catlogs.status", FALSE)	
		 ->from('rq_rqf_quote_catlogs')
		 ->where($array);
        echo $this->datatables->generate();
		
		
		}else if($type==31){
		$array = array('rq_rqf_quote_catlogs.catlog_type' => $type);
		$this->datatables
         ->select("rq_rqf_quote_catlogs.id, rq_rqf_quote_catlogs.catlog_type, rq_rqf_quote_catlogs.catlog_value, rq_rqf_quote_catlogs.status", FALSE)		 
		 ->from('rq_rqf_quote_catlogs')
		 ->where($array);
        echo $this->datatables->generate();
		
		
		}
		else if($type==32){
		$array = array('rq_rqf_quote_catlogs.catlog_type' => $type);
		$this->datatables
         ->select("rq_rqf_quote_catlogs.id, rq_rqf_quote_catlogs.catlog_type, rq_rqf_quote_catlogs.catlog_value, rq_rqf_quote_catlogs.status", FALSE)		 
		 ->from('rq_rqf_quote_catlogs')
		 ->where($array);
        echo $this->datatables->generate();	
		
		}
		else if($type==33){
		$array = array('rq_rqf_quote_catlogs.catlog_type' => $type);
		$this->datatables
         ->select("rq_rqf_quote_catlogs.id, rq_rqf_quote_catlogs.catlog_type, rq_rqf_quote_catlogs.catlog_value, rq_rqf_quote_catlogs.status", FALSE)		 
		 ->from('rq_rqf_quote_catlogs')
		 ->where($array);
        echo $this->datatables->generate();		
		
		}else if($type==39||$type==44||$type==45||$type==46||$type==50){
		$array = array('rq_rqf_quote_catlogs.catlog_type' => $type);
		$this->datatables
         ->select("rq_rqf_quote_catlogs.id, rq_rqf_quote_catlogs.catlog_type, rq_rqf_quote_catlogs.catlog_value,rq_rqf_quote_catlogs.module_name, rq_rqf_quote_catlogs.status", FALSE)		 
		 ->from('rq_rqf_quote_catlogs')
		 ->where($array);
        echo $this->datatables->generate();
		
		
		}else{
			
		$array = array('rq_rqf_quote_catlogs.catlog_type' => $type);
		$this->datatables
         ->select("rq_rqf_quote_catlogs.id, rq_rqf_quote_catlogs.catlog_type, rq_rqf_quote_catlogs.catlog_value, rq_rqf_quote_catlogs.status", FALSE)		 
		 ->from('rq_rqf_quote_catlogs')
		 ->where($array);
        echo $this->datatables->generate();
		
	   }

	}
	
	public function catlog_adds(){		
	    $type = $this->input->post('cat_f');
		if($type == 1){
			$catlog_value= $this->input->post('catlog_value');
		  $catlog['catlog_value'] =implode(',',$catlog_value);
		} else {
		  $catlog['catlog_value'] = $this->input->post('catlog_value');
			}
		
		if($type == 39){
		$catlog['module_name'] = $this->input->post('catlog_value_2');	
			}
		 if($type == 44 || $type ==45 || $type ==46 || $type ==50){	
		$file_types=$file_types = $this->config->item('image_file_types');		 
		$file_copy = $this->do_upload($file_types,'pic_name', 'file');			
		$catlog['module_name'] = isset($file_copy['file_name']) ? str_replace(' ', '_', $file_copy['file_name']) : NULL;		
			}
		$ids = $this->input->post('catlogId');
		$catlog_type=$this->input->post('catlog_type');
		
		$catlog['catlog_type']=isset($type)?$type:$catlog_type;
		$id=isset($ids)?$ids:'';
		
	    $this->catlog_model->insert_update_catlog($catlog,$id);
		echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$catlog['catlog_type']).'";
				</script>';
		
		}
	
	//ADD
	public function catlog_add1()
	{
		$data['header'] = array('title'=>'Add Catlog');
		
		$this->form_validation->set_rules('cat_f', 'Type', 'required');
		
		
		if ($this->form_validation->run() === TRUE)
		{
			//echo $catlog['catlog_type'] = implode(" ",$this->input->post('cat_f'));
			$catlog['catlog_type'] = $this->input->post('cat_f');;
			$catlog['catlog_name'] = $this->input->post('catlog_name');
			$catlog['state'] = $this->input->post('state');
			$catlog['slatax'] = $this->input->post('slatax');
			$catlog['unique_cargo_policy_fee'] = $this->input->post('unique_cargo_policy_fee');
			$catlog['unique_pd_policy_fee'] = $this->input->post('unique_pd_policy_fee');
			$catlog['pd_cargo_policy_fee'] = $this->input->post('pd_cargo_policy_fee');
			$catlog['association_fee'] = $this->input->post('association_fee');
			$catlog['date_created'] = date("Y-m-d");
			$catlog['status'] = 1;
			$this->catlog_model->insert_update_carrier_catlog($catlog);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$catlog['catlog_type']).'";
				</script>';
		}
		else
		{
			$this->load->view('administration/catlog_add',$data);
		}
	}
	public function catlog_add()
	{
		$data['header'] = array('title'=>'Add Catlog');
		$this->form_validation->set_rules('cat_f', 'Type', 'required');
		$this->form_validation->set_rules('catlog_value', 'Value', 'required');
		
		
		if ($this->form_validation->run() === TRUE)
		{
			
			$catlog['catlog_type'] = $this->input->post('cat_f');
			$catlog['catlog_value'] = $this->input->post('catlog_value');
			$catlog['status'] = 1;
			$this->catlog_model->insert_update_catlog($catlog);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog/'.$catlog['catlog_type'].'').'";
				</script>';
		}
		else
		{
			
			$this->load->view('administration/catlog_add',$data);
		}
	}
	
	
	
		public function catlog_adding($id='')
	{
		
		$data['header'] = array('title'=>'Add Catlog');		
		$data['required'] = ' Required';	
		$data['carrier_id']=$this->input->post('carrier_id');
		$catlog_value=$this->input->post('catlog_value');
		$data['carrier_state']=$this->input->post('carrier_state');
		$data['mandatory_value']=$this->input->post('mandatory_value');		
		$data['cat_type']=$this->input->post('cat_f');
		
		if (!empty($catlog_value))
		{
		
		$this->catlog_model->insert_update_carrier_catlogs($id);
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$data['cat_type']).'";
			  </script>';
		}else{
		if (!empty($id)){
		$data['catlog']=$this->catlog_model->get_catlog_infos($id);
		$this->load->view('administration/catlog_edit',$data);
		}else{
		 $this->load->view('administration/catlog_add',$data);
		 }
		}
	
	}
	
	
		public function catlog_slatax_adding($id='')
	{
		
		$data['header'] = array('title'=>'Add Catlog');		
		$data['required'] = ' Required';	
		$data['carrier_id']=$this->input->post('carrier_id');
		$catlog_value=$this->input->post('catlog_value');
		$data['carrier_state']=$this->input->post('carrier_state');
		$data['mandatory_value']=$this->input->post('mandatory_value');		
		$data['cat_type']=$this->input->post('cat_f');
		
		if (!empty($catlog_value))
		{
		
		$this->catlog_model->insert_update_slatax_catlogs($id);
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$data['cat_type']).'";
				</script>';
		}else{
			if (!empty($id)){
			 $data['catlog']=$this->catlog_model->get_slatax_infos($id);
		     $this->load->view('administration/catlog_edit',$data);
			}else{
			 $this->load->view('administration/catlog_add',$data);
			}
		}
	
	}
	
	
	//Edit
	public function catlog_edit1($id='')
	{
	
		$data['header'] = array('title'=>'Edit Catlog');
		$data['catlog']=$this->catlog_model->get_catlog_info1($id);
		$this->form_validation->set_rules('cat_f', 'Type', 'required');

		if ($this->form_validation->run() === TRUE)
		{
			$id = $this->input->post('catlogId');
			
			//$catlog['catlog_type'] = implode(" ", $this->input->post('cat_f'));
			$catlog['catlog_type'] = $this->input->post('cat_f');;
			$catlog['catlog_name'] = $this->input->post('catlog_name');
			$catlog['state'] = $this->input->post('state');
			$catlog['slatax'] = $this->input->post('slatax');
			$catlog['unique_cargo_policy_fee'] = $this->input->post('unique_cargo_policy_fee');
			$catlog['unique_pd_policy_fee'] = $this->input->post('unique_pd_policy_fee');
			$catlog['pd_cargo_policy_fee'] = $this->input->post('pd_cargo_policy_fee');
			$catlog['association_fee'] = $this->input->post('association_fee');
			$catlog['date_modified'] = time();
			$catlog['status'] = $this->input->post('status');
			
			$this->catlog_model->insert_update_carrier_catlog($catlog,$id);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$catlog['catlog_type']).'";
				</script>';
		}
		else
		{
			$this->load->view('administration/catlog_edit1',$data);
		}
	}
	public function catlog_edit($id='')
	{
			
		$cat_type= $this->input->get('cat_type');
		
		$data['header'] = array('title'=>'Edit Catlog');
		if($cat_type==26){
		$data['catlog']=$this->catlog_model->get_catlog_infos($id);
		
		}else if($cat_type==27){
		$data['catlog']=$this->catlog_model->get_catlog_infos($id);
		
		}else if($cat_type==28){
		$data['catlog']=$this->catlog_model->get_catlog_infos($id);
		
		}else if($cat_type==29){
		$data['catlog']=$this->catlog_model->get_slatax_infos($id);
		
		}else{
		$data['catlog']=$this->catlog_model->get_catlog_info($id);
		}
		$this->form_validation->set_rules('catlog_type', 'Type', 'required');
		$this->form_validation->set_rules('catlog_value', 'Value', 'required');
		
		$id = $this->input->post('catlogId');
		
		if ($this->form_validation->run() === TRUE)
		{
			$catlog['catlog_type'] = $this->input->post('catlog_type');
			$catlog['catlog_value'] = $this->input->post('catlog_value');
			
			$catlog['status'] = $this->input->post('status');
			
			$this->catlog_model->insert_update_catlog($catlog,$id);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$catlog['catlog_type']).'";
				</script>';
		}
		else
		{
			$this->load->view('administration/catlog_edit',$data);
		}
	}
	
	//DELETE
	public function ajax_catlog_delete1()
	{
		$this->db->where(array('id'=>$this->input->post('id')));
		$query=$this->db->delete('rq_rqf_quote_catlog_new'); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
	public function ajax_catlog_delete()
	{   
	    $cat_type=$this->input->post('cat_type');
		 
		if($cat_type==26){
		$this->db->where(array('id_pip_value'=>$this->input->post('id')));
		$value=array('is_delete'=>1);
		$query=$this->db->update('pip_value',$value); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}	
		}else if($cat_type==27){
		$this->db->where(array('id_pip_value'=>$this->input->post('id')));
		$value=array('is_delete'=>1);
		$query=$this->db->update('pip_value',$value); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}	
		}else if($cat_type==28){
		$this->db->where(array('id_pip_value'=>$this->input->post('id')));
		$value=array('is_delete'=>1);
		$query=$this->db->update('pip_value',$value); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}	
		}else if($cat_type==29){
		$this->db->where(array('id_slatax'=>$this->input->post('id')));
		$value=array('is_delete'=>1);
		$query=$this->db->update('slatax',$value); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}	
		}else{
			
		$this->db->where(array('id'=>$this->input->post('id')));
		$query=$this->db->delete('rq_rqf_quote_catlogs'); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	  }
		
	}
}