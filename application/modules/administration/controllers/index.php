<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends RQ_Controller {


	public function __construct()
	{
		parent::__construct();

		$this->load->model('admin_model');
	}
	
	public function index()
	{
		//$this->file_xls();
		//echo 'index called'; die;
		if($this->session->userdata('admin_id'))
		{
			$data['test'] = '';
			$this->load->view('administration/index',$data);
		
		}else{
			$this->form_validation->set_rules('username', 'Username', 'required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() === FALSE)
        {
			$this->load->view('administration/login');
		  }else{
				// if user is valid
				$valid_user = $this->admin_model->validate_admin();
				
				if($valid_user) 
				{
					 $data = array(
								'admin_username' => $valid_user->email,
								'admin_name' => $valid_user->name,
								'admin_id' => $valid_user->id,
								'admin_type' => $valid_user->admin_type
					 );
					 
					 $this->session->set_userdata($data);
					 $this->session->set_flashdata('success', 'You have successfully logged into your account');
					 $red_url = base_url('admin');
					 redirect($red_url);
				}
				
				else 
				{
					 $this->session->set_flashdata('error', 'Information submitted is incorrect.');
					 $red_url = base_url('admin');
					 redirect($red_url);
				}
			  }
		}
	}
	
	public function logout()
	{
        $this->session->sess_destroy();
		  echo '<script language="javascript">window.location.href=\''.base_url('administration/index').'\';</script>';
	}		public function update_agency_phone() { 		$this->db->select('id,phone_number,phone_area');				$this->db->from('rq_members');				$query = $this->db->get();				$res = $query->result();				print_r($res);				foreach($res as $key => $v){					$phone = str_replace("-", "", $v->phone_number);										$areacode = substr($phone, 0, 3);					$prefix   = substr($phone, 3, 3);					$number   = substr($phone, 6, 4);					$ext = substr($phone, 10, 6);					echo '<br>';					echo $data[$v->id]['phone_number'] = "$areacode-$prefix-$number-$ext";														}								foreach($data as $k => $d){					$this->db->update('rq_members',$d, array('id'=>$k));				}	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */