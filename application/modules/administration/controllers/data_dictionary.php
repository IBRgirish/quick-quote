<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_dictionary  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/
	  <method_name> 
 	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->helper('my_helper');
		$this->load->model('catlog_model');
		$this->load->model('data_model');
		$this->load->library('pagination');  
		$this->load->helper('date');
		$this->load->helper('url');
		//$this->load->library('customclass');
	}
	
			/**
	* Upload file using CI
	* @$data return uploaded file name
	* @$error return errors during file uploading
	*/
	public function do_upload($file_types='*',$name, $path, $file_name='') {
        $config['upload_path'] = 'uploads/'.$path;
        $config['allowed_types'] = $file_types;
    
		if($file_name){
			$config['file_name'] = $file_name;
		}
      
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($name)) {
            $error = array( 'error' => $this->upload->display_errors() );
            
            return $error;
        } else {
            $data = array( 'upload_data' => $this->upload->data() );
            return $data['upload_data'];
           
        }
    }
	
	
	public function browse($type, $type1='')
	{
		$limit = 25;
		$total = $this->catlog_model->count_catlog($type); 
		$data['admins'] = $this->catlog_model->list_catlog($limit, 0, $type, '');
		$data['object'] = $this;
		$data['type'] = $type1;
		$config['base_url'] = base_url('administration/catlog');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		$data['total'] = $total;
		$this->pagination->initialize($config);
		$this->load->view('administration/dictionary_main', $data);
	}
	
	public function index($type='')
	{   
		$this->browse('trailer_type', $type);	
	}
	public function ajax_list_dictionary()
	{ 		
		$this->datatables->select('rq_rqf_data_dictionary.id,rq_rqf_data_dictionary.field_name,rq_rqf_data_dictionary.default_value,rq_rqf_data_dictionary.mandatory,rq_rqf_data_dictionary.prefer,rq_rqf_data_dictionary.title,rq_rqf_data_dictionary.min_value,rq_rqf_data_dictionary.max_value,rq_rqf_data_dictionary.show,rq_rqf_data_dictionary.placeholder,rq_rqf_data_dictionary.disable,rq_rqf_data_dictionary.app,rq_rqf_data_dictionary.coverage,rq_rqf_data_dictionary.color,rq_rqf_data_dictionary.status');
		$this->datatables->where(array('is_delete'=>0));
		$this->datatables->from('rq_rqf_data_dictionary');
	    echo $this->datatables->generate();
		
		
	}

	
	
	
	//ADD
	
	public function data_add($id='')
	{  
	  if(!empty($id)){
		    $data['header'] = array('title'=>'Edit Data');	
			$data['datas'] = $this->data_model->data($id);	
		
	    }else{
			$data['header'] = array('title'=>'Add Data');	
		}
		
		
		
			if(!empty($_POST['submit'])){
				
			$catlog['field_name'] = $this->input->post('field_name');
			$catlog['default_value'] = $this->input->post('default_value');
			$catlog['mandatory'] = $this->input->post('mandatory');
			$catlog['prefer'] = $this->input->post('prefer');
			$catlog['title'] = $this->input->post('tooltip');
			$catlog['min_value'] = ($this->input->post('min_value')) ? $this->input->post('min_value') : NULL;
			$catlog['max_value'] = ($this->input->post('max_value')) ? $this->input->post('max_value') : NULL;
			$catlog['placeholder'] = $this->input->post('placeholder');	
			$catlog['show'] = $this->input->post('show');
			$catlog['disable'] = $this->input->post('disable');	
			$catlog['app'] = $this->input->post('application');
			$catlog['coverage'] = $this->input->post('coverage');	
			$catlog['color'] = $this->input->post('color');
					
			$catlog['status'] = 1;
			if(empty($id)){
			$this->data_model->_insert($catlog,'rq_rqf_data_dictionary');
			}else{
			$this->data_model->_update($id,$catlog,'rq_rqf_data_dictionary');
			}
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/data_dictionary/').'";
				</script>';
		
			}
			$this->load->view('administration/data_add',$data);
		
	}
	
	
	
		public function catlog_adding($id='')
	{
		
		$data['header'] = array('title'=>'Add Catlog');		
		$data['required'] = ' Required';	
		$data['carrier_id']=$this->input->post('carrier_id');
		$catlog_value=$this->input->post('catlog_value');
		$data['carrier_state']=$this->input->post('carrier_state');
		$data['mandatory_value']=$this->input->post('mandatory_value');		
		$data['cat_type']=$this->input->post('cat_f');
		
		if (!empty($catlog_value))
		{
		
		$this->catlog_model->insert_update_carrier_catlogs($id);
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$data['cat_type']).'";
			  </script>';
		}else{
		if (!empty($id)){
		$data['catlog']=$this->catlog_model->get_catlog_infos($id);
		$this->load->view('administration/catlog_edit',$data);
		}else{
		 $this->load->view('administration/catlog_add',$data);
		 }
		}
	
	}
	
	
		public function catlog_slatax_adding($id='')
	{
		
		$data['header'] = array('title'=>'Add Catlog');		
		$data['required'] = ' Required';	
		$data['carrier_id']=$this->input->post('carrier_id');
		$catlog_value=$this->input->post('catlog_value');
		$data['carrier_state']=$this->input->post('carrier_state');
		$data['mandatory_value']=$this->input->post('mandatory_value');		
		$data['cat_type']=$this->input->post('cat_f');
		
		if (!empty($catlog_value))
		{
		
		$this->catlog_model->insert_update_slatax_catlogs($id);
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$data['cat_type']).'";
				</script>';
		}else{
			if (!empty($id)){
			 $data['catlog']=$this->catlog_model->get_slatax_infos($id);
		     $this->load->view('administration/catlog_edit',$data);
			}else{
			 $this->load->view('administration/catlog_add',$data);
			}
		}
	
	}
	
	
	//Edit
	public function catlog_edit1($id='')
	{
	
		$data['header'] = array('title'=>'Edit Catlog');
		$data['catlog']=$this->catlog_model->get_catlog_info1($id);
		$this->form_validation->set_rules('cat_f', 'Type', 'required');

		if ($this->form_validation->run() === TRUE)
		{
			$id = $this->input->post('catlogId');
			
			//$catlog['catlog_type'] = implode(" ", $this->input->post('cat_f'));
			$catlog['catlog_type'] = $this->input->post('cat_f');;
			$catlog['catlog_name'] = $this->input->post('catlog_name');
			$catlog['state'] = $this->input->post('state');
			$catlog['slatax'] = $this->input->post('slatax');
			$catlog['unique_cargo_policy_fee'] = $this->input->post('unique_cargo_policy_fee');
			$catlog['unique_pd_policy_fee'] = $this->input->post('unique_pd_policy_fee');
			$catlog['pd_cargo_policy_fee'] = $this->input->post('pd_cargo_policy_fee');
			$catlog['association_fee'] = $this->input->post('association_fee');
			$catlog['date_modified'] = time();
			$catlog['status'] = $this->input->post('status');
			
			$this->catlog_model->insert_update_carrier_catlog($catlog,$id);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$catlog['catlog_type']).'";
				</script>';
		}
		else
		{
			$this->load->view('administration/catlog_edit1',$data);
		}
	}
	public function catlog_edit($id='')
	{
			
		$cat_type= $this->input->get('cat_type');
		
		$data['header'] = array('title'=>'Edit Catlog');
		if($cat_type==26){
		$data['catlog']=$this->catlog_model->get_catlog_infos($id);
		
		}else if($cat_type==27){
		$data['catlog']=$this->catlog_model->get_catlog_infos($id);
		
		}else if($cat_type==28){
		$data['catlog']=$this->catlog_model->get_catlog_infos($id);
		
		}else if($cat_type==29){
		$data['catlog']=$this->catlog_model->get_slatax_infos($id);
		
		}else{
		$data['catlog']=$this->catlog_model->get_catlog_info($id);
		}
		$this->form_validation->set_rules('catlog_type', 'Type', 'required');
		$this->form_validation->set_rules('catlog_value', 'Value', 'required');
		
		$id = $this->input->post('catlogId');
		
		if ($this->form_validation->run() === TRUE)
		{
			$catlog['catlog_type'] = $this->input->post('catlog_type');
			$catlog['catlog_value'] = $this->input->post('catlog_value');
			
			$catlog['status'] = $this->input->post('status');
			
			$this->catlog_model->insert_update_catlog($catlog,$id);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/catlog?cat_type='.$catlog['catlog_type']).'";
				</script>';
		}
		else
		{
			$this->load->view('administration/catlog_edit',$data);
		}
	}
	
	//DELETE
	public function ajax_catlog_delete1()
	{  
		$id=$this->input->post('id');
		$value=array('is_delete'=>1);
		$this->data_model->_update($id,$value,'rq_rqf_data_dictionary'); 
		echo 'done';
		
	}
	public function ajax_catlog_delete()
	{   
	    $cat_type=$this->input->post('cat_type');
		 
		if($cat_type==26){
		$this->db->where(array('id_pip_value'=>$this->input->post('id')));
		$value=array('is_delete'=>1);
		$query=$this->db->update('pip_value',$value); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}	
		}else if($cat_type==27){
		$this->db->where(array('id_pip_value'=>$this->input->post('id')));
		$value=array('is_delete'=>1);
		$query=$this->db->update('pip_value',$value); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}	
		}else if($cat_type==28){
		$this->db->where(array('id_pip_value'=>$this->input->post('id')));
		$value=array('is_delete'=>1);
		$query=$this->db->update('pip_value',$value); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}	
		}else if($cat_type==29){
		$this->db->where(array('id_slatax'=>$this->input->post('id')));
		$value=array('is_delete'=>1);
		$query=$this->db->update('slatax',$value); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}	
		}else{
			
		$this->db->where(array('id'=>$this->input->post('id')));
		$query=$this->db->delete('rq_rqf_quote_catlogs'); 
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	  }
		
	}
}