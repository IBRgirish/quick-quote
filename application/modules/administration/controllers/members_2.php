<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->model('member_model');
		$this->load->helper('my_helper');
		$this->load->library('pagination');  
		$this->load->helper('date');
	}
	public function index($sort_by = 'modify_date', $sort_order = 'desc', $offset = 0)
	{
		$data['fields'] = array(
			'create_date' => 'Create Date',
			
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'country' => 'Country',
			'annual_earnings' => 'Annual Earnings',
			'member' => 'Account Status',
			'' => 'More Details',
		);
		
		$keyword=isset($_GET['keyword']) ? $_GET['keyword'] :$this->input->get('keyword');
		
		$joined_in =isset($_GET['joined_in']) ? $_GET['joined_in'] :$this->input->get('joined_in');

		
		$sort_by=isset($_GET['sort_by']) ? $_GET['sort_by'] :$sort_by;
		$sort_order=isset($_GET['sort_order']) ? $_GET['sort_order'] :$sort_order;
		$data['srchStr']=$keyword;

		$limit = $this->lang->line('listing_limit');
		$total = $this->member_model->count_members($keyword, $joined_in =""); 
		$data['members'] = $this->member_model->list_members($limit, 0,$keyword,$joined_in, $sort_by, $sort_order);

		
		$data['object'] = $this;
		
		$config['base_url'] = base_url('administration/members');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		
		$this->pagination->initialize($config);
		$this->pagination->extras = ($this->input->get('sort_by')?'&sort_by='.$this->input->get('sort_by'):'');
			$this->pagination->extras .= ($this->input->get('sort_order')?'&sort_order='.$this->input->get('sort_order'):'');			


		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;

		$this->load->view('administration/members', $data);
	}
	public function page($offset = 0,$sort_by = 'modify_date', $sort_order = 'desc')
    {
		$data['fields'] = array(
			'create_date' => 'Create Date',
			'modify' => 'Modify',
			'email' => 'Email Address',						
			'modify_date' => 'Modify Date',
		);
		
		$keyword=isset($_GET['keyword']) ? $_GET['keyword'] :$this->input->get('keyword');
		$sort_by=isset($_GET['sort_by']) ? $_GET['sort_by'] :$sort_by;
		$sort_order=isset($_GET['sort_order']) ? $_GET['sort_order'] :$sort_order;
		$data['srchStr']=$keyword;

		$limit = $this->lang->line('listing_limit');
		$total = $this->member_model->count_members($keyword); 
		$data['members'] = $this->member_model->list_members($limit, $offset,$keyword, $sort_by, $sort_order);
		$data['object'] = $this;
		
		$config['base_url'] = base_url('administration/members/index');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		
		$this->pagination->initialize($config);
		$this->pagination->extras = ($this->input->get('sort_by')?'&sort_by='.$this->input->get('sort_by'):'');
			$this->pagination->extras .= ($this->input->get('sort_order')?'&sort_order='.$this->input->get('sort_order'):'');			
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;

		$this->load->view('administration/members', $data);
      
    }
		
	function details($id){
		 $members = $this->member_model->get_members_info($id);
         $data['member'] = $members;
		 $this->load->view('administration/members_details', $data);
	}
	
	 function delete($id)
	 {
			$this->member_model->delete_members($id);
			redirect($this->config->item('prevous_url'));
			exit;
	 }
	 
	  function member_status($id,$status)
	 {
			$this->member_model->member_status_update($id,$status);
			redirect($this->config->item('prevous_url'));
			exit;
	 }
}	
/* End of file test_post.php */
/* Location: ./application/controllers/test_post.php */