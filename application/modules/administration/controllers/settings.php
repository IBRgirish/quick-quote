<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->model('settings_model');
		$this->load->helper('my_helper');
		$this->load->library('pagination');  
		$this->load->helper('date');
	}
	public function index()
	{
		$data['header'] = array('title'=>'Manage Settings');
		$this->load->view('administration/settings_main', $data);
	}
	
	public function ajax_settings_list()
	{
		$array = array('delete' => 0);
		$this->datatables
         ->select('id, name, value, status, key')
		 ->from($this->config->item('config_table'))
		 ->where($array);
        echo $this->datatables->generate();
	}
	
	public function ajax_status()
	{
		$status['status']=$this->input->post('status');
		$query=$this->db->update($this->config->item('config_table'), $status, array('id'=>$this->input->post('id')));
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
	
	public function config_add()
	{
		$data['header'] = array('title'=>'Add New Setting');
		
		$this->form_validation->set_rules('key', 'Name (key)', 'required');
		$this->form_validation->set_rules('value', 'Value', 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$setting['key'] = $this->input->post('key');
			$setting['value'] = $this->input->post('value');
			$setting['status'] = 1;
			
			$this->settings_model->insert_update_settings($setting);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/settings').'";
				</script>';
		}
		else
		{
			$this->load->view('administration/setting_add',$data);
		}
	}
	
	public function setting_edit($id, $type)
	{
		$data['header'] = array('title'=>'Edit Setting');
		$data['setting']=$this->settings_model->get_setting_info($id);
		$data['type'] = $type;
		
		$this->form_validation->set_rules('key', 'Name (key)', 'required');
		$this->form_validation->set_rules('value', 'Email', 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$setting['key'] = $this->input->post('key');
			$setting['value'] = $this->input->post('value');
			$setting['sub'] = $this->input->post('sub');
			$setting['msg'] = $this->input->post('msg');
			$setting['status'] = $this->input->post('status');
			
			$this->settings_model->insert_update_settings($setting,$id);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/settings').'";
				</script>';
		}
		else
		{
			$this->load->view('administration/setting_edit',$data);
		}
	}
	
	public function ajax_setting_delete()
	{
		$status['delete']=1;
		$query=$this->db->update($this->config->item('config_table'), $status, array('id'=>$this->input->post('id')));
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
}	
/* End of file test_post.php */
/* Location: ./application/controllers/test_post.php */