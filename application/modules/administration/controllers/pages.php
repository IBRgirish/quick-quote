<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages  extends RQ_Controller {
	public function __construct()
	{
		parent::__construct();
		
		$this->require_admin_login();
		$this->load->model('page_model');
		$this->load->library('pagination');  
		$this->load->helper('date');
		$this->load->helper('my_helper');
		$this->lang->load('error');		
	}


	public function index($sort_by = 'modify_date', $sort_order = 'desc', $offset = 0)
	{

		
		if($this->input->post('pages_submit') || $sort_by || $sort_order){	
			
			$data['fields'] = array(
				'create_date' => 'Create Date',
				'modify' => 'Modify',
				'title' => 'Title',
				'description' => 'Description',				
				'modify_date' => 'Modify Date',			
			);
				
			$keyword=isset($_GET['keyword']) ? $_GET['keyword'] :$this->input->get('keyword');
			$sort_by=isset($_GET['sort_by']) ? $_GET['sort_by'] :$sort_by;
			$sort_order=isset($_GET['sort_order']) ? $_GET['sort_order'] :$sort_order;
			$data['srchStr']=$keyword;
			$limit = $this->lang->line('listing_limit');
			
			
			$total = $this->page_model->count_search($keyword); 
			$data['pages'] = $this->page_model->list_srch_pages($limit, 0,$keyword, $sort_by, $sort_order);
			$data['object'] = $this;
						
			$config['base_url'] = base_url("administration/pages");
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			$this->pagination->initialize($config);
			
			$this->pagination->extras = ($this->input->get('sort_by')?'&sort_by='.$this->input->get('sort_by'):'');
			$this->pagination->extras .= ($this->input->get('sort_order')?'&sort_order='.$this->input->get('sort_order'):'');			

			$data['total'] = $total;
			$data['sort_by'] = $sort_by;
			$data['sort_order'] = $sort_order;

			
			$this->load->view('administration/pages', $data);
		}
				
	}

	public function page($offset = 0,$sort_by = 'modify_date', $sort_order = 'desc')
    {
		
		if($this->input->post('pages_submit') || $sort_by || $sort_order){	
			
			$data['fields'] = array(
				'create_date' => 'Create Date',
				'modify' => 'Modify',
				'title' => 'Title',
				'description' => 'Description',				
				'modify_date' => 'Modify Date',				
			);
				
			$keyword=isset($_GET['keyword']) ? $_GET['keyword'] :$this->input->get('keyword');
			$sort_by=isset($_GET['sort_by']) ? $_GET['sort_by'] :$sort_by;
			$sort_order=isset($_GET['sort_order']) ? $_GET['sort_order'] :$sort_order;
			$data['srchStr']=$keyword;
			$limit = $this->lang->line('listing_limit');
			
			
			$total = $this->page_model->count_search($keyword); 
			$data['pages'] = $this->page_model->list_srch_pages($limit, $offset,$keyword, $sort_by, $sort_order);
			$data['object'] = $this;
						
			$config['base_url'] = base_url("administration/pages");
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			$this->pagination->initialize($config);
			
			$this->pagination->extras = ($this->input->get('sort_by')?'&sort_by='.$this->input->get('sort_by'):'');
			$this->pagination->extras .= ($this->input->get('sort_order')?'&sort_order='.$this->input->get('sort_order'):'');			

			$data['total'] = $total;
			$data['sort_by'] = $sort_by;
			$data['sort_order'] = $sort_order;

			$this->load->view('administration/pages', $data);
		}      
    }

	
	 public function edit($id)
	 {									
			
				$this->form_validation->set_rules('title', 'Title', 'xss_clean|trim|required');
				$this->form_validation->set_rules('keyword', 'keyword', 'xss_clean|trim|required');				
				$this->form_validation->set_rules('page_description', 'Discription', 'trim|required');
				$this->form_validation->set_rules('pagecontent', 'Body', 'trim|required');				
				$this->form_validation->set_rules('create_date', 'Create Date', 'xss_clean|trim|required');
				if($id == 0){
				$this->form_validation->set_rules('page_url', 'Page Url', 'xss_clean|trim|required|is_unique[real_pages.page_url]');
				}elseif($id != 0){
					if($this->input->post('page_url_val') != $this->input->post('page_url'))
					{
				$this->form_validation->set_rules('page_url', 'Page Url', 'xss_clean|trim|required|is_unique[real_pages.page_url]');
					}
				}
        if ($this->form_validation->run() === TRUE)
        {
				$this->load->helper('text');
			    $currentDateTime=($this->input->post('create_date')!='' && $this->input->post('create_date')!='0000-00-00 00:00:00') ? $this->input->post('create_date') :date("Y-m-d : H:i:s", time()); 
				if($this->input->post('right_bar') == 'ads'){
					$right_b = 'ad-'.$this->input->post('ads_bar');
				}elseif($this->input->post('right_bar') == 'vdo'){
					$right_b = 'video-'.$this->input->post('vdo_bar');
				}elseif($this->input->post('right_bar') == 'loginform-0'){
					$right_b = $this->input->post('right_bar');
				}else{$right_b = 0;}
				$data = array(
									'title' => $this->input->post('title'),
									'keywords' => $this->input->post('keyword'),
									'description' => $this->input->post('page_description'),
									'page_url' => $this->input->post('page_url'),
									//'content' => ascii_to_entities(html_entity_decode($this->input->post('pagecontent',TRUE))),	
									'content' => ($this->input->post('pagecontent',false)),	
									'right_bar' => $right_b,
									'right_bar_content' => $this->input->post('rightbody'),
									'publish_date' => '',
									'create_date' => $currentDateTime,
								  );
				if($this->input->post('save_pages')){	
					if($this->input->post('page_id') > 0)
					{						
						$data['modify_date'] = date('Y-m-d H:i:s');
					}else{						
						
						$data['modify_date'] = $currentDateTime;
					}
					$lastinsertid=$this->page_model->insert_update_pages($data, $this->input->post('page_id'));				
					redirect('administration/pages');
					exit;
				}
				

				if($this->input->post('cancel_pages')){
					$data['ads'] = $this->page_model->get_ads();	
					$data['vdo'] = $this->page_model->get_vdo();				
					$data['pages'] = array_to_object($data);
					$this->load->view('administration/pages_edit', $data);
				}
		  }
		  else
		  {
				$pages = $this->page_model->get_pages_info($id);  
				$data['pages'] = $pages;		
				$data['ads'] = $this->page_model->get_ads();				
				$data['vdo'] = $this->page_model->get_vdo();				
			  $this->load->view('administration/pages_edit', $data);
		  }
	 }
	 function delete($id)
	 {
			$this->page_model->delete_pages($id);
			//redirect('administration/pages');//changed on 29aug12
			redirect($this->config->item('prevous_url'));
			exit;
	 }


	 function deleteimage($id,$articleid,$status)
	 {
			$this->page_model->delete_pagesimage($id,$articleid,$status);
			redirect('administration/pages/edit/'.$articleid);
			exit;
	 }

	 function deletelink($id,$articleid,$status)
	 {
			$this->page_model->delete_pageslink($id,$articleid,$status);
			redirect('administration/pages/edit/'.$articleid);
			exit;
	 }

	public function verifycode($code)
	{
       $pages = $this->page_model->get_pages_info($this->input->post('page_id'), $code);
		if($pages)
		{
            $this->form_validation->set_message('verifycode', 'Promo Code already exists.  Please enter another one.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function update_pages_status($id,$status)
	{
		
			$this->page_model->pages_status($id,$status);
			redirect('administration/pages');
			exit;
	}

	function preview_pages_old()
	{			
			$this->load->view('administration/pages_preview');
			
	}	

	public function preview_pages($id)
	 {		
		
			$data['pagedata'] = $this->input->post();			
			$this->load->view('administration/pages_preview', $data);		  
	 }
}	
/* End of file pages.php */
/* Location: ./application/controllers/pages.php */