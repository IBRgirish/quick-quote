<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class rq_members  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/
	  <method_name> 
 	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		//$this->require_admin_login();
		$this->load->helper('my_helper');
		$this->load->model('catlog_model');
		$this->load->library('pagination');  
		$this->load->helper('date');
		$this->load->helper('url');
		//$this->load->library('customclass');
	}
	
	public function index()
	{  $email=$this->input->post('email');
		$this->db->select('id,status');	
		$this->db->where('email',$email);	
		$query = $this->db->get('rq_members');
		$rows = $query->result();
		$members[]=$rows[0]->id;
		$members[]=$rows[0]->status;
		echo json_encode($members);
		
	}
	
	
}