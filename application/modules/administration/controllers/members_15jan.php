<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->model('member_model');
		$this->load->helper('my_helper');
		$this->load->library('pagination');  
		$this->load->helper('date');
	}
	public function index()
	{
		$data['header'] = array('title'=>'Manage Agency');

		$this->load->view('administration/member_main', $data);
	}
	
	public function ajax_list_members()
	{
		$array = array('is_deleted' => 0);
		$this->datatables
         ->select('id,first_name,last_name,email,phone_number,status,DATE_FORMAT(created_date, "%d-%M-%Y"),DATE_FORMAT(last_login, "%d-%M-%Y %H:%i:%s"), phone_area', FALSE)
		 ->from($this->config->item('member_table'))
		 ->where($array);
        echo $this->datatables->generate();
	}
	
	public function ajax_status()
	{
		$status['status']=$this->input->post('status');
		$query=$this->db->update($this->config->item('member_table'), $status, array('id'=>$this->input->post('id')));
		if( $status['status'] =='ACTIVE'){
			$data['failed_attempt'] = '0';
			$query=$this->db->update($this->config->item('member_table'), $data, array('id'=>$this->input->post('id')));
		}
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
	
	public function page($offset = 0,$sort_by = 'modify_date', $sort_order = 'desc')
    {
		$data['fields'] = array(
			'create_date' => 'Create Date',
			'modify' => 'Modify',
			'email' => 'Email Address',						
			'modify_date' => 'Modify Date',
		);
		
		$keyword=isset($_GET['keyword']) ? $_GET['keyword'] :$this->input->get('keyword');
		$sort_by=isset($_GET['sort_by']) ? $_GET['sort_by'] :$sort_by;
		$sort_order=isset($_GET['sort_order']) ? $_GET['sort_order'] :$sort_order;
		$data['srchStr']=$keyword;

		$limit = $this->lang->line('listing_limit');
		$total = $this->member_model->count_members($keyword); 
		$data['members'] = $this->member_model->list_members($limit, $offset,$keyword, $sort_by, $sort_order);
		$data['object'] = $this;
		
		$config['base_url'] = base_url('administration/members/index');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		
		$this->pagination->initialize($config);
		$this->pagination->extras = ($this->input->get('sort_by')?'&sort_by='.$this->input->get('sort_by'):'');
			$this->pagination->extras .= ($this->input->get('sort_order')?'&sort_order='.$this->input->get('sort_order'):'');			
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;

		$this->load->view('administration/members', $data);
      
    }
		
	function details($id){
		 $members = $this->member_model->get_members_info($id);
         $data['member'] = $members;
		 $this->load->view('administration/members_details', $data);
	}
	
	 function delete($id)
	 {
			$this->member_model->delete_members($id);
			redirect($this->config->item('prevous_url'));
			exit;
	 }
	 
	  function member_status($id,$status)
	 {
			$this->member_model->member_status_update($id,$status);
			redirect($this->config->item('prevous_url'));
			exit;
	 }
	 
	 function ajax_user_delete()
	 {
	 	$data['is_deleted'] = 1;
		$query=$this->db->update($this->config->item('member_table'), $data, array('id'=>$this->input->post('id')));
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	 }
	 
	 public function view_member($id)
	 {
	 	$members = $this->member_model->get_members_info($id);
        $data['member'] = $members;
		$data['header'] = array('title'=>'View Agency');
		$this->load->view('administration/member_view',$data);
	 }
	 
	 public function add_member()
	 {
	 	$data['header'] = array('title'=>'Add New Agency');
		$data['underWriter'] = $this->member_model->active_underwriter_list();
		
		$member_table = $this->config->item('member_table');
		$registration_success = $this->lang->line('registration_success');
				
		$this->form_validation->set_rules('txtFirstName', 'Agency name', 'required');
		//$this->form_validation->set_rules('txtMiddleName', 'Middle name', '');
		//$this->form_validation->set_rules('txtLastName', 'Last name', 'required');	
		$this->form_validation->set_rules('txaAddress', 'Address', 'required');
		$this->form_validation->set_rules('txtCountry', 'Country', 'required');
		$this->form_validation->set_rules('txtState', 'State', 'required');
		$this->form_validation->set_rules('txtCity', 'City', 'required');
		$this->form_validation->set_rules('txtZipCode', 'Zip Code', 'required');
		/* New Phone Number End - Sunil Chouhan @ 11/08/2013 */
		//$this->form_validation->set_rules('txtPhoneNumber', 'Phone Number', 'required');
		//$this->form_validation->set_rules('txtPhArea', 'Phone Area Code', 'required');
		$this->form_validation->set_rules('txtPhone1', 'Phone Number', 'required');
		$this->form_validation->set_rules('txtPhone2', 'Phone Number', 'required');
		$this->form_validation->set_rules('txtPhone3', 'Phone Number', 'required');
		/* New Phone Number End - Sunil Chouhan @ 11/08/2013 */
		$this->form_validation->set_rules('email', 'Email', "required|valid_email|is_unique[$member_table.email]" );
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('cpassword', 'Confirm password', 'required|matches[password]');
				
		if ($this->form_validation->run() === TRUE)
		{
			$formData = $this->input->post();
						
			$phone_area =  isset($formData['txtPhArea']) ? $formData['txtPhArea'] : '';$txtphone_4 = isset ($formData['txtPhone4']) ? $formData['txtPhone4'] : '';
			$phone = $formData['txtPhone1'].'-'.$formData['txtPhone2'].'-'.$formData['txtPhone3'].'-'.$txtphone_4;
			$cell_area =  $formData['txtMoArea'];
			$cell = $formData['txtCell1'].'-'.$formData['txtCell2'].'-'.$formData['txtCell3'];
			$fax_area = $formData['txtFaxArea'];
			$faxN = $formData['txtFax1'].'-'.$formData['txtFax2'].'-'.$formData['txtFax3'];
				
			 $underWriter = isset($formData['optUnderWriter']) ? $formData['optUnderWriter'].':' : ''; 
			
			if ($this->input->post('chkchkUnderWriter'))		
			{
				$underWriter.= implode(', ',$this->input->post('chkchkUnderWriter'));
			}
			
			$userData = array(
				
				'first_name' => $this->input->post('txtFirstName'),
				//'middle_name' => $this->input->post('txtMiddleName'),
				//'last_name' => $this->input->post('txtLastName'),
				'address' => $this->input->post('txaAddress'),
				'city' => $this->input->post('txtCity'),
				'state' => $this->input->post('txtState'),
				'country' => $this->input->post('txtCountry'),
				'zip_code' => $this->input->post('txtZipCode'),
				'zip_code_extd' => $this->input->post('txtZipCodeExtd'),
				'phone_area' => $phone_area,
				'phone_number' => $phone,
				'phone_ext' => $this->input->post('txtPhoneExt'),
				'cell_area' => $cell_area,
				'cell_number' => $cell,
				'fax_number' => $faxN,
				'fax_area' => $fax_area,	
				'email' => $this->input->post('email'),	
				'sec_email' => $this->input->post('txtSecEmail'),
				'website' => $this->input->post('txtWebsite'),
				'password' => trim($this->input->post('password')),
				'underwriter' => $underWriter,
				'created_date' => date("Y-m-d : H:i:s", time()),
			);
			
			$this->db->insert($member_table,$userData);
			$this->session->set_flashdata('success', $registration_success);
			redirect('administration/agency');
		}
		else
		{
			$this->load->view('administration/add_member',$data);
		}
	 }
	 
	 public function edit_member($id)
	 {
	 	$members = $this->member_model->get_members_info($id);
		$data['member'] = $members;
		$data['underWriter'] = $this->member_model->active_underwriter_list();
		//print_r($data['underWriter']);
		//$data['underWriter']['flag'] = 1;
		$data['header'] = array('title'=>'Edit Agency');
		
		$registration_success = $this->lang->line('registration_success');
		
		$this->form_validation->set_rules('txtFname', 'Agency name', 'required');
		//$this->form_validation->set_rules('txtMname', 'Middle name', '');
		//$this->form_validation->set_rules('txtLname', 'Last name', 'required');
		$this->form_validation->set_rules('txaAddress', 'Address', 'required');
		$this->form_validation->set_rules('txtCountry', 'Country', 'required');
		$this->form_validation->set_rules('txtState', 'State', 'required');
		$this->form_validation->set_rules('txtCity', 'City', 'required');
		$this->form_validation->set_rules('txtZip', 'Zip Code', 'required');
		$this->form_validation->set_rules('txtZipExtd', 'Zip Code Extended', '');
		
		//$this->form_validation->set_rules('txtPhArea', 'Phone Area', 'required');
		$this->form_validation->set_rules('txtPhone1', 'Phone', 'required');
		$this->form_validation->set_rules('txtPhone2', 'Phone', 'required');
		$this->form_validation->set_rules('txtPhone3', 'Phone', 'required');
		$this->form_validation->set_rules('txtExtension', 'Phone Extension', '');
		
		$this->form_validation->set_rules('txtMoArea', 'Cell Area', '');
		$this->form_validation->set_rules('txtCell1', 'Cell Number', '');
		$this->form_validation->set_rules('txtCell2', 'Cell Number', '');
		$this->form_validation->set_rules('txtCell3', 'Cell Number', '');
		
		$this->form_validation->set_rules('txtFaxArea', 'Fax Area', '');
		$this->form_validation->set_rules('txtFax1', 'Fax', '');
		$this->form_validation->set_rules('txtFax2', 'Fax', '');
		$this->form_validation->set_rules('txtFax3', 'Fax', '');
		
		$this->form_validation->set_rules('txtSecEmail', 'Secondary Email', '');
		$this->form_validation->set_rules('txtWebsite', 'Website', '');
		$this->form_validation->set_rules('chkchkUnderWriter', 'Underwriter', 'required');
		$this->form_validation->set_rules('optUnderWriter', 'Primary Underwriter', 'required');
		if(trim($this->input->post('password'))!=''){
			$this->form_validation->set_rules('password', 'password', 'required');
			$this->form_validation->set_rules('cpassword', 'Confirm password', 'required|matches[password]');
		}
			
		if ($this->form_validation->run() === TRUE)
		{
			$underWriter = $this->input->post('optUnderWriter').':';
			if ($this->input->post('chkchkUnderWriter'))
			{
				$underWriter.= implode(', ',$this->input->post('chkchkUnderWriter'));
			}	
			$formData = $this->input->post();
					$phone_area =  isset($formData['txtPhArea']) ? $formData['txtPhArea'] : '';			$txtphone_4 = isset ($formData['txtPhone4']) ? $formData['txtPhone4'] : '';			$phone = $formData['txtPhone1'].'-'.$formData['txtPhone2'].'-'.$formData['txtPhone3'].'-'.$txtphone_4;
			
			$cell_area =  $formData['txtMoArea'];
			$cell = $formData['txtCell1'].'-'.$formData['txtCell2'].'-'.$formData['txtCell3'];
			$fax_area = $formData['txtFaxArea'];
			$faxN = $formData['txtFax1'].'-'.$formData['txtFax2'].'-'.$formData['txtFax3'];
			
			$status = $this->input->post('txtstatus');
			
			$uData = array(		
				'first_name' => $this->input->post('txtFname'),
				'middle_name' => $this->input->post('txtMname'),
				'last_name' => $this->input->post('txtLname'),
				'address' => $this->input->post('txaAddress'),
				'city' => $this->input->post('txtCity'),
				'state' => $this->input->post('txtState'),
				'country' => $this->input->post('txtCountry'),
				'zip_code' => $this->input->post('txtZip'),
				'zip_code_extd' => $this->input->post('txtZipExtd'),
				'phone_area' => $phone_area,
				'phone_number' => $phone,
				'phone_ext' => $this->input->post('txtExtension'),
				'cell_area' => $cell_area,
				'cell_number' => $cell,
				'fax_number' => $faxN,
				'fax_area' => $fax_area,	
				'email' => $this->input->post('email'),	
				'sec_email' => $this->input->post('txtSecEmail'),
				'website' => $this->input->post('txtWebsite'),
				'underwriter' => $underWriter,
				'status' => $status,
				'updated_date' => date("Y-m-d : H:i:s", time()),
			);
			
		//	if($this->input->post('edit_memberinfo'))
		//	{
				if($this->input->post('members_id') > 0)
				{	
					if(trim($this->input->post('password'))!=''){
						$uData['password'] = $this->input->post('password');
					} else {
						$uData['password'] = $this->input->post('savepassword');
					}
				}
		//	}
				//echo $uData['password']; die;
			$saved=$this->member_model->insert_update_members($uData, $id);
			
			if($saved){
				$this->load->library('email');
				if($status == 'REJECTED' && $members->status !='REJECTED' ){
					$mail_message = 'Hi, <br> Your Agency Account request at'.base_url().' has been rejected.';
					$user_mail_id = $this->input->post('email');
					$email_setting  = array('mailtype'=>'html');
					$this->email->initialize($email_setting);
					$this->email->from('GCIB', 'GCIB');
					$this->email->subject('Sorry, your agency account request is rejected.');
					$this->email->message($mail_message);
					$this->email->to($user_mail_id);
					
					$mail_sent = $this->email->send();
				}elseif($status == 'ACTIVE' && $members->status !='ACTIVE' ){
					$user_mail_id = $this->input->post('email');
				
		$mail_message = 'Welcome to Global Century Insurance Brokers!<br>
					    Thanks for signing up for our Quote online application.<br>The following account has been reservered for '.$user_mail_id.
					   '  If you have further information requirements please do not hesitate to contact us at marketing@gcib.net or to our phone number (925) 423-2575.<br><br></br>
					   Regards,<br>
					Global Century Insurance Brokers';
				
					$email_setting  = array('mailtype'=>'html');
					$this->email->initialize($email_setting);
					$this->email->from('m.suhel11@gmail.com', 'GCIB');
					$this->email->subject('Congratulations, your agency account request is approved.');
					$this->email->message($mail_message);
					$this->email->to($user_mail_id);
					$mail_sent = $this->email->send();
				}
				$this->session->set_flashdata('success', 'Your Information Updated successfully ');
				redirect(base_url('administration/agency'),'refresh');
			} 
			
		}
		else
		{ 
			$this->load->view('administration/edit_member',$data);
		}
	 }
	 
	 function agency_registration_email()
	{
		$this->load->library('email');
		$mail_message = 'Hi,<br>
                         Your account has been created.<br>
						 Your account will be activated after review.<br><br><br></br>
						 Thanks';
		$user_mail_id = 'ibr.anupama@gmail.com';
		$email_setting = array('mailtype'=>'html');
		$this->email->initialize($email_setting);
		$this->email->from('GCIB', 'GCIB');
		$this->email->subject('Welcome');
		$this->email->message($mail_message);
		$this->email->to($user_mail_id);
		$mail_sent = $this->email->send();
	}
}	





/* End of file test_post.php 123*/
/* Location: ./application/controllers/test_post.php */