<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quotes  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/
	  <method_name> 
 	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->helper('my_helper');
		$this->load->model('quote/quote_model');
		$this->load->model('ratesheet/ratesheet_model');
		$this->load->model('user/members_model');
		$this->load->model('underwriter_model');
		$this->load->library('pagination');  
		$this->load->helper('date');
		$this->load->helper('url');
		//$this->load->library('customclass');
	}
	public function index()
	{
			$limit = 25;
			$total = $this->underwriter_model->count_underwriter(); 
			
			$data['quote'] = $this->quote_model->get_quote();
			
			$data['rows'] = $this->quote_model->get_quote_rate('','');
			//print_r($data['rows']);
			$data['object'] = $this;
			
			$config['base_url'] = base_url('administration/quotes');
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			
			$data['total'] = $total;
			$this->pagination->initialize($config);

			$this->load->view('administration/quotes', $data);
	}
	
	/**By Ashvin Patel 13/may/2014
	public function QuoteView($quote_id='')
	{
		$data['quote'] = $this->quote_model->get_quote($quote_id);
		$data['insured'] = $this->quote_model->get_insured_info($quote_id);
		$data['losses'] = $this->quote_model->get_losses($quote_id);
		$data['tractors'] = $this->quote_model->get_all_vehicle($quote_id, 'TRACTOR');
		$data['trailers'] = $this->quote_model->get_all_vehicle($quote_id, 'TRAILER');
		$data['trucks'] = $this->quote_model->get_all_vehicle($quote_id, 'TRUCK');
		$data['drivers'] = $this->quote_model->get_all_drivers($quote_id);
		$data['fillings'] = $this->quote_model->get_all_fillings($quote_id);
		$data['owners'] = $this->quote_model->get_all_owner($quote_id);
		//$data = '';
		$this->load->view('quote/quote_mail_view1', $data);
	}
	By Ashvin Patel 13/may/2014*/
	
	
	/**By Ashvin Patel 13/may/2014**/
	public function QuoteView($quote_id=''){

		$data['quote'] = $this->quote_model->get_quote($quote_id);
	
		if(isset($data['quote'][0]->quote_id))
	    $parent_id = ($data['quote'][0]->main_quote_id) ? $data['quote'][0]->main_quote_id : $data['quote'][0]->quote_id;
	 	
		$data['quote_id'] = isset($data['quote'][0]->quote_id) ? $data['quote'][0]->quote_id : '';
		if($parent_id){
		 $previous_quote_ids = $this->get_all_related_quote_ids($parent_id);
		 $data['previous_quote_ids'] = $previous_quote_ids;	
		 
		 $data['losses_liability_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_liability');
		 $data['losses_pd_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_pd');
		 $data['losses_cargo_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_cargo');
		 $data['losses_other1_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_other1_specify_other');
		 $data['losses_other2_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_other2_specify_other');
		 	
		}
		$fields = array('contact_name', 'contact_middle_name', 'contact_last_name');
		$data['logs'] = getModificationLog('quote', $parent_id, $fields, 0);
		
		$data['insured'] = $this->quote_model->get_insured_info($quote_id);
		$fields = array('insured_fname', 'insured_mname', 'insured_lname', 'insured_dba', 'insured_telephone', 'insured_garaging_city', 'insured_state', 'insured_zip', 'nature_business', 'commodities_haulted', 'year_in_business', 'insured_email', 'Comments_request');
	    $data['insured_logs'] = getModificationLog('quote_insured', $parent_id, $fields, 0);
	  
		$data['losses'] = $this->quote_model->get_losses($quote_id);
		$data['losses_liability'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_liability');
		$data['losses_pd'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_pd');
		$data['losses_cargo'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_cargo');
		$data['losses_other1'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_other1_specify_other');
		$data['losses_other2'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_other2_specify_other');
		
		$data['tractors'] = $this->quote_model->get_all_vehicle($quote_id, 'TRACTOR');
		$data['tractors_diff'] = $this->vehicle_row_diff($previous_quote_ids, $quote_id, 'TRACTOR');		 

		$data['trailers'] = $this->quote_model->get_all_vehicle($quote_id, 'TRAILER');		

		$data['trucks'] = $this->quote_model->get_all_vehicle($quote_id, 'TRUCK');
		$data['trcuk_diff'] = $this->vehicle_row_diff($previous_quote_ids, $quote_id, 'TRUCK');

		$data['drivers'] = $this->quote_model->get_all_drivers($quote_id);
		$data['driver_diff'] = $this->driver_row_diff($previous_quote_ids, $quote_id);		 

		$data['fillings'] = $this->quote_model->get_all_fillings($quote_id);
		$data['filling_diff'] = $this->filling_row_diff($previous_quote_ids, $quote_id);

		$data['owners'] = $this->quote_model->get_all_owner($quote_id);
		$data['owner_diff'] = $this->owner_row_diff($previous_quote_ids, $quote_id);	

		$this->load->view('quote/quote_mail_view1', $data);

	}
	/**By Ashvin Patel 13/may/2014**/
	
	
	public function loss_row_diff($previous_quote_ids, $quote_id, $type='losses_liability'){		
	  $diff_data = '';

	  foreach($previous_quote_ids as $key => $previous_quote_id){	
		if($previous_quote_id!=$quote_id){
		  if(isset($previous_quote_ids[$key+1])){
			$previous_quote_id_1 = $previous_quote_ids[$key+1];
			$data_0 = $this->quote_model->get_losses_by_type($previous_quote_id, $type);
			$data_1 = $this->quote_model->get_losses_by_type($previous_quote_id_1, $type);
			foreach($data_0 as $key => $data){
			  $pre_data = isset($data_1[$key]) ? (array) $data_1[$key] : '';
			  $lat_data = (array) $data;
			  if(is_array($pre_data)&&is_array($lat_data)){				  
			  	$diff_data[$previous_quote_id][] = compare_array($lat_data, $pre_data);
			  }
			}
		  }

		}
	  }
	 
	 
	  return $diff_data;
	}
	public function filling_row_diff($previous_quote_ids, $quote_id){		
	  $diff_data = '';
	  foreach($previous_quote_ids as $key => $previous_quote_id){	
		if($previous_quote_id!=$quote_id){
		  if(isset($previous_quote_ids[$key+1])){
			$previous_quote_id_1 = $previous_quote_ids[$key+1];
			$data_0 = $this->quote_model->get_all_fillings($previous_quote_id);
			$data_1 = $this->quote_model->get_all_fillings($previous_quote_id_1);
			foreach($data_0 as $key => $data){
			  $pre_data = isset($data_1[$key]) ? (array) $data_1[$key] : '';
			  $lat_data = (array) $data;
			  if(is_array($pre_data)&&is_array($lat_data)){		
				$diff_data[$previous_quote_id][] = compare_array($lat_data, $pre_data);
			  }
			}
		  }
		}
	  }
	  //print_r($diff_data);
	  return $diff_data;
	}
	
	public function vehicle_row_diff($previous_quote_ids, $quote_id, $type='TRACTOR'){		
	  $diff_data = '';
	  foreach($previous_quote_ids as $key => $previous_quote_id){	
		if($previous_quote_id!=$quote_id){
		  if(isset($previous_quote_ids[$key+1])){
			$previous_quote_id_1 = $previous_quote_ids[$key+1];
			$tractors = $this->quote_model->get_all_vehicle($previous_quote_id, $type);
			$tractors_1 = $this->quote_model->get_all_vehicle($previous_quote_id_1, $type);
			foreach($tractors as $key => $tractor){
			  $pre_data = isset($tractors_1[$key]) ? (array) $tractors_1[$key] : '';
			  $lat_data = (array) $tractor;
			  if(is_array($pre_data)&&is_array($lat_data)){
			  	$diff_data[$previous_quote_id][] = compare_array($lat_data, $pre_data);
			  }
			}
		  }

		}
	  }
	  return $diff_data;
	}
	public function owner_row_diff($previous_quote_ids, $quote_id){		
	  $diff_data = '';
	  foreach($previous_quote_ids as $key => $previous_quote_id){	
		if($previous_quote_id!=$quote_id){
		  if(isset($previous_quote_ids[$key+1])){
			$previous_quote_id_1 = $previous_quote_ids[$key+1];
			$data_0 = $this->quote_model->get_all_owner($previous_quote_id);
			$data_1 = $this->quote_model->get_all_owner($previous_quote_id_1);
			foreach($data_0 as $key => $data){
			  $pre_data = isset($data_1[$key]) ? (array) $data_1[$key] : '';
			  $lat_data = (array) $data;
			  if(is_array($pre_data)&&is_array($lat_data)){	
				$diff_data[$previous_quote_id][] = compare_array($lat_data, $pre_data);
			  }
			}
		  }
		}
	  }
	  //print_r($diff_data);
	  return $diff_data;
	}
	public function driver_row_diff($previous_quote_ids, $quote_id){		
	  $diff_data = '';
	  foreach($previous_quote_ids as $key => $previous_quote_id){	
		if($previous_quote_id!=$quote_id){
		  if(isset($previous_quote_ids[$key+1])){
			$previous_quote_id_1 = $previous_quote_ids[$key+1];
			$data_0 = $this->quote_model->get_all_drivers($previous_quote_id);
			$data_1 = $this->quote_model->get_all_drivers($previous_quote_id_1);
			foreach($data_0 as $key => $data){
			  $pre_data =  isset($data_1[$key]) ? (array) $data_1[$key] : '';
			  $lat_data = (array) $data;
			  if(!empty($pre_data)&&!empty($lat_data)){
				$diff_data[$previous_quote_id][] = compare_array($lat_data, $pre_data);
			  }
			}
		  }
		}
	  }
	  //print_r($diff_data);
	  return $diff_data;
	}
	public function get_all_related_quote_ids($parent_quote_id=''){
		if($parent_quote_id){
			$this->db->select('quote_id');
			$this->db->where('main_quote_id', $parent_quote_id);
			$query = $this->db->get('rq_rqf_quote');	
			$result = $query->result();
			$ids[] = $parent_quote_id;
			if(is_array($result)){
				foreach($result as $value){
					$ids[] = $value->quote_id;	
				}
			}
			return $ids;
		}
	}
	
	//Losses version_diff
	public function losses_version_diff($v_id='', $v1_id='', $type='losses_liability'){
		$losses = $this->quote_model->get_losses_by_type($v_id, $type);
		$losses_1 = $this->quote_model->get_losses_by_type($v1_id, $type);
		$a = count($losses);
		$b = count($losses_1);
		$c = ($a>$b) ? $a : $b;
		$losse_data = '';
		for($i=0;$i<$a;$i++){
			if(isset($losses[$i])&&!isset($losses_1[$i])){
				$losse_data[$i] = $losses[$i];
			}
		}
		$data['losses'] = $losse_data;
		$data['l_type'] = $type;
		$this->load->view('losses_row_diff', $data);
	}
	
	//Vehicle version_diff
	public function vehicle_version_diff($v_id='', $v1_id='', $type='TRACTOR'){
		$vehicles = $this->quote_model->get_all_vehicle($v_id, $type);
		$vehicles_1 = $this->quote_model->get_all_vehicle($v1_id, $type);
		$a = count($vehicles);
		$b = count($vehicles_1);
		$c = ($a>$b) ? $a : $b;
		$vehicle_data = '';
		for($i=0;$i<$a;$i++){
			if(isset($vehicles[$i])&&!isset($vehicles_1[$i])){
				$vehicle_data[$i] = $vehicles[$i];
			}
		}
		$data['vehicles'] = $vehicle_data;
		$data['v_type'] = $type;
		$this->load->view('vehicle_row_diff', $data);
	}
	
	//driver_version_diff
	public function driver_version_diff($v_id='', $v1_id='', $type='Driver'){
		$drivers = $this->quote_model->get_all_drivers($v_id, $type);
		$drivers_1 = $this->quote_model->get_all_drivers($v1_id, $type);
		$a = count($drivers);
		$b = count($drivers_1);
		$c = ($a>$b) ? $a : $b;
		$driver_data = '';
		for($i=0;$i<$a;$i++){
			if(isset($drivers[$i])&&!isset($drivers_1[$i])){
				$driver_data[$i] = $drivers[$i];
			}
		}
		$data['drivers'] = $driver_data;
		$data['v_type'] = $type;
		$this->load->view('driver_row_diff', $data);
	}
	public function owner_version_diff($v_id='', $v1_id='', $type='Driver'){
		$owners = $this->quote_model->get_all_owner($v_id, $type);
		$owners_1 = $this->quote_model->get_all_owner($v1_id, $type);
		$a = count($owners);
		$b = count($owners_1);
		$c = ($a>$b) ? $a : $b;
		$owner_data = '';
		for($i=0;$i<$a;$i++){
			if(isset($owners[$i])&&!isset($owners_1[$i])){
				$owner_data[$i] = $owners[$i];
			}
		}
		$data['owners'] = $owner_data;
		$data['v_type'] = $type;
		$this->load->view('owner_row_diff', $data);
	}
	public function filing_version_diff($v_id='', $v1_id='', $type='Filing'){
		$filings = $this->quote_model->get_all_fillings($v_id, $type);
		$filings_1 = $this->quote_model->get_all_fillings($v1_id, $type);
		$a = count($filings);
		$b = count($filings_1);
		$c = ($a>$b) ? $a : $b;
		$filing_data = '';
		for($i=0;$i<$a;$i++){
			if(isset($filings[$i])&&!isset($filings_1[$i])){
				$filing_data[$i] = $filings[$i];
			}
		}
		$data['filings'] = $filing_data;
		$data['v_type'] = $type;
		$this->load->view('filing_row_diff', $data);
	}	
	
	
	
	/**By Ashvin Patel 10/may/2014**/
	public function sort_by_status()
	{	
		
			$status_re = $this->input->post('status');
			$data['quote'] = $this->quote_model->get_quote();
			
			$data['rows'] = $this->quote_model->get_quote_rate('','');
			//print_r($data['rows']);		

			//$this->load->view('administration/quotes', $data);
			$table_data = '';
			
			$table_data .= ' <table id="underwriterList" width="100%" class="dataTable">
								<thead>
								  <tr>
								    <th class="center" width="10%">Quote Number</th>									
									<th width="8%">Date Submitted</th>									
									<th width="12%">Agency Full Name/DBA</th>									
									<th width="12%">Agency Phone Number</th>
									<th width="12%">Insured Full Name/DBA </th>		
									<th>Coverage Date</th>							
									<th  style="width:349px">Action</th>
								  </tr>
								</thead>
							<tbody id="tabe_body">';
			
		$cnt= 0;
		if(!empty($data['quote']))  
		{
		 krsort($data['quote']); foreach($data['quote'] as $row)
		{
			$cnt++;
			 $ids[] = $row->quote_id;
				
				if($row->bundle_status == '') 
				{ 
					if($row->perma_reject=='Reject'){ 
						$status =  'Rejected';
					}
					else{ 
						$status =  'Requested' ;
						}
				} 
				else{ 
					$status =  $row->bundle_status; 
					}
				 if($row->bundle_id == 0) 
				 { 
				 	$ratesheet_generate = '<a class="btn" href="'.base_url('ratesheet/generate_ratesheet').'/'.$row->quote_id.'">Generate Ratesheet</a>';    
                 
					if($row->perma_reject!='Reject')
					{			
						$reject =  '<a href="javascript:;" class="btn btn-danger" onclick="changeStatus("reject", '.$row->quote_id.', '.$cnt.')" id="link_'.$cnt.'">Reject</a>';
					 } 
					 else
					 {
					 	$reject = '';
					 }
				} 
				else 
				{ 
					$ratesheet_generate = '<a class="btn btn-info" href="'.base_url().'administration/quotes/rate_by_quote/'.$row->quote_id.'">Quotte</a></a>';
					$reject = '';
				} 
				if($status_re!='')
				{
					if($status_re==$status)
					{
						$table_data .= '<tr>
							<td>'.$row->quote_id.'</td>
							
							<td>'.date('m/d/Y', strtotime( $row->date_added)).'</td>
							
							<td>'.get_agency_detail($row->requester_id, 'first_name').'</td>
							<td>'.get_agency_detail($row->requester_id, 'phone_number').'</td>
							<td>'.$row->contact_name.' '. $row->contact_middle_name.' '. $row->contact_last_name.'</td>
							<td >'.date('m/d/Y', strtotime($row->coverage_date)).'</td>
							<td>'.$ratesheet_generate.'
							   <a class="btn btn-primary" href="javascript:;" onclick="assign_underwriter("'.$row->assigned_underwriter.'", '.$row->quote_id.')">Assign Underwriter </a><a href="javascript:;" onclick="viewQuotePopup('.$row->quote_id.')" class="btn btn">View Request</a>
						   </td>
						   
						</tr>';
					}
				}
				else
				{
					$table_data .= '<tr>
							<td>'.$row->quote_id.'</td>
							
							<td>'.date('m/d/Y', strtotime( $row->date_added)).'</td>
							
							<td>'.get_agency_detail($row->requester_id, 'first_name').'</td>
							<td>'.get_agency_detail($row->requester_id, 'phone_number').'</td>
							<td>'.$row->contact_name.' '. $row->contact_middle_name.' '. $row->contact_last_name.'</td>
							<td >'.date('m/d/Y', strtotime($row->coverage_date)).'</td>
							<td>'.$ratesheet_generate.'
							   <a class="btn btn-primary" href="javascript:;" onclick="assign_underwriter("'.$row->assigned_underwriter.'", '.$row->quote_id.')">Assign Underwriter </a><a href="javascript:;" onclick="viewQuotePopup('.$row->quote_id.')" class="btn btn">View Request</a>
						   </td>
						   
						</tr>';
				}
				
		}
		}
		

		if(!empty($data['rows']))
		{
		 foreach($data['rows'] as $row) 
		 { 
		 	$cnt++; /*$ids[] = $row[0]['quote_id'];*/
      
			if(isset($row[0]['is_manual']))
			{
					if($row[0]['is_manual']==1)
					{ 
						if(isset($row[0]['is_manual']))
						{
							$quote_ref = isset($row[0]['quote_ref']) ? $row[0]['quote_ref'] : '';
							$quote_id1 =  isset($row[0]['quote_id']) ? $row[0]['quote_id'] : ''; 
							if($row[0]['is_manual']==1)
							{
								
								$view = '<a class="btn btn-info" href="'.base_url().'administration/quotes/rate_by_quote2/'.$quote_ref.'">Quotte</a>';
								$assign_underwriter = '<a class="btn btn-primary" href="javascript:;" onclick="assign_underwriter1('.$row[0]['underwriters'].', '.$row[0]['id'].', 1)">Assign Underwriter </a>';								
							}
							else
							{							
								$view ='<a class="btn btn-info" href="<?php echo base_url(); ?>administration/quotes/rate_by_quote1/'.$quote_ref.'">Quotte</a>';
								$assign_underwriter =  '<a class="btn btn-primary" href="javascript:;" onclick="assign_underwriter('.$quote_id1.' '.$quote_ref.')">Assign Underwriter</a>';						
							}
						}
						$quote_no = isset($row[0]['quote_id']) ? $row[0]['quote_id'] : '';
						$date_sub = date('m/d/Y', strtotime( $row[0]['creation_date']));
						$agency_name = get_agency_detail($row[0]['email'], 'first_name', 'email');
						$agency_phon = get_agency_detail($row[0]['email'], 'phone_number', 'email');
						$insured = isset($row[0]['insured']) ? $row[0]['insured'] : '';
						$creation_date = isset($row[0]['creation_date']) ? date("m/d/Y",strtotime($row[0]['creation_date'])) : '';
						$status = isset($row[0]['status']) ? $row[0]['status'] : '';
						if($status_re!='')
						{
							if($status_re==$status)
							{
								$table_data .= '<tr>
												<td  width="10%">'.$quote_no.'</td>
												<td>'.$date_sub.'</td>
												<td width="15%">'.$agency_name.'</td>
												<td width="15%">'.$agency_phon.'</td>
												<td width="15%">'.$insured.'</td>
												
												<td width="15%"></td>
												<td width="15%">'.$view.' '.$assign_underwriter.'</td>
										  </tr>';
							}
						}
						else{
							$table_data .= '<tr>
												<!--<td  width="10%"><?php echo $cnt; ?></td>-->
												<td>'.$quote_no.'</td>
												<td width="15%">'.$date_sub.'</td>
												<td width="15%">'.$agency_name.'</td>
												<td width="15%">'.$agency_phon.'</td>												
												<td width="15%">'.$insured.'</td>
												<td width="15%"></td>
												<td width="15%">'.$view.' '.$assign_underwriter.'</td>
										  </tr>';
						}
			 		} 
		  		} 
		 	} 
		 }
        $table_data .= ' </tbody>
 					 	 </table>';
  $data['table_data'] = $table_data;
		echo json_encode($data);
	}
	/**By Ashvin Patel 10/may/2014**/
	
	public function assign_underwriter()
	{
		//echo $id;
		$underwriter1 =  explode(',', $this->input->get('underwriter'));
		//print_r($underwriter1);
		$id =  $this->input->get('id');
		$is_maual =  $this->input->get('is_manual');
		$underwriters = $this->members_model->get_underwriters();
		$data['underwriter'] = '';
		$data['quote_id'] = $id;
		$data['is_maual'] = isset($is_maual) ? $is_maual : 0;
		foreach($underwriters as $underwriter)
		{
			//echo $underwriter->id;
			//echo $id;
			if(in_array($underwriter->id, $underwriter1)){ $check = 'checked'; }else{ $check = ''; }
			$data['underwriter'] .= '<input type="checkbox" name="underwriter[]"  class="underwrit" '.$check.'  value="'.$underwriter->id.'">  '.$underwriter->name.'  ';
			
		}
		$data['link'] = base_url('administration/quotes/rate_by_quote/'.$id.'');
		$this->load->view('change_underwriter', $data);
	}
	public function assign_underwriter1()
	{
		//echo $id;		
		//print_r($underwriter1);
		$id =  $this->input->get('id');
		$ref_id =  $this->input->get('ref_id');
		$is_maual =  $this->input->get('is_manual');
		$data['is_maual'] = isset($is_maual) ? $is_maual : 0;
		$this->db->select('quote_id,assigned_underwriter');
		$this->db->from('rq_rqf_quote');
		$this->db->where('quote_id', $id);
		$query = $this->db->get();
		$result = $query->row();
		if(isset($result->assigned_underwriter)&&$result->assigned_underwriter!='')
		{
			$underwriter1 =  explode(',', $result->assigned_underwriter);
		}
		else
		{
			$underwriter1 = array();
		}
		$underwriters = $this->members_model->get_underwriters();
		$data['underwriter'] = '';
		$data['quote_id'] = $id;
		foreach($underwriters as $underwriter)
		{
			//echo $underwriter->id;
			//echo $id;
			if(in_array($underwriter->id, $underwriter1)){ $check = 'checked'; }else{ $check = ''; }
			$data['underwriter'] .= '<input type="checkbox" name="underwriter[]"  class="underwrit" '.$check.'  value="'.$underwriter->id.'">  '.$underwriter->name.'  ';
			
		}
		$data['link'] = base_url('administration/quotes/rate_by_quote1/'.$ref_id.'');
		$this->load->view('change_underwriter', $data);
	}
	public function changeUnderwriter()
	{
		$id =  $this->input->post('id');
		$url_link =  $this->input->post('url_link');
		$underwriter_ids =  $this->input->post('underwriter');
		$is_maual = $this->input->post('is_maual');
		if($is_maual>0)
		{
		$this->db->update('rq_rqf_quote_bundle', array('underwriters' => $underwriter_ids), array('id' => $id));
		}else{
		$this->db->update('rq_rqf_quote', array('assigned_underwriter' => $underwriter_ids), array('quote_id' => $id));
		}
		$this->db->affected_rows();
		//echo 'parent.fancybox.close();';
		
		$underwriter1 =  explode(',', $underwriter_ids);		
		$underwriters = $this->members_model->get_underwriters();
		$data['underwriter'] = '';
		$data['quote_id'] = $id;
		$data['url_link'] = $url_link;
		$i = 0;
		foreach($underwriters as $underwriter)
		{
			
			if(in_array($underwriter->id, $underwriter1)){
				
					$data1[$underwriter->name] = $underwriter->email;
				
				$i++;
				
			}
			
		}
		foreach($data1 as $mail_name => $mail_email)
		{
			$data['name'] = $mail_name;
			$data['email'] = $mail_email;
			$this->send_mail($data);
		}
	}
	public function send_mail($data, $msg='') {
			$config = array(           
            'charset' => 'utf-8',
            'wordwrap' => TRUE,
            'mailtype' => 'html'
       	 );
		 $mail_from = get_mail_from_id();
		 
		 $data['mail_content'] = getMailContent('assign_underwriter');
		 
		/* $searchReplaceArray = array(		 
		  '<quote_no>' => ''
		);
		
		$result = str_replace(
		  array_keys($searchReplaceArray), 
		  array_values($searchReplaceArray), 
		  $data['mail_content']->msg
		);*/
		
		$result  =  nl2br(str_replace('<quote_no>', '<a href="'.$data['url_link'].'">'.$data['url_link'].'</a>', $data['mail_content']->msg));
		$result  =  nl2br(str_replace('<full_name>', $data['name'], $result));
		 
		/*echo '<script> alert('.$result.'); </script>';*/
		   $this->load->library( 'email', $config);
		   $this->email->from($mail_from , '' );
		   $this->email->to($data['email']);
		   $this->email->subject( $data['mail_content']->sub );
		   $this->email->message($result);
		   echo $this->email->send();
		}
	public function sort_date_quote1()
	{
			$limit = 25;
			$total = $this->underwriter_model->count_underwriter(); 
			
			$data['quote'] = $this->quote_model->get_quote();
			
			$date_f = strtotime($this->input->post('date_f'));
			$date_t = strtotime($this->input->post('date_t'));
			
			$data['object'] = $this;
			
			$config['base_url'] = base_url('administration/quotes');
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			
			$data['total'] = $total;
			$this->pagination->initialize($config);

			$cn = 0; 
			$tbl_data = '';
			$tbl_data .= '<div>';
			$tbl_data .= '<div class="table_data">
						 <table>
						 	<table id="underwriterList" width="60%" class="dataTable">
							<thead>
							  <tr>
								<!--<th class="center" width="10%">Sr.</th>-->
								
								<th width="10%">Title <input type="checkbox" name="quote_re[]" value="" id="checkAll"></th>
								
								<th width="22%">Name</th>
								
								<th width="22%">Email</th>
								 <th width="22%">Requested On</th>
								<th width="22%">Status</th>
								<th width="22%">Action</th>
							  </tr>
							</thead>
						 <tbody class="table_body">';
			krsort($data['quote']);
			foreach($data['quote'] as $row) 
			{ 
				
				
				$date = strtotime(date("m/d/Y",strtotime($row->date_added)));
				if($date>=$date_f&&$date<=$date_t)
				{
					if($row->bundle_status == '') { if($row->perma_reject=='Reject'){ $status =  'Rejected';}else{ $status = 'Requested'; }} else { $status = $row->bundle_status; }
					if($row->bundle_id == 0) 
					{ 
						$generate_rt_sheet = '<a href="'.base_url('ratesheet/generate_ratesheet').'/'.$row->quote_id.'">Generate Ratesheet</a>';
						if($row->perma_reject!='Reject')
						{	
							$view_re = '<a href="javascript:;" onclick="changeStatus(&quot;reject&quot;, '.$row->quote_id.')">Reject</a>';
						 } else {$view_re = '';}
					 } 
					 else 
					 { 
					 	$generate_rt_sheet = '<a href="'.base_url().'administration/quotes/rate_by_quote/'.$row->quote_id.'">View</a>';  
						$view_re = '';
					}
					$cn++; 
					 $ids[] = $row->quote_id;
					$tbl_data .= '<tr>
									<!--<td>'.$cn.'</td>-->
									<td><input type="checkbox" name="quote_re[]" value="'.$row->quote_id.'"></td>
									<td>'.$row->contact_name.'</td>
									<td>'.$row->email.'</td>
									<td>'.date("m/d/Y",strtotime($row->date_added)).'</td>
									<td>'.$status.'</td>
									<td>'.$generate_rt_sheet.' '.$view_re.'  <a class="" href="javascript:;" onclick="assign_underwriter('.$row->quote_id.')">Assign Underwriter</a></td></td>
								</tr>';
				}
				else
				{
					$tbl_data .= '';
				}
			}
			$tbl_data .= '</tbody></table></div>';
			$tbl_data .= '<div class="export_link"><a class="btn btn-primary" target="_top" href="'.base_url().'administration/quotes/file_xls/'.implode('.', $ids).'">Export</a></div>';
			$tbl_data .= '</div>';
			 echo $tbl_data;
	}
	
	public function file_xls($ids='')
	{
		$ids = $_GET['ids'];
		$ids1 = explode(',', $ids);
		//echo $ids[1];
	  $this->load->helper('csv');
	  $i=1;
	 // $data1_array = '';
	     $data1_array[] = array ("No.", "U/W", "Date Submission Recv'd", "Time", "Date Quoted", "Time", "Quote Number", "Policy Effective Date", "DOT #", "MC or MX #", "Insured / DBA", "Base State", "Broker", "Current Carrier", "Remarks", "Date Bound", "AL", "PD", "Cargo", "Premium Amount");
	  foreach($ids1 as $id)
	  {
	  	if($id>0)
		{
			$result = $this->quote_model->get_xls_data($id);
			 //print_r($result);
			 $time = explode(' ', $result['quote']->date_added);
			 $indured_dba = $result['insured']->insured_fname.' '.$result['insured']->insured_mname.' '.$result['insured']->insured_lname.'/'.$result['insured']->insured_dba;
			 $broker = $result['quote']->contact_name.' '.$result['quote']->contact_middle_name.' '.$result['quote']->contact_last_name;
			/* if($i>0)
			 {
				  $data1_array[] = array('1', '', '', $time[1], $result['quote']->coverage_date, '', $result['quote']->quote_id, '', '', '', '', $indured_dba, $result['insured']->insured_state, $broker, '', '', $time[0], '', '', '', '');
			 }else
			 {*/
			  $data1_array[] = array($i, '', '', $time[1], $result['quote']->coverage_date, '', $result['quote']->quote_id, '', '', '', $indured_dba, $result['insured']->insured_state, $broker, '', '', $time[0], '', '', '', '');
			/*  }*/
			  $i++;
		  }
	  }
	  //print_r($data1_array);
	   $data_array =  array (
	   $data_array[] = array ("No.", "U/W", "Date Submission Recv'd", "Time", "Date Quoted", "Time", "Quote Number", "Policy Effective Date", "DOT #", "MC or MX #", "Insured / DBA", "Base State", "Broker", "Current Carrier", "Remarks", "Date Bound", "AL", "PD", "Cargo", "Premium Amount"),
						$data1_array
				); 
	  array_to_csv($data1_array, $download = "Log_up.csv");
	}
	
	public function check_quotes()
	{
			$limit = 25;
			$total = $this->underwriter_model->count_underwriter(); 
			//print_r($total);
		//	$data['bundles'] = $this->ratesheet_model->get_bundles();

			//$data['admins'] = $this->quote_model->get_quote();
			$data['object'] = $this;
			$data['rows'] = $this->quote_model->get_quote_rate('','');
			
			$quote_id = $data['rows'][0];
			$config['base_url'] = base_url('administration/quotes');
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			
			$data['total'] = $total;
			$this->pagination->initialize($config);

			$this->load->view('administration/rate_quote', $data);
	}
	public function sort_date_quote()
	{
			$limit = 25;
			$total = $this->underwriter_model->count_underwriter(); 
			$date_f = strtotime($this->input->post('date_f'));
			$date_t = strtotime($this->input->post('date_t'));
			$data['object'] = $this;
			$data['rows'] = $this->quote_model->get_quote_rate('','');
			
			$quote_id = $data['rows'][0];
			$config['base_url'] = base_url('administration/quotes');
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			
			$data['total'] = $total;
			$this->pagination->initialize($config);
			$cn = 0; 
			$tbl_data = '';
			$tbl_data .= '<div>';
			$tbl_data .= '<div class="table_data"><table><tbody class="table_body">';
			foreach($data['rows'] as $row) 
			{ 
				
				$pd_total = isset($row[0]['total_pd_premium']) ? $row[0]['total_pd_premium'] : '';
				$liability_total = isset($row[0]['total_liability_premium']) ? $row[0]['total_liability_premium'] : '';
				$date = strtotime(date("m/d/Y",strtotime($row[0]['creation_date'])));
				if($date>=$date_f&&$date<=$date_t)
				{
					$cn++; 
					$ids[] = $row[0]['quote_id'];
					$tbl_data .= '<tr>
								<!--<td class="center" width="10%">'.$cn.'</td>-->
								<td><input type="checkbox" name="quote_re[]" value="'.$row[0]['quote_id'].'"></td>
								<td width="15%">'.$row[0]['email'].'</td>
								<td width="15%">'.$row[0]['total_cargo_premium'].'</td>
								<td width="15%">'.$pd_total.'</td>
								<td width="15%">'.$liability_total.'</td>
								<td width="15%">'.date("m/d/Y",strtotime($row[0]['creation_date'])).'</td>
								<td width="15%"><a class="btn btn-primary" href="'.base_url().'administration/quotes/rate_by_quote1/'.$row[0]['quote_ref'].'">Versions</a> <a class="btn btn-primary" href="javascript:;" onclick="assign_underwriter('.$row[0]['quote_id'].')">Assign Underwriter</a></td>
							  </tr>';
				}
				else
				{
					$tbl_data .= '';
				}
			}
			$tbl_data .= '<table></tbody></div>';
			$tbl_data .= '<div class="export_link"><a class="btn btn-primary" target="_top" href="'.base_url().'administration/quotes/file_xls/'.implode('.', $ids).'">Export</a></div>';
			$tbl_data .= '</div>';
			 echo $tbl_data;
	}
	public function changeStatus()
	{
		$action = $this->input->post('action');
		$id = $this->input->post('id');
		
		$this->db->update('rq_rqf_quote', array('perma_reject' => 'Reject'), array('quote_id' => $id));
		echo $this->db->affected_rows();
	}
	public function rate_by_quote1()
	{
			$limit = 25;
			$total = $this->underwriter_model->count_underwriter(); 
			
			$data['admins'] = $this->quote_model->get_quote();
			$data['object'] = $this;
			$quote_id = $this->uri->segment(4);
			
			$data['rows'] = $this->quote_model->get_bundle_by_quote1($quote_id);
			
			
			$quote_id = isset($data['rows'][0]) ? $data['rows'][0] : '';
			$config['base_url'] = base_url('administration/quotes');
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			
			$data['total'] = $total;
			$this->pagination->initialize($config);

			$this->load->view('administration/rate_by_quote', $data);
	}
	
	/*By Ashvin Patel 9/may/2014*/
	public function rate_by_quote2()
	{
			$limit = 25;
			$total = $this->underwriter_model->count_underwriter(); 
			
			$data['admins'] = $this->quote_model->get_quote();
			$data['object'] = $this;
			$quote_id = $this->uri->segment(4);
			
			$data['rows'] = $this->quote_model->get_bundle_by_quote2($quote_id);
			
			//print_r($data['rows']);
			$quote_id = isset($data['rows'][0]) ? $data['rows'][0] : '';
			$config['base_url'] = base_url('administration/quotes');
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			
			$data['total'] = $total;
			$this->pagination->initialize($config);

			$this->load->view('administration/rate_by_quote_maual', $data);
	}
	/*By Ashvin Patel 9/may/2014*/
	
	public function rate_by_quote()
	{
			$limit = 25;
			$total = $this->underwriter_model->count_underwriter(); 
			
			$data['admins'] = $this->quote_model->get_quote();
			$data['object'] = $this;
			$quote_id = $this->uri->segment(4);
			
			$data['rows'] = $this->quote_model->get_bundle_by_quote($quote_id);
			
			
			$quote_id = $data['rows'][0];
			$config['base_url'] = base_url('administration/quotes');
			$config['total_rows'] = $total;
			$config['per_page'] = $limit;
			
			$data['total'] = $total;
			$this->pagination->initialize($config);

			$this->load->view('administration/rate_by_quote', $data);
	}
	
	
	// start for header///////
	
	public function header_view($id)
	{
		if($id == 0)
		{
			$res['result'] = $this->templat_model->get_head_logo();
			$this->load->view('administration/header_logo', $res);
		}else{
			$res['row'] = $this->templat_model->get_header_info($id);
			$this->load->view('administration/header_logo', $res);
		}
			
	}
	
	public function insert_data($id)
	{
		$this->form_validation->set_rules('h_logo_title', 'Header Logo Title', 'required');
		if($id == 0){
			if (empty($_FILES['logo_image']['name']))
			{
			$this->form_validation->set_rules('logo_image', 'Header Logo Image', 'required');
			}
		}
		if ($this->form_validation->run() === TRUE)
		{			
			if(isset($_FILES['logo_image']['name']) && $_FILES['logo_image']['name']!=''){
				
				$returnval=$this->upload_mainimage('logo_image');	
					//echo "<pre>";print_r($returnval);
					if(is_array($returnval) && isset($returnval['error'])){
						$this->session->set_flashdata('error', @$returnval['error']);
						redirect(uri_string());exit;
					}
					elseif(is_array($returnval) && isset($returnval['mainimage'])){
						$mainimage=$returnval['mainimage'];
						
					} 
				}
			if($id == 0)
			{
				if($this->input->post('status') == 'active')
				{
					$up_status = $this->templat_model->update_status('real_header_table','pending');
				}
				$data['title'] = $this->input->post('h_logo_title');
				$data['logo_name'] = $mainimage;
				$data['create_date'] = date('Y-m-d H:i:s');
				$data['status'] = $this->input->post('status');
				$this->templat_model->insert_logo($data);
				redirect('administration/template/header_view/0');
			}else{
				if($this->input->post('status') == 'active')
				{
					$up_status = $this->templat_model->update_status('real_header_table','pending');
				}

				if(isset($_FILES['logo_image']['name']) && $_FILES['logo_image']['name'] !='')
				{
					$data['logo_name'] = $mainimage;
					$this->delete_logo($this->input->post('current_logo'), 'header_logo');
				}
				$data['title'] = $this->input->post('h_logo_title');
				$data['create_date'] = date('Y-m-d H:i:s');
				$data['status'] = $this->input->post('status');
				$this->templat_model->update_logo($data, $id);
				redirect('administration/template/header_view/0');
			}
				//redirect('administration/template/header_view/0');
		} else {
		if($id == 0)
			{
				$res['result'] = $this->templat_model->get_head_logo();
				$this->load->view('administration/header_logo', $res);
			}else{
				$res['row'] = $this->templat_model->get_header_info($id);
				$this->load->view('administration/header_logo', $res);
			}
		}
	}
	
	public function upload_mainimage($fieldname)
	{	
		  $imgPath = realpath(APPPATH . '../uploads/header_logo');
		  $config['upload_path'] = './uploads/header_logo'; /* NB! create this dir! */
		  $config['allowed_types'] = IMAGE_TYPES;
		  $config['max_size']  = '0';
		  $config['max_width']  = '0';
		  $config['max_height']  = '0';	
		  $config['overwrite'] = FALSE;
		  $this->load->library('upload', $config);
		  $configThumb = array();
		  $configThumb['image_library'] = 'gd2';
		  $configThumb['source_image'] = '';
		  $configThumb['create_thumb'] = TRUE;
		  $configThumb['maintain_ratio'] = FALSE;
		  $configThumb['width'] = 100;
		  $configThumb['height'] = 100;		  
		  $this->load->library('image_lib');
		 
		 if(isset($_FILES[$fieldname]['name']) && $_FILES[$fieldname]['name']!=''){		
				$upload = $this->upload->do_upload($fieldname);									
				if(!$upload){
					return array('error' => $this->upload->display_errors());
				}else{
					
					$data = $this->upload->data();
				}
			
						
				if($data['is_image'] == 1) {
				  $configThumb['source_image'] = $data['full_path'];
				  $configThumb['new_image'] = $imgPath . '/thumbs/';
				  $this->image_lib->initialize($configThumb);
				  $this->image_lib->resize();
				}						
			}
			$image_name   = (isset($_REQUEST['main_image_hid']))? $_REQUEST['main_image_hid']:''; 
			$story=array();
			$image_to_delete = '';
			if($image_name!='' && isset($data['file_name']) && $data['file_name']!=''){	
				$thumbImg=explode('.',$image_to_delete);	
				$thumbsimage=($thumbImg[0]!='') ? $thumbImg[0].'.'.$thumbImg[1] :'';
				$story = array('mainimage'=>$data['file_name']);									
			}
			elseif($image_name!='' && (!isset($data['file_name']))){			
				$story = array('mainimage'=>$image_name);
			}
			elseif($image_name=='' && isset($data['file_name']) && $data['file_name']!=''){		
				$story = array('mainimage'=>$data['file_name']);
			} 
			return $story;
		
		}
	//function delete_logo('filename', 'foldername')
	
	public function delete_logo($filename, $foldername)
	{
		echo unlink('./uploads/'.$foldername.'/'.$filename);
		echo unlink('./uploads/'.$foldername.'/'.'thumbs/'.$filename);
	}


	
	public function delete_head_logo($id)
		{
			$row = $this->db->where('id',$id)->get('real_header_table')->row();
			unlink('./uploads/header_logo/'.$row->logo_name);
			unlink('./uploads/header_logo/thumbs/'.$row->logo_name);
			$this->templat_model->delete_head_logo_mod($id);
			redirect('administration/template/header_view/0');
			//redirect($this->config->item('prevous_url'));
			exit;
		}
		
		// End for header///////

	
	
	
	// start for footer///////
	public function footer_edit($id)
	{
		if($id == 0){
		$res['result'] = $this->templat_model->get_footer();
		$this->load->view('administration/footer_e', $res);
		}else{
		$res['row'] = $this->templat_model->get_footer_info($id);
		$this->load->view('administration/footer_e', $res);
		}
	}
	
	
	
	
	public function footer_sub($id)
	{	
			$this->form_validation->set_rules('footer_title', 'Title', 'required');
			$this->form_validation->set_rules('footer_content', 'Footer Description', 'required');
		if ($this->form_validation->run() === TRUE)
		{			
	
			if($this->input->post('status') == 'active')
			{
				$up_status = $this->templat_model->update_status('real_footer_table','pending');
			}
				$data['title'] = $this->input->post('footer_title');
				$data['footer_content'] = $this->input->post('footer_content');
				$data['create_date'] = date('Y-m-d H:i:s');
				$data['status'] = $this->input->post('status');
			if($id == 0){
				$this->templat_model->insert_footer($data);
			}
			else{
				$this->templat_model->update_footer($data, $id);
			}
			redirect('administration/template/footer_edit/0');
		}else{
		
				if($id == 0){
				$res['result'] = $this->templat_model->get_footer();
				$this->load->view('administration/footer_e', $res);
				}else{
				$res['row'] = $this->templat_model->get_footer_info($id);
				$this->load->view('administration/footer_e', $res);
			}
		}
	}
	
	
	
	public function delete_footer($id)
	{
		$this->templat_model->delete_footer($id);
		redirect('administration/template/footer_edit/0');
	}
	// End for Footer///////
	
	
	
	
	
	
	// start for Video///////
	public function video($id)
	{
		$res['page_result'] = $this->templat_model->get_page_for_vdo();
	 if($id == 0)
	 {
		$res['result'] = $this->templat_model->get_videos();
		$this->load->view('administration/video', $res);
		}else
		{
		$res['row'] = $this->templat_model->get_videos_info($id);
		$this->load->view('administration/video', $res);
		}
	}

	public function insert_video($id)
	{ 
		$this->form_validation->set_rules('video_title', 'Video Title', 'required');
		if($id == 0)
		{
			if (empty($_FILES['video']['name']))
			{
				$this->form_validation->set_rules('video', 'Video', 'required');
			}
		} 
		if ($this->form_validation->run() === TRUE)
		{  
			if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
				unset($config);
				 $path = explode('.', $_FILES['video']['name']);
				 $exetention = end($path);
				$time = time();
				$configVideo['upload_path'] = './uploads/videos';
				$configVideo['max_size'] = '100000';
				$configVideo['allowed_types'] = 'flv|3gp|mp4|mp4';
				$configVideo['overwrite'] = FALSE;
				$configVideo['remove_spaces'] = TRUE;
				$video_name = $time.'.'.$exetention;
				$configVideo['file_name'] = $video_name;

				$this->load->library('upload', $configVideo);
				$this->upload->initialize($configVideo);
				if (!$this->upload->do_upload('video')) { 
					$error_msg = $this->upload->display_errors();
					$this->session->set_flashdata('error', @$error_msg);
					redirect(uri_string());exit;
				} else {
					$mainimage = $video_name;
				
				}/*
					$videoDetails = $this->upload->data();
					return 1;
					//$data['result'] = "Successfully Uploaded";
					//$this->load->view('upload',$data);
					*/
			if(isset($_FILES['video']['name']) && $_FILES['video']['name']!=''){
			
				$data['video_name'] = $mainimage;
				if($this->input->post('current_vdo') != ''){
				$this->delete_vdo($this->input->post('current_vdo'));
				}
			}
			$data['vdo_title'] = $this->input->post('video_title');
			$data['page_id'] = $this->input->post('page_id');
			$data['create_date'] = date('Y-m-d H:i:s');
			$data['vdo_status'] = $this->input->post('status');
			
			if($id == 0){
			$res = $this->templat_model->insert_vdo($data);
			redirect('administration/template/video/0');
			}else{
			$res = $this->templat_model->update_vdo($data, $id);
			redirect('administration/template/video/0');
			}	
		} else {
			//$this->input->post('ads_name');
			$data['vdo_title'] = $this->input->post('video_title');
			$data['page_id'] = $this->input->post('page_id');
			//$data['create_date'] = date('Y-m-d H:i:s');
			$data['vdo_status'] = $this->input->post('status');
			$data['modified_date'] = date('Y-m-d H:i:s');
			$this->templat_model->update_vdo($data, $id);
			
			$res['row'] = $this->templat_model->get_videos_info($id);
			$this->load->view('administration/video', $res);
		}
	} else {
		
		$res['page_result'] = $this->templat_model->get_page_for_vdo();
	 if($id == 0)
	 {
		$res['result'] = $this->templat_model->get_videos();
		$this->load->view('administration/video', $res);
		}else
		{
		$res['row'] = $this->templat_model->get_videos_info($id);
		$this->load->view('administration/video', $res);
		}
		//$this->video('');
	}
}

	
	public function delete_video($id)
	{
			$row = $this->db->where('id',$id)->get('real_video_table')->row();
			unlink('./uploads/videos/'.$row->video_name);
			$this->db->set('right_bar', 0);
			$this->db->where('right_bar', 'video-'.$id);
			$this->db->update('real_pages');
			$this->templat_model->delete_video_mod($id);
			redirect('administration/template/video/0');
			
			exit;
	}
	
	
	public function delete_vdo($filename)
	{
		unlink("./uploads/videos/".$filename);
	}

		// End for Video///////
   
   
   	// start for Ads///////
	
	
	public function ads($id)
	{
		if($id == 0)
		{
			$res['result'] = $this->templat_model->get_ads();
			$this->load->view('administration/ads', $res);
		}else{
			$res['row'] = $this->templat_model->get_ads_info($id);
			$this->load->view('administration/ads', $res);
		}
	}
	
	
	public function insert_ads($id)
	{
				
		$this->form_validation->set_rules('ads_name', 'AD Name', 'required');
		$this->form_validation->set_rules('ads_link', 'AD Link', 'required');
		
		if($id == 0){
		if (empty($_FILES['ads_img']['name']))
		{
			$this->form_validation->set_rules('ads_img', 'AD Image', 'required');
		}
		}
		if ($this->form_validation->run() === TRUE)
		{ 
			if(isset($_FILES['ads_img']['name']) && $_FILES['ads_img']['name']!=''){
				$returnval=$this->upload_ads_image('ads_img');	
					//echo "<pre>";print_r($returnval);
					if(is_array($returnval) && isset($returnval['error'])){
						$this->session->set_flashdata('error', @$returnval['error']);
						redirect(uri_string());exit;
					}
					elseif(is_array($returnval) && isset($returnval['mainimage'])){
						$mainimage=$returnval['mainimage'];
					} 
				}
			if($id == 0)
			{
				$data['name'] = $this->input->post('ads_name');
				$data['ads_img'] =$mainimage;
				$data['ads_link'] = $this->input->post('ads_link');
				$data['create_date'] = date('Y-m-d H:i:s');
				$data['status'] = $this->input->post('status');
				$this->templat_model->insert_ads($data);
				redirect('administration/template/ads/0');
			}else{
			if(isset($_FILES['ads_img']['name']) && $_FILES['ads_img']['name'] !='')
			{
				$data['ads_img'] = $mainimage;
				$this->delete_logo($this->input->post('current_logo'),'ads_images');
			}
				$data['name'] = $this->input->post('ads_name');
				$data['ads_link'] = $this->input->post('ads_link');
				$data['create_date'] = date('Y-m-d H:i:s');
				$data['status'] = $this->input->post('status');
				$this->templat_model->update_ads($data, $id);
				redirect('administration/template/ads/0');
			}

		} else {
			if($id == 0)
			{
				$res['result'] = $this->templat_model->get_ads();
				$this->load->view('administration/ads', $res);
			}else{
				$res['row'] = $this->templat_model->get_ads_info($id);
				$this->load->view('administration/ads', $res);
			}
		}
	}
	
	
	public function upload_ads_image($fieldname)
	{	
		  $imgPath = realpath(APPPATH . '../uploads/ads_images');
		  $config['upload_path'] = './uploads/ads_images'; /* NB! create this dir! */
		  $config['allowed_types'] = IMAGE_TYPES;
		  $config['max_size']  = '0';
		  $config['max_width']  = '0';
		  $config['max_height']  = '0';	
		  $config['overwrite'] = FALSE;
		  $this->load->library('upload', $config);
		  $configThumb = array();
		  $configThumb['image_library'] = 'gd2';
		  $configThumb['source_image'] = '';
		  $configThumb['create_thumb'] = TRUE;
		  $configThumb['maintain_ratio'] = FALSE;
		  $configThumb['width'] = 100;
		  $configThumb['height'] = 100;		  
		  $this->load->library('image_lib');
		 
		 if(isset($_FILES[$fieldname]['name']) && $_FILES[$fieldname]['name']!=''){		
				$upload = $this->upload->do_upload($fieldname);									
				if(!$upload){
					return array('error' => $this->upload->display_errors());
				}else{
					$data = $this->upload->data();
				}
			
						
				if($data['is_image'] == 1) {
				  $configThumb['source_image'] = $data['full_path'];
				  $configThumb['new_image'] = $imgPath . '/thumbs/';
				  $this->image_lib->initialize($configThumb);
				  $this->image_lib->resize();
				}						
			}
			$image_name   = (isset($_REQUEST['main_image_hid']))? $_REQUEST['main_image_hid']:''; 
			$story=array();
			$image_to_delete = '';
			if($image_name!='' && isset($data['file_name']) && $data['file_name']!=''){	
				$thumbImg=explode('.',$image_to_delete);	
				$thumbsimage=($thumbImg[0]!='') ? $thumbImg[0].'.'.$thumbImg[1] :'';
				$story = array('mainimage'=>$data['file_name']);									
			}
			elseif($image_name!='' && (!isset($data['file_name']))){			
				$story = array('mainimage'=>$image_name);
			}
			elseif($image_name=='' && isset($data['file_name']) && $data['file_name']!=''){		
				$story = array('mainimage'=>$data['file_name']);
			} 
			return $story;
		
		}

		public function delete_ads($id)
		{
			$row = $this->db->where('id',$id)->get('real_ads')->row();
			unlink('./uploads/ads_images/'.$row->ads_img);
			unlink('./uploads/ads_images/thumbs/'.$row->ads_img);
			$this->db->set('right_bar', 0);
			$this->db->where('right_bar', 'ad-'.$id);
			$this->db->update('real_pages');
			$this->templat_model->delete_ads($id);
			redirect('administration/template/ads/0');
			//redirect($this->config->item('prevous_url'));
			exit;
		}
			// End for Ads///////

		public function add_video(){
			if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
				unset($config);
				$date = date("ymd");
				$configVideo['upload_path'] = './uploads/videos';
				$configVideo['max_size'] = '10240';
				$configVideo['allowed_types'] = 'jpg|avi|flv|wmv|mp3|3gp';
				$configVideo['overwrite'] = FALSE;
				$configVideo['remove_spaces'] = TRUE;
				$video_name = $date.$_FILES['video']['name'];
				$configVideo['file_name'] = $video_name;

				$this->load->library('upload', $configVideo);
				$this->upload->initialize($configVideo);
				if (!$this->upload->do_upload('video')) {
					echo $this->upload->display_errors();
					return 0;
				} else {
					$videoDetails = $this->upload->data();
					return 1;
					//$data['result'] = "Successfully Uploaded";
					//$this->load->view('upload',$data);

				}
			}
	}
		
		public function show_video()
		{
			$this->load->view('upload');
		}
		
	public function ajax_list_quotes()
	{
		//$array = array('status' => 'Active');
		$this->datatables
         ->select("contact_name,contact_last_name,email,status,quote_id", FALSE)
		 ->from('rq_rqf_quote');
		// ->where($array)
        echo $this->datatables->generate();
	}
	

	
	public function ajax_list_quote_rates()
	{
		$id = $this->session->userdata('admin_id');
		//$array = array('rq_rqf_quote_bundle.quote_id' => $quote_id);
		$this->datatables
		->select("quote_id,contact_name,rq_rqf_quote.email,phone_no,name,DATE_FORMAT(coverage_date, '%d-%M-%Y'),rq_rqf_quote.status", FALSE)
	 	->from('rq_rqf_quote_bundle')
	 	->join('rq_rqf_quote', 'rq_rqf_quote.quote_id=rq_rqf_quote_bundle.quote_id')
		->join('rq_ratesheet', 'rq_ratesheet.quote_id=rq_rqf_quote_bundle.quote_id and rq_ratesheet.id=rq_rqf_quote_bundle.cargo_id')
		->join('rq_ratesheet', 'rq_ratesheet.quote_id=rq_rqf_quote_bundle.quote_id and rq_ratesheet.id=rq_rqf_quote_bundle.cargo_id')
		->join('rq_ratesheet_damage_ratesheet', 'rq_ratesheet_damage_ratesheet.quote_id=rq_rqf_quote_bundle.quote_id and rq_ratesheet_damage_ratesheet.id=rq_rqf_quote_bundle.pd_id')
		->join('rq_ratesheet_liability', 'rq_ratesheet_liability.quote_id=rq_rqf_quote_bundle.quote_id and rq_ratesheet_liability.id=rq_rqf_quote_bundle.liability_id');
		//->where('rq_rqf_quote.requester_id',$this->session->userdata('underwriter_id'));
		//->where($where);
		echo $this->datatables->generate();
	}
	
	public function ajax_status()
	{
		$status['status']=$this->input->post('status');
		$query=$this->db->update('rq_underwriter', $status, array('id'=>$this->input->post('id')));
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
	
	public function underwriter_add()
	{
		$data['header'] = array('title'=>'Add Underwriter');
		
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$underwriter['name'] = $this->input->post('name');
			$underwriter['email'] = $this->input->post('email');
			$underwriter['password'] = trim($this->input->post('password'));
			$underwriter['created_date'] = date('Y-m-d H:i:s');
			
			$this->underwriter_model->insert_update_underwriter($underwriter);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/underwriter').'";
				</script>';
		}
		else
		{
			$this->load->view('administration/underwriter_add',$data);
		}
	}
	
	public function underwriter_edit($id='')
	{
		$data['header'] = array('title'=>'Edit Underwriter');
		$data['underwriter']=$this->underwriter_model->get_underwriter_info($id);
		
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		
		if ($this->form_validation->run() === TRUE)
		{
			$underwriter['name'] = $this->input->post('name');
			$underwriter['email'] = $this->input->post('email');
			
			if($this->input->post('password')!='')
			$underwriter['password'] = trim($this->input->post('password'));
			
			$underwriter['status'] = $this->input->post('status');
			$underwriter['updated_date'] = date('Y-m-d H:i:s');
			
			$this->underwriter_model->insert_update_underwriter($underwriter,$id);
			
			echo '<script type="text/javascript">
					parent.$.fancybox.close();
					parent.location.href = "'.base_url('administration/underwriter').'";
				</script>';
		}
		else
		{
			$this->load->view('administration/underwriter_edit',$data);
		}
	}
	
	public function ajax_underwriter_delete()
	{
		$status['delete']=1;
		$query=$this->db->update('rq_underwriter', $status, array('id'=>$this->input->post('id')));
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
	
}	
/* End of file test_post.php */
/* Location: ./application/controllers/test_post.php */