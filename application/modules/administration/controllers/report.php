<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends RQ_Controller {


	public function __construct()
	{
		parent::__construct();

		$this->load->model('admin_model');
		
	}
	
	public function index()
	{
		
		if($this->session->userdata('admin_id'))
		{
			
			$data['total_cancelled_property'] = $this->admin_model->count_cancelled_property();
			$data['total_pending_property'] = $this->admin_model->count_pending_property();
			$data['total_active_property'] = $this->admin_model->count_active_property();
			$data['total_property'] = $this->admin_model->count_all_property();
			$data['top_members_by_listings'] = $this->admin_model->top_members_by_listings();
			$data['top_locations_by_listings'] = $this->admin_model->top_locations_by_listings();
			$data['top_marketvalue_listings'] = $this->admin_model->top_marketvalue_listings();
			$data['object'] = $this;
			
			$this->load->view('administration/report',$data);
		
		}else{
			$this->form_validation->set_rules('username', 'Username', 'required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() === FALSE)
        {
			$this->load->view('administration/login');
		  }else{
				// if user is valid
				$valid_user = $this->admin_model->validate_admin();
				
				if($valid_user) 
				{
					 $data = array(
								'admin_username' => $valid_user->email,
								'admin_name' => $valid_user->name,
								'admin_id' => $valid_user->id,
								'admin_type' => $valid_user->admin_type
					 );
					 
					 $this->session->set_userdata($data);
					 $this->session->set_flashdata('success', 'You have successfully logged into your account');
					 redirect(uri_string());
				}
				
				else 
				{
					 $this->session->set_flashdata('error', 'Information submitted is incorrect.');
					 redirect(uri_string());
				}
			  }
		}
	}
	
	public function get_member_info($id)
	{
			$this->db->where('id',$id);
			
		if( $query = $this->db->get($this->config->item('member_table')) )
		{
			
			if ($query->num_rows() > 0)
			{
				return $query->row();
			}
		}
		return FALSE;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */