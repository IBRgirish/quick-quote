<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Property  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->helper('my_helper');
		$this->load->model('property_model');
		$this->load->library('pagination');  
		$this->load->helper('date');
		$this->load->library('customclass');
	}
	public function index()
	{
			$data['fields'] = array(
			'property_title' => 'Property Title',
			'posted_by' => 'Posted By',
			'current_market_value' => 'Current Market Value',
			'create_date' => 'Listing Date',
			'modify' => 'Modify',
			'delete' => 'Delete',
			'valuation' => 'Valuation',
			'request_status' => 'Listing Status'
		);
		
		$limit = 25;
		
		$request_status=isset($_GET['request_status']) ? $_GET['request_status'] :$this->input->get('request_status');
		$member_id=isset($_GET['member_id']) ? $_GET['member_id'] :$this->input->get('member_id');
		$country=isset($_GET['country']) ? $_GET['country'] :$this->input->get('country');
		
		
		$sort_by=isset($_GET['sort_by']) ? $_GET['sort_by'] : 'create_date';
		$sort_order=isset($_GET['sort_order']) ? $_GET['sort_order'] : 'desc';
		
		$total = $this->property_model->count_property($request_status , $member_id , $country ); 
		$data['listings'] = $this->property_model->list_property($limit, 0,$request_status, $member_id, $country, $sort_by, $sort_order);
		$data['object'] = $this;
		$config['base_url'] = base_url('administration/property');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		$data['total'] = $total;
		$this->pagination->initialize($config);
		$this->load->view('administration/property', $data);
	}
	
	public function page($offset='')
    {
		$limit = 25;
		$data['fields'] = array(
			'property_title' => 'Property Title',
			'posted_by' => 'Posted By',
			'current_market_value' => 'Current Market Value',
			'create_date' => 'Listing Date',
			'modify' => 'Modify',
			'delete' => 'Delete',
			'request_status' => 'Listing Status'
		);
		
		$offset = isset($offset)&& !empty($offset) ? $offset : '0';
		$request_status=isset($_GET['keyword']) ? $_GET['keyword'] :$this->input->get('keyword');
		$sort_by=isset($_GET['sort_by']) ? $_GET['sort_by'] : 'create_date';
		$sort_order=isset($_GET['sort_order']) ? $_GET['sort_order'] : 'desc';
		$member_id=isset($_GET['member_id']) ? $_GET['member_id'] :$this->input->get('member_id');
		$country=isset($_GET['country']) ? $_GET['country'] :$this->input->get('country');
		
		$total = $this->property_model->count_property($request_status , $member_id , $country ); 
		$data['listings'] = $this->property_model->list_property($limit, $offset,$request_status , $member_id , $country , $sort_by, $sort_order);
		$data['object'] = $this;
		$config['base_url'] = base_url('administration/property');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		$data['total'] = $total;
		$this->pagination->initialize($config);
		$this->load->view('administration/property', $data);
    }
	 public function edit($id)
	 {
				
				$this->form_validation->set_rules('property_title', 'Property Title', 'required');
				$this->form_validation->set_rules('property_description', 'Property Description', 'required');
				//echo $this->input->post('property_title');
				//print_r($this->input->post('property_title'));

				
        if ($this->form_validation->run() === TRUE)
        {
		
						$mainimage='';
				if(isset($_FILES['main_image']['name']) && $_FILES['main_image']['name']!=''){
				
					$returnval=$this->upload_mainimage('main_image');	
					//echo "<pre>";print_r($returnval);
					if(is_array($returnval) && isset($returnval['error'])){
						$this->session->set_flashdata('error', @$returnval['error']);
						redirect(uri_string());exit;
					}
					elseif(is_array($returnval) && isset($returnval['mainimage'])){
						$mainimage=$returnval['mainimage'];
					} 
				}	
				$floor_image_uploaded='';
				if(isset($_FILES['floor_image']['name']) && $_FILES['floor_image']['name']!=''){
				
				$floor_image = 'floor_image';
				$returnval=$this->upload_mainimage($floor_image);
				if(is_array($returnval) && isset($returnval['error'])){
					$this->session->set_flashdata('error', @$returnval['error']);
					redirect(uri_string());exit;
				}
				elseif(is_array($returnval) && isset($returnval['mainimage'])){
					$floor_image_uploaded= $returnval['mainimage'];
				} 
				//'google_map' => $this->input->post('google_map'),
				}
	
				$data = array(		'property_title' => $this->input->post('property_title'),
									'property_description' => $this->input->post('property_description'),
									'num_rooms' => $this->input->post('num_rooms'),
									'toilet' => $this->input->post('toilet'),
									'bath' => $this->input->post('bath'),
									'garage' => $this->input->post('garage'),
									'size_of_land' => $this->input->post('size_of_land'),
									'size_of_floor' => $this->input->post('size_of_floor'),
									'year_built' => $this->input->post('year_built'),
									'selling_price' => $this->input->post('selling_price'),
									'location' => $this->input->post('location'),
									'house_num' => $this->input->post('house_num'),
									'street' => $this->input->post('street'),
									'zip_code' => $this->input->post('zip_code'),
									'country' => $this->input->post('country'),
									'currency' => $this->input->post('currency'),
									'state' => $this->input->post('state'),
									'city' => $this->input->post('city'),
									'current_market_value' => $this->input->post('current_market_value'),
									
									'design_concept' => $this->input->post('design_concept'),
									'quality_of_finish' => $this->input->post('quality_of_finish'),
									'neighborhood' => $this->input->post('neighborhood'),
									'property_type' => $this->input->post('property_type'),
									'property_category' => $this->input->post('property_category'),
									'available_for' => $this->input->post('available_for'),
									'year_built' => $this->input->post('year_built'),
									'annual_earnings' => $this->input->post('annual_earnings'),
									'realestate_taxes' => $this->input->post('realestate_taxes'),
									'apprisal_date' => $this->input->post('apprisal_date'),
									'apprisal_value' => $this->input->post('apprisal_value'),
									'to_purchased_in' => $this->input->post('to_purchased_in'),
									'posted_by' => $this->input->post('posted_by_user'),
									'request_status' => $this->input->post('request_status'),
									'status' => '1'
								  );
								 
								 if(!empty($mainimage)){
									 $data2 = array('pictures' => $mainimage);
									 $data = $data + $data2;
								}
							  	
								if(!empty($floor_image_uploaded)){
									 $data3 = array('floor_image' => $floor_image_uploaded);
									  $data = $data + $data3;
								
								}
									
				if($this->input->post('id') > 0)
				{
					$data['modified_date'] = date('Y-m-d H:i:s');
				}else{
					$data['create_date'] = date('Y-m-d H:i:s');
					$data ['posted_by'] = $this->input->post('posted_by');
				}
   	        $property_id = $this->property_model->insert_update_property_listings($data, $id);
			 
			 if($id >0)
			{
				$pid =$id; 
			}else{
				$pid =$property_id; 
			}
			 
			  if($id==0){
					//$realestate_valuation = 'administration/property/edit2/'.$pid;
					//redirect($realestate_valuation);
					redirect('administration/property');
				
			 }else{
				redirect('administration/property');
			}
			
			 exit;
		  }
            $listing = $this->property_model->get_property_info($id);
            $data['listing'] = $listing;
			$data['object'] = $this;
			$this->load->view('administration/property_edit', $data);
	}
	
	 public function edit2($id)
	 {
		$this->form_validation->set_rules('annual_gross_rent', 'Annual gross rent', 'required');
		if ($this->form_validation->run() === TRUE)
        {
			$data = array( 
							'property_id' => $id,
							'annual_gross_rent' => $this->input->post('annual_gross_rent'),
						    'vacancy_factor' => $this->input->post('vacancy_factor'),
							'operating_expenses' => $this->input->post('operating_expenses'),
						    'annual_change_rent' => $this->input->post('annual_change_rent'),
							'annual_change_expenses' => $this->input->post('annual_change_expenses'),
						    'loan_to_value' => $this->input->post('loan_to_value'),
							'stated_annual_interest' => $this->input->post('stated_annual_interest'),
						    'loan_term' => $this->input->post('loan_term'),
							'perOf_price_improvements' => $this->input->post('perOf_price_improvements'),
						    'cpi_annual_increase' => $this->input->post('cpi_annual_increase'),
							'real_discount_rate' => $this->input->post('real_discount_rate'),
						    'cap_rate' => $this->input->post('cap_rate'),
							'transaction_costs' => $this->input->post('transaction_costs'),
						    'cap_rate_purchase' => $this->input->post('cap_rate_purchase'),
							'recovery_period' => $this->input->post('recovery_period'),
						    'income_tax_rate' => $this->input->post('income_tax_rate'),
							'capital_gains' => $this->input->post('capital_gains'),
						    'loan_amount' => $this->input->post('loan_amount'),
							'equity_required' => $this->input->post('equity_required'),
						    'mortgage_loan' => $this->input->post('mortgage_loan')
						);
						
					$property_id = $this->property_model->insert_update_realestate_valuation($data, $id);
				redirect('administration/property');
				 exit;
		}
			$listing = $this->property_model->get_realestate_valuation_info($id);
           $data['listing'] = $listing;
           $data['id'] = $id;
			
			$this->load->view('administration/property_edit2',$data);
	 }
	
	 function delete_mainimage($id)
	 {
			$image_delete = $_GET['image']; 
			if($image_delete=='floor_image'){
				$data = array('floor_image' => '');
			}else{
				$data = array('pictures' => '');
			}
			$this->property_model->delete_mainimage($id,$data);
		    $pictures = $_GET['name'];
			$pathimg = 'uploads/mainimage'.'/'.$pictures;
			$image = explode('.',$pictures);
			$pathimg1 = 'uploads/mainimage/thumbs'.'/'.$image[0].'_thumb.'.$image[1];
			 unlink($pathimg);
			 unlink($pathimg1);
			redirect($this->config->item('prevous_url'));
			exit;
	 }
	
	 function delete($id)
	 {
			$this->property_model->delete_property_listing($id);
			//redirect('administration/admins');//changed on 29aug12
			redirect($this->config->item('prevous_url'));
			exit;
	 }
	 
	public function get_member_info($id)
	{
			$this->db->where('id',$id);
			
		if( $query = $this->db->get($this->config->item('member_table')) )
		{
			
			if ($query->num_rows() > 0)
			{
				return $query->row();
			}
		}
		return FALSE;
	}
	
	public function get_realestate_valuation($pid)
	{
			$this->db->where('property_id',$pid);
			
		if( $query = $this->db->get($this->config->item('realestate_valuation_table')) )
		{
			
			if ($query->num_rows() > 0)
			{
				return $query->row();
			}
		}
		return FALSE;
	}

	
	public function upload_mainimage($fieldname)
	{	
		  $imgPath = realpath(APPPATH . '../uploads/mainimage');
		  $config['upload_path'] = './uploads/mainimage'; /* NB! create this dir! */
		  $config['allowed_types'] = IMAGE_TYPES;
		  $config['max_size']  = '0';
		  $config['max_width']  = '0';
		  $config['max_height']  = '0';	
		  $config['overwrite'] = FALSE;
		  $this->load->library('upload', $config);
		  $configThumb = array();
		  $configThumb['image_library'] = 'gd2';
		  $configThumb['source_image'] = '';
		  $configThumb['create_thumb'] = TRUE;
		  $configThumb['maintain_ratio'] = FALSE;
		  $configThumb['width'] = 100;
		  $configThumb['height'] = 100;		  
		  $this->load->library('image_lib');
		 
		 if(isset($_FILES[$fieldname]['name']) && $_FILES[$fieldname]['name']!=''){		
				$upload = $this->upload->do_upload($fieldname);									
				if(!$upload){
					return array('error' => $this->upload->display_errors());
				}else{
					$data = $this->upload->data();
				}
			
						
				if($data['is_image'] == 1) {
				  $configThumb['source_image'] = $data['full_path'];
				  $configThumb['new_image'] = $imgPath . '/thumbs/';
				  $this->image_lib->initialize($configThumb);
				  $this->image_lib->resize();
				}						
			}
			$image_name   = (isset($_REQUEST['main_image_hid']))? $_REQUEST['main_image_hid']:''; 
			$story=array();
			$image_to_delete = '';
			if($image_name!='' && isset($data['file_name']) && $data['file_name']!=''){	
				$thumbImg=explode('.',$image_to_delete);	
				$thumbsimage=($thumbImg[0]!='') ? $thumbImg[0].'.'.$thumbImg[1] :'';
				$story = array('mainimage'=>$data['file_name']);									
			}
			elseif($image_name!='' && (!isset($data['file_name']))){			
				$story = array('mainimage'=>$image_name);
			}
			elseif($image_name=='' && isset($data['file_name']) && $data['file_name']!=''){		
				$story = array('mainimage'=>$data['file_name']);
			} 
			return $story;
		
		}

	public function verifycode($code)
	{
       $admins = $this->property_model->get_admins_info($this->input->post('id'), $code);
		if($admins)
		{
            $this->form_validation->set_message('verifycode', 'Promo Code already exists.  Please enter another one.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	
	
	public function resize_image_from_url(){
		$this->load->library('resizeclass');
		ini_set("max_execution_time", "5000");
		//ini_set('memory_limit', '5000m');
		$articles=$this->property_model->get_story_list();		
		$finalImgs=array();
		foreach($articles as $article)
		{		
			$doc = new DOMDocument();
			@$doc->loadHTML($article->articles_content);
			$img_tags = $doc->getElementsByTagName('img');
			foreach ($img_tags as $itag) {
				$finalImgs[] = str_replace('../../../', FCPATH, $itag->getAttribute('src'));
			}
			//echo "<pre>";print_r($finalImgs);
			if(count($finalImgs)<=0)
				continue;
			foreach($finalImgs as $img)
			{	//echo $img.'<br>';					
				$imgNameArr = explode('/', $img);
				$absolutepath = $img;
				
				//rename name
				$main_image=explode('.',$imgNameArr[count($imgNameArr)-1]);
				$finalimage=$main_image[0].'_'.$article->id.'.'.$main_image[1];
				//$absolutepath1=str_replace("/","\\",$absolutepath);
				//echo FCPATH;die;
				//$imgfull_path = FCPATH.'application/mydirname/'.$imgNameArr[count($imgNameArr)-1];
				//$imgfull_path = FCPATH.'uploads/story_mainimage/'.$imgNameArr[count($imgNameArr)-1];
				$imgfull_path = FCPATH.'uploads/story_mainimage/'.$finalimage;
				//$main_image='story_mainimage/'.$imgNameArr[count($imgNameArr)-1];	
			
				
				//echo $absolutepath.'<br>';
				
				if(isset($img) && !empty($img) && file_exists($absolutepath)){
					echo "UPDATE acms_articles SET main_image='". $finalimage."' where id =".$article->id;										
					$sql=$this->db->query("UPDATE acms_articles SET main_image='". $finalimage."' where id =".$article->id);

					$this->resizeclass->getimage($img);
					$this->resizeclass->resizeImage(360, 160,'none');
					$saved=$this->resizeclass->saveImage($imgfull_path, 100);										
				}
				
			}
		}				
	}
}	
/* End of file test_post.php */
/* Location: ./application/controllers/test_post.php */