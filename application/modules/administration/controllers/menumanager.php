<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menumanager  extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->model('menumanager_model');
		$this->load->library('pagination');  
		$this->load->helper('date');
	}
	public function index()
	{
		$id = $this->input->post('id');
		if($id == 0){
		$data['object'] = $this;
		$res['result'] = $this->menumanager_model->get_menu();
		$res['result_page'] = $this->menumanager_model->get_page();
		$res['result_menu'] = $this->menumanager_model->get_menu_val();
		$this->load->view('menumanager', $res);
		}else{
		$res['row'] = $this->menumanager_model->get_menu_detail($id);
		$this->load->view('administration/menumanager', $res);
		}
	}
	
	function insert_menu()
	{
		$id = $this->input->post('id');
		$this->form_validation->set_rules('menu_name', 'Menu Name', 'required');
		$this->form_validation->set_rules('alias', 'Alias', 'required');
		if ($this->form_validation->run() === TRUE)
		{		
			$data['menu_name'] = $this->input->post('menu_name');
			$data['menu_alias'] = $this->input->post('alias');
			
			if($this->input->post('menu_type') != 0)
			{
				$data['menu_type'] = $this->input->post('menu_select');
			}else{
				$data['menu_type'] = $this->input->post('menu_type');
			}
			
			$data['page_id'] = $this->input->post('page_id');
			$data['menu_link'] = $this->input->post('menu_link');
			$data['sort_order'] = $this->input->post('sort_order');
			$data['menu_create_date'] = date('Y-m-d H:i:s');
			$data['menu_status'] = $this->input->post('status');
			
			$da = $this->menumanager_model->row_count($data['menu_type']);
			if($da['num_row'] > 0){
				$this->menumanager_model->update_page($da['data']->menu_id);
			}
			if($id == 0){
			$this->menumanager_model->insert_menu($data);
			}else{
			$this->menumanager_model->update_menu($data, $id);
			}
			redirect('administration/menumanager');
		}else{
			if($this->input->post('id') == 0){
				$res['result'] = $this->menumanager_model->get_menu();
				$res['result_page'] = $this->menumanager_model->get_page();
				$res['result_menu'] = $this->menumanager_model->get_menu_val();
				$this->load->view('menumanager', $res);
				}else{
				$res['result'] = $this->menumanager_model->get_menu();
				$res['result_page'] = $this->menumanager_model->get_page();
				$res['result_menu'] = $this->menumanager_model->get_menu_val();
				$res['row'] = $this->menumanager_model->get_menu_detail($id);
				$this->load->view('administration/menumanager', $res);
				}
		}
	}
	
	function menu_edit($id)
	{
		if($id != 0){
		$res['result'] = $this->menumanager_model->get_menu();
		$res['result_page'] = $this->menumanager_model->get_page();
		$res['result_menu'] = $this->menumanager_model->get_menu_val();
		}
		$res['row'] = $this->menumanager_model->get_menu_detail($id);
		$this->load->view('administration/menumanager', $res);
	}
	
	function delete_menu($id)
	{
		$this->menumanager_model->delete_menu_mod($id);
		$res['result'] = $this->menumanager_model->get_menu();
		$res['result_page'] = $this->menumanager_model->get_page();
		$res['result_menu'] = $this->menumanager_model->get_menu_val();
		$res['row'] = $this->menumanager_model->get_menu_detail($id);
		$this->load->view('administration/menumanager', $res);
		return true;
	}
	
	function get_n_menu($id)
	{
		$res['row_for_menu'] = $this->menumanager_model->get_menudata($id);
		$this->load->view('administration/menumanager', $res);
	}
	
	
	// Sub menu managment/////
	
}	
/* End of file test_post.php */
/* Location: ./application/controllers/test_post.php */