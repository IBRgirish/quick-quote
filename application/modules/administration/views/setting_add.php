<?php $this->load->view('includes/head_style_script'); ?>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="container">
  <div class="content">
    <!-- Underwriter List -->
    <div class="span3">
      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmUnderwriter" id="frmUnderwriter" autocomplete="off">
        
		<label>Name (key )<em style="color:#FF0000">*</em></label>
        <input type="text" name="key" id="key" value="<?php echo set_value('key') ?>" class="span3">
		<?php echo form_error('key','<span style="color:red"">','</span>'); ?>
        
		<label>Value <em style="color:#FF0000">*</em></label>
        <input type="text" name="value" id="value" value="<?php echo set_value('value') ?>" class="span3">
		<?php echo form_error('value','<span style="color:red"">','</span>'); ?>
		
		<input type="button" value="Cancel" class="btn btn-primary pull-right">
        <input type="submit" value="Add" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
    </div>
    <!-- Underwriter List -->
  </div>
</div>
</body>
