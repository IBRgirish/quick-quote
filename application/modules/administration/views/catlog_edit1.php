<?php 
$this->load->view('includes/head_style_script');

$id	= isset($_POST['catlogId']) ? $_POST['catlogId'] : (isset($catlog->id)?$catlog->id :NULL);
$catlog_type	= isset($_POST['catlog_type']) ? $_POST['catlog_type'] : (isset($catlog->catlog_type)?$catlog->catlog_type :NULL);
$catlog_value	= isset($_POST['catlog_value']) ? $_POST['catlog_value'] : (isset($catlog->catlog_value)?$catlog->catlog_value :NULL);
$status	= isset($_POST['status']) ? $_POST['status'] : (isset($catlog->status)?$catlog->status :NULL);
?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}
</script>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="container">
  <div class="content">
    <div class="span3">
      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmCatlog" id="frmCatlog" autocomplete="off">
        
		<input type="hidden" name="catlogId" id="catlogId" value="<?php echo $id ?>" />
        
		<label>Catlog Types <em style="color:#FF0000">*</em></label>
         <?php
		  $selected = $catlog_type;
    	echo get_active_catlog_dropdown($selected, $attr='disabled="disabled"','cat_f_n',$id='cat_f_n');
	?>
	<input type="hidden" name="cat_f" value="<?php echo $selected; ?>">
       <!-- <select name="catlog_type" id="catlog_type" class="span3">
			<option value="nature_of_business" <?php echo($catlog_type=='nature_of_business' ? 'selected="selected"': '');?>>Nature of Business</option>
			<option value="trailer_type" <?php echo($catlog_type=='trailer_type' ? 'selected="selected"': '');?>>Trailer Type</option>
			<option value="commodities_haulted" <?php echo($catlog_type=='commodities_haulted' ? 'selected="selected"': '');?>>Commodities Haulted</option>
            <option value="trac_truck_model" <?php echo($catlog_type=='trac_truck_model' ? 'selected="selected"': '');?>>Truck & Tractor Models</option>
            <option value="number_of_axles" <?php echo($catlog_type=='number_of_axles' ? 'selected="selected"': '');?>>Number of Axles</option>
		</select>-->
        <?php echo form_error('insured_state','<span style="color:red"">','</span>'); ?>
        
		<label>Catlog Name <em style="color:#FF0000">*</em></label>
        <input type="text" name="catlog_name" id="catlog_value" class="span3" value="<?php echo $catlog->catlog_name; ?>">
		
		<label>State <em style="color:#FF0000">*</em></label>
        <div class="textinput span12 ">
			<?php
				$attr = "class='span12 ui-state-valid state'";
				if($catlog->slatax == '')
					$broker_state = 'CA';
				else
					$broker_state = $catlog->state;
			?>
			<?php echo get_state_dropdown('state', $broker_state, $attr); ?>
			
		</div>
		
		<label>SLATax%<em style="color:#FF0000">*</em></label>
        <input type="text" name="slatax" id="slatax" class="span3 numr_valid" value="<?php echo $catlog->slatax;?>">
		
		<label>Unique Cargo Policy Fee <em style="color:#FF0000">*</em></label>
        <input type="text" name="unique_cargo_policy_fee" id="unique_cargo_policy_fee" class="span3 numr_valid" value="<?php echo $catlog->unique_cargo_policy_fee;?>">
		
		<label>Unique PD Policy Fee <em style="color:#FF0000">*</em></label>
        <input type="text" name="unique_pd_policy_fee" id="unique_pd_policy_fee" class="span3 numr_valid" value="<?php echo $catlog->unique_pd_policy_fee;?>">
		
		<label>PD Cargo Policy Fee <em style="color:#FF0000">*</em></label>
        <input type="text" name="pd_cargo_policy_fee" id="pd_cargo_policy_fee" class="span3 numr_valid" value="<?php echo $catlog->pd_cargo_policy_fee;?>">
		
		<label>Association Fee <em style="color:#FF0000">*</em></label>
        <input type="text" name="association_fee" id="ssociation_fee" class="span3 numr_valid" value="<?php echo $catlog->association_fee;?>">
		
		
        
		<label>Status <em style="color:#FF0000">*</em></label>
        <input type="radio" name="status" id="status_active" value="1" <?php if ($status==1) echo 'checked'; ?> />
        &nbsp;Active&nbsp;
        <input type="radio" name="status" id="status_inactive" value="0" <?php if ($status==0) echo 'checked'; ?> />
        &nbsp;Inactive <br />
        <input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Save" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
    </div>
  </div>
</div>
</body>
