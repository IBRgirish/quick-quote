<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/header');
}
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">

<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script src="<?php echo base_url('js/date.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
    load_member_list();
});

function load_member_list() {
    $("#underwriterList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo base_url(); ?>administration/members/ajax_list_members",

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [{
                "sClass": "center"
            },

            //{"sClass": "center"},
            {
                "fnRender": function (oObj) {
                    //var a = oObj.aData[1]+' '+oObj.aData[2];
					var a = oObj.aData[1];
                    return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];
                    return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    var a1 = oObj.aData[4];
					var b1 = oObj.aData[8];
					/* var a = b1+'-'+a1; */					myUrl = a1;					if (myUrl.substring(myUrl.length-1) == "-")					{						myUrl = myUrl.substring(0, myUrl.length-1);					}					var a = myUrl;
                    return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[5];
					var status_name = toTitleCase(a);
                    if (a == 'ACTIVE') {
						a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj.aData[0] + ')" class="cursor-pointer-no"/>&nbsp;Active';
                    } else if (a == 'INACTIVE') {
                        a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj.aData[0] + ')" class="cursor-pointer-no"/>&nbsp;Inactive';
                    } else {
						a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj.aData[0] + ')" class="cursor-pointer-no"/>&nbsp;'+status_name+'';
					}
					return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    var b = oObj.aData[6];
					 //alert(b);
					 if(b){						 						 
					 var datesplit=b.split("-");
					var date = new Date(datesplit[1]+" "+datesplit[0]+", "+datesplit[2]);
					//alert(date);
					var a = date.toString('MM/dd/yyyy');
			         //var a=  date.parse('MM/dd/yyyy');
                    return (a);
					 }
					 else {
						 return('');
					 }

                }

            }, {
                "fnRender": function (oObj) {
                    var b = oObj.aData[7];
					//alert(b);
					
					if(b!=null){
						 var datesplit=b.split("-");
					var date = new Date(datesplit[1]+" "+datesplit[0]+", "+datesplit[2]);
						var a = date.toString("MM/dd/yyyy");
					}
					else
					{
						var a = '';
					}
                    return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    // var a = oObj.aData[2];
                    var jobId = oObj.aData[0];
                    a = '<a href="javascript:void(0)" onClick="view_member('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn- btn-phone-block"><span class="hidden-phone">View</span></button></a>&nbsp;<a href="<?php echo base_url('administration/agency/edit_agency') ?>/'+oObj.aData[0]+'" data-toggle="modal"><button class="btn btn- btn-phone-block"><span class="hidden-phone">Edit</span></button></a>&nbsp;<a href="javascript:void(0)" onClick="user_delete('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></button></a>';
					
                    return (a);

                }

            }
        ]
    });

}

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function ajax_status(status,id)
{ 
	//Disabled the function for now 
	return false;
	var val = 'ACTIVE';
	if (status==0) {
		var val = 'INACTIVE';
	}
	
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('administration/members/ajax_status'); ?>",
		data:{status:val,id:id},
		success:function(data){
			
			if(data=="done")
			{
				load_member_list(); 
			} 
			
	}});
}

function user_delete(id)
{
	var r=confirm("Are you sure want to delete Agency ..?");
	if (r==true)
  	{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('administration/members/ajax_user_delete'); ?>",
			data:{id:id},
			success:function(data){
			
				if(data=="done")
				{
					load_member_list(); 
				} 
			
		}});
	}
}

function view_member(id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url() ?>administration/members/view_member/'+id,
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '500',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}

</script>
<br>

<h1><?php echo $header['title']; ?></h1>
<div class="table"> <!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> <!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<a class="fancybox-add btn btn-primary" href="<?php echo base_url('administration/agency/add_agency') ?>">Add Agency</a>
	<label style="font-size:13px; color:green"><?php echo $this->session->flashdata('success') ?></label>
  <!-- Underwriter List -->
  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="5%">Sr.</th>
        <th width="15%">Agency</th>
		<th width="15%">Email</th>
        <th width="10%">Contact</th>
        <th width="10%">Status</th>
		<th width="10%">Created</th>
		<th width="15%">Last Login</th>
        <th width="20%">Action</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <!-- Underwriter List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
