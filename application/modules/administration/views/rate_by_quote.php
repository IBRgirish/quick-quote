<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
		//echo 'a';
	} 
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>

<br>
<h1>Quote List</h1>
<div class="table"> 
<!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> 
<!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<!--<a class="fancybox-add btn btn-primary" onclick="javascript:void(0)" href="">Add New</a>-->

  <!-- Quote List -->
  <input type="hidden" name="requested_by" id="requested_by" value="<?php //echo $user ?>" />
 
  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">Sr.</th>
        <th width="15%">Email</th>
        <th width="15%">Total Cargo Premium</th>
        <th width="15%">Total PD Premium</th>
		<th width="15%">Total Liability Premium</th>
		<th width="15%">Created On</th>
		<th width="15%">Status</th>
		<th width="15%">Action</th>
	</tr>
    </thead>
    <tbody>
		<?php $cn = 0;krsort($rows); foreach($rows as $row) {  $cn++; $ids[] = $row[0]['id']; ?>
			<tr>
				<td class="center" width="10%"><?php echo $cn; ?></td>
				<td width="15%"><?php echo $row[0]['email']; ?></td>
				<td width="15%"><?php echo isset($row[0]['total_cargo_premium']) ? $row[0]['total_cargo_premium'] : ''; ?></td>
				<td width="15%"><?php echo isset($row[0]['total_pd_premium']) ? $row[0]['total_pd_premium'] : ''; ?></td>
				<td width="15%"><?php echo isset($row[0]['total_liability_premium']) ? $row[0]['total_liability_premium'] : ''; ?></td>
				<td width="15%"><?php echo date("m/d/Y",strtotime($row[0]['creation_date'])); ?></td>
				<td width="15%"><?php if($row[0]['perma_reject']!='Reject'){ echo $row[0]['status'];}else{ echo 'Rejectd';} ?></td>
				<td width="15%">
				<?php if($cn == 1) { ?>
				<a class="btn btn-primary" href="<?php echo base_url(); ?>ratesheet/edit_ratesheet/<?php echo $row[0]['id']; ?>">Edit</a>
				<?php } else { ?>
				<a class="btn btn-primary" href="<?php echo base_url(); ?>ratesheet/edit_ratesheet/<?php echo $row[0]['id']; ?>">Open</a>
				
				<?php } ?>
               
				</td>
			  </tr>
		<?php } ?>
    </tbody>
  </table>
  <?php if($cn==0){
  	echo '<div class="not_data_tble">No Data in table</div>';
  }?>
  
  <!-- Quote List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>
