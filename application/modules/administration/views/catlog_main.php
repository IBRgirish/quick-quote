<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/header');
}

?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
	<?php if(isset($_GET['cat_type']) && ($_GET['cat_type'] > 0) ) { ?>
		var cat_type = '<?php echo $_GET['cat_type']; ?>';
		
		if(cat_type != '21')
		{
			
			load_catlog_list('<?php echo $_GET['cat_type']; ?>');
		}
		else
		{
			
			load_catlog_list1('21');
		}
	<?php } else { ?>
		
		load_catlog_list1('21');
	<?php } ?>
    
	/* $('.fancybox-add').click(function(){ alert('d');
		//add_catlog_item();
		edit_catlog('9')
	}) */
	
});

function add_catlog_item(){
	var cat_type = $('#cat_f').val();
	
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url('administration/catlog/catlog_add?cat_type=') ?>'+cat_type,
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '400',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}
function load_catlog_list1(type) {

	$("#catlogList_new_div").show();
	$("#catlogList_data_div").hide();
	$("#catlogList_l_div").hide();
	$("#catlogList_div").hide();
    $("#catlogList_new").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo base_url(); ?>administration/catlog/ajax_list_catlog1/"+type,

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [
              
           

            {"sClass": "center"},
            {  
                "fnRender": function (oObj) {
					
                    var a = oObj.aData[1];
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
				
                    var a = oObj.aData[3];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[4];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[5];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[6];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[7];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[8];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[9];;
					
                    if (a == 1) {
                        a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Active)';
                    } else {
                        a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Disable)';
                    }
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    // var a = oObj.aData[2];
                    var jobId = oObj.aData[0];
                    a = '<a href="javascript:void(0);" onClick="edit_catlog1('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">Edit</span></button></a>&nbsp;<a href="javascript:void(0)" onClick="catlog_delete1('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></button></a>';
                    return (a);

                }

            }
        ]
    });

}


function load_catlog_list(type) {
	
	if(type==30 || type==31 || type==32 || type==33 || type==34 || type==35 || type==36 || type==37 || type==38 || type==1 || type==17 || type==2 || type==3 || type==18 || type==25 || type==47 || type==48 || type==49 || type==51){
	$("#catlogList_data_div").show();
	$("#catlogList_new_div").hide();
	$("#catlogList_l_div").hide();
	$("#catlogList_div").hide();
    $("#catlogList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo base_url(); ?>administration/catlog/ajax_list_catlog/"+type,

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
			
        },
     

        "aoColumns": [
              
           

            {"sClass": "center"},
            {  
                "fnRender": function (oObj) {
					
                    var a = oObj.aData[1];
					if (a == 30) {
						a = 'Make Truck';
                    } else if (a == 31) {
                        a = 'Model Truck';
                    }else if (a == 32) {
                        a = 'Make Tractor';
                    }
					else if (a == 33) {
                        a = 'Model Tractor';
                    }
					else if (a == 37) {
                        a = 'Priority sequence';
                    }
					else if (a == 34) {
                        a = 'Email type';
                    }
					else if (a == 35) {
                        a = 'Phone type';
                    }
					else if (a == 36) {
                        a = 'Code type';
                    }
					else if (a == 38) {
                        a = 'Fax type';
                    }
					else if (a == 1) {
                        a = 'Commodities haulted';
                    }
					else if (a == 17) {
                        a = 'Nature of business';
                    }
					else if (a == 2) {
                        a = 'Truck & Tractor Model';
                    }
					
					else if (a == 3) {
                        a = 'Trailer Model';
                    }
					
					else if (a == 18) {
                        a = 'Trailer Type';
                    }
					else if (a == 25) {
                        a = 'Filing Type';
                    }
					else if (a == 47) {
                        a = 'Trailer Make';
                    }
					else if (a == 48) {
                        a = 'Vehicle Color';
                    }
					else if (a == 49) {
                        a = 'Cargo Limit';
                    }
					else if (a == 51) {
                        a = 'Coverages';
                    }
					
					
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];;
                    return (a);

                }

            },  			
			{  
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];;
                    if (a == 1) {
                        a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Active)';
                    } else {
                        a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Disable)';
                    }
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    // var a = oObj.aData[2];
                    var jobId = oObj.aData[0];
                    a = '<a href="" onClick="edit_catlog('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">Edit</span></button></a>&nbsp;<a href="" onClick="catlog_delete('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></button></a>';
                    return (a);

                }

            }
        ]
    });
		
       }else if(type==39 || type==44 || type==45 || type==46 || type==50){		   
		   	
    $("#catlogList_data_div").hide();
	$("#catlogList_new_div").hide();
	$("#catlogList_div").hide();
	$("#catlogList_l_div").show();
    $("#catlogLists_l").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo base_url(); ?>administration/catlog/ajax_list_catlog/"+type,

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
			
        },
     

        "aoColumns": [
              
           

            {"sClass": "center"},
            {  
                "fnRender": function (oObj) {
					
                    var a = oObj.aData[1];
					
					if (a == 39) {
						   a = 'Lessor Type';
                    } 
					else if (a == 44) {
						   a = 'Map pic truck';
                    }
					else if (a == 45) {
						   a = 'Map pic tractor';
                    }
					else if (a == 46) {
						   a = 'Map pic trailer';
                    }
					else if (a == 50) {
						   a = 'Equipment Type';
                    }
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
					var b = oObj.aData[1];		
					var a = oObj.aData[3];;
					var a1='';
					if (b == 'Map pic truck') {
						   a1 = '<img src="<?php echo base_url('uploads/file'); ?>/'+a+'" width="40" height="40"/>';
                    }
					else if (b == 'Map pic tractor') {
						   a1 = '<img src="<?php echo base_url('uploads/file'); ?>/'+a+'" width="40" height="40"/>';
                    }
					else if (b == 'Map pic trailer') {
						   a1 = '<img src="<?php echo base_url('uploads/file'); ?>/'+a+'" width="40" height="40"/>';
                    }
					else if (b == 'Equipment Type') {
						   a1 = '<img src="<?php echo base_url('uploads/file'); ?>/'+a+'" width="40" height="40"/>';
                    }
                   
                    return (a1);

                }

            }, 			
			{  
                "fnRender": function (oObj) {
                    var a = oObj.aData[4];
                    if (a == 1) {
                        a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Active)';
                    } else {
                        a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Disable)';
                    }
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    // var a = oObj.aData[2];
                    var jobId = oObj.aData[0];
                    a = '<a href="" onClick="edit_catlog('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">Edit</span></button></a>&nbsp;<a href="" onClick="catlog_delete('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></button></a>';
                    return (a);

                }

            }
        ]
    });
  
			
			
			
	   }else{	
    $("#catlogList_data_div").hide();
	$("#catlogList_l_div").hide();	
	$("#catlogList_new_div").hide();
	$("#catlogList_div").show();
    $("#catlogLists").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo base_url(); ?>administration/catlog/ajax_list_catlog/"+type,

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
			
        },
     

        "aoColumns": [
              
           

            {"sClass": "center"},
            {  
                "fnRender": function (oObj) {
					
                    var a = oObj.aData[1];
					
					if (a == 26) {
						   a = 'PIP';
                    } else if (a == 27) {
                        a = 'UM';
                    }else if (a == 28) {
                        a = 'UIM';
                    }else if (a == 29) {
                        a = 'Slatax';
                    }
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];;
                    return (a);

                }

            }, 
			{  
                "fnRender": function (oObj) {
                    var a = oObj.aData[4];;
                    return (a);

                }

            },
			{  
                "fnRender": function (oObj) {
                    var a = oObj.aData[5];
                    if (a == 1) {
                        a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Active)';
                    } else {
                        a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Disable)';
                    }
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    // var a = oObj.aData[2];
                    var jobId = oObj.aData[0];
                    a = '<a href="" onClick="edit_catlog('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">Edit</span></button></a>&nbsp;<a href="" onClick="catlog_delete('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></button></a>';
                    return (a);

                }

            }
        ]
    });
  }
}

$(document).ready(function(){
	$("#cat_f").change(function(){
		if($(this).val() == '21')
		{
		load_catlog_list1($(this).val());
		}else if($(this).val() == '21'){
		load_catlog_list12($(this).val());
		}
		else 
		{
			
		load_catlog_list($(this).val());
		}
	});
});
	
function edit_catlog1(id)
{	var cat_type = $('#cat_f').val();
	//alert(cat_type);
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url() ?>administration/catlog/catlog_edit1/'+id+'?cat_type='+cat_type,
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '450',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}	
	
function edit_catlog(id)
{	var cat_type = $('#cat_f').val();
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url() ?>administration/catlog/catlog_edit/'+id+'?cat_type='+cat_type,
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '450',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}

function catlog_delete1(id)
{ 	var cat_type = $('#cat_f').val();
	var r=confirm("Are you sure want to delete catlog ..?");
	if (r==true)
  	{
	$.ajax({
		type: "POST",
		url: "<?php echo base_url().'administration/catlog/ajax_catlog_delete1'; ?>",
		data:{id:id},
		success:function(data){
			
			if(data=="done")
			{
				
				load_catlog_list1(cat_type); 
			} 
			
	}});
	}
}
function catlog_delete(id)
{ 	var cat_type = $('#cat_f').val();
	var r=confirm("Are you sure want to delete catlog ..?");
	if (r==true)
  	{
	$.ajax({
		type: "POST",
		url: "<?php echo base_url().'administration/catlog/ajax_catlog_delete'; ?>",
		data:{id:id,cat_type:cat_type},
		success:function(data){
			
			if(data=="done")
			{
				load_catlog_list(cat_type); 
			} 
			
	}});
	}
}

</script>
<br>
<h1>Manage Catlogs</h1>
<div class="table"> <!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> <!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<a data-toggle="modal" onclick="add_catlog_item()"  class="fancybox-add_ btn btn-primary" href="">Add New</a>

<p></p>
<?php if(isset($_GET['cat_type']) && ($_GET['cat_type'] > 0) ) { $cat_type_selected = $_GET['cat_type'];  } else { $cat_type_selected=''; } ?>
<div class="row-fluid">
	<div class="span4 lightblue">  
		<label>Select Catlog Type</label>
         <?php
		//echo  $cat_type_selected = $type; 
		
    	echo get_active_catlog_dropdown($selected=$cat_type_selected,$attr='','cat_f',$id='cat_f');
	?>
		<!--<select id="catlog_type" name="catlog_type">
			<option value="nature_of_business">Nature of Business</option>
			<option value="trailer_type">Trailer Type</option>
			<option value="commodities_haulted">Commodities Haulted</option>
			<option value="trac_truck_model">Truck & Tractor Models</option>
            <option value="number_of_axles">Number of Axles</option>
           <!--option value="trac_truck_model">Tractor Models</option
           <option value="trailer_model">Trailer Models</option>
		</select>-->
	</div>
</div>

  <!-- Catlog List -->
  <div id="catlogList_data_div">
  <table id="catlogList" style="width:100%!important" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">ID.</th>
        <th width="22%">Catlog Type</th>      
        <th width="22%">Catlog Value </th>
        <th width="22%">Status</th>
        <th width="22%">Action</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  </div>
  <div id="catlogList_div">
  <table id="catlogLists" width="100%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">ID.</th>
        <th width="22%">Catlog Type</th>
        <th width="22%">Catlog State Name </th>
        <th width="22%">Catlog Carrier Name </th>
        <th width="22%">Catlog Option Value </th>
        <th width="22%">Status</th>
        <th width="22%">Action</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  </div>
  <div id="catlogList_l_div">
  <table id="catlogLists_l" width="100%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">ID.</th>
        <th width="22%">Catlog Type</th>
        <th width="22%">Catlog Name </th>
        <th width="22%"><?php if($cat_type_selected==44 || $cat_type_selected==45 || $cat_type_selected==46) { echo 'Catlog picture' ;}else{?>Catlog label Name<?php } ?> </th>        
        <th width="22%">Status</th>
        <th width="22%">Action</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  </div>
  <div id="catlogList_new_div">
  <table id="catlogList_new" width="100%" class="dataTable">
     <thead>
      <tr>
        <th class="center" width="10%">ID.</th>
        <th width="22%">Catlog Type</th>
        <th width="22%">Catlog Name</th>
        <th width="22%">State</th>
		<th width="22%">SLATax%</th>
		<th width="22%">Unique cargo policy fee</th>
		<th width="22%">Unique PD policy fee</th>
		<th width="22%">PD & Cargo policy fee</th>
		<th width="22%">Association fee</th>		
        <th width="22%">Status</th>
        <th width="22%">Action</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  </div>
  <!-- Catlog List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
