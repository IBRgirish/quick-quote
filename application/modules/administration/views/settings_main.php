<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/header');
}
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">

<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
    load_settings_list();
	
	$(".fancybox-add").fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url('administration/settings/config_add') ?>',
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '300',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
});

function load_settings_list() {
    $("#underwriterList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo base_url(); ?>administration/settings/ajax_settings_list",

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [{
                "sClass": "center"
            },

            //{"sClass": "center"},
            {
                "fnRender": function (oObj) {
                    var a = oObj.aData[1];
                    return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];
                    return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];
                    if (a == 1) {
						a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Active)';
                    } else {
						a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Inactive)';
					}
					return (a);

                }

            }, {
                "fnRender": function (oObj) {
                    // var a = oObj.aData[2];
                    var jobId = oObj.aData[0];
                    a = '<a href="javascript:void(0);" onClick="edit_setting('+oObj.aData[0]+', &quot;'+oObj.aData[4]+'&quot;)" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">Edit</span></button></a>';
					
					/* &nbsp;<a href="javascript:void(0)" onClick="setting_delete('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></button></a> */
                    return (a);

                }

            }
        ]
    });

}

function ajax_status(status,id)
{
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('administration/settings/ajax_status'); ?>",
		data:{status:status,id:id},
		success:function(data){
			
			if(data=="done")
			{
				load_settings_list(); 
			} 
			
	}});
}

function edit_setting(id, type)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url() ?>administration/settings/setting_edit/'+id+'/'+type,
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '300',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}

function setting_delete(id)
{
	$.ajax({
		type: "POST",
		url: "<?php echo base_url('administration/settings/ajax_setting_delete'); ?>",
		data:{id:id},
		success:function(data){
			
			if(data=="done")
			{
				load_settings_list(); 
			} 
			
	}});
}
</script>
<br>
<h1><?php echo $header['title']; ?></h1>
<div class="table"> <!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> <!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<!--<a class="fancybox-add" onclick="javascript:void(0)" href="">Add New</a>-->

  <!-- Underwriter List -->
  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">Sr.</th>
        <th width="25%">Name</th>
		<th width="25%">value</th>
        <th width="20%">Status</th>
		<th width="20%">Action</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <!-- Underwriter List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
