<?php $this->load->view('includes/head_style_script'); ?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}
</script>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="container">
  <div class="content">
    <!-- Underwriter List -->
    <div class="span3">
      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmUnderwriter" id="frmUnderwriter" autocomplete="off">
        
		<label>Name <em style="color:#FF0000">*</em></label>
        <input type="text" name="name" id="name" value="<?php echo set_value('name') ?>" class="span3">
		<?php echo form_error('name','<span style="color:red"">','</span>'); ?>
        
		<label>Email Address <em style="color:#FF0000">*</em></label>
        <input type="text" name="email" id="email" value="<?php echo set_value('email') ?>" class="span3">
		<?php echo form_error('email','<span style="color:red"">','</span>'); ?>
		
		<label>Password <em style="color:#FF0000">*</em></label>
        <input type="text" name="password" id="password" class="span3">
		<?php echo form_error('password','<span style="color:red"">','</span>'); ?>
		
		<input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Add" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
    </div>
    <!-- Underwriter List -->
  </div>
</div>
</body>
