<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/header');
}

?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
	<?php if(isset($_GET['cat_type']) && ($_GET['cat_type'] > 0) ) { ?>
		var cat_type = '<?php echo $_GET['cat_type']; ?>';
		
		if(cat_type != '21')
		{
			
			load_catlog_list('<?php echo $_GET['cat_type']; ?>');
		}
		else
		{
			
			load_catlog_list1('21');
		}
	<?php } else { ?>
		
		load_catlog_list();
	<?php } ?>
    
	/* $('.fancybox-add').click(function(){ alert('d');
		//add_catlog_item();
		edit_catlog('9')
	}) */
	
});

function add_catlog_item(){
	//var cat_type = $('#cat_f').val();	
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url('administration/data_dictionary/data_add') ?>',
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '400',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}



function load_catlog_list() {	
	
    $("#catlogList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo base_url(); ?>administration/data_dictionary/ajax_list_dictionary",

        "fnServerData": function (sSource, aoData, fnCallback) {
            
			$.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
			
        },
     

        "aoColumns": [
              
           

            {"sClass": "center"},
            {  
                "fnRender": function (oObj) {
					
                    var a = oObj.aData[1];				
					
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];;
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[4];;
                    return (a);

                }

            },  
			 {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[5];;
                    return (a);

                }

            },  
			{  
                "fnRender": function (oObj) {
                    var a = oObj.aData[6];;
                    return (a);

                }

            },  
			 {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[7];;
					if (a == 1) {
                        a = 'Yes';
                    } else {
                        a = 'No';
                    }
                    return (a);

                }

            },
			 {  
                "fnRender": function (oObj) {
                    var a = oObj.aData[8];;
                    return (a);

                }

            },{  
                "fnRender": function (oObj) {
                    var a = oObj.aData[9];;
					if (a == 1) {
                        a = 'Yes';
                    } else {
                        a = 'No';
                    }
					
                    return (a);

                }

            },{  
                "fnRender": function (oObj) {
                    var a = oObj.aData[10];;
				      if (a == 1) {
                        a = 'Quick Quote';
                    } else {
                        a = 'Application';
                    }
                    return (a);

                }

            },		
			{  
                "fnRender": function (oObj) {
                    var a = oObj.aData[11];;
					
                    return (a);

                }

            },{  
                "fnRender": function (oObj) {
                    var a = oObj.aData[12];;
				      if (a != '' || a > 0) {
                        a = '<input style="background-color:#'+a+';width: 50%;"/>';
                    } 
                    return (a);

                }

            },		  			
			{  
                "fnRender": function (oObj) {
                    var a = oObj.aData[13];;
                    if (a == 1) {
                        a = '<img src="<?php echo base_url('images/green-dot.png') ?>" onClick="ajax_status(0,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Active)';
                    } else {
                        a = '<img src="<?php echo base_url('images/red-dot.png') ?>" onClick="ajax_status(1,' + oObj.aData[0] + ')" class="cursor-pointer"/>&nbsp;(Disable)';
                    }
                    return (a);

                }

            }, {  
                "fnRender": function (oObj) {
                    // var a = oObj.aData[2];
                    var jobId = oObj.aData[0];
                    a = '<a href="" onClick="edit_catlog('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn- btn-phone-block"><icon class="icon-pencil icon-white"></icon><span class="hidden-phone">Edit</span></button></a>&nbsp;<a href="" onClick="catlog_delete('+oObj.aData[0]+')" data-toggle="modal"><button class="btn btn-danger btn-phone-block"><icon class="icon-remove icon-white"></icon><span class="hidden-phone">Delete</span></button></a>';
                    return (a);

                }

            }
        ]
    });		
    
}

$(document).ready(function(){
	$("#cat_f").change(function(){
		if($(this).val() == '21')
		{
		load_catlog_list1($(this).val());
		}else if($(this).val() == '21'){
		load_catlog_list12($(this).val());
		}
		else 
		{
			
		load_catlog_list($(this).val());
		}
	});
});
	
function edit_catlog(id)
{	
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url('administration/data_dictionary/data_add') ?>/'+id+'',
    	autoSize: false,
    	closeBtn: true,
    	width: '500',
    	height: '450',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}	
	


function catlog_delete(id)
{ 	
	var r=confirm("Are you sure want to delete catlog ..?");
	if (r==true)
  	{
	$.ajax({
		type: "POST",
		url: "<?php echo base_url().'administration/data_dictionary/ajax_catlog_delete1'; ?>",
		data:{id:id},
		success:function(data){			
				
				load_catlog_list(); 
			
			
	}});
	}
}

</script>
<br>
<h3>Manage Data Dictionary</h3>
<div class="table"> <!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> <!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<a data-toggle="modal" onclick="add_catlog_item()"  class="fancybox-add_ btn btn-primary" href="">Add New</a>

<p></p>
<?php if(isset($_GET['cat_type']) && ($_GET['cat_type'] > 0) ) { $cat_type_selected = $_GET['cat_type'];  } else { $cat_type_selected=''; } ?>
<div class="row-fluid">
	<div class="span4 lightblue">  
		<!--<label>Select Catlog Type</label>-->
         <?php		
    	/*echo get_active_catlog_dropdown($selected=$cat_type_selected,$attr='','cat_f',$id='cat_f');*/
	     ?>	
	</div>
</div>

  <!-- Catlog List -->
  <div id="catlogList_data_div" style="width:100%!important">
  <table id="catlogList" style="width:100%!important" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">ID.</th>
        <th width="10%">Field Name</th>      
        <th width="10%">Default Value </th>
        <th width="10%">Required</th>
        <th width="10%">Tooltip</th>          
        <th width="10%">Min Value </th>
        <th width="10%">Max Value</th>
        <th width="10%">Show</th>
        <th width="10%">Placeholder</th>
        <th width="10%">Disable</th>
        <th width="10%">Application</th>
        <th width="10%">Coverages</th>
        <th width="10%">Colour</th>
        <th width="10%">Status</th>
        <th width="10%">Action</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  </div>
  
  
  
  <!-- Catlog List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
<style>
div#catlogList_wrapper {
    width: 103%!important;
}
</style>