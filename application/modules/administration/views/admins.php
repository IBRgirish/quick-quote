<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/header');
}

?>
<?php if(permission_levels('user')) { ?>
<br>
      <h1>Admin Users</h1> 
   <div class="table"> <img src="<?php echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7"> <img src="<?php echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">
      <table class="listing" cellpadding="0" cellspacing="0" id="showrecord">
         <tbody>
            <tr>
               <th class="first" width="177">Name</th>
               <th>Email</th>
			   <th>Type</th>
               <th>Status</th>
               <th>Create Date</th>
               <th>Modify Date</th>
               <th>Modify</th>
               <th class="last">Delete</th>
            </tr>
<?php 


if($admins){


	foreach($admins as $admin){
	
		if($admin->admin_type=='superadmin'){
			$admintype="Super Admin";			
		}
		elseif($admin->admin_type=='reportadmin'){
			$admintype="Report Admin";			
		}		
		elseif($admin->admin_type=='useradmin'){
			$admintype="User Admin";			
		}		
		elseif($admin->admin_type=='memberadmin'){
			$admintype="Member Admin";			
		}		
		elseif($admin->admin_type=='propertyadmin'){
			$admintype="Property Admin";			
		}
	?>
            <tr>
               <td class="first style1"><?php echo $admin->name;?></td>
               <td><?php echo $admin->email;?>&nbsp;</td>
			    <td><?php echo $admintype;?>&nbsp;</td>
               <td><?php echo $admin->status;?></td>
               <td><?php echo mdate("%m/%d/%Y",strtotime($admin->create_date));?>&nbsp;</td>
               <td><?php echo mdate("%m/%d/%Y",strtotime($admin->modified_date));?>&nbsp;</td>
               <td><a href="<?php echo base_url('administration/admins/edit/'.$admin->id);?>"><img src="<?php echo base_url();?>img/admin/edit-icon.gif" alt="" width="16" height="16"></a></td>
               <td>
               <?php if($admin->id == 1){?>
               <a href="#" onclick="jAlert('You can not delete system admin', 'Alert'); return false;"><img src="<?php echo base_url();?>img/admin/hr.gif" alt="" width="16" height="16"></a>
               <?php }else{?>
               <a href="#" onclick=" delete_case('<?php echo base_url('administration/admins/delete/'.$admin->id);?>', 'Are you sure?', 'Information deleted'); return false;"><img src="<?php echo base_url();?>img/admin/hr.gif" alt="" width="16" height="16"></a>
               <?php }?>
               </td>
            </tr>
<?php }
}else{ ?>
            <tr>
               <td class="first style1">&nbsp;</td>
               <td colspan="5">Oops, no data found!</td>
            </tr>
<?php }?>
         </tbody>
      </table>
   </div>
   <div class="pagination">
      <?php $this->pagination->create_links();?>

	    </div>
 <?php }else { echo 'You are not authorized to view'; } ?> 
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>