<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>

<link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
<script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
<script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
<script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script>
<script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>

<script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>

<script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
<script>
$(document).ready(function(){
	$(".date").mask("99/99/9999");
		/*var date = new Date();
		date.setDate(date.getDate()-0);
				
		 var inputs1 = $('.date').datepicker({
			startDate: date,
			format: 'mm/dd/yyyy',
			autoclose: true
		});  */
		/** Filter Start and End Date **/
		var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		$('#date_f').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_t').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_t').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_f').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
$('#checkAll').click(function () {    
     $('input:checkbox').prop('checked', this.checked);    
 });	

$("#under").click(function(){
    var checked = $("input:checked").length > 0;
    if (!checked){
        alert("Please check at least one checkbox");
        return false;
    }
});

});
</script>
<br>
<h1>Quote List</h1>
<? //print_r($rows);?>
<?php
$agency_name = $this->session->userdata('member_name'); 
$characters_only = ' pattern="^[a-zA-Z0-9][a-zA-Z0-9-_\.\ \;\,\*\@\&\)\(\-\=\+\:/]{0,50}$"  ';

//placeholder="MM/DD/YYYY" 
//$date_pattern = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" ';

$date_pattern_js = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ';

/*
  $date_pattern = ' placeholder="MM/DD/YYYY" class="date"  pattern="^([0-9]{2,2})/([0-9]{2,2})/([0-9]{4,4})" ';
 */

$email_pattern = 'pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$"';
$number_pattern = ' pattern="[0-9]+" ';
$less_100_limit = ' pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" ';
?>

<div class="table"> 
<!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> 
<!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<!--<a class="fancybox-add btn btn-primary" onclick="javascript:void(0)" href="">Add New</a>-->

  <!-- Quote List -->
  <input type="hidden" name="requested_by" id="requested_by" value="<?php //echo $user ?>" />
  <a href="<?php echo base_url(); ?>ratesheet/manualrate" class="btn btn-primary"> Add New</a>
  <div class="pull-right" class="sapn6">
  <!--onchange="sort_btwn_date()"-->
  	<input type="text" class="span2 date pull-left" style="float:left;margin-right: 5px;" name="date_f" id="date_f"  <?= $date_pattern_js; ?> />
    <input type="text" class="span2 date pull-left" style="float:left;margin-right: 5px;" name="date_t" id="date_t"  <?= $date_pattern_js; ?> />
    <input type="button" class="btn btn-primary pull-left" name="filter" value="Filter" onclick="sort_btwn_date()" />
  </div>

  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">Sr.</th>
        <th width="10%">Title <input type="checkbox" name="quote_re[]" value="" id="checkAll"></th>
        <th width="15%">Email</th>
        <th width="15%">Total Cargo Premium</th>
        <th width="15%">Total PD Premium</th>
		<th width="15%">Total Liability Premium</th>
		<th width="15%">Created On</th>
		<th width="15%">Versions</th>
	</tr>
    </thead>
    <tbody id="tabe_body">
		<?php $cn = 0; foreach($rows as $row) { $cn++; /*$ids[] = $row[0]['quote_id']*/;?>
			<tr>
				<td class="center" width="10%"><?php echo $cn; ?></td>
                <td><input type="checkbox" name="quote_re[]" value="<?= isset($row[0]['quote_id']) ? $row[0]['quote_id'] : ''; ?>"></td>
				<td width="15%"><?php echo isset($row[0]['email']) ? $row[0]['email'] : ''; ?></td>
				<td width="15%"><?php echo  isset($row[0]['total_cargo_premium']) ? $row[0]['total_cargo_premium'] : ''; ?></td>
				<td width="15%"><?php echo isset($row[0]['total_pd_premium']) ? $row[0]['total_pd_premium'] : ''; ?></td>
				<td width="15%"><?php echo isset($row[0]['total_liability_premium']) ? $row[0]['total_liability_premium'] : ''; ?></td>
				<td width="15%"><?php echo isset($row[0]['creation_date']) ? date("m/d/Y",strtotime($row[0]['creation_date'])) : ''; ?></td>
				<td width="15%">
                <?php
                if(isset($row[0]['is_manual']))
				{
					if($row[0]['is_manual']==1)
					{
						?>
						<a class="btn btn-primary" href="<?php echo base_url(); ?>administration/quotes/rate_by_quote2/<?php echo isset($row[0]['quote_ref']) ? $row[0]['quote_ref'] : ''; ?>">View</a>
						<?php
					}
					else
					{
					?>
						<a class="btn btn-primary" href="<?php echo base_url(); ?>administration/quotes/rate_by_quote1/<?php echo isset($row[0]['quote_ref']) ? $row[0]['quote_ref'] : ''; ?>">View</a>
						  <a class="btn btn-warning" href="javascript:;" onclick="assign_underwriter(<?= isset($row[0]['quote_id']) ? $row[0]['quote_id'] : ''; ?>, '<?php echo isset($row[0]['quote_ref']) ? $row[0]['quote_ref'] : ''; ?>')">Assign Underwriter</a>
					<?php
					}
				}
				?>
              <!--  <a class="btn btn-primary" href="<?php /*echo base_url(); ?>administration/quotes/rate_by_quote1/<?php echo isset($row[0]['quote_ref']) ? $row[0]['quote_ref'] : ''; ?>">Versions</a>
                <a class="btn btn-primary" href="javascript:;" onclick="assign_underwriter(<?= isset($row[0]['quote_id']) ? $row[0]['quote_id'] : ''; ?>, '<?php echo isset($row[0]['quote_ref']) ? $row[0]['quote_ref'] : '';*/ ?>')">Assign Underwriter</a>--></td>
			  </tr>
		<?php } ?>
        <? //print_r($ids);?>
    </tbody>
  </table>
  <span id="export_div">
   <a class="btn btn-primary" target="_top" href="javascript:;" onclick="exportExcel()">Export</a>
  </span>
 
  <!-- Quote List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
<script>
function exportExcel()
{
	var n= 0;
	var underwriter = [];
	$('[name="quote_re[]"]').each(function(index) {	
		 if($(this).is(':checked'))
		 {	
			  underwriter[n] = $(this).val();
			  n++;	
		}		
	});
	if(n>0)
	{
	window.location = '<?php echo base_url();?>administration/quotes/file_xls/?ids='+underwriter;
	//alert('<?php //echo base_url();?>administration/quotes/file_xls/'+underwriter);
	}
	else
	{
		alert('First Select atleast one record');
	}
}
function sort_btwn_date(){
	var date_f = $("#date_f").val();
	var date_t = $("#date_t").val();
	if(date_t!='')
	{
		$.ajax({
			url: "<?php echo base_url();?>administration/quotes/sort_date_quote",
			type: "post",
			data: "date_f="+date_f+"&date_t="+date_t,
			success: function(json){
				//alert(json);
				
				var table_body = $(json).find('.table_data').html();
				var export_link = $(json).find('.export_link').html();
				//alert(table_data);
				var table_data = $(table_body).find('.table_body').html();
				$("#tabe_body").html(table_data);
				//$("#export_div").html(export_link);
			}
		});
	}
}
function assign_underwriter(id, ref_id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url() ?>administration/quotes/assign_underwriter1/?id='+id+'&ref_id='+ref_id+'&type=quote',
    	autoSize: false,
    	closeBtn: true,
    	width: '250',
    	height: '150',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
		'afterClose':function () {
		  //window.location.reload();
		},
	});
}
</script>
