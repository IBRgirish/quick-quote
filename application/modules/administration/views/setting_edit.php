<?php 
$this->load->view('includes/head_style_script');
$key	= isset($_POST['key']) ? $_POST['key'] : (isset($setting->key)?$setting->key :NULL);
$value	= isset($_POST['email']) ? $_POST['email'] : (isset($setting->value)?$setting->value :NULL);
$sub	= isset($_POST['sub']) ? $_POST['sub'] : (isset($setting->sub)?$setting->sub :NULL);
$msg	= isset($_POST['msg']) ? $_POST['msg'] : (isset($setting->msg)?$setting->msg :NULL);
$status	= isset($_POST['status']) ? $_POST['status'] : (isset($setting->status)?$setting->status :NULL);
?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}
</script>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="container">
  <div class="content">
    <!-- Underwriter List -->
    <div class="span3">
    <?php //echo $type; ?>
      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmUnderwriter" id="frmUnderwriter" autocomplete="off">
        
		<label>Name (key) <em style="color:#FF0000">*</em></label>
        <input type="text" name="key" id="key" value="<?php echo $key ?>" class="span3" readonly="readonly" >
        <?php echo form_error('key','<span style="color:red"">','</span>'); ?>
        
		<label>Value <em style="color:#FF0000">*</em></label>
        <input type="text" name="value" id="value" value="<?php echo $value ?>" class="span3">
        <?php echo form_error('value','<span style="color:red"">','</span>'); ?>
        
        <?php if($type=='new_agency_email'||$type=='quote_release'||$type=='assign_underwriter'){?>
        <label>Sub <em style="color:#FF0000"></em></label>
        <input type="text" name="sub" id="sub" value="<?php echo $sub ?>" class="span3">
        <?php echo form_error('value','<span style="color:red"">','</span>'); ?>
        
        <label>Msg <em style="color:#FF0000"></em></label>
        <textarea class="span3" rows="4" name="msg" id="msg"><?php echo $msg ?></textarea>
       
        <?php echo form_error('value','<span style="color:red"">','</span>'); ?>
        <?php }?>
		<label>Status <em style="color:#FF0000">*</em></label>
        <input type="radio" name="status" id="status_active" value="1" <?php if ($status==1) echo 'checked'; ?> />
        &nbsp;Active&nbsp;
        <input type="radio" name="status" id="status_inactive" value="0" <?php if ($status==0) echo 'checked'; ?> />
        &nbsp;Inactive <br />
        <input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Add" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
    </div>
    <!-- Underwriter List -->
  </div>
</div>
</body>
