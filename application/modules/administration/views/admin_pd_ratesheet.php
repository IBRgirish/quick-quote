</form><div class="sheet_container">
					<div class="row-fluid">
						<span class="span12">
						  
						  <div class="well pull-center well-3 bga sheet-header">
							<h4 class="heading pull-center heading-3">Physical Damage Rate Sheet</h4>           
						  </div>
						</span>
				    </div>
					<div class="well well-3 well sheet-content">
						<?php
							//$attributes = array('class' => 'request_ratesheet_physical_damage', 'id' => 'main_form4');
							//echo form_open_multipart(base_url('ratesheet_physical_damage'), $attributes);
							
						?> 
                        <?php
								$attributes = array('class' => 'request_ratesheet', 'id' => 'main_form3');	
								$pd = isset($pd_rate['0']) ? (array) $pd_rate['0'] : '';		
								//print_r($pd);
								if($segement == 'edit_ratesheet'){
									$broker = isset($pd['broker']) ? $pd['broker'] :'';
									$insured = isset($pd['insured']) ? $pd['insured'] :'';
								}else{
									if($segement != 'manualrate'){
									$broker = isset($quote->contact_name) ? $quote->contact_name : ''.' '.$quote->contact_middle_name.' '.$quote->contact_last_name; 
									$insured = $quote->insured_fname.' '.$quote->insured_mname.' '.$quote->insured_lname;
									}else{
										$broker = '';
										$insured = '';
									}
								}	
								$base_rate = isset($pd['base_rate']) ? $pd['base_rate'] :'';	
							
						?>  
                        <form class="request_ratesheet" id="main_form3" action="javascript:void(0);" method="post">
						<input type="hidden" name="producer_email" class="producer_email" value="<?php echo isset($quote->email) ? $quote->email :''; ?>">
                         <input type="hidden" name="underwriter_ids" class="underwriter_ids" value="">
							<div class="row-fluid">
							
                        
                        
						<div class="row-fluid">
						  <span class="span6">
							<h5 class="heading  heading-4">Broker</h5>
						     <input type="text" class="span9" name="broker" placeholder="Full Name/ DBA	" value="<?= $broker?>">
						  </span>
						  <span class="span6">
							<h5 class="heading ">Insured</h5>
							<input type="text" class="span9" name="insured" placeholder="Full Name/ DBA	" value="<?= $insured?>">
						  </span>
						</div>
						<div class="row-fluid">
						  <span class="custom_span3">
						   <h5 class="heading  heading-3 hed-3">Radius of Operation</h5>
								<!--<input class="textinput span10 pull-left textinput-3 textinput-4 radiusofoperation" type="text" name="radius_of_operation" placeholder="">-->                
								<select name="radius_of_operation" class="select-3 span12 tractor_radius_of_operation" id="tractor_radius_of_operation_pd">
									<option value="0-100 miles" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == '0-100 miles') ? 'selected' : '' :''; ?> <?php if(isset($pd['radius_of_operation'])) { if($pd['radius_of_operation'] == '0-100 miles') echo 'selected'; } ?>>0-100 miles</option>
									<option value="101-500 miles" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == '101-500 miles') ? 'selected' : '' :''; ?> <?php if(isset($pd['radius_of_operation'])) {if($pd['radius_of_operation'] == '101-500 miles') echo 'selected';} ?>>101-500 miles</option>           	
									<option value="501+ miles" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == '501+ miles') ? 'selected' : '' :''; ?> <?php if(isset($pd['radius_of_operation'])) {if($pd['radius_of_operation'] == '501+ miles') echo 'selected'; } ?>>501+ miles</option>
									<option value="other" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == 'other') ? 'selected' : '' :''; ?> <?php if(isset($pd['radius_of_operation'])) {if($pd['radius_of_operation'] == 'other') echo 'selected';}?>>Other</option>                      
								</select><br>
									
							
						   
						  </span>
                          <?php $pdOther = isset($pd['radius_of_operation']) ? ($pd['radius_of_operation'] == 'other') ? 'style="display:block"' :'style="display:none"' : 'style="display:none"';?>
						  <span class="span2" <?php echo isset($quote_vehicle[0]->radius_of_operation) ? ($quote_vehicle[0]->radius_of_operation == 'other') ? 'style="display:block"' : 'style="display:none"' : $pdOther; ?> id="pdRoOther">
							<h5 class="heading  heading-4 hed-3">Others</h5>
                            <?php $pdOtherVal = isset($pd['other_rop']) ? $pd['other_rop'] : '';?>
						   <input class="textinput span12 pull-left show_tractor_others" type="text" name="pdRoOther" value="<?php echo isset($quote_vehicle[0]->radius_of_operation) ? $quote_vehicle[0]->radius_of_operation_other : $pdOtherVal; ?>" placeholder="" >
						   
						  </span>
						   <span class="span2">
								<h5 class="heading  heading-4 hed-3">State</h5>
							
									<?php
									$attr = "class='span12 ui-state-valid' onchange='changeCarrier(this.value, &quot;pd&quot;)'";
									//$pdstate =  isset($pd['state']) ? $pd['state'] :'';
									$broker_state = isset($quote->insured_state) ? ($quote->insured_state != '') ? $quote->insured_state : 'CA' : '';
									if($segement=='edit_ratesheet')	{
										$broker_state = isset($pd['state']) ? $pd['state'] : 'CA';	
									}
									?>
									<?php echo get_state_dropdown('state', $broker_state, $attr); ?>
						   
						  </span>
						   <span class="span2" >
							<h5 class="heading  heading-4 hed-3">Base rate</h5>
							<input class="textinput span10 pull-left textinput-3 textinput-4 price_format pd_base_rate" type="text" name="base_rate" placeholder="" value="<?= $base_rate; ?>">
						   
						  </span>              
						</div>
						<div class="hold">
							 <div class="row-fluid pull-center row-efl" style="text-align:right"> 
								<span class="span5">
									<h5 class="heading pull-left heading-4" style="text-align:right">Losses</h5>&nbsp;
									 <select name="losses_surcharge" class="select-3 span6 efl">
									  <option value="None" <?php if(isset($pd['losses_surcharge'])) { if(($pd['losses_surcharge'] == 'None') || ($pd['losses_surcharge'] == '0.00')) echo 'selected'; } ?>>None</option>
									   <option value="Yes" <?php if(isset($pd['losses_surcharge'])) { if(($pd['losses_surcharge'] == 'Yes')) echo 'selected'; } ?>>Yes</option>              
									</select> 
							   </span>
								<span style="padding: 0px 0px 5px 14px;display: block;float: left;">@</span> 
							  
							   <span class="span2">           
									<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
									<select name="losses_surcharge_cost" class="select-3 span12 mkread1 loses_cost nvc" 
	<?php isset($pd['losses_surcharge']) ? ($pd['losses_surcharge'] == 'None'||$pd['losses_surcharge'] == '0.00') ? 'disabled' : '' : 'disabled';  ?>>
										<option value="0" <?php if(isset($pd['losses_surcharge_cost'])) { if(($pd['losses_surcharge_cost'] == 'None') || ($pd['losses_surcharge_cost'] == '0.00')) echo 'selected'; } ?>>0</option>
										   <option value="5" <?php if(isset($pd['losses_surcharge_cost'])) { if(($pd['losses_surcharge_cost'] == '5') || ($pd['losses_surcharge_cost'] == '5.00')) echo 'selected'; } ?>>5%</option>
										   <option value="10" <?php if(isset($pd['losses_surcharge_cost'])) { if(($pd['losses_surcharge_cost'] == '10') || ($pd['losses_surcharge_cost'] == '10.00')) echo 'selected'; } ?>>10%</option>
										   <option value="15" <?php if(isset($pd['losses_surcharge_cost'])) { if(($pd['losses_surcharge_cost'] == '15') || ($pd['losses_surcharge_cost'] == '15.00')) echo 'selected'; } ?>>15%</option>
										   <option value="20" <?php if(isset($pd['losses_surcharge_cost'])) { if(($pd['losses_surcharge_cost'] == '20') || ($pd['losses_surcharge_cost'] == '20.00')) echo 'selected'; } ?>>20%</option>
										   <option value="25" <?php if(isset($pd['losses_surcharge_cost'])) { if(($pd['losses_surcharge_cost'] == '25') || ($pd['losses_surcharge_cost'] == '25.00')) echo 'selected'; } ?>>25%</option>
										   <option value="30" <?php if(isset($pd['losses_surcharge_cost'])) { if(($pd['losses_surcharge_cost'] == '30') || ($pd['losses_surcharge_cost'] == '30.00')) echo 'selected'; } ?>>30%</option>            
									</select>
								   
							  </span>
							  <span class="span3">
								<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
							   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 lsp nvp price_format_decimal" type="text" name="losses_surcharge_premium" placeholder="" value="<?php echo isset($pd['losses_surcharge_premium']) ? $pd['losses_surcharge_premium'] : ''; ?>" readonly="readonly">
							   
							  </span>
							</div>
							 <div class="row-fluid pull-center row-efl" style="text-align:right"> 
								<span class="span5">
									<h5 class="heading pull-left heading-4" style="text-align:right">Misc.surcharges</h5>&nbsp;
									 <select name="miscellaneos_surcharge" class="select-3 span6 efl">
									   <option value="None" <?php if(isset($pd['miscellaneos_surcharge'])) { if(($pd['miscellaneos_surcharge'] == 'None') || ($pd['miscellaneos_surcharge'] == '0.00')) echo 'selected'; } ?>>None</option>
									   <option value="Yes" <?php if(isset($pd['miscellaneos_surcharge'])) { if(($pd['miscellaneos_surcharge'] == 'Yes') || ($pd['miscellaneos_surcharge'] == '0.00')) echo 'selected'; } ?>>Yes</option>                             
									</select>
							   
								</span>
								<span style="padding: 0px 0px 5px 14px;display: block;  float: left;">@</span> 
							  
							  <span class="span2">           
								<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
										<select name="miscellaneos_surcharge_cost" class="select-3 span12 mkread1 mis_cost nvc" <?php isset($pd['miscellaneos_surcharge']) ? ($pd['miscellaneos_surcharge'] == 'None'||$pd['miscellaneos_surcharge'] == '0.00') ? 'disabled' : '' : 'disabled';  ?>>
											<option value="0" <?php if(isset($pd['miscellaneos_surcharge_cost'])){ if(($pd['miscellaneos_surcharge_cost'] == 'None') || ($pd['miscellaneos_surcharge_cost'] == '0.00')) echo 'selected'; }?>>0</option>
										   <option value="5" <?php if(isset($pd['miscellaneos_surcharge_cost'])){ if(($pd['miscellaneos_surcharge_cost'] == '5') || ($pd['miscellaneos_surcharge_cost'] == '5.00')) echo 'selected';} ?>>5%</option>
										   <option value="10" <?php if(isset($pd['miscellaneos_surcharge_cost'])){ if(($pd['miscellaneos_surcharge_cost'] == '10') || ($pd['miscellaneos_surcharge_cost'] == '10.00')) echo 'selected';} ?>>10%</option>
										   <option value="15" <?php if(isset($pd['miscellaneos_surcharge_cost'])){ if(($pd['miscellaneos_surcharge_cost'] == '15') || ($pd['miscellaneos_surcharge_cost'] == '15.00')) echo 'selected'; }?>>15%</option>
										   <option value="20" <?php if(isset($pd['miscellaneos_surcharge_cost'])){ if(($pd['miscellaneos_surcharge_cost'] == '20') || ($pd['miscellaneos_surcharge_cost'] == '20.00')) echo 'selected'; }?>>20%</option>
										   <option value="25" <?php if(isset($pd['miscellaneos_surcharge_cost'])){ if(($pd['miscellaneos_surcharge_cost'] == '25') || ($pd['miscellaneos_surcharge_cost'] == '25.00')) echo 'selected'; }?>>25%</option>
										   <option value="30" <?php if(isset($pd['miscellaneos_surcharge_cost'])){ if(($pd['miscellaneos_surcharge_cost'] == '30') || ($pd['miscellaneos_surcharge_cost'] == '30.00')) echo 'selected'; } ?>>30%</option>           
										</select>
							  </span>
							  <span class="span3">
								<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
							   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 msp nvp price_format_decimal" type="text" name="miscellaneos_surcharge_premium" placeholder="" value="<?php echo isset($pd['miscellaneos_surcharge_premium']) ? $pd['miscellaneos_surcharge_premium'] : ''; ?>" readonly="readonly">
							   
							  </span>
							</div>
							<div class="row-fluid pull-center row-efl" style="text-align:right"> 
								 <span class="span5">
								<h5 class="heading pull-left heading-4" style="text-align:right">Driver surcharges(value)</h5>&nbsp;
										  
								<input class="textinput span6 pull-left textinput-3 textinput-4 select-3 span4 driver_surcharge numr_valid efl" type="text" name="driver_surcharge" placeholder="" value="<?php echo isset($pd['driver_surcharge']) ? $pd['driver_surcharge'] : ''; ?>">
							   
							  </span>
								<span style="   padding: 0px 0px 5px 14px;    display: block;    float: left;">@</span> 
							  <span class="span2">           
								<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
							   <select name="driver_surcharge_cost" class="select-3 span12 driver_cost mkread1 mis_cost nvc" <?php isset($pd['driver_surcharge']) ? ($pd['driver_surcharge'] == 'None'||$pd['driver_surcharge'] == '0.00') ? 'disabled' : '' : 'disabled';  ?>>
								  <option value="0" <?php if(isset($pd['driver_surcharge_cost'])){ if(($pd['driver_surcharge_cost'] == 'None') || ($pd['driver_surcharge_cost'] == '0.00')) echo 'selected';} ?>>0</option>
								   <option value="5" <?php if(isset($pd['driver_surcharge_cost'])){ if(($pd['driver_surcharge_cost'] == '5') || ($pd['driver_surcharge_cost'] == '5.00')) echo 'selected'; }?>>5%</option>
								   <option value="10" <?php if(isset($pd['driver_surcharge_cost'])){ if(($pd['driver_surcharge_cost'] == '10') || ($pd['driver_surcharge_cost'] == '10.00')) echo 'selected'; }?>>10%</option>
								   <option value="15" <?php if(isset($pd['driver_surcharge_cost'])){ if(($pd['driver_surcharge_cost'] == '15') || ($pd['driver_surcharge_cost'] == '15.00')) echo 'selected';} ?>>15%</option>
								   <option value="20" <?php if(isset($pd['driver_surcharge_cost'])){ if(($pd['driver_surcharge_cost'] == '20') || ($pd['driver_surcharge_cost'] == '20.00')) echo 'selected'; }?>>20%</option>
								   <option value="25" <?php if(isset($pd['driver_surcharge_cost'])){ if(($pd['driver_surcharge_cost'] == '25') || ($pd['driver_surcharge_cost'] == '25.00')) echo 'selected';} ?>>25%</option>
								   <option value="30" <?php if(isset($pd['driver_surcharge_cost'])){ if(($pd['driver_surcharge_cost'] == '30') || ($pd['driver_surcharge_cost'] == '30.00')) echo 'selected';} ?>>30%</option>          
								</select>
							  </span>
							  <span class="span3">
								<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
							   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  dsp nvp price_format_decimal" type="text" name="driver_surcharge_premium" placeholder="" value="<?php echo isset($pd['driver_surcharge_premium']) ? $pd['driver_surcharge_premium'] : ''; ?>" readonly="readonly">
							   
							  </span>
							</div>
							<div class="row-fluid pull-center row-efl" style="text-align:right"> 
						  <span class="span5">
							<h5 class="heading pull-left heading-4" style="text-align:right">15 year tractor/trailer quantity</h5>&nbsp;
							<input class="select-3 span3 tractor_trailer_qunt numr_valid efl" type="text" name="tractor/trailer_quantity" placeholder="" value="<?php   echo isset($pd['tractor/trailer_quantity']) ? $pd['tractor/trailer_quantity'] : ''; ?>">
						  </span>
						  <span style="padding: 0px 0px 5px 14px;display: block;float: left;">@</span> 
						  
						  <span class="span2">           
							<input class="textinput span12 pull-left textinput-3 textinput-4 ttcc mkread1 mis_cost price_format" type="text" name="tractor/trailer_surcharge_cost" placeholder="$200" value="200" 
							<?php isset($pd['tractor/trailer_quantity']) ? ($pd['tractor/trailer_quantity']=='0'||$pd['tractor/trailer_quantity']=='0.00') ? 'readonly' : '' : '';?> >
						   
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 ttcp scp price_format" type="text" name="tractor/trailer_surcharge_premium" placeholder="" value="<?php echo isset($pd['tractor/trailer_surcharge_premium']) ? $pd['tractor/trailer_surcharge_premium'] : ''; ?>" readonly="readonly">
						  </span>
						</div>
						</div>
						<strong > Optional endrosements:</strong>
						<div class="row-fluid pull-center row-efl" style="text-align:right"> 
							<span class="span4">
								<h5 class="heading pull-left heading-4" style="text-align:right">Debris removal</h5>&nbsp;
								<select name="debris_removal_limit" class="select-3 span4 efl">
								  <option value="None" <?php if(isset($pd['debris_removal_limit'])){  echo isset($pd['debris_removal_limit']) & ($pd['debris_removal_limit']=='None') ? 'selected' : ''; }?>>None</option>
								   <option value="2,500" <?php if(isset($pd['debris_removal_limit'])){  echo isset($pd['debris_removal_limit']) & ($pd['debris_removal_limit']=='2,500') ? 'selected' : ''; }?>>$2,500</option>                           
								</select> 
							</span>
							<span style="    padding: 0px 0px 5px 14px;    display: block;    float: left;">@</span> 
							  <span class="span2">           
								<!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
								<select name="debris_removal_cost" class="select-3 span12 mkread1 drc" 
								<?php echo isset($pd['debris_removal_limit']) ? ($pd['debris_removal_limit']=='None') ? 'disabled' : ''  :'';?>>
								  <option value="0" <?php if(isset($pd['debris_removal_cost'])){ echo isset($pd['debris_removal_cost']) & (intval($pd['debris_removal_cost'])==0) ? 'selected' : ''; } ?>>0</option>
								   <option value="150" <?php if(isset($pd['debris_removal_cost'])){ echo isset($pd['debris_removal_cost']) & (intval($pd['debris_removal_cost'])==150) ? 'selected' : ''; }?>>$150</option>                           
								</select> 
							  </span>
							  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 drp price_format" type="text" name="debris_removal_premium" placeholder="" value="<?php echo isset($pd['debris_removal_premium']) ? intval($pd['debris_removal_premium']) : ''; ?>" readonly="readonly">
						  </span>
						</div>
						<div class="row-fluid pull-center row-efl" style="text-align:right"> 
							 <span class="span4">
							<h5 class="heading pull-left heading-4" style="text-align:right">Towing labor storage</h5>&nbsp;
							 <select name="towing_labor_storage" class="select-3 span4 efl">
							 <option value="None" <?php if(isset($pd['towing_labor_storage'])){ echo isset($pd['towing_labor_storage']) & (intval($pd['towing_labor_storage'])=='None') ? 'selected' : ''; }?>>None</option>
							  <option value="7500" <?php if(isset($pd['towing_labor_storage'])){ echo isset($pd['towing_labor_storage']) & (intval($pd['towing_labor_storage'])==7500) ? 'selected' : ''; }?>>$7,500</option>                              
							</select> 
						  
						  </span>
						  <span style="padding: 0px 0px 5px 14px; display: block; float: left;">@</span> 
						  
						   <span class="span2">           
							<select name="towing_labor_storage_cost" class="select-3 span12 mkread1 tls" <?php echo isset($pd['towing_labor_storage']) ?  (intval($pd['towing_labor_storage'])=='None'||intval($pd['towing_labor_storage'])=='0'||intval($pd['towing_labor_storage'])=='0.00') ? 'disabled' : '' : ''; ?> >
							   <option value="None" <?php if(isset($pd['towing_labor_storage_cost'])){ echo isset($pd['towing_labor_storage_cost']) & (intval($pd['towing_labor_storage_cost'])=='None') ? 'selected' : ''; }?>>None</option>
							  <option value="200" <?php if(isset($pd['towing_labor_storage_cost'])){ echo isset($pd['towing_labor_storage_cost']) & (intval($pd['towing_labor_storage_cost'])==200) ? 'selected' : ''; }?>>$200</option>                            
							</select> 
						  </span>
						  <span class="span3">
							<h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
						   &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4  tlp price_format" type="text" name="towing_labor_storage_premium" placeholder="" value="<?php echo isset($pd['towing_labor_storage_premium']) ? intval($pd['towing_labor_storage_premium']) : ''; ?>" readonly="readonly">
						   
						  </span>
						</div>
						<div class="row-fluid pull-center" style="text-align:right"> 
						 <span class="span4">
							<h5 class="heading pull-left heading-4" style="text-align:right">Number of Tractor</h5>&nbsp;
								 <input class="textinput span5 pull-left textinput-3 textinput-4 select-3 span4 no_of_tractor" type="text" name="number_of_tractor" placeholder=""  id="no_of_vehicle" value="<?php echo isset($tractor_vehicle) ? count($tractor_vehicle) : ''; ?>" data-min="<?php echo isset($tractor_vehicle) ? count($tractor_vehicle) : ''; ?>">
						   
						  </span>
						   <span class="span3">  
							<h5 class="heading pull-left heading-4" style="text-align:right">Number of Truck</h5>&nbsp;         
							<input class="textinput span5 pull-left textinput-3 textinput-4 select-3 span4 trkcnt no_of_truck" type="text" name="no_of_truck" placeholder="" id="no_of_truck" value="<?php echo isset($truck_vehicle) ? count($truck_vehicle) : ''; ?>">
						  </span>
						   <span class="span3">  
							<h5 class="heading pull-left heading-4" style="text-align:right">Number of Trailer</h5>&nbsp;         
							<input class="textinput span5 pull-left textinput-3 textinput-4 select-3 span4 trailercnt number_of_trailer" type="text" name="number_of_trailer" placeholder="" id="no_of_trailer" value="<?php echo isset($trailer_vehicle) ? count($trailer_vehicle) :''; ?>">
						  </span>
						</div>
						
						<div class="tb">
								<table width="100%" border="1">
									<thead>
										  <tr style="background-color:#CCCCCC; color:#000000; text-weight:bold;">
											<th width="16%">Vehicles</th>
											<th width="19%">value/limit</th>
											<th width="15%">Deductible</th>
											<th width="16%">% Rate</th>
											<th width="18%">or Rate</th>
											<th width="16%">Premium</th>
										  </tr>
									</thead>
									<tbody id="add_owner_row">
										<?php 
										$tractorcnt = 0;
										$tot_tractor_value = 0;
										$tot_liability_ded = 0;
										//print_r($tractor_vehicle);
										if($segement=='edit_ratesheet'){
											$pd1 = 'vh_pd';
											$pd_ded1 = 'vh_pd_ded';
										}else{
											$pd1 = 'pd';
											$pd_ded1 = 'pd_ded';	
										}
										if(isset($tractor_vehicle)){
											foreach($tractor_vehicle as $tractor)
											{
												
												$tractorcnt++;
												$tot_tractor_value += getAmount($tractor->$pd1);
												if($segement!='edit_ratesheet'){
													$tot_liability_ded += getAmount($tractor->liability_ded);
												}
												?>
													<tr id="tractorrw<?php echo $tractorcnt; ?>" class="tractorrw">
														<td class="vehicle_name">Tractor <?php echo $tractorcnt; ?><input type="hidden" name="tractorname_<?php echo $tractorcnt; ?>" value="Truck <?php echo $tractorcnt; ?>">
														<input type="hidden" name="tractor_id<?php echo $tractorcnt; ?>" value="<?= isset($tractor->id) ? $tractor->id : '';?>">
														</td>
														<td style="text-align:center"><input type="text" class="tractor_value vehicle_value span6 ui-state-valid price_format" value="<?php echo $tractor->$pd1; ?>" name="tractor_val<?php echo $tractorcnt; ?>"></td>
														<td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="<?php echo $tractor->$pd_ded1; ?>" name="tractor_ded<?php echo $tractorcnt; ?>"></td>
														<td style="text-align:center"><input type="text" name="tractor_rate<?php echo $tractorcnt; ?>" class="span6 vehicle_rate ui-state-valid" value="<?= isset($tractor->vh_rate) ? $tractor->vh_rate : '';?>"></td>
														<td style="text-align:center"><input type="text" name="tractor_flat_rate<?php echo $tractorcnt; ?>" class="span6 numr_valid vehicle_frate ui-state-valid" value="<?= isset($tractor->vh_frate) ? $tractor->vh_frate : '';?>"></td>
														<td style="text-align:center"><input type="text" name="tractor_premium<?php echo $tractorcnt; ?>" class="span6 tractor_prem vehicle_rp price_format_decimal ui-state-valid" value="<?= isset($tractor->vh_primium) ? $tractor->vh_primium : '';?>"></td>
													</tr>
													
												<?php
											}
										}
										?>
										
										<?php 
										$truckcnt = 0;
										$tot_truc_value = 0;
										$truc_liability_ded = 0;
										if(isset($truck_vehicle)){
											foreach($truck_vehicle as $truck)
											{
												$truckcnt++;
												$tot_truc_value += getAmount($truck->$pd1);
												if($segement!='edit_ratesheet'){
													$truc_liability_ded += getAmount($truck->liability_ded);
												}
												?>
													<tr id="truckrw<?php echo $truckcnt; ?>" class="truckrw">
														<td class="vehicle_name">Truck <?php echo $truckcnt; ?><input type="hidden" name="truckname_<?php echo $truckcnt; ?>" value="Truck <?php echo $truckcnt; ?>">
														 <input type="hidden" name="truck_id<?php echo $truckcnt; ?>" value="<?= isset($truck->id) ? $truck->id : '';?>">
														 </td>
														<td style="text-align:center"><input type="text" class="truck_value vehicle_value span6 ui-state-valid price_format" value="<?php echo $truck->$pd1; ?>" name="truck_val<?php echo $truckcnt; ?>"></td>
														<td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="<?php echo $truck->$pd_ded1; ?>" name="truck_ded<?php echo $truckcnt; ?>"></td>
														<td style="text-align:center"><input type="text" name="truck_rate<?php echo $truckcnt; ?>" class="span6 vehicle_rate ui-state-valid" value="<?= isset($truck->vh_rate) ? $truck->vh_rate : '';?>"></td>
														<td style="text-align:center"><input type="text" name="truck_flat_rate<?php echo $truckcnt; ?>" class="span6 numr_valid vehicle_frate ui-state-valid" value="<?= isset($truck->vh_frate) ? $truck->vh_frate : '';?>"></td>
														<td style="text-align:center"><input type="text" name="truck_premium<?php echo $truckcnt; ?>" class="span6 truck_prem1 vehicle_rp price_format_decimal ui-state-valid" value="<?= isset($truck->vh_primium) ? $truck->vh_primium : '';?>"></td>
													</tr>
												<?php
											}
										}
										?>
										
										<?php 
										$tot_trailer_value = 0;
										$trailercnt = 0;
										$trail_liability_ded = 0;
										if(isset($trailer_vehicle)){
											foreach($trailer_vehicle as $trailer)
											{
												
												$trailercnt++;
												$tot_trailer_value += getAmount($trailer->$pd1);
												if($segement!='edit_ratesheet'){
													$trail_liability_ded += getAmount($tractor->liability_ded);
												}
												?>
													<tr id="trailerrw<?php echo $trailercnt; ?>" class="trailerrw">
														<td class="vehicle_name">Trailer <?php echo $trailercnt; ?><input type="hidden" name="trailername_<?php echo $trailercnt; ?>" value="Trailer <?php echo $trailercnt; ?>">
														<input type="hidden" name="trailer_id<?php echo $trailercnt; ?>" value="<?= isset($trailer->id) ? $trailer->id : '';?>">
														</td>
														<td style="text-align:center"><input type="text" class="trailer_value vehicle_value span6 ui-state-valid price_format" value="<?php echo $trailer->$pd1; ?>" name="trailer_val<?php echo $trailercnt; ?>"></td>
														<td style="text-align:center"><input type="text" class="price_format vehicle_ded span6 ui-state-valid" value="<?php echo $trailer->$pd_ded1; ?>" name="trailer_ded<?php echo $trailercnt; ?>"></td>
														<td style="text-align:center"><input type="text" name="trailer_rate<?php echo $trailercnt; ?>" class="span6 vehicle_rate ui-state-valid" value="<?= isset($trailer->vh_rate) ? $trailer->vh_rate : '';?>"></td>
														<td style="text-align:center"><input type="text" name="trailer_flat_rate<?php echo $trailercnt; ?>" class="span6 numr_valid vehicle_frate ui-state-valid" value="<?= isset($trailer->vh_frate) ? $trailer->vh_frate : '';?>"></td>
														<td style="text-align:center"><input type="text" name="trailer_premium<?php echo $trailercnt; ?>" class="span6 trailer_prem vehicle_rp price_format_decimal ui-state-valid" value="<?= isset($trailer->vh_primium) ? $trailer->vh_primium : '';?>"></td>
													</tr>
													
												<?php
											}
										}
										?>
			
									</tbody>
								</table>
						</div>
						   <?php $deductible = $tot_liability_ded+$trail_liability_ded+$truc_liability_ded; ?>
                           
							<div class="tdd">
							 <div class="row-fluid pull-center" style="text-align:left;"> 
								 <span class="span4">
								<h5 class="heading pull-left heading-4 span6" style="text-align:left">Total Tractor Limit</h5>&nbsp;
									 <input class="textinput span5 pull-left textinput-3 textinput-4 price_format total_tractor_limit" type="text" name="total_tractor_limit" placeholder="" value="<?php echo isset($pd['total_tractor_limit']) ? $pd['total_tractor_limit'] : $tot_tractor_value; ?>">
							   
							  </span>
							   <span class="span4">  
								<h5 class="heading pull-left heading-4 span6" style="text-align:left;width:58%;">Total Tractor premium</h5>&nbsp;         
								<input class="textinput span5 pull-left textinput-3 textinput-4 price_format_decimal" id="total_tractor_premium_pd" type="text" name="total_tractor_premium" placeholder="" value="<?= isset($pd['total_tractor_premium']) ? $pd['total_tractor_premium'] : ''; ?>" style="margin:0px 0px 0px -4px;">
							  </span><br />
							  
							</div>
							 <div class="row-fluid pull-center" style="text-align:left; "> 
								 <span class="span4">
								<h5 class="heading pull-left heading-4 span6" style="text-align:left">Total Trailer Limit</h5>&nbsp;
									 <input class="textinput span5 pull-left textinput-3 textinput-4 price_format total_trailer_limit" type="text" name="total_trailer_limit" placeholder="" value="<?= isset($pd['total_trailer_limit']) ? $pd['total_trailer_limit'] : $tot_trailer_value; ?>">
							   
							  </span>
							   <span class="span4">  
								<h5 class="heading pull-left heading-4" style="text-align:left;width:57%;">Total Trailer premium</h5>&nbsp;         
								<input class="textinput span5 pull-left textinput-3 textinput-4 price_format_decimal" type="text" name="total_trailer_premium" placeholder="" id="total_trailer_premium_pd" value="<?= isset($pd['total_trailer_premium']) ? $pd['total_trailer_premium'] : ''; ?>">
							  </span><br />
							  
							</div>
                             <div class="row-fluid pull-center" style="text-align:left;"> 
								 <span class="span4">
								<h5 class="heading pull-left heading-4 span6" style="text-align:left">Total Truck Limit</h5>&nbsp;
									 <input class="textinput span5 pull-left textinput-3 textinput-4 price_format total_truck_limit" type="text" name="total_truck_limit" placeholder="" value="<?php echo isset($pd['total_truck_limit']) ? $pd['total_truck_limit'] : $tot_truc_value; ?>">
							   
							  </span>
							   <span class="span4">  
								<h5 class="heading pull-left heading-4" style="text-align:left;width:57%;">Total Truck premium</h5>&nbsp;         
								<input class="textinput span5 pull-left textinput-3 textinput-4 price_format_decimal" id="total_truck_premium_pd" type="text" name="total_truck_premium" placeholder="" value="<?= isset($pd['total_truck_premium']) ? $pd['total_truck_premium'] : ''; ?>">
							  </span><br />
							  
							</div>
							</div>
						 
							 <div class="row-fluid">
							  <span class="span2">
								<h5 class="heading  heading-4">Total PD premium</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4 total_pd_pr price_format_decimal" type="text" id="total_pd_pr" name="total_pd_premium" placeholder="" value="<?= isset($pd['total_pd_premium']) ? $pd['total_pd_premium'] : ''; ?>" readonly>
							   
							  </span>
							  <span class="span3">
								<h5 class="heading  heading-4"> Carrier</h5>
								<div class="pd_carrier_drop_down">
										<?php
                                            $attr1 = "id='carrier_cargo2' class='span12 ui-state-valid select-required' onchange='updatePolicyValue(&quot;pd&quot;, this.value)' ";
                                            $broker_state = '';
                                             if($carriers!=''){ $carriers = $carriers; } else {$carriers = array();}
											 $broker_state = isset($pd['carrier']) ? $pd['carrier'] : '';											
                                        ?>
                                        <?php echo form_dropdown('carriers', $carriers, $broker_state, $attr1); ?>
                                    </div>
								
							  </span>	
							   <span class="span2">
								<h5 class="heading  heading-4">PD police fee</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4 price_format" type="text" name="pd_policy_fee" id="pd_policy_fee" placeholder="" value="<?= isset($pd['pd_policy_fee']) ? intval($pd['pd_policy_fee']) : ''; ?>">
							   
							  </span>
							   <span class="span2">
								<h5 class="heading  heading-4">PD SLA tax</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4  price_format_decimal" type="text" name="pd_sla_tax" id="pd_sla_tax" placeholder="" value="<?= isset($pd['pd_sla_tax']) ? $pd['pd_sla_tax'] : ''; ?>">
							   
							  </span>
							  
							   <span class="span3">
								<h5 class="heading  heading-4">Required for FIRM quote</h5>
								<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="required_for_firm_quote" placeholder="" value="<?= isset($pd['required_for_firm_quote']) ? $pd['required_for_firm_quote'] : ''; ?>">
							  </span>
							</div>
							 <div class="row-fluid">
							  <span class="span12">
								<h5 class="heading pull-left">Comments</h5>
								<input class="textinput span9" type="text" placeholder="" name="comments" value="<?= isset($pd['comments']) ? $pd['comments'] : ''; ?>">
							  </span>
							 </div>
								<div id='loader2' style="display:none"><img src="<?php echo base_url(); ?>images/spinner.gif"/></div><div id="cargo_sheet_message"></div>
								<div id="physical_damage_sheet_message" style="color:green"></div>
								<!--<input type="hidden" class="pd_id" value="" name="pd_id">-->
								 <input type="hidden" name="bundleid" class="bundleid" value="<?php echo isset($bundleid) ? $bundleid : ''; ?>">
								<button class="btn btn-primary" id="physical_damage_submit" type="submit" >Save Draft</button>
								<input type="hidden" name="for_submit1" value="1" />
                                  <input type="hidden" name="quote_id" value="<?php echo $quote_id; ?>" id="quote_id1"/>
                                <input type="hidden" id="base_url" name="" value="<?php echo base_url();?>" />
								<input type="hidden" name="action" value="<?php echo $action; ?>" />
							</div>
							
								
							</form>
					</div>
				
				</div>
<script>
changeCarrierPd();
function changeCarrierPd(){
	var val = $('#main_form3 select[name=state]').val();
	var type = 'pd';
	var selected = '<?php echo $broker_state; ?>';
	var url = '<?php echo base_url('ratesheet'); ?>/get_carrier_by_state';
	changeCarrier1(val, type, url, selected)
}
</script>
<?php $data['deductible'] = '';?>
<?php $this->load->view('admin_liability_ratesheet', $data); ?>