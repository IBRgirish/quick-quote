<?php 
$this->load->view('includes/head_style_script');

$id	= isset($_POST['underwriterId']) ? $_POST['underwriterId'] : (isset($underwriter->id)?$underwriter->id :NULL);
$name	= isset($_POST['name']) ? $_POST['name'] : (isset($underwriter->name)?$underwriter->name :NULL);
$email	= isset($_POST['email']) ? $_POST['email'] : (isset($underwriter->email)?$underwriter->email :NULL);
$status	= isset($_POST['status']) ? $_POST['status'] : (isset($underwriter->status)?$underwriter->status :NULL);
$password	= isset($_POST['password']) ? $_POST['password'] : (isset($underwriter->password)?$underwriter->password :NULL);
?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}
</script>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="container">
  <div class="content">
    <!-- Underwriter List -->
    <div class="span3">
      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmUnderwriter" id="frmUnderwriter" autocomplete="off">
        
		<input type="hidden" name="underwriterId" id="underwriterId" value="<?php echo $id ?>" />
        
		<label>Name <em style="color:#FF0000">*</em></label>
        <input type="text" name="name" id="name" value="<?php echo $name ?>" class="span3">
        <?php echo form_error('name','<span style="color:red"">','</span>'); ?>
        
		<label>Email Address <em style="color:#FF0000">*</em></label>
        <input type="text" name="email" id="email" value="<?php echo $email ?>" class="span3">
        <?php echo form_error('email','<span style="color:red"">','</span>'); ?>
        
		<label>Password</label>
        <input type="text" name="password" id="password" class="span3" value="<?php echo $password ?>">
        
		<label>Status <em style="color:#FF0000">*</em></label>
        <input type="radio" name="status" id="status_active" value="1" <?php if ($status==1) echo 'checked'; ?> />
        &nbsp;Active&nbsp;
        <input type="radio" name="status" id="status_inactive" value="0" <?php if ($status==0) echo 'checked'; ?> />
        &nbsp;Inactive <br />
        <input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Update" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
    </div>
    <!-- Underwriter List -->
  </div>
</div>
</body>
