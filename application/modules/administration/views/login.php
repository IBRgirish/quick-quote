<?php $this->load->view('administration/includes/header'); ?>

<?php
if ($this->session->flashdata('error')){    
		//echo '<div class="error"><p>'.$this->session->flashdata('error').'</p></div>';
		$flash_error = $this->session->flashdata('error');
	}
	if ($this->session->flashdata('success')){    
		echo '<div class="success">'.$this->session->flashdata('success').'</div>';
	}
	// Validation Errors
	  $error_message = (validation_errors()?'<div class="error">'.validation_errors().'</div>':'');
	  
    if(!empty ($error_message)) {
		$error_message_div = '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$error_message.'</div>';
	} else {
		$error_message_div = '';
	}	
	
    if(!empty ($flash_error)) {
		$error_message_div .= '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$flash_error.'</div>';
	} 
	
?>
  
		 
 <div class="row">
    <div class="span4 offset4">
		<div class="well">
		<legend>Adminitrator Login</legend>
			<form id="frmLogin" name="frmLogin" method="post" action="<?php echo base_url(uri_string());?>" accept-charset="UTF-8">
			<?php echo $error_message_div ?>
			<input class="span3" placeholder="Email" name="username" type="text" id="username" value="<?php echo set_value('username');?>">
			<input class="span3" placeholder="Password" name="password" type="password"  id="password">
			<button class="btn btn-primary" type="submit">Login</button>
			</form>
		</div>
    </div>
</div>
