<?php $this->load->view('includes/head_style_script'); ?>

<body>
<div class="row-fluid" style="margin: 14px 0px 15px 0px;">
	<?php echo $underwriter;?>
</div>

<div class="row-fluid">
 <div id='prod_userloader3' style="display:none">
    <img src="<?php echo base_url(); ?>images/spinner.gif"/>
</div>
<a href="javascript:;" class="btn btn-primary" onClick="changeUnderwriter('<?= $quote_id; ?>', '<?= $link; ?>', <?= $is_maual?>)">Change</a>
</div>
<script>
function changeUnderwriter(id, url_link, is_maual)
{
	var n= 0;
	var underwriter = [];
	$('[name="underwriter[]"]').each(function(index) {	
		 if($(this).is(':checked'))
		 {	
			  underwriter[n] = $(this).val();
			  n++;	
		}		
	});
	var baseurl2 = "<?php echo base_url('administration'); ?>/quotes/changeUnderwriter";
	if(n>0)
	{
		$.ajax({
			  url: baseurl2,
			  type: "post",
			  data: 'id='+id+'&underwriter='+underwriter+'&url_link='+url_link+'&is_maual='+is_maual,
			  beforeSend: function() {
				 $('#prod_userloader3').show();
			  },
			  complete: function(){
				  $('#prod_userloader3').hide();
			  },
			  success: function(data){
				//alert(data);
				
				parent.jQuery.fancybox.close();
				parent.window.location.reload();
				
			  },
			  error:function(){
			  
			  }   
		});
	}
	else{
		alert('Please select atleast on option');
	}
}
</script>
</body>
</html>
