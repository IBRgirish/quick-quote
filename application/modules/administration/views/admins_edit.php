<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/header');
}
?>
 <?php if(permission_levels('user')) { ?>
<?php
	
	$header=isset($admins->id)?'Modify Admin':'Add Admin';
	echo '<h1>'.$header.'</h1><br>';

	  if ($this->session->flashdata('error')){    
		echo '<div class="error">'.$this->session->flashdata('error').'</div>';
	}
	if ($this->session->flashdata('success')){    
		echo '<div class="success">'.$this->session->flashdata('success').'</div>';
	}
	// Validation Errors
	  echo (validation_errors()?'<div class="error">'.validation_errors().'</div>':'');

echo '<div class="table" id="addtable"><img height="7" width="8" class="left" alt="" src="'.base_url().'img/admin/bg-th-left.gif"> <img height="7" width="7" class="right" alt="" src="'.base_url().'img/admin/bg-th-right.gif">';
echo form_open(uri_string(), 'id="frmAdminUpdate"');
$this->load->library('table');

?>
<?php
	$tmpl = array ( 'table_open'  => '<table border="0" cellpadding="0" cellspacing="0" class="listing form">' );
	$this->table->set_template($tmpl);
	if(isset($admins->id))		
		$this->table->set_heading(array('data' => 'Modify Admin', 'class' => 'full', 'colspan' => 2));
	else		
		$this->table->set_heading(array('data' => 'Add Admin', 'class' => 'full', 'colspan' => 2));

    // ** Place information in between these comments ** //        
        $data = array(
                      'name'        => 'name',
                      'id'          => 'name',
                      'value'       =>  set_value('name', (isset($admins->name)?$admins->name:NULL)),
                   );
			        
		$this->table->add_row(array('<strong>Name:</strong>', form_input($data)));	
		          
        $data = array(
                      'name'        => 'password',
                      'id'          => 'password',
                      'type'          => 'password',
                      'value'       =>  set_value('password'),
                    );
        $this->table->add_row(array('<strong>Password:</strong>', form_input($data)));
		          
        $data = array(
                      'name'        => 'email',
                      'id'          => 'email',
                      'value'       =>  set_value('email', (isset($admins->email)?$admins->email:NULL)),
                   );
			
         $this->table->add_row(array('<strong>Email:</strong>', form_input($data)));
		          
			$options = array(
                  'Active'  => 'Active',
                  'Pending'    => 'Pending'
                );
		
		 $this->table->add_row(array('<strong>Status:</strong>', form_dropdown('status', $options, (isset($admins->status)?$admins->status:''))));

		 $usertype = array(
                  'superadmin'  => 'Super Admin',
                  'reportadmin'    => 'Report Admin',
                  'useradmin'    => 'User Admin',
                  'memberadmin'    => 'Member Admin',
                  'propertyadmin'    => 'Property Admin',
                );
		
		 $this->table->add_row(array('<strong>User Type:</strong>', form_dropdown('admin_type', $usertype, (isset($admins->admin_type)?$admins->admin_type:''))));
		
		$resetbtn='&nbsp;<input type="button" onclick="closeWindow(\''.base_url('administration/admins/').'\')" value="Cancel" class="button">';
		$this->table->add_row(array('', form_submit('mysubmit', 'Save', 'class="button"').$resetbtn));
        ?>

<input type="hidden" name="id" id="id" value="<?php echo set_value('id', (isset($admins->id)?$admins->id:NULL));?>" >
<?php		  
    // ** Place information in between these comments ** //
   echo $this->table->generate();
    echo form_close(); 
	echo '</div>';
		?>

 <?php }else { echo 'You are not authorized to view'; } ?> 
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>