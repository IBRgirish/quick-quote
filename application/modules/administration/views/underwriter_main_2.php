<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/header');
}

?>

<br>
<h1>Admin Users</h1>
<div class="table"> <img src="<?php echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7"> <img src="<?php echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">
  <table class="listing" cellpadding="0" cellspacing="0" id="showrecord">
    <tbody>
      <tr>
        <th class="first" width="177">Name</th>
        <th>Email</th>
        <th>Status</th>
        <th>Modify</th>
        <th class="last">Delete</th>
      </tr>
      <?php 
if($total){
	foreach($admins as $row){
	
	?>
      <tr>
        <td class="first style1"><?php echo $row->name; ?></td>
        <td><?php echo $row->email;?></td>
        <td><?php echo ($row->status=='1'?'Active':'Disable');?></td>
        <td><a href="<?php echo base_url('administration/admins/edit/'.$row->id);?>"><img src="<?php echo base_url();?>img/admin/edit-icon.gif" alt="" width="16" height="16"></a></td>
        <td><a href="#" onclick=" delete_case('<?php echo base_url('administration/admins/delete/'.$row->id);?>', 'Are you sure?', 'Information deleted'); return false;"><img src="<?php echo base_url();?>img/admin/hr.gif" alt="" width="16" height="16"></a></td>
      </tr>
      <?php }
}else{ ?>
      <tr>
        <td class="first style1">&nbsp;</td>
        <td colspan="5">Oops, no data found!</td>
      </tr>
      <?php }?>
    </tbody>
  </table>
</div>
<div class="pagination">
  <?php $this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
