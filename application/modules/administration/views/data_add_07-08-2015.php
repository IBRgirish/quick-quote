<?php $this->load->view('includes/head_style_script'); ?>
<link rel="stylesheet" href="<?= base_url(); ?>css/colpick.css" type="text/css" />

<script type="text/javascript" src="<?= base_url(); ?>js/colpick.js"></script>

<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
	
}


</script>
   


	
<?php 
  $field_name=!empty($datas)?!empty($datas[0]->field_name)?$datas[0]->field_name:'':'';
  $default_value=!empty($datas)?!empty($datas[0]->default_value)?$datas[0]->default_value:'':'';
  $mandatory=!empty($datas)?!empty($datas[0]->mandatory)?$datas[0]->mandatory:'':'';
  $tooltip=!empty($datas)?!empty($datas[0]->title)?$datas[0]->title:'':'';
  $status=!empty($datas)?!empty($datas[0]->status)?$datas[0]->status:'':'';
  $id=!empty($datas)?!empty($datas[0]->id)?$datas[0]->id:'':'';
  $min_value=!empty($datas)?!empty($datas[0]->min_value)?$datas[0]->min_value:'':'';
  $max_value=!empty($datas)?!empty($datas[0]->max_value)?$datas[0]->max_value:'':'';
  $placeholder=!empty($datas)?!empty($datas[0]->placeholder)?$datas[0]->placeholder:'':'';  
  $show=!empty($datas)?!empty($datas[0]->show)?'checked':'':'checked';
  $disable=!empty($datas)?!empty($datas[0]->disable)?'checked':'':'';
  $app=!empty($datas)?!empty($datas[0]->app)?$datas[0]->app:1:1;
  $coverage=!empty($datas)?!empty($datas[0]->coverage)?$datas[0]->coverage:'':'';
  $color=!empty($datas)?!empty($datas[0]->color)?$datas[0]->color:'':'';
?>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="container" style="width:100%!important">
  <div class="content">
    <!-- Underwriter List -->
    <div class="span3">
      <h2><?php echo $header['title'] ?></h2>
	
	   <form action="<?php echo base_url(); ?>administration/data_dictionary/data_add/<?= $id;?>" method="post" name="frmCatlog" id="frmCatlog" autocomplete="off">
        
		<label>Field Name </label>       
	    <input type="text" name="field_name" value="<?= $field_name?>">

        
		<label>Default Value </label>
        <input type="text" name="default_value" id="default_value"  value="<?= $default_value?>">
		
		<label>Required </label>
        <select name="mandatory" style="width:65px"><option <?php if($mandatory=='Yes') {echo 'selected';}?> value='Yes'>Yes</option><option <?php if($mandatory=='No') {echo 'selected';}?> value='No'>No</option></select>
		
		<label>Tooltip</label>
        <input type="text" name="tooltip" value="<?= $tooltip?>" >		
	     
        <label>Min Value</label>
        <input type="text" name="min_value" value="<?= $min_value?>" >	
        
        <label>Max Value</label>
        <input type="text" name="max_value" value="<?= $max_value?>" >
        
        <label>Placeholder</label>
        <input type="text" name="placeholder" value="<?= $placeholder?>" >
        	
        <label for="show">Show</label>
        <input class="show" type="checkbox" name="show" value='1' <?= $show ?> />	
        
        <label for="show">Disable</label>
        <input class="show" type="checkbox" name="disable" value='1' <?= $disable ?> />	
        
        <label>Colour</label>        
        <input type="text" id="picker" name="color" value="<?= $color?>" style="border-right: 20px solid <?= '#'.$color?>;margin-bottom: 5px;"/>
        
        <label for="show">Application</label>
        <select name="application">
        <option value='1' <?php if($app==1){echo 'selected';}?>>Quick Quote</option>
        <option value='2' <?php if($app==2){echo 'selected';}?>>Application</option>
        </select>
        
        <label for="show">Coverages</label>
        
        <?php  echo getMakeModelVehicles('coverage', 'class="no_margin chzn-select" id="coverages_req"',$coverage,51,'');?> 
 
		
		<?php if(!empty($datas)){ ?>
        <label>Status</label>
        <input type="radio" name="status" id="status" <?php if($status==1) {echo 'checked';}?> value="1" >Active
        <input type="radio" name="status" id="status" <?php if($status==0) {echo 'checked';}?>  value="0" >In Active
		
         <?php }else{ ?>
         <?php } ?>
		<input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <?php if(!empty($datas)){ ?>
        <input type="submit" value="Edit" name="submit" class="btn btn-primary pull-right" style="margin-right:10px">
        <?php }else{ ?>
        <input type="submit" value="Add" name="submit" class="btn btn-primary pull-right" style="margin-right:10px">
        <?php } ?>
        <div class="clearfix"></div>
      </form>
	
    </div>
  </div>
</div>
</body>

<style>
#picker {
	margin:0;
	width:70px;
	height:20px;
	border-right:20px solid green;
	line-height:20px;
}
</style>
<script>
$(document).ready(function(e) {

$('#picker').colpick({
	layout:'hex',
	submit:0,
	colorScheme:'dark',
	onChange:function(hsb,hex,rgb,el,bySetColor) {
		$(el).css('border-color','#'+hex);
		// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
		if(!bySetColor) $(el).val(hex);
	}
}).keyup(function(){
	$(this).colpickSetColor(this.value);
});
});
</script>