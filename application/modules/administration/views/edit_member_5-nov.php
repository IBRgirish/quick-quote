<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');
	$flash_success = '';
if ($this->session->flashdata('error')){    
		//echo '<div class="error"><p>'.$this->session->flashdata('error').'</p></div>';
		$flash_error = $this->session->flashdata('error');
	}
	if ($this->session->flashdata('success')){    
		$flash_success = '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; 
	}
	// Validation Errors
	  $error_message = (validation_errors()?'<div class="error">'.validation_errors().'</div>':'');
	  
    if(!empty ($error_message)) {
		$error_message_div = '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$error_message.'</div>';
	} else {
		$error_message_div = '';
	}	
	
    if(!empty ($flash_error)) {
		$error_message_div .= '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$flash_error.'</div>';
	} 
}
	
	$id	=isset($member->id)?$member->id :$this->session->userdata('member_id');
	$email = isset($_POST['email']) ? $_POST['email']  :(isset($member->email)?$member->email: NULL);
	$password = isset($_POST['password']) ? $_POST['password'] :(isset($member->password)?$member->password : NULL);
	$first_name = isset($_POST['txtFname']) ? $_POST['txtFname'] : (isset($member->first_name)?$member->first_name : NULL);
	//$middle_name = isset($_POST['txtMname']) ? $_POST['txtMname'] : (isset($member->middle_name)?$member->middle_name : NULL);
	//$last_name = isset($_POST['txtLname']) ? $_POST['txtLname']  :(isset($member->last_name)?$member->last_name : NULL);	
	$address = isset($_POST['txaAddress']) ? $_POST['txaAddress'] :(isset($member->address)?$member->address : NULL);
	$country_id= isset($_POST['country']) ? $_POST['country'] :( isset($member->country)?$member->country : NULL);
	$state= isset($_POST['state']) ? $_POST['state']:( isset($member->state)?$member->state : NULL);
	$city = isset($_POST['city']) ? $_POST['city'] :( isset($member->city)?$member->city : NULL);
	$zip_code = isset($_POST['zip_code']) ? $_POST['zip_code']  :( isset($member->zip_code)?$member->zip_code : NULL); 
	$zip_code_extd = isset($_POST['txtZipExtd']) ? $_POST['txtZipExtd']  :( isset($member->zip_code_extd)?$member->zip_code_extd : NULL);
	$phone_area  = isset($_POST['txtPhArea']) ? $_POST['txtPhArea'] :(isset($member->phone_area) ? $member->phone_area : NULL);
	/*$phone = isset($_POST['phone']) ? $_POST['phone']  :( isset($member->phone_number)?$member->phone_number: NULL);*/
	$extension = isset($_POST['txtExtension']) ? $_POST['txtExtension']  :( isset($member->phone_ext)?$member->phone_ext: NULL);
	$cell_area  = isset($_POST['txtMoArea']) ? $_POST['txtMoArea'] :(isset($member->cell_area) ? $member->cell_area : NULL);
	/*$cell_number = isset($_POST['txtCellNumber']) ? $_POST['txtCellNumber']  :( isset($member->cell_number)?$member->cell_number: NULL);*/
	$fax_area  = isset($_POST['txtFaxArea']) ? $_POST['txtFaxArea'] :(isset($member->fax_area) ? $member->fax_area : NULL);
	/*$fax_number = isset($_POST['txtFaxNumber']) ? $_POST['txtFaxNumber']  :( isset($member->fax_number)?$member->fax_number: NULL);*/
	$sec_email = isset($_POST['txtSecEmail']) ? $_POST['txtSecEmail']  :( isset($member->sec_email)?$member->sec_email: NULL);
	$website = isset($_POST['txtWebsite']) ? $_POST['txtSecEmail']  :( isset($member->website)?$member->website: NULL);
	
	
	if(isset($_POST['txtPhone1'])&&isset($_POST['txtPhone2'])&&isset($_POST['txtPhone3'])&&isset($_POST['txtPhone4']))
	{
		$phone = $_POST['txtPhone1'].'-'.$_POST['txtPhone2'].'-'.$_POST['txtPhone3'].'-'.$_POST['txtPhone4'];
	}
	else if(isset($member->phone_number))
	{
		$phone = $member->phone_number;
	}
	else
	{
		$phone = NULL;
	}
	
	if(isset($_POST['txtCell1'])&&isset($_POST['txtCell2'])&&isset($_POST['txtCell3']))
	{
		$cell_number = $_POST['txtCell1'].'-'.$_POST['txtCell2'].'-'.$_POST['txtCell3'];
	}
	else if(isset($member->cell_number))
	{
		$cell_number = $member->cell_number;
	}
	else
	{
		$cell_number = NULL;
	}
	
	if(isset($_POST['txtFax1'])&&isset($_POST['txtFax2'])&&isset($_POST['txtFax3']))
	{
		$fax_number = $_POST['txtFax1'].'-'.$_POST['txtFax3'].'-'.$_POST['txtFax3'];
	}
	else if(isset($member->fax_number))
	{
		$fax_number = $member->fax_number;
	}
	else
	{
		$fax_number = NULL;
	}
	
	
	$fax_number = isset($_POST['txtFaxNumber']) ? $_POST['txtFaxNumber']  :( isset($member->fax_number)?$member->fax_number: NULL);
?>
<script type="text/javascript">
function select_uw(id) 
{
	$('#chkUnderWriter_'+id).prop('checked', true);
} 

function check_pri(id)
{
	if ($("#optUnderWriter_"+id).prop("checked") == true) {
		$('#chkUnderWriter_'+id).prop('checked', true);
	}
}
</script>
<div class="content">
  <!--div class="page-header">
    <h1>Edit Profile</h1>
  </div-->
  <div class="row">
  <div class="span10 offset1">
  <div class="well">
  <legend>Edit Agency</legend>
  <?php echo $error_message_div ?>
  <form action="" method="post" id="manageaccount_add_edit" name="manageaccount_add_edit" >
    <div>
      <div class="account form-wrapper" id="edit-account">
        <div id="account-login-container">
          <div id="edit-account-login" class="form-wrapper">
            <div class="row-fluid">
            	<div class="span3">
                    <label for="email">E-mail </label>
                    <input class="form-text span12" size="60" maxlength="128" type="text" name="email" id="email" value="<?php echo $email ?>" disabled />
            	</div>
                 <!-- Email * -->
                <div class="span3">
                    <label for="password">Password </label>
               		<input class="form-text span12" size="60" maxlength="128" type="text" name="password" id="password" value="<?php echo $password; ?>"/>
                </div>
            	<!-- Password * -->
                <div class="span3">
                    <label for="cpassword">Re-Password</label>
                    <input  class="form-text span12" size="60" maxlength="128"  type="text" name="cpassword" id="cpassword" value="<?php echo $password; ?>"/>
                </div>
                <!-- Confirm Password * -->
                <div class="span3">
                  <label for="txtFname">Agency Name <span class="form-required" title="This field is required.">*</span></label>
                  <input name="txtFname" type="text" class="form-text span12" size="60" maxlength="128" value="<?php echo $first_name ?>" >
                </div>
                <!-- First Name * -->
            </div>  
          </div>
          <!--<div class="">
            <label for="txtLname">Middle Name </label>
            <input name="txtMname" class="form-text" type="text" size="60" maxlength="128" value="< ?php echo $middle_name ?>" >
          </div>-->
          <!-- Middle Name -->
          <!--<div class="">
            <label for="txtLname">Last Name <span class="form-required" title="This field is required.">*</span></label>
            <input name="txtLname" class="form-text" type="text" size="60" maxlength="128" value="< ?php echo $last_name ?>" >
          </div>-->
          <!-- Last Name * -->
          <div class="row-fluid">
              <div class="span12">
                <label for="txtcompany">Address <span class="form-required" title="This field is required.">*</span></label>
                <textarea  class="form-textarea span12" name="txaAddress" id="txaAddress"><?php echo $address ?></textarea>
              </div>
          </div>
          <div class="row-fluid">
              <!-- Address * -->
              <div class="span3">
                <label for="txtCountry">Country <span class="form-required" title="This field is required.">*</span></label>
                <?php 
      $country_table = $this->config->item('country_table');
                $country=form_simple_dropdown_from_db_valuebyname('txtCountry', "SELECT id,country_name FROM ".$country_table." where status=1", $country_id, 'Choose', 'class="select_input span12"');
                echo $country;
     ?>
              </div>
              <!-- Country * -->
              <div class="span2">
                <label for="txtState">state <span class="form-required" title="This field is required.">*</span></label>
                <input name="txtState"  id="txtState" class="form-text span12" type="text" size="30" maxlength="128" value="<?php echo $state ?>"  >
              </div>
              <!-- State * -->
              <div class="span3">
                <label for="txtCity">City <span class="form-required" title="This field is required.">*</span></label>
                <input name="txtCity"  id="txtCity" class="form-text span12" type="text" size="30" maxlength="128"  value="<?php echo $city ?>" >
              </div>
              <!-- City * -->
               <div class="span2">
                <label for="txtZip">Zip Code <span class="form-required" title="This field is required.">*</span></label>
                <input name="txtZip"  id="txtZip" maxlength="5" class="form-text span12" type="text" size="10" value="<?php echo $zip_code ?>" >
              </div>
              <!-- Zip Code * -->
               <div class="span2">
                <label for="txtZipExtd">Zip Code Extended <span class="form-required" title="This field is required.">*</span></label>
                <input name="txtZipExtd"  id="txtZipExtd" maxlength="5" class="form-text span12" type="text" size="10" value="<?php echo $zip_code_extd ?>" />
              </div>
              <!-- Zip Code Extended -->
              
          </div>
          <div class="row-fluid">
             
              <div class="span4">
                <label for="txtPhone">Phone Number <span class="form-required" title="This field is required.">*</span></label>
               <!-- <input name="txtPhone" class="form-text" type="text" size="30" maxlength="128" value="<?php //echo $phone ?>"/>-->
               <?php $phone_no = explode('-', $phone);?>
                 <!--input type="text" value="<?php echo $phone_area; ?>" id="txtPhArea" name="txtPhArea" class="validate[required] text-input form-text span3" maxlength="5"  /-->
                              <input type="text" value="<?php echo isset($phone_no[0]) ? $phone_no[0] : ''; ?>" id="txtPhone1" name="txtPhone1" class="validate[required] text-input form-text span3" maxlength="3" />
                              <input type="text" value="<?php echo isset($phone_no[1]) ? $phone_no[1] : ''; ?>" id="txtPhone2" name="txtPhone2" class="validate[required] text-input form-text span3" maxlength="3"  />
                              <input type="text" value="<?php echo isset($phone_no[2]) ? $phone_no[2] : ''; ?>" id="txtPhone3" name="txtPhone3" class="validate[required] text-input form-text span3" maxlength="4"  />							                                <input type="text" value="<?php echo isset($phone_no[3]) ? $phone_no[3] : ''; ?>" id="txtPhone4" name="txtPhone4" class=" text-input form-text span3" maxlength="4"  />
              </div>
              <!-- Phone Number * -->
            <!--  <div class="span3">
                <label for="txtExtension">Extension </label>
                <input name="txtExtension" class="form-text span12" id="txtExtension" type="text" size="30" maxlength="6" value="<?php echo $extension ?>" >
              </div>-->
              <!-- Phone Extension -->
              <div class="span4">
                <label for="txtCellNumber">Cell Number </label>
               <!-- <input name="txtCellNumber" class="form-text" id="txtCellNumber" type="text" size="30" maxlength="128" value="<?php echo $cell_number ?>" >-->
               <?php $cell_number_no = explode('-', $cell_number);?>
                  <input type="text" value="<?php echo $cell_area; ?>" id="txtMoArea" name="txtMoArea" style="display:none;"  class="validate[required] text-input form-text span3" maxlength="5"  />
                              <input type="text" value="<?php echo isset($cell_number_no[0]) ? $cell_number_no[0] : ''; ?>" id="txtCell1" name="txtCell1" class="validate[required] text-input form-text span4" maxlength="3" />
                              <input type="text" value="<?php echo isset($cell_number_no[1]) ? $cell_number_no[1] : ''; ?>" id="txtCell2" name="txtCell2" class="validate[required] text-input form-text span4" maxlength="3"  />
                              <input type="text" value="<?php echo isset($cell_number_no[2]) ? $cell_number_no[2] : ''; ?>" id="txtCell3" name="txtCell3" class="validate[required] text-input form-text span4" maxlength="4"  />
              </div>
              <!-- Cell Number -->
               <div class="span4">
                <label for="txtFaxNumber">Fax Number </label>
              <!--  <input name="txtFaxNumber" class="form-text" id="txtFaxNumber" type="text" size="30" maxlength="128" value="<?php echo $fax_number ?>" >-->
               <?php $fax_number_no = explode('-', $fax_number);?>
                  <input type="text" value="<?php echo $fax_area; ?>" id="txtFaxArea" name="txtFaxArea" style="display:none;" class="validate[required] text-input form-text span3" maxlength="5"  />
                              <input type="text" value="<?php echo isset($fax_number_no[0]) ? $fax_number_no[0] : ''; ?>" id="txtFax1" name="txtFax1" class="validate[required] text-input form-text span4" maxlength="3" />
                              <input type="text" value="<?php echo isset($fax_number_no[1]) ? $fax_number_no[1] : '';?>" id="txtFax2" name="txtFax2" class="validate[required] text-input form-text span4" maxlength="3"/>
                              <input type="text" value="<?php echo isset($fax_number_no[2]) ? $fax_number_no[2] : '';?>" id="txtFax3" name="txtFax3" class="validate[required] text-input form-text span4" maxlength="4"  />
              </div>
              <!-- Fax Number -->
              
          </div>
          <div class="row-fluid">
              
               <div class="span3">
                <label for="txtWebsite">Website </label>
                <input name="txtWebsite" class="form-text span12" id="txtWebsite" type="text" size="30" maxlength="128" value="<?php echo $website ?>" >
              </div>
              <!-- Website -->
              <div class="span3">
                <label for="txtSecEmail">Secondary Email </label>
                <input name="txtSecEmail" class="form-text span12" id="txtSecEmail" type="text" size="30" maxlength="128" value="<?php echo $sec_email ?>" >
              </div>
              <!-- Secondary Email -->
            
              <?php $status = $member->status; if(empty($status)) $status = '';  ?>
              <div class="span3">
                <label for="txtstatus">Status </label>
                <select id="txtstatus" name="txtstatus" class="span12" >
                    <option <?php echo $status=='ACTIVE' ? 'selected' : ''; ?> value="ACTIVE" >Active</option>
                    <option <?php echo $status=='REQUESTED' ? 'selected' : ''; ?> value="REQUESTED" >Requested</option>
                    <option <?php echo $status=='REJECTED' ? 'selected' : ''; ?> value="REJECTED" >Rejected</option>
                    <option <?php echo $status=='BLOCKED' ? 'selected' : ''; ?> value="BLOCKED" >Blocked</option>
                </select>
              </div>
          </div>
          <div class="row-fluid">
              
		  </div>
          <div>
                <label for="chkUnderWriter">Underwriter Assigned <span class="form-required" title="This field is required.">*</span></label>
                <?php
				if ($underWriter['flag']==1)
				{
					echo '<table border="0" width="100%">';
					echo '<tr>';
					echo '<th width="10%" align="left">Underwriter</th>';
					echo '<th width="20%" align="left">Primary</th>';
					echo '</tr>';
					$count=0;
					echo '<tr><td colspan="2">';
					echo '<div style="height:170px; overflow-y:scroll; overflow-x:hidden" >';
					echo '<table width="100%">';
					foreach($underWriter['resultData'] as $val)
					{
						$count++;
						
						$primary = array();
						$uwData[] = array();
						if(!empty($member->underwriter)){
							list($primary, $underwriter) = explode(':',$member->underwriter);
							if(!empty($underwriter)){
								$uwData = explode(',',$underwriter);
							}
						} 
						
						$chk = '';
						if(in_array($val['id'],$uwData)) {
							$chk='checked';
						}
						
						$prim = '';
						if ($val['id'] == $primary) {
							$prim = 'checked="checked"';
						}
						
						echo '<tr>';
						echo '<td><input type="checkbox" id="chkUnderWriter_'.$count.'" name="chkchkUnderWriter[]" value="'.$val['id'].'" '.$chk.' onChange="check_pri('.$count.')" />
						&nbsp;<label for="chkUnderWriter_'.$count.'" style="display:inline">'.$val['name'].'</label></td>';
						echo '<td><input type="radio" id="optUnderWriter_'.$count.'" name="optUnderWriter" value="'.$val['id'].'" '.$prim.' onChange="select_uw('.$count.');" /></td>';
						echo '</tr/>';
					}
					echo '</table>';
					echo '</div>';
					echo '</td></tr>';
					echo '</table>';
				}
                ?>
              </div>
         
          <input type="hidden" name="members_id" id="members_id" value="<?php echo $id?>" >
          <input type="hidden" name="subscription_id" id="subscription_id" value="1" >
          <input type="hidden" name="email" id="email" value="<?php echo $email?>"/>
          <input type="hidden" name="savepassword" id="savepassword" value="<?php echo $password?>" >
        </div>
      </div>
      <p>&nbsp;</p>
    </div>
    </div>
    <button class="btn btn-primary"  id="edit_memberinfo" name="edit_memberinfo" type="submit">Update</button>
    </div>
    </div>
  </form>
</div>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>
