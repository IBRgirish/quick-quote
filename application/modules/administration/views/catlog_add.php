<?php $this->load->view('includes/head_style_script'); ?>
<script type="text/javascript">
function close_pop_up()
{
	parent.$.fancybox.close();
}
</script>
<?php if(isset($_GET['cat_type']) && ($_GET['cat_type'] > 0) ) { $cat_type_v = $_GET['cat_type']; } else {  $cat_type_v = !empty($cat_type)?$cat_type:0; } ?>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="container" style="width:100%!important">
  <div class="content">
    <!-- Underwriter List -->
    <div class="span3">
      <h2><?php echo $header['title'] ?></h2>
	  <?php if($cat_type_v == '21') {
		?>
		 <form action="<?php echo base_url(); ?>/administration/catlog/catlog_add1" method="post" name="frmCatlog" id="frmCatlog" autocomplete="off">
        
		<label>Name <em style="color:#FF0000">*</em></label>
         <?php
    	echo get_active_catlog_dropdown($selected=$cat_type_v,$attr='disabled="disabled"','cat_f_n',$id='cat_f_n');
	?>
	<input type="hidden" name="cat_f" value="<?php echo $cat_type_v; ?>">
       <!-- <select name="catlog_type" id="catlog_type" class="span3">
			<option value="nature_of_business">Nature of Business</option>
			<option value="trailer_type">Trailer Type</option>
			<option value="commodities_haulted">Commodities Haulted</option>
			<option value="trac_truck_model">Truck & Tractor Models</option>
            <option value="number_of_axles">Number of Axles</option>
           <!--option value="trac_truck_model">Tractor Models</option
           <option value="trailer_model">Trailer Models</option>
		</select>-->
		<?php echo form_error('insured_state','<span style="color:red"">','</span>'); ?>
        
		<label>Catlog Name <em style="color:#FF0000">*</em></label>
        <input type="text" name="catlog_name" id="catlog_value" class="span3">
		
		<label>State <em style="color:#FF0000">*</em></label>
        <div class="textinput span12 ">
			<?php
				$attr = "class='span12 ui-state-valid state'";
				$broker_state = 'CA';
			?>
			<?php echo get_state_dropdown('state', $broker_state, $attr); ?>
		</div>
		
		<label>SLATax%<em style="color:#FF0000">*</em></label>
        <input type="text" name="slatax" id="slatax" class="span3 numr_valid">
		
		<label>Unique Cargo Policy Fee <em style="color:#FF0000">*</em></label>
        <input type="text" name="unique_cargo_policy_fee" id="unique_cargo_policy_fee" class="span3 numr_valid">
		
		<label>Unique PD Policy Fee <em style="color:#FF0000">*</em></label>
        <input type="text" name="unique_pd_policy_fee" id="unique_pd_policy_fee" class="span3 numr_valid">
		
		<label>PD Cargo Policy Fee <em style="color:#FF0000">*</em></label>
        <input type="text" name="pd_cargo_policy_fee" id="pd_cargo_policy_fee" class="span3 numr_valid">
		
		<label>Association Fee <em style="color:#FF0000">*</em></label>
        <input type="text" name="association_fee" id="association_fee" class="span3 numr_valid">
		
		
		<?php echo form_error('catlog_value','<span style="color:red"">','</span>'); ?>
		
		<input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Add" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
		<?php
	  } else if($cat_type_v == '26') { ?>
      
     <?php echo isset($required)? '<em style="color:#FF0000">*'.$required.'</em>':''?>
      <form action="<?php echo base_url('administration/catlog/catlog_adding');?>" method="post" name="frmCatlog" id="frmCatlog" autocomplete="off">
        <input type="hidden" name="status" value="1">
		<label>Name </label>
         <?php
    	echo get_active_catlog_dropdown($selected=$cat_type_v,$attr='disabled="disabled"','cat_f_n',$id='cat_f_n'); 
	?>
	<input type="hidden" name="cat_f" value="<?php echo $cat_type_v; ?>">

		<?php echo form_error('insured_state','<span style="color:red"">','</span>'); ?>
        <label>Catlog Name </label>
         <?php
		 $select=!empty($carrier_state)?$carrier_state:$cat_type_v;
		 
    	echo get_active_carriers_dropdown($selected=$select,$attr='style="width:300px"','carrier_id',$id='carrier_id'); 
	?>

		
		<label>State </label>
        <div class="textinput span12 ">
		
			<?php echo get_state_carriers_dropdown($selected=$cat_type_v,$attr='style="width: 15%;"','carrier_state',$id='carrier_state'); ?>
		</div>
		<label>Catlog Value <em style="color:#FF0000">*</em></label>
        <input type="text" name="catlog_value" id="catlog_value"   class="span1">
        
       

        
        <label>Mandatory</label>
        <input type="radio" name="mandatory_value" id="mandatory_value"  value="1" >Yes
        <input type="radio" name="mandatory_value" id="mandatory_value"  value="0" >No
        
		<?php echo form_error('catlog_value','<span style="color:red"">','</span>'); ?>
		
		<input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Add" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
	  <?php }  else if($cat_type_v == '27') { ?>
       <?php echo isset($required)? '<em style="color:#FF0000">*'.$required.'</em>':''?>
       <form action="<?php echo base_url('administration/catlog/catlog_adding');?>" method="post" name="frmCatlog" id="frmCatlog" autocomplete="off">
        <input type="hidden" name="status" value="1">
		<label>Name </label>
         <?php
    	echo get_active_catlog_dropdown($selected=$cat_type_v,$attr='disabled="disabled"','cat_f_n',$id='cat_f_n'); 
	?>
	<input type="hidden" name="cat_f" value="<?php echo $cat_type_v; ?>">
  
		<?php echo form_error('insured_state','<span style="color:red"">','</span>'); ?>
        <label>Catlog Name </label>
         <?php
		 $select=!empty($carrier_state)?$carrier_state:$cat_type_v;
		 
    	echo get_active_carriers_dropdown($selected=$select,$attr='style="width:300px"','carrier_id',$id='carrier_id'); 
	?>

		
		<label>State </label>
        <div class="textinput span12 ">
		
			<?php echo get_state_carriers_dropdown($selected=$cat_type_v,$attr='style="width: 15%;"','carrier_state',$id='carrier_state'); ?>
		</div>
		<label>Catlog Value <em style="color:#FF0000">*</em></label>
        <input type="text" name="catlog_value" id="catlog_value"   class="span1">
    
        <label>Mandatory</label>
        <input type="radio" name="mandatory_value" id="mandatory_value"  value="1" >Yes
        <input type="radio" name="mandatory_value" id="mandatory_value"  value="0" >No
        
		<?php echo form_error('catlog_value','<span style="color:red"">','</span>'); ?>
		
		<input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Add" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
	  <?php }else if($cat_type_v == '28') { ?>
       <?php echo isset($required)? '<em style="color:#FF0000">*'.$required.'</em>':''?>
       <form action="<?php echo base_url('administration/catlog/catlog_adding');?>" method="post" name="frmCatlog" id="frmCatlog" autocomplete="off">
        <input type="hidden" name="status" value="1">
		<label>Name </label>
         <?php
    	echo get_active_catlog_dropdown($selected=$cat_type_v,$attr='disabled="disabled"','cat_f_n',$id='cat_f_n'); 
	?>
	<input type="hidden" name="cat_f" value="<?php echo $cat_type_v; ?>">
  
		<?php echo form_error('insured_state','<span style="color:red"">','</span>'); ?>
        <label>Catlog Name </label>
         <?php
		 $select=!empty($carrier_state)?$carrier_state:$cat_type_v;
		 
    	echo get_active_carriers_dropdown($selected=$select,$attr='style="width:300px"','carrier_id',$id='carrier_id'); 
	?>

		
		<label>State </label>
        <div class="textinput span12 ">
		
			<?php echo get_state_carriers_dropdown($selected=$cat_type_v,$attr='style="width: 15%;"','carrier_state',$id='carrier_state'); ?>
		</div>
		<label>Catlog Value <em style="color:#FF0000">*</em></label>
        <input type="text" name="catlog_value" id="catlog_value"   class="span1">
    
        <label>Mandatory</label>
        <input type="radio" name="mandatory_value" id="mandatory_value"  value="1" >Yes
        <input type="radio" name="mandatory_value" id="mandatory_value"  value="0" >No
        
		<?php echo form_error('catlog_value','<span style="color:red"">','</span>'); ?>
		
		<input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Add" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
	  <?php } else if($cat_type_v == '29') { ?>
       <?php echo isset($required)? '<em style="color:#FF0000">*'.$required.'</em>':''?>
       <form action="<?php echo base_url('administration/catlog/catlog_slatax_adding');?>" method="post" name="frmCatlog" id="frmCatlog" autocomplete="off">
        <input type="hidden" name="status" value="1">
		<label>Name </label>
         <?php
    	echo get_active_catlog_dropdown($selected=$cat_type_v,$attr='disabled="disabled"','cat_f_n',$id='cat_f_n'); 
	?>
	<input type="hidden" name="cat_f" value="<?php echo $cat_type_v; ?>">
  
		<?php echo form_error('insured_state','<span style="color:red"">','</span>'); ?>
        <label>Catlog Name </label>
         <?php
		 $select=!empty($carrier_state)?$carrier_state:$cat_type_v;
		 
    	echo get_active_carriers_dropdown($selected=$select,$attr='style="width:300px"','carrier_id',$id='carrier_id'); 
	?>

		
		<label>State </label>
        <div class="textinput span12 ">
		
			<?php echo get_state_carriers_dropdown($selected=$cat_type_v,$attr='style="width: 15%;"','carrier_state',$id='carrier_state'); ?>
		</div>
		<label>Catlog Value <em style="color:#FF0000">*</em></label>
        <input type="text" name="catlog_value" id="catlog_value"   class="span1">
               
		<?php echo form_error('catlog_value','<span style="color:red"">','</span>'); ?>
		
		<input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Add" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
	  <?php } else{  ?>   
        
       <form action="<?php echo base_url('administration/catlog/catlog_adds');?>" method="post" name="frmCatlog" id="frmCatlog" enctype="multipart/form-data" autocomplete="off">
        
		<label>Name <em style="color:#FF0000">*</em></label>
         <?php
    	echo get_active_catlog_dropdown($selected=$cat_type_v,$attr='disabled="disabled"','cat_f_n',$id='cat_f_n'); 
	?>
	<input type="hidden" name="cat_f" value="<?php echo $cat_type_v; ?>">


		<?php echo form_error('insured_state','<span style="color:red"">','</span>'); ?>
        <?php if($_GET['cat_type']==44){ 
		 echo '<label>Catlog Value <em style="color:#FF0000">*</em></label>';
		 echo getMakeModelVehicles('catlog_value', 'class="span2 span_25"','',30,'');
		}else if($_GET['cat_type']==45){ 
		 echo '<label>Catlog Value <em style="color:#FF0000">*</em></label>';
		 echo getMakeModelVehicles('catlog_value', 'class="span2 span_25"','',32,'');
		}else if($_GET['cat_type']==46){ 
		 echo '<label>Catlog Value <em style="color:#FF0000">*</em></label>';
		 echo getMakeModelVehicles('catlog_value', 'class="span2 span_25"','',18,'');
		}else{
		?>
		
		
       
        <?php }
		if($_GET['cat_type']==1){
		echo '<label>Catlog Value <em style="color:#FF0000">*</em></label>
        <input type="text" name="catlog_value" id="catlog_value" class="span3">
		<label>Reefer<em style="color:#FF0000">*</em>&nbsp;&nbsp;<input type="checkbox" name="catlog_value[]" id="catlog_value" value="Reefer"></label>';
             	 
			}else{ ?>
		<label>Catlog Value <em style="color:#FF0000">*</em></label>
        <input type="text" name="catlog_value" id="catlog_value" class="span3">		
	<?php }
		if($_GET['cat_type']==39)
		{
		?>
        <label>Catlog label Value <em style="color:#FF0000">*</em></label>
        <input type="text" name="catlog_value_2" id="catlog_value_2" class="span3">
        <?php 
		}
		?>
        <?php if($_GET['cat_type']==44 || $_GET['cat_type']==45 || $_GET['cat_type']==46 || $_GET['cat_type']==50)
		{
		?>
        <label>Catlog picture <em style="color:#FF0000">*</em></label>
        <input type="file" name="pic_name" />
        <?php 
		}
		?>
		<?php echo form_error('catlog_value','<span style="color:red"">','</span>'); ?>
		
		<input type="button" value="Cancel" class="btn btn-primary pull-right" onClick="close_pop_up();">
        <input type="submit" value="Add" class="btn btn-primary pull-right" style="margin-right:10px">
        <div class="clearfix"></div>
      </form>
	  <?php } ?>
    </div>
  </div>
</div>
</body>
