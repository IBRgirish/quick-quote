<?php $this->load->view('includes/head_style_script'); ?>
<body style="margin-top: 0px;" class="preview" id="top" data-spy="scroll" data-target=".subnav" data-offset="80">
<div class="container">
  <div class="content">
    <!-- Underwriter List -->
    <div class="span3">
      <h2><?php echo $header['title'] ?></h2>
      <form action="" method="post" name="frmUnderwriter" id="frmUnderwriter" autocomplete="off"> 
        <!--<label><span style="width:100px; text-align:right"><strong>Name:</strong></span> < ?php echo $member->first_name.'&nbsp;'.$member->last_name?></label>-->
		<label><span style="width:100px; text-align:right"><strong>Agency:</strong></span> <?php echo $member->first_name ?></label>
        <label><span style="width:100px; text-align:right"><strong>Email:</strong></span> <?php echo $member->email ?></label>
        <label><span style="width:100px; text-align:right"><strong>Address:</strong></span> <?php echo $member->address ?></label>
        <label><span style="width:100px; text-align:right"><strong>City:</strong></span> <?php echo $member->city ?></label>
        <label><span style="width:100px; text-align:right"><strong>State:</strong></span> <?php echo $member->state ?></label>
        <label><span style="width:100px; text-align:right"><strong>Country:</strong></span> <?php echo $member->country ?></label>
        <label><span style="width:100px; text-align:right"><strong>Zip Code:</strong></span> <?php echo $member->zip_code ?></label>
        <label><span style="width:100px; text-align:right"><strong>Contact Number:</strong></span><?php  echo $member->phone_area.'-'.$member->phone_number; ?></label>
		<label><span style="width:100px; text-align:right"><strong>Cell Number:</strong></span> <?php echo $member->cell_number; ?></label>
		<label><span style="width:100px; text-align:right"><strong>Fax Number:</strong></span> <?php echo $member->fax_number; ?></label>
		<label><span style="width:100px; text-align:right"><strong>Website:</strong></span> <?php echo $member->website ?></label>
		
		<label><span style="width:100px; text-align:right"><strong>Created Date:</strong></span> <?php echo date('m/d/Y H:i:s', strtotime($member->created_date)); ?></label>
		<label><span style="width:100px; text-align:right"><strong>Status:</strong></span> <?php echo ucfirst(strtolower($member->status)) ?></label>
        <div class="clearfix"></div>
      </form>
    </div>
    <!-- Underwriter List -->
  </div>
</div>
</body>
