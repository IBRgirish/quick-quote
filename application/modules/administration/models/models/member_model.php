<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Member_model extends CI_Model {

	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
	
	public function member_status_update($id,$status)
	{

		$member_table = $this->config->item('member_table');

		$data = array(
			'status' => $status,
		);
		
		$this->db->where('id', $id);
		if($this->db->update($member_table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * validate_admin function
	 *
	 * @access	public
	 * @return	array
	 */
	public function validate_admin()
	{
	  $this->db->where('email');
	  $this->db->where('password', md5($this->input->post('password')));
	  $this->db->where('status', 'Active');
	  $query = $this->db->get($this->config->item('member_table'));
	  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else 
	  {
			return FALSE;
	  }
	}




	


/**
 * count_admins function
 *
 * @access	public
 * @return	integer
 */
	public function count_members($str, $joined_in)
	{
		if(!empty($str)){
			$this->db->like('email', $str);
			$this->db->or_like('first_name', $str); 	
			$this->db->or_like('last_name', $str); 	
		}
		if(!empty($joined_in)){
			$this->db->where("create_date >= DATE_SUB(NOW(),INTERVAL $joined_in )", NULL, FALSE);	
		}
		return $this->db->count_all_results($this->config->item('member_table'));
	}
/**
 * list_admins function
 *
 * @access	public
 * @param	integer limit, integer offset
 * @return	array
 */
	public function list_members($limit, $offset,$str='',$joined_in, $sort_by, $sort_order)
	{
		$sort_order = ($sort_order == 'asc') ? 'asc' : 'desc';
		$sort_columns = array('email','create_date', 'modify_date');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'email';
		
		$this->db->limit($limit, $offset);
		$this->db->from($this->config->item('member_table'));
		$this->db->order_by($sort_by, $sort_order);
		if(!empty($str)){
			$this->db->like('email', $str);
			$this->db->or_like('first_name', $str); 	
			$this->db->or_like('last_name', $str); 	
		}
		if(!empty($joined_in)){
			$this->db->where("create_date >= DATE_SUB(NOW(),INTERVAL $joined_in )", NULL, FALSE);	
		}
		
		
		$query = $this->db->get();
		return $query->result();
	}	
	
	
	
	
	
	/**
	 * insert_update_members function
	 *
	 * @access	public
	 * @params	array data, integer id
	 * @return	boolean
	 */
	
	public function insert_update_members($data, $id=NULL)
	{
		if($id)
			return $this->_update($id, $data, $this->config->item('member_table'));
		else
			return $this->_insert($data, $this->config->item('member_table'));
	}
/**
 * get_members_info function
 *
 * @access	public
 * @param	integer id
 * @return	array
 */
	
	public function get_members_info($id, $code='')
	{
			$member_table = $this->config->item('member_table');
			
			$this->db->select("$member_table.*,$member_table.id as memberid");
			$this->db->where("$member_table.id ",$id);
			//$this->db->join('acms_orders', 'acms_orders.member_id = acms_members.id', 'left');
			
		if( $query = $this->db->get($this->config->item('member_table')) )
		{
			
			if ($query->num_rows() > 0)
			{
				return $query->row();
			}
		}
		return FALSE;
	}
	
	
	
/**
 * delete_members function
 *
 * @access	public
 * @param	integer id
 * @return	boolean
 */
	
	public function delete_members($id)
	{
		return $this->_delete($id, $this->config->item('member_table'));
	}
	
	public function active_underwriter_list()
	{
		$sql = 'SELECT * FROM '.$this->config->item('underwriter_table').' WHERE status=1 AND delete_status=0';
		$query = $this->db->query($sql);
		$count = $query->num_rows();
		if($count >= 1)
		{
			$data['flag'] = 1;
			$data['resultData'] = $query->result_array();
		}
		else
		{
			$data['flag'] = 0;
		}
		return $data;
	}
	

	
	
	
	
}