<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Page_model extends CI_Model {

	
	function Page_model() {
		parent::__construct();
		
		$this->gallery_path = realpath(APPPATH . '../uploads/pages');		
	}
	
	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{		
		$this->db->set($data);//echo $this->db->last_query(); 
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}
		
		return FALSE;
	}
		
	
/**
 * count_pages function
 *
 * @access	public
 * @return	integer
 */
	public function count_pages()
	{
		return $this->db->count_all_results($this->config->item('page_table'));
	}
/**
 * list_pages function
 *
 * @access	public
 * @param	integer limit, integer offset
 * @return	array
 */
	public function list_pages($limit, $offset,$sort_by, $sort_order)
	{	
		
		$sort_order = ($sort_order == 'asc') ? 'asc' : 'desc';
		$sort_columns = array('title', 'description', 'create_date', 'modify_date');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'modify_date';

		$this->db->limit($limit, $offset);		
		$this->db->from($this->config->item('page_table'));
		$this->db->order_by($sort_by, $sort_order);
		$query = $this->db->get(); 
		return $query->result();
	}
	/**
	 * insert_update_pages function
	 *
	 * @access	public
	 * @params	array data, integer id
	 * @return	boolean
	 */
	
	public function insert_update_pages($data, $id=NULL)
	{				
		if($id){
			$this->_update($id, $data, $this->config->item('page_table'));			
		}
		else{
			$this->_insert($data, $this->config->item('page_table'));
			return $lastinsertid =$this->db->insert_id();
		}

		
	}

/**
 * get_pages_info function
 *
 * @access	public
 * @param	integer id
 * @return	array
 */
	
	public function get_pages_info($id, $code='')
	{
			$this->db->where('id',$id);
			
		if( $query = $this->db->get($this->config->item('page_table')) )
		{			
			if ($query->num_rows() > 0)
			{
				return $query->row();
			}
		}
		return FALSE;
	}
/**
 * delete_pages function
 *
 * @access	public
 * @param	integer id
 * @return	boolean
 */
	
	public function delete_pages($id)
	{
		return $this->_delete($id, $this->config->item('page_table'));
	}	
		
	public function pages_status($id,$status)
	{				
		
		$status_val=($status==1)? 0 :1;

		$data = array(
               'status' => $status_val,               
            );
		$this->db->where('id', $id);
		$this->db->update($this->config->item('page_table'), $data); 
		
	}
	
	public function list_srch_pages($limit, $offset,$str='',$sort_by, $sort_order)
	{		
		$sort_order = ($sort_order == 'asc') ? 'asc' : 'desc';
		$sort_columns = array('title', 'description', 'create_date', 'modify_date');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'modify_date';
		
		$this->db->limit($limit, $offset);
		$this->db->from($this->config->item('page_table'));
		
		$this->db->order_by($sort_by, $sort_order);
		if(!empty($str)){
			$this->db->like('title', $str);
			$this->db->or_like('content', $str);
		}
		
		$query = $this->db->get(); 		
		return $query->result();
	}

	public function count_search($str){
		if(!empty($str)){
			$this->db->like('title', $str);
			$this->db->or_like('content', $str);
		}
		return $this->db->count_all_results($this->config->item('page_table'));
	}
	
	
	public function get_ads()
	{
		$query = $this->db->get_where('real_ads', array('status' => 'active'));
		return $query->result();
	}
	public function get_vdo()
	{
		$query = $this->db->get_where('real_video_table', array('vdo_status' => 'active'));
		return $query->result();
	}
	
}