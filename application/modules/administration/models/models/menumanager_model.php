<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Menumanager_model extends CI_Model {

	/**
	 * _delete function
	 *
	 * @access	protected
	 * @paramsinteger id, string table name
	 * @return	boolean
	 */
	function get_menu()
	{
		$this->db->select('*');
		$this->db->from('real_menumanager');
		$this->db->join('real_pages', 'real_pages.id = real_menumanager.page_id', 'left');
		$this->db->order_by('sort_order', 'asc'); 
		$query = $this->db->get();
		return $query->result();
	}
	function get_menu_val()
	{
		$query = $this->db->get_where('real_menumanager', array('menu_status'=> '1', 'menu_type'=>'0'));
		return $query->result();
	}
	 
	 function get_page()
	 {
	 	$query = $this->db->get_where('real_pages',array('status'=>'1'));
		return $query->result();
	 }
	 
	 function row_count($id)
	 {
		$this->db->where('menu_id', $id);
		$query = $this->db->get('real_menumanager');
		$rec['num_row'] = $query->num_rows();
		$rec['data'] = $query->row();
		//$rec['result'] = $query->result();
		return $rec;
		
	 }
	 function update_page($val)
	 {
	 	$value['menu_type'] = '0';
	 	$value['menu_status'] = '0';
	 	$value['page_id'] = '0';
		$this->db->where('menu_id', $val);
		$this->db->update('real_menumanager', $value);
		//$this->db->insert('real_menumanager', $data);
		return true;
	 }
	 
	 function insert_menu($data)
	 {
		$this->db->insert('real_menumanager', $data);
		return true;
	 } 
	 
	 // sub menu managment///
	 function get_submenu()
	 {
		$this->db->select('*');
		$this->db->from('real_submenu_managment');
		$this->db->join('real_menumanager', 'real_menumanager.menu_id = real_submenu_managment.mainmenu_id', 'left');
		$query = $this->db->get();
		return $query->result();
	 }
	 
	 function get_menu_for_select()
	 {
		$menus = $this->db->get('real_menumanager', array('status'=>'1'));
		return $menus->result(); 
	 }
	 
	 
	 function row_count_for_submenu($id)
	 {
		$this->db->where('page_id', $id);
		$query = $this->db->get('real_submenu_managment');
		$rec1['num_row'] = $query->num_rows();
		$rec1['data'] = $query->row();
		//$rec['result'] = $query->result();
		return $rec1;
		
	 }
	 function update_page_for_submit($val)
	 {
	 	$value['page_id'] = '0';
		$this->db->where('id', $val);
		$this->db->update('real_submenu_managment', $value);
		//$this->db->insert('real_menumanager', $data);
		return true;
	 }

	 
	 
	 
	 function insert_submenu($data)
	 {
		$this->db->insert('real_submenu_managment', $data);
		return true;
	}
	
	
	function get_menu_detail($id)
	{
		$res = $this->db->get_where('real_menumanager', array('menu_id'=>$id));
		return $res->row();
	}
	
	function update_menu($data, $id)
	{
		$this->db->where('menu_id', $id);
		return $this->db->update('real_menumanager', $data);
	}
	
	function delete_menu_mod($id)
	{
		$this->db->where('menu_id', $id);
		$this->db->delete('real_menumanager');
		return true;
	}
	
	function get_menudata($id)
	{
		$data = $this->db->get_where('real_menumanager', array('menu_type'=>$id));
		return $data->row();
	}
	
}