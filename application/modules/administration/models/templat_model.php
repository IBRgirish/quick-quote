<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Templat_model extends CI_Model {

	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	 function insert_logo($data)
	 {
			$this->db->insert('real_header_table', $data); 
			return true;
	 }
	 
	 function update_logo($data, $id)
	 {
		$this->db->where('id', $id);
		$this->db->update('real_header_table', $data); 
		return true;
	 }
	 
	 function update_status($tablename,$data)
	 {
	 	$val['status'] = $data;
		$this->db->update($tablename, $val); 
	 }
	 
	 function insert_footer($data)
	 {
			$this->db->insert('real_footer_table', $data); 
			return true;
	 }
	 
	 function insert_vdo($data)
	 {
			$this->db->insert('real_video_table', $data); 
			return true;
	 }
	 
	 function get_videos_info($id)
	 {
		$res = $this->db->get_where('real_video_table', array('id' => $id));
		return $res->row();
	 }
	 
	 function insert_ads($data)
	 {
			$this->db->insert('real_ads', $data); 
			return true;
	 }
	 
	 function get_head_logo()
	 {
	 	$res = $this->db->get('real_header_table');
		return $res->result();
	 }
	 
	 function get_footer()
	 {
	 	$res = $this->db->get('real_footer_table');
		return $res->result();
	 }
	 
	 function get_footer_info($id)
	 {
	 		$res = $this->db->get_where('real_footer_table', array('id' => $id));
			return $res->row();
	 }
	 
	 function update_footer($data, $id)
	 {
		$this->db->where('id', $id);
		$this->db->update('real_footer_table', $data); 
		return true;
	 }
	 
	 function delete_footer($id)
	 {
		return $this->db->delete('real_footer_table', array('id' => $id)); 
	 }
	 
	 function get_videos()
	 {
		$this->db->select('real_video_table.id as vdo_id, real_video_table.vdo_title as v_title, real_video_table.video_name as video_name, real_video_table.create_date as v_create_date, real_video_table.modified_date as v_modified_date, real_video_table.vdo_status as v_status, real_pages.title as p_title');
		$this->db->from('real_video_table');
		$this->db->join('real_pages', 'real_pages.id = real_video_table.page_id', 'left');
		$query = $this->db->get();
		return $query->result();
	 }
	 
	 function update_vdo($data, $id)
	 {
		$this->db->where('id', $id);
		$this->db->update('real_video_table', $data); 
		return true;
	 }
	 
	 function get_page_for_vdo()
	 {
	 	$re = $this->db->get_where('real_pages', array('status'=>1));
		return $re->result();
	 }
	 
	 function delete_video_mod($id)
	 {
		return $this->db->delete('real_video_table', array('id' => $id)); 
	 }
	 
	 	 
	 function get_ads()
	 {
	 	$res = $this->db->get('real_ads');
		return $res->result();
	 }
	 
	 function update_ads($data, $id)
	 {
		$this->db->where('id', $id);
		$this->db->update('real_ads', $data); 
		return true;
	 }
	 
	 // here edit
	 
	 function get_header_info($id)
	 {
	 		$res = $this->db->get_where('real_header_table', array('id' => $id));
			return $res->row();
	 }
	 
	 function get_ads_info($id)
	 {
	 		$res = $this->db->get_where('real_ads', array('id' => $id));
			return $res->row();
	 }
	 
	 function delete_ads($id)
	 {
		return $this->db->delete('real_ads', array('id' => $id)); 
	 }
	 
	 function delete_head_logo_mod($id)
	 {
		return $this->db->delete('real_header_table', array('id' => $id)); 
	 }
	 
}