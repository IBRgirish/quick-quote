<?php

class Settings_model extends CI_Model {
	


	function Members_model() {
		parent::__construct();		
	}
	
	public function insert_update_settings($data, $id='')
	{
		if($id!='')
		{
			$this->db->update($this->config->item('config_table'),$data, array('id'=>$id));
		}
		else
		{
			$this->db->insert($this->config->item('config_table'),$data);
		}
	}
	
	public function get_setting_info($id)
	{
		$setting_table = $this->config->item('config_table');
	  	$this->db->select("$setting_table.*");	
	  	$this->db->where("$setting_table.id", $id);	  
	  	$query = $this->db->get($this->config->item('config_table'));
	  	
	  	if($query->num_rows == 1) 
	  	{
			return $query->row();
	  	}
	  	else 
	  	{
			return FALSE;
	  	}
	}
}

