<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Order_model extends CI_Model {

	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	 
	public function get_user_order_list($limit, $offset)
	{
		$this->db->limit($limit, $offset);
		$this->db->from($this->config->item('realestate_order_table'));
		$query = $this->db->get();
		return $query->result();
	}
	 
	 
	public function get_order_info($order_id)
	{
		$this->db->where('id', $order_id);
		$this->db->from($this->config->item('realestate_order_table'));
		$query = $this->db->get();
		return $query->result();
	}
	 
	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
	
	public function member_status_update($id,$status)
	{
		$member_table = $this->config->item('member_table');
		
		$data = array(
			'status' => $status,
		);
		
		$this->db->where('id', $id);
		if($this->db->update($member_table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
	public function count_order()
	{
	
		return $this->db->count_all_results($this->config->item('realestate_order_table'));
	}
	
	
}