<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Data_model extends CI_Model {

	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	public function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	public function _insert($data, $table)
	{
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	public function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * validate_admin function
	 *
	 * @access	public
	 * @return	array
	 */
	
	
	
	
	public function list_catlog($limit, $offset, $sort_by, $sort_order)
	{
		$sort_order = ($sort_order == 'asc') ? 'asc' : 'desc';
		$sort_columns = array('name', 'id');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'id';
		
		$this->db->limit($limit, $offset);
		$this->db->from('rq_rqf_quote_catlogs');
		$this->db->order_by($sort_by, $sort_order);
		
		
		$query = $this->db->get();
		return $query->result();
	}	

	
	public function data($id)
	{   
	    $this->db->select('*');
		$this->db->where('id', $id);
		$this->db->from('rq_rqf_data_dictionary');
		$query = $this->db->get();
		return $query->result();
	}
	
	
	
	
	//OLD
	public function get_property_info($id)
	{
			$this->db->where('id',$id);
			
		if( $query = $this->db->get($this->config->item('property_listing_table')) )
		{
			
			if ($query->num_rows() > 0)
			{
				return $query->row();
			}
		}
		return FALSE;
	}
	
	public function insert_update_property_listings($data, $id=NULL)
	{
		if($id)
			return $this->_update($id, $data, $this->config->item('property_listing_table'));
		else
			$this->_insert($data, $this->config->item('property_listing_table'));
			return $this->db->insert_id();
	}
	
	public function insert_update_realestate_valuation($data, $id=NULL)
	{
	
		$this->db->where('property_id',$id);
		$query = $this->db->get('real_estate_valuation');
		if ($query->num_rows() > 0)
		{
			$this->db->where('property_id', $id);
			return $this->db->update('real_estate_valuation', $data); 

		}else{

			return $this->db->insert('real_estate_valuation', $data); 
		}
	}
	
	public function get_realestate_valuation_info($id)
	{
		$this->db->where('property_id',$id);
			
		if( $query = $this->db->get($this->config->item('realestate_valuation_table')))
		{
			
			if ($query->num_rows() > 0)
			{
			
				return $query->row();
			}
		}
		return FALSE;
	}
	
	
	
	public function delete_property_listing($id)
	{
		$this->_delete($id, $this->config->item('property_listing_table'));
		return $this->db->delete($this->config->item('realestate_valuation_table'), array('property_id' => $id)); 
	}
	
	public function delete_mainimage($id,$data)
	{
		
		return $this->_update($id, $data, $this->config->item('property_listing_table'));
		
	}
	
	
	/**
	 * insert_update_pages function
	 *
	 * @access	public
	 * @params	array data, integer id
	 * @return	boolean
	 */
	
	public function insert_update_pages($data, $id=NULL)
	{
		if($id)
			return $this->_update($id, $data, $this->config->item('pages_table'));
		else
			return $this->_insert($data, $this->config->item('pages_table'));
	}
	
/**
 * delete_page function
 *
 * @access	public
 * @param	integer id
 * @return	boolean
 */
	
	public function delete_page($id)
	{
		return $this->_delete($id, $this->config->item('pages_table'));
	}

/**
 * count_admins function
 *
 * @access	public
 * @return	integer
 */
	public function count_admins()
	{
		return $this->db->count_all_results($this->config->item('admin_table'));
	}
/**
 * list_admins function
 *
 * @access	public
 * @param	integer limit, integer offset
 * @return	array
 */
	public function list_admins($limit, $offset)
	{
		
		$query = $this->db->get($this->config->item('admin_table'));
		return $query->result();
	}
	/**
	 * insert_update_admins function
	 *
	 * @access	public
	 * @params	array data, integer id
	 * @return	boolean
	 */
	

/**  
 * get_admins_info function
 *
 * @access	public
 * @param	integer id
 * @return	array
 */
	
	
/**
 * delete_admins function
 *
 * @access	public
 * @param	integer id
 * @return	boolean
 */
	


	public function count_records($table){		
		$total;
		foreach($table as $tablename){			
			$total[]=$this->db->count_all_results($this->config->item($tablename));			
		}
		return $total;
	}
	
	public function count_activerecords($table){		
		$active;
		foreach($table as $tablename){						
			$this->db->where('status',1);
			$active[]=$this->db->count_all_results($this->config->item($tablename));
		}
		return $active;
	}
	
	public function count_inactiverecords($table){		
		$inactive;
		foreach($table as $tablename){									
			$this->db->where('status',0);
			$inactive[]=$this->db->count_all_results($this->config->item($tablename));
		}

		return $inactive;		
	}
	
	public function count_assetrecords($table){		
			$asset;												
			$asset[]=$this->db->count_all_results($this->config->item($table));		

		return $asset;		
	}

	public function count_updatesrecords($table){		
			$asset;												
			$asset[]=$this->db->count_all_results($this->config->item($table));		

		return $asset;		
	
	}
	
	public function get_story_list(){		
		$this->db->select('acms_articles.id,acms_articles.articles_content');
		$this->db->from('acms_articles');
		//$this->db->limit(5);
		$this->db->order_by('id','desc');			
		$this->db->where('status',1);
		$this->db->where('main_image','');
		$query = $this->db->get();  						
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		
	}
	
	/* By Sunil Chouhan */
	public function get_catlog_info($id)
	{	
	  $this->db->select('rq_rqf_quote_catlogs.*');	
	  $this->db->where('rq_rqf_quote_catlogs.id', $id);	  
	  $query = $this->db->get('rq_rqf_quote_catlogs');
	  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else 
	  {
			return FALSE;
	  }
	}
	
		public function get_catlog_infos($id)
	{	
	  
		$this->db
         ->select("a.id_pip_value,a.catlog_type,g.id,d.carrier_id,a.value,a.status,f.mandatory,c.id_pip_carrier,b.id_pip_carrier_value,e.id_pip_state_carrier,f.id_pip_state");
		 $this->db->join('pip_carrier_value b', 'b.id_pip_value=a.id_pip_value');
		 $this->db->join('pip_carrier c', 'c.id_pip_carrier=b.id_pip_carrier');
		 $this->db->join('carriers d', 'd.carrier_id=c.id_carrier');
		 $this->db->join('pip_state_carrier e', 'e.id_pip_carrier=c.id_pip_carrier');
		 $this->db->join('pip_state f', 'f.id_pip_state=e.id_pip_state');
		 $this->db->join('real_country g', 'g.id=f.id_state');
		 $this->db->from('pip_value a');
		 $this->db->where('a.id_pip_value',$id);
         $query = $this->db->get();
	  
	  if($query->num_rows == 1) 
	  {
			return $query->result();
	  }
	  else 
	  {
			return FALSE;
	  }
	}
	
	public function get_catlog_info1($id)
	{	
	  $this->db->select('rq_rqf_quote_catlog_new.*');	
	  $this->db->where('rq_rqf_quote_catlog_new.id', $id);	  
	  $query = $this->db->get('rq_rqf_quote_catlog_new');
	  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else 
	  {
			return FALSE;
	  }
	}
	public function get_catlog_by_name($name,$state)
	{  
	
	 
		if($name != '' && $state != '')
		{
			
			//$sql = "SELECT * FROM `rq_rqf_quote_catlog_new` WHERE `catlog_name` LIKE '%$name%' And `state` LIKE '%$state%'";
		      
		 $this->db->select("a.tax,");		
		 $this->db->join('carriers d', 'd.carrier_id=a.id_carrier');
		 $this->db->join('real_state g', 'g.id=a.id_state');
		 $this->db->from('slatax a');
		 $this->db->where('a.status',1);
		 $this->db->where('a.is_delete',0);
		 $this->db->like('d.carrier_value',$name);
		 $this->db->like('g.state_name',$state);
         $query = $this->db->get();
		     			
			if($query->num_rows > 0) 
			  {
				return $query->result();
				
			
			  }
			  else 
			  {
					return FALSE;
			  }			
		}
		
	}
	
	public function get_carrier_by_state($state)
	{
		
			$this->db->select('rq_rqf_quote_catlog_new.catlog_name');
			
			if($state != '')
			{
				$this->db->where('rq_rqf_quote_catlog_new.state', $state);
			}			
			$this->db->where('rq_rqf_quote_catlog_new.status', '1');
			$this->db->order_by("date_modified", "desc");
			$query = $this->db->get('rq_rqf_quote_catlog_new');	
			
			if($query->num_rows > 0) 
			  {
					foreach ($query->result_array() as $row)
					{
					   $data['catlogs'][$row['catlog_name']] = $row['catlog_name'];
					}
					
					return $data;
			  }
			  else 
			  {
					return FALSE;
			  }			
		
		
	}
	public function insert_update_carrier_catlog($data, $id='')
	{
		if($id!='')
		{
			$this->db->update('rq_rqf_quote_catlog_new',$data, array('id'=>$id));
			//echo $this->db->last_query();
		}
		else
		{
			$this->db->insert('rq_rqf_quote_catlog_new',$data);
		}
	}
	public function insert_update_catlog($data, $id='')
	{
		if($id!='')
		{
			$this->db->update('rq_rqf_quote_catlogs',$data, array('id'=>$id));
		}
		else
		{
			$this->db->insert('rq_rqf_quote_catlogs',$data);
		}
	}
	
	public function insert_update_carrier_catlogs($id='')
	{
	       
		   
		
			$id_pip_state=$this->input->post('id_pip_state');
			$id_pip_carrier=$this->input->post('id_pip_carrier');
			$id_pip_carrier_value=$this->input->post('id_pip_carrier_value');
			$id_pip_state_carrier=$this->input->post('id_pip_state_carrier');
			$status=$this->input->post('status');
			
			
			$catlog['catlog_type'] = $this->input->post('cat_f');
			$catlog['value'] = $this->input->post('catlog_value');
			$catlog['status'] = isset($status)?$status: 1;	
		    if($id != ''){	
			$this->db->where('id_pip_value',$id);	
			$this->db->update('pip_value',$catlog);
			}else{
			$this->db->insert('pip_value',$catlog);					
			$id=$this->db->insert_id();	
			}
			$catlog1['id_state'] = $this->input->post('carrier_state');
			$catlog1['default_value'] = $this->input->post('catlog_value');
			$catlog1['mandatory'] = $this->input->post('mandatory_value');
			$catlog1['catlog_type'] = $this->input->post('cat_f');
			$catlog1['status'] = isset($status)?$status: 1;	
			if($id_pip_state != ''){	
			$this->db->where('id_pip_state',$id_pip_state);	
			$this->db->update('pip_state',$catlog1);
			}else{
			$this->db->insert('pip_state',$catlog1);
			$state_id=$this->db->insert_id();
			}
					
						
			$catlog4['id_carrier'] = $this->input->post('carrier_id');
			$catlog4['status'] = isset($status)?$status: 1;				
		
			
			if($id_pip_carrier != ''){	
			$this->db->where('id_pip_carrier',$id_pip_carrier);	
			$this->db->update('pip_carrier',$catlog4);
			}else{
			$this->db->insert('pip_carrier',$catlog4);
			$carrier_id=$this->db->insert_id();
			}
						
			$catlog2['id_pip_carrier'] = isset($carrier_id)?$carrier_id :$id_pip_carrier;
			$catlog2['id_pip_state'] = isset($state_id)?$carrier_id:$id_pip_state;
			$catlog2['status'] = isset($status)?$status: 1;	
			
			if($id_pip_state_carrier != ''){	
			$this->db->where('id_pip_state_carrier',$id_pip_state_carrier);	
			$this->db->update('pip_state_carrier',$catlog2);
			}else{
			$this->db->insert('pip_state_carrier',$catlog2);
			
			}
			
			
			
			$catlog3['id_pip_value'] = $id;
			$catlog3['id_pip_carrier'] = isset($carrier_id)?$carrier_id :$id_pip_carrier;
			$catlog3['status'] = isset($status)?$status: 1;	
			
			if($id_pip_carrier_value != ''){	
			$this->db->where('id_pip_carrier_value',$id_pip_carrier_value);	
			$this->db->update('pip_carrier_value',$catlog3);
			}else{
			$this->db->insert('pip_carrier_value',$catlog3);
			
			}		
			
			
	
	}
	
	
	public function insert_update_slatax_catlogs($id=''){
		   
		    $carrier_id=$this->input->post('carrier_id');
			$catlog_value=$this->input->post('catlog_value');
			$carrier_state=$this->input->post('carrier_state');
		    $status=$this->input->post('status');
		   
		    $catlog1['id_state'] = $this->input->post('carrier_state');
			$catlog1['id_carrier'] = $this->input->post('carrier_id');
			$catlog1['tax'] = $this->input->post('catlog_value');
			$catlog1['catlog_type'] = $this->input->post('cat_f');
			$catlog1['status'] = isset($status)?$status: 1;
			if($id!= ''){	
			$this->db->where('id_slatax',$id);	
			$this->db->update('slatax',$catlog1);
			}else{
			$this->db->insert('slatax',$catlog1);
			
			}
		}
   public function get_slatax_infos($id)
	{	
	  
		 $this->db->select("*");	
		 $this->db->from('slatax a');
		 $this->db->where('id_slatax',$id);
       $query = $this->db->get();
	  
	  if($query->num_rows == 1) 
	  {
			return $query->result();
	  }
	  else 
	  {
			return FALSE;
	  }
	}
}