<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Quote extends RQ_Controller {

	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -  

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in 

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */

    

   public $file_upload_path = './uploads/quote_request/';

	

    public function __construct()

	{

		parent::__construct();

	
        $this->load->library('api');  
		//$this->call = & get_instance();
		$this->load->model('quote_model');
        
		$this->load->model('new_quote_model');
		
		$this->load->model('edit_model');
		
		$this->load->model('administration/underwriter_model');

		$this->load->library('pagination');  

		$this->load->helper('my_helper');

		// Your own constructor code

	}
	function checkdatatable(){
		$this->load->view('list_data_table');
	}
	function ajax_list_siteuser(){
		$this->datatables->select("id,'Carrier'", FALSE)
		->from('rq_rqf_quote_catlog_new');
        echo $this->datatables->generate();
	}
	
	
	
	
	
	
	
	public function index()
	{ 
		$expiration_date = array();
		$broker_license = $this->api->get_response(BAPLICENSE);
		$brokerdata = $this->api->prepare_response($broker_license);
		foreach($brokerdata as $br_data){
			if($br_data->primary_email == $this->session->userdata('username')){
				$expiration_date[] = $br_data->expiration_date;
			}
		}
		$exp_date = implode($expiration_date ,',');
		$data['expiration_date'] = $exp_date;

		$user_quoute_info = $this->quote_model->get_quote_data(); 

		$data['user_quote_data'] = $user_quoute_info;

		$data['trailer_type_select'] = $this->trailer_type_select();
        
		$data['truck_trailer_type_select'] = $this->trailer_type_select1();
		
		$data['nature_of_business'] = $this->nature_of_business();

		$data['commodities_haulted'] = $this->commodities_haulted();

		$data['make_model_tractor'] = $this->make_model('tractor');

		$data['make_model_truck'] = $this->make_model('truck');

		$data['make_model_trailer'] = $this->trailer_model();
		
		$data['truck_make_model_trailer'] = $this->trailer_model1();

		$data['truck_number_of_axles'] = $this->number_of_axles('truck');

		$data['tractor_number_of_axles'] = $this->number_of_axles('tractor');
			
		$data['filing_type_show'] = $this->filing_type_show();
		$data['underwriter'] = $variable;
		
		$this->load->view('quote_view', $data);
		
	}


public function request_quote($quote_id='',$print=''){
	/*if(!$this->require_login_user() && !$this->require_login_underwriter())
		{
			$base = base_url('login');
			redirect($base);
		}*/
		if($print=='print' || $print=='print_css' || $this->session->userdata('member_id') || $this->session->userdata('underwriter_id')){
	
		$user_quoute_info = $this->quote_model->get_quote_data(); 
		$data['user_quote_data'] = $user_quoute_info;
		$data['get_all_broker'] = $this->new_quote_model->getAgencyBroker($this->session->userdata('member_id'));
		$data['make_model_truck'] = $this->make_model('truck');
		$data['truck_number_of_axles'] = $this->number_of_axles('truck');
		$data['commodities_haulted'] = $this->commodities_haulted();
		
		if($quote_id=='underwriter'){
		$data['underwriter']=$quote_id;
		}else{
		if(!empty($quote_id)){	
		if($print=='print'){	
		$data['print'] = 'print';	
		$data['underwriter'] = 'underwriter';	
		}
		if($print=='print_css'){	
		$data['print'] = 'print_css';			
		}
 		$data['broker_info'] = $this->new_quote_model->broker_info($quote_id);
		$data['broker_email'] = $this->new_quote_model->email_info($quote_id,'broker');
		$data['broker_phone'] = $this->new_quote_model->phone_info($quote_id,'broker');
		$data['broker_fax'] = $this->new_quote_model->fax_info($quote_id,'broker');
		
		$data['person_info'] = $this->new_quote_model->person_info($quote_id);
		
		$data['person_email'] = $this->new_quote_model->email_info($quote_id,'person');
		$data['person_phone'] = $this->new_quote_model->phone_info($quote_id,'person');
		$data['person_fax'] = $this->new_quote_model->fax_info($quote_id,'person');
		
		$data['insured_info'] = $this->new_quote_model->insured_info($quote_id);
		
		$data['insured_email'] = $this->new_quote_model->email_info($quote_id,'insured');
		$data['insured_phone'] = $this->new_quote_model->phone_info($quote_id,'insured');
		$data['insured_fax'] = $this->new_quote_model->fax_info($quote_id,'insured');
		$data['insured_b'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_bussiness');
		$data['insured_gh8'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_gh8');
		$data['insured_t'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_transported');
		$data['certificate'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_certificate');
		$data['insured_a'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_additional');	



		$data['insured_gh6']=$this->new_quote_model->get_database_inform6($quote_id,'rq_rqf_quick_quote_gh6',18);
        $data['insured_gh61']=$this->new_quote_model->get_database_inform6($quote_id,'rq_rqf_quick_quote_gh6',19);
        $data['insured_gh62']=$this->new_quote_model->get_database_inform6($quote_id,'rq_rqf_quick_quote_gh6',20);
		
		$data['insured_gh5a']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5a',25);
		$data['insured_gh51a']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5a',26);
		 
		$data['insured_gh5b']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5b',25);
		$data['insured_gh51b']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5b',26);
		
		$data['insured_gh5c']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5c',25);
		$data['insured_gh51c']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5c',26);
		
		$data['insured_gh5d']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5d',25);
		$data['insured_gh51d']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5d',26);
		
		$data['insured_gh5e']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5e',25);
		$data['insured_gh51e']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5e',26);
		
		$data['insured_gh5f']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5f',25);
		$data['insured_gh51f']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5f',26);
		
		$data['insured_gh5g']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5g',25);
		$data['insured_gh51g']=$this->new_quote_model->get_database_inform5($quote_id,'rq_rqf_quick_quote_gh5g',26);
		 
		 
		$insured_g=$this->new_quote_model->quick_quote_info_array($quote_id,'rq_rqf_quick_quote_gh9');
		$insured_gh9=array();
		 foreach($insured_g as $insured_gs){			
			$array2 = array('suppss'=>array());
		    $insured_gh9[] = array_merge($array2, $insured_gs); 			 
			
			}
			
		$data['insured_gh11']=$this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_gh11');
		 
		 
	    $data['insured_gh9']= $insured_gh9;	
		
		if(!empty($data['insured_info'])){
	
		$data['commodities_haulted'] = $this->commodities_haulted($data['insured_info'][0]->commodities_haulted);
       
		$data['insured_infos_1'] = $this->nature_of_business($data['insured_info'][0]->nature_of_business);
		
		
		 }
		
		$data['ins_info'] = $this->new_quote_model->ins_info($quote_id);
		$data['ins_email'] = $this->new_quote_model->email_info($quote_id,'ins');
		$data['ins_phone'] = $this->new_quote_model->phone_info($quote_id,'ins');
		$data['ins_fax'] = $this->new_quote_model->fax_info($quote_id,'ins');
		$data['filing'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_filing');
		$data['filings'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_filings');
		
		$data['coverage'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_coverage');
		$data['truck_vh'] = $this->new_quote_model->quick_quote_vh_info($quote_id,'rq_rqf_quick_quote_vh','truck');
		$data['tractor_vh'] = $this->new_quote_model->quick_quote_vh_info($quote_id,'rq_rqf_quick_quote_vh','tractor');
		$data['trailer_vh'] = $this->new_quote_model->quick_quote_vh_info($quote_id,'rq_rqf_quick_quote_vh','trailer');
		$data['other_vh'] = $this->new_quote_model->quick_quote_vh_info($quote_id,'rq_rqf_quick_quote_vh','other');
		
		
		
		
		$data['truck_lessor'] = $this->new_quote_model->quick_quote_lessor_info($quote_id,'rq_rqf_quick_quote_vh_garage','truck');
		$data['tractor_lessor'] = $this->new_quote_model->quick_quote_lessor_info($quote_id,'rq_rqf_quick_quote_vh_garage','tractor');
		$data['trailer_lessor'] = $this->new_quote_model->quick_quote_lessor_info($quote_id,'rq_rqf_quick_quote_vh_garage','trailer');
		$data['other_lessor'] = $this->new_quote_model->quick_quote_lessor_info($quote_id,'rq_rqf_quick_quote_vh_garage','other');
		
		$data['truck_vh_eqp'] = $this->new_quote_model->quick_quote_vh_infos($quote_id,'rq_rqf_quick_quote_vh_eqp','truck');
		$data['tractor_vh_eqp'] = $this->new_quote_model->quick_quote_vh_infos($quote_id,'rq_rqf_quick_quote_vh_eqp','tractor');
		$data['trailer_vh_eqp'] = $this->new_quote_model->quick_quote_vh_infos($quote_id,'rq_rqf_quick_quote_vh_eqp','trailer');
		$data['other_vh_eqp'] = $this->new_quote_model->quick_quote_vh_infos($quote_id,'rq_rqf_quick_quote_vh_eqp','other');
		
		$data['tractor_vh_eqpmntss'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_vh_eqps','tractor');
		$data['other_vh_eqpmntss'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_vh_eqps','other');
	
		
		$data['truck_fax'] = $this->new_quote_model->fax_info($quote_id,'truck');
		$data['tractor_fax'] = $this->new_quote_model->fax_info($quote_id,'tractor');
		$data['trailer_fax'] = $this->new_quote_model->fax_info($quote_id,'trailer');
		$data['other_fax'] = $this->new_quote_model->fax_info($quote_id,'other');
		
		$data['truck_email'] = $this->new_quote_model->email_info($quote_id,'truck');
		$data['tractor_email'] = $this->new_quote_model->email_info($quote_id,'tractor');
		$data['trailer_fax'] = $this->new_quote_model->email_info($quote_id,'trailer');
		$data['other_email'] = $this->new_quote_model->email_info($quote_id,'other');
		
		
		$data['losses_lia'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_losses','liability');
		$data['losses_pd'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_losses','pd');
		$data['losses_cargo'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_losses','cargo');
		
		$data['losses'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_loss');	
		$data['owner'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_owner');	
		$data['driver'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_driver');
		$data['drivers'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_drivers');
		$data['owners'] = $this->new_quote_model->quick_quote_info($quote_id,'rq_rqf_quick_quote_owners');
		//$data['underwriter'] = 'underwriter';
		$data['underwriter_u'] = 'underwriter';
		if ($this->session->userdata('underwriter_id')) {
			$data['underwriter'] = 'underwriter';
			$data['ver']=$this->new_quote_model->quick_quote_ver($quote_id,'rq_rqf_quick_quote_ver');
			
		}

	  // $data['insured_info1'] = get_catlog_dropdown_company('nature_of_business','nature_of_business','class="myclass span12 chzn-select" multiple', '', '', isset($data['insured_info'][0]->nature_of_business) ? $data['insured_info'][0]->nature_of_business : '');
	
		}
		else{
			$id=$this->session->userdata('member_id');	
			
			$b_info=$this->new_quote_model->rq_members_info($id,'rq_members');
			
			if(!empty($b_info)){
			$data_b['br_id']=!empty($b_info[0]->id)?$b_info[0]->id:'';
			$data_b['fistname']=!empty($b_info[0]->first_name)?$b_info[0]->first_name:'';
			$data_b['middlename']=!empty($b_info[0]->middle_name)?$b_info[0]->middle_name:'';
			$data_b['lastname']=!empty($b_info[0]->last_name)?$b_info[0]->last_name:'';
			$data_b['dba']=!empty($b_info[0]->dba_name)?$b_info[0]->dba_name:'';			
			$data_b['street']=!empty($b_info[0]->address)?$b_info[0]->address:'';
			$data_b['city']=!empty($b_info[0]->city)?$b_info[0]->city:'';
			$data_b['state']=!empty($b_info[0]->state)?$b_info[0]->state:'';
			$data_b['zip']=!empty($b_info[0]->zip_code)?$b_info[0]->zip_code:'';
			$data_b['country']=!empty($b_info[0]->country)?$b_info[0]->country:'';			
			$data_e['email']=!empty($b_info[0]->email)?$b_info[0]->email:'';
			//$data_e['email']=!empty($b_info[0]->sec_email)?$b_info[0]->sec_email:'';
			$data_e['email_type']='Office';
			
			
			if(!empty($b_info[0]->phone_number)){
			$data_p_1 = explode('-',$b_info[0]->phone_number);
			if($b_info[0]->country=='US'){
			     $data_p['code_type']='1';	
			}
			$data_p['area_code']=!empty($data_p_1[0])?$data_p_1[0]:'';
			$data_p['prefix']=!empty($data_p_1[1])?$data_p_1[1]:'';
			$data_p['suffix']=!empty($data_p_1[2])?$data_p_1[2]:'';
			$data_p['phone_type']='Office';
			$data_p['pri_country']=1;
			
			}
			
		    if(!empty($b_info[0]->fax_number)){
			$data_b['fax']='Yes';
			if($b_info[0]->country=='US'){
			     $data_f['code_type_fax']='1';	
			}
			$data_f_1 = explode('-',$b_info[0]->fax_number);
			$data_f['area_code_fax']=!empty($data_f_1[0])?$data_f_1[0]:'';
			$data_f['prefix_fax']=!empty($data_f_1[1])?$data_f_1[1]:'';
			$data_f['suffix_fax']=!empty($data_f_1[2])?$data_f_1[2]:'';
			$data_f['fax_type']='Office';
			$data_f['country_fax']=1;
			}
		
			}
			
			$data['broker_info'][]=$data_b;
			$data['broker_email'][]=$data_e;
			$data['broker_phone'][]=$data_p;
			$data['broker_fax'][]=$data_f;
			
			$data['insured_infos_1'] = $this->nature_of_business();
			$data['underwriter_u'] = 'underwriter';
		}
		
		//$data['insured_info1'] = get_catlog_dropdown_company('nature_of_business','nature_of_business','myclass span2 chzn-select', '', '', isset($data['insured_info'][0]->nature_of_business) ? $data['insured_info'][0]->nature_of_business : '');
	  }
		$this->load->view('quote_view', $data);
		}else{
		 if(!$this->require_login_user() && !$this->require_login_underwriter())
		 {
		  	$base = base_url('login');
			redirect($base);
		  }
		}
	} 

      public function getSingleperonInfo()
 {
	$person_id = $this->input->post('person_id');
	$person_data = $this->new_quote_model->getSingleperonInfo($person_id);	
	
	 
	$data['person_info'] = $this->new_quote_model->getSingleperonInfo($person_id);	
	$data['person_phone'] = $this->new_quote_model->getPeronPhone($person_id);	
	$data['person_fax'] = $this->new_quote_model->getPeronFax($person_id);
	$data['person_email'] = $this->new_quote_model->getPeronemail($person_id);
    echo form_safe_json(json_encode($data));
	
 }
  
  
  
  
               

    //ADDED BY khan 14 -oct 2014//
		//$data['filing_type_show'] = $this->filing_type_show();
		
	function filing_type_show($fillings='') {
		$catlog =''; //print_r($fillings);
		$catlog .='<select id="filing_type_otherABC123" class="span12 filing_type filing_type_add" required="required" name="filing_type[]"  onChange="filling_other(this.value,this.id)" >';
		$catlog .='<option value="">Select</option>';
		$catlog_data = $this->quote_model->get_catlog_options(25);
		//echo"<pre>"; print_r($catlog_data);die;
		if($catlog_data)
		{		
			foreach ($catlog_data as $catlogData)
						{	/*if(in_array($catlogData->catlog_value, $fillings)){ $selected = "selected"; }else { $selected = ""; }*/
				$catlog .='<option value="'.$catlogData->catlog_value.'" >'.$catlogData->catlog_value.'</option>';	
			}
		}
		$catlog .='<option value="Other">Other</option>';
		$catlog .='</select>';
			return $catlog;
    }

	//ADDED BY MAYANK 21 JULY//

	function make_model($type,$selected = '')

	{

		if($type == "tractor")

		{

			$catlog ='';

			$catlog .='<select type="text" id="make_model_vh" name="tractor_make_model_vh[]" class="span12" >';

			$catlog .='<option value="">Select</option>';

			$catlog_data = $this->quote_model->get_catlog_options('trac_truck_model');

			

			if($catlog_data)

			{ 	

				foreach ($catlog_data as $catlogData)

				{

					$catlog_array[] = ucfirst($catlogData->catlog_value);

				}

				sort($catlog_array);

				foreach($catlog_array as $catlog_value)
				{
					// checnged by sulbha
					if ($catlog_value == $selected)
					{
						$catlog .='<option value="'.$catlog_value.'" selected="selected">'.$catlog_value.'</option>';	
						
					}else
					{
						$catlog .='<option value="'.$catlog_value.'">'.$catlog_value.'</option>';		
					}
					// END checnged by sulbha
					
					//$catlog .='<option value="'.$catlog_value.'">'.$catlog_value.'</option>';	

					}	

			}

			$catlog .='</select>';

		}

		else if($type == "truck")

		{

			$catlog ='';

			$catlog .='<select type="text" id="make_model_vh" name="make_model_vh[]" class="span12"  >';

			$catlog .='<option value="">Select</option>';

			$catlog_data = $this->quote_model->get_catlog_options('trac_truck_model');

			if($catlog_data)

			{

				foreach ($catlog_data as $catlogData)

				{

					$catlog_array[] = ucfirst($catlogData->catlog_value);

				}

				sort($catlog_array);

				foreach($catlog_array as $catlog_value)

				{
	if ($catlog_value == $selected)
						{
							$catlog .='<option value="'.$catlog_value.'" selected="selected">'.$catlog_value.'</option>';			
						}else
						{
							$catlog .='<option value="'.$catlog_value.'" >'.$catlog_value.'</option>';		
						}
					//$catlog .='<option value="'.$catlog_value.'">'.$catlog_value.'</option>';	

					}	

			}

			$catlog .='</select>';

		}

			return $catlog;

	}

	

	public function quotes()

	{

			$limit = 25;

			$total = $this->underwriter_model->count_underwriter(); 

			

			$data['quote'] = $this->quote_model->get_quote();

			

			$data['rows'] = $this->quote_model->get_quote_rate('','');

			//print_r($data['rows']);

			$data['object'] = $this;

			

			$config['base_url'] = base_url('administration/quotes');

			$config['total_rows'] = $total;

			$config['per_page'] = $limit;

			

			$data['total'] = $total;

			$this->pagination->initialize($config);



			$this->load->view('administration/quotes', $data);

	}

	

	/**By Ashvin Patel 14/may/2014**/

	

	function trailer_model($value='') {

		$catlog ='';

		$catlog .='<select type="text" id="make_model_vh__SECID__" name="trailer_make_model_vh[]" class="span12">';

		$catlog .='<option value="">Select</option>';

		$catlog_data = $this->quote_model->get_catlog_options('trailer_model');

		if($catlog_data)

		{

			foreach ($catlog_data as $catlogData)

			{

				$catlog_array[] = ucfirst($catlogData->catlog_value);

			}

			sort($catlog_array);
$selecty='';
			foreach($catlog_array as $catlog_value)

			{
              if($catlog_value==$value){$select='selected';}else{$select='';}
				$catlog .='<option value="'.$catlog_value.'" '.$select.'>'.$catlog_value.'</option>';	

				}

		}

		$catlog .='</select>';

			return $catlog;

    }

	
	function trailer_model1($value='') {

		$catlog ='';

		$catlog .='<select type="text" id="make_model_vh__SECID__" name="truck_trailer_make_model_vh[]" class="span12">';

		$catlog .='<option value="">Select</option>';

		$catlog_data = $this->quote_model->get_catlog_options('trailer_model');

		if($catlog_data)

		{

			foreach ($catlog_data as $catlogData)

			{

				$catlog_array[] = ucfirst($catlogData->catlog_value);

			}

			sort($catlog_array);
$selecty='';
			foreach($catlog_array as $catlog_value)

			{
              if($catlog_value==$value){$select='selected';}else{$select='';}
				$catlog .='<option value="'.$catlog_value.'" '.$select.'>'.$catlog_value.'</option>';	

				}

		}

		$catlog .='</select>';

			return $catlog;

    }
	
	 function number_of_axles($type='',$value='') {

               $catlog ='';

               $catlog .='<select type="text" id="'.$type.'_number_of_axles" name="'.$type.'_number_of_axles[]" class="span12">';

               $catlog .='<option value="">Select</option>';

               $catlog_data = $this->quote_model->get_catlog_options('number_of_axles');

               if($catlog_data)

               {

                       foreach ($catlog_data as $catlogData)

						{

							$catlog_array[] = ucfirst($catlogData->catlog_value);

						}

						sort($catlog_array);
                        $select='';
						foreach($catlog_array as $catlog_value)

						{
                          if($catlog_value==$value){$select='selected';}else{$select='';}
							$catlog .='<option value="'.$catlog_value.'" '.$select.'>'.$catlog_value.'</option>';	

						}        

               }

               $catlog .='</select>';

                       return $catlog;

   }

	

	

	function trailer_type_select($value='') {

		$catlog ='';
       
		$catlog .='<select id="trailer_type_veh__SECID__" type="text" name="trailer_type[]" class="span12" >';

		$catlog .='<option value="">Select</option>';

		$catlog_data = $this->quote_model->get_catlog_options('trailer_type');

		if($catlog_data)

		{

			foreach ($catlog_data as $catlogData)

			{

				$catlog_array[] = ucfirst($catlogData->catlog_value);

			}
 $select='';
			sort($catlog_array);

			foreach($catlog_array as $catlog_value)

			{
                 if($catlog_value==$value){$select='selected';}else{$select='';}
				$catlog .='<option value="'.$catlog_value.'" '.$select.'>'.$catlog_value.'</option>';	

				}	

		}

		$catlog .='</select>';

			return $catlog;

    }


	function trailer_type_select1($value='') {

		$catlog ='';
       
		$catlog .='<select id="trailer_type_veh__SECID__" type="text" name="truck_trailer_type[]" class="span12" >';

		$catlog .='<option value="">Select</option>';

		$catlog_data = $this->quote_model->get_catlog_options('trailer_type');

		if($catlog_data)

		{

			foreach ($catlog_data as $catlogData)

			{

				$catlog_array[] = ucfirst($catlogData->catlog_value);

			}
 $select='';
			sort($catlog_array);

			foreach($catlog_array as $catlog_value)

			{
                 if($catlog_value==$value){$select='selected';}else{$select='';}
				$catlog .='<option value="'.$catlog_value.'" '.$select.'>'.$catlog_value.'</option>';	

				}	

		}

		$catlog .='</select>';

			return $catlog;

    }

	

	function nature_of_business($data='') {

		$catlog ='';
        $select='';
		$catlog .='<select type="text" name="nature_of_business[]" id="nature_of_business" class="span12 chzn-select" multiple >';

		//$catlog .='<option value="">Select</option>';

		$catlog_data = $this->quote_model->get_catlog_options('nature_of_business');

		
		if($catlog_data)

		{

			foreach ($catlog_data as $catlogData)

			{

				$catlog_array[] = ucfirst($catlogData->catlog_value);

			}
			
			sort($catlog_array);
			$data1=explode(';',$data);
	
			 $comarray = array();
			 foreach ($data1 as $data1_value){
				 $comarray[] = $data1_value;
			 }
			foreach($catlog_array as $catlog_value)

			{
				 foreach ($data1 as $data1_value){
                  if($catlog_value==$data1_value){  $select="selected='selected'";  }else{ $select="";}
				   $catlog .='<option value="'.$catlog_value.'" '.$select.'>'.$catlog_value.'</option>';	
				 }
				}
				 if(in_array('other',$comarray)){ $select="selected='selected'";  }else{ $select="";}
				$catlog .='<option value="other" '.$select.'>other</option>';

		}

		 $catlog .='</select>';
		

		return $catlog;

    }

	

	function commodities_haulted($data='') {

		$catlog ='';
        $select='';
		$catlog .='<select type="text" id="commodities_haulted" name="commodities_haulted[]" class="span12 chzn-select" multiple ng-model="insured_info.commodities_haulted" ng-change="commodities_haulted_v(insured_info.commodities_haulted)" >'; //onchange="check_commodities_other(this.id)"

		$catlog .='<option value="">Select</option>';

		$catlog_data = $this->quote_model->get_catlog_options('commodities_haulted');

		//print_r($catlog_data);

		if($catlog_data)

		{

			

			foreach ($catlog_data as $catlogData)

			{

				$catlog_array[] =  ucfirst($catlogData->catlog_value);

			}

			sort($catlog_array);
			
			
            $data1=explode(',',$data);
			$data1_value = array();
			foreach ($data1 as $data11){
				$data1_value[] = $data11;
			}
			
			foreach ($catlog_array as $catlog_value)
			{
			
             if(in_array($catlog_value,$data1_value)){ $select="selected";  }else{ $select="";}
				$catlog .='<option value="'.$catlog_value.'"'.$select.'>'.$catlog_value.'</option>';	
				
			}

		}
		      if(in_array('other',$data1_value)){ $select="selected";  }else{ $select="";}
		$catlog .='<option value="other" '.$select.'>other</option>';	

		$catlog .='</select>';

		return $catlog;

    }

	

// View Quick Quote

    public function QuickQuote($quote_id = ''){
		 $id= $this->session->userdata('member_id');
		 $data['brokers'] = $this->new_quote_model->get_quotes_info($id);
	     
		 $this->load->view('quick_info',$data);
		}


// Add And Edit Quick Quote
    public function Quick_Quotes($quote_id = '')

	{    					
			$this->new_quote_model->insert_broker_quote($quote_id);	
		    $this->new_quote_model->insert_person_quote($quote_id);	
		    $this->new_quote_model->insert_insured_quote($quote_id);
			$this->new_quote_model->insert_coverage_quote($quote_id);
			$this->new_quote_model->insert_driver_quote($quote_id);
			$this->new_quote_model->insert_owner_quote($quote_id);
			$this->new_quote_model->insert_losses_quote($quote_id);
			
			$this->edit_model->app_edit_db($quote_id);
			$this->edit_model->app_edit_ghi6($quote_id);
			$this->edit_model->app_edit_ghi9($quote_id);
			$this->edit_model->app_edit_ghi11($quote_id);
			
			$this->edit_model->app_edit_ghi51($quote_id);
			$this->edit_model->app_edit_ghi5($quote_id);
			 
			
			if($this->session->userdata('member_id')){              		
					 redirect('/quote/response', 'refresh');
					 
			}else if($this->session->userdata('underwriter_id')){				
				   redirect('/list_quote', 'refresh');
			}  
       
	}

	    public function Section_saves($sec_name='',$quote_id='')

	{    
	  if(!empty($sec_name)){	       
			   
	   echo $this->new_quote_model->insert_broker_quote($quote_id,'saved');	
			$this->new_quote_model->insert_person_quote($quote_id);			  
		    $this->new_quote_model->insert_insured_quote($quote_id);
			$this->edit_model->app_edit_db($quote_id);
			$this->edit_model->app_edit_ghi6($quote_id);
			$this->edit_model->app_edit_ghi9($quote_id);
			$this->edit_model->app_edit_ghi11($quote_id);			
			$this->edit_model->app_edit_ghi51($quote_id);
			$this->edit_model->app_edit_ghi5($quote_id);
		
			$this->new_quote_model->insert_coverage_quote($quote_id);
		  
			$this->new_quote_model->insert_driver_quote($quote_id);
			
			$this->new_quote_model->insert_owner_quote($quote_id);
		  
			$this->new_quote_model->insert_losses_quote($quote_id);
		  	
	   }
       
	}
	
    public function information_save($saved='',$quote_id='')

	{    
	       if($saved =='saved'){			   
	       echo $request_id=$this->new_quote_model->insert_broker_quote($quote_id,$saved);	
			$this->new_quote_model->insert_person_quote($quote_id);			  
		    $this->new_quote_model->insert_insured_quote($quote_id);
			$this->edit_model->app_edit_db($quote_id);
			$this->edit_model->app_edit_ghi6($quote_id);
			$this->edit_model->app_edit_ghi9($quote_id);
			$this->edit_model->app_edit_ghi11($quote_id);			
			$this->edit_model->app_edit_ghi51($quote_id);
			$this->edit_model->app_edit_ghi5($quote_id);			
			$this->new_quote_model->insert_coverage_quote($quote_id);		   
			$this->new_quote_model->insert_driver_quote($quote_id);		
			$this->new_quote_model->insert_owner_quote($quote_id);		   
			$this->new_quote_model->insert_losses_quote($quote_id);
		   }	
			
       
	}

	public function GenerateQuickQuoteMail($result)

	{	

		//ini_set('display_errors',1); 

		//error_reporting(E_ALL);

		

		set_time_limit(0);

		ini_set("memory_limit","-1");  

		ini_set("post_max_size","200M");  

		ini_set("upload_max_filesize","200M"); 

		

		$this->load->library('email');

		

		$this->load->model('user/login_model');



		$mvr =	explode (',', $result['uploaded_file']['mvr']);

		$losses = explode (',',	$result['uploaded_file']['losses']);

	

	$quote_folder = $result['quote_id'];

	$mvr_link = '<br><strong>MVR for Driver/Owner</strong><br>';



	

	foreach($mvr as $mvr_file){

		$url = '';

		$url = base_url('/uploads/docs').'/'.$mvr_file;

		

		//$url = str_replace(' ','',$url);

		

		$mvr_link .= '<a href="'.$url.'">'.$mvr_file.'</a><br>';

		

		$up_mvr_file_name = trim($mvr_file);

		$mvr_file_attach = str_replace(' ', '_', $up_mvr_file_name);

		

		$mvr_file_path = "./uploads/docs/$mvr_file";

		

		if(!empty($up_mvr_file_name)){
				
			$this->email->attach($mvr_file_path);

		}

			

	} 	

	

	$losses_link = '<br><strong>Losses</strong><br>';

	foreach($losses as $losses_file){

		$url = '';

		//$url = base_url('/uploads/quote_request').'/'.$quote_folder.'/'.$losses_file;

		$url = base_url('uploads/docs').'/'.$losses_file;

		//$url = str_replace(' ','',$url);

		

		$losses_link .= '<a href="'.$url.'">'.$losses_file.'</a><br>';

		

		$up_losses_file_name = trim($losses_file);

		$losses_file_path = str_replace(' ', '_', $up_losses_file_name);

		$losses_file_path = "./uploads/docs/$losses_file";

	

		if(!empty($up_losses_file_name)){

			$this->email->attach($losses_file_path);

		} 

	} 



	$data['mvr_link'] = $mvr_link;

	$data['losses_link'] = $losses_link;

	

	//$result['quote_id'] = 1;

	

	$mail_message = $this->load->view('quote_mail_view', $data, TRUE);



	//$my_view = $this->load->view('test_mail_view', $this->data, TRUE);

		$filename = $quote_folder.'/'.$result['quote_id'];

		// As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/

		//$pdfFilePath = FCPATH."uploads\quote_request\$filename.pdf";

		$pdfFilePath = FCPATH."/uploads/quote_request/$filename.pdf";

		

		//$data['page_title'] = 'Hello world'; // pass data to the view

	

	

		//if (file_exists($pdfFilePath) == FALSE)

		//{

		//ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley firstChild">

		// $html = $this->load->view('pdf_report', $data, true); // render the view into HTML



		$this->load->library('pdf');

		$pdf = $this->pdf->load();

		$pdf->SetFooter('Page : {PAGENO}');

		$pdf->SetHeader('{DATE m-j-Y}| Quote Request Number -'.$result['quote_id'] .' | Page : {PAGENO}/{nb}|');

		

		// LOAD a stylesheet

		//$stylesheet = file_get_contents(base_url().'/css/quote_mail_view.css');

		//echo $stylesheet; die;

		//$pdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

		

		//$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">

		$pdf->WriteHTML($mail_message); // write the HTML into the PDF

		$pdf->Output($pdfFilePath, 'F'); // save to file because we can

		//}



		$quote_pdf_path = "./uploads/quote_request/$filename.pdf"; 

	

		

		$members = $this->session->userdata('member_id');

		$member_id = (!empty($members)) ? $members : 0 ;

		

		//$underwriter_mail =  $this->quote_model->get_underwriter_email($member_id);

		//Old Email Address
		$underwriter_mail =  $this->quote_model->get_underwriter_email_address($member_id);
		
		//New email address by ashvin patel 15/sep/2014 quotes@gcib.net
		//$underwriter_mail['email'] =  'quotes@gcib.net';
		$underwriter_mail['ccEmail'][] = '';//quotes@gcib.net'; 
		//print_r($underwriter_mail['ccEmail']);
		//die;

		if($underwriter_mail){

			$email_value[] = $underwriter_mail['email'];

			

			if(!empty($underwriter_mail['ccEmail'])){

				$cc_email = implode(',',$underwriter_mail['ccEmail']);

				$cc_email = $cc_email.','.$this->session->userdata('username');

			} else {

				$cc_email = $this->session->userdata('username');

			}

		}

	//	echo "1".$cc_email."2";

		$super_mail_ids = $this->quote_model->get_supervisor_email();

		if($super_mail_ids){

			$email_value[] = $super_mail_ids;

		}

		

		if(!empty($email_value)){

			

			//Overwriting the actual PDF view

			$mail_message = 'Please find attachment.';

			

			$this->email->attach($quote_pdf_path);

			//$underwriter_mail['email'];
			 $email_add = implode(', ',$email_value);
			//die;
			//print_r($email_add); die(882);
			$insured_name = isset($result['insured_fname']) ? $result['insured_fname'] : '';
			$insured_name .= isset($result['insured_mname']) ? ' '.$result['insured_mname'] : '';
			$insured_name .= isset($result['insured_lname']) ? ' '. $result['insured_lname'] : '';
			$dba_name = isset($result['insured_dba']) ? $result['insured_dba'] : '';
			$insured_name = (str_replace(' ', '', $insured_name)) ? $insured_name : $dba_name;
			
				$email_setting  = array('mailtype'=>'html');

				$this->email->initialize($email_setting);

				$this->email->from($this->session->userdata('username'), $this->session->userdata('member_name'));

				//$this->email->subject('Quotation Request from '. $this->session->userdata('member_name'));
				$this->email->subject('Quote request: '. $insured_name);

				$this->email->message($mail_message);

                
				$this->email->to("$email_add");

				$this->email->cc("$cc_email");

				$mail_sent = $this->email->send();				
        
		}

 

		if($mail_sent) { 
//die(' die 911');
			return TRUE;

		}

		else { 
//die(' die 917');
			return FALSE;

		}

	}

	

	public function list_quote()

	{

		$limit=0;$total=0;

		$data['user'] = 'member';

		if ($this->session->userdata('underwriter_id')) {

			$data['user'] = 'underwriter';

		}

		//$data['quote1'] = $this->quote_model->get_quote1();

		
        
		$data['quote'] = $this->new_quote_model->get_quote();
		
	
		$data['object'] = $this;

		

		$config['base_url'] = base_url('administration/quotes');

		$config['total_rows'] = $total;

		$config['per_page'] = $limit;

		

		

		$this->pagination->initialize($config);

		$this->load->view('list_quote',$data);

	}

	public function sort_list_quote_by_status()

	{

		$status_re = $this->input->post('status');

		$status = '';

		$data['quote1'] = $this->quote_model->get_quote1();

		$data['quote'] = $this->quote_model->get_quote();

		$table_data = '';

		

		$table_data .= '<table id="underwriterList" width="60%" class="dataTable">

							<thead>

							  <tr>

								<th width="15%">Quote No.</th>

								<th width="15%">Date Submitted</th>

								<th width="15%">Agency Full Name/DBA</th>

								

								<th width="12%">Agency Phone Number</th>

								<th width="12%">Insured Full Name/DBA</th>

								 <th width="12%">Coverage Date</th>

								 <th>Status</th>

								<th width="30%">Quotes</th>

							  </tr>

							</thead>

						<tbody>';

		 $cnt= 0; krsort($data['quote1']); foreach($data['quote1'] as $row1)

		 {

			

			$cnt++;

			

				if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id')))

				{						

						/*if($row->bundle_status == '') { ?>Pending<?php } else { */ 

					if($row1->status == '') 

					{ 

						$status = 'Pending'; 

					} 

					else 

					{ 

						$status = $row1->status; 

					}  /*}*/ 

					 if($row1->is_manual)

					 { 

                      	$view = '<a class="btn btn-primary" href="'.base_url().'quote/quote/rate_by_quote2/'.$row1->quote_ref.'">Quote</a>';     

					 }

                     else

					 {	

                        $view = '<a class="btn btn-primary" href="'.base_url().'quote/quote/rate_by_quote1/'.$row1->quote_ref.'">Quote</a>  <a href="javascript:;" onclick="viewQuotePopup('.$row1->quote_id.')" class="btn btn">View Request</a>';

						

                     }

					 $cancel = '<a class="btn btn-danger" href="javascript:;" onclick="calcelQuote('.$row1->id.')">Cancel</a>';             

				} 

				else if($this->session->userdata('member_id'))

				{

					if($row1->status == '') 

					{ 

						$status = 'Pending'; 

					} 

					else 

					{ 

						$status = $row1->status; 

					} 

					if($row1->status != 'Released' && $row1->status != 'Accepted') 

					{ 

						$view = '-'; 

					} 

					else 

					{ 

						$view = '<a class="btn btn-primary" href="'.base_url().'ratesheet/view_quote_rate/'.$row1->bundle_id.'">Quote</a>';

					} 					

                       /* <a class="btn btn-primary" href="<?php echo base_url(); ?>quote/quote/rate_by_quote1/<?php echo $row[0]['quote_ref']; ?>">Versions</a>*/					

				}

				if($status_re!='')

				{

					if($status_re==$status)

					{

						$table_data .= '<tr>

								<td>'.$row1->quote_id.'</td>	

								<td>'.date("m/d/Y",strtotime($row1->creation_date)).'</td>							

								<td>'.get_agency_detail($row1->created_by, 'first_name').'</td>

								<td>'.get_agency_detail($row1->created_by, 'phone_number').'</td>

								<td>'.$row1->insured.'</td>								

								<td></td>

								<td>'.$status.'</td>

								<td>'.$view.' '.$cancel.'</td>					


							</tr>';

					}

				}

				else

				{

					$table_data .= '<tr>

								<td>'.$row1->quote_id.'</td>	

								<td>'.date("m/d/Y",strtotime($row1->creation_date)).'</td>							

								<td>'.get_agency_detail($row1->created_by, 'first_name').'</td>

								<td>'.get_agency_detail($row1->created_by, 'phone_number').'</td>

								<td>'.$row1->insured.'</td>								

								<td></td>

								<td>'.$status.'</td>

								<td>'.$view.' '.$cancel.'</td>				

							</tr>';

				}

				

		}

		//$view = '';

		$status = '';

		krsort($data['quote']); foreach($data['quote'] as $row)

		 {

			

			$cnt++;

			

				if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id')))

				{						

						/*if($row->bundle_status == '') { ?>Pending<?php } else { */

					if($row->bundle_status == '') 

					{ 

						$status = 'Pending'; 

					} 

					else 

					{ 

						$status = $row->bundle_status; 

					}  /*}*/ 

					/* if($row->is_manual)

					 { 

                      	$view = '<a class="btn btn-primary" href="'.base_url().'quote/quote/rate_by_quote2/'.$row->quote_ref.'">View Quote</a>';     

					 }

                     else

					 {	*/

					 	if(get_ref_id($row->quote_id))

						{

                        	$view = '<a class="btn btn-primary" href="'.base_url().'quote/quote/rate_by_quote1/'.get_ref_id($row->quote_id).'">Quote</a> ';

						}

						else

						{

							$view = '<a class="btn btn-primary" href="'.base_url('ratesheet/generate_ratesheet/'.$row->quote_id.'').'">Rate Quote Req.</a>';

						}

						$view .=' <a href="javascript:;" onclick="viewQuotePopup('.$row->quote_id.')" class="btn btn">View Request</a>';

						

                  /*   }*/

					 $cancel = '<a class="btn btn-danger" href="javascript:;" onclick="calcelQuote('.$row->quote_id.')">Cancel</a>';             

				} 

				else if($this->session->userdata('member_id'))

				{

					if($row->bundle_status == '') 

					{ 

						$status = 'Pending'; 

					} 

					else 

					{ 

						$status = $row->bundle_status; 

					} 

					if($row->bundle_status != 'Released' && $row->bundle_status != 'Accepted') 

					{ 

						$view = '-'; 

					} 

					else 

					{ 

						

						$view = '<a class="btn btn-primary" href="'.base_url().'ratesheet/view_quote_rate/'.$row->bundle_id.'">Quote</a>';

					} 					

                       /* <a class="btn btn-primary" href="<?php echo base_url(); ?>quote/quote/rate_by_quote1/<?php echo $row[0]['quote_ref']; ?>">Versions</a>*/					

				}

				if($status_re!='')

				{

					if($status_re==$status)

					{

						$table_data .= '<tr>

								<td>'.$row->quote_id.'</td>

								

								<td>'.date("m/d/Y",strtotime($row->date_added)).'</td>

								<td>'.get_agency_detail($row->requester_id, 'first_name').'</td>

								<td>'.get_agency_detail($row->requester_id, 'phone_number').'</td>

								<td>'.$row->contact_name.' '. $row->contact_middle_name.' '. $row->contact_last_name.'</td>

								<td>'.date("m/d/Y",strtotime($row->coverage_date)).'</td>

								<td>'.$status.'</td>

								<td>'.$view.' '.$cancel.'</td>					

							</tr>';

					}

				}

				else

				{

					$table_data .= '<tr>

								<td>'.$row->quote_id.'</td>

								

								<td>'.date("m/d/Y",strtotime($row->date_added)).'</td>

								<td>'.get_agency_detail($row->requester_id, 'first_name').'</td>

								<td>'.get_agency_detail($row->requester_id, 'phone_number').'</td>

								<td>'.$row->contact_name.' '. $row->contact_middle_name.' '. $row->contact_last_name.'</td>

								<td>'.date("m/d/Y",strtotime($row->coverage_date)).'</td>

								<td>'.$status.'</td>

								<td>'.$view.' '.$cancel.'</td>						

							</tr>';

				}

				

		}

		

		 $table_data .= ' </tbody>

 					 	 </table>';

 	 $data['table_data'] = $table_data;

	 

		echo json_encode($data);

		

	}

	public function rate_by_quote1()

	{

			$limit = 25;

			$total = $this->underwriter_model->count_underwriter(); 

			

			$data['admins'] = $this->quote_model->get_quote();

			$data['object'] = $this;

			$quote_id = $this->uri->segment(4);

			

			$data['rows'] = $this->quote_model->get_bundle_by_quote1($quote_id);

			

			

			$quote_id = isset($data['rows'][0]) ? $data['rows'][0] : '';

			$config['base_url'] = base_url('administration/quotes');

			$config['total_rows'] = $total;

			$config['per_page'] = $limit;

			

			$data['total'] = $total;

			$this->pagination->initialize($config);



			$this->load->view('quote/rate_by_quote', $data);

	}

	

	/*By Ashvin Patel 9/may/2014*/

	public function rate_by_quote2()

	{

			$limit = 25;

			$total = $this->underwriter_model->count_underwriter(); 

			

			$data['admins'] = $this->quote_model->get_quote();

			$data['object'] = $this;

			$quote_id = $this->uri->segment(4);

			

			$data['rows'] = $this->quote_model->get_bundle_by_quote2($quote_id);

			

			//print_r($data['rows']);

			$quote_id = isset($data['rows'][0]) ? $data['rows'][0] : '';

			$config['base_url'] = base_url('administration/quotes');

			$config['total_rows'] = $total;

			$config['per_page'] = $limit;

			

			$data['total'] = $total;

			$this->pagination->initialize($config);



			$this->load->view('quote/rate_by_quote_maual', $data);

	}

	/*By Ashvin Patel 9/may/2014*/

	

	/*Ashvin Patel 7/may/2014*/

	public function cancel_quote($id)

	{

		//echo $id;
		$sql = "update rq_rqf_quote set bundle_status = 'Cancelled' where quote_id=".$id;
		$query = $this->db->query($sql);
			
		//$sql = "update rq_rqf_quote_bundle set status = 'Cancel' where id=".$id;
		//$query = $this->db->query($sql);
		//$this->db->delete('rq_rqf_quote_bundle', array('id' => $id));

		//$this->db->delete('rq_ratesheet', array('id' => $id));

		//echo $this->db->last_query();

		redirect(base_url('list_quote'));

	}

	/*Ashvin Patel 7/may/2014*/

	

	/*Ashvin patel 28/april/2014*/

	public function quote_status()

	{

		$limit=0;$total=0;

		$data['user'] = 'member';

		if ($this->session->userdata('underwriter_id')) {

			$data['user'] = 'underwriter';


		}

		$data['quote'] = $this->quote_model->get_quote();


//print_r($data['quote']);
//die;
		/*Ashvin Patel 8/may/2014*/

		$data['manual_quote'] = $this->quote_model->get_manual_quote();

		/*Ashvin Patel 8/may/2014*/

		$data['object'] = $this;

		//print_r($data['quote']);

		$config['base_url'] = base_url('administration/quotes');

		$config['total_rows'] = $total;

		$config['per_page'] = $limit;

		

		

		$this->pagination->initialize($config);

		$this->load->view('quote_status',$data);

	}

	/*Ashvin patel 28/april/2014*/

	

	/**By Ashvin Patel 10/may/2014**/

	public function sort_by_status()

	{	

		

			$status_re = $this->input->post('status');

			$data['quote'] = $this->quote_model->get_quote();

			

			$data['manual_quote'] = $this->quote_model->get_manual_quote();

			//print_r($data['rows']);		



			//$this->load->view('administration/quotes', $data);

			$table_data = '';

			

			$table_data .= ' <table id="underwriterList" width="100%" class="dataTable">

								<thead>

								 <tr>

									   <th class="center" width="10%">Quote Number</th>

										<th width="12%">Date Submitted</th>

										<th width="15%">Agency Full Name/DBA</th>

										<th width="15%">Agency Phone Number</th>

										

										<th width="12%">Insured Full Name/DBA</th>

										<th width="12%">Coverage Date</th>

										<th>Status</th>

										<th width="30%">Action</th>

									  </tr>

								</thead>

							<tbody id="tabe_body">';

			

		$cnt= 0;

		if(!empty($data['quote']))  

		{

		 krsort($data['quote']); foreach($data['quote'] as $row)

		{

			

			 $ids[] = $row->quote_id;

				

				if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id'))){	

					if($row->bundle_status == ''){

						$status = 'Pending';

						//$action = '<a href="javascript:;" onclick="viewQuotePopup('.$row->quote_id.')" class="btn btn">View Request</a>';

					} 

					else{ 

						$status = $row->bundle_status; 

					} 

					if($row->bundle_id == 0){ 

						$action = '<a href="javascript:;" onclick="viewQuotePopup('.$row->quote_id.')" class="btn btn">View Request</a>';

					 }

					 else{ 

					 	$action = '<a href="'.base_url().'ratesheet/view_quote_rate1/'.$row->bundle_id.'" class="btn btn-primary">View Ratesheet</a><a href="javascript:;" onclick="viewQuotePopup('.$row->quote_id.')" class="btn btn">View Request</a>';

					} 

				}

				else if($this->session->userdata('member_id'))

				{						

				 if($row->bundle_status == ''){ 

					 $status = 'Pending';

				 }

				 else{

						if($row->bundle_status=='Released'){

							 $cd1 = strtotime($row->update_dt);

							 $cd2 = strtotime(date("Y-m-d"));

							 $cd3 = ($cd2-$cd1)/(60*60*24);

							 if($cd3>=30){

								$status =  'Expired';								

							 }

							 else{

								//$status = '';

								$status = $row->bundle_status; 

							}	

						}

						else{

							 if($row->perma_reject=='Reject'){ $status ='Pending';} else {$status = $row->bundle_status;}

						}

						  

				}

				 if($row->bundle_status != 'Released' && $row->bundle_status != 'Accepted') {							

							$action = '-';

				 } 

				 else{

					if($row->bundle_status!=''){

						if($status!='Expired')

						{

							$action = '<a class="btn btn-primary" href="'.base_url().'ratesheet/view_quote_rate1/'.$row->bundle_id.'">View</a>&nbsp;</a>';

						 }

						 else

						 {

							$action =  '-';

						 }

				 	}

					else {

						$action =  '-';

					}

				} 

					

			}

				if($status_re!='')

				{

					

					if($status_re==$status)

					{$cnt++;

						$table_data .= '<tr>

							<td>'.$row->quote_id.'</td>							

							<td>'.date("m/d/Y",strtotime($row->date_added)).'</td>							

							<td>'.get_agency_detail($row->requester_id, 'first_name').'</td>

							<td>'.get_agency_detail($row->requester_id, 'phone_number').'</td>

							<td>'.$row->contact_name.' '. $row->contact_middle_name.' '. $row->contact_last_name.'</td>

							<td>'.date("m/d/Y",strtotime($row->coverage_date)).'</td>	

							<td>'.$status.'</td>						

							<td>'.$action.'</td>

						   

						</tr>';

					}

				}

				else

				{

					$cnt++;

					$table_data .= '<tr>

							<td>'.$row->quote_id.'</td>							

							<td>'.date("m/d/Y",strtotime($row->date_added)).'</td>							

							<td>'.get_agency_detail($row->requester_id, 'first_name').'</td>

							<td>'.get_agency_detail($row->requester_id, 'phone_number').'</td>

							<td>'.$row->contact_name.' '. $row->contact_middle_name.' '. $row->contact_last_name.'</td>

							<td>'.date("m/d/Y",strtotime($row->coverage_date)).'</td>		

							<td>'.$status.'</td>					

							<td>'.$action.'</td>

						</tr>';

				}

				

		}

		}

		

		$action = '';

		if(!empty($data['manual_quote']))

		{

		 foreach($data['manual_quote'] as $quote1) 

		 { 

		 	 /*$ids[] = $row[0]['quote_id'];*/

      

		/*	if(isset($row[0]['is_manual']))

			{*/

					

						if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id')))

						{						

							if($quote1->status == '')

							{ 						

								$status = 'Pending'; 

							} 

							else 

							{ 

								$status =  $quote1->status; 

							} 

											

						}  

						else if($this->session->userdata('member_id'))

						{					

							if($quote1->status == '') 

							{ 

								$status = 'Pending'; 

							} 

							else

							{ 

								$status =  $quote1->status;  

							} 

						//echo	$row->bundle_status;

						 if($row->status != 'Released' && $row->status != 'Accepted') 

						 { 

							//echo $row->update_dt;

							$action = '<a class="btn btn-primary" href="'.base_url().'ratesheet/view_quote_rate1/'.$quote1->id.'">View</a>&nbsp;</a>';

						 } 

						 else 

						{

							if($row->bundle_status!='')

							{

								if($status!='Expired')

								{

									$action = '<a class="btn btn-primary" href="'.base_url().'ratesheet/view_quote_rate1/'.$row->bundle_id.'">View</a>&nbsp;</a>';

								 }

								 else

								 {

									$action = '-';

								 }

							}

							 else

								 {

									$action = '-';

								 }

						 } 

					}

					

						if($status_re!='')

						{

							if($status_re==$status)

							{

								$cnt++;

								$table_data .= '<tr>

												<td>'.$quote1->quote_id.'</td>

												<td>'.date("m/d/Y",strtotime($quote1->creation_date)).'</td>

												<td>'.get_agency_detail($quote1->producer_email, 'first_name', 'email').'</td>

												<td>'.get_agency_detail($quote1->producer_email, 'phone_number', 'email').'</td>

												<td>'.$quote1->insured.'</td>

												<td ></td>

												<td >'.$status.'</td>

												<td >'.$action.'</td>

										  </tr>';

							}

						}

						else{

							$cnt++;

							$table_data .= '<tr>

												<td>'.$quote1->quote_id.'</td>

												<td>'.date("m/d/Y",strtotime($quote1->creation_date)).'</td>

												<td>'.get_agency_detail($quote1->producer_email, 'first_name', 'email').'</td>

												<td>'.get_agency_detail($quote1->producer_email, 'phone_number', 'email').'</td>

												<td>'.$quote1->insured.'</td>

												<td ></td>

												<td >'.$status.'</td>

												<td >'.$action.'</td>

										  </tr>';

						}

			 		/*} */

		  		} 

		 	} 

		 

        $table_data .= ' </tbody>

 					 	 </table>';

  $data['table_data'] = $table_data;

		echo json_encode($data);

	}

	/**By Ashvin Patel 10/may/2014**/

	
	
		public function sort_by_status1()

	{	

		

		  $status_re = $this->input->post('status');
   
			$data['quote'] = $this->quote_model->get_quote();

			

			$data['manual_quote'] = $this->quote_model->get_manual_quote();

			//print_r($status_re);		



			//$this->load->view('administration/quotes', $data);

			$table_data = '';

			

			$table_data .= ' <table id="underwriterList" width="100%" class="dataTable">

								<thead>

								 <tr>

									   <th class="center" width="10%">Quote Number</th>

										<th width="12%">Date Submitted</th>

										<th width="15%">Agency Full Name/DBA</th>

										<th width="15%">Agency Phone Number</th>

										

										<th width="12%">Insured Full Name/DBA</th>

										<th width="12%">Coverage Date</th>

										<th>Status</th>

										<th width="30%">Action</th>

									  </tr>

								</thead>

							<tbody id="tabe_body">';

			

		$cnt= 0;

		if(!empty($data['quote']))  

		{

		 krsort($data['quote']); foreach($data['quote'] as $row)

		{

			

			 $ids[] = $row->quote_id;

				

				if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id'))){	

					if($row->bundle_status == ''){

						$status = 'Pending';

						//$action = '<a href="javascript:;" onclick="viewQuotePopup('.$row->quote_id.')" class="btn btn">View Request</a>';

					} 

					else{ 

						$status = $row->bundle_status; 

					} 

					if($row->bundle_id == 0){ 

						$action = '<a href="javascript:;" onclick="viewQuotePopup('.$row->quote_id.')" class="btn btn">View Request</a>';

					 }

					 else{ 

					 	$action = '<a href="'.base_url().'ratesheet/view_quote_rate1/'.$row->bundle_id.'" class="btn btn-primary">View Ratesheet</a><a href="javascript:;" onclick="viewQuotePopup('.$row->quote_id.')" class="btn btn">View Request</a>';

					} 

				}

				else if($this->session->userdata('member_id'))

				{						

				 if($row->bundle_status == ''){ 

					 $status = 'Pending';

				 }

				 else{

						if($row->bundle_status=='Released'){

							 $cd1 = strtotime($row->update_dt);

							 $cd2 = strtotime(date("Y-m-d"));

							 $cd3 = ($cd2-$cd1)/(60*60*24);

							 if($cd3>=30){

								$status =  'Expired';								

							 }

							 else{

								//$status = '';

								$status = $row->bundle_status; 

							}	

						}

						else{

							 if($row->perma_reject=='Reject'){ $status ='Pending';} else {$status = $row->bundle_status;}

						}

						  

				}

				 if($row->bundle_status != 'Released' && $row->bundle_status != 'Accepted') {							

							$action = '-';

				 } 

				 else{

					if($row->bundle_status!=''){

						if($status!='Expired')

						{

							$action = '<a class="btn btn-primary" href="'.base_url().'ratesheet/view_quote_rate1/'.$row->bundle_id.'">View</a>&nbsp;</a>';

						 }

						 else

						 {

							$action =  '-';

						 }

				 	}

					else {

						$action =  '-';

					}

				} 

					

			}
			
			$action1='';
						if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id'))){	
					
					if($row->bundle_id == 0) { } else {
					 $action1='<a href="'. base_url().'ratesheet/view_quote_rate1/'.$row->bundle_id.'" class="btn btn-primary">View Ratesheet</a>';
                 	$action1.='<a href="javascript:;" onclick="viewQuotePopup('.$row->quote_id.')" class="btn btn">View Request</a>';
                        
					
					}  } else if( $this->session->userdata('member_id')){	
					
						
											
						//echo	$row->bundle_status;
						 if($row->bundle_status != 'Released' && $row->bundle_status != 'Accepted') { 
							//echo $row->update_dt;
							echo '';
						} 
						else 
						{
							if($row->bundle_status!='')
							{
								if($status!='Expired')
								{
						$action1.='<a class="btn btn-primary" href="'.base_url().'ratesheet/view_quote_rate1/'. $row->bundle_id.'">View Quote</a>'; 
								 }
								 else
								 {
									echo '';
								 }
							}
							 else
								 {
									echo '';
								 }
						 } 
                         $action1.= '<a href="javascript:;" onclick="viewQuotePopup('. $row->quote_id.')" class="btn btn">View Request</a>';
                         $action1.=   '<a class="btn btn-primary" href="'. base_url().'quote/quote_view_edit/'. $row->quote_id.'">Edit</a>';
                           if($row->main_quote_id){ 
                           $action1.=   '<a class="btn btn-primary" target="_blank" href="'. base_url('quote/get_revision_pdf/'.$row->quote_id.'').'">Get Revision</a>';
                      
					           }
					
							 $action2= '<a href="javascript:;" onclick="viewQuotePopup('. $row->quote_id.')" class="btn btn">View Request</a>'	;		 

							}

						
					
			

				if($status_re!='')

				{

					//console.log($row->status);

					if($status_re==$row->status)
                        
					{  $cnt++;

						$table_data .= '<tr>

							<td>'.$row->quote_id.'</td>							

							<td>'.date("m/d/Y",strtotime($row->date_added)).'</td>							

							<td>'.get_agency_detail($row->requester_id, 'first_name').'</td>

							<td>'.get_agency_detail($row->requester_id, 'phone_number').'</td>

							<td>'.$row->contact_name.' '. $row->contact_middle_name.' '. $row->contact_last_name.'</td>

							<td>'.date("m/d/Y",strtotime($row->coverage_date)).'</td>	

							<td>'.$status.'</td>						

							<td>'.$action2.'</td>

						   

						</tr>';

					}

				}

				else

				{
                  if($row->status!="deactivate"){
					$cnt++;

					$table_data .= '<tr>

							<td>'.$row->quote_id.'</td>							

							<td>'.date("m/d/Y",strtotime($row->date_added)).'</td>							

							<td>'.get_agency_detail($row->requester_id, 'first_name').'</td>

							<td>'.get_agency_detail($row->requester_id, 'phone_number').'</td>

							<td>'.$row->contact_name.' '. $row->contact_middle_name.' '. $row->contact_last_name.'</td>

							<td>'.date("m/d/Y",strtotime($row->coverage_date)).'</td>		

							<td>'.$status.'</td>					

							<td>'.$action1.'</td>

						</tr>';
                      }
				}

				

		}

		}

		

		$action = '';

		if(!empty($data['manual_quote']))

		{

		 foreach($data['manual_quote'] as $quote1) 

		 { 

		 	 /*$ids[] = $row[0]['quote_id'];*/

      

		/*	if(isset($row[0]['is_manual']))

			{*/

					

						if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id')))

						{						

							if($quote1->status == '')

							{ 						

								$status = 'Pending'; 

							} 

							else 

							{ 

								$status =  $quote1->status; 

							} 

											

						}  

						else if($this->session->userdata('member_id'))

						{					

							if($quote1->status == '') 

							{ 

								$status = 'Pending'; 

							} 

							else

							{ 

								$status =  $quote1->status;  

							} 

						//echo	$row->bundle_status;

						 if($row->status != 'Released' && $row->status != 'Accepted') 

						 { 

							//echo $row->update_dt;

							$action = '<a class="btn btn-primary" href="'.base_url().'ratesheet/view_quote_rate1/'.$quote1->id.'">View</a>&nbsp;</a>';

						 } 

						 else 

						{

							if($row->bundle_status!='')

							{

								if($status!='Expired')

								{

									$action = '<a class="btn btn-primary" href="'.base_url().'ratesheet/view_quote_rate1/'.$row->bundle_id.'">View</a>&nbsp;</a>';

								 }

								 else

								 {

									$action = '-';

								 }

							}

							 else

								 {

									$action = '-';

								 }

						 } 

					}

					
					
					
					
					

						if($status_re!='')

						{

							if($status_re==$status)

							{

								$cnt++;

								$table_data .= '<tr>

												<td>'.$quote1->quote_id.'</td>

												<td>'.date("m/d/Y",strtotime($quote1->creation_date)).'</td>

												<td>'.get_agency_detail($quote1->producer_email, 'first_name', 'email').'</td>

												<td>'.get_agency_detail($quote1->producer_email, 'phone_number', 'email').'</td>

												<td>'.$quote1->insured.'</td>

												<td ></td>

												<td >'.$status.'</td>

												<td >'.$action.'</td>

										  </tr>';

							}

						}

						else{

							$cnt++;

							$table_data .= '<tr>

												<td>'.$quote1->quote_id.'</td>

												<td>'.date("m/d/Y",strtotime($quote1->creation_date)).'</td>

												<td>'.get_agency_detail($quote1->producer_email, 'first_name', 'email').'</td>

												<td>'.get_agency_detail($quote1->producer_email, 'phone_number', 'email').'</td>

												<td>'.$quote1->insured.'</td>

												<td ></td>

												<td >'.$status.'</td>

												<td >'.$action.'</td>

										  </tr>';

						}

			 		/*} */

		  		} 

		 	} 

		 

        $table_data .= ' </tbody>

 					 	 </table>';

      $data['table_data'] = $table_data;

		echo json_encode($data);

	}
	

	public function list_assigned_quote()

	{

		//if(restrict())

		$total = 2;

		$limit = 10;

		$data['user'] = 'member';

		if ($this->session->userdata('underwriter_id')) {

			$data['user'] = 'underwriter';

		}

		$data['quote'] = $this->quote_model->get_quote();

		

		$data['object'] = $this;

		

		$config['base_url'] = base_url('administration/quotes');

		$config['total_rows'] = $total;

		$config['per_page'] = $limit;

		

		$data['total'] = $total;

		$this->pagination->initialize($config);

		

		//print_r($data['quote']);

		$this->load->view('list_assigned_quote',$data);

	}

	public function changeStatus()

	{

		$action = $this->input->post('action');

		$id = $this->input->post('id');

		

		$this->db->update('rq_rqf_quote', array('perma_reject' => 'Reject'), array('quote_id' => $id));

		echo $this->db->affected_rows();

	}

	public function ajax_quote_list($uType='')

	{

		

		if ($uType=='underwriter')

		{

			$this->datatables

			->select("quote_id,contact_name,rq_rqf_quote.email,phone_no,name,DATE_FORMAT(coverage_date, '%d-%M-%Y'),rq_rqf_quote.status", FALSE)

		 	->from('rq_rqf_quote')

		 	->join('rq_underwriter', 'rq_underwriter.id=rq_rqf_quote.requester_id')

			->where('rq_rqf_quote.requester_id',$this->session->userdata('underwriter_id'));

			echo $this->datatables->generate();

		}

		else

		{

			$this->datatables

			->select("quote_id,contact_name,rq_rqf_quote.email,phone_no,first_name,DATE_FORMAT(coverage_date, '%d-%M-%Y'),rq_rqf_quote.status,rq_rqf_quote.bundle_id,rq_rqf_quote.bundle_status", FALSE)

		 	->from('rq_rqf_quote')

		 	->join('rq_members', 'rq_members.id=rq_rqf_quote.requester_id')

			->where('rq_rqf_quote.requester_id',$this->session->userdata('member_id'));

			echo $this->datatables->generate();

		}

	}

	

	public function ajax_assigned_quote()

	{

		$id = $this->session->userdata('underwriter_id');

		$where = "FIND_IN_SET('".$id."', assigned_underwriter)";

		$this->datatables

		->select("quote_id,contact_name,rq_rqf_quote.email,phone_no,name,DATE_FORMAT(coverage_date, '%d-%M-%Y'),rq_rqf_quote.status,rq_rqf_quote.bundle_id,rq_rqf_quote.bundle_status", FALSE)

	 	->from('rq_rqf_quote')

	 	->join('rq_underwriter', 'rq_underwriter.id=rq_rqf_quote.requester_id')

		//->where('rq_rqf_quote.requester_id',$this->session->userdata('underwriter_id'));

		->where($where);

		echo $this->datatables->generate();

	}

	

	public function response(){
        $data['request_id']=$this->new_quote_model->get_quote_id();
		$this->load->view('quote_success',$data);

	}
	
public function quote_view_edit($quote_id='')

	{
		$data['quote'] = $this->quote_model->get_quote($quote_id);
		
		$data['insured'] = $this->quote_model->get_insured_info($quote_id);
		
		//print_r($data['insured']); echo $insured123;die;
		
		$insured= $this->quote_model->get_insured_info($quote_id);
		
		$insured123 = $insured->nature_business;
		
		$insured12 = $insured->commodities_haulted;
		//echo $insured12;die;
		
		$data['nature_of_business'] = $this->nature_of_business($insured123);
		
		$data['commodities_haulted'] = $this->commodities_haulted($insured12);

		$data['losses'] = $this->quote_model->get_losses($quote_id);
		

		$data['tractors'] = $this->quote_model->get_all_vehicle($quote_id, 'TRACTOR');

		$data['trailers'] = $this->quote_model->get_all_vehicle($quote_id, 'TRAILER');
		
 // print_r($data['trailers']);
		$data['trucks'] = $this->quote_model->get_all_vehicle($quote_id, 'TRUCK');
		//print_r($data['trucks']);
	//	die;

		$data['drivers'] = $this->quote_model->get_all_drivers($quote_id);

		$data['owners'] = $this->quote_model->get_all_owner($quote_id);
		
		$catlog_data = $this->quote_model->get_catlog_options(25);
		if(!empty($data['trailers'])){
			for($i=0;$i<count($data['trailers']);$i++){
				
				$data['truck_trailer_type_select']= $this->trailer_type_select1();
		        $data['truck_make_model_trailer']= $this->trailer_model1();
				$data['trailer_type_select'] = $this->trailer_type_select();
		        $data['make_model_trailer'] = $this->trailer_model();
		$data['trailer_type_select1'][] = $this->trailer_type_select($data['trailers'][$i]->type_of_trailer);
		$data['make_model_trailer1'][] = $this->trailer_model($data['trailers'][$i]->make_model);
		
		$data['truck_trailer_type_select1'][] = $this->trailer_type_select1($data['trailers'][$i]->type_of_trailer);
		$data['truck_make_model_trailer1'][] = $this->trailer_model1($data['trailers'][$i]->make_model);
			}
		}else{
			    $data['truck_trailer_type_select']= $this->trailer_type_select1();
		        $data['truck_make_model_trailer']= $this->trailer_model1();
				$data['trailer_type_select'] = $this->trailer_type_select();
		        $data['make_model_trailer'] = $this->trailer_model();
			
			}
		
		if(!empty($data['tractors'])){
			for($i=0;$i<count($data['tractors']);$i++){
		$data['make_model_tractor'][] = $this->make_model('tractor',$data['tractors'][$i]->make_model);
		$data['tractor_number_of_axles'][] = $this->number_of_axles('tractor',$data['tractors'][$i]->number_of_axles);
			}
		}else{
			$data['make_model_tractor'] = $this->make_model('tractor');
		$data['tractor_number_of_axles'] = $this->number_of_axles('tractor');
			
			}
		
		
		if(!empty($data['trucks'])){
		for($i=0;$i<count($data['trucks']);$i++){

		$data['make_model_truck'][] = $this->make_model('truck',$data['trucks'][$i]->make_model);
		
		$data['truck_number_of_axles'][] = $this->number_of_axles('truck',$data['trucks'][$i]->number_of_axles);
		 }
		}else{
			
			
		$data['make_model_truck'] = $this->make_model('truck');
		
		$data['truck_number_of_axles'] = $this->number_of_axles('truck');
			
			}
	
		
		$data['fillings'] = $this->quote_model->get_all_fillings($quote_id);
		
		$all_fillings = $this->quote_model->get_all_fillings($quote_id);		
		
		$get_fillings1 = isset($all_fillings[0]->filings_type);
		
		
		foreach($all_fillings as $filling_type){
		$filling_type_show[] = $filling_type->filings_type;
		}
		//echo "<pre>"; print_r($filling_type_show); die;
		if(!empty($filling_type_show)){
		$data['filing_type_show'] = $this->filing_type_show($filling_type_show);
		}else{
		$data['filing_type_show'] = $this->filing_type_show();	
			}
		//print_r($data['filing_type_show']);die;
		//$data = '';

		$this->load->view('quote_view', $data);

	}
	
	public function ajax_change_status() {
		$status['status']=$this->input->post('status');
		$query=$this->db->update('rq_rqf_quote', $status, array('quote_id'=>$this->input->post('id')));
		if($this->db->affected_rows()==1)
		{
			echo "done";
		}
	}
	
	/**By Ashvin Patel 13/may/2014**/
	public function QuoteView($quote_id=''){

		$data['quote'] = $this->quote_model->get_quote($quote_id);
	
		if(isset($data['quote'][0]->quote_id))
	     $parent_id = ($data['quote'][0]->main_quote_id) ? $data['quote'][0]->main_quote_id : $data['quote'][0]->quote_id;
			
		$data['quote_id'] = isset($data['quote'][0]->quote_id) ? $data['quote'][0]->quote_id : '';
		if($parent_id){
		 $previous_quote_ids = $this->get_all_related_quote_ids($parent_id);
		 $data['previous_quote_ids'] = $previous_quote_ids;	
		 
		 $data['losses_liability_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_liability');
		 $data['losses_pd_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_pd');
		 $data['losses_cargo_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_cargo');
		 $data['losses_other1_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_other1_specify_other');
		 $data['losses_other2_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_other2_specify_other');
		 	
		}
		$fields = array('contact_name', 'contact_middle_name', 'contact_last_name');
		$data['logs'] = getModificationLog('quote', $parent_id, $fields, 0);
		
		$data['insured'] = $this->quote_model->get_insured_info($quote_id);
		$fields = array('insured_fname', 'insured_mname', 'insured_lname', 'insured_dba', 'insured_telephone', 'insured_garaging_city', 'insured_state', 'insured_zip', 'nature_business', 'commodities_haulted', 'year_in_business', 'insured_email', 'Comments_request');
	    $data['insured_logs'] = getModificationLog('quote_insured', $parent_id, $fields, 0);
	  
		$data['losses'] = $this->quote_model->get_losses($quote_id);
		$data['losses_liability'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_liability');
		$data['losses_pd'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_pd');
		$data['losses_cargo'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_cargo');
		$data['losses_other1'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_other1_specify_other');
		$data['losses_other2'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_other2_specify_other');
		
		$data['tractors'] = $this->quote_model->get_all_vehicle($quote_id, 'TRACTOR');
		$data['tractors_diff'] = $this->vehicle_row_diff($previous_quote_ids, $quote_id, 'TRACTOR');		 

		$data['trailers'] = $this->quote_model->get_all_vehicle($quote_id, 'TRAILER');		

		$data['trucks'] = $this->quote_model->get_all_vehicle($quote_id, 'TRUCK');
		$data['trcuk_diff'] = $this->vehicle_row_diff($previous_quote_ids, $quote_id, 'TRUCK');

		$data['drivers'] = $this->quote_model->get_all_drivers($quote_id);
		$data['driver_diff'] = $this->driver_row_diff($previous_quote_ids, $quote_id);		 

		$data['fillings'] = $this->quote_model->get_all_fillings($quote_id);
		$data['filling_diff'] = $this->filling_row_diff($previous_quote_ids, $quote_id);

		$data['owners'] = $this->quote_model->get_all_owner($quote_id);
		$data['owner_diff'] = $this->owner_row_diff($previous_quote_ids, $quote_id);	

		$this->load->view('quote_mail_view1', $data);

	}
	/**By Ashvin Patel 13/may/2014**/
	
	
	/** Start Download Link Qoute requet**/
	
	
		public function QuoteView_Download($quote_id=''){

		$data['quote'] = $this->quote_model->get_quote($quote_id);
	
		if(isset($data['quote'][0]->quote_id))
	     $parent_id = ($data['quote'][0]->main_quote_id) ? $data['quote'][0]->main_quote_id : $data['quote'][0]->quote_id;
			
		$data['quote_id'] = isset($data['quote'][0]->quote_id) ? $data['quote'][0]->quote_id : '';
		if($parent_id){
		 $previous_quote_ids = $this->get_all_related_quote_ids($parent_id);
		 $data['previous_quote_ids'] = $previous_quote_ids;	
		 
		 $data['losses_liability_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_liability');
		 $data['losses_pd_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_pd');
		 $data['losses_cargo_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_cargo');
		 $data['losses_other1_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_other1_specify_other');
		 $data['losses_other2_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_other2_specify_other');
		 	
		}
		$fields = array('contact_name', 'contact_middle_name', 'contact_last_name');
		$data['logs'] = getModificationLog('quote', $parent_id, $fields, 0);
		
		$data['insured'] = $this->quote_model->get_insured_info($quote_id);
		$fields = array('insured_fname', 'insured_mname', 'insured_lname', 'insured_dba', 'insured_telephone', 'insured_garaging_city', 'insured_state', 'insured_zip', 'nature_business', 'commodities_haulted', 'year_in_business', 'insured_email', 'Comments_request');
	    $data['insured_logs'] = getModificationLog('quote_insured', $parent_id, $fields, 0);
	  
		$data['losses'] = $this->quote_model->get_losses($quote_id);
		$data['losses_liability'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_liability');
		$data['losses_pd'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_pd');
		$data['losses_cargo'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_cargo');
		$data['losses_other1'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_other1_specify_other');
		$data['losses_other2'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_other2_specify_other');
		
		$data['tractors'] = $this->quote_model->get_all_vehicle($quote_id, 'TRACTOR');
		$data['tractors_diff'] = $this->vehicle_row_diff($previous_quote_ids, $quote_id, 'TRACTOR');		 

		$data['trailers'] = $this->quote_model->get_all_vehicle($quote_id, 'TRAILER');		

		$data['trucks'] = $this->quote_model->get_all_vehicle($quote_id, 'TRUCK');
		$data['trcuk_diff'] = $this->vehicle_row_diff($previous_quote_ids, $quote_id, 'TRUCK');

		$data['drivers'] = $this->quote_model->get_all_drivers($quote_id);
		$data['driver_diff'] = $this->driver_row_diff($previous_quote_ids, $quote_id);		 

		$data['fillings'] = $this->quote_model->get_all_fillings($quote_id);
		$data['filling_diff'] = $this->filling_row_diff($previous_quote_ids, $quote_id);

		$data['owners'] = $this->quote_model->get_all_owner($quote_id);
		$data['owner_diff'] = $this->owner_row_diff($previous_quote_ids, $quote_id);	


        $this->load->view('pdf/MPDF53/mpdf.php');	
		$mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0);
		
		$mpdf->SetDisplayMode('fullpage');
				 
	    $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		
		$stylesheet = base_url('css').'/style.css';
		$stylesheetcont = file_get_contents($stylesheet);
				
				
				
		$filecont = $this->load->view('quote_download',$data, TRUE);
		$stylesheetcont = file_get_contents($stylesheet);		
		
		$mpdf->WriteHTML($stylesheetcont,1);
		$mpdf->WriteHTML($filecont);
				 
		$mpdf->Output();
		

	}
	
	
	
	/** End Download Link Qoute requet**/
	
	
	
	
	//Ashvin revision controll pdf By Ashvin Patel
	public function get_revision_pdf($quote_id=''){
	  $data['quote'] = $this->quote_model->get_quote($quote_id);
	  $data['insured'] = $this->quote_model->get_insured_info($quote_id);
	  if(isset($data['quote'][0]->quote_id))
	  $parent_id = ($data['quote'][0]->main_quote_id) ? $data['quote'][0]->main_quote_id : $data['quote'][0]->quote_id;
	  
	  $data['quote_id'] = isset($data['quote'][0]->quote_id) ? $data['quote'][0]->quote_id : '';
	  $fields = array('contact_name', 'contact_middle_name', 'contact_last_name');
	  $data['logs'] = getModificationLog('quote', $parent_id, $fields, 0);
	  
	  $fields = array('insured_fname', 'insured_mname', 'insured_lname', 'insured_dba', 'insured_telephone', 'insured_garaging_city', 'insured_state', 'insured_zip', 'nature_business', 'commodities_haulted', 'year_in_business', 'insured_email', 'Comments_request');
	  $data['insured_logs'] = getModificationLog('quote_insured', $parent_id, $fields, 0);
	  
	  $parent_quote_id = isset($data['quote'][0]->main_quote_id) ? $data['quote'][0]->main_quote_id : $data['quote'][0]->quote_id;
	 
	  if($parent_quote_id){
		 // $previous_quote_ids[0] = $parent_quote_id;
		 $previous_quote_ids = $this->get_all_related_quote_ids($parent_quote_id);		  
		 $data['tractors_diff'] = $this->vehicle_row_diff($previous_quote_ids, $quote_id, 'TRACTOR');
		 $data['trcuk_diff'] = $this->vehicle_row_diff($previous_quote_ids, $quote_id, 'TRUCK');
		 $data['previous_quote_ids'] = $previous_quote_ids;
		 $data['driver_diff'] = $this->driver_row_diff($previous_quote_ids, $quote_id);
		 $data['owner_diff'] = $this->owner_row_diff($previous_quote_ids, $quote_id);
		 $data['filling_diff'] = $this->filling_row_diff($previous_quote_ids, $quote_id);
		 $data['losses_liability_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_liability');
		 $data['losses_pd_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_pd');
		 $data['losses_cargo_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_cargo');
		 $data['losses_other1_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_other1_specify_other');
		 $data['losses_other2_diff'] = $this->loss_row_diff($previous_quote_ids, $quote_id, 'losses_other2_specify_other');
	  }
	  
	  $data['tractors'] = $this->quote_model->get_all_vehicle($quote_id, 'TRACTOR');
	  $data['trucks'] = $this->quote_model->get_all_vehicle($quote_id, 'TRUCK');
	  $data['drivers'] = $this->quote_model->get_all_drivers($quote_id);
	  $data['owners'] = $this->quote_model->get_all_owner($quote_id);
	  $data['fillings'] = $this->quote_model->get_all_fillings($quote_id);
	  $data['losses_liability'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_liability');
	  $data['losses_pd'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_pd');
	  $data['losses_cargo'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_cargo');
	  $data['losses_other1'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_other1_specify_other');
	  $data['losses_other2'] = $this->quote_model->get_losses_by_type($quote_id, 'losses_other2_specify_other');
	  
	  $this->load->view('revision_pdf', $data);
	}    
	
	public function loss_row_diff($previous_quote_ids, $quote_id, $type='losses_liability'){		
	  $diff_data = '';
	  foreach($previous_quote_ids as $key => $previous_quote_id){	
		if($previous_quote_id!=$quote_id){
		  if(isset($previous_quote_ids[$key+1])){
			$previous_quote_id_1 = $previous_quote_ids[$key+1];
			$data_0 = $this->quote_model->get_losses_by_type($previous_quote_id, $type);
			$data_1 = $this->quote_model->get_losses_by_type($previous_quote_id_1, $type);
			foreach($data_0 as $key => $data){
			  $pre_data = isset($data_1[$key]) ? (array) $data_1[$key] : '';
			  $lat_data = (array) $data;
			  if(is_array($pre_data)&&is_array($lat_data)){				  
			  	$diff_data[$previous_quote_id][] = compare_array($lat_data, $pre_data);
			  }
			}
		  }

		}
	  }
	  return $diff_data;
	}
	public function filling_row_diff($previous_quote_ids, $quote_id){		
	  $diff_data = '';
	  foreach($previous_quote_ids as $key => $previous_quote_id){	
		if($previous_quote_id!=$quote_id){
		  if(isset($previous_quote_ids[$key+1])){
			$previous_quote_id_1 = $previous_quote_ids[$key+1];
			$data_0 = $this->quote_model->get_all_fillings($previous_quote_id);
			$data_1 = $this->quote_model->get_all_fillings($previous_quote_id_1);
			foreach($data_0 as $key => $data){
			  $pre_data = isset($data_1[$key]) ? (array) $data_1[$key] : '';
			  $lat_data = (array) $data;
			  if(is_array($pre_data)&&is_array($lat_data)){		
				$diff_data[$previous_quote_id][] = compare_array($lat_data, $pre_data);
			  }
			}
		  }
		}
	  }
	  //print_r($diff_data);
	  return $diff_data;
	}
	
	public function vehicle_row_diff($previous_quote_ids, $quote_id, $type='TRACTOR'){		
	  $diff_data = '';
	  foreach($previous_quote_ids as $key => $previous_quote_id){	
		if($previous_quote_id!=$quote_id){
		  if(isset($previous_quote_ids[$key+1])){
			$previous_quote_id_1 = $previous_quote_ids[$key+1];
			$tractors = $this->quote_model->get_all_vehicle($previous_quote_id, $type);
			$tractors_1 = $this->quote_model->get_all_vehicle($previous_quote_id_1, $type);
			foreach($tractors as $key => $tractor){
			  $pre_data = isset($tractors_1[$key]) ? (array) $tractors_1[$key] : '';
			  $lat_data = (array) $tractor;
			  if(is_array($pre_data)&&is_array($lat_data)){
			  	$diff_data[$previous_quote_id][] = compare_array($lat_data, $pre_data);
			  }
			}
		  }

		}
	  }
	  return $diff_data;
	}
	public function owner_row_diff($previous_quote_ids, $quote_id){		
	  $diff_data = '';
	  foreach($previous_quote_ids as $key => $previous_quote_id){	
		if($previous_quote_id!=$quote_id){
		  if(isset($previous_quote_ids[$key+1])){
			$previous_quote_id_1 = $previous_quote_ids[$key+1];
			$data_0 = $this->quote_model->get_all_owner($previous_quote_id);
			$data_1 = $this->quote_model->get_all_owner($previous_quote_id_1);
			foreach($data_0 as $key => $data){
			  $pre_data = isset($data_1[$key]) ? (array) $data_1[$key] : '';
			  $lat_data = (array) $data;
			  if(is_array($pre_data)&&is_array($lat_data)){	
				$diff_data[$previous_quote_id][] = compare_array($lat_data, $pre_data);
			  }
			}
		  }
		}
	  }
	  //print_r($diff_data);
	  return $diff_data;
	}
	public function driver_row_diff($previous_quote_ids, $quote_id){		
	  $diff_data = '';
	  foreach($previous_quote_ids as $key => $previous_quote_id){	
		if($previous_quote_id!=$quote_id){
		  if(isset($previous_quote_ids[$key+1])){
			$previous_quote_id_1 = $previous_quote_ids[$key+1];
			$data_0 = $this->quote_model->get_all_drivers($previous_quote_id);
			$data_1 = $this->quote_model->get_all_drivers($previous_quote_id_1);
			foreach($data_0 as $key => $data){
			  $pre_data =  isset($data_1[$key]) ? (array) $data_1[$key] : '';
			  $lat_data = (array) $data;
			  if(!empty($pre_data)&&!empty($lat_data)){
				$diff_data[$previous_quote_id][] = compare_array($lat_data, $pre_data);
			  }
			}
		  }
		}
	  }
	  //print_r($diff_data);
	  return $diff_data;
	}
	public function get_all_related_quote_ids($parent_quote_id=''){
		if($parent_quote_id){
			$this->db->select('quote_id');
			$this->db->where('main_quote_id', $parent_quote_id);
			$query = $this->db->get('rq_rqf_quote');	
			$result = $query->result();
			$ids[] = $parent_quote_id;
			if(is_array($result)){
				foreach($result as $value){
					$ids[] = $value->quote_id;	
				}
			}
			return $ids;
		}
	}
	
	//Losses version_diff
	
	public function losses_version_diff($v_id='', $v1_id='', $type='losses_liability'){
		$losses = $this->quote_model->get_losses_by_type($v_id, $type);
		$losses_1 = $this->quote_model->get_losses_by_type($v1_id, $type);
		$a = count($losses);
		$b = count($losses_1);
		$c = ($a>$b) ? $a : $b;
		$losse_data = '';
		for($i=0;$i<$a;$i++){
			if(isset($losses[$i])&&!isset($losses_1[$i])){
				$losse_data[$i] = $losses[$i];
			}
		}
		$data['losses'] = $losse_data;
		$data['l_type'] = $type;
		$this->load->view('losses_row_diff', $data);
	}
	
	//Vehicle version_diff
	public function vehicle_version_diff($v_id='', $v1_id='', $type='TRACTOR'){
		$vehicles = $this->quote_model->get_all_vehicle($v_id, $type);
		$vehicles_1 = $this->quote_model->get_all_vehicle($v1_id, $type);
		$a = count($vehicles);
		$b = count($vehicles_1);
		$c = ($a>$b) ? $a : $b;
		$vehicle_data = '';
		for($i=0;$i<$a;$i++){
			if(isset($vehicles[$i])&&!isset($vehicles_1[$i])){
				$vehicle_data[$i] = $vehicles[$i];
			}
		}
		$data['vehicles'] = $vehicle_data;
		$data['v_type'] = $type;
		$this->load->view('vehicle_row_diff', $data);
	}
	
	//driver_version_diff
	public function driver_version_diff($v_id='', $v1_id='', $type='Driver'){
		$drivers = $this->quote_model->get_all_drivers($v_id, $type);
		$drivers_1 = $this->quote_model->get_all_drivers($v1_id, $type);
		$a = count($drivers);
		$b = count($drivers_1);
		$c = ($a>$b) ? $a : $b;
		$driver_data = '';
		for($i=0;$i<$a;$i++){
			if(isset($drivers[$i])&&!isset($drivers_1[$i])){
				$driver_data[$i] = $drivers[$i];
			}
		}
		$data['drivers'] = $driver_data;
		$data['v_type'] = $type;
		$this->load->view('driver_row_diff', $data);
	}
	public function owner_version_diff($v_id='', $v1_id='', $type='Driver'){
		$owners = $this->quote_model->get_all_owner($v_id, $type);
		$owners_1 = $this->quote_model->get_all_owner($v1_id, $type);
		$a = count($owners);
		$b = count($owners_1);
		$c = ($a>$b) ? $a : $b;
		$owner_data = '';
		for($i=0;$i<$a;$i++){
			if(isset($owners[$i])&&!isset($owners_1[$i])){
				$owner_data[$i] = $owners[$i];
			}
		}
		$data['owners'] = $owner_data;
		$data['v_type'] = $type;
		$this->load->view('owner_row_diff', $data);
	}
	public function filing_version_diff($v_id='', $v1_id='', $type='Filing'){
		$filings = $this->quote_model->get_all_fillings($v_id, $type);
		$filings_1 = $this->quote_model->get_all_fillings($v1_id, $type);
		$a = count($filings);
		$b = count($filings_1);
		$c = ($a>$b) ? $a : $b;
		$filing_data = '';
		for($i=0;$i<$a;$i++){
			if(isset($filings[$i])&&!isset($filings_1[$i])){
				$filing_data[$i] = $filings[$i];
			}
		}
		$data['filings'] = $filing_data;
		$data['v_type'] = $type;
		$this->load->view('filing_row_diff', $data);
	}	
	//end of version controll
public function checkLicense()
	{
		
		
		//$expiration_date = array();
		echo BAPLICENSE;
		$broker_license = $this->api->get_response(BAPLICENSE);
		$brokerdata = $this->api->prepare_response($broker_license);	
	    
		foreach($brokerdata as $br_data){
			
			if($br_data->primary_email == $this->session->userdata('username')){				
				$state = $this->input->post('state');
				$curdate = date('Y-m-d');
				 $exp_data_bap = $br_data->expiration_date;
				
				if( (strtotime($exp_data_bap) >= strtotime($curdate)) && ($state == $br_data->state_issue_linc) )
				{
					echo 1;
				}
				else if($state != $br_data->state_issue_linc)
				{
					echo 2;	
				}
				else
				{
					echo 0;	
				}
				
			}
			
		}
	
		
		
		
		
		
		
		
	}
	
	public function update_db()
 {
  $this->db->select('nature_business');
  $this->db->from('rq_rqf_insured_info');
  $query = $this->db->get();
  $rows = $query->result_array(); 
  foreach($rows as $row)
  {
   
    {
				
   echo $row['nature_business']; 
   echo "<br>";
    }
  }
  
 }
 
public function chenge_pip_um_uim(){
	   $state=$this->input->post('state');
	   
	   $this->quote_model->change_value_catlog($state);
	  
	}
	
	


 
 public function mapping(){
	    $address=$this->input->post('address');		
		
		if(!empty($address)){
			
		     $data_arr = form_safe_json(json_encode($this->geocode($address)));
			echo $data_arr;
			
			}
	 
	 
	 }
public function geocode($address){
 
    // url encode the address
    $address = urlencode($address);
     
    // google map geocode api url
    $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address={$address}";
 
    // get the json response
    $resp_json = file_get_contents($url);
     
    // decode the json
    $resp = json_decode($resp_json, true);
 
    // response status will be 'OK', if able to geocode given address 
    if($resp['status']='OK'){
 
        // get the important data
        $lati = $resp['results'][0]['geometry']['location']['lat'];
        $longi = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];
         
        // verify if data is complete
        if($lati && $longi && $formatted_address){
         
            // put the data in the array
            $data_arr = array();            
             
            array_push(
                $data_arr, 
                    $lati, 
                    $longi, 
                    $formatted_address
                );
             
            return $data_arr;
             
        }else{
            return false;
        }
         
    }else{
        return false;
    }
}

public function vh_picture(){
	
	    $scope=$this->input->post('scope');
		$vh=$this->input->post('vh');
		if(!empty($scope)){
			
			$img=$this->new_quote_model->get_img_name($scope,'rq_rqf_quote_catlogs',$vh);	
			if(!empty($img)){		
			echo !empty($img[0]->module_name)? '<input type="hidden" name="img_name_make[]" value="'.$img[0]->module_name.'" /><img src="'.base_url().'uploads/file/'.$img[0]->module_name.'" />':'';
			}else{
			echo '<div style="color:red">Image Not Avaliable!</div>';
			}
		}
	 
	}
	public function Get_ids(){		
		$sql = "SELECT * FROM rq_rqf_quick_quote where pdf_flag != 1 ";
        $stmt = $this->db->query($sql);
		$users =$stmt->result();
		if($users){		
		
        foreach ($users as $user) {			    
		  if(!empty($user->id)){
           $broker_ids[]=$user->id;	   
		   }
		}
		print_r(implode(",",$broker_ids));	
	   }
		
	} 
    public function flag_set(){
        $table="rq_rqf_quick_quote";
		$pdf_name = $this->input->post('pdf_name');
		$id = $this->input->post('id');
        $data= array(
            "pdf_flag" => 1,
			"pdf_name" => $pdf_name,			
        );
        $this->db->where('id', trim($id));
        if($this->db->update($table, $data) !== FALSE){
            return TRUE;
        }		
    }
    public function Underwriter_email(){
      
        $id = trim($this->input->post('id'));
        $this->db->select('assigned_underwriter');  
        $this->db->where('id', $id);
      	$this->db->from('rq_rqf_quick_quote');
		$query = $this->db->get();			
		$result = $query->result();
		
		if(!empty($result)){
		$res=explode(',',$result[0]->assigned_underwriter);
		$r=array_unique($res, SORT_REGULAR);
		for($i=0;$i<count($r);$i++){
		$this->db->select('email');  
        $this->db->where('id',$r[$i]);
      	$this->db->from('rq_underwriter');
		$query = $this->db->get();			
		$results[] = $query->result_array();		
		}
		foreach($results as $ress){
	    	$email[]=$ress[0]['email'];
		}
	  echo implode(',',$email);
	  }
    }	
   
    public function getRelaesedids(){		
		$sql = "SELECT * FROM rq_rqf_quick_quote where uw_mail_flag != 1 AND status LIKE 'Released'";
        $stmt = $this->db->query($sql);
		$users =$stmt->result();
		if($users){		
		
        foreach ($users as $user) {			    
		  if(!empty($user->id)){
           $broker_ids[]=$user->id;	   
		   }
		}
		print_r(implode(",",$broker_ids));	
	   }
		
	}
	
	   public function flag_set_agency(){
        $table="rq_rqf_quick_quote";
		$pdf_name = $this->input->post('u_pdf_name');
		$id = $this->input->post('id');
        $data= array(
            "uw_mail_flag" => 1,
			"u_pdf_name" => $pdf_name,			
        );
        $this->db->where('id', trim($id));
        if($this->db->update($table, $data) !== FALSE){
            return TRUE;
        }		
    } 
      public function email_agency(){
        $id = trim($this->input->post('id'));
        $this->db->select('requester_id');  
        $this->db->where('id', $id);
      	$this->db->from('rq_rqf_quick_quote');
		$query = $this->db->get();			
		$result = $query->result();		
		if(!empty($result)){
		$res=$result[0]->requester_id;		
		$this->db->select('email');  
        $this->db->where('id',$res);
      	$this->db->from('rq_members');
		$query = $this->db->get();			
		$results = $query->result();		
		echo $results[0]->email;
	  
	  }	
    } 
	
	public function getZipcode(){
	   $city = ucwords(strtolower($this->input->post('city')));
       $state = trim($this->input->post('state'));
       $county = ucwords(strtolower($this->input->post('county')));	    	
	   $name = $this->input->post('name');
	   $attr = $this->input->post('attr'); 
	  	  
	    echo getZipCode($name,$attr,$city,$state,$county);
	 
	 
	}
	
	public function getcounty(){
	   $city = ucwords(strtolower($this->input->post('city')));
       $state = trim($this->input->post('state'));      	 
	   $name = $this->input->post('name');
	   $attr = $this->input->post('attr'); 
	   echo getCounty($name,$attr,$city,$state);	 
		
	}	
	public function data_dictionary(){
		
		$this->db->select('*');   
		$this->db->where('is_delete',0);
		$this->db->where('status',1);		   
      	$this->db->from('rq_rqf_data_dictionary');
		$query = $this->db->get();			
		$result = $query->result();	
		echo json_encode($result);
	}
	
 }




/* End of file welcome.php */

/* Location: ./application/controllers/welcome.php */