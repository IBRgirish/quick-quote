<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quote extends RQ_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
	public $file_upload_path = './uploads/quote_request/';
	
    public function __construct()
	{
		parent::__construct();
		if(!$this->require_login_user() && !$this->require_login_underwriter())
		{
			$base = base_url('login');
			redirect($base);
		}
		$this->load->model('quote_model');
		 
		
		// Your own constructor code
		 
		  
		
	}
	public function index()
	{
		$user_quoute_info = $this->quote_model->get_quote_data();
		$data['user_quote_data'] = $user_quoute_info;
		$data['trailer_type_select'] = $this->trailer_type_select();
		$data['nature_of_business'] = $this->nature_of_business();
		$data['commodities_haulted'] = $this->commodities_haulted();
		$data['make_model_tractor'] = $this->make_model('tractor');
		$data['make_model_truck'] = $this->make_model('truck');
		$data['make_model_trailer'] = $this->trailer_model();
		$data['truck_number_of_axles'] = $this->number_of_axles('truck');
		$data['tractor_number_of_axles'] = $this->number_of_axles('tractor');
		$data['filing_type_show'] = $this->filing_type_show();
		//print_r($data['make_model_truck']);
		$this->load->view('quote_view', $data);
	}

//ADDED BY khan 14 -oct 2014//
		//$data['filing_type_show'] = $this->filing_type_show();
	function filing_type_show() {
		$catlog ='';
		$catlog .='<select class="span12 filing_type" required="required" name="filing_type[]"  >';
		$catlog .='<option value="">Select</option>';
		$catlog_data = $this->quote_model->get_catlog_options(25);
		//print_r($catlog_data);die;
		if($catlog_data)
		{
			foreach ($catlog_data as $catlogData)
				$catlog .='<option value="'.$catlogData->catlog_value.'">'.$catlogData->catlog_value.'</option>';	
		}
		$catlog .='</select>';
			return $catlog;
    }
	//ADDED BY MAYANK 21 JULY//
	function make_model($type)
	{
		if($type == "tractor")
		{
			$catlog ='';
			$catlog .='<select type="text" id="make_model_vh" name="tractor_make_model_vh[]" class="span12" >';
			$catlog .='<option value="">Select</option>';
			$catlog_data = $this->quote_model->get_catlog_options('trac_truck_model');
			
			if($catlog_data)
			{ 	
				foreach ($catlog_data as $catlogData)
					$catlog .='<option value="'.$catlogData->catlog_value.'">'.$catlogData->catlog_value.'</option>';	
			}
			$catlog .='</select>';
		}
		else if($type == "truck")
		{
			$catlog ='';
			$catlog .='<select type="text" id="make_model_vh" name="make_model_vh[]" class="span12"  >';
			$catlog .='<option value="">Select</option>';
			$catlog_data = $this->quote_model->get_catlog_options('trac_truck_model');
			if($catlog_data)
			{
				foreach ($catlog_data as $catlogData)
					$catlog .='<option value="'.$catlogData->catlog_value.'">'.$catlogData->catlog_value.'</option>';	
			}
			$catlog .='</select>';
		}
			return $catlog;
	}
	
	
	function trailer_model() {
		$catlog ='';
		$catlog .='<select type="text" id="make_model_vh__SECID__" name="trailer_make_model_vh[]" class="span12">';
		$catlog .='<option value="">Select</option>';
		$catlog_data = $this->quote_model->get_catlog_options('trailer_model');
		if($catlog_data)
		{
			foreach ($catlog_data as $catlogData)
				$catlog .='<option value="'.$catlogData->catlog_value.'">'.$catlogData->catlog_value.'</option>';	
		}
		$catlog .='</select>';
			return $catlog;
    }
	
	 function number_of_axles($type='') {
               $catlog ='';
               $catlog .='<select type="text" id="'.$type.'_number_of_axles" name="'.$type.'_number_of_axles[]" class="span12">';
               $catlog .='<option value="">Select</option>';
               $catlog_data = $this->quote_model->get_catlog_options('number_of_axles');
               if($catlog_data)
               {
                       foreach ($catlog_data as $catlogData)
                               $catlog .='<option value="'.$catlogData->catlog_value.'">'.$catlogData->catlog_value.'</option>';        
               }
               $catlog .='</select>';
                       return $catlog;
   }
	
	
	function trailer_type_select() {
		$catlog ='';
		$catlog .='<select id="trailer_type_veh__SECID__" type="text" name="trailer_type[]" class="span12" >';
		$catlog .='<option value="">Select</option>';
		$catlog_data = $this->quote_model->get_catlog_options('trailer_type');
		if($catlog_data)
		{
			foreach ($catlog_data as $catlogData)
				$catlog .='<option value="'.$catlogData->catlog_value.'">'.$catlogData->catlog_value.'</option>';	
		}
		$catlog .='</select>';
			return $catlog;
    }
	
	function nature_of_business() {
		$catlog ='';
		$catlog .='<select type="text" name="nature_of_business[]" class="span12 chzn-select" multiple >';
		$catlog .='<option value="">Select</option>';
		$catlog_data = $this->quote_model->get_catlog_options('nature_of_business');
		if($catlog_data)
		{
			foreach ($catlog_data as $catlogData)
				$catlog .='<option value="'.$catlogData->catlog_value.'">'.$catlogData->catlog_value.'</option>';	
		}
		$catlog .='</select>';
		return $catlog;
    }
	
	function commodities_haulted() {
		$catlog ='';
		$catlog .='<select type="text" id="commodities_haulted" name="commodities_haulted[]" class="span12 chzn-select" multiple onchange="check_commodities_other(this.id)" >';
		$catlog .='<option value="">Select</option>';
		$catlog_data = $this->quote_model->get_catlog_options('commodities_haulted');
		if($catlog_data)
		{
			foreach ($catlog_data as $catlogData)
				$catlog .='<option value="'.$catlogData->catlog_value.'">'.$catlogData->catlog_value.'</option>';	
		}
		$catlog .='<option value="other">other</option>';	
		$catlog .='</select>';
		return $catlog;
    }
	//END ADDED BY MAYANK 21 JULY//
    
    public function QuickQuote($quote_id = '')
	{
		$this->load->library('form_validation');
		$this->quote_model->get_quote($quote_id);
		//$this->quote_model->get_insured_info($quote_id);
		$this->quote_model->get_losses($quote_id);
		$this->quote_model->get_all_vehicle($quote_id);
		$this->quote_model->get_all_drivers($quote_id);
		$this->quote_model->get_all_fillings($quote_id);
		$this->quote_model->get_all_owner($quote_id);
		
			$mail_sent = 0;
			
				
			$upload_path = $this->file_upload_path;
			
			
           // 
			//$this->GenerateQuickQuoteMail();
			
			if($_POST){  //print_r($_POST); die;
			//$myarray = array_values($myarray);
			//print_r($_FILES); die;
			//error_reporting(E_ALL);
				$result = $this->quote_model->insert_quote($upload_path);				
				$mail_sent = $this->GenerateQuickQuoteMail($result);
				//$this->session->set_flashdata('success', '<strong>Success</strong> Quote Request Sent Successfully');
			} else{
				//$this->session->set_flashdata('error', '<strong>Error</strong> In Sending Quote Request ');
			}
			
			
			
			/* if($mail_sent){
				$this->session->set_flashdata('success', '<strong>Success</strong> Quote Request Sent Successfully');
			}else{
				$this->session->set_flashdata('error', '<strong>Error</strong> In Sending Quote Request ');
			} */
			//$this->data['message'] = (!isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
			if($quote_id){
				$this->load->view('quote_view', $this->data);
			}else { 
				$this->load->view('quote_success');
				/* $red_url = base_url('quote/response');
				redirect($red_url); */
			}
	}
	
	public function GenerateQuickQuoteMail($result)
	{	
		//ini_set('display_errors',1); 
		//error_reporting(E_ALL);
		
		set_time_limit(0);
		ini_set("memory_limit","-1");  
		ini_set("post_max_size","200M");  
		ini_set("upload_max_filesize","200M"); 
		
		$this->load->library('email');
		
		$this->load->model('user/login_model');

		$mvr =	explode (',', $result['uploaded_file']['mvr']);
		$losses = explode (',',	$result['uploaded_file']['losses']);
	
	$quote_folder = $result['quote_id'];
	$mvr_link = '<br><strong>MVR for Driver/Owner</strong><br>';

	
	foreach($mvr as $mvr_file){
		$url = '';
		$url = base_url('/uploads/docs').'/'.$mvr_file;
		
		$url = str_replace(' ','',$url);
		
		$mvr_link .= '<a href="'.$url.'">'.$mvr_file.'</a><br>';
		
		$up_mvr_file_name = trim($mvr_file);
		$mvr_file_attach = str_replace(' ', '_', $up_mvr_file_name);
		
		$mvr_file_path = "./uploads/docs/$mvr_file";
		
		if(!empty($up_mvr_file_name)){
			$this->email->attach($mvr_file_path);
		}
			
	} 	
	
	$losses_link = '<br><strong>Losses</strong><br>';
	foreach($losses as $losses_file){
		$url = '';
		//$url = base_url('/uploads/quote_request').'/'.$quote_folder.'/'.$losses_file;
		$url = base_url('uploads/docs').'/'.$losses_file;
		$url = str_replace(' ','',$url);
		
		$losses_link .= '<a href="'.$url.'">'.$losses_file.'</a><br>';
		
		$up_losses_file_name = trim($losses_file);
		
		$losses_file_path = "./uploads/docs/$losses_file";
	
		if(!empty($up_losses_file_name)){
			$this->email->attach($losses_file_path);
		}
	} 

	$data['mvr_link'] = $mvr_link;
	$data['losses_link'] = $losses_link;
	
	//$result['quote_id'] = 1;
	
	$mail_message = $this->load->view('quote_mail_view', $data, TRUE);

	//$my_view = $this->load->view('test_mail_view', $this->data, TRUE);
		$filename = $quote_folder.'/'.$result['quote_id'];
		// As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
		//$pdfFilePath = FCPATH."uploads\quote_request\$filename.pdf";
		$pdfFilePath = FCPATH."/uploads/quote_request/$filename.pdf";
		
		//$data['page_title'] = 'Hello world'; // pass data to the view
	
	
		//if (file_exists($pdfFilePath) == FALSE)
		//{
		//ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley firstChild">
		// $html = $this->load->view('pdf_report', $data, true); // render the view into HTML

		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf->SetFooter('Page : {PAGENO}');
		$pdf->SetHeader('{DATE m-j-Y}| Quote Request Number -'.$result['quote_id'] .' | Page : {PAGENO}/{nb}|');
		
		// LOAD a stylesheet
		//$stylesheet = file_get_contents(base_url().'/css/quote_mail_view.css');
		//echo $stylesheet; die;
		//$pdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		
		//$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="http://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley lastChild">
		$pdf->WriteHTML($mail_message); // write the HTML into the PDF
		$pdf->Output($pdfFilePath, 'F'); // save to file because we can
		//}

		$quote_pdf_path = "./uploads/quote_request/$filename.pdf"; 
	
		
		$members = $this->session->userdata('member_id');
		$member_id = (!empty($members)) ? $members : 0 ;
		
		//$underwriter_mail =  $this->quote_model->get_underwriter_email($member_id);
		$underwriter_mail =  $this->quote_model->get_underwriter_email_address($member_id);
		if($underwriter_mail){
			$email_value[] = $underwriter_mail['email'];
			
			if(!empty($underwriter_mail['ccEmail'])){
				$cc_email = implode(',',$underwriter_mail['ccEmail']);
				$cc_email = $cc_email.','.$this->session->userdata('username');
			} else {
				$cc_email = $this->session->userdata('username');
			}
		}
		
		$super_mail_ids = $this->quote_model->get_supervisor_email();
		if($super_mail_ids){
			$email_value[] = $super_mail_ids;
		}
		
		if(!empty($email_value)){
			
			//Overwriting the actual PDF view
			$mail_message = 'Please find attachment.';
			
			$this->email->attach($quote_pdf_path);
			
			$email_add = implode(', ',$email_value);
				$email_setting  = array('mailtype'=>'html');
				$this->email->initialize($email_setting);
				$this->email->from($this->session->userdata('username'), $this->session->userdata('member_name'));
				$this->email->subject('Quotation Request from '. $this->session->userdata('member_name'));
				$this->email->message($mail_message);
				$this->email->to("$email_add");
				$this->email->cc("$cc_email");
				$mail_sent = $this->email->send();
		}

		if($mail_sent) { 
			return TRUE;
		}
		else { 
			return FALSE;
		}
	}
	
	public function list_quote()
	{
		$data['user'] = 'member';
		if ($this->session->userdata('underwriter_id')) {
			$data['user'] = 'underwriter';
		}
		$this->load->view('list_quote',$data);
	}
	
	public function list_assigned_quote()
	{
		$this->load->view('list_assigned_quote');
	}
	
	public function ajax_quote_list($uType='')
	{
		
		if ($uType=='underwriter')
		{
			$this->datatables
			->select("quote_id,contact_name,rq_rqf_quote.email,phone_no,name,DATE_FORMAT(coverage_date, '%d-%M-%Y'),rq_rqf_quote.status", FALSE)
		 	->from('rq_rqf_quote')
		 	->join('rq_underwriter', 'rq_underwriter.id=rq_rqf_quote.requester_id')
			->where('rq_rqf_quote.requester_id',$this->session->userdata('underwriter_id'));
			echo $this->datatables->generate();
		}
		else
		{
			$this->datatables
			->select("quote_id,contact_name,rq_rqf_quote.email,phone_no,first_name,DATE_FORMAT(coverage_date, '%d-%M-%Y'),rq_rqf_quote.status", FALSE)
		 	->from('rq_rqf_quote')
		 	->join('rq_members', 'rq_members.id=rq_rqf_quote.requester_id')
			->where('rq_rqf_quote.requester_id',$this->session->userdata('member_id'));
			echo $this->datatables->generate();
		}
	}
	
	public function ajax_assigned_quote()
	{
		$id = $this->session->userdata('underwriter_id');
		$where = "FIND_IN_SET('".$id."', assigned_underwriter)";
		$this->datatables
		->select("quote_id,contact_name,rq_rqf_quote.email,phone_no,name,DATE_FORMAT(coverage_date, '%d-%M-%Y'),rq_rqf_quote.status", FALSE)
	 	->from('rq_rqf_quote')
	 	->join('rq_underwriter', 'rq_underwriter.id=rq_rqf_quote.requester_id')
		//->where('rq_rqf_quote.requester_id',$this->session->userdata('underwriter_id'));
		->where($where);
		echo $this->datatables->generate();
	}
	
	public function response(){
		$this->load->view('quote_success');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */