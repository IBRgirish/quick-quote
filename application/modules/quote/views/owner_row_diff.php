<?php $this->load->view('includes/head_style_script')?>
<?php 
if(isset($owners)){
	if(is_array($owners)){		
	  ?>
	  <table class="table">
		<tr align="left">
		  <th>Owners</th>
          <th>First Name</th>
          <th>Middle Name</th>
          <th>Last Name</th>
          <th>Driver</th>
          <th>Driver License number</th>
          <th>License Issue State</th>
		</tr>
		<?php foreach($owners as $key => $owner){ ?>
		<tr>
		  <td>Owner #<?php echo $key+1; ?></td>
		  <td><?php echo $owner->owner_name; ?></td>
          <td><?php echo $owner->owner_middle_name; ?></td>
          <td><?php echo $owner->owner_last_name; ?></td>
          <td><?php echo $owner->is_driver; ?></td>
          <td><?php echo $owner->driver_licence_no; ?></td>
          <td><?php echo $owner->license_issue_state; ?></td>
		</tr>
        <?php } ?>
	  </table>
	  <?php
	}
}
?>