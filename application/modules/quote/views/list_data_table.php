<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>

<script type="text/javascript">
$(document).ready(function () {
    load_siteuser_list();

function load_siteuser_list() {
    $("#siteuserList").dataTable({
          "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "sAjaxSource": "<?php echo site_url(); ?>/quote/ajax_list_siteuser",

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [{ "sClass": "center"},
            {
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];
                    return (a);

                }

            }
        ]
    });
}
});

</script>

</head> 

<body>
<!-- siteuser List -->
  <table id="siteuserList" width="100%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">Sr.</th>
        <th width="22%">Name</th>
        <!--<th width="20%">Email</th>-->
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <!-- siteuser List -->

</body>
</html>