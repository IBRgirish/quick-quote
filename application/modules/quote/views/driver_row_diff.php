<?php $this->load->view('includes/head_style_script')?>
<?php 
if(isset($drivers)){
	if(is_array($drivers)){		
	  ?>
	  <table class="table">
		<tr align="left">
		  <th>Drivers</th>
		  <th>First Name</th>
		  <th>Middle Name</th>
		  <th>Last Name</th>
		  <th>Driver License number</th>
		  <th>License Class</th>
		  <th>Date Hired</th>
		  <th>DOB</th>
		  <th>Years Class A license</th>
		  <th>License Issue State</th>
		</tr>
		<?php foreach($drivers as $key => $driver){ ?>
		<tr>
		  <td>Driver #<?php echo $key+1; ?></td>
		  <td><?php echo $driver->driver_name; ?></td>
		  <td><?php echo $driver->driver_middle_name; ?></td>
		  <td><?php echo $driver->driver_last_name; ?></td>
		  <td><?php echo $driver->driver_licence_no; ?></td>
		  <td><?php echo $driver->driver_license_class; ?></td>
		  <td><?php echo date('m/d/Y', strtotime($driver->driver_license_issue_date)); ?></td>
		  <td><?php echo date('m/d/Y', strtotime($driver->driver_dob)); ?></td>
		  <td><?php echo $driver->driver_class_a_years; ?></td>
		  <td><?php echo $driver->driver_license_issue_state; ?></td>
		</tr>
        <?php } ?>
	  </table>
	  <?php
	}
}
?>