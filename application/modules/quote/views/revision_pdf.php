<?php $this->load->view('includes/head_style_script')?>
<style>
.pdf_view legend {
display: block!important;
width: auto!important; 
padding: 0!important;
margin-bottom: 20px!important;
font-size: 21px!important;
line-height: 40px!important;
color: #333;
/* border: 0; */
border-bottom: 1px solid #e5e5e5!important;
}
fieldset {
padding: 6px 0px 0px 10px;
border: 1px solid;
}
fieldset:last-child{
margin-bottom: 50px;	
}
</style>
<div class="pdf_view container">
<div class="row-fluid">
	<fieldset>
    	<legend style="margin:0px 0px 0px 0px !important;">Versions</legend>
        <table class="table">
        	<thead>
            	<tr>
                	<th>Versions</th>
                    <th>Created by</th>
                    <th>Created on</th>
                </tr>
            </thead>
            <tbody>
            	<?php 
				
				$previous_quote_ids_1 = $previous_quote_ids;
				$v=count($previous_quote_ids_1)-2;
				rsort($previous_quote_ids_1);
				
				foreach($previous_quote_ids_1 as $key => $previous_quote_id){
				if($quote_id!=$previous_quote_id){					
				$v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
				
				//$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
				$version_detail = get_version_detail($v_1);
				?>
            	<tr>
                	<td>v<?= $v; ?></td>
                    <td><?= isset($version_detail->requester_id) ? get_username($version_detail->requester_id) : ''; ?></td>
                    <td><?= isset($version_detail->date_added) ? date('m/d/Y h:i a', strtotime($version_detail->date_added)) : ''; ?></td>
                </tr>
                <?php 
				$v--;
				}
				}
				?>
            </tbody>
        </table>
    </fieldset>
</div>
  <div class="row-fluid">
  <fieldset>
    <legend>Broker Information</legend>
    <table class="table">
        <tr align="left">
            <th>Today's Date</th>
            <th>Time</th>
            <th>Coverage Date</th>
            <th>Person submitting business</th>
        </tr>
        <?php
		$p = '';
		$r = count($previous_quote_ids);
		for($i=0;$i<$r;$i++){
			$p_1 = isset($logs['contact_name'][$i])? $logs['contact_name'][$i] : '';
			$p_2 = isset($logs['contact_middle_name'][$i])? $logs['contact_middle_name'][$i] : '';
			$p_3 = isset($logs['contact_last_name'][$i])? $logs['contact_last_name'][$i] : '';
			$pr = isset($p_1->value) ? $p_1->value : '';
			$pr .= isset($p_2->value) ? ' '.$p_2->value : '';
			$pr .= isset($p_3->value) ? ' '.$p_3->value : '';
			if($pr){
			  $p[] = 'v'.$i.' '.$pr;
			}
		}
		?>
        <tr>
            <td><?php echo date('m/d/Y', time());?></td>
            <td><?php echo date('h:i:s a', time());?></td>
            <td><?php echo date('m/d/Y', strtotime($quote[0]->coverage_date));?></td>
            <td><?php echo $quote[0]->contact_name.' '.$quote[0]->contact_middle_name.' '.$quote[0]->contact_last_name;?>
            <b><?= is_array($p) ? ', '.implode(', ', $p) : ''; ?></b>
            </td>
        </tr>
    </table>
    <table class="table">
        <tr>
            <th>Cab Number</th>
            <th>Agency Name</th>
            <th>Phone Number</th>
            <th>Cell Ph. Number</th>
        </tr>
        <tr>
            <td><?php echo $quote[0]->cab;?></td>
            <td><?php echo $quote[0]->agency;?></td>
            <td><?php echo $quote[0]->phone_no;?></td>
            <td><?php echo $quote[0]->cell_no;?></td>
        </tr>
    </table>
    <table class="table">
        <tr>
            <th><?php echo $quote[0]->contact_field; ?></th>
            <th>Email</th>
            <th>Zip</th>
        </tr>
        <tr>
            <td><?php echo $quote[0]->contact_field_value; ?></td>
            <td><?php echo $quote[0]->email;?></td>
            <td><?php echo $quote[0]->zip;?></td>
        </tr>
    </table>
    </fieldset>
 <?php $quote = $quote[0]; ?>
  </div>
  <div class="row-fluid">
  	<fieldset>
        <legend>Insured Information</legend>
        <table class="table">
            <tr align="left">
                <th>Insured</th>
                <th>DBA</th>
                <th>Email</th>				
            </tr>
            <tr>
            	<?php 
				$p = '';
				$pr = '';
				for($i=0;$i<$r;$i++){
					$p_1 = isset($insured_logs['insured_fname'][$i]) ? $insured_logs['insured_fname'][$i] : '';
					$p_2 = isset($insured_logs['insured_mname'][$i]) ? $insured_logs['insured_mname'][$i] : '';
					$p_3 = isset($insured_logs['insured_lname'][$i]) ? $insured_logs['insured_lname'][$i] : '';
					
					$pr = isset($p_1->value) ? $p_1->value : '';
					$pr .= isset($p_2->value) ? ' '.$p_2->value : '';
					$pr .= isset($p_3->value) ? ' '.$p_3->value : '';
					if($pr){
					  $p[] = 'v'.$i.' '.$pr;
					}
				}
				$insured_name = $insured->insured_fname.' '.$insured->insured_mname.' '.$insured->insured_lname;
				?>
                <td><?php echo $insured_name; ?>
                <b><?php echo is_array($p) ? ', '.implode(', ', $p) : '';?></b>
                </td>                			
                <td><?php echo $insured->insured_dba;?>
                <?php $insured_dba = (isset($insured_logs['insured_dba']) ? $insured_logs['insured_dba'] : '');?>	
                <b><?php echo array_to_version($r, $insured_dba);?></b>
                </td>
                <td><?php echo $insured->insured_email;?>
                <?php $insured_email = (isset($insured_logs['insured_email']) ? $insured_logs['insured_email'] : '');?>	
                <b><?php echo array_to_version($r, $insured_email);?></b>
                </td>
            </tr>
        </table>
        <table class="table">
            <tr align="left">
                <th>Phone Number</th>
                <th>Garaging City</th>
                <th>State</th>
                <th>Zip</th>
            </tr>
            <tr>
                <td><?php echo $insured->insured_telephone;?>
                <?php $insured_telephone = (isset($insured_logs['insured_telephone']) ? $insured_logs['insured_telephone'] : '');?>	
                <b><?php echo array_to_version($r, $insured_telephone);?></b>
                </td>
                <td><?php echo $insured->insured_garaging_city;?>
                <?php $insured_garaging_city = (isset($insured_logs['insured_garaging_city']) ? $insured_logs['insured_garaging_city'] : '');?>	
                <b><?php echo array_to_version($r, $insured_garaging_city);?></b>
                </td>
                <td><?php echo $insured->insured_state;?>
                <?php $insured_state = (isset($insured_logs['insured_state']) ? $insured_logs['insured_state'] : '');?>	
                <b><?php echo array_to_version($r, $insured_state);?></b>
                </td>
                <td><?php echo $insured->insured_zip;?>
                <?php $insured_zip = (isset($insured_logs['insured_zip']) ? $insured_logs['insured_zip'] : '');?>	
                <b><?php echo array_to_version($r, $insured_zip);?></b>
                </td>
            </tr>
        </table>
        <table class="table">
            <tr>
                <th>Nature of Business</th>
                <th>Commodities Hauled</th>
                <th>Years in Business</th>
            </tr>
            
            <tr>
                <td><?php echo  $insured->nature_business; ?>
                <?php $nature_business = (isset($insured_logs['nature_business']) ? $insured_logs['nature_business'] : '');?>	
                <b><?php echo array_to_version($r, $nature_business);?></b>
                </td>
                <td><?php echo  $insured->commodities_haulted; ?>
                <?php $commodities_haulted = (isset($insured_logs['commodities_haulted']) ? $insured_logs['commodities_haulted'] : '');?>	
                <b><?php echo array_to_version($r, $commodities_haulted);?></b>
                </td>
                <td><?php echo  $insured->year_in_business; ?>
                <?php $year_in_business = (isset($insured_logs['year_in_business']) ? $insured_logs['year_in_business'] : '');?>	
                <b><?php echo array_to_version($r, $year_in_business);?></b>
                </td>
            </tr>
        </table>
     </fieldset>
  </div>
<div class="row-fluid">
      <fieldset>
      <legend>Losses Section</legend>
      <?php losses($losses_liability, $previous_quote_ids, $losses_liability_diff, 'losses_liability'); ?>
      <?php $los_count1 = count($losses_liability); ?>
		<?php
        if(count($previous_quote_ids)>1){
            $this->load->model('quote_model');
        ?>
        <div class="row-fluid">
        <label><b>Liability Losses Versions</b></label>
        <table style="margin-bottom: 9px;margin-top: 5px;" class="table">
          <thead>
            <tr>
              <th>Versions</th>
              <th>Number of Losses</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $previous_quote_ids_1 = $previous_quote_ids;
            $v=count($previous_quote_ids_1)-2;
            //$v=0;
            rsort($previous_quote_ids_1);    
            foreach($previous_quote_ids_1 as $key => $previous_quote_id){
              if($quote_id!=$previous_quote_id){					
                $v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
                
               //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
               $no_losses = $this->quote_model->get_losses_by_type($previous_quote_id, 'losses_liability');
              
               $los_count = count($no_losses);
               $los_version_diff = '';
                ?>
                <tr>
                    <td>v<?= $v; ?></td>
                    <td <?= $los_version_diff; ?> style="cursor: pointer;"><?= count($no_losses); ?></td>
                </tr>
                <?php  if($los_count!=$los_count1){ ?>
                <tr>
                  <td colspan="2"><?php
                        $this->call->losses_version_diff($previous_quote_id, $v_1, 'losses_liability');          	
                  ?></td>
                </tr>
                <?php } ?>
                <?php 
                $los_count1 = count($no_losses);
                $v--;
                //$v++;
              }
            }
            ?>
          </tbody>
        </table>
        </div>
        <?php
        }
        ?>
      </table>
      <?php losses($losses_pd, $previous_quote_ids, $losses_pd_diff, 'losses_pd'); ?>
      <?php $pos_count1 = count($losses_pd); ?>
		<?php
        if(count($previous_quote_ids)>1){
            $this->load->model('quote_model');
        ?>
        <div class="row-fluid">
        <label><b>PD Losses Versions</b></label>
        <table style="margin-bottom: 9px;margin-top: 5px;" class="table">
          <thead>
            <tr>
              <th>Versions</th>
              <th>Number of Losses</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $previous_quote_ids_1 = $previous_quote_ids;
            $v=count($previous_quote_ids_1)-2;
            //$v=0;
            rsort($previous_quote_ids_1);    
            foreach($previous_quote_ids_1 as $key => $previous_quote_id){
              if($quote_id!=$previous_quote_id){					
                $v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
                
               //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
               $no_losses = $this->quote_model->get_losses_by_type($previous_quote_id, 'losses_pd');
              
               $pos_count = count($no_losses);
               $pos_version_diff = '';
                ?>
                <tr>
                    <td>v<?= $v; ?></td>
                    <td <?= $pos_version_diff; ?> style="cursor: pointer;"><?= count($no_losses); ?></td>
                </tr>
                <?php  if($pos_count!=$pos_count1){ ?>
                <tr>
                  <td colspan="2"><?php
                        $this->call->losses_version_diff($previous_quote_id, $v_1, 'losses_pd');          	
                  ?></td>
                </tr>
                <?php } ?>
                <?php 
                $pos_count1 = count($no_losses);
                $v--;
                //$v++;
              }
            }
            ?>
          </tbody>
        </table>
        </div>
        <?php
        }
        ?>
      </table>
      <?php losses($losses_cargo, $previous_quote_ids, $losses_cargo_diff, 'losses_cargo'); ?>
      <?php $cos_count1 = count($losses_cargo); ?>
		<?php
        if(count($previous_quote_ids)>1){
            $this->load->model('quote_model');
        ?>
        <div class="row-fluid">
        <label><b>Cargo Losses Versions</b></label>
        <table style="margin-bottom: 9px;margin-top: 5px;" class="table">
          <thead>
            <tr>
              <th>Versions</th>
              <th>Number of Losses</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $previous_quote_ids_1 = $previous_quote_ids;
            $v=count($previous_quote_ids_1)-2;
            //$v=0;
            rsort($previous_quote_ids_1);    
            foreach($previous_quote_ids_1 as $key => $previous_quote_id){
              if($quote_id!=$previous_quote_id){					
                $v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
                
               //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
               $no_losses = $this->quote_model->get_losses_by_type($previous_quote_id, 'losses_cargo');
              
               $cos_count = count($no_losses);
               $cos_version_diff = '';
                ?>
                <tr>
                    <td>v<?= $v; ?></td>
                    <td <?= $cos_version_diff; ?> style="cursor: pointer;"><?= count($no_losses); ?></td>
                </tr>
                <?php  if($cos_count!=$cos_count1){ ?>
                <tr>
                  <td colspan="2"><?php
                        $this->call->losses_version_diff($previous_quote_id, $v_1, 'losses_cargo');          	
                  ?></td>
                </tr>
                <?php } ?>
                <?php 
                $cos_count1 = count($no_losses);
                $v--;
                //$v++;
              }
            }
            ?>
          </tbody>
        </table>
        </div>
        <?php
        }
        ?>
      </table>
      <?php losses($losses_other1, $previous_quote_ids, $losses_other1_diff, 'losses_other1'); ?>
      <?php $os_count1 = count($losses_other1); ?>
		<?php
        if(count($previous_quote_ids)>1){
            $this->load->model('quote_model');
        ?>
        <div class="row-fluid">
        <label><b>Other1 Losses Versions</b></label>
        <table style="margin-bottom: 9px;margin-top: 5px;" class="table">
          <thead>
            <tr>
              <th>Versions</th>
              <th>Number of Losses</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $previous_quote_ids_1 = $previous_quote_ids;
            $v=count($previous_quote_ids_1)-2;
            //$v=0;
            rsort($previous_quote_ids_1);    
            foreach($previous_quote_ids_1 as $key => $previous_quote_id){
              if($quote_id!=$previous_quote_id){					
                $v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
                
               //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
               $no_losses = $this->quote_model->get_losses_by_type($previous_quote_id, 'losses_other1_specify_other');
              
               $os_count = count($no_losses);
               $os_version_diff = '';
                ?>
                <tr>
                    <td>v<?= $v; ?></td>
                    <td <?= $os_version_diff; ?> style="cursor: pointer;"><?= count($no_losses); ?></td>
                </tr>
                <?php  if($os_count!=$os_count1){ ?>
                <tr>
                  <td colspan="2"><?php
                        $this->call->losses_version_diff($previous_quote_id, $v_1, 'losses_other1_specify_other');          	
                  ?></td>
                </tr>
                <?php } ?>
                <?php 
                $os_count1 = count($no_losses);
                $v--;
                //$v++;
              }
            }
            ?>
          </tbody>
        </table>
        </div>
        <?php
        }
        ?>
      </table>
      <?php losses($losses_other2, $previous_quote_ids, $losses_other2_diff, 'losses_other2'); ?>
      <?php $oos_count1 = count($losses_other2); ?>
		<?php
        if(count($previous_quote_ids)>1){
            $this->load->model('quote_model');
        ?>
        <div class="row-fluid">
        <label><b>Other2 Losses Versions</b></label>
        <table style="margin-bottom: 9px;margin-top: 5px;" class="table">
          <thead>
            <tr>
              <th>Versions</th>
              <th>Number of Losses</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $previous_quote_ids_1 = $previous_quote_ids;
            $v=count($previous_quote_ids_1)-2;
            //$v=0;
            rsort($previous_quote_ids_1);    
            foreach($previous_quote_ids_1 as $key => $previous_quote_id){
              if($quote_id!=$previous_quote_id){					
                $v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
                
               //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
               $no_losses = $this->quote_model->get_losses_by_type($previous_quote_id, 'losses_other2_specify_other');
              
               $oos_count = count($no_losses);
               $oos_version_diff = '';
                ?>
                <tr>
                    <td>v<?= $v; ?></td>
                    <td <?= $oos_version_diff; ?> style="cursor: pointer;"><?= count($no_losses); ?></td>
                </tr>
                <?php  if($oos_count!=$oos_count1){ ?>
                <tr>
                  <td colspan="2"><?php
                        $this->call->losses_version_diff($previous_quote_id, $v_1, 'losses_other2_specify_other');          	
                  ?></td>
                </tr>
                <?php } ?>
                <?php 
                $oos_count1 = count($no_losses);
                $v--;
                //$v++;
              }
            }
            ?>
          </tbody>
        </table>
        </div>
        <?php
        }
        ?>
      </table>
      </fieldset>
      <fieldset>
      	<legend>Vehicle</legend>
        <label><strong>Current Value</strong></label>
        <?php
		//print_r($trcuk_diff);
		vehicle_rows($tractors, $previous_quote_ids, $tractors_diff, 'Tractors');
		?>
		<?php
		if(count($previous_quote_ids)>1){
			$this->load->model('quote_model');
		?>
		<div class="row-fluid">
		<label><b>Tractor Versions</b></legend>
		<table style="margin-bottom: 9px;margin-top: 5px;" class="table">
		  <thead>
			<tr>
			  <th>Versions</th>
			  <th>No of Tractors</th>
			</tr>
		  </thead>
		  <tbody>
			<?php 
			$tra_count1 = count($tractors);
			$previous_quote_ids_1 = $previous_quote_ids;
			$v=count($previous_quote_ids_1)-2;
			//$v=0;
			rsort($previous_quote_ids_1);    
			foreach($previous_quote_ids_1 as $key => $previous_quote_id){
			  if($quote_id!=$previous_quote_id){					
				$v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
				
			   //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
			   $no_tractors = $this->quote_model->get_all_vehicle($previous_quote_id, 'TRACTOR');
			   $tra_count = count($no_tractors);
			   $tra_version_diff = '';
			   if($tra_count!=$tra_count1){
				$tra_version_diff = 'onclick="version_diff('.$previous_quote_id.', '.$v_1.', &quot;TRACTOR&quot;)"';
			   }
				?>
				<tr>
					<td>v<?= $v; ?></td>
					<td <?= $tra_version_diff; ?> style="cursor: pointer;"><?= count($no_tractors); ?></td>
				</tr>
                <tr>
                  <td colspan="2"><?php
                        $this->call->vehicle_version_diff($previous_quote_id, $v_1, 'TRACTOR');          	
                  ?></td>
                </tr>
				<?php 
				$tra_count1 = count($no_tractors);
				$v--;
				//$v++;
			  }
			}
			?>
		  </tbody>
		</table>
		</div>
		<?php
		}
		?>
        <?php
		vehicle_rows($trucks, $previous_quote_ids, $trcuk_diff, 'Trucks');
		?>
        <?php
		if(count($previous_quote_ids)>1){
			$this->load->model('quote_model');
		?>
		<div class="row-fluid">
		<label><b>Truck Versions</b></legend>
		<table style="margin-bottom: 9px;margin-top: 5px;" class="table">
		  <thead>
			<tr>
			  <th>Versions</th>
			  <th>No of Trucks</th>
			</tr>
		  </thead>
		  <tbody>
			<?php 
			$tru_count1 = count($trucks);
			$previous_quote_ids_1 = $previous_quote_ids;
			$v=count($previous_quote_ids_1)-2;
			//$v=0;
			rsort($previous_quote_ids_1);    
			foreach($previous_quote_ids_1 as $key => $previous_quote_id){
			  if($quote_id!=$previous_quote_id){					
				$v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
				
			   //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';	
			   $no_trucks = $this->quote_model->get_all_vehicle($previous_quote_id, 'TRUCK');
			   $tru_count = count($no_trucks);
			   $tra_version_diff = '';
			   $tru_version_diff = '';
			   if($tru_count!=$tru_count1){
				$tru_version_diff = 'onclick="version_diff('.$previous_quote_id.', '.$v_1.', &quot;TRUCK&quot;)"';
			   }
				?>
				<tr>
					<td>v<?= $v; ?></td>
					<td <?= $tru_version_diff; ?> style="cursor: pointer;"><?= count($no_trucks) ?></td>
				</tr>
                <tr>
                  <td colspan="2"><?php
                        $this->call->vehicle_version_diff($previous_quote_id, $v_1, 'TRUCK');          	
                  ?></td>
                </tr>
				<?php 
				$tru_count1 = count($no_trucks);
				$v--;
				//$v++;
			  }
			}
			?>
		  </tbody>
		</table>
		</div>
		<?php
		}
		?>
        <!--</pre>  -->
      </fieldset>      
      
      <fieldset>
        <legend>Driver</legend>
         <table class="table">
          <tr align="left">
          	<th>Versions</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Driver License number</th>
            <th>License Class</th>
            <th>Date Hired</th>
            <th>DOB</th>
            <th>Years Class A license</th>
            <th>License Issue State</th>
          </tr>
        <?php //print_r($drivers); 
		 $i = 0;
		  foreach($drivers as $driver){
			?>
            <tr>
				<td colspan="10"><label><b>Driver #<?php echo $i+1; ?></b></label></td>
            </tr>
                <tr>
                	<td>Current</td>
                    <td><?php echo $driver->driver_name; ?></td>
                    <td><?php echo $driver->driver_middle_name; ?></td>
                    <td><?php echo $driver->driver_last_name; ?></td>
                    <td><?php echo $driver->driver_licence_no; ?></td>
                    <td><?php echo $driver->driver_license_class; ?></td>
                    <td><?php echo date('m/d/Y', strtotime($driver->driver_license_issue_date)); ?></td>
                    <td><?php echo date('m/d/Y', strtotime($driver->driver_dob)); ?></td>
                    <td><?php echo $driver->driver_class_a_years; ?></td>
                    <td><?php echo $driver->driver_license_issue_state; ?></td>
                </tr>
            
            <?php
			$j = 0;
			//rsort($previous_quote_ids);
			if(isset($previous_quote_ids)){
			  if(isset($previous_quote_ids)){
				foreach($previous_quote_ids as $previous_quote_id){
				  if(isset($driver_diff[$previous_quote_id][$i])){
					$diff_field = $driver_diff[$previous_quote_id][$i]; 
					//print_r($diff_field);
					if(count($diff_field)>2){
					?>
                     <tr>
                     	<td>v<?= $j; ?></td>
                        <td><?php echo isset($diff_field['driver_name']) ? $diff_field['driver_name'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_middle_name']) ? $diff_field['driver_middle_name'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_last_name']) ? $diff_field['driver_last_name'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_licence_no']) ? $diff_field['driver_licence_no'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_license_class']) ? $diff_field['driver_license_class'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_license_issue_date']) ? date('m/d/Y', strtotime($diff_field['driver_license_issue_date'])) : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_dob']) ? date('m/d/Y', strtotime($diff_field['driver_dob'])) : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_class_a_years']) ? $diff_field['driver_class_a_years'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_license_issue_state']) ? $diff_field['driver_license_issue_state'] : ''; ?></td>
                    </tr>
                    <?php					
					}
				  }
				  $j++;
				}
			  }
			}
			$i++;
		  }
		?>
        </table>
        <?php $dr_count1 = count($drivers); ?>
		<?php
        if(count($previous_quote_ids)>1){
            $this->load->model('quote_model');
        ?>
        <div class="row-fluid">
        <label><b>Driver Versions</b></label>
        <table style="margin-bottom: 9px;margin-top: 5px;" class="table">
          <thead>
            <tr>
              <th>Versions</th>
              <th>Number of Drivers</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            $tra_count1 = count($tractors);
            $tru_count1 = count($trucks);
            $previous_quote_ids_1 = $previous_quote_ids;
            $v=count($previous_quote_ids_1)-2;
            //$v=0;
            rsort($previous_quote_ids_1);    
            foreach($previous_quote_ids_1 as $key => $previous_quote_id){
              if($quote_id!=$previous_quote_id){					
                $v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
               //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
               $no_drivers = $this->quote_model->get_all_drivers($previous_quote_id);
               $dr_count = count($no_drivers);
               $dr_version_diff = '';
               if($dr_count!=$dr_count1){
                $dr_version_diff = 'onclick="driver_version_diff('.$previous_quote_id.', '.$v_1.', &quot;Driver&quot;)"';
               }
                ?>
                <tr>
                  <td>v<?= $v; ?></td>
                  <td <?= $dr_version_diff; ?> style="cursor: pointer;"><?= count($no_drivers); ?></td>
                </tr>
                <tr>
                  <td colspan="2"><?php
                        $this->call->driver_version_diff($previous_quote_id, $v_1, 'Driver');          	
                  ?></td>
                </tr>
                <?php 
                $dr_count1 = count($no_drivers);
                $v--;
                //$v++;
              }
            }
            ?>
          </tbody>
        </table>
        </div>
        <?php
        }
        ?>
      </fieldset>
      <fieldset>
        <legend>Owner</legend>
         <table class="table">
          <tr align="left">
          	<th>Versions</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Driver</th>
            <th>Driver License number</th>
            <th>License Issue State</th>
          </tr>
        <?php //print_r($drivers); 
		 $i = 0;
		  foreach($owners as $owner){
			?>
            <tr>
				<td colspan="10"><label><b>Owner #<?php echo $i+1; ?></b></label></td>
            </tr>
                <tr>
                	<td>Current</td>
                    <td><?php echo $owner->owner_name; ?></td>
                    <td><?php echo $owner->owner_middle_name; ?></td>
                    <td><?php echo $owner->owner_last_name; ?></td>
                    <td><?php echo $owner->is_driver; ?></td>
                    <td><?php echo $owner->driver_licence_no; ?></td>
                    <td><?php echo $owner->license_issue_state; ?></td>
                </tr>
            
            <?php
			$j = 0;
			//rsort($previous_quote_ids);
			if(isset($previous_quote_ids)){
			  if(isset($previous_quote_ids)){
				foreach($previous_quote_ids as $previous_quote_id){
				  if(isset($owner_diff[$previous_quote_id][$i])){
					$diff_field = $owner_diff[$previous_quote_id][$i]; 
					//print_r($diff_field);
					if(count($diff_field)>2){
					?>
                     <tr>
                      <td>v<?= $j; ?></td>
                      <td><?php echo isset($diff_field['owner_name']) ? $diff_field['owner_name'] : ''; ?></td>
                      <td><?php echo isset($diff_field['owner_middle_name']) ? $diff_field['owner_middle_name'] : ''; ?></td>
                      <td><?php echo isset($diff_field['owner_last_name']) ? $diff_field['owner_last_name'] : ''; ?></td>
                      <td><?php echo isset($diff_field['is_driver']) ? $diff_field['is_driver'] : ''; ?></td>
                      <td><?php echo isset($diff_field['driver_licence_no']) ? $diff_field['driver_licence_no'] : ''; ?></td>
                      <td><?php echo isset($diff_field['license_issue_state']) ? $diff_field['license_issue_state'] : ''; ?></td>
                    </tr>
                    <?php					
					}
				  }
				  $j++;
				}
			  }
			}
			$i++;
		  }
		?>
        </table>
        <?php $or_count1 = count($owners); ?>
		<?php
        if(count($previous_quote_ids)>1){
            $this->load->model('quote_model');
        ?>
        <div class="row-fluid">
        <label><b>Owner Versions</b></label>
        <table style="margin-bottom: 9px;margin-top: 5px;" class="table">
          <thead >
            <tr>
              <th>Versions</th>
              <th>Number of Owners</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $previous_quote_ids_1 = $previous_quote_ids;
            $v=count($previous_quote_ids_1)-2;
            //$v=0;
            rsort($previous_quote_ids_1);    
            foreach($previous_quote_ids_1 as $key => $previous_quote_id){
              if($quote_id!=$previous_quote_id){					
                $v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
                
               //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
               $no_owners = $this->quote_model->get_all_owner($previous_quote_id);
              
               $or_count = count($no_owners);
               $or_version_diff = '';
              
               if($or_count!=$or_count1){
                $or_version_diff = 'onclick="owner_version_diff('.$previous_quote_id.', '.$v_1.', &quot;Owner&quot;)"';
               }
              
                ?>
                <tr>
                    <td>v<?= $v; ?></td>
                    <td <?= $or_version_diff; ?> style="cursor: pointer;"><?= count($no_owners); ?></td>
                </tr>
                <tr>
                  <td colspan="2"><?php
                        $this->call->owner_version_diff($previous_quote_id, $v_1, 'Owner');          	
                  ?></td>
                </tr>
                <?php 
                $or_count1 = count($no_owners);
                $v--;
                //$v++;
              }
            }
            ?>
          </tbody>
        </table>
        </div>
        <?php
        }
        ?>
      </fieldset>
      <fieldset>
        <legend>Filings Section</legend>
        <?php if( count($fillings) > 0 ) { ?>
        <table class="table">
            <tr align="left">
            	<th>Versions</th>
                <th>Filing Type </th>
                <th>Filing #</th>
            </tr>
            <?php $i = 0; foreach($fillings as $filling) { ?>
            <tr>
             <tr>
				<td colspan="3"><label><b>Filing #<?php echo $i+1; ?></b></label></td>
            </tr>
            	<td>Current</td>
                <td><?php echo $filling->filings_type; ?> <?php if($filling->filings_type=='Other'){ echo " : - ".(isset($filling->filing_type_other) ? $filling->filing_type_other : ''); }?></td>
                <td><?php echo $filling->filing_no; ?></td>
            </tr>
            	<?php
                $j = 0;
				//rsort($previous_quote_ids);
				if(isset($previous_quote_ids)){
				  if(isset($previous_quote_ids)){
					foreach($previous_quote_ids as $previous_quote_id){
					  if(isset($filling_diff[$previous_quote_id][$i])){
						$diff_field = $filling_diff[$previous_quote_id][$i]; 
						//print_r($diff_field);
						if(count($diff_field)>2){
						?>
						 <tr>
						  <td>v<?= $j; ?></td>
						  <td><?php echo isset($diff_field['filings_type']) ? $diff_field['filings_type'] : ''; ?></td>
						  <td><?php echo isset($diff_field['filing_no']) ? $diff_field['filing_no'] : ''; ?></td>
						</tr>
						<?php
						}
					  }
					  $j++;
					}
				  }
				}
				?>
            <?php } ?>
        </table>
        <?php  $i++; } ?>
        <?php $fil_count1 = count($fillings); ?>
		<?php
        if(count($previous_quote_ids)>1){
            $this->load->model('quote_model');
        ?>
        <div class="row-fluid">
        <label><b>Filings Versions</b></label>
        <table style="margin-bottom: 9px;margin-top: 5px;" class="table">
          <thead>
            <tr>
              <th>Versions</th>
              <th>Number of Filings</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $previous_quote_ids_1 = $previous_quote_ids;
            $v=count($previous_quote_ids_1)-2;
            //$v=0;
            rsort($previous_quote_ids_1);    
            foreach($previous_quote_ids_1 as $key => $previous_quote_id){
              if($quote_id!=$previous_quote_id){					
                $v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
                
               //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
               $no_filings = $this->quote_model->get_all_fillings($previous_quote_id);
              
               $fil_count = count($no_filings);
               $fil_version_diff = '';
              
               if($fil_count!=$fil_count1){
                $fil_version_diff = 'onclick="filing_version_diff('.$previous_quote_id.', '.$v_1.', &quot;Owner&quot;)"';
               }
              
                ?>
                <tr>
                    <td>v<?= $v; ?></td>
                    <td <?= $fil_version_diff; ?> style="cursor: pointer;"><?= count($no_filings); ?></td>
                </tr>
                <tr>
                  <td colspan="2"><?php
                        $this->call->filing_version_diff($previous_quote_id, $v_1, 'Filing');          	
                  ?></td>
                </tr>
                <?php 
                $fil_count1 = count($no_filings);
                $v--;
                //$v++;
              }
            }
            ?>
          </tbody>
        </table>
        </div>
        <?php
        }
        ?>
        </fieldset>
  </div>
</div>
<?php
function losses($losses, $previous_quote_ids, $diff_data, $type=''){
	if(is_array($losses)){
		if(!empty($losses)){
	?>
	<table class="table">
	  <tr>
		  <th colspan="6" style="text-align:left">Losses Type</th>
		  <td colspan="1"><?php echo $type;  ?></td>
	  </tr>
	  <tr>
		<th colspan="3" >Period of losses</th>
		<th rowspan="2" >Amount</th>
		<th rowspan="2" >Company</th>
        <th rowspan="2" >General <br /> Agent</th>
        <th rowspan="2" >Date of Loss</th>
	  </tr>
	  <tr colspan="1" >
      	<th>Versions</th>
		<th>From (mm/dd/yyyy)</th>
		<th>To (mm/dd/yyyy)</th>
	  </tr>        
  <?php $i = 0; foreach($losses as $losse) { ?>
	  <tr>
      	  <td>Current</td>
		  <td><?php echo date('m/d/Y', strtotime($losse->losses_from_date)); ?></td>
		  <td><?php echo date('m/d/Y', strtotime($losse->losses_to_date)); ?></td>
		  <td><?php echo $losse->losses_amount; ?></td>
		  <td><?php echo $losse->losses_company; ?></td>
		  <td><?php echo $losse->losses_general_agent; ?></td>
		  <td><?php echo date('m/d/Y', strtotime($losse->date_of_loss)); ?></td>
	  </tr>
      <?php
      	$j = 0;
		//rsort($previous_quote_ids);
		if(isset($previous_quote_ids)){
		  if(isset($previous_quote_ids)){
			foreach($previous_quote_ids as $previous_quote_id){
			  if(isset($diff_data[$previous_quote_id][$i])){
				$diff_field = $diff_data[$previous_quote_id][$i]; 
				if(count($diff_field)>2){
				?>
                <tr>
                  <td>v<?= $j?></td>
                  <td><?php echo isset($diff_field['losses_from_date']) ? date('m/d/Y', strtotime($diff_field['losses_from_date'])) : ''; ?></td>
                  <td><?php echo isset($diff_field['losses_to_date']) ? date('m/d/Y', strtotime($diff_field['losses_to_date'])) : ''; ?></td>
                  <td><?php echo isset($diff_field['losses_amount']) ? $diff_field['losses_amount'] : ''; ?></td>
                  <td><?php echo isset($diff_field['losses_company']) ? $diff_field['losses_company'] : ''; ?></td>
                  <td><?php echo isset($diff_field['losses_general_agent']) ? $diff_field['losses_general_agent'] : ''; ?></td>
                  <td><?php echo isset($diff_field['date_of_loss']) ? date('m/d/Y', strtotime($diff_field['date_of_loss'])) : ''; ?></td>
                </tr>
                <?php
				}
			  }
			  $j++;
			}
		  }
		}
	  ?>
  <?php $i++; } //IF Ends?>
  </table>	
  <?php
	}
  }
}
function vehicle_rows($vehicles, $previous_quote_ids, $diff_data, $v_type='Tractors'){
	$CI = & get_instance();
	$data = '';
	
	//print_r($diff_data);
if(is_array($vehicles)) {
		  $i = 0;
		  foreach($vehicles as $vehicle){
			  $data['radiou_of_operation'] = '';$data['radius_of_operation_other']='';$data['request_for_radius_of_operation']='';$data['number_of_axles']='';$data['spacial_request']='';$data['vehicle_year']='';$data['make_model']='';$data['gvw']='';$data['vin']='';$data['liability']='';$data['liability_ded']='';$data['um']='';$data['pip']='';$data['pd']='';$data['pd_ded']='';$data['cargo']='';$data['cargo_ded']='';$data['commodities_haulted']='';
			?>
			<label><b><?php echo $v_type; ?> #<?php echo $i+1; ?></b></label>
            
            <?php
			$j = 0;
			//rsort($previous_quote_ids);
			if(isset($previous_quote_ids)){
			  if(isset($previous_quote_ids)){
				foreach($previous_quote_ids as $previous_quote_id){
				  if(isset($diff_data[$previous_quote_id][$i])){
					$diff_field = $diff_data[$previous_quote_id][$i]; 
					if(isset($diff_field['radius_of_operation'])){
						$data['radiou_of_operation'][] = "v$j. ".$diff_field['radius_of_operation'];
					}
					if(isset($diff_field['radius_of_operation_other'])){
						$data['radius_of_operation_other'][] = "v$j. ".$diff_field['radius_of_operation_other'];
					}
					if(isset($diff_field['request_for_radius_of_operation'])){
						$data['request_for_radius_of_operation'][] = "v$j. ".$diff_field['request_for_radius_of_operation'];
					}
					if(isset($diff_field['number_of_axles'])){
						$data['number_of_axles'][] = "v$j. ".$diff_field['number_of_axles'];
					}
					if(isset($diff_field['vaule'])){
						$data['spacial_request'][] = "v$j. ".$diff_field['vaule'];
					}
					if(isset($diff_field['vehicle_year'])){
						$data['vehicle_year'][] = "v$j. ".$diff_field['vehicle_year'];
					}
					if(isset($diff_field['make_model'])){
						$data['make_model'][] = "v$j. ".$diff_field['make_model'];
					}
					if(isset($diff_field['gvw'])){
						$data['gvw'][] = "v$j. ".$diff_field['gvw'];
					}
					if(isset($diff_field['vin'])){
						$data['vin'][] = "v$j. ".$diff_field['vin'];
					}
					if(isset($diff_field['liability'])){
						$data['liability'][] = "v$j. ".$diff_field['liability'];
					}
					if(isset($diff_field['liability_ded'])){
						$data['liability_ded'][] = "v$j. ".$diff_field['liability_ded'];
					}
					if(isset($diff_field['um'])){
						$data['um'][] = "v$j. ".$diff_field['um'];
					}
					if(isset($diff_field['pip'])){
						$data['pip'][] = "v$j. ".$diff_field['pip'];
					}
					if(isset($diff_field['pd'])){
						$data['pd'][] = "v$j. ".$diff_field['pd'];
					}
					if(isset($diff_field['pd_ded'])){
						$data['pd_ded'][] = "v$j. ".$diff_field['pd_ded'];
					}
					if(isset($diff_field['cargo'])){
						$data['cargo'][] = "v$j. ".$diff_field['cargo'];
					}
					if(isset($diff_field['cargo_ded'])){
						$data['cargo_ded'][] = "v$j. ".$diff_field['cargo_ded'];
					}
					if(isset($diff_field['commodities_haulted'])){
						$data['commodities_haulted'][] = "v$j. ".$diff_field['commodities_haulted'];
					}
					//$data['radiou_of_operation'][] = $diff_field['radius_of_operation'];
				  }
				  $j++;
				}
			  }
			}
			?>  
            <?php 
			$data['data'] = $vehicle; 
			//print_r($data);
			$CI->load->view('vehicle_tractor_row', $data);
			?>
			<?php //print_r($tractor); ?>  
			
			<?php 
			$i++;
		  } 
		}	
}
?>
