<style>
	legend { font-family:Tahoma, Geneva, sans-serif; font-size:18px;border:#000 solid 2px; }
	table 
	{
		border:1px solid #CCC;
		width:800px;
	}
	tr th { background:none repeat scroll 0 0 #FFF;color:#000;font-family:Arial, Helvetica, sans-serif;font-size:14px;padding:4px 3px; }
	
	tr td { background:none repeat scroll 0 0 #E7EEF5;color:#666;font-family:Arial, Helvetica, sans-serif;font-size:13px;padding:4px 3px;text-align:center; }
	
	fieldset, div { width:810px; }
	legend { background:#000 ; color:#000 !important; }
</style>
<?php

function currency_format($value){
	return "$ ".number_format($value, 0);
}

if($_POST)
{ 
	if(isset($_POST['vehicle_liability']) && $_POST['vehicle_liability'] != '') { 
		$liability_checked = 1;
	} else {
		$liability_checked = 0;
	}
	
	//echo "<pre>";print_r($_POST);echo "</pre>";
	//Tractor
	$tot_tractor_liability_vh = 0;
	$tot_tractor_liability_ded_vh = 0;
	$tot_tractor_pd_vh = 0;
	$tot_tractor_ph_ded_vh = 0;
	$tot_tractor_cargo_vh = 0;
	$tot_tractor_cargo_ded_vh = 0;
	
	//Trailer
	$tot_trailer_liability_vh = 0;
	$tot_trailer_liability_ded_vh = 0;
	$tot_trailer_pd_vh = 0;
	$tot_trailer_ph_ded_vh = 0;
	$tot_trailer_cargo_vh = 0;
	$tot_trailer_cargo_ded_vh = 0;
	
	//Trucks
	$tot_liability_vh = 0;
	$tot_liability_ded_vh = 0;
	$tot_pd_vh = 0;
	$tot_ph_ded_vh = 0;
	$tot_cargo_vh = 0;
	$tot_cargo_ded_vh = 0;
	if(isset($_POST['vehicle_truck_count']) && !empty($_POST['vehicle_truck_count'])){
		$total_truck = $_POST['vehicle_truck_count'];
	} else {
		$total_truck = '0';
	}
	
	if(isset($_POST['vehicle_tractor_count']) && !empty($_POST['vehicle_tractor_count'])){
		$total_tractors = $_POST['vehicle_tractor_count'];
	} else {
		$total_tractors = '0';
	}
	
	if(isset($_POST['trailer_vehicle_year_vh']) && !empty($_POST['trailer_vehicle_year_vh'])){
		//$total_trailers = count($_POST['trailer_associated_with_tractor']);
		$total_trailers = count($_POST['trailer_vehicle_year_vh']);
	} else {
		$total_trailers = '0';
	}	
	
	if(isset($_POST['driver_count']) && !empty($_POST['driver_count'])){
		$total_driver = $_POST['driver_count'];
	} else {
		$total_driver = '0';
	}
	
	if(isset($_POST['number_of_owner']) && !empty($_POST['number_of_owner'])){
		$total_owner = $_POST['number_of_owner'];
	} else {
		$total_owner = '0';
	}
	
	if(isset($_POST['number_of_filings']) && !empty($_POST['number_of_filings'])){
		$total_filings = $_POST['number_of_filings'];
	} else {
		$total_filings = '0';
	}
	
?>

<table>
	<tr align="left">
		<th rowspan="3">Coverage Summary</th>
	</tr>
    <tr align="left">
		<td>Number of Tractors</td>
        <td>Number of Trailers</td>
        <td>Number of Trucks</td>
	</tr>
    <tr align="left">
		<td><?php echo $total_tractors; ?></td>
        <td><?php echo $total_trailers;?></td>
        <td><?php echo $total_truck; ?></td>
	</tr>
</table>

<!----------------------------------------------------------------------------------------------->

<?php if($total_tractors > 0 ) { ?>
<table>
	<tr align="left">
		<th colspan="7">Tractor</th>
	</tr>
    <tr align="left">
		<th>Vehicle</th>
        <?php if($liability_checked) { ?>
		<th>Liability</th>
        <th>Liability Deductible</th>
		<?php } ?>
        <th>PD</th>
        <th>PD Deductible</th>
        <th>Cargo</th>
        <th>Cargo Deductible</th>
	</tr>
	
	
<?php //for($i = 0; $i < count($total_tractors); $i++) { echo $total_tractors.'tractor count + '.$i;?>
<?php $trac_count = $total_tractors; $i =0; while($trac_count > 0){ echo $i; ?>
	<tr>
    	<td><?php echo $i+1;?></td>
		<?php if($liability_checked) { ?>
		<td><?php if(isset($_POST['tractor_liability_vh']))echo $_POST['tractor_liability_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['tractor_liability_ded_vh']))echo $_POST['tractor_liability_ded_vh'][$i]; ?></td>
		<?php } ?>
        <td><?php if(isset($_POST['tractor_pd_vh']))echo $_POST['tractor_pd_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['tractor_ph_ded_vh']))echo $_POST['tractor_ph_ded_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['tractor_cargo_vh']))echo $_POST['tractor_cargo_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['tractor_cargo_ded_vh']))echo $_POST['tractor_cargo_ded_vh'][$i]; ?></td>
	</tr>
      
<?php
		//Calculating the total
		if(isset($_POST['tractor_liability_vh']))
		{
		$tot_tractor_liability_vh = $tot_tractor_liability_vh + str_replace(',','',str_replace('$','',$_POST['tractor_liability_vh'][$i])); 
		}
		if(isset($_POST['tractor_liability_ded_vh']))
		{
		$tot_tractor_liability_ded_vh = $tot_tractor_liability_ded_vh + str_replace(',','',str_replace('$','',$_POST['tractor_liability_ded_vh'][$i])); 
		}
		if(isset($_POST['tractor_pd_vh']))
		{
		$tot_tractor_pd_vh = $tot_tractor_pd_vh + str_replace(',','',str_replace('$','',$_POST['tractor_pd_vh'][$i])); 
		}
		if(isset($_POST['tractor_ph_ded_vh']))
		{
		$tot_tractor_ph_ded_vh = $tot_tractor_ph_ded_vh + str_replace(',','',str_replace('$','',$_POST['tractor_ph_ded_vh'][$i])); 
		}
		if(isset($_POST['tractor_cargo_vh']))
		{
		$tot_tractor_cargo_vh = $tot_tractor_cargo_vh + str_replace(',','',str_replace('$','',$_POST['tractor_cargo_vh'][$i])); 
		}
		if(isset($_POST['tractor_cargo_ded_vh']))
		{
		$tot_tractor_cargo_ded_vh = $tot_tractor_cargo_ded_vh + str_replace(',','',str_replace('$','',$_POST['tractor_cargo_ded_vh'][$i])); 
		} 
		$i++;
		$trac_count--;
		
	}  
?>
	<tr>
    	<th>Total</th>
		<?php if($liability_checked) { ?>
        <td><?php echo currency_format($tot_tractor_liability_vh); ?></td>
        <td><?php echo currency_format($tot_tractor_liability_ded_vh);?></td>
		<?php } ?>
        <td><?php echo currency_format($tot_tractor_pd_vh);?></td>
        <td><?php echo currency_format($tot_tractor_ph_ded_vh);?></td>
        <td><?php echo currency_format($tot_tractor_cargo_vh);?></td>
        <td><?php echo currency_format($tot_tractor_cargo_ded_vh); ?></td>
    </tr>
</table>
<?php } ?>

<!----------------------------------------------------------------------------------------------->

<?php if($total_trailers > 0 ) { ?>
<table>
	<tr align="left">
		<th colspan="7">Trailer</th>
	</tr>
    <tr align="left">
		<th>Vehicle</th>
		<?php if($liability_checked) { ?>
        <th>Liability</th>
        <th>Liability Deductible</th>
		<?php } ?>
        <th>PD</th>
        <th>PD Deductible</th>
        <th>Cargo</th>
        <th>Cargo Deductible</th>
	</tr>
	<?php for($j = 0; $j < $total_trailers; $j++) { ?>
	<tr>
    	<td><?php echo $j+1;?></td>
		<?php if($liability_checked) { ?>
		<td><?php if(isset($_POST['trailer_liability_vh']))echo $_POST['trailer_liability_vh'][$j]; ?></td>
        <td><?php if(isset($_POST['trailer_liability_ded_vh']))echo $_POST['trailer_liability_ded_vh'][$j]; ?></td>
		<?php } ?>
        <td><?php if(isset($_POST['trailer_pd_vh']))echo $_POST['trailer_pd_vh'][$j]; ?></td>
        <td><?php if(isset($_POST['trailer_ph_ded_vh']))echo $_POST['trailer_ph_ded_vh'][$j]; ?></td>
        <td><?php if(isset($_POST['trailer_cargo_vh']))echo $_POST['trailer_cargo_vh'][$j]; ?></td>
        <td><?php if(isset($_POST['trailer_cargo_ded_vh']))echo $_POST['trailer_cargo_ded_vh'][$j]; ?></td>
	</tr>
<?php 
		//Calculating the total
		if(isset($_POST['trailer_liability_vh']))
		{
		$tot_trailer_liability_vh = $tot_trailer_liability_vh + str_replace(',','',str_replace('$','',$_POST['trailer_liability_vh'][$j])); 
		}
		if(isset($_POST['trailer_liability_ded_vh']))
		{
		$tot_trailer_liability_ded_vh = $tot_trailer_liability_ded_vh + str_replace(',','',str_replace('$','',$_POST['trailer_liability_ded_vh'][$j])); 
		}
		if(isset($_POST['trailer_pd_vh']))
		{
		$tot_trailer_pd_vh = $tot_trailer_pd_vh + str_replace(',','',str_replace('$','',$_POST['trailer_pd_vh'][$j])); 
		}
		if(isset($_POST['trailer_ph_ded_vh']))
		{
		$tot_trailer_ph_ded_vh = $tot_trailer_ph_ded_vh + str_replace(',','',str_replace('$','',$_POST['trailer_ph_ded_vh'][$j])); 
		}
		if(isset($_POST['trailer_cargo_vh']))
		{
		$tot_trailer_cargo_vh = $tot_trailer_cargo_vh + str_replace(',','',str_replace('$','',$_POST['trailer_cargo_vh'][$j])); 
		}
		if(isset($_POST['trailer_cargo_ded_vh']))
		{
		$tot_trailer_cargo_ded_vh = $tot_trailer_cargo_ded_vh + str_replace(',','',str_replace('$','',$_POST['trailer_cargo_ded_vh'][$j])); 
		}	
	} ?>
	<tr>
    	<th>Total</th>
		<?php if($liability_checked) { ?>
    	<td><?php echo currency_format($tot_trailer_liability_vh);?></td>
        <td><?php echo currency_format($tot_trailer_liability_ded_vh);?></td>
		<?php } ?>
        <td><?php echo currency_format($tot_trailer_pd_vh);?></td>
        <td><?php echo currency_format($tot_trailer_ph_ded_vh);?></td>
        <td><?php echo currency_format($tot_trailer_cargo_vh);?></td>
        <td><?php echo currency_format($tot_trailer_cargo_ded_vh);?></td>
    </tr>
</table>
<?php } ?>

<!----------------------------------------------------------------------------------------------->

<?php if($total_truck > 0 ) { ?>
<table>
	<tr align="left">
		<th colspan="7">Truck</th>
	</tr>
    <tr align="left">
		<th>Vehicle</th>
		<?php if($liability_checked) { ?>
        <th>Liability</th>
        <th>Liability Deductible</th>
		<?php } ?>
        <th>PD</th>
        <th>PD Deductible</th>
        <th>Cargo</th>
        <th>Cargo Deductible</th>
	</tr>
	<?php for($i = 0; $i < count($total_truck); $i++) { ?>
	<tr>
    	<td><?php echo $i+1;?></td>
		<?php if($liability_checked) { ?>
		<td><?php if(isset($_POST['liability_vh']))echo $_POST['liability_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['liability_ded_vh']))echo $_POST['liability_ded_vh'][$i]; ?></td>
		<?php } ?>
        <td><?php if(isset($_POST['pd_vh']))echo $_POST['pd_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['ph_ded_vh']))echo $_POST['ph_ded_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['cargo_vh']))echo $_POST['cargo_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['cargo_ded_vh']))echo $_POST['cargo_ded_vh'][$i]; ?></td>
	</tr>
<?php 
		//Calculating the total trucks
		if(isset($_POST['liability_vh']))
		{
		$tot_liability_vh = $tot_liability_vh + str_replace(',','',str_replace('$','',$_POST['liability_vh'][$i])); 
		}
		if(isset($_POST['liability_ded_vh']))
		{
		$tot_liability_ded_vh = $tot_liability_ded_vh + str_replace(',','',str_replace('$','',$_POST['liability_ded_vh'][$i])); 
		}
		if(isset($_POST['pd_vh']))
		{
		$tot_pd_vh = $tot_pd_vh + str_replace(',','',str_replace('$','',$_POST['pd_vh'][$i])); 
		}
		if(isset($_POST['ph_ded_vh']))
		{
		$tot_ph_ded_vh = $tot_ph_ded_vh + str_replace(',','',str_replace('$','',$_POST['ph_ded_vh'][$i])); 
		}
		if(isset($_POST['cargo_vh']))
		{
		$tot_cargo_vh = $tot_cargo_vh + str_replace(',','',str_replace('$','',$_POST['cargo_vh'][$i])); 
		}
		if(isset($_POST['cargo_ded_vh']))
		{
		$tot_cargo_ded_vh = $tot_cargo_ded_vh + str_replace(',','',str_replace('$','',$_POST['cargo_ded_vh'][$i])); 
		}
	} ?>
	<tr>
    	<th>Total</th>
		<?php if($liability_checked) { ?>
        <td><?php echo currency_format($tot_liability_vh);?></td>
        <td><?php echo currency_format($tot_liability_ded_vh);?></td>
		<?php } ?>
        <td><?php echo currency_format($tot_pd_vh);?></td>
        <td><?php echo currency_format($tot_ph_ded_vh);?></td>
        <td><?php echo currency_format($tot_cargo_vh);?></td>
        <td><?php echo currency_format($tot_cargo_ded_vh);?></td>
    </tr>
</table>
<?php } ?>

<!----------------------------------------------------------------------------------------------->
<br>
<fieldset>
<legend>Broker Information</legend>
<table>
	<tr align="left">
		<th>Today's Date</th>
		<th>Time</th>
		<th>Coverage Date</th>
		<th>Person submitting business</th>
	</tr>
	<tr>
		<td><?php echo $_POST['date'];?></td>
		<td><?php echo $_POST['time'];?></td>
		<td><?php echo $_POST['coverage_date'];?></td>
		<td><?php echo $_POST['name'].' '.$_POST['middle_name'].' '.$_POST['last_name'];?></td>
	</tr>
</table>
<table>
	<tr>
    	<th>Cab Number</th>
		<th>Agency Name</th>
		<th>Phone Number</th>
		<th>Cell Ph. Number</th>
	</tr>
	<tr>
    	<td><?php echo $_POST['cab'];?></td>
		<td><?php echo  $_POST['agency'];?></td>
		<td><?php echo $_POST['telephone'][1].'-'.$_POST['telephone'][2].'-'.$_POST['telephone'][3].'-'.$_POST['tel_ext'];?></td>
		<td><?php echo $_POST['cell'][1].'-'.$_POST['cell'][2].'-'.$_POST['cell'][3];?></td>
	</tr>
</table>
<table>
	<tr>
    	<th><?php if($_POST['contact_field'] == "fax") { echo "Fax"; } else if($_POST['contact_field'] == "other") { echo "Other Phone"; } else if($_POST['contact_field'] == "other_email") { echo "Other Email"; } ?></th>
		<th>Email</th>
        <th>Zip</th>
    </tr>
    <tr>
    	<td><?php if($_POST['contact_field'] == "fax") { echo $_POST['fax'][1].'-'.$_POST['fax'][2].'-'.$_POST['fax'][3]; } else if($_POST['contact_field'] == "other") { $_POST['other'][1].'-'.$_POST['other'][2].'-'.$_POST['other'][3]; } else if($_POST['contact_field'] == "other_email") { echo $_POST['broker_other_email']; } ?></td>
        <td><?php echo  $_POST['email'];?></td>
        <td><?php echo $_POST['zip'][1].'-'.$_POST['zip'][2];?></td>
    </tr>
</table>
</fieldset>

<!----------------------------------------------------------------------------------------------->
<br>
<fieldset>
<legend>Insured Information</legend>
<table>
	<tr align="left">
    	<th>Insured</th>
        <th>DBA</th>
        <th>Email</th>				
	</tr>
	<tr>
    	<td><?php echo $_POST['insured_name'].' '.$_POST['insured_middle_name'].' '.$_POST['insured_last_name'];?></td>
        <td><?php echo  $_POST['dba'];?></td>
        <td><?php echo  $_POST['insured_email'];?></td>
	</tr>
</table>
<table>
	<tr align="left">
    	<th>Phone Number</th>
    	<th>Garaging City</th>
		<th>State</th>
    	<th>Zip</th>
	</tr>
	<tr>
    	<td><?php echo $_POST['insured_telephone'][1].'-'.$_POST['insured_telephone'][2].'-'.$_POST['insured_telephone'][3].'-'.$_POST['insured_tel_ext'];?></td>
        <td><?php echo  $_POST['insured_garaging_city'];?></td>
		<td><?php echo  $_POST['insured_state'][0];?></td>
    	<td><?php echo $_POST['insured_zip'][1].'-'.$_POST['insured_zip'][2];?></td>
	</tr>
</table>
<table>
	<tr>
    	<th>Nature of Business</th>
		<th>Commodities Hauled</th>
		<th>Years in Business</th>
    </tr>
	<?php $nature_of_business = isset($_POST['nature_of_business']) ? $_POST['nature_of_business'] : '';
		  $commodities_haulted = isset($_POST['commodities_haulted']) ? $_POST['commodities_haulted'] : '';
		  $nature_business_data = !empty($nature_of_business) ? implode(',', $nature_of_business) : '';
		  $commodities_haulted_data = !empty($commodities_haulted) ? implode(',', $commodities_haulted) : '';?>
    <tr>
    	<td><?php echo  $nature_business_data; ?></td>
		<td><?php echo  $commodities_haulted_data; ?></td>
		<td><?php echo  $_POST['business_years']; ?></td>
    </tr>
</table>
</fieldset>

<!----------------------------------------------------------------------------------------------->
<br>
<fieldset>
<legend>Loss(es) Information</legend>
<?php 
if($this->input->post('losses_need') == 'yes')
{
	$losses_fields = array('losses_liability','losses_pd','losses_cargo','losses_other1','losses_other2');
			
				foreach($losses_fields as $heading)
				{
					$losses_from = FALSE;
					
					$losses_from = $this->input->post($heading.'_from');
					$losses_to = $this->input->post($heading.'_to');
					$losses_amount = $this->input->post($heading.'_amount');
					$losses_company = $this->input->post($heading.'_company');
					$losses_general_agent = $this->input->post($heading.'_general_agent');
					$losses_date_of_loss = $this->input->post($heading.'_date_of_loss');
					if(count($losses_from) > 0 && $losses_from)
					{
						$losses_type = ucwords(str_replace('losses_','',$heading));

?>
<table>
    <tr>
    	<th colspan="4" style="text-align:left">Losses Type</th>
        <td colspan="1"><?php if($heading == 'losses_other1') { echo ucwords(str_replace('losses_','',$heading))." : ".$_POST['losses_other1_specify_other'];} else if($heading == 'losses_other2'){ echo ucwords(str_replace('losses_','',$heading))." : ".$_POST['losses_other2_specify_other'];} else { echo $losses_type; } ?></td>
    </tr>
    <tr>
    	<th colspan="4" style="text-align:left">Number of Losses in last 3 years</th>
        <td><?php echo count($losses_from);?></td>
    </tr>
	<tr>
	  <th colspan="2" >Period of losses</th>
	  <th rowspan="2" >Amount</th>
	  <th rowspan="2" >Company</th>
	<th rowspan="2" >General <br /> Agent</th>
	<th rowspan="2" >Date of Loss</th>
	</tr>
	<tr colspan="2" >
		<th>From (mm/dd/yyyy)</th>
		<th>To (mm/dd/yyyy)</th>
	</tr>

<?php for($i = 0; $i < count($losses_from); $i++) { ?>
	<tr>
		<td><?php echo $losses_from[$i];?></td>
		<td><?php echo $losses_to[$i];?></td>
		<td><?php echo $losses_amount[$i];?></td>
		<td><?php echo $losses_company[$i];?></td>
		<td><?php echo $losses_general_agent[$i];?></td>
		<td><?php echo $losses_date_of_loss[$i];?></td>
	</tr>
					<?php }//Inner Loop Ends
						} //IF Ends?>
</table>

		<?php	} //Outer Loop Ends
	} 
	else
	 echo "No Losses";
	?>
</fieldset>

<!----------------------------------------------------------------------------------------------->
<br>
<fieldset>
<legend>Vehicle Schedule Section</legend>
<table>
	<tr align="left">
		<th>Number of Vehicles</th>
		<td>Tractor : <?php echo $total_tractors; ?></td>
		<td>Truck   : <?php echo $total_truck; ?></td>
	</tr>
</table>
<br/>


<!-- For Tractors and Trailers -->
<?php 
$trailer_start = 0;
for($i = 0; $i < $total_tractors; $i++) { ?>
<!--<div style="border:2px solid #7BB33D;">-->
<table>
	<tr><th colspan="5">Tractor #<?php echo $i+1;?></th></tr>
	<tr align="left">
		<th>Coverage selected:</th>
		<?php if(isset($_POST['vehicle_liability'])) { ?><td>Liability</td><?php } ?>
		<?php if((isset($_POST['tractor_pd_vh'][$i]) && $_POST['tractor_pd_vh'][$i] != '') || (isset($_POST['tractor_ph_ded_vh'][$i]) && $_POST['tractor_ph_ded_vh'][$i] != '')) { ?><td>PD</td><?php } ?>
		<?php if((isset($_POST['tractor_cargo_vh'][$i]) && $_POST['tractor_cargo_vh'][$i] != '') || (isset($_POST['tractor_cargo_ded_vh'][$i]) && $_POST['tractor_cargo_ded_vh'][$i] != '')) { ?><td>Cargo</td><?php } ?>
	</tr>
</table>
<br/>	
<table>
    <tr align="left">
		<!--th>How many trailors associated with this tractor?</th-->
		<th>Radius of Operation</th>
		<?php if(isset($_POST['tractor_radius_of_operation_other'][$i]) && $_POST['tractor_radius_of_operation_other'][$i]!= '') { echo '<th>Specify Other</th>'; } ?>	
	    <th>Any Special Request for Radius</th>
		<th>Number of Axles</th>
	</tr>
	<tr>
		<!--td><?php echo $total_trailers; ?></td-->
		<td><?php if(isset($_POST['tractor_radius_of_operation'][$i])) echo $_POST['tractor_radius_of_operation'][$i]; ?></td>
	    <?php if(isset($_POST['tractor_radius_of_operation_other'][$i]) && $_POST['tractor_radius_of_operation_other'][$i]!= '') echo '<td>'.$_POST['tractor_radius_of_operation_other'][$i].'</td>'; ?>
		<td><?php if(isset($_POST['tractor_request_for_radius_of_operation'][$i])) echo $_POST['tractor_request_for_radius_of_operation'][$i]; ?></td>
		<td><?php if(isset($_POST['tractor_number_of_axles'][$i])) echo $_POST['tractor_number_of_axles'][$i];?></td>
	</tr>
</table>
<table>
	<tr align="left">
		<th>Vehicle Year</th>
		<th>Make Model</th>
		<th>GVW</th>
        <th>VIN</th>
        <?php if(isset($_POST['tractor_pd'][$i]) && $_POST['tractor_pd'][$i] != '') { echo '<th>Special Request</th>'; } ?>
	</tr>
	<tr>
		<td><?php if(isset($_POST['tractor_vehicle_year_vh'][$i])) echo $_POST['tractor_vehicle_year_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['tractor_make_model_vh'][$i]))  echo $_POST['tractor_make_model_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['tractor_gvw_vh'][$i])) echo $_POST['tractor_gvw_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['tractor_vin_vh'][$i])) echo $_POST['tractor_vin_vh'][$i]; ?></td>
        <?php if(isset($_POST['tractor_pd'][$i]) && $_POST['tractor_pd'][$i] != '') { ?>
			<td><?php if(isset($_POST['tractor_value_amount_vh'][$i])) echo $_POST['tractor_value_amount_vh'][$i]; ?></td>
		<?php } ?>
	</tr>
</table>
<?php if(isset($_POST['vehicle_liability']) && $_POST['vehicle_liability'] != '') { ?>
<table>
    <tr>
    	<?php if(isset($_POST['tractor_liability_vh'][$i]) && $_POST['tractor_liability_vh'][$i] != '') echo '<th>Liability</th>'; ?>
        <?php if(isset($_POST['tractor_liability_ded_vh'][$i]) && $_POST['tractor_liability_ded_vh'][$i] != '') echo '<th>Liability Deductible</th>'; ?>
		<?php if(isset($_POST['tractor_um']) && $_POST['tractor_um'][$i] != '') echo '<th>UM</th>'; ?>
		<?php if(isset($_POST['tractor_pip']) && $_POST['tractor_pip'][$i] != '') echo '<th>PIP</th>' ; ?>
    </tr>
    <tr>
		<?php if(isset($_POST['tractor_liability_vh'][$i]) && $_POST['tractor_liability_vh'][$i] != '') echo '<td>'.$_POST['tractor_liability_vh'][$i].'</td>'; ?>
        <?php if(isset($_POST['tractor_liability_ded_vh'][$i]) && $_POST['tractor_liability_ded_vh'][$i] != '') echo '<td>'.$_POST['tractor_liability_ded_vh'][$i].'</td>'; ?>
		<?php if(isset($_POST['tractor_um']) && $_POST['tractor_um'][$i] != '') echo '<td>'.$_POST['tractor_um'][$i].'</td>'; ?>
		<?php if(isset($_POST['tractor_pip']) && $_POST['tractor_pip'][$i] != '') echo '<td>'.$_POST['tractor_pip'][$i].'</td>'; ?>
    </tr>
</table>
<?php }  ?>

<?php if((isset($_POST['tractor_pd_vh'][$i]) && $_POST['tractor_pd_vh'][$i] != '') || (isset($_POST['tractor_ph_ded_vh'][$i]) && $_POST['tractor_ph_ded_vh'][$i] != '')) {?>
<table>
	<tr>
		<?php if(isset($_POST['tractor_pd_vh'][$i]) && $_POST['tractor_pd_vh'][$i] != '') echo '<th>PD</th>'; ?>
        <?php if(isset($_POST['tractor_ph_ded_vh'][$i]) && $_POST['tractor_ph_ded_vh'][$i] != '') echo '<th>PD Deductible</th>'; ?>
	</tr>
	<tr>
		<?php if(isset($_POST['tractor_pd_vh'][$i]) && $_POST['tractor_pd_vh'][$i] != '') echo '<td>'.$_POST['tractor_pd_vh'][$i].'</td>'; ?>
        <?php if(isset($_POST['tractor_ph_ded_vh'][$i]) && $_POST['tractor_ph_ded_vh'][$i] != '') echo '<td>'.$_POST['tractor_ph_ded_vh'][$i].'</td>'; ?>
	</tr>
</table>
<?php } ?>

<?php if((isset($_POST['tractor_cargo_vh'][$i]) && $_POST['tractor_cargo_vh'][$i] != '') || (isset($_POST['tractor_cargo_ded_vh'][$i]) && $_POST['tractor_cargo_ded_vh'][$i] != '')) { ?>
<table>
	<tr>
		<?php if(isset($_POST['tractor_cargo_vh'][$i]) && $_POST['tractor_cargo_vh'][$i] != '') echo '<th>Cargo</th>'; ?>
        <?php if(isset($_POST['tractor_cargo_ded_vh'][$i]) && $_POST['tractor_cargo_ded_vh'][$i] != '') echo '<th>Cargo Deductible</th>'; ?>
	</tr>
	<tr>
		<?php if(isset($_POST['tractor_cargo_vh'][$i]) && $_POST['tractor_cargo_vh'][$i] != '') echo '<td>'.$_POST['tractor_cargo_vh'][$i].'</td>'; ?>
        <?php if(isset($_POST['tractor_cargo_ded_vh'][$i]) && $_POST['tractor_cargo_ded_vh'][$i] != '') echo '<td>'.$_POST['tractor_cargo_ded_vh'][$i].'</td>'; ?>		
	</tr>
</table>
<?php } ?>
<table>
	<tr>
		<th>Commodities hauled</th>
		<th>Number of Trailers Pulled with this tractor</th>
	</tr>
	<tr>
		<td><?php if(isset($_POST['commodities_haulted_tractor'][$i])) echo $_POST['commodities_haulted_tractor'][$i]; ?></td>
		<td><?php if(isset($_POST['vehicle_trailer_no'][$i])) echo $_POST['vehicle_trailer_no'][$i]; ?></td>
	</tr>
</table>
<!--<table>
	<tr>
    	<th>Refrigerated Breakdown</th>
        <th>Deductible</th>
        <th>Type of Cargo Carried</th>
    </tr>
	<tr>
    	<td><?php echo isset($_POST['tractor_refrigerated_breakdown'][$i])? "YES": "NO";?></td>
        <td><?php echo isset($_POST['tractor_refrigerated_breakdown'][$i])? $_POST['tractor_deductible'][$i]: "";?></td>
        <td><?php echo isset($_POST[($i+1).'tractor_cargo_specifier'])? implode(',',$_POST[($i+1).'tractor_cargo_specifier']): "";?></td>
    </tr>
</table>-->
<br>
<?php 
if(isset($_POST['vehicle_trailer_no'][$i])) { 
	$tractor_trailer = $_POST['vehicle_trailer_no'][$i] + $trailer_start; 
} else { 
	$tractor_trailer = '0'; 
}
$trailer_number = 1;

$trailer_start_in = $trailer_start;
for($j = $trailer_start_in; $j < $tractor_trailer; $j++) { ?>
<!--<div style="border:2px solid #F60;">-->
<table>
	<tr><th colspan="5">Trailer #<?php echo $trailer_number;?></th></tr>
	<tr align="left">
		<th>Coverage selected:</th>
		<?php if(isset($_POST['vehicle_liability'])) { ?><td>Liability</td><?php } ?>
		<?php if((isset($_POST['trailer_pd_vh'][$j]) && $_POST['trailer_pd_vh'][$j] != '') || (isset($_POST['trailer_ph_ded_vh'][$j]) && $_POST['trailer_ph_ded_vh'][$j] != '')) { ?><td>PD</td><?php } ?>
		<?php if((isset($_POST['trailer_cargo_vh'][$j]) && $_POST['trailer_cargo_vh'][$j] != '') || (isset($_POST['trailer_cargo_ded_vh'][$j]) && $_POST['trailer_cargo_ded_vh'][$j] != '') || (isset($_POST['trailer_deductible'][$j]) && $_POST['trailer_deductible'][$j] != '')) { ?><td>Cargo</td><?php } ?>
	</tr>
</table>
<table>
    <tr align="left">
		<th>Vehicle Year</th>
		<th>Make Model</th>
		<th>GVW</th>
        <th>VIN</th>
		<th>Trailer Type</th>
		<th>Owned Vehicle</th>
        <!--<th>Special Request</th>-->
	</tr>
	<tr>
		<td><?php if(isset($_POST['trailer_vehicle_year_vh'][$j])) echo $_POST['trailer_vehicle_year_vh'][$j]; ?></td>
        <td><?php if(isset($_POST['trailer_make_model_vh'][$j])) echo $_POST['trailer_make_model_vh'][$j]; ?></td>
        <td><?php if(isset($_POST['trailer_gvw_vh'][$j])) echo $_POST['trailer_gvw_vh'][$j]; ?></td>
        <td><?php if(isset($_POST['trailer_vin_vh'][$j])) echo $_POST['trailer_vin_vh'][$j]; ?></td>
		<td><?php if(isset($_POST['trailer_type'][$j])) echo $_POST['trailer_type'][$j]; ?></td>
		<td><?php if(isset($_POST['trailer_owned_vehicle'][$j])) echo $_POST['trailer_owned_vehicle'][$j]; ?></td>
        <!--<td><?php if(isset($_POST['trailer_value_amount_vh'][$j])) echo $_POST['trailer_value_amount_vh'][$j]; ?></td>-->
	</tr>
</table>

<table>
    <tr>
    	<?php if(isset($_POST['vehicle_liability']) && $_POST['vehicle_liability'] != '') {?>
			<?php if(isset($_POST['trailer_liability_vh'][$j]) && $_POST['trailer_liability_vh'][$j] != '') echo '<th>Liability</th>'; ?>
			<?php if(isset($_POST['trailer_liability_ded_vh'][$j]) && $_POST['trailer_liability_ded_vh'][$j] != '') echo '<th>Liability Deductible</th>'; ?>
		<?php } ?>
		<?php if((isset($_POST['trailer_pd_vh'][$j]) && $_POST['trailer_pd_vh'][$j] != '') || (isset($_POST['trailer_ph_ded_vh'][$j]) && $_POST['trailer_ph_ded_vh'][$j] != '')) {?>
			<?php if(isset($_POST['trailer_pd_vh'][$j]) && $_POST['trailer_pd_vh'][$j] != '') echo '<th>PD</th>'; ?>
			<?php if(isset($_POST['trailer_ph_ded_vh'][$j]) && $_POST['trailer_ph_ded_vh'][$j] != '') echo '<th>PD Deductible</th>'; ?>
		<?php } ?>
    </tr>
    <tr>
		<?php if(isset($_POST['vehicle_liability']) && $_POST['vehicle_liability'] != '') {?>
			<?php if(isset($_POST['trailer_liability_vh'][$j]) && $_POST['trailer_liability_vh'][$j] != '') echo '<td>'.$_POST['trailer_liability_vh'][$j].'</td>'; ?>
			<?php if(isset($_POST['trailer_liability_ded_vh'][$j]) && $_POST['trailer_liability_ded_vh'][$j] != '') echo '<td>'.$_POST['trailer_liability_ded_vh'][$j].'</td>'; ?>
		<?php } ?>
		<?php if((isset($_POST['trailer_pd_vh'][$j]) && $_POST['trailer_pd_vh'][$j] != '') || (isset($_POST['trailer_ph_ded_vh'][$j]) && $_POST['trailer_ph_ded_vh'][$j] != '')) {?>
			<?php if(isset($_POST['trailer_pd_vh'][$j]) && $_POST['trailer_pd_vh'][$j] != '') echo '<td>'.$_POST['trailer_pd_vh'][$j].'</td>'; ?>
			<?php if(isset($_POST['trailer_ph_ded_vh'][$j]) && $_POST['trailer_ph_ded_vh'][$j] != '') echo '<td>'.$_POST['trailer_ph_ded_vh'][$j].'</td>'; ?>
		<?php } ?>
	</tr>
</table>
<?php if((isset($_POST['trailer_cargo_vh'][$j]) && $_POST['trailer_cargo_vh'][$j] != '') || (isset($_POST['trailer_cargo_ded_vh'][$j]) && $_POST['trailer_cargo_ded_vh'][$j] != '') || (isset($_POST['trailer_deductible'][$j]) && $_POST['trailer_deductible'][$j] != '')) { ?>
<table>
    <tr align="left">
		<?php if(isset($_POST['trailer_cargo_vh'][$j]) && $_POST['trailer_cargo_vh'][$j] != '') { echo '<th>Cargo</th>'; } ?>
        <?php if(isset($_POST['trailer_cargo_ded_vh'][$j]) && $_POST['trailer_cargo_ded_vh'][$j] != '') { echo '<th>Cargo Deductible</th>'; } ?>
		<?php if(isset($_POST['trailer_refrigerated_breakdown'][$j]) && $_POST['trailer_refrigerated_breakdown'][$j] != '') { echo '<th>Refrigeration Breakdown</th>'; } ?>
		<?php if(isset($_POST['trailer_deductible'][$j]) && $_POST['trailer_deductible'][$j] != '') { echo '<th>Deductible</th>'; } ?>
	</tr>
	<tr>
		<td><?php if(isset($_POST['trailer_cargo_vh'][$j]) && $_POST['trailer_cargo_vh'][$j] != '') echo $_POST['trailer_cargo_vh'][$j]; ?></td>
        <td><?php if(isset($_POST['trailer_cargo_ded_vh'][$j]) && $_POST['trailer_cargo_ded_vh'][$j] != '') echo $_POST['trailer_cargo_ded_vh'][$j]; ?></td>
        <td><?php echo isset($_POST['trailer_refrigerated_breakdown'][$j]) && $_POST['trailer_refrigerated_breakdown'][$j] != '' ? "YES": "NO";?></td>
        <td><?php echo isset($_POST['trailer_deductible'][$j]) && $_POST['trailer_deductible'][$j] != '' ? $_POST['trailer_deductible'][$i]: "";?></td>
	</tr>
</table>
<?php } ?>

<!--</div>--><br>
<?php $trailer_start++; 
$trailer_number ++;
}//Inner Loop Ends (Trailors) ?>
<!--</div>-->
<br>
<?php } //Outer Loop Ends (Tractors) ?>
<!--END For Tractors and Trailers -->


<!-- For Trucks-->
<?php for($i = 0; $i < $total_truck; $i++) { ?>
<!--<div style="border:2px solid #3DAAE9;">-->
<table>
	<tr><th colspan="5">Truck #<?php echo $i+1;?></th></tr>
	<tr align="left">
		<th>Coverage selected:</th>
		<?php if(isset($_POST['vehicle_liability'])) { echo '<td>Liability</td>'; } ?>
		<?php if((isset($_POST['pd_vh'][$i]) && $_POST['pd_vh'][$i] != '') || (isset($_POST['ph_ded_vh'][$i]) && $_POST['ph_ded_vh'][$i] != '')) { echo '<td>PD</td>'; } ?>
		<?php if((isset($_POST['cargo_vh'][$i]) && $_POST['cargo_vh'][$i] != '') || (isset($_POST['cargo_ded_vh'][$i]) && $_POST['cargo_ded_vh'][$i] != '')  || (isset($_POST['deductible'][$i]) && $_POST['deductible'][$i] != '')) { echo '<td>Cargo</td>'; } ?>
	</tr>
</table>
<br/>	
<table>
    <tr align="left">
		<th>Radius of Operation</th>
		<?php if (isset($_POST['truck_radius_of_operation_other'][$i]) && $_POST['truck_radius_of_operation_other'][$i] != '') { echo '<th>Specify Other</th>'; } ?>
		<th>Any Special Request for Radius</th>
        <th>Number of Axles</th>
	</tr>
	<tr>
		<td><?php if(isset($_POST['truck_radius_of_operation'])) echo $_POST['truck_radius_of_operation'][$i]; ?></td>
		<?php if (isset($_POST['truck_radius_of_operation_other'][$i]) && $_POST['truck_radius_of_operation_other'][$i] != '') echo  '<td>'.$_POST['truck_radius_of_operation_other'][$i].'</td>'; ?></td>
		<td><?php if(isset($_POST['truck_request_for_radius_of_operation'])) echo $_POST['truck_request_for_radius_of_operation'][$i]; ?></td>
        <td><?php if(isset($_POST['truck_number_of_axles'])) echo $_POST['truck_number_of_axles'][$i]; ?></td>
	</tr>
</table>
<table>
	<tr align="left">
		<th>Vehicle Year</th>
		<th>Make Model</th>
		<th>GVW</th>
        <th>VIN</th>
		<?php if(isset($_POST['truck_vehicle_cargo'][$i])) { echo '<th>Special req.</th>'; } ?>
	</tr>
	<tr>
		<td><?php if(isset($_POST['vehicle_year_vh']))  echo $_POST['vehicle_year_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['make_model_vh']))  echo $_POST['make_model_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['gvw_vh'])) echo $_POST['gvw_vh'][$i]; ?></td>
        <td><?php if(isset($_POST['vin_vh'])) echo $_POST['vin_vh'][$i]; ?></td>
		<?php if(isset($_POST['truck_vehicle_cargo'][$i])) { ?>
			<td><?php if(isset($_POST['value_amount_vh'])) echo $_POST['value_amount_vh'][$i]; ?></td>
		<?php } ?>
	</tr>
</table>

<?php if(isset($_POST['vehicle_liability']) && $_POST['vehicle_liability'] != '') { ?>
<table>
    <tr>
    	<?php if(isset($_POST['liability_vh'][$i]) && $_POST['liability_vh'][$i] != '') echo '<th>Liability</th>'; ?>
        <?php if(isset($_POST['liability_ded_vh'][$i]) && $_POST['liability_ded_vh'][$i] != '') echo '<th>Liability Deductible</th>'; ?>
		<?php if(isset($_POST['truck_um'][$i]) && $_POST['truck_um'][$i] != '') echo '<th>UM</th>'; ?>
		<?php if(isset($_POST['truck_pip'][$i]) && $_POST['truck_pip'][$i] != '') echo '<th>PIP</th>'; ?>
    </tr>
    <tr>
		<?php if(isset($_POST['liability_vh'][$i]) && $_POST['liability_vh'][$i] != '') echo '<td>'.$_POST['liability_vh'][$i].'</td>'; ?>
        <?php if(isset($_POST['liability_ded_vh'][$i]) && $_POST['liability_ded_vh'][$i] != '') echo '<td>'.$_POST['liability_ded_vh'][$i].'</td>'; ?>
		<?php if(isset($_POST['truck_um'][$i]) && $_POST['truck_um'][$i] != '') echo '<td>'.$_POST['truck_um'][$i].'</td>'; ?>
		<?php if(isset($_POST['truck_pip'][$i]) && $_POST['truck_pip'][$i] != '') echo '<td>'.$_POST['truck_pip'][$i].'</td>'; ?>
	</tr>
</table>
<?php } ?>

<?php if((isset($_POST['pd_vh'][$i]) && $_POST['pd_vh'][$i] != '') || (isset($_POST['ph_ded_vh'][$i]) && $_POST['ph_ded_vh'][$i] != '')) { ?>
<table>
	<tr>
		<?php if(isset($_POST['pd_vh'][$i]) && $_POST['pd_vh'][$i] != '') echo '<th>PD</th>'; ?>
        <?php if(isset($_POST['ph_ded_vh'][$i]) && $_POST['ph_ded_vh'][$i] != '') echo '<th>PD Deductible</th>'; ?>
	</tr>
	<tr>
		<?php if(isset($_POST['pd_vh'][$i]) && $_POST['pd_vh'][$i] != '') echo '<td>'.$_POST['pd_vh'][$i].'</td>'; ?>
        <?php if(isset($_POST['ph_ded_vh'][$i]) && $_POST['ph_ded_vh'][$i] != '') echo '<td>'.$_POST['ph_ded_vh'][$i].'</td>'; ?>
	</tr>
</table>
<?php } ?>

<?php if((isset($_POST['cargo_vh'][$i]) && $_POST['cargo_vh'][$i] != '') || (isset($_POST['cargo_ded_vh'][$i]) && $_POST['cargo_ded_vh'][$i] != '') || (isset($_POST['deductible'][$i]) && $_POST['deductible'][$i] != '')) { ?>
<table>
	<tr>
		<?php if(isset($_POST['cargo_vh'][$i]) && $_POST['cargo_vh'][$i] != '') echo '<th>Cargo</th>'; ?>
        <?php if(isset($_POST['cargo_ded_vh'][$i]) && $_POST['cargo_ded_vh'][$i] != '') echo '<th>Cargo Deductible</th>'; ?>
    	<?php if(isset($_POST['refrigerated_breakdown'][$i]) && $_POST['refrigerated_breakdown'][$i] != '') echo '<th>Refrigeration Breakdown</th>'; ?>
        <?php if(isset($_POST['deductible'][$i]) && $_POST['deductible'][$i] != '') echo '<th>Deductible</th>'; ?>
    </tr>
    <tr>
		<?php if(isset($_POST['cargo_vh'][$i]) && $_POST['cargo_vh'][$i] != '') echo '<td>'.$_POST['cargo_vh'][$i].'</td>'; ?>
        <?php if(isset($_POST['cargo_ded_vh'][$i]) && $_POST['cargo_ded_vh'][$i] != '') echo '<td>'.$_POST['cargo_ded_vh'][$i].'</td>'; ?>
    	<?php echo isset($_POST['refrigerated_breakdown'][$i]) && $_POST['refrigerated_breakdown'][$i] != ''? "<td>YES</td>": "<td>NO</td>";?>
        <?php echo isset($_POST['deductible'][$i]) && $_POST['deductible'][$i] != ''? '<td>'.$_POST['deductible'][$i].'</td>': "";?>
    </tr>
</table>
<?php } ?>
<table>
	<tr>
		<th>Commodities hauled</th>
        <th>Do you pull anything with this truck?</th>
	</tr>
	<tr>
		<td><?php if(isset($_POST['commodities_haulted_truck']))echo $_POST['commodities_haulted_truck'][$i]; ?></td>
		<td><?php if(isset($_POST['put_on_truck']))echo $_POST['put_on_truck'][$i]; ?></td>
	</tr>
</table>
<!--</div>-->
<br>
<?php } //Loop Ends (Trucks) ?>
<!-- END For Trucks-->
</fieldset>

<!----------------------------------------------------------------------------------------------->


<fieldset>
<legend>Schedule of Dirvers</legend>
<table>
	<tr align="left">
		<th>Number of Drivers</th>
		<td><?php echo $total_driver; ?></td>
    </tr>
</table>
<?php if ($total_driver > 0)  { ?>
<table>
	<tr align="left">
		<th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
		<th>Driver License number</th>
		<th>License Class</th>
		<th>Date Hired</th>
		<th>DOB</th>
		<th>Years Class A license</th>
		<th>License Issue State</th>
	</tr>
    <?php for($di =0; $di < $total_driver; $di++){ 	?>
	<tr>
		<td><?php echo $_POST['driver_name'][$di]; ?></td>
        <td><?php echo $_POST['driver_middle_name'][$di]; ?></td>
        <td><?php echo $_POST['driver_last_name'][$di]; ?></td>
        <td><?php echo $_POST['driver_license'][$di]; ?></td>
		<td><?php echo $_POST['driver_license_class'][$di]; ?></td>
		<td><?php echo $_POST['driver_license_issue_date'][$di];?></td>
		<td><?php echo $_POST['driver_dob'][$di];?></td>
		<td><?php echo $_POST['driver_class_a_years'][$di];?></td>
		<td><?php echo $_POST['driver_license_issue_state'][$di] ?></td>
    </tr>
    <?php } ?>
</table>
<?php } ?>
</fieldset>

<!----------------------------------------------------------------------------------------------->
<br/>
<fieldset>
<legend>Number of Owner</legend>
<table>
	<tr align="left">
		<th>Number of Owner?</th>
		<td><?php echo $total_owner;?></td>
    </tr>
</table>

<?php if($total_owner > 0 ) { ?>
<table>
	<tr align="left">
		<th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
		<th>Driver</th>
		<th>Driver License number</th>
		<th>License Issue State</th>
	</tr>
    <?php for($i = 0; $i < count($_POST['owner_name']); $i++) { ?>
	<tr>
		<td><?php echo $_POST['owner_name'][$i]; ?></td>
        <td><?php echo $_POST['owner_middle_name'][$i]; ?></td>
        <td><?php echo $_POST['owner_last_name'][$i]; ?></td>
        <td><?php echo $_POST['owner_is_driver'][$i]; ?></td>
        <td><?php echo $_POST['owner_license'][$i]; ?></td>
		<td><?php echo $_POST['license_issue_state'][$i] ?></td>
	</tr>
    <?php } ?>
</table>
<?php } ?>
</fieldset>

<!----------------------------------------------------------------------------------------------->
<br/>
<fieldset>
<legend>Filings Section</legend>

<table>
	<tr align="left">
		<th>Number of Filings</th>
		<td><?php echo $total_filings;?></td>
    </tr>
</table>

<?php if( $total_filings > 0 ) { ?>
<table>
	<tr align="left">
		<th>Filing Type </th>
		<th>Filing #</th>
	</tr>
    <?php for($i = 0; $i < $total_filings; $i++) { ?>
	<tr>
		<td><?php echo $_POST['filing_type'][$i]; ?></td>
        <td><?php echo $_POST['filing_numbers'][$i]; ?></td>
	</tr>
    <?php } ?>
</table>
<?php  } ?>
</fieldset>
<br/>
<fieldset>
<legend >Comments Section</legend><?php //echo $insured->Comments_request;?>
<?php echo $_POST['Comments_request']; ?>
</fieldset>

<?php 
if (isset($mvr_link) && $mvr_link != '')
	echo $mvr_link;
if (isset($losses_link) && $losses_link != '')
	echo $losses_link;

}//End Post IF
?>