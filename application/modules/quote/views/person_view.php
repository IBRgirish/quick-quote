<input type="hidden" name="baseurls" id="baseurls" value="<?php echo base_url(); ?>" /> 
<fieldset class="insured_info_boxs ">



                    <!-- Form Name -->
                    <legend><span class="characters" id="p_info">Person submitting business info</span><span class="characters" id="p_info_1" style="display:none">PSB</span></legend>
                    <?php
									if(isset($user_quote_data) && !empty($user_quote_data)) {
									$broker_email = $user_quote_data->email;
									} elseif(isset($quote[0]->email)) {
									$broker_email = $quote[0]->email;
									}else { 
									$broker_email = '';
									}
									?>
                    <input id="email" name="email_u" type="hidden"  value="<?php echo isset($broker_email)?$broker_email:''?>" >
                      
                    <div class="row-fluid" ng-controller="personCtrl" id="person_cntrl">
                    <div class="width105"><!--handleClick(message) -->
                       <div ="row-fluid">
                          <label><span class="numbers"></span><input value="yes" ng-model="message" type="checkbox" name="infosubmitted" id="infosubmitted" ng-change="handleClick()"  ng-checked="message"/> Information submitted by broker  </label><!-- Person submitting business</label>-->
                           </div>
                           
                          <div ="row-fluid ">
                          <label><span class="numbers"></span> Person submitting business</label>
                           <select name="infosubmitted" class="span3"   ng-model="person_infos.quote_id" ng-change="getSinglepersons(person_infos.quote_id)">
                                   <option value=""> Select</option>     
                                  <?php if(count($get_all_broker)>0){
									 
									 	foreach($get_all_broker as $brokers){
											if(!empty($brokers->fistname)){											
												if($brokers->assigned_agency == $this->session->userdata('member_id')){												
										?>
										
                                      <option value="<?php echo $brokers->quote_id ?>" ><?php echo $brokers->fistname.' '.$brokers->middlename.' '.$brokers->lastname ?></option>       
                                        <?php											
												}else{
										?>
									  <option value="<?php echo $brokers->quote_id ?>" ><?php echo $brokers->fistname.' '.$brokers->middlename.' '.$brokers->lastname ?></option>       		
										<?php			} } }
									 }?>
                                                       
                                   </select>
                          </div>
                            <div class="row-fluid widths_50">  
                            <input type="hidden" name="person_id" ng-model="person_infos.person_id" value="{{person_infos.person_id}}"/>
                                <div class="span3 lightblue">
                                    <label id="label_per_fistname"><span class="numbers"></span>First name</label>
                                    <input id="fistname" name="per_fistname" type="text" class="span12 myclass" ng-model="person_infos.fistname" ng-change="quick_fname(person_infos.fistname)" >
                                </div>
                                <div class="span3 lightblue">
                                    <label id="label_per_middlename">Middle name</label>
                                    <input  id="middlename" name="per_middlename" type="text" class="span12 myclass"   ng-model="person_infos.middlename" ng-change="quick_mname(person_infos.middlename)">
                                </div>
                                <div class="span3 lightblue">
                                    <label id="label_per_lastname">Last Name</label>
                                    <input   id="lastname" name="per_lastname" type="text" class="span12 myclass" ng-model="person_infos.lastname" ng-change="quick_lname(person_infos.lastname)">
                                </div>	
                           </div>
                           <div class="row-fluid widths_50 margin-14">
                                <table id="table" class="tables">
                                    <tr>
									 <td id="label_per_Street"><span class="numbers"></span>Street</td>	
                                     <td id="label_per_city"><span class="numbers"></span>City</td>	
                                     <td id="label_per_state"><span class="numbers"></span>State</td>	
                                     <td id="label_per_county"><span class="numbers"></span>County</td>
                                     <td id="label_per_zip"><span class="numbers"></span>Zip</td>	
                                     <td id="label_per_pri_country"><span class="numbers"></span>Country</td>	                                     
                                     </tr>
                                     <tr >                                    
                                     <td style="width:28%"> <input  id="per_Street" maxlength="80" name="per_Street"  type="text"  class="span12 first_name  myclass" ng-model="person_infos.street" > </td>
                                     <td style="width:18%"><input id="person_city" maxlength="80"  name="per_city" type="text"  class="span12  myclass zip_code_p" ng-model="person_infos.city"> </td>
                                     <td> 
                                      <?php  echo get_state_dropdown('per_state', $selected_val='CA', $attr='class="span9 zip_code_p" id="person_state" ng-model="person_infos.state"') ?>
                                                              
                                     </select>
                                     </td>
                                     <td id="per_county_td">
                                     <?php  $city=!empty($person_info[0]->city)?$person_info[0]->city:'';
									        $state=!empty($person_info[0]->state)?$person_info[0]->state:'';										 
									  ?>
                                      <?php echo getCounty('per_county', 'class="span12" onChange="getZipCodes_p(this.value)" id="per_county"  ng-model="person_infos.county"',"$city","$state"); ?>                                
                                 
                                    <!-- <input id="person_county" maxlength="80" name="per_county" placeholder="" type="text"  class="span12  myclass zip_code_p" ng-model="person_infos.county" > -->
                                     
                                     </td>
                                     <td id="per_zip_td">
                                      <?php echo getZipCode('per_zip', 'class="span12" id="per_zip"  ng-model="person_infos.zip"',"$city","$state"); ?>                                
                                 
                                    <!-- <input id="person_zip" maxlength="80" name="per_zip" placeholder="" type="text"  class="span12  myclass" ng-model="person_infos.zip" >-->
                                     
                                     </td>
                                     <td>
                                     <?php echo getCountryDropDown1('per_pri_country', 'class="span12" id="person_pri_country" ng-model="person_infos.country"','',''); ?>
                                   
                                     </td>
                                    
                                     </tr>
                                  </table> 
                            </div>	
                           <div class="row-fluid widths_51">
                           
                             <table id="table" class="tables">
                                    <label >  </label>	
                                    <tr>
                                     <td style="width:15%" id="label_per_phone_type"><span class="numbers"></span>Phone Type</td>	
                                     <td style="width:18%" id="label_per_code_type"><span class="numbers"></span>Country Code</td>	
                                     <td id="label_per_area_code"><span class="numbers"></span>Area code</td>	
                                     <td id="label_per_prefix"><span class="numbers"></span>Prefix</td>
                                     <td id="label_per_suffix"><span class="numbers"></span>Suffix</td>	
                                     <td style="width: 14%;" id="label_per_extension"><span class="numbers" ></span>Extension</td>
                                     <td style="width:20%" id="label_per_country"> <span class="numbers"></span>Priority sequence</td>                                  
                                     <td style="color:white"> action</td>
                                     </tr>
                                     <tr ng-repeat="person_phone in person_phones">   
                                     <input type="hidden" name="per_phone_id[]" ng-model="person_phone.phone_id" value="{{person_phone.phone_id}}"/>                                 
                                     <td>
                                     <?php echo getCatlogValue('per_phone_type[]', 'class="span11" id="person_phone_type" ng-model="person_phone.phone_type"','',35,''); ?>
                                       
                                     </td>
                                     <td>
                                     <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
									   <?php
                                       $selected =  '1'; 
                                        echo getCountryCodeDropDown($name='per_code_type[]', $att='class="span11" id="person_code_type" ng-model="person_phone.code_type"', 'code', $selected); //'.$highlighted.'
									 ?>
                                     <?php //echo getCatlogValue('per_code_type[]', 'class="span12" id="person_code_type" ng-model="person_phone.code_type"','',36,''); ?>
                                     
                                     <td> 
                                     <input  id="person_area_code"  name="per_area_code[]" maxlength="3" size="3" placeholder="Area"pattern="[0-9]+" type="text"  class="span10 first_name myclass" ng-model="person_phone.area_code" > </td>
                                     <td>
                                     <input id="person_prefix"  name="per_prefix[]" type="text" maxlength="3" size="3" placeholder="Prefix"pattern="[0-9]+" class="span10  myclass" ng-model="person_phone.prefix">
                                     </td>
                                     <td> 
                                     <input id="person_suffix"   name="per_suffix[]" type="text" maxlength="4" size="4" placeholder="Suffix"pattern="[0-9]+" class="span10  myclass" ng-model="person_phone.suffix">
                                     </td>
                                      <td> 
                                     <input id="person_ext"   name="per_extension[]" type="text" maxlength="5" size="5" placeholder="Ext" pattern="[0-9]+" class="span10  myclass" ng-model="person_phone.extension">
                                     </td>
                                     <td>
                                     <?php echo getCatlogValue('per_country[]', 'class="span8" id="pri_country_copy" ng-model="person_phone.pri_country"','1',37,''); ?>
                                    
                                     </td>                                    
                                   
                                    <td>
                                     <a href="javscript:;" ng-click="addRowPhone('person_phone')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                     <a href="javscript:;" ng-click="remove_row_phone($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                   </tr>
                                  </table>

                            </div>
                            
                            
                            
                            <div class="row-fluid widths_49">  
							<table id="table" class="tables">
                                  
                                    <tr>
                                     		
									 <td><span class="numbers" id="label_per_email_type"></span>Email Type</td>	
                                     <td><span class="numbers" id="label_per_email"></span>Email address</td>	
                                     <td><span class="numbers" id="label_per_priority_email"></span>Priority sequence</td>	
                                     <td></td>
                                     </tr>
                                     <tr ng-repeat="person_email in person_emails">
                                     <input type="hidden" name="per_email_id[]" ng-model="person_email.email_id" value="{{person_email.email_id}}"/>
                                     <td>
                                     <?php echo getCatlogValue('per_email_type[]', 'class="span8" id="person_email_type" ng-model="person_email.email_type"','',34,''); ?>
                                     
                                     </td>	
                                     <td style="width:25%">
                                     <input id="person_email" name="per_email[]" type="text" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" class="span12 myclass"  ng-model="person_email.email"></td>	
                                     <td> 
                                     <?php echo getCatlogValue('per_priority_email[]', 'class="span8" id="person_priority_email" ng-model="person_email.priority_email"','',37,''); ?>
                                     </td>	                                    
                                     <td> 
                                      <a href="javscript:;" ng-click="addRowEmail('person_email')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                      <a href="javscript:;" ng-click="remove_row_email($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                    </tr>
                                  </table>
                                </div>   
                              <!-- <div class="fluid-row"> 
                               <span class="numbers"></span> 
                                  <label>Do you have any fax number?</label>
                                   <select name="in_per_fax" id="in_person_fax" class="span2 span_8" ng-model="person_infos.fax">
                                    <option>Yes</option>
                                   <option selected="selected">No</option>
                                 </select>
                                </div>-->
                            <div class="row-fluid widths_50">
                              <table id="table" class="tables">                                   	
                                    <tr>		
									 <td style="width:15%" id="label_per_fax_type"><span class="numbers"></span>Fax Type</td>	
                                     <td style="width:18%" id="label_per_code_type_fax"><span class="numbers"></span>Country Code</td>	
                                     <td style="width:15%" id="label_per_area_code_fax"><span class="numbers"></span>Area code</td>	
                                     <td id="label_per_prefix_fax"><span class="numbers"></span>Prefix</td>
                                     <td id="label_per_suffix_fax"><span class="numbers"></span>Suffix</td>	
                                     <td style="width:20%" id="label_per_email_type"><span class="numbers"></span>Priority sequence</td>
                                     <td></td>
                                     </tr>
                                     <tr ng-repeat="person_fax in person_faxs">
                                     <input type="hidden" name="per_fax_id[]" ng-model="person_fax.fax_id" value="{{person_fax.fax_id}}"/>
                                     <td>
                                      <?php echo getCatlogValue('per_fax_type[]', 'class="span11" id="person_fax_type" ng-model="person_fax.fax_type"','',38,''); ?>
                                     </td>
                                     <td>
                                     
                                     <?php  $selected =  '1'; 
                                        echo getCountryCodeDropDown($name='per_code_type_fax[]', $att='class="span11" id="person_code_type_fax" ng-model="person_fax.code_type_fax"', 'code', $selected); //'.$highlighted.'
									 ?>
                                     
                                     <td> 
                                      <input  name="per_area_code_fax[]" maxlength="3" size="3" placeholder="Area"pattern="[0-9]+" id="person_area_code_fax" type="text"  class="span10 first_name myclass" ng-model="person_fax.area_code_fax" > </td>
                                     <td>
                                     <input    name="per_prefix_fax[]" id="person_prefix_fax" maxlength="3" size="3" placeholder="Prefix"pattern="[0-9]+" type="text"  class="span10  myclass" ng-model="person_fax.prefix_fax">
                                     </td>
                                     <td> 
                                     <input   name="per_suffix_fax[]" id="person_suffix_fax" maxlength="4" size="4" placeholder="Suffix"pattern="[0-9]+" type="text"  class="span10  myclass" ng-model="person_fax.suffix_fax">
                                     </td>
                                     <td>
                                     <?php echo getCatlogValue('person_country_fax[]', 'class="span8" id="per_country_fax" ng-model="person_fax.country_fax"','',37,''); ?>
                                     </td> 
                                     <td>
                                           <a href="javscript:;" ng-click="addRowFax('person_fax')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                           <a href="javscript:;" ng-click="remove_row_fax($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                    </tr>
                                    </table>
                             							

                            </div>

                            <div class="span6">  
                            
								  <label id="label_per_special_remark"><span class="numbers"></span> Special Remarks </label>
                                  <input id="person_special_remark" maxlength="80"  name="per_special_remark" type="text"  class="span12 middle_name myclass" ng-model="person_infos.special_remark">
							</div>
                          </div>
                        
                       </div>
				</fieldset>
              