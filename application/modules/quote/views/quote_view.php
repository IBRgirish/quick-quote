<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 

?>
<?php
$agency_name = $this->session->userdata('member_name'); 
$characters_only = ' pattern="^[a-zA-Z0-9][a-zA-Z0-9-_\.\ \;\,\*\@\&\)\(\-\=\+\:/]{0,50}$"  ';
$date_pattern_js = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ';
$email_pattern = 'pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$"';
$number_pattern = ' pattern="[0-9]+" ';
$less_100_limit = ' pattern="([0-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" ';
?>
<?php

$date = date('m/d/Y', time());
$time = date('h:i:s a', time());

$quote_id = 1;


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title> Quick Quote </title>

     <link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
    <link href="<?php echo base_url('css');?>/new_style.css" rel="stylesheet">
    <link href="<?php echo base_url('css');?>/bootstrap-datepicker.css" rel="stylesheet">       
  <!--  <link href="<?php echo base_url('css');?>/bootstrap.css" rel="stylesheet">-->
	<link href="<?php echo base_url('css');?>/msdropdown/dd.css" rel="stylesheet"> 
   
    <script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
    <script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
    <script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script>
    <script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
	

	<script src="<?php echo base_url('js'); ?>/msdropdown/jquery.dd.min.js"></script>
    
    <script src="<?= base_url('js/angular.min.js')?>" type="text/javascript"></script>
    <script src="<?= base_url('js/qq_angular.js')?>" type="text/javascript"></script>
    <script src="<?= base_url('js/angular-price-format.js')?>" type="text/javascript"></script>
    
     <script src="<?php echo base_url('js'); ?>/new_script.js"></script>

  </head>
   <img src="<?php echo base_url()?>images/fancybox_loading.gif" id="loader"  class=""/>
   
<body ng-app="qq">

 <?php  
error_reporting(1);
@ini_set('display_errors', 'On');
ob_start();
ob_end_clean();
 if($this->uri->segment(3)!='underwriter') {
     $editid = $this->uri->segment(3) ;?>
	
     
	<?php }
        $attributes = array('class' => 'request_quote', 'id' => 'main_form','novalidate'=>'novalidate','onsubmit'=>"return validateForm();");
    echo form_open_multipart(base_url('quote/Quick_Quotes/'.$editid), $attributes); ?> 
   <div id="error_msg"></div>
  <div class="container-fluid"> 

  <span class="request_css"><?php if(is_numeric($this->uri->segment(3))){echo 'Reuest id # '.$this->uri->segment(3).'';}?></span><h2 style="text-align:center;  margin-top: -30px;line-height:20px;" >Welcome to Global Century Insurance Brokerage</h2><br/>
  <h5 style="text-align:center;  margin-top: -15px;" >For more information please call us at (925) 493-7525</h5>
	
    <?php if($this->session->userdata('underwriter_id')){
		   if(is_numeric($this->uri->segment(3))){
		?>
    <span class="version_css">
     
       <input type="hidden" name="version" value="<?php echo !empty($ver)?$ver[0]->version:1; ?>"/>
       <input type="hidden" name="ver_id" value="<?php echo !empty($ver)?$ver[0]->ver_id:''; ?>"/>
       
       Version # <?php echo !empty($ver)?$ver[0]->version:1; ?>
    </span>
	<?php
	   }
	}
 ?>
    
    <div class="">
	  <?php if (!empty($message)) { ?>
      <div id="message">
		<?php echo $message; ?>
      </div>
      <?php } ?>
      <input type="hidden" name="req_id" value="<?php echo $editid;?>" id="req_id"/>
      <input type="hidden" name="request_id" value="" id="request_id"/>
      <div class="row-fluid" style="text-align:center" >
				 <h3 id="page-crumbs">This is a  
				 <select onchange='quick_quote()' id="modules" name="modules" class="span_10" required='required'>
				  <option value="">Select</option>
				  <option value="1" selected>Quick Quote</option>
				  <option value="2">Application</option>
				 </select>
                 for 
                  <?php echo getMakeModelVehicles('coverages_req[]', 'chosen multiple="multiple" required="required" onChange="coverages_reqs();"   class="span3 no_margin chzn-select" id="coverages_req"','',51,'');?> 
                <!-- <select name="modules" multiple chosen myDirective class="span_10">
				  <option value="">Select</option>
				  <option >Liability</option>
                  <option>PD</option>
                  <option>Cargo</option>
				 </select> -->
                 </h3>
			<br><h4>Please Fill In <span id="color_y" style="font-style: italic;text-decoration: underline;display:none"> Yellow</span> <span id="color_b" style="font-style: italic;text-decoration: underline;display:none"> Blue</span> <span id="color_g" style="font-style: italic;text-decoration: underline;display:none"> Green </span>colored fields only Other fields are optional </h4>	
			</div>
      <?php 
	     $this->load->view('quotes_view');
		?>
		 
	  <?php 
	     $this->load->view('insured_info');           
		 $this->load->view('coverage_info'); 
      ?>
    </div>
   <?php  if(empty($print)) { ?>
    <div class="row-fluid top20">
      <input type="submit" value="Submit" class="btn btn-success" />
    </div>
    <?php } ?>
    <?php echo form_close(); ?>
  </div>

<script src="<?php echo base_url('js');?>/jquery.knob.js"></script>
<!-- jQuery File Upload Dependencies -->
<script src="<?php echo base_url('js');?>/jquery.ui.widget.js"></script>
<script src="<?php echo base_url('js');?>/jquery.iframe-transport.js"></script>
<script src="<?php echo base_url('js');?>/jquery.fileupload.js"></script>
<!-- Our main JS file -->
<script src="<?php echo base_url('js');?>/script.js"></script>

<link rel="stylesheet" href="//rawgithub.com/mgcrea/bootstrap-additions/master/dist/bootstrap-additions.min.css">


    
   

</body>
</html>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
$this->load->view('includes/footer');
}
?>
<script>
$(document).ready(function(e) {
	setTimeout(function(){
		var config = {
		  '.chzn-select'           : {},
		  '.chzn-select-deselect'  : {allow_single_deselect:true},
		  '.chzn-select-no-single' : {disable_search_threshold:10},
		  '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
		  '.chzn-select-width'     : {width:"95%"}
		}
		for (var selector in config) {
		  $(selector).chosen(config[selector]);
		}
	}, 1000);
	setTimeout(function(){
		$('select.chzn-select').each(function(index, element) {
           $(this).hide();
        });	
	}, 2000)
});
	

</script>

<?php $this->load->view('json_data.php'); ?>
 <?php if(isset($print)){ ?>
 
  <script src="<?php echo base_url('js');?>/html2canvas.js"></script>
<!--  <script src="<?php echo base_url('js');?>/html2canvas.min.js"></script>-->
  <script src="<?php echo base_url('js');?>/jquery.plugin.html2canvas.min.js"></script>
  <script src="<?php echo base_url('js');?>/print.js"></script> 
  <link href="<?php echo base_url('css');?>/print.css" rel="stylesheet"> 
  
<?php  
   }
?>