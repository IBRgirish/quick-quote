<fieldset class="loses_information_box" id="loss_divs">
<div ng-controller="lossesCtrl">
	<legend >
    <div><?php if($underwriter_u=="underwriter") { ?><span class="characters_css_l" style="  width: 29%;"><?php echo '<label style="color:white" ng-if="loss.liability || loss.pd ||loss.cargo">Losses Type :</label>&nbsp;<label ng-if="loss.liability">Liability</label><label ng-if="loss.pd">,PD</label><label ng-if="loss.cargo">,Cargo</label>';?>&nbsp;&nbsp;<label style="color:white" ng-if="loss.liability && !loss.pd && !loss.cargo">No pd & cargo losses. </label>&nbsp;&nbsp;<label style="color:white" ng-if="loss.liability && loss.pd && !loss.cargo">No cargo losses.</label><label style="color:white" ng-if="!loss.liability && !loss.pd && loss.cargo">No liability & pd losses.</label> &nbsp;&nbsp;<label style="color:white" ng-if="loss.liability || loss.pd || loss.cargo">{{loss.liability -- loss.pd -- loss.cargo}} of losses</label></span><?php } ?><span class="characters"></span> E Loss(es) information<?php if($underwriter_u=="underwriter") { ?>
       <div class="pull-right icon_show_l" >
          <span class="green" id="show-loss" title="Show" style="display:none" onclick="loss_section('show')"> <img src="<?= base_url() ?>images/hide.png"></span>
          <span class="red" id="hide-loss" title="Hide" onclick="loss_section('hide')"> <img src="<?= base_url() ?>images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
         </div>
    <span class="characters_css_r" style="width: 24%; margin: 5px 22px 0px 0px;color:white;">  
    <span class="pull-left">
        <label><span class="numbers"> </span>Loss/es in the last three years ?</label>
        </span>
        
       <span class="pull-left">&nbsp;
        <select  required="required" ng-change="losses_need_value(losses_need)" name="losses_need" ng-model="loss.losses_need" class="hear_about_us myclass span10" >          
            <option >Yes</option>
            <option >No</option>
        </select>
        </span>
    </span><?php } ?>
   
</div>
 
    </legend>
   <!-- <div class="row-fluid heading_border">
        	<div class="row-fluid ">
            	<label class="heading_vehicles ng-binding">Loss(es) information</label>
            </div>           
        </div>-->
        <input type="hidden" name="loss_id" ng-model="loss.loss_id" value="{{loss.loss_id}}" />
   <div class="well" id="loss_div">
	<div class="row-fluid " >
		<div class="span12" >
		<label ><span class="numbers"> </span>Disclaimer: You do not need to fill in the loss info if you are uploading the loss runs.</label>
		</div>
	</div>
	<div class="row-fluid">
    

<div class="row-fluid "  id="losses_inform" ng-if='loss.losses_need=="Yes"'>
   <div class="row-fluid ">
		<div class="row-fluid" id="lia_losses">
			<div class="span6" id="lia_losses_div">

			<div class="span4 lightblue losses_head" ><img src="<?php echo base_url('images/Trucking-Accident.jpg');?>" width="100"/><b> Liability </b></div>
			<button class="label label-info" type="button"  id="liability_open" lia_item="open" ng-click="show_losses_('liability_open')" style="width:140px; height:31px;margin-bottom:15px; padding:5px;" >
			<label class="open_btn" id="liability_text" >Click To Open</label></button>

			</div> 
           
			<div >
				<div class="row-fluid "  id="liability_div" ng-show="liability_show">
					<div class="span12">
						<div class="span4">
						<span class="numbers">1. </span> Number of losses in last 3 years ? 
                       
					  	<input type="text"  ng-change="add_losses('liability')" name="loss_liability" ng-model="loss.liability" class="ssss span2 number_limit myclass"  />
						
                        </div>
						
					</div>
				</div>
				                           
				<div class="row-fluid"  ng-if="loss.liability>0" ng-show="liability_show">  
					<div class="span12 lightblue">
						<table id="table" class="table table-bordered table-striped table-hover"  >
							<thead> 
							  <tr>
                                <th rowspan="2" class="span_5" class="label_losses_date_of_loss">1.1 Date of Loss</th>
								<th colspan="2" class="span_6" class="label_date_from_losses">1.2 Period of Policy</th>
								<th rowspan="2" class="span_5" class="label_date_to_losses">1.3 Liability <br> Amount</th>
								<th rowspan="2" class="span_15" class="label_losses_amount">1.4 Company</th>
								<th rowspan="2" class="span_15" class="label_losses_company">1.5 General Agent </th>	
                                <th rowspan="2" class="span_5" class="label_losses_general_agent">1.6 Are you at Fault? </th>	
                                <th rowspan="2" class="span_5" class="label_losses_fault">1.7  % of Fault </th>	
                                							
							  </tr>
							  <tr colspan="4" >
								<th class="span_3"> From (mm/dd/yyyy)</th>
								<th class="span_3"> To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
					    <tbody id="add__row" ng-repeat="row in rows">                                            
                                                <tr> 
                                                <input type="hidden" maxlength="15" class="span12 " name="losses_id[]"  ng-model="row.losses_id" value="{{row.losses_id}}">
                                                <input type="hidden" maxlength="15" class="span12 " name="losses_type[]" value="liability" >                
												<td><input type="text" maxlength="18" class="span12 date_from_losses_liability myclass" name="losses_date_of_loss[]" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" class="span8_date " placeholder="MM/DD/YYYY" ng-model="row.losses_date_of_loss" id="losses_liability_date_of_loss_{{$index}}" jdatepickers="" end="losses_liability_date_of_loss_{{$index}}" ></td>
                                                <td> 
                                                <input type="text" required="required" id="date_from_losses_liability_{{$index}}" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" class="span8_date" placeholder="MM/DD/YYYY" name="date_from_losses[]" ng-model="row.date_from_losses" jqdatepicker="" end="date_to_losses_liability_{{$index}}" >                                              
                                                <td>
                                                <input type="text" required="required" id="date_to_losses_liability_{{$index}}" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" class="span8_date " placeholder="MM/DD/YYYY" name="date_to_losses[]" ng-model="row.date_to_losses" jqdatepicker="" end="date_to_losses_liability_{{$index}}" >
                                                </td>
                                                <td><input type="text" maxlength="18" class="span12  myclass" name="losses_amount[]" ng-model="row.losses_amount" format></td>
                                                <td><input type="text" maxlength="70" class="span12 myclass " name="losses_company[]" ng-model="row.losses_company"></td>
                                                <td><input type="text" maxlength="70" class="span12 myclass" name="losses_general_agent[]" ng-model="row.losses_general_agent" ></td>
                                                <td><select class="span12 myclass " name="losses_fault[]" ng-model="row.losses_fault">
                                                         <option>Yes</option>
                                                         <option>No</option>
                                                     </select>
                                                </td>
                                                <td ><input type="text" maxlength="3" pattern="[0-9]{1,2}|100|M" ng-if="row.losses_fault=='Yes'" class="span12 myclass" name="losses_fault_per[]" ng-model="row.losses_fault_per" value="{{row.losses_fault_per}}"></td>
                                                
                                                </tr> 
                                             </tbody>
                                        </table>
						
					</div>
               
				</div>
                
			</div> 
         
         </div>
	<div class="row-fluid top10">
	 <div class="span6">

			<div class="span4 lightblue losses_head"><img src="<?php echo base_url('images/image.jpg');?>" width="100"/><b>P.D. </b></div>
			<button class="label label-info" type="button" id="pd_open" ng-click="show_losses_pd('pd_open')" style="width:140px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="pd_text">Click To Open</label></button>


			
			</div>
            
			<div id="_div"  >
				<div class="row-fluid "  id="pd_div" ng-show="pd_show">
					<div class="span12"  >
						<div class="span4">
						<span class="numbers">2. </span>Number of losses in last 3 years ? 
                       
						<input type="text" ng-change="add_losses('pd')" ng-change="add_losses('pd')" name="loss_pd" ng-model="loss.pd" class="ssss span2 number_limit myclass"  />
						
                        </div>
					
					
					
					</div>
				</div>
				                           
				<div class="row-fluid"  ng-show="pd_show" ng-if="loss.pd>0">  
					<div class="span12 lightblue">
						<table id="table" class="table table-bordered table-striped table-hover"  >
							<thead> 
							  <tr>
                              <th rowspan="2" class="span_5" class="label_losses_date_of_loss">2.1 Date of Loss</th>
								<th colspan="2" class="span_6" class="label_date_from_losses">2.2 Period of Policy</th>
								<th rowspan="2" class="span_5" class="label_date_to_losses">2.3 Liability <br> Amount</th>
								<th rowspan="2" class="span_15" class="label_losses_amount">2.4 Company</th>
								<th rowspan="2" class="span_15" class="label_losses_company">2.5 General Agent </th>	
                                <th rowspan="2" class="span_5" class="label_losses_general_agent">2.6 Are you at Fault? </th>	
                                <th rowspan="2" class="span_5" class="label_losses_fault">2.7  % of Fault </th>								
							  </tr>
							  <tr colspan="4" >
								<th class="span_3"> From (mm/dd/yyyy)</th>
								<th class="span_3"> To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
					    <tbody id="add__row" ng-repeat="pd_row in pd_rows">
	
             
                                                <tr>   
                                                 <input type="hidden" maxlength="15" class="span12 " name="losses_id[]" ng-model="pd_row.losses_id"  value="{{pd_row.losses_id}}">
                                                 <input type="hidden" maxlength="15" class="span12 " name="losses_type[]" value="pd" >              
												 <td><input type="text" maxlength="18" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" class="span8_date " placeholder="MM/DD/YYYY" class="span12 date_from_losses_pd myclass" name="losses_date_of_loss[]" ng-model="pd_row.losses_date_of_loss" id="losses_pd_date_of_loss_{{$index}}" jdatepickers="" end="losses_pd_date_of_loss_{{$index}}" ></td>
                                                <td> <input type="text" required="required" id="date_from_losses_pd_{{$index}}" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" class="span8_date" placeholder="MM/DD/YYYY" name="date_from_losses[]" ng-model="pd_row.date_from_losses"  jqdatepicker="" end="date_to_losses_pd_{{$index}}"></td>
                                                <td> <input type="text" required="required" id="date_to_losses_pd_{{$index}}" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" class="span8_date " placeholder="MM/DD/YYYY" name="date_to_losses[]" ng-model="pd_row.date_to_losses" jqdatepicker="" end="date_to_losses_pd_{{$index}}"></td>
                                                <td><input type="text" maxlength="18" class="span12 price_format" name="losses_amount[]" ng-model="pd_row.losses_amount" format></td>
                                                <td><input type="text" maxlength="70" class="span12 " name="losses_company[]" ng-model="pd_row.losses_company"></td>
                                                <td><input type="text" maxlength="70" class="span12 " name="losses_general_agent[]" ng-model="pd_row.losses_general_agent" ></td>
                                                <td><select class="span12 myclass " name="losses_fault[]" ng-model="pd_row.losses_fault">
                                                         <option>Yes</option>
                                                         <option>No</option>
                                                     </select>
                                                </td>
                                                <td ><input type="text" maxlength="3" pattern="[0-9]{1,2}|100|M" ng-if="pd_row.losses_fault=='Yes'" class="span12 myclass" name="losses_fault_per[]" ng-model="pd_row.losses_fault_per" value="{{pd_row.losses_fault_per}}"></td>
                                                
                                               
                                                </tr> 
                                             </tbody>
                                        </table>
						
					</div>
				</div>                
		     </div>            
          </div>
     <div class="row-fluid top10">
		<div class="span6">
			<div class="span4 lightblue losses_head"><img src="<?php echo base_url('images/images.jpg');?>" width="100"/><b>Cargo </b></div>
			<button class="label label-info" type="button" id="cargo_open" ng-click="show_losses_cargo('cargo_open')" style="width:140px; height:31px;margin-bottom:15px; padding:5px;">
			<label class="open_btn" id="cargo_text">Click To Open</label></button>
		</div>
			<div id="_div" >
				<div class="row-fluid "  ng-show="cargo_show" id="cargo_div" >
					<div class="span12"  >
						<div class="span4">
						<span class="numbers">3. </span>Number of losses in last 3 years ? 
                       
						<input type="text"  ng-change="add_losses('cargo')" name="loss_cargo" ng-model="loss.cargo" class="ssss span2 number_limit"  />
						
                        </div>			
					
						
					</div>
				</div>
				                           
				<div class="row-fluid" ng-if="loss.cargo>0" ng-show="cargo_show">  
					<div class="span12 lightblue">
						<table id="table" class="table table-bordered table-striped table-hover"   >
							<thead> 
							<tr>
                               <th rowspan="2" class="span_5" class="label_losses_date_of_loss">3.1 Date of Loss</th>
								<th colspan="2" class="span_6" class="label_date_from_losses">3.2 Period of Policy</th>
								<th rowspan="2" class="span_5" class="label_date_to_losses">3.3 Liability <br> Amount</th>
								<th rowspan="2" class="span_15" class="label_losses_amount">3.4 Company</th>
								<th rowspan="2" class="span_15" class="label_losses_company">3.5 General Agent </th>	
                                <th rowspan="2" class="span_5" class="label_losses_general_agent">3.6 Are you at Fault? </th>	
                                <th rowspan="2" class="span_5" class="label_losses_fault">3.7  % of Fault </th>	
								
							  </tr>
							  <tr colspan="4" >
								<th class="span_3"> From (mm/dd/yyyy)</th>
								<th class="span_3"> To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
					        <tbody id="add__row" ng-repeat="cargo_row in cargo_rows">
               
                                                <tr> 
                                                <input type="hidden" maxlength="15" class="span12 " name="losses_id[]"  ng-model="cargo_row.losses_id" value="{{cargo_row.losses_id}}">
                                                <input type="hidden" maxlength="15" class="span12 " name="losses_type[]" value="cargo" >               
												<td><input type="text" maxlength="18" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" class="span8_date " placeholder="MM/DD/YYYY" class="span12 date_from_losses_cargo myclass" name="losses_date_of_loss[]" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" class="span8_date " placeholder="MM/DD/YYYY" ng-model="row.losses_date_of_loss" id="losses_cargo_date_of_loss_{{$index}}" jdatepickers="" end="losses_cargo_date_of_loss_{{$index}}" ></td>
                                                <td> <input type="text" required="required" id="date_from_losses_cargo_{{$index}}" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" class="span8_date" placeholder="MM/DD/YYYY" name="date_from_losses[]" ng-model="cargo_row.date_from_losses" jqdatepicker="" end="date_to_losses_cargo_{{$index}}"></td>
                                                <td> <input type="text" required="required" id="date_to_losses_cargo_{{$index}}" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" class="span8_date " placeholder="MM/DD/YYYY" name="date_to_losses[]" ng-model="cargo_row.date_to_losses" jqdatepicker="" end="date_to_losses_cargo_{{$index}}"></td>
                                                <td><input type="text" maxlength="18" class="span12 price_format" name="losses_amount[]" ng-model="cargo_row.losses_amount" format></td>
                                                <td><input type="text" maxlength="70" class="span12 " name="losses_company[]" ng-model="cargo_row.losses_company"></td>
                                                <td><input type="text" maxlength="70" class="span12 " name="losses_general_agent[]" ng-model="cargo_row.losses_general_agent" ></td>
                                                <td><select class="span12 myclass " name="losses_fault[]" ng-model="cargo_row.losses_fault">
                                                         <option>Yes</option>
                                                         <option>No</option>
                                                     </select>
                                                </td>
                                                <td ><input type="text" maxlength="3" pattern="[0-9]{1,2}|100|M" ng-if="cargo_row.losses_fault=='Yes'" class="span12 myclass" name="losses_fault_per[]" ng-model="cargo_row.losses_fault_per" value="{{cargo_row.losses_fault_per}}"></td>
                                                
                                                </tr> 
	                                        </tbody>
                                        </table>
						
				  </div>
			   </div>              
			</div> 	       
		 </div>
	  </div>
  <div class="row-fluid top10">
    <label>Attach Loss Report(s) for all Accident(s)</label>
    <input type="file" class=""  name="loss_report_file[]" multiple ng-model="loss.loss_report_file" value="{{loss.loss_report_file}}"/>
     <?php $files_l=($losses)?!empty($losses[0]->loss_report_file)?explode(',',$losses[0]->loss_report_file):'':''; 
	 if($files_l){
		 foreach($files_l as $file){
	?>
    <br/><a href="<?php echo base_url();?>uploads/file/<?= $file;?>" download><?= $file;?></a>
    <?php } } ?>
  <!--  <a href="<?= base_url();?>uploads/file/{{loss.loss_report_file}}">{{loss.loss_report_file}}</a>-->
  </div> 
    </div>
    
  </div>
   
    <div class="row-fluid top10">
     <input type="button" class="btn-primary pull-right" onclick="pdf_click('loss_divs');" value="Pdf" name="save"/>&nbsp;&nbsp;
     <input type="button" class="btn-primary pull-right" onclick="sec_info_save('losses_info');" value="Save" name="save"/>
  </div>
</div>
		                  	
</fieldset>
