<?php 
	if (strtolower($this->input->server('HTTP_X_Pending_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
  //load_underwriter_list();
    $("#underwriterList").dataTable({
				 "sPaginationType": "full_numbers",
				 "aoColumnDefs" : [ {
					'bSortable' : false,
					'aTargets' : [ 0 ]
				} ]
			 });
});

function load_underwriter_list() {
    $("#underwriterList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo site_url(); ?>quote/ajax_quote_list/"+$("#Pending_by").val(),

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [{
                "sClass": "center"
            },

            //{"sClass": "center"},
            {
                "fnRender": function (oObj) {
                    var a = oObj.aData[1];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[4];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
					var a = oObj.aData[5];
					var d = new Date(a);
					var day = d.getDate();
					var month = d.getMonth() + 1;
					var year = d.getFullYear();
				
                    return (month + "/" + day + "/" + year);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[6];
                    return (a);
				}
			}, {
			"fnRender": function (oObj) {
				if(oObj.aData[8] == 'Released') {
					b = '<a class="btn btn-primary"" href="<?php echo base_url(); ?>ratesheet/rate_by_quote/'+oObj.aData[7]+'">View</a>';
					return (b);
					}	
					else
					{
						b = '-';
						return (b);
					}
				}
				
			}
			
        ]
    });

}
function viewQuotePopup(id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url('quote/QuoteView/');?>/'+id,
    	autoSize: true,
    	closeBtn: true,
    	width: '850',
    	//height: '150',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}
/**By Ashvin Patel popup for Quoteview 13/may/2014**/
</script>
<br>
<h1>Quote List</h1>
<div class="table"> 
<!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> 
<!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<!--<a class="fancybox-add btn btn-primary" onclick="javascript:void(0)" href="">Add New</a>-->
<a href="<?php echo base_url(); ?>ratesheet/manualrate" class="btn btn-primary"> Add New</a>
  <!-- Quote List -->
<input type="hidden" name="Pending_by" id="Pending_by" value="<?php echo $user ?>" />
<select onchange="sortStatus(this.value)" style="margin: 0;">
    <option value="">Sort By Status</option>
    <option value="Active">Active</option>
    <option value="Draft">Draft</option>
    <option value="Pending">Pending</option>
    <option value="Rejected">Rejected</option>
    <option value="Released">Released</option>    
    <option value="Requested">Requested</option>
    <option value="Revise Request">Revise Request</option>  
</select>
<div id="data_table">
  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
       <!-- <th class="center" width="10%">Sr.</th>-->
        <th width="15%">Quote No.</th>
        <th width="15%">Date Submitted</th>
        <th width="15%">Agency Full Name/DBA</th>
		
		<th width="12%">Agency Phone Number</th>
        <th width="12%">Person submitting business</th>
        <th width="12%">Insured Full Name/DBA</th>
         <th width="12%">Coverage Date</th>
         <th>Status</th>
		<th width="30%">Quotes</th>
      </tr>
    </thead>
    <tbody>
		<?php // print_r($quote3);
		
		//echo $quote3[0]['insured_fname']; ?>
		<?php $quote0 = ''; $cnt= 0; krsort($quote); foreach($quote as $row)
		{
			 
		//echo"<pre>";	print_r($row); die;
		//	$cnt++;
			if($quote0!=$row->id)
			{
				$quote0==$row->id;
				?>
				<tr>
					<!--<td><?php echo $cnt; ?></td>-->					
					<td><?php echo $row->id; ?> </td>
					<td><?php echo date("m/d/Y",strtotime($row->submit_date)); ?></td>
					<td><?php echo get_agency_detail($row->requester_id, 'first_name')?></td>
					<td><?php echo get_agency_detail($row->requester_id, 'phone_number')?></td>
					<td><?php echo $row->fistname.' '. $row->middlename.' '. $row->lastname?></td>
                    
                    <td><?php if($row->insured_name || $row->insured_middle_name ||$row->insured_last_name ){ echo  $row->insured_middle_name." ".$row->insured_last_name." ".$row->insured_middle_name ;}
					else{ echo  isset($row->dba) ? $row->dba : '' ;
					}
					?></td>
                    <td><?php  ?></td>
                    <td><?php
				//echo"<pre>";	print_r($row);die;
					 if($row->status==''){ echo 'Pending'; }else{ echo $row->status; }?></td>
					<?php 
						if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id'))){	
					?>						
						<td>
                        	<?php if(get_ref_id($row->id)){?>
                            
                        	 <!--<a class="btn btn-primary" href="<?php echo base_url(); ?>quote/quote/rate_by_quote1/<?php echo get_ref_id($row->id); ?>">Quote</a>
							<?php if($row->bundle_status=='Released' && get_ref_id($row->id)){?>
                           <a class="btn btn-success btn-sm" href="<?php echo base_url(); ?>ratesheet/view_quote_rate1/<?php echo $row->bundle_id; ?>">Review Quote</a> <?php }else{}?>-->
                            
                            <?php }
							else
							{
								?>
                               
                               <!-- <a class="btn btn-primary" id="" href="<?php echo base_url('quote/request_quote/'.$row->id.'');?>">Rate Quote Req.</a>-->
                                <?php
							}?>
                             <!--  <a href="javascript:;" onclick="viewQuotePopup(<?= $row->id;?>)" class="btn btn">View Request</a>
                            
                         	<a class="btn btn-danger" href="javascript:;" onclick="calcelQuote(<?php echo $row->id; ?>)">Cancel</a>
                            <a class="btn btn" target="_blank" href="<?php echo base_url('quote/QuoteView_Download/'.$row->id.'')?>">Download Request</a> -->
                            <a class="btn btn-primary" id="" href="<?php echo base_url('quote/request_quote/'.$row->id.'');?>">Rate Quote Req.</a>
							<?php if(empty($row->pdf_name)){  ?>
                            <a class="btn btn download" target="_blank" class="" >Download Request</a>
							<?php }else{ ?>
							<a class="btn btn " target="_blank" class="" href="<?php echo base_url('uploads/pdf/'.$row->pdf_name.'');?>">Download Request</a>
						 <?php } ?>
                         <?php if($row->status=='Released') { if(!empty($row->u_pdf_name)){  ?>
                          <a class="btn btn " target="_blank" class="" href="<?php echo base_url('uploads/pdf_u/'.$row->u_pdf_name.'');?>">Download Quote</a>
                         <?php } else { ?>
						    <a class="btn btn download" target="_blank" >Download Quote</a>
                     <?php  } } ?>
                        </td>
					<?php
					}  else if($this->session->userdata('member_id')){	
					?>
						
						<td><?php if($row->bundle_status != 'Released' && $row->bundle_status != 'Accepted') { ?>-<?php } else { ?><a class="btn btn-primary" href="<?php echo base_url(); ?>ratesheet/view_quote_rate/<?php echo $row->bundle_id; ?>">Quote</a>&nbsp;</a><?php } ?>
                        <a class="btn btn-primary" href="<?php echo base_url(); ?>quote/quote/rate_by_quote1/<?php echo $row[0]['quote_ref']; ?>">Versions</a></td>
					<?php
					}  $cnt++;
					?>
					
				</tr>
				<?php  
				}
		}
		?>
        <?php  $cnt= 0; $quote2 = ''; krsort($quote1); foreach($quote1 as $row1)
		{
			//print_r($row1);
			if($quote2!=$row1->quote_id)
			{
				$quote2==$row1->quote_id;
			$cnt++;
				?>
				<tr>
					<!--<td><?php echo $cnt; ?></td>-->
					
					<td><?php echo $row1->quote_id; ?></td>
					<td><?php echo date("m/d/Y",strtotime($row1->creation_date)); ?></td>
					<td><?php echo get_agency_detail($row1->created_by, 'first_name');?></td>
					<td><?php echo get_agency_detail($row1->created_by, 'phone_number');?></td>
					<td><?php echo $row1->insured; ?></td>
                    <td></td>
                    <td><?php if($row->status==''){ echo 'Pending'; }else{ echo $row->status; }; ?></td>
					<?php 
						if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id'))){	
					?>
						
						<td>
                        	<?php if($row1->is_manual){ ?>
                            <a class="btn btn-primary" href="<?php echo base_url(); ?>quote/quote/rate_by_quote2/<?php echo $row1->quote_ref; ?>">Quote</a>
                            
                           <?php
							}
                            else {
							?>
                        	<a class="btn btn-primary" href="<?php echo base_url(); ?>quote/quote/rate_by_quote1/<?php echo $row1->quote_ref; ?>">Quote</a>
                            
                             <a href="javascript:;" onclick="viewQuotePopup(<?= $row1->quote_id;?>)" class="btn btn">View Request</a>
                             
                            <?php
                            }
							?>
                        	<a class="btn btn-danger" href="javascript:;" onclick="calcelQuote(<?php echo $row1->id; ?>)">Cancel</a>
                        </td>
					<?php
					}  else if($this->session->userdata('member_id')){	
					?>
						
						<td><?php if($row1->bundle_status != 'Released' && $row1->bundle_status != 'Accepted') { ?>-<?php } else { ?><a class="btn btn-primary" href="<?php echo base_url(); ?>ratesheet/view_quote_rate/<?php echo $row1->bundle_id; ?>">Quotte</a>&nbsp;</a><?php } ?>
                        <a class="btn btn-primary" href="<?php echo base_url(); ?>quote/quote/rate_by_quote1/<?php echo $row1->quote_ref; ?>">Versions</a></td>
					<?php
					}
					?>
					
				</tr>
				<?php 
				}
		}
		?>
    </tbody>
  </table>
</div>
  <!-- Quote List -->
</div>
<script>
 $( ".download" ).click(

  function() {
	
    alert("File is not availbale right now, you'll get the file after 10 mins");
   }
  );

function calcelQuote(id)
{
	if(confirm('Are you sure you want Cancel this quote'))
	{
		window.location = '<?php echo base_url(); ?>quote/quote/cancel_quote/'+id;
	}
}
function sortStatus(val)
{	
	$.ajax({
		url: "<?php echo base_url();?>quote/quote/sort_list_quote_by_status",
		type: "post",
		data: "status="+val,
		success: function(json){
		//alert(json);
			 var obj = $.parseJSON(json);  
			//alert(obj['table_data']);
			var table_body = obj['table_data'];
			//var table_body = $(json).find('.table_data').html();
			//var export_link = $(json).find('.export_link').html();
			//alert(table_data);
			//var table_data = $(table_body).find('.table_body').html();
			$("#data_table").html(table_body);
			//$("#export_div").html(export_link);
			 $("#underwriterList").dataTable({
				 "sPaginationType": "full_numbers",
						/* "aoColumnDefs" : [ {
							'bSortable' : false,
							'aTargets' : [ 0 ]
						} ],*/
						"aaSorting": [[ 0, "asc" ]]
			 });
		}
	});	
}
</script>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_Pending_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
