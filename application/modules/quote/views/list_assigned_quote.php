<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
   // load_underwriter_list();
});

function load_underwriter_list() {
    $("#underwriterList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo site_url(); ?>quote/ajax_assigned_quote/",

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [{
                "sClass": "center"
            },

            //{"sClass": "center"},
            {
                "fnRender": function (oObj) {
                    var a = oObj.aData[1];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[4];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[5];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[6];
					
					if(oObj.aData[7] > 0)
					{
						a = oObj.aData[8];
						b = '(<a href="<?php echo base_url(); ?>ratesheet/rate_by_quote/'+oObj.aData[0]+'">View</a>)';
					}
					else 
					{
						a = 'Pending';
						b = '(<a href="<?php echo base_url('ratesheet/generate_ratesheet');?>/'+oObj.aData[0]+'">Generate</a>)';
						
					}
					
					//var b = '(<a href="javascript:void(0);">Change</a>)';
                    return (a+'&nbsp;'+b);
                }

            }
        ]
    });

}
</script>
<br>
<h1>Quote List</h1>
<div class="table"> 
<!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> 
<!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<!--<a class="fancybox-add btn btn-primary" onclick="javascript:void(0)" href="">Add New</a>-->

  <!-- Quote List -->
  <a href="<?php echo base_url(); ?>ratesheet/manualrate" class="btn btn-primary"> Add New</a>
  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">Sr.</th>
        <th width="15%">Name</th>
        <th width="15%">Email</th>
        <th width="15%">Phone</th>
		<th width="15%">Requester</th>
		<th width="15%">Cov. Date</th>
        <th width="15%">Status</th>
      </tr>
    </thead>
    <tbody>
		<?php ?>
		<?php  $cnt= 0; krsort($quote); foreach($quote as $row)
		{
		
			$cnt++;
				?>
				<tr>
					<td><?php echo $cnt; ?></td>
					
					<td><?php echo $row->contact_name; ?></td>
					<td><?php echo $row->email; ?></td>
					<td><?php echo $row->phone_no; ?></td>
					<td><?php echo date("m/d/Y",strtotime($row->date_added)); ?></td>
					<td id="status_<?= $cnt; ?>"><?php if($row->bundle_status == '') { if($row->perma_reject=='Reject'){ echo 'Rejected';}else{?>Requested<?php }} else { echo $row->bundle_status; } ?></td>
					<td><?php if($row->bundle_id == 0) { ?><a href="<?php echo base_url('ratesheet/generate_ratesheet');?>/<?php echo $row->quote_id; ?>">Generate Ratesheet</a>  <?php
					if($row->perma_reject!='Reject'){				
					?>
					 <a href="javascript:;" onclick="changeStatus('reject', <?php echo $row->quote_id; ?>, <?= $cnt; ?>)" id="link_<?= $cnt; ?>">Reject</a><?php } } else { ?><a href="<?php echo base_url(); ?>ratesheet/rate_by_quote/<?php echo $row->quote_id; ?>">View</a></a><?php } ?></td>
				</tr>
				<?php 
		}
		?>
    </tbody>
  </table>
  <!-- Quote List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
<script>
function changeStatus(action, id, id1){
	$.ajax({
		url: "<?php echo base_url('quote/changeStatus')?>",
		type: "post",
		data: "action="+action+"&id="+id,
		beforeSend: function() {
			$('#loader1').show();
		},
		complete: function(){
			$('#loader1').hide();
		},
		success: function(data){
			//alert(data);
			$("#status_"+id1).html('Rejected');
			$("#link_"+id1).hide();
		}
	});
}
</script>