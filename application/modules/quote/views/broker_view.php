<fieldset class="head_box">


                    <!-- Form Name -->
                    <legend><span class="characters" id="p_infos">Main Broker info</span><span class="characters" id="p_infos_1" style="display:none">MBI</span></legend>
                  
                    <div class="row-fluid" ng-controller="brokerCtrl">
                     <input id="br_id" name="br_id" type="hidden"  value="{{myservice.broker_infos.br_id}}" ng-model="myservice.broker_infos.br_id" >
                     <input id="email" name="requester_id" type="hidden"  value="{{myservice.broker_infos.requester_id}}" ng-model="myservice.broker_infos.requester_id" >
                        <div class="span12">
                         <input   maxlength="80" name="quote_id"  type="hidden"  class="span12 first_name myclass" ng-model="myservice.broker_infos.id" >
                           <div class="row-fluid width_50">  
                                <div class="span3 span_15 lightblue">
                                    <label id="label_bro_fistname"><span class="numbers"></span> First name</label>                                    
                                    <input id="bro_fistname" name="bro_fistname" type="text" class="span12 myclass" ng-model="myservice.broker_infos.fistname"  >
                                </div>
                                <div class="span3 span_15 lightblue">
                                    <label id="label_bro_middlename" >Middle name</label>
                                    <input id="bro_middlename" name="bro_middlename" type="text" class="span12 myclass"  ng-model="myservice.broker_infos.middlename" >
                                </div>
                                <div class="span3 span_15 lightblue">
                                    <label id="label_bro_lastname">Last Name</label>
                                    <input   id="bro_lastname" name="bro_lastname" type="text" class="span12 myclass" ng-model="myservice.broker_infos.lastname" >
                                </div>	
                                <div class="span3 span_50 lightblue">
                                    <label id="label_bro_dba"><span class="numbers"></span> DBA</label>
                                    <input id="bro_dba" name="bro_dba" type="text" class="span12 myclass" ng-model="myservice.broker_infos.dba" ng-change="quick_dba(myservice.broker_infos.dba)">
                                </div>	
                           </div>
                           <div class="row-fluid width_50">
                                <table id="table" class="tables">                                   
                                    <tr>
                                     		
									 <td id="label_bro_Street"> <span class="numbers"></span>Street</td>	
                                     <td id="label_bro_city"><span class="numbers"></span>City</td>	
                                     <td id="label_bro_state"><span class="numbers"></span>State</td>	
                                     <td id="label_bro_county"><span class="numbers"></span>County</td>
                                     <td id="label_bro_zip"><span class="numbers"></span>Zip</td>	
                                     <td id="label_bro_country"><span class="numbers"></span>Country</td>                                    
                                     </tr>
                                     <tr >
                                     
                                     <td style="width:28%"> <input  id="bro_Street" maxlength="80" name="bro_Street"  type="text"  class="span12 first_name myclass " ng-model="myservice.broker_infos.street" > </td>
                                     <td style="width:18%"><input id="bro_city" maxlength="80"  name="bro_city" type="text"  class="span12  myclass zip_code" ng-model="myservice.broker_infos.city"> </td>
                                     <td> 
                                     <?php  echo get_state_dropdown('bro_state', $selected_val='CA', $attr='class="span10 zip_code" id="bro_state" ng-model="myservice.broker_infos.state"') ?>
                                      
                                     </td>
                                     <td id="bro_county_td">
									  <?php  
									  $city='';
									  $state='';
									         if(!empty($broker_info[0]->city)){  
									         $city=!empty($broker_info[0]->city)?$broker_info[0]->city:'';
											 }else{
											 $city=!empty($broker_info[0]['city'])?$broker_info[0]['city']:''; 
											 }
											 
											 if(!empty($broker_info[0]->state)){  
									         $state=!empty($broker_info[0]->state)?$broker_info[0]->state:'';	
											 }else{
											 $state=!empty($broker_info[0]['state'])?$broker_info[0]['state']:'';	
											 }
									         									 
									  ?>
									 <?php echo getCounty('bro_county', 'class="span12" onChange="getZipCodes(this.value)" id="bro_county"  ng-model="myservice.broker_infos.county"',"$city","$state"); ?>                                
                                    
                                     </td>
                                     <td id="bro_zip_td">
									 <?php echo getZipCode('bro_zip', 'class="span12" id="bro_zip"  ng-model="myservice.broker_infos.zip"',"$city","$state");?>
                                     </td>
                                     <td>
                                     <?php echo getCountryDropDown1('bro_country', 'class="span12" id="bro_country" ng-model="myservice.broker_infos.country"','',''); ?>
                                        
                                     </td>
                                    
                                     </tr>
                                     
                                     </table> 
                            </div>	
							
                           <div class="row-fluid width_51">
                           
                             <table id="table" class="tables">                                  
                                    <tr>
                                     <td style="width:15%" id="label_bro_phone_type"><span class="numbers"></span>Phone Type</td>	
                                     <td style="width:17%" id="label_bro_code_type"><span class="numbers"></span>Country Code</td>	
                                     <td id="label_bro_area_code"><span class="numbers" style="width: 15%;"></span>Area code</td>	
                                     <td id="label_bro_prefix"><span class="numbers"></span>Prefix</td>
                                     <td id="label_bro_suffix"><span class="numbers"></span>Suffix</td>	
                                     <td style="width: 14%;" id="label_bro_extension"><span class="numbers" ></span>Extension</td>
                                     <td style="width:20%" id="label_bro_pri_country"><span class="numbers"></span>Priority sequence</td>	
                                     <td style="color:white" id="">action	</td>
                                     </tr>
                                     <tr ng-repeat="broker_phone in myservice.broker_phones">
                                       <input type="hidden" name="phone_id[]" ng-model="broker_phone.phone_id" value='{{broker_phone.phone_id}}'/>
                                     <td>
                                     <?php echo getCatlogValue('bro_phone_type[]', 'class="span11" id="bro_phone_type" ng-model="broker_phone.phone_type"','',35,''); ?> 
                                     </td>
                                     <td>
                                      <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
									   <?php
                                       $selected =  '1';
									    //isset($voice_no[$i]->country_code) ? decrypt($voice_no[$i]->country_code) :
                                      // $highlighted = isset($vncc_highlighted) ? $vncc_highlighted : '';		
                                         getCountryCodeDropDown($name='bro_code_type[]', $att='class="span11" id="bro_code_type" ng-model="broker_phone.code_type"', 'code', $selected); //'.$highlighted.'
									 ?>
                                     <?php //echo getCatlogValue('bro_code_type[]', 'class="span12" id="bro_code_type" ng-model="broker_phone.code_type"','',36,''); ?> 
                                     <td> 
                                      <input  id="bro_area_code" maxlength="3" size="3" placeholder="Area" pattern="[0-9]+" name="bro_area_code[]"  type="text"  class="span10 first_name myclass" ng-model="broker_phone.area_code" > </td>
                                     <td>
                                     <input id="bro_prefix" maxlength="3" size="3" placeholder="Prefix" pattern="[0-9]+"  name="bro_prefix[]" type="text"  class="span10  myclass" ng-model="broker_phone.prefix">
                                     </td>
                                     <td> 
                                      <input id="bro_suffix" maxlength="4" size="4" placeholder="Suffix"pattern="[0-9]+"  name="bro_suffix[]" type="text"  class="span10  myclass" ng-model="broker_phone.suffix">
                                     </td>
                                     <td> 
                                      <input id="bro_extension" maxlength="5" size="5" placeholder="Ext"pattern="[0-9]+"  name="bro_extension[]" type="text"  class="span10  myclass" ng-model="broker_phone.extension">
                                     </td>
                                     <td>
                                     <?php echo getCatlogValue('bro_pri_country[]', 'class="span8" id="bro_pri_country" ng-model="broker_phone.pri_country"','',37,''); ?> 
                                        
                                    </td>
                                    <td>
                                     <a href="javscript:;" ng-click="addRowPhone('broker_phone')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                     <a href="javscript:;" ng-click="remove_row_phone($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                    </tr>
                                    </table>

                            </div>
                           <div class="row-fluid width_49">  
							<table id="table" class="tables">
                                     <tr>
                                    	
									 <td><span class="numbers" id="label_bro_email_type"></span>Email Type</td>	
                                     <td><span class="numbers" id="label_bro_email"></span>Email Address</td>	
                                     <td><span class="numbers" id="label_bro_priority_email"></span>Priority sequence</td>	
                                     <td></td>
                                     </tr>
                                     <tr ng-repeat="broker_email in myservice.broker_emails">
                                     <input type="hidden" name="email_id[]" ng-model="broker_email.email_id" value='{{broker_email.email_id}}'/>
                                      <td>
                                      <?php echo getCatlogValue('bro_email_type[]', 'class="span8" id="bro_email_type" ng-model="broker_email.email_type"','',34,''); ?> 
                                      </td>	
                                      <td style="width:25%">                                      
                                      <input id="bro_email" name="bro_email[]" id="bro_email" type="text" class="span12 myclass" ng-model="broker_email.email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$">	
                                      </td>
                                      <td> 
                                      <?php echo getCatlogValue('bro_priority_email[]', 'class="span8" id="bro_priority_email" ng-model="broker_email.priority_email"','',37,''); ?> 
                                      </td>	
                                      <td>
                                           <a href="javscript:;" ng-click="addRowEmail('broker_email')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                           <a href="javscript:;" ng-click="remove_row_email($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                     </td>
                                     </tr>
                                  </table>
							    
									
                                </div>   
         <!--  <div class="fluid-row">
           <span class="numbers"></span> 
            <label>Do you have any fax number?</label>
             <select name="in_broker_fax" class="span2 span_8 " id="in_broker_fax" ng-model="myservice.broker_infos.fax">
        	  <option>Yes</option>
             <option selected="selected">No</option>
           </select>
          </div>-->
                            <div class="row-fluid width_50">
                              <table id="table" class="tables">                                   	
                                    <tr>                                     			
									 <td style="width:14%" id="label_bro_fax_type"><span class="numbers"> </span>Fax Type</td>	
                                     <td style="width:18%" id="label_bro_code_type_fax"><span class="numbers"> </span>Country Code</td>	
                                     <td style="width:15%" id="label_bro_area_code_fax"><span class="numbers"></span>Area code</td>	
                                     <td id="label_bro_prefix_fax"><span class="numbers"> </span>Prefix</td>
                                     <td id="label_bro_suffix_fax"><span class="numbers"></span>Suffix</td>	
                                     <td style="width:21%" id="label_bro_country_fax"><span class="numbers"></span>Priority sequence</td>	
                                     <td>	</td>
                                     </tr>
                                     <tr ng-repeat="broker_fax in myservice.broker_faxs">
                                     <input   maxlength="80" name="fax_id[]"  type="hidden"  class="span11" value='{{broker_fax.fax_id}}' ng-model="broker_fax.fax_id" >
                                     <td>
                                     <?php echo getCatlogValue('bro_fax_type[]', 'class="span12" id="bro_fax_type" ng-model="broker_fax.fax_type"','',38,''); ?> 
                                   
                                     </td>
                                     <td>
                                      <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
									  <?php
                                       $selected =  '1'; 
                                       getCountryCodeDropDown($name='bro_code_type_fax[]', $att='class="span11" id="bro_code_type_fax" ng-model="broker_fax.code_type_fax"', 'code', $selected); //'.$highlighted.'
									  ?>                                     
                                     <td> 
                                     <input  id="bro_area_code_fax"  name="bro_area_code_fax[]"  type="text"  class="span10  " maxlength="3" size="3" placeholder="Area" pattern="[0-9]+" ng-model="broker_fax.area_code_fax" > </td>
                                     <td>
                                     <input id="bro_prefix_fax"  maxlength="3" size="3" placeholder="Prefix"pattern="[0-9]+" name="bro_prefix_fax[]" type="text"  class="span10  " ng-model="broker_fax.prefix_fax">
                                     </td>
                                     <td> 
                                     <input id="bro_suffix_fax"  maxlength="4" size="4" placeholder="Sufix"pattern="[0-9]+" name="bro_suffix_fax[]" type="text"  class="span10  " ng-model="broker_fax.suffix_fax">
                                     </td>
                                     <td>
                                     <?php echo getCatlogValue('bro_country_fax[]', 'class="span8" id="bro_country_fax" ng-model="broker_fax.country_fax"','',37,''); ?> 
                                    </td>                            
                                   
                                    <td>
                                           <a href="javscript:;" ng-click="addRowFax('broker_fax')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                           <a href="javscript:;" ng-click="remove_row_fax($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                    </tr>
                                   </table>			

                            </div>
                            <div class="span6 width_46">                             
								  <label id="label_bro_special_remark"> <span class="numbers"></span> Special Remarks </label>
                                  <input id="bro_special_remark" maxlength="80"  name="bro_special_remark" type="text"  class="span12 middle_name myclass" ng-model="myservice.broker_infos.special_remark">
							</div>
                          </div>
                       </div>
							</fieldset>