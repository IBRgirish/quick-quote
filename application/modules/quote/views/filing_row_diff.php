<?php $this->load->view('includes/head_style_script')?>
<?php 
if(isset($filings)){
	if(is_array($filings)){		
	  ?>
	  <table class="table">
		<tr align="left">
         <th>Filings</th>
		 <th>Filing Type </th>
		 <th>Filing #</th>
		</tr>
		<?php foreach($filings as $key => $filling){ ?>
		<tr>
          <td>Filing #<?= $key+1; ?></td>
		  <td>
		  <?php echo $filling->filings_type; ?> 
		  <?php if($filling->filings_type=='Other'){ echo " : - ".(isset($filling->filing_type_other) ? $filling->filing_type_other : ''); }?>
          </td>
          <td>
		  <?php echo $filling->filing_no; ?>       
          </td>
		</tr>
        <?php } ?>
	  </table>
	  <?php
	}
}
?>