
<fieldset id="driv_divs" class="schedule_of_drivers_box top20" ng-controller="driverCtrl">
<legend>
<div class="span11 no_margin"><?php if($underwriter_u=="underwriter") { ?><span class="characters_css_l" ><label style="color:white" ng-if="myservices.rows[0]['driver_name']">Drivers Name :</label><label>{{myservices.rows[0]['driver_name']}} </label></span><?php } ?>D Schedule of Drivers</div>
<div class="span2 span_18 margin_6">
  <label>Number of drivers ?</label>
  <input <?php echo $less_100_limit; ?> required type="text" id="num_of_drivers" name="driver_count" class="span1 no_margin input_15" ng-model="n_drivers" ng-change="add_row_()"  />
  </div>
  <div class="pull-right">
          <span class="green" id="show-driv" title="Show" style="display:none" onclick="driv_section('show')"> <img src="<?= base_url()?>images/hide.png"></span>
          <span class="red" id="hide-driv" title="Hide"  onclick="driv_section('hide')"> <img src="<?= base_url()?>images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
         </div>
</legend>
<div class="well"  ng-if="n_drivers>0" id="driv_div">
  <div id="row_driver">
    <div class="row-fluid">  
      <div class="span12 lightblue">
    
      <input type="hidden" name="driver_ids" ng-model="driver_ids" value="{{driver_ids}}"/>
        <table id="driver_table" class="table table-bordered table-striped table-hover" >
          <thead class="myPhone.showIt"> 
            <tr>
              <th ><span class="numbers" id="label_driver_name">1. </span>First Name</th>
              <th ><span class="numbers" id="label_driver_middle_name">2. </span>Middle Name</th>
              <th ><span class="numbers" id="label_driver_last_name">3. </span>Last Name</th>
              <th ><span class="numbers" id="label_driver_license">4. </span>Driver License number</th>
              <th ><span class="numbers" id="label_driver_license_class">5. </span>License Class</th>
              <th ><span class="numbers" id="label_driver_license_issue_date">6. </span>Date Hired</th>
              <th ><span class="numbers" id="label_driver_dob">7. </span>DOB</th>
              <th ><span class="numbers" id="label_driver_class_a_years">8. </span>How Many Years Class A license</th>
              <th ><span class="numbers" id="label_driver_license_issue_state">9. </span>License issue state</th>
              <th></th>
            </tr>
          </thead>
          <tbody id="add_driver_row" ng-repeat="row in myservices.rows">
            <tr>
              <input type="hidden" name="driver_id[]" ng-model="row.driver_id" value="{{row.driver_id}}"/>
              <td><input type="text" maxlength="20" class="span12 first_name" name="driver_name[]" ng-model="row.driver_name" id="driver_name"></td>   
              <td><input type="text" maxlength="20" class="span12 middle_name" name="driver_middle_name[]" ng-model="row.driver_middle_name"></td>   
              <td><input type="text" maxlength="20" class="span12 last_name" name="driver_last_name[]" ng-model="row.driver_last_name"></td>   
              <td><input type="text" maxlength="20" class="span12" name="driver_license[]"  ng-model="row.driver_license"></td> 
              <td><input type="text" maxlength="20" class="span12" name="driver_license_class[]"  ng-model="row.driver_license_class"></td>
              <td><input type="text" maxlength="20" class="span12 date_from_license_issue" name="driver_license_issue_date[]" ng-model="row.driver_license_issue_date" jdatepickers id="driver_license_issue_date{{$index}}"></td>
              <td><input type="text" maxlength="20" class="span12 date_from_dob"  name="driver_dob[]" ng-model="row.driver_dob" jqdatepicker id="driver_dob{{$index}}"></td>
              <td><input type="text" maxlength="20" class="span12" name="driver_class_a_years[]"  ng-model="row.driver_class_a_years"></td>
              <td><?php  echo get_state_dropdown('driver_license_issue_state[]', 'CA', 'style="width:70px;" ng-model="row.driver_license_issue_state"'); ?></td>
              <td>
              	<a href="javscript:;" ng-click="add_row()" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                <a href="javscript:;" ng-click="remove_row($index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
              </td>
            </tr> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row-fluid">
    <label>Attach MVR`s for all Driver(s) no mare than 30 days old</label>
    <input type="file" name="driv_mvr_file[]" multiple class="span1 span_25">
    <?php $files_d=($drivers)?!empty($drivers[0]->driv_mvr_file)?explode(',',$drivers[0]->driv_mvr_file):'':''; 
	 if($files_d){
		 foreach($files_d as $file){
	?>
    <br/><a href="<?php echo base_url();?>uploads/file/<?= $file;?>" download><?= $file;?></a>
    <?php } } ?>
</div>
<div class="row-fluid top5">
  <label><b>Disclaimer : </b> Please attach the MVR for each driver, otherwise the quote is an indication only and not a formal quote.</label>
  </div>
  
</div>
 <div class="well " ng-if="n_drivers==0">
  <label> Please give an explanation:</label>
  
   <textarea class="span12" name="driver_explain" ng-model="driver_explain">{{driver_explain}}</textarea>
 </div>
 <div class="row-fluid top20">
  <input type="button" class="btn-primary pull-right" onclick="pdf_click('driv_divs');" value="Pdf" name="save"/>&nbsp;&nbsp;
    <input type="button" class="btn-primary pull-right" onclick="sec_info_save('driver_info');" value="Save" name="save"/>
  
  </div>
</fieldset>

<!-- Losses Section Start Ashvin Patel 26/April/2015-->
<div class="top20">
 <div class="row-fluid ">
     
               <?php $this->load->view('losses_view'); ?>
    
  </div>
 </div>
<!-- Losses Section End Ashvin Patel 26/April/2015-->


<!-- Insured Owner Section Start Ashvin Patel 26/April/2015-->
   

<fieldset id="own_divs" class="schedule_of_drivers_boxs top20" ng-controller="ownerCtrl">
<legend>
<div class="span11 no_margin"><?php if($underwriter_u=="underwriter") { ?><span class="characters_css_l"> <label style="color:white" ng-if="rowss[0]['owner_name']">Owners Name :</label><label style="color: lightblue;">{{rowss[0]['owner_name']}}</label></span><?php } ?>F Owner Infomation</div>
<div class="span2 span_18 margin_6">
  <label>Number of owner ?</label>
  <input <?php echo $less_100_limit; ?> required type="text" id="number_of_owner" name="number_of_owner" class="span1 no_margin input_15" ng-model="n_owners" ng-change="add_row_()"  />
  </div>
   <div class="pull-right">
          <span class="green" id="show-own" title="Show" style="display:none" onclick="own_section('show')"> <img src="<?= base_url()?>images/hide.png"></span>
          <span class="red" id="hide-own" title="Hide"  onclick="own_section('hide')"> <img src="<?= base_url()?>images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
         </div>
</legend>
<div class="well" ng-if="n_owners>0" id="own_div">
  <div id="row_owner">
    <div class="row-fluid">  
      <div class="row-fluid">
        <table id="owner_table" class="table table-bordered table-striped table-hover" >
         <input type="hidden" name="owner_ids" ng-model="owner_ids"/>
          <thead>
            <tr>
              <th><span class="numbers" id="label_owner_name">1. </span>First Name</th>
              <th><span class="numbers" id="label_owner_middle_name">2. </span>Middle Name</th>
              <th><span class="numbers" id="label_owner_last_name">3. </span>Last Name</th>
              <th><span class="numbers" id="label_owner_is_driver">4. </span>Driver ?</th>
              <th><span class="numbers" id="label_owner_license">5. </span>Driver License number</th>
              <th><span class="numbers" id="label_license_issue_state">6. </span>License issue state</th>
            </tr>
          </thead> 
          <tbody id="add_owner_row">
            <tr ng-repeat="row in rowss">
            <input type="hidden" name="owner_id[]" ng-model="row.owner_id" value="{{row.owner_id}}"/>
              <td><input type="text" maxlength="20" class="span12 first_name" name="owner_name[]" ng-model="row.owner_name"></td>
              <td><input type="text" maxlength="20" class="span12 middle_name" name="owner_middle_name[]" ng-model="row.owner_middle_name"></td>
              <td><input type="text" maxlength="20" class="span12 last_name" name="owner_last_name[]" ng-model="row.owner_last_name"></td>            
              <td style="  width: 13%;">
                <select name="owner_is_driver[]" class="span12" ng-model="row.owner_is_driver" ng-change="owner_d(row.owner_is_driver)">
                  <option value="">SELECT</option>
                  <option value="Y">Yes</option>
                  <option value="N">NO</option>
                </select>
              </td> 
              <td><input type="text" maxlength="20" class="span12" name="owner_license[]" ng-model="row.owner_license"></td> 
              <td>
			  <?php  echo get_state_dropdown('license_issue_state[]', 'CA', 'style="width:70px;" ng-model="row.license_issue_state"'); ?>
              <a href="javscript:;" ng-click="add_row('owners')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
              <a href="javscript:;" ng-click="remove_row($index, 'owners')"><img src="<?= base_url('images/remove.png'); ?>"></a>
              </td>
            </tr>
          </tbody> 
        </table>
      </div>    
    </div>
  </div>

<div class="row-fluid">
    <label>Attach MVR`s for all Owner(s) no mare than 30 days old</label>
    <input type="file" name="own_mvr_file[]" multiple class="span1 span_25">
     <?php $files_o=($owners)?!empty($owners[0]->own_mvr_file)?explode(',',$owners[0]->own_mvr_file):'':''; 
	 if($files_o){
		 foreach($files_o as $file){
	?>
    <br/><a href="<?php echo base_url();?>uploads/file/<?= $file;?>" download><?= $file;?></a>
    <?php } } ?>
   <!--<a href="<?php echo base_url();?>uploads/file/{{own_mvr_file}}" download>{{own_mvr_file}}</a>-->
</div>
<div class="row-fluid top5">
  <label><b>Disclaimer : </b> Please attach the MVR for each driver, otherwise the quote is an indication only and not a formal quote.</label>
  </div>
  
  <div class="row-fluid top20">
  <input type="button" class="btn-primary pull-right" onclick="pdf_click('own_divs');" value="Pdf" name="save"/>&nbsp;&nbsp;
    <input type="button" class="btn-primary pull-right" onclick="sec_info_save('owner_info');" value="Save" name="save"/>
  </div>
 </div> 

 </fieldset>


<!-- Insured Owner Section End Ashvin Patel 26/April/2015-->