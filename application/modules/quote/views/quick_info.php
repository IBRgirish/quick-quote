<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	 $this->load->view('../includes/header'); 
		if($this->session->userdata('admin_id')){
            $this->load->view('includes/header'); 
        }else if($this->session->userdata('underwriter_id')){
            $this->load->view('../includes/header'); 
        }
	} 
?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>datatable/media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>datatable/resources/syntax/shCore.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>datatable/resources/demo.css">
    

	<script type="text/javascript" language="javascript" src="<?php echo base_url()?>datatable/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url()?>datatable/resources/syntax/shCore.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url()?>datatable/resources/demo.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url()?>jquery.price_format.js"></script>
	
	<script type="text/javascript" language="javascript" class="init"></script>
    <script>
$(document).ready(function() {
   $('#example').dataTable( {
       "order": [[ 0, "desc" ]]
    } );
} );
</script>


<h2 style="text-align:center;">Quick Quote Information</h2>
<div    ng-app="additional_phone">

<div class="well">

<table id="example" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
                    	<!--<th>SNo</th>-->
                        <th>Submitted date/time</th>
						<th>Insured Name</th>
						<th>Insured DBA</th>
						<!--<th>Email</th>						
						<th>Phone No</th>-->
                        <th>Status</th>
						<th width="15%">Action</th>
					</tr>
				</thead>
				<?php 
				$i=1;				
				if(is_array($brokers)){
					foreach($brokers as $broker)
					{ 
						?>
                        <tr>
                        	<!--<td><?php echo $i; ?></td>-->
                            <?php 
                            $name = isset($broker['insured_name']) ? $broker['insured_name'] : '';	
                            $name .= isset($broker['insured_middle_name']) ? ' '.$broker['insured_middle_name'] : '';	
                            $name .= isset($broker['insured_last_name']) ? ' '.$broker['insured_last_name'] : '';	
                            ?>
                            <td><?php echo isset($broker['submit_date']) ? date('m/d/Y H:i a', strtotime($broker['submit_date'])) : ''; ?></td>
                        	<td><?= $name; ?></td>
                            <td><?php echo isset($broker['dba'])?$broker['dba']:'';?></td>
                           <!-- <td><?php echo isset($broker['email'])?$broker['email']:''; ?></td>                            
                            <td><?php echo isset($broker['broker_phone'])?$broker['broker_phone']:''; ?></td>-->
                            
                            <td><?php if($broker['status']==''){ ?>Pending <?php }else if($broker['status']=='Draft'){ ?>Draft <?php }else{ echo 'Quoted'; }?></td>
                           
                           
                            <td><a href="<?php echo base_url()?>quote/request_quote/<?php echo $broker['id']?>" data-toggle="modal"><button class="btn btn-phone-block"><span class="hidden-phone">View</span></button></a>&nbsp;
                             <?php if($broker['status']=='Released'){ ?>
							 <?php    if(!empty($broker['u_pdf_name'])){?>
                                <a href="<?php echo base_url('uploads/pdf_u')?>/<?php echo $broker['u_pdf_name']?>" data-toggle="modal"><button class="btn btn-phone-block"><span class="hidden-phone">Download Quote</span></button></a>
                              <?php  }else{ ?>
							     <a class="btn btn download_1" target="_blank" >Download Quote</a>
                            
							  <?php } } ?>
                            <!--<a href="<?php echo base_url()?>agency/deleteBroker/<?php echo $broker['id']?>" onClick="return confirm('Are you sure you want to delete')"  data-toggle="modal"><button class="btn btn-danger btn-phone-block"><span class="hidden-phone">Delete</span></button></a>--> </td>
                        </tr>
                        <?php 
						$i++;	
					}	
					
				}?>
				
			</table>
	   
        </div>	

</div>			


<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
<script>
 $( ".download_1" ).click(

  function() {
	
    alert("File is not availbale right now, you'll get the file after 10 mins");
   }
  );

</script>

