<div class="row-fluid bottom5 other_box" ng-repeat="other in rows.others" ng-init="otherIndex=$index" ng-if="coverages.other_no>0">
	 
    <div class="well">
      	<div class="row-fluid heading_border">
        	<div class="row-fluid ">
            	<label class="heading_vehicles">
                  <div class="row-fluid img_1">
                   <span style="color: #377814;font-weight: bolder;"> Other #{{$index+1}} </span><br /><h6 style="margin: 0% 0% 0% 0%; width: 100%;color: #377814;">This is only one vehicle information</h6>
                  </div>
                </label>
                  <div class="pull-right icon_show_b">
                    <span class="green" id="show-cov_o{{otherIndex}}" title="Show" style="display:none" ng-click="cov_section_o('show',otherIndex)"> <img src="<?= base_url()?>images/hide.png"></span>
                    <span class="red" id="hide-cov_o{{otherIndex}}" title="Hide" ng-click="cov_section_o('hide',otherIndex)"> <img src="<?= base_url()?>images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                 </div>
                 <div class="row-fluid pull-right">
            	<label>Vehicle Info <?php if($underwriter=='underwriter') { ?><img ng-if="other.vh_salvage =='Yes'" src="<?php echo base_url()?>images/flag-red_24x24.png" /><?php } ?></label>
            	
            </div>
            </div>
           
        </div>
        <div class="row-fluid" id="cov_div_o{{otherIndex}}">
        <?php $vhit = 0;?>
      	<div class="row-fluid">  	
          <div class="span1 span_7dot6"> 
          <input type="hidden" value="other" name="vehicle_type[]"/>     
          <input type="hidden" value="{{other.vehicle_id}}" ng-model="other.vehicle_id" name="vehicle_id[]"/>   
            <label><span class="numbers"><?= ++$vhit; ?> </span>Vehicle Year</label>          
            <select class="span12 myclass" name="vehicle_year_vh[]" id="vehicle_year_vh" ng-model="other.vehicle_year_vh">
              <?php 
              $year=$vehicle->vehicle_year;
              $current_year = date("Y");
			   echo "<option value=''>Select</option>";
              for($i = 1985; $i <= $current_year + 1; $i++){
				 
                echo "<option value='$i'>$i</option>";
              } 
              ?>
            </select>
          </div>
          <div class="span2 span_7dot6">
            <label class="label_make_truck"><span class="numbers"><?= ++$vhit; ?> </span> Make</label>
           
		
            <input type="text" maxlength="19" class="span12 myclass" name="make_truck[]" id="vin_vh" ng-model="other.make_truck">

          </div>
           <div class="span2 span_7dot6">
            <label class="label_model_tuck"><span class="numbers"><?= ++$vhit; ?> </span> Model</label>
                     
            <input type="text" maxlength="19" class="span12 myclass" name="model_tuck[]" id="vin_vh"  ng-model="other.model_tuck">

          </div>
           <div class="span2 span_7dot6">
            <label class="label_model_color"><span class="numbers"><?= ++$vhit; ?> </span> color</label>
    
             <?php //echo $make_model_truck;
			echo getMakeModelVehicles('model_color[]', 'class="span12" ng-model="other.model_color"','',48,'');?>
             <input type="text" maxlength="19" class="span12 myclass" name="model_color_other[]" id="vin_vh" ng-if="other.model_color=='Other'" ng-model="other.model_color_other">
            <input type="text" maxlength="19" class="span12 myclass" name="model_color_other[]" id="vin_vh" ng-if="other.model_color=='other'" ng-model="other.model_color_other">
          </div>
          <div class="span2 span_11 lightblue">
            <label><span class="numbers"><?= ++$vhit; ?> </span>GVW</label>
            <select class="span12 myclass" name="gvw_vh[]" id="gvw_vh" ng-model="other.gvw_vh">
              <option value="">Select</option>
              <option value="0-10,000 Lb">0-10,000 Lb</option>
              <option value="10,001-26,000 Lb">10,001-26,000 Lb</option>
              <option value="26,001-40,000 Lb">26,001-40,000 Lb</option>
              <option value="40,001-80,000 Lb">40,001-80,000 Lb</option>
              <option value="Over 80,000 Lb">Over 80,000 Lb</option>
            </select>
          </div>
          <div class="span2 lightblue span_15">
            <label><span class="numbers truck_number_vehicle"><?= ++$vhit; ?> </span>VIN</label>
            <input type="text" maxlength="19" class="span12 myclass" name="vin_vh[]" id="vin_vh" ng-model="other.vin_vh">
          </div>
          <!--<div class="span2 lightblue">
            <label>Special req.</label>
            <input type="text" class="span12 myclass" name="value_amount_vh[]" id="value_amount_vh" value="">
          </div>-->
          <div class="span2 span_9">
              <label><span class="numbers truck_number_vehicle"><?= ++$vhit; ?> </span>Number of Axles</label>
                 <?php //echo $truck_number_of_axles;
			        echo num_of_axle('class="span9" ng-model="other.truck_number_of_axles" ng-if="other.truck_number_of_axles" ','truck_number_of_axles[]');
			        echo num_of_axle('class="span9" ng-model="other.truck_number_of_axles" ng-if="!other.truck_number_of_axles" ng-init="other.truck_number_of_axles=&quot;2 Axle&quot;"','truck_number_of_axles[]');
			    ?>             
          </div>
          <div class="span1">
          	<label><span class="numbers truck_number_vehicle"><?= ++$vhit; ?> </span>Salvage</label>
            <select name="vh_salvage[]" class="span12" ng-model="other.vh_salvage">
            	<option value="">Select</option>
                <option value="No" >No</option>
                <option value="Yes" >Yes</option>
            </select>
          </div>
          <div class="span2 span_11 lightblue ">
            <label><span class="numbers truck_number_vehicle"><?= ++$vhit; ?> </span>Radius of Operation</label>            
            <select class="span12 rofo truck_radius_of_operation myclass" name="truck_radius_of_operation[]" ng-model="other.truck_radius_of_operation" id="radius_of_operation"  >
                <option value="">Select</option>
                <option value="0-100 miles" >0-100 miles</option>
                <option value="101-500 miles"  >101-500 miles</option>           	
                <option value="501+ miles"  >501+ miles</option>
                <option value="other"  >Other</option>
            </select>  
            <input type="text" name="radius_of_operation_other[]" ng-if="other.truck_radius_of_operation=='other'" ng-model="other.radius_of_operation_other" class="span12"/>   
          </div>
          <div class="span2 span_10">
          	<label><span class="numbers truck_number_vehicle"><?= ++$vhit; ?> </span>Nature of business</label>
            <input type="text" name="vh_nature_of_business[]" ng-model="other.vh_nature_of_business" class="span12 nature_bus">
          </div>          
        </div>
         <div class="row-fluid">
          <span id="o_make_tr{{otherIndex}}" class="span6">
               <input type="hidden" name="img_name_make[]" id="img_name_make_{{otherIndex}}" ng-if="other.img_name_make" value="{{other.img_name_make}}" ng-model="other.img_name_make"/>
               <img src="<?php echo base_url();?>uploads/file/{{other.img_name_make}}" ng-if="other.img_name_make"/>
         </span>
         <span class="mapping" id="mapping_{{otherIndex}}" class="span6" <?php if(!$this->session->userdata('underwriter_id')) { echo 'style="display:none"'; }?>>
          <!--  <span class="span_8">
             <label>Show Google Map</label>
              <select ng-change="map_address_o(otherIndex,o_map)" id="google_map_id_{{otherIndex}}" ng-model="o_map"class="span10" ng-init="o_map='Yes'">
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
             </span>-->  
               <span class="pull-right icon_show_m">
                    <span class="green" id="o_show-cov_map{{otherIndex}}" title="Show" style="display:none" ng-click="o_cov_section_map('show',otherIndex)"> <img src="<?= base_url()?>/images/max.png"></span>
                    <span class="red" id="o_hide-cov_map{{otherIndex}}" title="Hide" ng-click="o_cov_section_map('hide',otherIndex)"> <img src="<?= base_url()?>/images/min.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                 </span>          
               <div id="o_map-address-{{otherIndex}}" class="Map" style="width:500px;height:380px;"></div><span class="span_11"></span>
          </span>
        </div>
        <div class="row-fluid">
        	<div class="span2 truck-parent">
              <label><span class="numbers truck_number_ownvehicle"><?= ++$vhit; ?> </span>Owned Vehicle</label>
              <select name="truck_trailer_owned_vehicle[]" id="truck_owned_vehicle" ng-model="other.truck_trailer_owned_vehicle" ng-change="add_loss_row('others',otherIndex)" class="span7">
                  <option>Yes</option>
                  <option>No</option>
              </select>
          	</div>
            <div class="span2" ng-if="other.truck_trailer_owned_vehicle=='No'">
          
            	<label>&nbsp;</label>
            	<label class="span12">Please provide the owner</label>
            </div>
            <div class="" ng-if="other.truck_trailer_owned_vehicle=='No'">
             <div class="pull-right icon_show_c">
                    <span class="green" id="show-cov_ov{{otherIndex}}" title="Show" style="display:none" ng-click="cov_section_ov('show',otherIndex)"> <img src="<?= base_url()?>images/hide.png"></span>
                    <span class="red" id="hide-cov_ov{{otherIndex}}" title="Hide" ng-click="cov_section_ov('hide',otherIndex)"> <img src="<?= base_url()?>images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                 </div>
             </div>
        </div>
        <div class="row-fluid" >
         <div class="row-fluid" ng-if="other.truck_trailer_owned_vehicle=='No'" id="cov_div_ov{{otherIndex}}">
          <div class="row-fluid bottom5" ng-repeat="lessor in other.lessor" ng-init="lessorIndex=$index">     
               <input type="hidden" value="other" name="vh_types[]"/>  
            	<div class="well">
                 <div class="row-fluid">
            	  <div class="span6">
                  <label>Sequence</label>
                 <!-- <input type="text" name="vh_owner_sq" class="span1" ng-change="add_loss_row('others', $index)" ng-model="other.lessor_n" >-->
                  <input type="text" name="vh_owner_sq" class="span1"  value="{{$index+1}}" readonly>
                   <label>Lessor Type</label>                   
                   <?php echo getCatDropDownss('lessor_type[]', 'ng-model="lessor.lessor_type" class="span2"', 39,'') ?>
                </div>
                <div class="span6 pull-right">
                    	<label class="span12">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_loss_row('others', otherIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a> 
                		<a href="javscript:;" ng-click="remove_type('others', otherIndex,$index)">
                        <img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
               </div>
               <?php $vhitow = 0; ?>
            	<div class="row-fluid">
                        <input type="hidden" name="vh_ls_id[]" class="span12" ng-model="lessor.vh_ls_id" value="{{lessor.vh_ls_id}}">                
                	<div class="span2 span_10">
                    	
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>{{lessor.lessor_type}} name</label>
                        <input type="text" name="vh_ls_name[]" class="span12" ng-model="lessor.vh_ls_name">
                    </div>
                    <div class="span2 span_8">
                    	
                    	<label><span class=""></span>Middle name</label>
                        <input type="text" name="vh_ls_m_name[]" class="span12" ng-model="lessor.vh_ls_m_name">
                    </div>
                    <div class="span2 span_10">
                    	
                    	<label><span class=""></span>Last name</label>
                        <input type="text" name="vh_ls_l_name[]" class="span12" ng-model="lessor.vh_ls_l_name">
                    </div>
                    <div class="span3 span_10">
                    	
                    	<label><span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>{{lessor.lessor_type}} DBA</label>
                        <input type="text" name="vh_ls_dba[]" class="span12" ng-model="lessor.vh_ls_dba">
                    </div>
                    <div class="span3 span_15">
                    
                    	<label><span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Street</label>
                        <input type="text" name="vh_ls_address[]" class="span12" ng-model="lessor.vh_ls_address">
                    </div>
                    <div class="span1 span_8">
                    
                    	<label><span class="numbers_child_child "></span>City</label>
                        <input type="text" name="vh_ls_city[]" class="span12" ng-model="lessor.vh_ls_city" ng-change="zip_code_ls('3')" id="3_vh_ls_city">
                    </div>
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child "></span>State</label>
                        <?php  echo get_state_dropdown('vh_ls_state[]','', $attr='class="span12" ng-model="lessor.vh_ls_state" id="3_vh_ls_state"  ng-change="zip_code_ls(&quot;3&quot;)"') ?>
                        
                    </div>
                    <div class="span2 span_10">
                  
                    	<label> <span class="numbers_child_child "></span>County</label>
                        <span id="3_vh_ls_county_span">
                        <input type="text" name="vh_ls_county[]" class="span12" ng-model="lessor.vh_ls_county" id="3_vh_ls_county">
                        </span>
                    </div>
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child "></span>Zip</label>
                        <span id="3_vh_ls_zip_span">
                        <input type="text" name="vh_ls_zip[]" class="span12" ng-model="lessor.vh_ls_zip" id="3_vh_ls_zip">
                        </span>
                    </div>
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child"></span>Country</label>
                         <?php echo getCountryDropDown1('vh_ls_country[]', 'class="span12" id="person_pri_country" ng-model="lessor.vh_ls_country"','',''); ?>
                         
                    </div>
                </div>
                <!--<div class="row-fluid">
                	
                </div>-->
                <div class="row-fluid">
                	<div class="span2 span_8" style="width: 7.7%!important;">
                   
                    	<label> <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Phone type</label>                       
                        <?php echo getCatlogValue('vh_ls_ph_type[]', 'class="span9"  ng-model="lessor.vh_ls_ph_type"','',35,''); ?>
                           
                    </div>
                    <div class="span2 span_10">
                   
                    	<label><span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Country code</label>
                         <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
					   <?php
                       $selected =  '1'; 
					   echo  getCountryCodeDropDown($name='vh_ls_country_code[]', $att='class="span12"  ng-model="lessor.vh_ls_country_code"', 'code', $selected); //'.$highlighted.'
                     ?>
                        <?php //echo getCatlogValue('vh_ls_country_code[]', 'class="span12"  ng-model="lessor.vh_ls_country_code"','',36,''); ?>
                        
                    </div>
                    <div class="span2 span_7dot6">
                   
                    	<label> <span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Area code</label>
                        <input type="text" name="vh_ls_areacode[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Area" ng-model="lessor.vh_ls_areacode">
                    </div> 
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Prefix</label>
                        <input type="text" name="vh_ls_prefix[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Prefix" ng-model="lessor.vh_ls_prefix">
                    </div>
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Suffix</label>
                        <input type="text" name="vh_ls_suffixe[]" class="span8" pattern="[0-9]+" maxlength="4" size="4" placeholder="Suffix" ng-model="lessor.vh_ls_suffixe">
                    </div>
                    <div class="span2 span_7dot6">
                   
                    	<label> <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Extension</label>
                         <input type="text" name="vh_ls_ext[]" class="span8" pattern="[0-9]+" maxlength="4" size="4" placeholder="Ext" ng-model="lessor.vh_ls_ext">
                     </div> 
                    <div class="span2 span_12">
                   
                    	<label> <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Priority sequence</label>
                        <?php echo getCatlogValue('vh_ls_ph_pri_sequence[]', 'class="span6"  ng-model="lessor.pri_country"','',37,''); ?>
                     </div>   
                       <input type="hidden" value="truck" name="vh_garage_type[]"/>
                <div class="row-fluid" ng-repeat="email in lessor.emails">
                <input type="hidden" ng-model="email.email_id" name="vh_ls_email_id_{{lessorIndex}}[]" value="{{email.email_id}}"/>
                	<div class="span2 span_9">
                     
                    	<label><span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Email Type</label>
                        <?php echo getCatlogValue('vh_ls_email_type_{{lessorIndex}}[]', 'class="span9"  ng-model="email.email_type"','',34,''); ?>
                     
                    </div>
                    <div class="span2" style="width: 10.5%!important;">
                    
                    	<label> <span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Email Address</label>
                        <input type="text" name="vh_ls_email_{{lessorIndex}}[]" class="span12" ng-model="email.email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$">
                    </div>
                    <div class="span2 span_12">
                   
                    	<label>  <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Priority sequence</label>
                        <?php echo getCatlogValue('vh_ls_em_pri_sequence_{{lessorIndex}}[]', 'class="span5"  ng-model="email.priority_email"','',37,''); ?>
                    </div>
                    <input type="hidden" value="truck" name="vh_em_type[]"/>
                    <div class="span1">
                    
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_email('trucks', truckIndex, lessorIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                		<a href="javscript:;" ng-click="remove_email('trucks', truckIndex, lessorIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
                 </div>                 
                </div>
              
              <!--  <div class="fluid-row"> 
               	
                  <label> <span class="numbers_child_child"> </span>Do you have any fax number?</label>
                    <select name="in_other_fax[]" class="span1" ng-model="lessor.in_other_fax">
                      <option>Yes</option>
                   <option selected="selected">No</option>
                 </select>
                </div>-->
               <div class="fluid-row" > 
                <div class="row-fluid" ng-repeat="fax in lessor.faxs">
                 <input type="hidden" ng-model="fax.fax_id" name="vh_fx_id_{{lessorIndex}}[]" value="{{fax.fax_id}}"/>
                	<div class="span2 span_10">
                   
                    	<label> <span class="numbers_child other_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Fax Type</label>
                         <?php echo getCatlogValue('vh_ls_fx_type_{{lessorIndex}}[]', 'class="span12"  ng-model="fax.fax_type"','',38,''); ?>
                     
                    </div>
                    <div class="span2 span_10">
                   		<label><span class="numbers_child other_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Country code</label>
                        <?php //echo getCatlogValue('vh_ls_fx_country_code[]', 'class="span12"  ng-model="fax.code_type_fax"','',36,''); ?>
                        <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
					   <?php
                       $selected =  '1'; //isset($voice_no[$i]->country_code) ? decrypt($voice_no[$i]->country_code) :
                      // $highlighted = isset($vncc_highlighted) ? $vncc_highlighted : '';		
                         getCountryCodeDropDown($name='vh_ls_fx_country_code_{{lessorIndex}}[]', $att='class="span12"  ng-model="fax.code_type_fax" ', 'code', $selected); //'.$highlighted.'
                     ?>
                    </div>
                    <div class="span2 span_8">
 						
                    	<label><span class="numbers_child other_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Area code</label>
                        <input type="text" name="vh_ls_fx_areacode_{{lessorIndex}}[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Area" ng-model="fax.area_code_fax">
                    </div>
                    <div class="span1">
                    
                    	<label><span class="numbers_child other_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Prefix</label>
                        <input type="text" name="vh_ls_fx_prefix_{{lessorIndex}}[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Prefix" ng-model="fax.prefix_fax">
                    </div>
                    <div class="span1">
                   
                    	<label><span class="numbers_child other_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Suffix</label>
                        <input type="text" name="vh_ls_fx_suffix_{{lessorIndex}}[]" class="span9" pattern="[0-9]+" maxlength="4" size="4" placeholder="Suffix" ng-model="fax.suffix_fax">
                    </div>
                    <div class="span2 span_15">
                  
                    	<label><span class="numbers_child other_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Priority sequence</label>
                        <?php echo getCatlogValue('vh_ls_fx_pri_sequence_{{lessorIndex}}[]', 'class="span6"  ng-model="fax.country_fax"','',37,''); ?>
                    
                    </div>
                    <input type="hidden" value="other" name="vh_fx_type[]"/>
                    <div class="span1">
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_fax('others', otherIndex, lessorIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                		<a href="javscript:;" ng-click="remove_fax('others', otherIndex, lessorIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
                </div>
                </div>
              </div>
            </div> 
         <?php /*?><div class="row-fluid bottom5" ng-repeat="lessor in other.lessor" ng-init="lessorIndex=$index">     
               <input type="hidden" value="other" name="vh_types[]"/>  
            	<div class="well">
                 <div class="row-fluid">
            	  <div class="span6">
                  <label>Sequence</label>
                 <!-- <input type="text" name="vh_owner_sq" class="span1" ng-change="add_loss_row('others', $index)" ng-model="other.lessor_n" >-->
                  <input type="text" name="vh_owner_sq" class="span1"  value="{{$index+1}}" readonly>
                      <label>Lessor Type</label>
                   
                   <?php echo getCatDropDownss('lessor_type[]', 'ng-model="lessor.lessor_type" class="span2"', 39,'') ?>
                
                </div>
                <div class="span6 pull-right">
                    	<label class="span12">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_loss_row('others', otherIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a> 
                		<a href="javscript:;" ng-click="remove_type('others', otherIndex,$index)">
                        <img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
               </div>
               <?php $vhitow = 0; ?>
            	<div class="row-fluid">
                        <input type="hidden" name="vh_ls_id[]" class="span12" ng-model="lessor.vh_ls_id" value="{{lessor.vh_ls_id}}">                
                	<div class="span2 span_10">
                    	
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>{{lessor.lessor_type}} name</label>
                        <input type="text" name="vh_ls_name[]" class="span12" ng-model="lessor.vh_ls_name">
                    </div>
                    <div class="span2 span_8">
                    	
                    	<label><span class=""></span>Middle name</label>
                        <input type="text" name="vh_ls_m_name[]" class="span12" ng-model="lessor.vh_ls_m_name">
                    </div>
                    <div class="span2 span_10">
                    	
                    	<label><span class=""></span>Last name</label>
                        <input type="text" name="vh_ls_l_name[]" class="span12" ng-model="lessor.vh_ls_l_name">
                    </div>
                    <div class="span3">
                    	
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>{{lessor.lessor_type}} DBA</label>
                        <input type="text" name="vh_ls_dba[]" class="span12" ng-model="lessor.vh_ls_dba">
                    </div>
                </div>
                <div class="row-fluid">
                	<div class="span3">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Street</label>
                        <input type="text" name="vh_ls_address[]" class="span12" ng-model="lessor.vh_ls_address">
                    </div>
                    <div class="span1 span_8">
                    
                    	<label><span class="numbers_child_child"></span>City</label>
                        <input type="text" name="vh_ls_city[]" class="span12" ng-model="lessor.vh_ls_city">
                    </div>
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child"></span>State</label>
                        <?php  echo get_state_dropdown('vh_ls_state[]','', $attr='class="span12" ng-model="lessor.vh_ls_state"') ?>
                        
                    </div>
                    <div class="span2 span_10">
                  
                    	<label> <span class="numbers_child_child"></span>County</label>
                        <input type="text" name="vh_ls_county[]" class="span12" ng-model="lessor.vh_ls_county">
                    </div>
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child"></span>Zip</label>
                        <input type="text" name="vh_ls_zip[]" class="span12" ng-model="lessor.vh_ls_zip">
                    </div>
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child"></span>Country</label>
                         <?php echo getCountryDropDown1('vh_ls_country[]', 'class="span12" id="person_pri_country" ng-model="lessor.vh_ls_country"','',''); ?>
                         
                    </div>
                </div>
                <div class="row-fluid">
                	<div class="span2 span_10">
                   
                    	<label> <span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Phone type</label>                       
                        <?php echo getCatlogValue('vh_ls_ph_type[]', 'class="span9"  ng-model="lessor.vh_ls_ph_type"','',35,''); ?>
                           
                    </div>
                    <div class="span2 span_10">
                   
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Country code</label>
                         <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
					   <?php
                       $selected =  '1'; 
					   echo  getCountryCodeDropDown($name='vh_ls_country_code[]', $att='class="span12"  ng-model="lessor.vh_ls_country_code"', 'code', $selected); //'.$highlighted.'
                     ?>
                        <?php //echo getCatlogValue('vh_ls_country_code[]', 'class="span12"  ng-model="lessor.vh_ls_country_code"','',36,''); ?>
                        
                    </div>
                    <div class="span2 span_7dot6">
                   
                    	<label> <span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Area code</label>
                        <input type="text" name="vh_ls_areacode[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Area" ng-model="lessor.vh_ls_areacode">
                    </div> 
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Prefix</label>
                        <input type="text" name="vh_ls_prefix[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Prefix" ng-model="lessor.vh_ls_prefix">
                    </div>
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Suffix</label>
                        <input type="text" name="vh_ls_suffixe[]" class="span8" pattern="[0-9]+" maxlength="4" size="4" placeholder="Suffix" ng-model="lessor.vh_ls_suffixe">
                    </div>
                    <div class="span2 ">
                   
                    	<label> <span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Priority sequence</label>
                        <?php echo getCatlogValue('vh_ls_ph_pri_sequence[]', 'class="span6"  ng-model="lessor.pri_country"','',37,''); ?>
                     </div>                    
                </div>
                <input type="hidden" value="other" name="vh_garage_type[]"/>
                <div class="row-fluid" ng-repeat="email in lessor.emails">
                <input type="hidden" ng-model="email.email_id" name="vh_ls_email_id_{{lessorIndex}}[]" value="{{email.email_id}}"/>
                	<div class="span2 span_10">
                     
                    	<label><span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Email Type</label>
                        <?php echo getCatlogValue('vh_ls_email_type_{{lessorIndex}}[]', 'class="span9"  ng-model="email.email_type"','',34,''); ?>
                     
                    </div>
                    <div class="span3">
                    
                    	<label> <span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Email Address</label>
                        <input type="text" name="vh_ls_email_{{lessorIndex}}[]" class="span12" ng-model="email.email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$">
                    </div>
                    <div class="span2 ">
                   
                    	<label>  <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Priority sequence</label>
                        <?php echo getCatlogValue('vh_ls_em_pri_sequence_{{lessorIndex}}[]', 'class="span5"  ng-model="email.priority_email"','',37,''); ?>
                    </div>
                    <input type="hidden" value="other" name="vh_em_type[]"/>
                    <div class="span1">
                    
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_email('others', otherIndex, lessorIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                		<a href="javscript:;" ng-click="remove_email('others', otherIndex, lessorIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
                </div>
            <!--    <div class="fluid-row"> 
               	
                  <label> <span class="numbers_child_child"></span>Do you have any fax number?</label>
                    <select name="in_truck_fax[]" class="span1" ng-model="lessor.in_truck_fax">
                      <option>Yes</option>
                   <option selected="selected">No</option>
                 </select>
                </div>-->
               <div class="fluid-row" > 
                <div class="row-fluid" ng-repeat="fax in lessor.faxs">
                 <input type="hidden" ng-model="fax.fax_id" name="vh_fx_id_{{lessorIndex}}[]" value="{{fax.fax_id}}"/>
                	<div class="span2 span_10">
                   
                    	<label> <span class="numbers_child_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?> </span>Fax Type</label>
                         <?php echo getCatlogValue('vh_ls_fx_type_{{lessorIndex}}[]', 'class="span12"  ng-model="fax.fax_type"','',38,''); ?>
                     
                    </div>
                    <div class="span2 span_10">
                   		<label><span class="numbers_child_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?>  </span>Country code</label>
                        <?php //echo getCatlogValue('vh_ls_fx_country_code[]', 'class="span12"  ng-model="fax.code_type_fax"','',36,''); ?>
                        <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
					   <?php
                       $selected =  '1'; //isset($voice_no[$i]->country_code) ? decrypt($voice_no[$i]->country_code) :
                      // $highlighted = isset($vncc_highlighted) ? $vncc_highlighted : '';		
                         getCountryCodeDropDown($name='vh_ls_fx_country_code_{{lessorIndex}}[]', $att='class="span12"  ng-model="fax.code_type_fax" ', 'code', $selected); //'.$highlighted.'
                     ?>
                    </div>
                    <div class="span2 span_8">
 						
                    	<label><span class="numbers_child_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?>  </span>Area code</label>
                        <input type="text" name="vh_ls_fx_areacode_{{lessorIndex}}[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Area" ng-model="fax.area_code_fax">
                    </div>
                    <div class="span1">
                    
                    	<label><span class="numbers_child_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?>  </span>Prefix</label>
                        <input type="text" name="vh_ls_fx_prefix_{{lessorIndex}}[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Prefix" ng-model="fax.prefix_fax">
                    </div>
                    <div class="span1">
                   
                    	<label><span class="numbers_child_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?>  </span>Suffix</label>
                        <input type="text" name="vh_ls_fx_suffix_{{lessorIndex}}[]" class="span9" pattern="[0-9]+" maxlength="4" size="4" placeholder="Suffix" ng-model="fax.suffix_fax">
                    </div>
                    <div class="span2 span_15">
                  
                    	<label><span class="numbers_child_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?>  </span>Priority sequence</label>
                        <?php echo getCatlogValue('vh_ls_fx_pri_sequence_{{lessorIndex}}[]', 'class="span6"  ng-model="fax.country_fax"','',37,''); ?>
                    
                    </div>
                    <input type="hidden" value="other" name="vh_fx_type[]"/>
                    <div class="span1">
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_fax('others', otherIndex, lessorIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                		<a href="javscript:;" ng-click="remove_fax('others', otherIndex, lessorIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
                </div>
                </div>
              </div>
            </div><?php */?>
           </div>
          
           <div class="row-fluid top20">
            	<div class="row-fluid ">
                   <span>
                	<label><span class="numbers truck_equipment_numbers"><?= ++$vhit; ?> </span>Do you pull any equipment with {{other.vehicle_year_vh}}<span ng-if="other.make_truck">,{{other.make_truck}}</span><span ng-if="other.vin_vh">,{{other.vin_vh}}</span>?</label>
                    <select name="vh_equipment_pull[]" class="span1 no_margin" ng-model="other.vh_equipment_pull" value="{{other.vh_equipment_pull}}">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    </span>
                   
                </div>
                <?php $vhteqp = 0; ?>
                <div class="row-fluid top20" id="cov_div_trve{{otherIndex}}" ng-if="other.vh_equipment_pull=='Yes'"> 
                  
                 <div class="left_section"> 
                   <div class="row-fluid"> 
                	<label><span class="numbers truck_equipment_numbers"><?= $vhit; ?> A </span>How many trailers do you pull with this vehicle?</label>
                    <input type="text" name="vh_equipment_trailers[]" id="others_vh_eqpmnt_pull_{{otherIndex}}" class="span1 span_5 no_margin" ng-model="other.vh_equipment_trailers" ng-change="vh_eqpmnt_pull('others', otherIndex,other.vh_equipment_trailers)"/>
                   <!--<select name="vh_equipment_trailers[]" class="span1 span_7dot6 no_margin" ng-model="vh_eqpmnt.vh_equipment_trailers">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>-->
                     <span  ng-if="other.vh_equipment_pull=='Yes'"> 
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_eqp('others', otherIndex)" ><img src="<?= base_url('images/add.png'); ?>"></a>
                		<div class="pull-right icon_show_ce">
                         <span class="green" id="show-cov_trve{{otherIndex}}" title="Show" style="display:none" ng-click="cov_section_trve('show',otherIndex)"> <img src="<?= base_url()?>images/hide.png"></span>
                         <span class="red" id="hide-cov_trve{{otherIndex}}" title="Hide" ng-click="cov_section_trve('hide',otherIndex)"> <img src="<?= base_url()?>images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                       </div>
                    </span>
                    </div>
                  
                 <div class="row-fluid " ng-repeat="vh_eqpmnt in other.vh_eqpmnts" ng-init="eqpIndex=$index"> 
                 <div class="row-fluid">
                  <input type="hidden" ng-model="vh_eqpmnt.vh_eqp_id" value="{{vh_eqpmnt.vh_eqp_id}}" name="vh_eqp_id[]"/>
                   <?php if($underwriter=='underwriter') { ?><img ng-if="vh_eqpmnt.vh_eqp_salvage =='Yes'" src="<?php echo base_url()?>images/flag-red_24x24.png" /><?php } ?>
                  
               
                  <div class="row-fluid">
                    <label>Trailer #{{$index+1}}</label>   
                  </div>                            	
                	<div class="span3 no_margin">
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>Equipment Type</label>
					
						<?php echo getequipVehicles('vh_eqp_type[]', 'class="span12 eqp_type" ng-model="vh_eqpmnt.vh_eqp_type"','',50,'');?>
             
                    </div>
						<script language="javascript">
                          $( document ).ready(function() {
                               $(".eqp_type").msDropDown().data('dd');
                               });
                         </script>
                    <div class="span1 span_11">
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>Vehicle Year</label>
                        <select name="vh_eqp_year[]" class="span10" ng-model="vh_eqpmnt.vh_eqp_year">
                        <option value="">Select</option>
						<?php 						 
						  $current_year = date("Y");
						  
						  for($i = 1985; $i <= $current_year + 1; $i++){
							echo "<option value='$i'>$i</option>";
						  } 
						?>
                        </select>
                    </div>
                    <div class="span2 span_10">
                     
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>Make</label>
						<?php echo getMakeModelVehicles('make_model_vh[]', 'class="span12" ng-model="vh_eqpmnt.make_model_vh"','',32,'');?>
                    </div>
                    <div class="span2 span_10">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>Model</label>
                        <?php echo getMakeModelVehicles('vh_eqp_model[]', 'class="span12" ng-model="vh_eqpmnt.vh_eqp_model"','',33,'');?>
                    </div>
                    <div class="span2">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>GVW</label>
                        <select class="span12" name="gvw_vh[]" id="gvw_vh" ng-model="vh_eqpmnt.gvw_vh">
                          <option value="">Select</option>
                          <option value="0-10,000 pounds">0-10,000 pounds</option>
                          <option value="10,001-26,000 pounds">10,001-26,000 pounds</option>
                          <option value="26,001-40,000 pounds">26,001-40,000 pounds</option>
                          <option value="40,001-80,000 pounds">40,001-80,000 pounds</option>
                          <option value="Over 80,000 pounds">Over 80,000 pounds</option>
                        </select>
                    </div>
                    <div class="span2 span_10">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>VIN</label>
                        <input type="text" name="vh_eqp_vin[]" class="span12" ng-model="vh_eqpmnt.vh_eqp_vin">
                    </div>
                    <div class="span2 span_10">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>Salvage</label>
                        <select name="vh_eqp_salvage[]" class="span12" ng-model="vh_eqpmnt.vh_eqp_salvage">
                        <option value="">Select</option>
               			<option value="No" >No</option>
                		<option value="Yes" >Yes</option>
                        </select>
                    </div>
                    <div class="span1">
                    	<label class="span12 no_margin">&nbsp;</label>
                    	
                		<a href="javscript:;" ng-click="remove_eqp('others', otherIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
              <div class="row-fluid top20 liability_color">
            	<div class="row-fluid">
            	<p>
            	<span class="span2 span_2 truck_liability_numbers"> </span>   </p>
                	<label><span class="numbers"><?= $vhit.'.'.++$vhteqp; ?> </span>Liability coverage</label>
                    <select name="vh_lib_coverage[]" class="span1 no_margin" ng-model="vh_eqpmnt.vh_lib_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span>
                </div>
                <?php $vhteqplib = 0;?>
               
                <div class="row-fluid top10" ng-if="vh_eqpmnt.vh_lib_coverage=='Yes'">
                	<div class="span2 span_12">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqplib; ?> </span>Liability</label>
                        <select class="span12" name="liability_vh[]" id="liability_vh" ng-model="vh_eqpmnt.liability_vh">
                  			<option value="">Select</option>
                            <option value="$1,000,000">$1,000,000</option>
                            <option value="$750,000">$750,000</option>
                            <option value="$500,000">$500,000</option>
                        </select>
                    </div>
                    <div class="span2 ">
                    
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqplib; ?> </span>Liability Ded</label>
                     
                        <select class="span10" name="liability_ded_vh[]" id="liability_ded_vh" ng-model="vh_eqpmnt.liability_ded_vh">                        
                            <option value="">Select</option>
                            <option value="$1,000">$1,000</option>
                            <option value="$1,500">$1,500</option>
                            <option value="$2,000">$2,000</option>
                            <option value="$2,500">$2,500</option>
                        </select>
                    </div>
                    <div class="span2 span_10">
                    <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqplib; ?> </span>UM</label>
                     <?php
					   $um = '';                                                               
					   $state = 'CA';
					   $att = 'class="span12 ui-state-valid myclass um_changes_option" ng-model="vh_eqpmnt.truck_um"'; 
					   echo getumcoverages('truck_um[]',$att,$um,$state);
                     ?>
                   </div>
                  <div class="span2 span_10">
                
                    <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqplib; ?> </span>PIP</label>
                    <?php   
					  $pip = '';                                                              
					  $state = 'CA';
					  $att = 'class="span12 ui-state-valid myclass pip_changes_option" ng-model="vh_eqpmnt.truck_pip" ng-if="myServ.state_pip!=&quot;CA&quot;"'; 
					  echo getpipcoverages('truck_pip[]',$att,$pip,$state);
                    ?>
                    <select name="truck_pip[]" class="span12 ui-state-valid myclass pip_changes_option" ng-model="vh_eqpmnt.truck_pip" ng-if="myServ.state_pip==&quot;CA&quot;" ng-init="vh_eqpmnt.truck_pip='Not available'" >
                       <option>Not available</option>
                    </select>
                  </div>                                  
                  <div class="span2 span_10">
                 
                    <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqplib; ?> </span>UIM</label>
                    <?php   
					  $uim = '';
					  $state = 'CA';
					  $att = 'class="span12 ui-state-valid myclass uim_changes_option" ng-model="vh_eqpmnt.truck_uim"'; 
					  echo getuimcoverages('truck_uim[]',$att,$uim,$state);
                    ?>
                  </div>
                </div>
            </div>
            
            <?php $vhteqppd = 0; ?>
              <div class="row-fluid top20 pd_color">
            	<div class="row-fluid">
               
                	<label><span class="numbers"><?= $vhit.'.'.++$vhteqp; ?> </span>PD coverage  {{other.vehicle_year_vh}}<span ng-if="other.make_truck">,{{other.make_truck}} </span><span ng-if="other.vin_vh">,{{other.vin_vh}}</span></label>
                    <select name="vh_pd_coverage[]" class="span1 no_margin" ng-model="vh_eqpmnt.vh_pd_coverage" onchange="quick_quote()">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span><span ng-if="vh_eqpmnt.vh_pd_coverage=='No'" style="color:#000;font-size:9px;">(PD coverage was not selected, but you can select it for this vehicle)</span
                </div>
                <div class="row-fluid top10" ng-if="vh_eqpmnt.vh_pd_coverage=='Yes'">

                	 <div class="span2 span_10">
                    
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqppd; ?> </span>ACV</label>
                        <input type="text" class="span12 " ng-model="vh_eqpmnt.pd_vh" maxlength="13" format    name="pd_vh[]" id="pd_vh" >
                    </div>
                    <div class="span2 span_12">
                   
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqppd; ?> </span>PD Ded</label>
                        <input type="text" class="span12 " ng-model="vh_eqpmnt.ph_ded_vh" ng-blur="min_max_val(vh_eqpmnt.ph_ded_vh,$index,'te')" title="$1,000/$10,000" maxlength="13" format name="ph_ded_vh[]" id="te_ph_ded_vh_{{$index}}" value="">
                        <span style="color:red;display:none;font-size:10px;" id="te_ph_ded_vh_span_{{$index}}">Min/Max $1,000/$10,000</span>
                    </div>
                    <div class="span3">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqppd; ?> </span>Remarks</label>
                        <input type="text" name="vh_pd_remark[]" ng-model="vh_eqpmnt.vh_pd_remark" class="span12">
                    </div>
                       <div class="span3">
                       
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqppd; ?> </span> Do you want :</label>
                        <select name="pd_want[]" ng-model="vh_eqpmnt.pd_want" class="span9">
                        <option value="">Select</option>
                        <option value="primary liability">Primary Liability</option>
                        <option value="bobtail">Bobtail</option>
                        <option value="deadhead">Deadhead</option>
                        <option value="under company policy">Under company policy</option>                        
                        </select>
                    </div>
                </div>
                
            </div>
            
            <?php $vhteqpcargo = 0; ?>
              <div class="row-fluid top20 cargo_color">
            	<div class="row-fluid">
                
                	<label><span class="numbers"><?= $vhit.'.'.++$vhteqp; ?> </span>Cargo coverage {{other.vehicle_year_vh}}<span ng-if="other.make_truck">, {{other.make_truck}}</span>, <span ng-if="other.vin_vh">{{other.vin_vh}}</span></label>
                    <select name="vh_cargo_coverage[]" class="span1 no_margin" ng-model="vh_eqpmnt.vh_cargo_coverage" onchange="quick_quote()">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span><span ng-if="vh_eqpmnt.vh_cargo_coverage=='No'" style="color:#000;font-size:9px;">(Cargo coverage was not selected, but you can select it for this vehicle)</span>
                </div>
                <div class="row-fluid top10" ng-if="vh_eqpmnt.vh_cargo_coverage=='Yes'">
                	<div class="span2 span_12">
                      
                      <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqpcargo; ?> </span>Cargo limit</label>
                      <!--<input type="text" class="span12 myclass" ng-model="vh_eqpmnt.cargo_vh" maxlength="13" format name="cargo_vh[]" id="cargo_vh" >-->
                      <?php   echo getMakeModelVehicles('cargo_vh[]', 'class="span12" ng-model="vh_eqpmnt.cargo_vh"','',49,'');?>
                    </div>
                    <div class="span2 span_11">
                     
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqpcargo; ?> </span>Cargo Ded</label>
                        <input type="text" class="span12  myclass" ng-model="vh_eqpmnt.cargo_ded_vh" maxlength="13" format name="cargo_ded_vh[]" id="cargo_ded_vh" >
                    </div>
                      <div class="span3 cargo"> 
                       
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqpcargo; ?> </span>Refrigeration breakdown:</label>	
                        <input type="checkbox" name="refrigerated_breakdown[]" ng-model="vh_eqpmnt.refrigerated_breakdown" id="refrigerated_breakdown" ng-checked="vh_eqpmnt.refrigerated_breakdown">                       
                        <div class="refrigerated_breakdown span12" ng-if="vh_eqpmnt.refrigerated_breakdown==true || vh_eqpmnt.refrigerated_breakdown=='on'">
                         	<label>Deductible : </label>
                            <input type="text" class="span6 " maxlength="13"  format name="deductible[]" ng-model="vh_eqpmnt.deductible" id="deductible">
                         </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span12">
                     
                     <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqpcargo; ?> </span>Commodities Hauled</label>
                     <input type="text" name="commodities_haulted_truck[]" ng-model="vh_eqpmnt.commodities_haulted" class="span6 commhauled2 ui-state-valid myclass">  
                     <label>if you are requesting cargo coverage, please be as specific as possible, For Example, Do not list Dry Goods, General Freight or Dry Freight </label>
                   </div>   
                 </div>
                           <script>
		    setTimeout(function(){
			  $('input').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 });
				  $('select').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 }); 
				  $('input').blur(function() {	
					 if($(this).val()!=''){	
					  $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					  $(this).removeAttr('style');
					  }
				   });
				
				$('select').blur(function() {
					 if($(this).val()!=''){		
					$(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					 $(this).removeAttr('style'); 
					 }
				   });
		 },200);
  
		  </script> 
                </div>
                 
             </div>
                <div class="row-fluid">
                <input type="hidden" name="vh_eqp_types[]" value="other"/>
              
        <label><span class="numbers truck_needcargo_numbers"></span>Please upload any registration file from the vehicle:</label>
                <input type="file" multiple name="vehicle_file[][]" ng-model="vh_eqpmnt.vehicle_file" class="span1 span_25">
                
                <a href="<?php echo base_url();?>uploads/vehicle_file/{{vh_eqpmnt.vehicle_file}}" download>{{vh_eqpmnt.vehicle_file}}</a>
           </div>
          </div>
          
         <div class="right_section" <?php  if($underwriter=="underwriter"){}else{echo'style="display:none"';}; ?>>
            <div class="row-fluid top20">
            	<table class="table_css_w">
                 <tr>&nbsp;</tr>
                 <tr>&nbsp;</tr>
                 <tr>&nbsp;</tr>
                  <tr>
                     <td style="width:40%">
                	   Liability Price
                       </td>
                     <td>
                     <input type="text" name="liability[]" ng-model="vh_eqpmnt.liability_vh" ng-if="vh_eqpmnt.liability==NULL" format class="span10"/>
                     <input type="text" name="liability[]" ng-model="vh_eqpmnt.liability" ng-if="vh_eqpmnt.liability!=NULL" format class="span10"/>
                    </td>                    
                  </tr>
                  <tr>
                    <td>                     	
                	  UM
                     </td>
                     <td>
                        <input type="text" name="um[]" ng-model="vh_eqpmnt.truck_um" ng-if="vh_eqpmnt.um==NULL"  class="span10"/>
                        <input type="text" name="um[]" ng-model="vh_eqpmnt.um" ng-if="vh_eqpmnt.um!=NULL" class="span10"/>
                    </td>
                  </tr>
                     <td>    	
                	   PIP
                     </td>
                     <td> 
                        <input type="text" name="pip[]" ng-model="vh_eqpmnt.truck_pip" ng-if="vh_eqpmnt.pip==NULL" format class="span10"/>
                        <input type="text" name="pip[]" ng-model="vh_eqpmnt.pip" ng-if="vh_eqpmnt.pip!=NULL" format class="span10"/>
                    </td>
                </tr>
                <tr>
                    <td>                     	
                	   UIM
                    </td>
                    <td>
                        <input type="text" name="uim[]" ng-model="vh_eqpmnt.truck_uim" ng-if="vh_eqpmnt.uim==NULL" format class="span10"/>
                        <input type="text" name="uim[]" ng-model="vh_eqpmnt.uim" ng-if="vh_eqpmnt.uim!=NULL" format class="span10"/>
                    </td>
                </tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>  
                 <tr>
                    <td>                     	
                	   PD
                    </td>
                    <td>
                        <input type="text" name="pd[]" ng-model="vh_eqpmnt.pd_vh" ng-if="vh_eqpmnt.pd==NULL" class="span10" maxlength="13" format/>
                        <input type="text" name="pd[]" ng-model="vh_eqpmnt.pd" ng-if="vh_eqpmnt.pd!=NULL" class="span10" maxlength="13" format/>
                    </td>
                </tr> 
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                 <tr>
                    <td>                     	
                	   Cargo
                    </td>
                    <td>
                        <input type="text" name="cargo[]" ng-model="vh_eqpmnt.cargo_vh" ng-if="vh_eqpmnt.cargo!=NULL"class="span10" maxlength="13" format/>
                        <input type="text" name="cargo[]" ng-model="vh_eqpmnt.cargo" ng-if="vh_eqpmnt.cargo==NULL"class="span10" maxlength="13" format/>
                    </td>
                </tr> 
                
            </table>          
          </div>           
        </div>
        <?php $vhteqp_c=0; ?> 
        <div class="" style="min-height:10px">  
         <div class="top40">
          <div class="row-fluid no_margin"> 
                	<label><span class="numbers truck_equipment_numbers"><?= $vhit; ?> B </span>Do you have more trailers (pick up or drops)?</label>
                    <select name="vh_equip_trailer[]" class="span1 span_7dot6 no_margin" ng-model="vh_eqpmnt.vh_equip_trailer">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span ng-if="vh_eqpmnt.vh_equip_trailer=='Yes'">
                    <label>How many ?</label>  
                    <select name="vh_equipment_number[]" class="span1 span_7dot6 no_margin" ng-model="vh_eqpmnt.vh_equipment_number" ng-change="vh_equip_tr(vh_eqpmnt.vh_equipment_number,'others',otherIndex,$index);">
                    	<option value="">Select</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select> 
                    </span>
                    </div> 
                    <div ng-if="vh_eqpmnt.vh_equip_trailer=='Yes' && vh_eqpmnt.vh_equipment_number > 0">
                      <div  ng-repeat="vh_eqpm in vh_eqpmnt.vh_eqpmntss" class="top20" >  
                  <input type="hidden" ng-model="vh_eqpm.vh_eqp_id" value="{{vh_eqpm.vh_eqp_id}}" name="vh_eqp_id_o{{eqpIndex}}[]"/>           
                  <div class="row-fluid">
                    <label>Trailer #{{otherIndex+1}}</label>   
                  </div>                                	
                   <div class="span3 no_margin">
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp_c; ?> </span>Equipment Type</label>
					
						<?php echo getequipVehicles('vh_eqp_typess_tr_o[]', 'class="span12 eqp_type" ng-model="vh_eqpm.vh_eqp_type"','',50,'');?>
             
                    </div>
						<script language="javascript">
                          $( document ).ready(function() {
                               $(".eqp_type").msDropDown().data('dd');
                               });
                         </script>
                    <div class="span1 span_11">
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp_c; ?> </span>Vehicle Year</label>
                        <select name="vh_eqp_year_o{{eqpIndex}}[]" class="span10" ng-model="vh_eqpm.vh_eqp_year">
                        <option value="">Select</option>
						<?php 						 
						  $current_year = date("Y");
						  
						  for($i = 1985; $i <= $current_year + 1; $i++){
							echo "<option value='$i'>$i</option>";
						  } 
						?>
                        </select>
                    </div>
                    <div class="span2 span_10">
                     
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp_c; ?> </span>Make</label>
						<?php echo getMakeModelVehicles('make_model_vh_o{{eqpIndex}}[]', 'class="span12" ng-model="vh_eqpm.make_model_vh"','',32,'');?>
                    </div>
                    <div class="span2 span_10">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp_c; ?> </span>Model</label>
                        <?php echo getMakeModelVehicles('vh_eqp_model_o{{eqpIndex}}[]', 'class="span12" ng-model="vh_eqpm.vh_eqp_model"','',33,'');?>
                    </div>
                    <div class="span2">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp_c; ?> </span>GVW</label>
                        <select class="span12" name="gvw_vh_o{{eqpIndex}}[]" id="gvw_vh" ng-model="vh_eqpm.gvw_vh">
                          <option value="">Select</option>
                          <option value="0-10,000 pounds">0-10,000 pounds</option>
                          <option value="10,001-26,000 pounds">10,001-26,000 pounds</option>
                          <option value="26,001-40,000 pounds">26,001-40,000 pounds</option>
                          <option value="40,001-80,000 pounds">40,001-80,000 pounds</option>
                          <option value="Over 80,000 pounds">Over 80,000 pounds</option>
                        </select>
                    </div>
                    <div class="span2 span_10">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp_c; ?> </span>VIN</label>
                        <input type="text" name="vh_eqp_vin_o{{eqpIndex}}[]" class="span12" ng-model="vh_eqpm.vh_eqp_vin">
                    </div>
                    <div class="span2 span_10">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp_c; ?> </span>Salvage</label>
                        <select name="vh_eqp_salvage_o{{eqpIndex}}[]" class="span12" ng-model="vh_eqpm.vh_eqp_salvage">
                        <option value="">Select</option>
               			<option value="No" >No</option>
                		<option value="Yes" >Yes</option>
                        </select>
                    </div>
                    <div class="span1">
                    	<label class="span12 no_margin">&nbsp;</label>
                    	
                		<a href="javscript:;" ng-click="vh_equip_tr_remove('others',otherIndex,$index);" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
              <div class="row-fluid top20 liability_color">
            	<div class="row-fluid">
            	<p>
            	<span class="span2 span_2 truck_liability_numbers"> </span>   </p>
                	<label><span class="numbers"><?= $vhit.'.'.++$vhteqp_c; ?> </span>Liability coverage</label>
                    <select name="vh_lib_coverage_o{{eqpIndex}}[]" class="span1 no_margin" ng-model="vh_eqpm.vh_lib_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span>
                </div>
                <?php $vhteqplib_c = 0;?>
               
                <div class="row-fluid top10" ng-if="vh_eqpm.vh_lib_coverage=='Yes'">
                	<div class="span2 span_12">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqplib_c; ?> </span>Liability</label>
                        <select class="span12" name="liability_vh_o{{eqpIndex}}[]" id="liability_vh" ng-model="vh_eqpm.liability_vh">
                  			<option value="">Select</option>
                            <option value="$1,000,000">$1,000,000</option>
                            <option value="$750,000">$750,000</option>
                            <option value="$500,000">$500,000</option>
                        </select>
                    </div>
                    <div class="span2 ">
                    
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqplib_c; ?> </span>Liability Ded</label>
                     
                        <select class="span10" name="liability_ded_vh_o{{eqpIndex}}[]" id="liability_ded_vh" ng-model="vh_eqpm.liability_ded_vh">                        
                            <option value="">Select</option>
                            <option value="$1,000">$1,000</option>
                            <option value="$1,500">$1,500</option>
                            <option value="$2,000">$2,000</option>
                            <option value="$2,500">$2,500</option>
                        </select>
                    </div>
                    <div class="span2 span_10">
                    <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqplib_c; ?> </span>UM</label>
                     <?php
					   $um = '';                                                               
					   $state = 'CA';
					   $att = 'class="span12 ui-state-valid myclass um_changes_option" ng-model="vh_eqpm.truck_um"'; 
					   echo getumcoverages('truck_um_o{{eqpIndex}}[]',$att,$um,$state);
                     ?>
                   </div>
                  <div class="span2 span_10">
                
                    <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqplib_c; ?> </span>PIP</label>
                    <?php   
					  $pip = '';                                                              
					  $state = 'CA';
					  $att = 'class="span12 ui-state-valid myclass pip_changes_option" ng-model="vh_eqpm.truck_pip" ng-if="myServ.state_pip!=&quot;CA&quot;"'; 
					  echo getpipcoverages('truck_pip_o{{eqpIndex}}[]',$att,$pip,$state);
                    ?>
                    <select name="truck_pip_o{{eqpIndex}}[]" class="span12 ui-state-valid myclass pip_changes_option" ng-model="vh_eqpmnt.truck_pip" ng-if="myServ.state_pip==&quot;CA&quot;" ng-init="vh_eqpmnt.truck_pip='Not available'" >
                       <option>Not available</option>
                    </select>
                  </div>                                  
                  <div class="span2 span_10">
                 
                    <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqplib_c; ?> </span>UIM</label>
                    <?php   
					  $uim = '';
					  $state = 'CA';
					  $att = 'class="span12 ui-state-valid myclass uim_changes_option" ng-model="vh_eqpm.truck_uim"'; 
					  echo getuimcoverages('truck_uim_o{{eqpIndex}}[]',$att,$uim,$state);
                    ?>
                  </div>
                </div>
            </div>
            
            <?php $vhteqppd_c = 0; ?>
              <div class="row-fluid top20 pd_color">
            	<div class="row-fluid">
               
                	<label><span class="numbers"><?= $vhit.'.'.++$vhteqp_c; ?> </span>PD coverage  {{other.vehicle_year_vh}}<span ng-if="other.make_truck">,{{other.make_truck}} </span><span ng-if="other.vin_vh">,{{other.vin_vh}}</span></label>
                    <select name="vh_pd_coverage_o{{eqpIndex}}[]" class="span1 no_margin" ng-model="vh_eqpm.vh_pd_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span>
                </div>
                <div class="row-fluid top10" ng-if="vh_eqpm.vh_pd_coverage=='Yes'">

                	 <div class="span2 span_10">
                    
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqppd_c; ?> </span>ACV</label>
                        <input type="text" class="span12 " ng-model="vh_eqpm.pd_vh" maxlength="13" format    name="pd_vh_o{{eqpIndex}}[]" id="pd_vh" >
                    </div>
                    <div class="span2 span_12">
                   
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqppd_c; ?> </span>PD Ded</label>
                        <input type="text" class="span12 " ng-model="vh_eqpm.ph_ded_vh" ng-blur="min_max_val(vh_eqpmnt.ph_ded_vh,$index,'te')" title="$1,000/$10,000" maxlength="13" format name="ph_ded_vh_o{{eqpIndex}}[]" id="te_ph_ded_vh_{{$index}}" value="">
                        <span style="color:red;display:none;font-size:10px;" id="te_ph_ded_vh_span_{{$index}}">Min/Max $1,000/$10,000</span>
                    </div>
                    <div class="span3">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqppd_c; ?> </span>Remarks</label>
                        <input type="text" name="vh_pd_remark_o{{eqpIndex}}[]" ng-model="vh_eqpm.vh_pd_remark" class="span12">
                    </div>
                       <div class="span3">
                       
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqppd_c; ?> </span> Do you want :</label>
                        <select name="pd_want_o{{eqpIndex}}[]" ng-model="vh_eqpm.pd_want" class="span9">
                        <option value="">Select</option>
                        <option value="primary liability">Primary Liability</option>
                        <option value="bobtail">Bobtail</option>
                        <option value="deadhead">Deadhead</option>
                        <option value="under company policy">Under company policy</option>                        
                        </select>
                    </div>
                              <script>
		    setTimeout(function(){
			  $('input').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 });
				  $('select').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 }); 
				  $('input').blur(function() {	
					 if($(this).val()!=''){	
					  $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					  $(this).removeAttr('style');
					  }
				   });
				
				$('select').blur(function() {
					 if($(this).val()!=''){		
					$(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					 $(this).removeAttr('style'); 
					 }
				   });
		 },200);
  
		  </script> 
                </div>
                
            </div>
            
            <?php $vhteqpcargo = 0; ?>
              <div class="row-fluid top20 cargo_color">
            	<div class="row-fluid">
                
                	<label><span class="numbers"><?= $vhit.'.'.++$vhteqp_c; ?> </span>Cargo coverage {{other.vehicle_year_vh}}<span ng-if="other.make_truck">, {{other.make_truck}}</span>, <span ng-if="other.vin_vh">{{other.vin_vh}}</span></label>
                    <select name="vh_cargo_coverage_o{{eqpIndex}}[]" class="span1 no_margin" ng-model="vh_eqpm.vh_cargo_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span>
                </div>
                <div class="row-fluid top10" ng-if="vh_eqpm.vh_cargo_coverage=='Yes'">
                	<div class="span2 span_12">
                      
                      <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqpcargo_c; ?> </span>Cargo limit</label>
                      <!--<input type="text" class="span12 myclass" ng-model="vh_eqpmnt.cargo_vh" maxlength="13" format name="cargo_vh[]" id="cargo_vh" >-->
                      <?php   echo getMakeModelVehicles('cargo_vh_o{{eqpIndex}}[]', 'class="span12" ng-model="vh_eqpmnt.cargo_vh"','',49,'');?>
                    </div>
                    <div class="span2 span_11">
                     
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqpcargo_c; ?> </span>Cargo Ded</label>
                        <input type="text" class="span12  myclass" ng-model="vh_eqpm.cargo_ded_vh" maxlength="13" format name="cargo_ded_vh_{{$index}}[]" id="cargo_ded_vh" >
                    </div>
                      <div class="span3 cargo"> 
                       
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqpcargo_c; ?> </span>Refrigeration breakdown:</label>	
                        <input type="checkbox" name="refrigerated_breakdown_o{{eqpIndex}}[]" ng-model="vh_eqpmnt.refrigerated_breakdown" id="refrigerated_breakdown" ng-checked="vh_eqpmnt.refrigerated_breakdown">                       
                        <div class="refrigerated_breakdown span12" ng-if="vh_eqpm.refrigerated_breakdown==true || vh_eqpmnt.refrigerated_breakdown=='on'">
                         	<label>Deductible : </label>
                            <input type="text" class="span6 " maxlength="13"  format name="deductible_o{{eqpIndex}}[]" ng-model="vh_eqpmnt.deductible" id="deductible">
                         </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span12">
                     
                     <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp_c.'.'.++$vhteqpcargo_c; ?> </span>Commodities Hauled</label>
                     <input type="text" name="commodities_haulted_truck_o{{eqpIndex}}[]" ng-model="vh_eqpm.commodities_haulted" class="span6 commhauled2 ui-state-valid myclass">  
                     <label>if you are requesting cargo coverage, please be as specific as possible, For Example, Do not list Dry Goods, General Freight or Dry Freight </label>
                   </div>   
                 </div>
                           <script>
		    setTimeout(function(){
			  $('input').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 });
				  $('select').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 }); 
				  $('input').blur(function() {	
					 if($(this).val()!=''){	
					  $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					  $(this).removeAttr('style');
					  }
				   });
				
				$('select').blur(function() {
					 if($(this).val()!=''){		
					$(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					 $(this).removeAttr('style'); 
					 }
				   });
		 },200);
  
		  </script> 
                </div>
                  <input type="hidden" name="vh_eqp_types_o{{eqpIndex}}[]" value="other"/>
             </div>
                <div class="row-fluid">
               
              
                <label><span class="numbers truck_needcargo_numbers"></span>Please upload any registration file from the vehicle:</label>
                <input type="file" multiple name="vehicle_file[][]" ng-model="vh_eqpmnt.vehicle_file" class="span1 span_25">
                
                <a href="<?php echo base_url();?>uploads/vehicle_file/{{vh_eqpmnt.vehicle_file}}" download>{{vh_eqpmnt.vehicle_file}}</a>
               </div>
             </div>
                   </div>
                 
           </div>
         </div>
       </div>
                   <div class="row-fluid border_equp" <?php  if($underwriter=="underwriter"){}else{echo'style="display:none"';}; ?>>
                        <span class="pull-right" style="  margin-right: -6px;">
                              <input type="text" name="total_equipment[]" ng-model="vh_eqpmnt.total_equipment" ng-if="vh_eqpmnt.total_equip==NULL" class="span10"/>
                              <input type="text" name="total_equip[]" ng-model="vh_eqpmnt.total_equip" ng-if="vh_eqpmnt.total_equip!=NULL" class="span10"/>
                            </span>
                          <span class="pull-right" style="  margin-right: 6px;">                    	
                             Total For equipment {{$index+1}}
                        </span >
                   
                       </div> 
                    </div>
                </div>
            </div>
            
            
            <?php $vhitlib = 0; ?>
             <div class="row-fluid top20" >
             <div class="left_section">
              <div class="row-fluid top20 liability_color">
            	<div class="row-fluid">
                	<label><span class="numbers"><?= ++$vhit; ?> </span>Liability coverage</label>
                    <select name="t_vh_lib_coverage[]" class="span1 no_margin" ng-model="other.vh_lib_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span>
                </div>
               
                <div class="row-fluid top10" ng-if="other.vh_lib_coverage=='Yes'">
                	<div class="span2 span_12">
                    	<label><span class="numbers"><?= $vhit.'.'.++$vhitlib;?> </span> Liability</label>
                        <select class="span12 lia_vh" name="t_liability_vh[]" ng-model="other.liability_vh" id="liability_vh">
                  			<option value="">Select</option>
                            <option value="$1,000,000">$1,000,000</option>
                            <option value="$750,000">$750,000</option>
                            <option value="$500,000">$500,000</option>
                        </select>
                    </div>
                    <div class="span2 span_8">
                        <label><span class="numbers"><?= $vhit.'.'.++$vhitlib;?> </span> Liability Ded</label>
                        <select class="span12 lia_ded_vh" name="t_liability_ded_vh[]" ng-model="other.liability_ded_vh" id="liability_ded_vh">                        
                            <option value="">Select</option>
                            <option value="$1,000">$1,000</option>
                            <option value="$1,500">$1,500</option>
                            <option value="$2,000">$2,000</option>
                            <option value="$2,500">$2,500</option>
                        </select>
                    </div>
                    <div class="span2 span_10">
                     <label><span class="numbers"><?= $vhit.'.'.++$vhitlib;?> </span>UM</label>
                     <?php
					   $um = '';                                                               
					   $state = 'CA';
					   $att = 'class="span12 ui-state-valid myclass um_changes_option" ng-model="other.truck_um"'; 
					   echo getumcoverages('t_truck_um[]',$att,$um,$state);
                     ?>
                   </div>
                  <div class="span2 span_10">
                    <label><span class="numbers"><?= $vhit.'.'.++$vhitlib;?> </span>PIP</label>
                    <?php   
					  $pip = '';                                                              
					  $state = 'CA';
					  $att = 'class="span12 ui-state-valid myclass pip_changes_option" ng-model="other.truck_pip" ng-if="myServ.state_pip!=&quot;CA&quot;"'; 
					  echo getpipcoverages('t_truck_pip[]',$att,$pip,$state);
                    ?>
                     <select name="t_truck_pip[]" class="span12 ui-state-valid myclass pip_changes_option" ng-model="other.truck_pip" ng-if="myServ.state_pip==&quot;CA&quot;" ng-init="other.truck_pip='Not available'" >
                       <option>Not available</option>
                    </select>
                  </div>                                  
                  <div class="span2 span_10">
                    <label><span class="numbers"><?= $vhit.'.'.++$vhitlib;?></span> UIM</label>
                    <?php   
					  $uim = '';
					  $state = 'CA';
					  $att = 'class="span12 ui-state-valid myclass uim_changes_option" ng-model="other.truck_uim"'; 
					  echo getuimcoverages('t_truck_uim[]',$att,$uim,$state);
                    ?>
                  </div>
                </div>
            </div>
            <?php $vhitpd = 0; ?>
              <div class="row-fluid top20 pd_color">
            	<div class="row-fluid">
                	<label><span class="numbers "><?= ++$vhit; ?> </span>PD coverage {{other.vehicle_year_vh}}<span ng-if="other.make_truck">,{{other.make_truck}}</span><span ng-if="other.vin_vh">,{{other.vin_vh}}</span></label>
                    <select name="t_vh_pd_coverage[]" class="span1 no_margin" ng-model="other.vh_pd_coverage" onchange="quick_quote()">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span><span ng-if="other.vh_pd_coverage=='No'" style="color:#000;font-size:9px;">(PD coverage was not selected, but you can select it for this vehicle)</span>
                </div>
                <div class="row-fluid top10" ng-if="other.vh_pd_coverage=='Yes'">
                	 <div class="span2 span_10">
                        <label><span class="numbers"><?= $vhit.'.'.++$vhitpd;?> </span>ACV</label>
                        <input type="text" class="span12 " ng-model="other.pd_vh" format  name="t_pd_vh[]" id="pd_vh"  >
                    </div>
                    <div class="span2 span_12" >
                        <label><span class="numbers"><?= $vhit.'.'.++$vhitpd;?> </span>PD Ded</label>
                        <input type="text" class="span12 " name="t_ph_ded_vh[]" title="$1,000/$10,000" ng-blur="min_max_val(other.ph_ded_vh,otherIndex,'o')" ng-model="other.ph_ded_vh" format id="o_ph_ded_vh_{{otherIndex}}">
                        <span style="color:red;display:none;font-size:10px;" id="o_ph_ded_vh_span_{{otherIndex}}">Min/Max $1,000/$10,000</span>
                    </div>
                    <div class="span3">
                    	<label><span class="numbers"><?= $vhit.'.'.++$vhitpd;?> </span>Remarks</label>
                        <input type="text" name="t_vh_pd_remark[]" ng-model="other.vh_pd_remark" class="span12">
                    </div>
                      <div class="span3">
                        <label><span class="numbers"><?= $vhit.'.'.++$vhitpd;?> </span>Do you want :</label>
                        <select name="t_pd_want[]" ng-model="other.pd_want" class="span9" disabled="disabled">
                        <option value="">Select</option>
                        <option value="primary liability">Primary Liability</option>
                        <option value="bobtail">Bobtail</option>
                        <option value="deadhead">Deadhead</option>
                        <option value="under company policy">Under company policy</option>                        
                        </select>
                    </div>
                </div>
            </div>
            <?php $vhitcargo = 0; ?>
              <div class="row-fluid top20 cargo_color">
            	<div class="row-fluid">
                	<label><span class="numbers "><?= ++$vhit; ?> </span>Cargo coverage {{other.vehicle_year_vh}}<span ng-if="other.make_truck">,{{other.make_truck}}</span><span ng-if="other.vin_vh">,{{other.vin_vh}}</span></label>
                    <select name="t_vh_cargo_coverage[]" class="span1 no_margin" ng-model="other.vh_cargo_coverage" onchange="quick_quote()">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span><span ng-if="other.vh_cargo_coverage=='No'" style="color:#000;font-size:9px;">(Cargo coverage was not selected, but you can select it for this vehicle)</span>
                </div>
                <div class="row-fluid top10" ng-if="other.vh_cargo_coverage=='Yes'">
                	<div class="span2 span_10">
                      <label><span class="numbers"><?= $vhit.'.'.++$vhitcargo;?> </span>Cargo limit</label>
                      <?php   echo getMakeModelVehicles('t_cargo_vh[]', 'class="span12" ng-model="other.cargo_vh"','',49,'');?>
                     <!-- <input type="text" class="span12  myclass" ng-model="other.cargo_vh"  maxlength="13" format  name="t_cargo_vh[]" id="cargo_vh">-->
                    </div>
                    <div class="span2 span_10">
                        <label><span class="numbers"><?= $vhit.'.'.++$vhitcargo;?> </span>Cargo Ded</label>
                        <input type="text" class="span12  myclass" ng-model="other.cargo_ded_vh" maxlength="13" format  name="t_cargo_ded_vh[]" id="cargo_ded_vh" >
                    </div>
                     <div class="span3 cargo"> 
                    	<label><span class="numbers"><?= $vhit.'.'.++$vhitcargo;?> </span>Refrigeration breakdown:</label>
                        <input type="checkbox" name="t_refrigerated_breakdown[]" ng-model="other.refrigerated_breakdown" id="refrigerated_breakdown" ng-checked="other.refrigerated_breakdown">            
                        <div class="refrigerated_breakdown span12" ng-if="other.refrigerated_breakdown==true || other.refrigerated_breakdown=='on'">
                         	<label>Deductible : </label>
                            <input type="text" class="span6 " maxlength="13" ng-model="other.deductible" format name="t_deductible[]" id="deductible">
                         </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span12">
                     <label><span class="numbers"><?= $vhit.'.'.++$vhitcargo;?> </span>Commodities Hauled</label>
                     <input type="text" name="t_commodities_haulted_truck[]" ng-model="other.commodities_haulted" class="span6 commhauled2 ui-state-valid myclass">  
                     <label>if you are requesting cargo coverage, please be as specific as possible, For Example, Do not list Dry Goods, General Freight or Dry Freight </label>
                   </div>   
                 </div>
                </div>
                 
             </div>
              <div class="row-fluid">
              <input type="hidden"  name="t_vh_eqp_types[]" value="other">
              <label>Please upload any registration file from the vehicle:</label>
              <input type="file" multiple name="t_vehicle_file[]" class="span1 span_25" value="{{other.vehicle_file}}">
               <?php $files_o=($other_vh)?!empty($other_vh[0]['vehicle_file'])?explode(',',$other_vh[0]['vehicle_file']):'':''; 
				 
				 if($files_o){
					 foreach($files_o as $file){
				?>
                 <br/><a href="<?php echo base_url();?>uploads/vehicle_file/<?= $file;?>" download><?= $file;?></a>
               <?php } } ?>
           <!--   <a href="<?php echo base_url();?>uploads/vehicle_file/{{other.vehicle_file}}" download>{{other.vehicle_file}}</a>-->
           </div>
            </div>
           <div class="right_section" <?php  if($underwriter=="underwriter"){}else{echo'style="display:none"';}; ?>>
            <div class="row-fluid top20" >
            	<table class="table_css_w">
                 <tr>&nbsp;</tr>
                 <tr>&nbsp;</tr>
                 <tr>&nbsp;</tr>
                  <tr>
                     <td style="width:40%">
                	   Liability Price
                       </td>
                     <td>
                      <input type="text" name="liability_price[]" ng-model="other.liability_vh" ng-if="other.liability==NULL" format   class="span10"/>
                       <input type="text" name="liability_price[]" ng-model="other.liability" ng-if="other.liability!=NULL" format   class="span10"/>
                    </td>                    
                  </tr>
                  <tr>
                    <td>                     	
                	  UM
                     </td>
                     <td>
                        <input type="text" name="um_price[]" ng-model="other.truck_um" ng-if="other.um==NULL" class="span10"/>
                        <input type="text" name="um_price[]" ng-model="other.um" ng-if="other.um!=NULL" format   class="span10"/>
                    </td>
                  </tr>
                     <td>    	
                	   PIP
                     </td>
                     <td> 
                        <input type="text" name="pip_price[]" ng-model="other.truck_pip" ng-if="other.pip==NULL" class="span10"/>
                        <input type="text" name="pip_price[]" ng-model="other.pip" ng-if="other.pip!=NULL" format   class="span10"/>
                    </td>
                </tr>
                <tr>
                    <td>                     	
                	   UIM
                    </td>
                    <td>
                        <input type="text" name="uim_price[]" ng-model="other.truck_uim" ng-if="other.uim==NULL" class="span10"/>
                        <input type="text" name="uim_price[]" ng-model="other.uim" ng-if="other.uim!=NULL" class="span10"/>
                    </td>
                </tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>  
                 <tr>
                    <td>                     	
                	   PD
                    </td>
                    <td>
                        <input type="text" name="pd_price[]" ng-model="other.pd_vh" ng-if="other.pd==NULL" format class="span10"/>
                        <input type="text" name="pd_price[]" ng-model="other.pd" ng-if="other.pd!=NULL" format class="span10"/>
                    </td>
                </tr> 
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                <tr>
                    <td>                     	
                	   Cargo
                    </td>
                    <td>
                        <input type="text" name="cargo_price[]" ng-model="other.cargo_vh" ng-if="other.cargo==NULL" format class="span10"/>
                        <input type="text" name="cargo_price[]" ng-model="other.cargo" ng-if="other.cargo!=NULL" format class="span10"/>
                    </td>
                </tr> 
                
            </table>          
          </div>          
        </div>       
       </div>
            <div class="row-fluid border_equp" <?php  if($underwriter=="underwriter"){}else{echo'style="display:none"';}; ?>>
                        <span class="pull-right" style="  margin-right: -6px;">
                              <input type="text" name="sub_total_price[]" maxlength="13" ng-model="other.sub_total_price" ng-if="other.total_price==NULL" format class="span10"/>
                              <input type="text" name="sub_total_price[]" maxlength="13" ng-model="other.total_price"  ng-if="other.total_price!=NULL" format class="span10"/>
                              </span>
                          <span class="pull-right" style="  margin-right: 6px;">                    	
                            Sub Total Vehicle #{{$index+1}} 
                        </span >
                      </div>
                    </div>
                   </div>
                
                <!--    <div class="row-fluid black">
                       NOTE: If you want OR do not want a particular coverage then you might check them on or check them off
                   </div>-->
                </div>
        </div>