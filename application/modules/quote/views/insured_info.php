<fieldset class="insured_info_box" ng-controller="insuredCtrl" id="ins_divs">
<legend ><div><?php if($underwriter_u=="underwriter") { ?><span class="characters_css_l" ><label style="color:white" ng-if="insured_info.insured_name">Insured Name :</label><label>{{insured_info.insured_name}} {{insured_info.insured_middle_name}} {{insured_info.insured_last_name}}</label></span><?php } ?><span class="characters"></span> <?php if($underwriter_u=="underwriter") { ?><span class="characters_css_r" ><label style="color:white" ng-if="insured_info.dba">DBA :</label><label style="margin-right:2px">{{insured_info.dba}}</label></span><?php } ?>
</div><div>B Insured info</div>
   <div class="pull-right icon_show">
          <span class="green" id="show-ins" title="Show" style="display:none" onclick="ins_section('show')"> <img src="<?= base_url();?>images/hide.png"></span>
          <span class="red" id="hide-ins" title="Hide" onclick="ins_section('hide')"> <img src="<?= base_url();?>/images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
         </div>
      </legend>
<div class="well" id="ins_div">  	
  <div class="row-fluid" > 
  	<div class="row-fluid">
    	<label id="label_per_email_type"><span class="numbers"></span>This is a</label>
        <select class="span2 span_8 no_margin in_this_is" id="in_this_is" ng-model="insured_info.in_this_is" onchange="corp_name(this.value)" name="in_this_is">
        	<option>Corporation</option>
            <option>Individual</option>
        </select>
    </div>
  	<div class="row-fluid top20">
    	<label ng-if="insured_info.in_this_is=='Corporation'">Person in charge</label>
        <label ng-if="insured_info.in_this_is=='Individual'">Insured name</label>
        <label ng-if="!insured_info.in_this_is">Person in charge / Insured name</label>
    </div>
    
    <div class="row-fluid">
        <input   name="insured_id"  type="hidden"  class="span12 first_name myclass" ng-model="insured_info.insured_id" value="{{insured_info.insured_id}}" >
        <div class="span2 span_10">
          <label id="label_insured_name"><span class="numbers"></span>First name</label>
          <input type="text" maxlength="255" class="span12 first_name myclass" placeholder="First Name" name="insured_name" id="insured_name" ng-model="insured_info.insured_name">
        </div>
        <div class="span2 span_10">
          <label id="label_insured_middle_name">Middle name</label>
          <input type="text"  class="span12 middle_name myclass" name="insured_middle_name" placeholder="Middle Name" id="insured_middle_name" ng-model="insured_info.insured_middle_name">
        </div>
        <div class="span2 span_10">
          <label id="label_insured_last_name">last Name</label>
          <input type="text" class="span12 last_name myclass" placeholder="Last Name" name="insured_last_name" id="insured_last_name" ng-model="insured_info.insured_last_name">
        </div>
        <div class="span3 span_10">
          <label id="label_dba"><span class="numbers"></span>DBA</label>
          <input id="dba" name="dba" type="text" class="span12 myclass" maxlength="255" ng-model="insured_info.dba"  >
        </div>
        <div class="span2">
        	<label id="label_in_address"><span class="numbers"></span>Street</label>
            <input type="text" name="in_address" class="span12" id="in_address"  ng-model="insured_info.in_address" ng-change="google_address()">
        </div>
        <div class="span1">
        	<label id="label_in_city"><span class="numbers"></span>City</label>
            <input type="text" name="in_city" class="span12 zip_code_i" id="in_city" ng-model="insured_info.in_city" ng-change="google_address()">
        </div>
        <div class="span1">
        	<label id="label_in_state"><span class="numbers"></span>State</label>
             <?php  echo get_state_dropdown('in_state','', $attr='class="span12 zip_code_i" id="in_state" ng-model="insured_info.in_state" ng-change="checkLicense(insured_info.in_state)"') ?>
                                      
            
        </div>
        <div class="span2 span_10">
        	<label id="label_in_county"><span class="numbers"></span>County</label>
            <span id="in_county_span">
             <?php   $city=!empty($insured_info[0]->in_city)?$insured_info[0]->in_city:'';
					 $state=!empty($insured_info[0]->in_state)?$insured_info[0]->in_state:'';										 
			 ?>
             <?php echo getCounty('in_county', 'class="span12" onChange="getZipCodes_i(this.value)" id="in_county"  ng-model="insured_info.in_county"',"$city","$state"); ?>                                
             </span>                   
         <!--   <input type="text" name="in_county" class="span12 zip_code_i" id="in_county" ng-model="insured_info.in_county" ng-change="google_address()">-->
        </div>
        <div class="span1">
        	<label id="label_in_zip"><span class="numbers"></span>Zip</label>
            <span id="in_zip_span">
             <?php echo getZipCode('in_zip', 'class="span12" id="in_zip"  ng-model="insured_info.in_zip"',"$city","$state"); ?>                                
            </span>                    
            <!--<input type="text" name="in_zip" class="span12" id="in_zip" ng-model="insured_info.in_zip" ng-change="google_address()">-->
        </div>
        <div class="span1">
        	<label id="label_in_country"><span class="numbers"></span>Country</label>
             <?php echo getCountryDropDown1('in_country', 'class="span10"  id="in_country" ng-model="insured_info.in_country" ng-change="google_address()"','',''); ?>
             
        </div>
      </div>       
   
      <div class="row-fluid">
                           <span class="span7">
                             <table id="table" class="tables_ins">                                  
                                    <tr>
					                 <td style="width:13%" id="label_insured_phone_type"><span class="numbers"></span>Phone Type</td>	
                                     <td style="width:15%" id="label_insured_code_type"><span class="numbers"></span>Country Code</td>	
                                     <td style="width:12%" id="label_insured_area_code"><span class="numbers"></span>Area code</td>	
                                     <td id="label_insured_prefix"><span class="numbers"></span>Prefix</td>
                                     <td id="label_insured_suffix"><span class="numbers"></span>Suffix</td>
                                     <td style="width: 11%;" id="label_insured_extension"><span class="numbers"></span>Extension</td>	
                                     <td style="width: 16%;!important" id="label_insured_country"><span class="numbers"></span>Priority sequence</td>	
                                     <td>	</td>
                                     </tr>
                                     <tr ng-repeat="insured_phone in insured_phones">
                                     <input   name="insured_phone_id[]"  type="hidden"  class="span6 first_name myclass" ng-model="insured_phone.phone_id" value="{{insured_phone.phone_id}}">
                                     <td>
                                     <?php echo getCatlogValue('insured_phone_type[]', 'class="span10"  ng-model="insured_phone.phone_type"','',35,''); ?>
                                     </td>
                                     <td>
                                     <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
									 <?php
                                       $selected =  '1'; 	
                                        echo getCountryCodeDropDown($name='insured_code_type[]', $att='id="voice_ctcode_'.$i.'" style="" class="span11  drop " required ', 'code', $selected); 
									 ?>
                                     <?php //echo getCatlogValue('insured_code_type[]', 'class="span12"  ng-model="insured_phone.code_type"','',36,''); ?>
                                     <td> 
                                      <input  id="name"  name="insured_area_code[]" pattern="[0-9]+" maxlength="3" size="3" placeholder="Area"  type="text"  class="span10 first_name myclass" ng-model="insured_phone.area_code" > </td>
                                     <td>
                                     <input id=""  name="insured_prefix[]" type="text" pattern="[0-9]+" maxlength="3" size="3" placeholder="Prefix" class="span10  myclass" ng-model="insured_phone.prefix">
                                     </td>
                                     <td> 
                                     <input id=""   name="insured_suffix[]" type="text" pattern="[0-9]+" maxlength="4" size="4" placeholder="Suffix" class="span10  myclass" ng-model="insured_phone.suffix">
                                     </td>
                                     <td> 
                                     <input id=""   name="insured_extension[]" type="text" pattern="[0-9]+" maxlength="4" size="4" placeholder="Ext" class="span10  myclass" ng-model="insured_phone.extension">
                                     </td>
                                     <td>
                                     <?php echo getCatlogValue('insured_country[]', 'class="span8"  ng-model="insured_phone.pri_country"','',37,''); ?>
                                       
                                     </td>                                    
                                   
                                    <td>
                                     <a href="javscript:;" ng-click="addRowPhone('broker_phone')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                     <a href="javscript:;" ng-click="remove_row_phone($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                    </tr>
                                    </table>
                           </span>
                           <span class="span5">
                             <table id="table" class="tables_ins" <?php echo isset($print)?'style="width: 160%;"':''; ?>>                                  
                                     <tr>
									 <td id="label_insured_email_type"><span class="numbers"></span>Email Type</td>	
                                     <td id="label_insured_email"><span class="numbers"></span>Email address</td>	
                                     <td id="label_insured_priority_email"><span class="numbers"></span>Priority sequence</td>	
                                     <td style="width:5%"></td>
                                     </tr>
                                     <tr ng-repeat="insured_email in insured_emails">       
                                     <input   name="insured_email_id[]"  type="hidden"  class="span10 first_name myclass" ng-model="insured_email.email_id" value="{{insured_email.email_id}}">                              	
                                     <td >
                                     <?php echo getCatlogValue('insured_email_type[]', 'class="span12"  ng-model="insured_email.email_type"','',34,''); ?>
                                     </td>	 
                                     <td >
                                     <input id="agency" name="insured_email[]" type="text" class="span12 myclass" ng-model="insured_email.email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$"></td>	
                                     <td> 
                                      <?php echo getCatlogValue('insured_priority_email[]', 'class="span8"  ng-model="insured_email.priority_email"','',37,''); ?>
                                     </td>	
                                      <td >
                                           <a href="javscript:;" ng-click="addRowEmail('broker_email')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                           <a href="javscript:;" ng-click="remove_row_email($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                     </tr>
                                  </table>
                           </span>
                           </div>
                            
                            
                            
                            <div class="row-fluid">  
							
							   
                         </div>   
        <!--  <div class="fluid-row"> 
           <label><span class="numbers"> </span>Do you have any fax number?</label>
           <select name="in_garag_faxs" class="span1" ng-model="insured_info.in_garag_fax">
        	  <option selected="selected">No</option>
			 <option>Yes</option>
            
           </select>
           </div>-->
                        <div class="row-fluid" >
                              <table id="table" class="tables_inss">
                                   	
                                    <tr>		
									 <td style="width:15%" id="label_insured_fax_type"> <span class="numbers"></span> Fax Type</td>	
                                     <td style="width:18%" id="label_insured_code_type_fax"><span class="numbers"></span> Country Code</td>	
                                     <td style="width:15%" id="label_insured_area_code_fax"><span class="numbers"></span> Area code</td>	
                                     <td id="label_insured_prefix_fax"><span class="numbers"></span> Prefix</td>
                                     <td id="label_insured_suffix_fax"><span class="numbers"></span> Suffix</td>	
                                     <td style="width:20%" id="label_insured_country_fax"><span class="numbers"></span> Priority sequence</td>	
                                     <td>	</td>
                                     </tr>
                                     <tr ng-repeat="insured_fax in insured_faxs">
                                     <input   name="insured_fax_id[]"  type="hidden"  class="span12 first_name myclass" ng-model="insured_fax.fax_id" value="{{insured_fax.fax_id}}" >
                                     <td>
                                      <?php echo getCatlogValue('insured_fax_type[]', 'class="span11"  ng-model="insured_fax.fax_type"','',38,''); ?>
                                     </td>
                                     <td>
                                      <?php //echo getCatlogValue('insured_code_type_fax[]', 'class="span12"  ng-model="insured_fax.code_type_fax"','',36,''); ?>
                                     <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
									   <?php
                                       $selected =  '1'; //isset($voice_no[$i]->country_code) ? decrypt($voice_no[$i]->country_code) :
                                      // $highlighted = isset($vncc_highlighted) ? $vncc_highlighted : '';		
                                         getCountryCodeDropDown($name='insured_code_type_fax[]', $att='ng-model="insured_fax.code_type_fax" class="span11" ', 'code', $selected); //'.$highlighted.'
									 ?>
                                     <td> 
                                      <input   name="insured_area_code_fax[]"  type="text" pattern="[0-9]+" maxlength="3" size="3" placeholder="Area" class="span10 first_name myclass" ng-model="insured_fax.area_code_fax" > </td>
                                     <td>
                                       <input   name="insured_prefix_fax[]" type="text" pattern="[0-9]+" maxlength="3" size="3" placeholder="Prifix" class="span10  myclass" ng-model="insured_fax.prefix_fax">
                                     </td>
                                     <td> 
                                      <input   name="insured_suffix_fax[]" type="text" pattern="[0-9]+" maxlength="4" size="4" placeholder="Suffix" class="span10  myclass" ng-model="insured_fax.suffix_fax">
                                     </td>
                                     <td>
                                     <?php echo getCatlogValue('insured_country_fax[]', 'class="span8"  ng-model="insured_fax.country_fax"','',37,''); ?>
                                     </td>   
                                     <td>
                                        <a href="javscript:;" ng-click="addRowFax('broker_fax')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                        <a href="javscript:;" ng-click="remove_row_fax($index)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                      </td>
                                    </tr>
                                </table>
                             							

                            </div>
       <div class="row-fluid">
      	<label><span class="numbers"></span>All vehicles garage in the same address?</label>
        <select name="in_garag_in" class="span1" ng-model="insured_info.in_garag_in">
        	<option>Yes</option>
            <option selected="selected">No</option>
        </select>
      </div>
      <div class="row-fluid" ng-if="insured_info.in_garag_in=='No'">      	
          <div class="row-fluid">
     
              <label>Please fill in all the addresses where the vehicles are garaging:</label>
              <a href="javascript:;" ng-click="add_row('in_garags')"><img src="<?= base_url('images/add.png'); ?>"></a>
          </div>
          <div class="row-fluid" ng-repeat="in_garag_ad in rows.in_garags" ng-init="garagIndex=$index" >
            <div class="row-fluid">
                <div class="span1 span_5">
                <input type="hidden" name="in_ga_id[]" ng-model="in_garag_ad.in_ga_id" value="{{in_garag_ad.in_ga_id}}"/>
                    <label id="label_in_ga_sequence">Sequence</label>
                    <input type="text" name="in_ga_sequence[]" class="span12" value="{{garagIndex+1}}" readonly="readonly">
                </div>
                <div class="span1 span_4">               
                    <label >Contact Person</label>                   
                </div>
                <div class="span1 span_10">                
                    <label id="label_in_ga_fname">First Name</label>
                    <input type="text" name="in_ga_fname[]" class="span12" ng-model="in_garag_ad.in_ga_fname">
                </div>
                <div class="span1 span_10">               
                    <label id="label_in_ga_mname">Middle Name</label>
                    <input type="text" name="in_ga_mname[]" class="span12" ng-model="in_garag_ad.in_ga_mname">
                </div>
                <div class="span1 span_10">                
                    <label id="label_in_ga_lname">Last Name</label>
                    <input type="text" name="in_ga_lname[]" class="span12" ng-model="in_garag_ad.in_ga_lname">
                </div>
               <div class="span2">
                    <label id="label_in_ga_address">Street</label>
                    <input type="text" name="in_ga_address[]" class="span12" ng-model="in_garag_ad.in_ga_address">
                </div>
                <div class="span1">
                    <label id="label_in_ga_city">City</label>
                    <input type="text" name="in_ga_city[]" class="span12" id_ga="{{$index}}" ng-model="in_garag_ad.in_ga_city" id="in_ga_city_{{$index}}" ng-change="zip_code_ga({{$index}})">
                </div>
                <div class="span1">
                    <label id="label_in_ga_state">State</label>
                 
                     <?php  echo get_state_dropdown('in_ga_state[]','', $attr='class="span12" ng-model="in_garag_ad.in_ga_state" id="in_ga_state_{{$index}}" ng-change="zip_code_ga({{$index}})"') ?>
                </div>
                <div class="span2 span_10">
                    <label id="label_in_ga_county">County</label>
                    <span id="in_ga_county_span_{{$index}}">
                   
                    <?php // echo getCounty('in_ga_county[]', 'class="span12" ng-change="getZipCodes_ga({{$index}})" id="in_ga_county_{{$index}}"  ng-model="in_garag_ad.in_ga_county"',"in_garag_ad.in_ga_city","in_garag_ad.in_ga_state"); ?>  
                   <input type="text" name="in_ga_county[]" id="in_ga_county_{{$index}}" class="span12" ng-model="in_garag_ad.in_ga_county">
                    </span>
                </div>
                <div class="span1">
                    <label id="label_in_ga_zip">Zip</label>
                    <span id="in_ga_zip_span_{{$index}}">
                     <input type="text" name="in_ga_zip[]" id="in_ga_zip_{{$index}}" class="span12" ng-model="in_garag_ad.in_ga_zip">
                    </span>
                </div>
                <div class="span1">
                    <label id="label_in_ga_country">Country</label>
                     <?php echo getCountryDropDown1('in_ga_country[]', 'class="span12"  ng-model="in_garag_ad.in_ga_country"','',''); ?>
                  
                </div>
                <div class="span1">
                	<label class="span12 no_margin">&nbsp;</label>
                	<a href="javascript:;" ng-click="remove_row($index, 'in_garags')"><img src="<?= base_url('images/remove.png')?>"></a>
                </div>
            </div>
                <div class="row-fluid">
                           
                             <table id="table" class="tables_inss span12">                                  
                                    <tr>		
					<td style="width:10%!important">Phone Type</td>	
                                     <td style="width:10%!important" >Country Code</td>	
                                     <td style="width:6%!important" >Area code</td>	
                                     <td style="width:5%!important" >Prefix</td>
                                     <td style="width:5%!important" >Suffix</td>	
                                     <td style="width:5%!important" >Extension</td>	
                                     <td style="width:10%!important" >Priority sequence</td>	
                                     <td>	</td>
                                     </tr>
                                     <tr ng-repeat="phone in in_garag_ad.phones">
                                     <td>
                                     <input type="hidden" name="in_phone_id_{{garagIndex}}[]" ng-model="phone.phone_id" value="{{phone.phone_id}}"/>
                                     <?php echo getCatlogValue('ins_phone_type_{{garagIndex}}[]', 'class="span11"  ng-model="phone.phone_type"','',35,''); ?>
                                   
                                     </td>
                                     <td>
                                      <?php
                                       $selected =  '1'; 		
                                        echo  getCountryCodeDropDown($name='ins_code_type_{{garagIndex}}[]', $att='class="span11"  ng-model="phone.code_type" ', 'code', $selected); 
									 ?>
                                     <?php //echo getCatlogValue('ins_code_type[]', 'class="span12"  ng-model="phone.code_type"','',36,''); ?>
                                 
                                     <td> 
                                       <input   name="ins_area_code_{{garagIndex}}[]"  type="text" pattern="[0-9]+" maxlength="3" size="3" placeholder="Area" class="span11 first_name myclass" ng-model="phone.area_code" > </td>
                                     <td>
                                       <input    name="ins_prefix_{{garagIndex}}[]" type="text" pattern="[0-9]+" maxlength="3" size="3" placeholder="Prefix" class="span11 myclass" ng-model="phone.prefix">
                                     </td>
                                     <td> 
                                        <input id=""   name="ins_suffix_{{garagIndex}}[]" type="text" pattern="[0-9]+" maxlength="4" size="4" placeholder="Suffix" class="span11 myclass" ng-model="phone.suffix">
                                     </td>
                                     <td> 
                                        <input id=""   name="ins_extension_{{garagIndex}}[]" type="text" pattern="[0-9]+" maxlength="4" size="4" placeholder="Ext" class="span11 myclass" ng-model="phone.extension">
                                     </td>
                                     <td>
                                     <?php echo getCatlogValue('ins_country_{{garagIndex}}[]', 'class="span6"  ng-model="phone.pri_country"','',37,''); ?>
                                   
                                     </td>                                    
                                   
                                    <td>
                                        <a href="javscript:;" ng-click="addRowPhone('in_garags',garagIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                     <a href="javscript:;" ng-click="remove_row_phone($index,'in_garags',garagIndex)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                    </tr>
                                    </table>

                            </div>
                            <div class="row-fluid">  
							<table id="table" class="tables_inss">                                  
                                    <tr>		
									 <td style="width:15%!important">Email Type</td>	
                                     <td>Email address</td>	
                                     <td>Priority sequence</td>	
                                     <td></td>
                                     </tr>
                                     <tr ng-repeat="email in in_garag_ad.emails">
                                      <input type="hidden" name="in_email_id_{{garagIndex}}[]" ng-model="email.email_id" value="{{email.email_id}}"/>
                                      <td>
                                      <?php echo getCatlogValue('ins_email_type_{{garagIndex}}[]', 'class="span11"  ng-model="email.email_type"','',34,''); ?>
                                 
                                     </td>	
                                     <td >
                                     <input id="agency" name="ins_email_{{garagIndex}}[]" type="text" class="span11 myclass" ng-model="email.email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$"></td>	
                                     <td> 
                                     <?php echo getCatlogValue('ins_priority_email_{{garagIndex}}[]', 'class="span5"  ng-model="email.priority_email"','',37,''); ?>
                                  
                                     </td>	                                    
                                     <td>
                                           <a href="javscript:;" ng-click="addRowEmail('in_garags',garagIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                           <a href="javscript:;" ng-click="remove_row_email($index,'in_garags',garagIndex)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                    </td>
                                     </tr>
                                  </table>
							   
                         </div>   
                    <!--    <div class="fluid-row"> 
                         <label><span class="numbers"></span>Do you have any fax number?</label>
                         <select name="in_garag_fax[]" class="span1" ng-model="in_garag_ad.in_garag_fax" >
                            <option>Yes</option>
                           <option selected="selected">No</option>
                         </select>
                         </div>-->
                        <div class="row-fluid" >
                              <table id="table" class="tables_inss tables_ins">
                            
                                    <tr>		
									 <td style="width:10%!important">Fax Type</td>	
                                     <td style="width:12%!important">Country Code</td>	
                                     <td style="width:6%!important">Area code</td>	
                                     <td style="width:5%!important">Prefix</td>
                                     <td style="width:5%!important">Suffix</td>	
                                     <td style="width:15%!important">Priority sequence</td>	
                                     <td>	</td>
                                     </tr>
                                     <tr ng-repeat="fax in in_garag_ad.faxs">
                                     <input type="hidden" name="in_fax_id_{{garagIndex}}[]" ng-model="fax.fax_id" value="{{fax.fax_id}}" />
                                     <td>
                                      <?php echo getCatlogValue('ins_fax_type_{{garagIndex}}[]', 'class="span11"  ng-model="fax.fax_type"','',38,''); ?>
                                  
                                     </td>
                                     <td>
                                      <?php
                                       $selected =  '1'; 	
                                         echo getCountryCodeDropDown($name='ins_code_type_fax_{{garagIndex}}[]', $att='class="span11"  ng-model="fax.code_type_fax" ', 'code', $selected);
									 ?>
                                      <?php //echo getCatlogValue('ins_code_type_fax[]', 'class="span12"  ng-model="fax.code_type_fax"','',36,''); ?>
                                  
                                     <td> 
                                      <input   name="ins_area_code_fax_{{garagIndex}}[]"  type="text" pattern="[0-9]+" maxlength="3" size="3" placeholder="Area" class="span11 first_name myclass" ng-model="fax.area_code_fax" > </td>
                                     <td>
                                       <input  name="ins_prefix_fax_{{garagIndex}}[]" type="text" pattern="[0-9]+" maxlength="3" size="3" placeholder="Prefix" class="span11  myclass" ng-model="fax.prefix_fax">
                                    </td>
                                    <td> 
                                      <input  name="ins_suffix_fax_{{garagIndex}}[]" type="text" pattern="[0-9]+" maxlength="4" size="4" placeholder="Suffix" class="span11  myclass" ng-model="fax.suffix_fax">
                                    </td>
                                    <td>
                                     <?php echo getCatlogValue('ins_country_fax_{{garagIndex}}[]', 'class="span6"  ng-model="fax.country_fax"','',37,''); ?>
                                  
                                    </td>  
                                    <td>   <a href="javscript:;" ng-click="addRowFax('in_garags',garagIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                                           <a href="javscript:;" ng-click="remove_row_fax($index,'in_garags',garagIndex)" ><img src="<?= base_url('images/remove.png'); ?>"></a>
                                      </td>
                                    </tr>
                                </table>                         							

                            </div>
                           </div>
              <script>
		    setTimeout(function(){
			  $('input').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 });
				  $('select').each(function(){
				   if($(this).val()!=''){	 
					$(this).css({'border-color':'rgba(44, 197, 8, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82, 236, 105, 0.6)'});
				   }
				 }); 
				  $('input').blur(function() {	
					 if($(this).val()!=''){	
					  $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					  $(this).removeAttr('style');
					  }
				   });
				
				 $('select').blur(function() {	
					$(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
				   });
		 },200);
  
		  </script>     
                           
                           
                           
                      </div>
    <div class="row-fluid top20">
     <div class="row-fluid">
      <div class="heading_border">
    	<div class="heading_vehicles">        
          <div class="row-fluid ">B.1 Business Information</div>
       
        </div>
           <div class="pull-right icon_show_b">
            <span class="green" id="show-ins_b" title="Show" style="display:none" onclick="ins_section_b('show')"> <img src="<?= base_url()?>images/hide.png"></span>
            <span class="red" id="hide-ins_b" title="Hide" onclick="ins_section_b('hide')"> <img src="<?= base_url()?>/images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
         </div>
      </div>
     </div>
      <div class="row-fluid top20" id="ins_div_b">
         <div class="span3 span_11  lightblue top45 " >
          <label id="label_business_years" ><span class="numbers"></span>Years in Business </label>
          <!--change by sulbha -->
          <select id="business_years" name="business_years" class="myclass span12" ng-model="insured_info.business_years">
            <option  value="New venture">New venture</option>
            <option  value="1 year" >1 year</option>
            <option   value="2 years">2 years</option>
            <option  value="3 years">3 years</option>
            <option  value="4+ years" >4+ years</option>
          </select>
        </div>
        <div class="span2 span_12 top45" name="nature_of_business" >
          <label id="label_nature_of_business"><span class="numbers"></span>Nature of Business</label>
           <?php  echo $insured_infos_1;
	        if(isset($insured_infos_1)){ ?> <style> div#nature_of_business_chzn { width: 100% !important;} </style> <?php }
	           //echo get_catlog_dropdown_company('nature_of_business[]','nature_of_business','class="myclass span12 chzn-select" multiple ng-model="insured_info.nature_of_business"', '', '', '');;
	       ?> 
     <!--   <span style="color:white">    </span>-->
         <div class="span12  nature_of_business_otr nob1" ng-if="insured_info.nature_of_business.indexOf('other') >=0">
            <label class="row-fluid" id="label_nature_of_business_other">Specify Other</label>
            <input type="text" name="nature_of_business_other" class="span12 " ng-model="insured_info.nature_of_business_other">
        </div>
        </div>
   
 
        <div class="span2 span_25  lightblue nob1" >
          <label id="label_commodities_haulted"><span class="numbers"></span>Commodities Hauled </label><label >If you don't see your commodity please "Other" on the following field type</label>
          <?php //echo $commodities_haulted;		  
		        $ins_com= !empty($insured_info)?$insured_info[0]->commodities_haulted:'';
		        echo commodities_haulteds($ins_com);
		     ?>          
        <span style="color:white">   </span>
          
         <div class="span12 commodities_haulted_otr" ng-if="insured_info.commodities_haulted.indexOf('other') >=0">
          <label class="row-fluid" id="label_commodities_haulted_other">Specify Other</label>
          <input type="text" id="commodities_haulted_other" ng-model="insured_info.commodities_haulted_other" name="commodities_haulted_other" value="" class="span9" >
          </div>
        </div>       
      
          <div class="span_15 top25">
            <label id="label_operation"><span class="numbers"></span>Are all vehicles going to do the same operation?</label>
            <select  name="operation" id="operation" class="myclass span8" ng-model="insured_info.operation" ng-change="operation_info(insured_info.operation);">
            <option  value="">Select</option>
            <option >Yes</option>
            <option >No</option>
          </select>
        </div> 
        
        <div class="span2 span_8 top20">
         <label class="row-fluid"><span class="numbers"></span>Policy Period</label>
          <label id="label_terms_name"><span class="">31.A </span>Terms</label>
          <select name="terms_name" class="span12" id="terms_name" onchange="terms_names(this.value)" ng-model="insured_info.terms_name"> 
          <option value=""> Select</option>
          <option value="3"> 3 months</option>
          <option value="6"> 6 months</option>  
          <option value="9"> 9 months</option>
          <option value="12"> Year</option>
          <option value="Other"> Other</option>     
          </select>
        </div>
        
         <div class="span2 span_7dot6 top45" id="other_terms" style="display:none">
          <label >Other</label>
          <input type="text" name="terms_other" id="terms_other" class="span12" >          
        </div>
         <div class="span2 span_7dot6 top45" id="from">
          <label id="label_terms_from" ><span class="">31.B </span>From</label>
          <input type="text" name="terms_from"  onchange="term_from(this.value)" ng-model="insured_info.terms_from" placeholder="MM/DD/YYYY" id="terms_from" class="span12" />
        </div>
        <div class="span2 span_7dot6 top45" id="to">
          <label id="label_terms_to"><span class="">31.C </span>To</label>
          <input type="text" name="terms_to"  id="terms_to" class="span12" ng-model="insured_info.terms_to" placeholder="MM/DD/YYYY" />
        </div>  
      </div>
      <div class="row-fluid top20">
    <!--  <label class="row-fluid"><span class="numbers"></span>Policy Period</label>
        <div class="span2 span_7dot6">
          <label><span class="numbers_child"></span>Terms</label>
          <select name="terms_name" class="span12" id="terms_name" onchange="terms_names(this.value)" ng-model="insured_info.terms_name"> 
          <option value=""> Select</option>
          <option value="3"> 3 months</option>
          <option value="6"> 6 months</option>  
          <option value="9"> 9 months</option>
          <option value="12"> Year</option>
          <option value="Other"> Other</option>     
          </select>
        </div>
        
         <div class="span2 span_7dot6" id="other_terms" style="display:none">
          <label >Other</label>
          <input type="text" name="terms_other" id="terms_other" class="span12" >          
        </div>
         <div class="span2 span_7dot6" id="from">
          <label ><span class="numbers_child"></span>From</label>
          <input type="text" name="terms_from"  onchange="term_from(this.value)" ng-model="insured_info.terms_from" placeholder="MM/DD/YYYY" id="terms_from" class="span12" />
        </div>
        <div class="span2 span_7dot6" id="to">
          <label><span class="numbers_child"></span>To</label>
          <input type="text" name="terms_to"  id="terms_to" class="span12" ng-model="insured_info.terms_to" placeholder="MM/DD/YYYY" />
        </div>  -->                           
       <!--   <div class="span2 span_7dot6" id="terms" >        
         <div class="span2 span_7dot6" id="from">
          <label ><span class="numbers_child"></span>From</label>
          <input type="text" name="terms_from"  onchange="term_from(this.value)" ng-model="insured_info.terms_from" placeholder="MM/DD/YYYY" id="terms_from_0" class="span12" />
        </div>
        <div class="span2 span_7dot6" id="to">
          <label><span class="numbers_child"></span>To</label>
          <input type="text" name="terms_to"  id="terms_to_0" class="span12" ng-model="insured_info.terms_to" placeholder="MM/DD/YYYY" />
        </div> -->
       </div>  
      </div>
     </div>

    
    <!-- Insured filling Section Ashvin Patel 26/April/2015-->
  
 <div class="row-fluid top20">
     
    <div class="heading_border">
    	<div class="heading_vehicles"><div class="row-fluid ">B.2 Filing Section</div></div>
         <div class="pull-right icon_show_b">
            <span class="green" id="show-ins_f" title="Show" style="display:none" onclick="ins_section_f('show')"> <img src="<?= base_url()?>images/hide.png"></span>
            <span class="red" id="hide-ins_f" title="Hide" onclick="ins_section_f('hide')"> <img src="<?= base_url()?>/images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
         </div>
        <div class="span3 pull-right heading_vehicles">
          <label id="label_filings_need">Do you need Filing / Filings :</label>
          <select id="filings_need1" name="filings_need" ng-model="insured_info.filings_need" onChange="radius_ope()" class="span3 no_margin">
            <option>Yes</option>
            <option>No</option>
          </select>
        </div>
      
    </div>
  <div class="well" id="ins_div_f">
    <div class="row-fluid" ng-if="insured_info.filings_need=='Yes'">	     
      <div class="span12 lightblue" id="number_of_filings_div">
        <label><a href="javascript:;" ng-click="add_row('filing')"><img src="<?= base_url('images/add.png')?>"></a></label>
      
      </div>
      <div class="row-fluid" id="filings_need_update" style="display:block;">  
        <div class="span12 lightblue">
          <table id="filing_table" class="table table-bordered table-striped table-hover filing_table" ng-repeat="filing in rows.filing">
            <thead>
              <tr>
                <th style="width: 31%;" id="label_filing_type">Filing Type:</th>
                <th style="width: 31%;" id="label_filing_type_other">Filing Num</th>
                <th id="label_filing_remark">Remarks</th>
              </tr>
            </thead>
            <tbody id="add_filing_row" >
              <tr > 
              <input type="hidden" name="filing_id[]" class="span12" ng-model="filing.filing_id" value="{{filing.filing_id}}">
                <td> 
		         <?php   echo filing_type_show('filing_type[]','',0,'ng-model="filing.filing_type" onChange="radius_ope()"'); ?>
                 <input  id="filing_type_other" type="text" name="filing_type_other[]" ng-if="filing.filing_type.indexOf('Other') >= 0" class="span12 filing_numbers" ng-model="filing.filing_other" value=""/> 	
               
                </td>
                <td>
                    <input type="text" maxlength="20" class="span12" name="filing_numbers[]" ng-model="filing.filing_numbers">
                    
                </td>
                 <td>
                    <input type="text" maxlength="20" class="span9" name="filing_remark[]" ng-model="filing.filing_remark">
                    <a href="javascript:;" ng-click="remove_row($index, 'filing')"><img src="<?= base_url('images/remove.png')?>"></a>
                </td>
              </tr>					
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="row-fluid" ng-if="insured_info.filings_need=='No'">
    	<div class="row-fluid">
        	<label>32 Under who`s authority you are driving?</label>
        </div>
        <div class="row-fluid" ng-repeat="filing_no in rows.filing_nos">
        <input type="hidden" name="in_fi_id[]" class="span12" ng-model="filing_no.in_fi_id" value="{{filing_no.in_fi_id}}">
        	<div class="span2 span_10">
              <label id="label_fil_type">32.1 Filing Type</label>
               <?php   echo filing_type_show('fil_type[]','',0,'ng-model="filing_no.fil_type"'); ?>
                 <input  id="fil_type_other" type="text" name="fil_type_other[]" ng-if="filing_no.fil_type.indexOf('Other') >= 0" class="span12 filing_numbers" ng-model="filing.fil_type_other" value=""/> 	
              </div> 
            <div class="span2 span_10">
            	<label id="label_in_fi_filing_number">32.2 Filing Number</label>
                <input type="text" name="in_fi_filing_number[]" class="span12" ng-model="filing_no.in_fi_filing_number">
            </div>
            <div class="span3 span_10">
            	<label id="label_in_fi_dba">32.3 DBA</label>
                <input type="text" name="in_fi_dba[]" class="span12" ng-model="filing_no.in_fi_dba">
            </div>
            <div class="span2">
            	<label id="label_in_fi_address">32.4 Street</label>
                <input type="text" name="in_fi_address[]" class="span12" ng-model="filing_no.in_fi_address">
            </div>
            <div class="span1 span_10">
            	<label id="label_in_fi_city">32.5 City</label>
                <input type="text" name="in_fi_city[]" class="span12" ng-model="filing_no.in_fi_city">
            </div>
            <div class="span1">
            	<label id="label_in_fi_state">32.6 State</label>
                 <?php  echo get_state_dropdown('in_fi_state[]','', $attr='class="span12" ng-model="filing_no.in_fi_state"') ?>
                
            </div>
            <div class="span1 span_7dot6">
            	<label id="label_in_fi_zip">32.7 Zip</label>
                <input type="text" name="in_fi_zip[]" class="span12" ng-model="filing_no.in_fi_zip">
            </div>
             <div class="span1 span_7dot6">
            	<label id="label_in_fi_in_fi_remark">32.8 Remarks</label>
                <input type="text" name="in_fi_remark[]" class="span12" ng-model="filing_no.in_fi_remark">
            </div>
            <div class="span1">
            	<label class="span12 no_margin">&nbsp;</label>
            	<a href="javscript:;" ng-click="add_row('filing_nos')" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                <a href="javscript:;" ng-click="remove_row($index, 'filing_nos')"><img src="<?= base_url('images/remove.png'); ?>"></a>
            </div>
        </div>
    </div>
    </div>
  </div>
   
<!-- Insured filling Section End Ashvin Patel 26/April/2015-->
 <!-- Insured Application Section Ashvin Patel 26/April/2015-->
  
 <div class="row-fluid top20">
     
    <div class="heading_border">
    	<div class="heading_vehicles"><div class="row-fluid ">B.3 Application Section</div></div>
         <div class="pull-right icon_show_b">
            <span class="green" id="show-ins_bu" title="Show" style="display:none" onclick="ins_section_bu('show')"> <img src="<?= base_url()?>images/hide.png"></span>
            <span class="red" id="hide-ins_bu" title="Hide" onclick="ins_section_bu('hide')"> <img src="<?= base_url()?>/images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
         </div>
    </div>
    <div class="well" id="ins_div_bu">
    <input type="hidden" value="{{insured_b.b_id}}" name="b_id"/>
      <div class="row-fluid top20">
          <label><span class="numbers"></span>Has the applicant, a business partner or any associate of the applicant ever filed for bankruptcy under any name in the past 10 years?</label>
          <select id="filings_need1" name="applicant_ever" ng-model="insured_b.applicant_ever" class="width_option no_margin">
            <option value="">Select</option>
            <option>Yes</option>
            <option>No</option>
          </select>
        </div>
        
     <div id="explain_business1" class="row-fluid top20" ng-if="insured_b.applicant_ever=='Yes'">
       <span class="span2 span_8">
          <label class="control-label">Year</label>
                 <select class="span12  myclass" name="exp_year" id="exp_year" ng-model="insured_b.exp_year">
              <?php 
              $year=$vehicle->vehicle_year;
              $current_year = date("Y");
			   echo "<option value=''>Select</option>";
              for($i = 1985; $i <= $current_year + 1; $i++){
				 
                echo "<option value='$i'>$i</option>";
              } 
              ?>
            </select>
                </span>
    		   <span class="span2 span_8" style="float:left;">
            <label class="control-label pull-left">Case &nbsp;</label>
            <input class="textinput span11" type="text" placeholder=""ng-model="insured_b.exp_case" name="exp_case">
          </span>
		   <span class="span1 span_8">
            <label class="control-label pull-left">Number &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" ng-model="insured_b.exp_number" name="exp_number">
          </span>
		  
		    <span class="span1">
            <label class="control-label pull-left">County &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" ng-model="insured_b.exp_county" name="exp_county">
          </span>
          <span class="span1">
            <label class="control-label pull-left ">State &nbsp;</label>
			<?php
									$attr = "class='span12 ui-state-valid exp_state' id='exp_state' ng-model='insured_b.exp_state'";
									//$broker_state =  isset($app_loss[0]->exp_state) ? $app_loss[0]->exp_state : ''; 
									
									?>
									<?php echo get_state_dropdown('exp_state', '', $attr); ?>
           
          </span>
        
            
            <textarea class="span10" placeholder="If yes, please explain"  name="explain_business" id="explain_business" ng-model='insured_b.explain_business' required>{{insured_b.explain_business}}</textarea>
        
	  </div>
      
     <div class="row-fluid top20">
          <label><span class="numbers"></span>Has the applicant ever been a partner, member or an associate of any other transportation firm or related field in the last five years?</label>
          <select id="" name="applicant_transportation" ng-model="insured_b.applicant_transportation" class="span_7dot6 no_margin">
            <option value="">Select</option>
            <option>Yes</option>
            <option>No</option>
          </select>
        </div>
     <div id="explain_business1" class="row-fluid top20" ng-if="insured_b.applicant_transportation=='Yes'">                 
            <textarea class="span10" placeholder="If yes, please explain"  name="explain_conducted" id="explain_conducted" ng-model='insured_b.explain_conducted' required>{{insured_b.explain_conducted}}</textarea>
        
	  </div>
      
    <div class="row-fluid top20">
          <label><span class="numbers"></span>Has the applicant, a business partner or any associate of the applicant conducted business under any other name in the past five years?</label>
          <select id="" name="applicant_conducted" ng-model="insured_b.applicant_conducted" class="span_7dot6 no_margin">
            <option value="">Select</option>
            <option>Yes</option>
            <option>No</option>
          </select>
     </div>
      <div id="explain_business1" class="row-fluid top20" ng-if="insured_b.applicant_conducted=='Yes'">                 
      <div class="page-header">
           <h1><!--GHI-8 --> <small>Previous / Subsidiary name supplement form</small>
           </h1>
      </div>
    <div class="well">
      
   <input type="hidden"  name="app_edit_id" value="{{insured_gh8.app_edit_id}}"/> 
        <div class="row-fluid">
          <span class="span4">
            <div class="row-fluid">
              <span class="span12">
                <label class="control-label row-fluid"> Applicant's Name</label>
                <input class="textinput span4" type="text" name="applicant_fname" ng-model="insured_gh8.applicant_fname"  placeholder="First name">
                <input class="textinput span4" type="text" placeholder="Middle name" ng-model="insured_gh8.applicant_mname" name="applicant_mname">
                <input class="textinput span4" type="text" placeholder="Last name" ng-model="insured_gh8.applicant_lname" name="applicant_lname">
              </span>
            </div>
          </span>
          <span class="span2">
            <label class="control-label">DBA(if applicable)</label>
            <input class="textinput span12" type="text" name="applicant_dba" ng-model="insured_gh8.applicant_dba" placeholder="DBA">
          </span>
		  
		   <span class="span3">
            <label class="control-label pull-left">Address &nbsp;</label>
            <input class="textinput span11" type="text" placeholder="" ng-model="insured_gh8.applicant_address" name="applicant_address">
          </span>
		   <span class="span1">
            <label class="control-label pull-left"> City &nbsp;</label>
            <input class="textinput span12" type="text" placeholder="" ng-model="insured_gh8.applicant_city" name="applicant_city">
          </span>
		  
		    <span class="span1">
            <label class="control-label pull-left"> County</label>
            <input class="textinput span12" type="text" placeholder="" ng-model="insured_gh8.applicant_country"  name="applicant_country">
          </span>
          <span class="span1">
            <label class="control-label pull-left "> State </label>
			<?php
									$attr = "class='span12 ui-state-valid' ng-model='insured_gh8.applicant_state'";
									//$broker_state =  isset($app_ghi8[0]->applicant_state) ? $app_ghi8[0]->applicant_state : ''; 
									
									?>
									<?php echo get_state_dropdown('applicant_state', '', $attr); ?>
           
          </span>
        </div>
       
        <div class="row-fluid">
         
        
          <span class="span2">
            <label class="control-label pull-left span12">Zip &nbsp;</label>
			
            <input class="textinput span5" maxlength="5" type="text" style="margin-top: -10px;" pattern="[0-9]+"  placeholder="" name="applicant_zip1" pattern="[0-9]+" maxlength="5" ng-model="insured_gh8.applicant_zip1">
			<input class="textinput span5" maxlength="4" type="text" style="margin-top: -10px;" pattern="[0-9]+"  placeholder="" name="applicant_zip2" pattern="[0-9]+" maxlength="5" ng-model="insured_gh8.applicant_zip2">
          
		   </span>
		   
		          <span class="span3" >
            <label class="control-label pull-left span12" > Date operation started &nbsp;</label>
            <input class="textinput pull-left input-small datepick" style="margin-top: -10px;"type="text"  id="date_from" placeholder="mm/dd/yyyy" name="applicant_from" ng-model="insured_gh8.applicant_from">
          
        
            <label class="control-label pull-left">&nbsp; &nbsp;To &nbsp;</label>
            <input class="textinput input-small datepick" type="text" style="margin-top: -12px;float: right;" id="date_to"  placeholder="mm/dd/yyyy" name="applicant_to" ng-model="insured_gh8.applicant_to">
       
         
		  </span>
		  
		    <span class="span2">
            <label class="control-label pull-left"> CA# &nbsp;</label>
            <input class="textinput span12" type="text" placeholder=""  name="applicant_ca" ng-model="insured_gh8.applicant_ca">
          </span>
          <span class="span2">
            <label class="control-label pull-left"> MC# &nbsp;</label>
            <input class="textinput span12" type="text" placeholder=""  name="applicant_ma" ng-model="insured_gh8.applicant_ma">
          </span>
          <span class="span2">
            <label class="control-label pull-left"> DOT# &nbsp;</label>
            <input class="textinput span12" type="text" placeholder=""  name="applicant_dot" ng-model="insured_gh8.applicant_dot">
          </span>
        </div>
   
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">Is this entity still in operation? &nbsp;</label>
            <select name="applicant_entity" class="span1" id="applicant_entity" ng-model="insured_gh8.applicant_entity">            
                <option value="Yes" >Yes</option>
                <option value="No" >No</option>
                 
            </select>
      
          </span>
        </div>
        <div class="row-fluid"  ng-if="insured_gh8.applicant_entity=='Yes'">
          <span class="span12">
            <label class="control-label"> If yes, please give full details and role of who controls it.</label>
            <textarea class="span12" placeholder="" name="applicant_role">{{insured_gh8.applicant_role}}</textarea>
          </span>
        </div>
        <div class="row-fluid"  >
          <span class="span12" ng-if="insured_gh8.applicant_entity=='No'">
            <label class="control-label">If no, please give detail of termination and explain reason for termination.</label>
            <textarea class="span12" placeholder=""  name="applicant_explain">{{insured_gh8.applicant_explain}}</textarea>
          </span>
        </div>       
      </div>
	 </div>
      
      <div class="row-fluid top20">
      <input type="hidden" value="{{insured_t.t_id}}" name="t_id"/>
       <span class="span6">
          <label><span class="numbers"></span>Does the applicant own the cargo transported?</label>
          <select id="" name="transported_name" ng-model="insured_t.transported_name" class="span2 no_margin">
            <option value="">Select</option>
            <option>Yes</option>
            <option>No</option>
          </select>
        </span>
        <span class="span6">
           <label class="control-label pull-left"><span class="numbers"></span> Does the applicant transport passengers? &nbsp; &nbsp;</label>
           <select id="" name="passengers_name" ng-model="insured_t.passengers_name" class="span2 no_margin">
            <option value="">Select</option>
            <option>Yes</option>
            <option>No</option>
          </select>
         </span>
     </div>
      <div id="text_no" ng-if="insured_t.transported_name=='Yes'" >   
             <div class="row-fluid top20">  
								<div class="span3 lightblue">
                                    <label class="row-fluid"> Name </label>
                                    <input type="text" maxlength="255"  class="span4 first_name ui-state-valid " placeholder="First Name" name="insured_f_name" id="" ng-model="insured_t.insured_f_name"/>
									<input type="text"  class="span4 middle_name" name="insured_m_name" placeholder="Middle Name" id="" ng-model="insured_t.insured_m_name"/>
									<input type="text"  class="span4 last_name " placeholder="Last Name" name="insured_l_name" id="" ng-model="insured_t.insured_l_name"/>
                             		</div>
                                    
							   <div class="span2 lightblue">
                                    <label>DBA(if applicable)</label>
                                   <input id="" name="insured_d" type="text" class="span12 " maxlength="255" ng-model="insured_t.insured_d"/>
                                </div>
							         
                                <div class="span2 lightblue">
                                  <label> Address</label> 
									                                  
								  <input class="textinput span12" type="text" placeholder="" name="insuerd_a" ng-model="insured_t.insuerd_a">
                                   
                                </div>		
                                	
                       <div class="span1 lightblue">
                                    <label > City</label>
                                    <input id="garaging_city" name="insuerd_c" type="text" class="span12" ng-model="insured_t.insuerd_c">
                                </div>	
		               <div class="span1 lightblue">
                                    <label style=""> Country</label>
                            	<?php 
								 
								    	$app_country1 = isset($insured_t[0]->country_app1) ? $insured_t[0]->country_app1 : 'AD';
									    $app_state = isset($insured_t[0]->state_app1) ? $insured_t[0]->state_app1 : '';
									?>
									
                                  <?php echo  getCountryDropDown1($name='country_app1', $att='style="width:100%;" id="country_app2" onChange="getState(this.value, &quot;state_app2&quot;, &quot;state_app1&quot; )"',$app_state, $app_country1);?>

                                </div>	
                              <div class="span2 lightblue" >
                                    <label style="">State</label>
                                    <div id="state_app2" style="width: 45%;">
                                  
                                	<?php echo getStateDropDown($app_country1, $name='state_app1', $att='id="state_app2"', '', $app_state); ?>
                                  </div>

                                </div>								

                            </div>	

                            <div class="row-fluid"> 							
								 <div class="span2 lightblue">
                                    <label class="row-fluid">Zip</label>
                                    
                                     <input id="zip1" name="insured_z1" type="text" class="span6" maxlength="5" pattern="[0-9]+" ng-model="insured_t.insured_z1">
									 <input id="zip2" name="insured_z2" type="text" class="span6" maxlength="4" pattern="[0-9]+" ng-model="insured_t.insured_z2">
                                        <!--<input id="zip2" name="insured_zip[]" type="text" class="span6 zip2" value="">-->
                                   
                                </div>
								
			 <div class="span3 lightblue">
                <div class="row-fluid">
                     <span class="span12">
                           <label class="control-label pull-left">Phone</label>
                         </span>
                </div>
				
                <div class="row-fluid">
            				         <div class="span12">
										<input id="insured_telephone1" name="insured_tel1" type="text" class="span3" maxlength="3"  pattern="[0-9]+"  ng-model="insured_t.insured_tel1"  >
                                        <input id="insured_telephone2" name="insured_tel2" type="text" class="span3"   pattern="[0-9]+"  maxlength="3" ng-model="insured_t.insured_tel2" >
                                        <input id="insured_telephone3" name="insured_tel3" type="text" class="span3"   pattern="[0-9]+"  maxlength="4" ng-model="insured_t.insured_tel3"  >
									 <input id="insured_tel_ext" name="insured_tel_ext1" placeholder="Ext" type="text" pattern="[0-9]+"  class="span3" maxlength="5" pattern="[0-9]+" ng-model="insured_t.insured_tel4"  >
                                    
                              </div>
                       </div>									
                  </div>
              <div class="span3 lightblue">
                 <div class="row-fluid">
                  <span class="span12">
                    <label class="control-label left_pull">Email</label>
                 </span>
                </div>				 
                  <input class="textinput span9" type="text" placeholder="" id="email" name="insured_em" maxlength="100" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30" ng-model="insured_t.insured_em">
				</div>
							
			   </div>      
                 <textarea class="span10 span12-1" id="" placeholder="If no, who owns it" name="transported_desc" required="required">{{insured_t.transported_desc}}</textarea>
            </div>
     <div class="row-fluid top20">        
      <span class="span6">
              <label class="control-label pull-left span5" ><span class="numbers"></span> Does the applicant haul for</label>
             <?php $selected=!empty($insured_t)?explode(',',$insured_t[0]->trucking_comp_name):'';?>
              <select name="trucking_comp_name[]" class="pull-left chzn-select span4 "   chosen multiple="multiple"    id="trucking_comp_id" >  
                 <option <?= in_array('Trucking Company',$selected)?'selected':''?>>Trucking Company</option>
                 <option <?= in_array('Shippers',$selected)?'selected':''?>>Shippers</option>
                 <option <?= in_array('Other',$selected)?'selected':''?>>Other</option>
               </select>
              <input class="textinput span3"  type="text" ng-if="insured_t.trucking_comp_name.indexOf('Other') >= 0" placeholder="Please specify" id="trucking_specify" name="trucking_comp_other" ng-model="insured_t.trucking_comp_other">
            </span>
            <label class="control-label pull-left "> &nbsp;<span class="numbers"></span> Does the applicant need any certificate(s)? &nbsp;</label>
              <select class="pull-left pull-left-7 width_option" name="certificate_name"  id="certificate_id_val" ng-model="insured_t.certificate_name" required>               
                <option value="" >Select</option>
                <option value="No">No</option>
                <option value="Yes">Yes</option>
              </select>
       </div>
      <div id="certificate_app_id" ng-if="insured_t.certificate_name=='Yes'">
		  <div class="row-fluid">
            <span class="span6">
              <label class="control-label pull-left "> How many certificates?&nbsp;</label>
              <input class="span_7dot6" type="text" placeholder="" name="cert_num"  pattern="[0-9]+" ng-model="insured_t.cert_num" maxlength="2" ng-change="add_row_i('certificates')">
            </span>
          </div>	
		  <div  ng-if="insured_t.cert_num > 0">		 	
           <div class="top20" ng-repeat="certificate in rows.certificates" ng-init="certIndex=$index">
              <div class="well"> 
              <input type="hidden" name="cert_id[]" ng-model="certificate.cert_id" value="{{certificate.cert_id}}" />
			<div class="row-fluid">
            <span class="span2 span_11">
              <label class="control-label pull-left "> Company  &nbsp;</label>
              <input class="textinput span12"   type="text" placeholder="" ng-model="certificate.cert_comp" name="cert_comp[]">
            </span>
            <span class="span1">
              <label class="control-label "> Attention &nbsp;</label>
              <input class="textinput span12" type="text"  placeholder="" ng-model="certificate.cert_attent" name="cert_attent[]">
            </span>
             <span class="span2">
              <label class="control-label "> Address</label>
              <input class="textinput span12" type="text" placeholder="" ng-model="certificate.cert_add" name="cert_add[]">
            </span>
            <span class="span1">
              <label class="control-label row-fluid"> City</label>
              <input class="textinput span12"  type="text" placeholder="" ng-model="certificate.cert_city" name="cert_city[]">
            </span>
              <span class="span1"  >
              <label class="control-label row-fluid"> country</label>
              <!--<input class="textinput span8" type="text" placeholder="" value="<?php echo $value->cert_zip;?>"  style="margin-left: -13px;"name="cert_zip[]" >-->
              <?PHP  //$state_country=$value->cert_zip;?>
              <?php               
				echo getCountryDropDown1($name='cert_zip[]', $att='class="span12" id="cert_zip_{{certIndex}}"ng-model="certificate.cert_zip"  ng-change="getState(certificate.cert_zip, &quot;state_show_{{certIndex}}&quot;, &quot;state_cert[]&quot; )"', '', '');
				 ?>
            </span>
            <span class="span1 " >
              <label class="control-label span12"> State</label>
            
              <?php 
		       echo getStateDropDown('AD', $name='state_cert[]', $att='class="span12" style="margin-top:-10px;" ng-model="certificate.state_show" id="state_show_{{certIndex}}"', '', $app_state); 
			?>
			</span>
			 <span class="span2 span_11">
              <label class="control-label span12">Email&nbsp;</label>
              <input class="textinput span12" type="text" style="margin-top:-10px;" placeholder="" value="" name="cert_email[]" ng-model="certificate.cert_email" id="email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30">
            </span>   
			 <span class="span2 span_20" >
              <label class="control-label span12">Phone</label>
             
              <input class="textinput span3 top-10" type="text" placeholder="Area" name="cert_telephone1[]"   id="insured_telephone1" maxlength="3"  pattern="[0-9]+" ng-model="certificate.cert_telephone1">
              <input class="textinput span3 top-10" type="text" placeholder="Prefix" name="cert_telephone2[]"  id="insured_telephone2" maxlength="3"  pattern="[0-9]+" ng-model="certificate.cert_telephone2">
              <input class="textinput span3 top-10" type="text" placeholder="Sub" name="cert_telephone3[]"  id="insured_telephone3" maxlength="4"  pattern="[0-9]+" ng-model="certificate.cert_telephone3">
              <input class="textinput span3 top-10" type="text" placeholder="Ext" pattern="[0-9]+"  pattern="[0-9]+" name="cert_telephone_ext[]" maxlength="5" ng-model="certificate.cert_telephone_ext">
            </span>
            <span class="span2 span_15" style="">
              <label class="control-label span12"> Fax</label>             
              <input class="textinput span4 top-10" type="text" pattern="[0-9]+" placeholder="Area" name="cert_fax1[]" maxlength="3" ng-model="certificate.cert_fax1">
              <input class="textinput span4 top-10" type="text" pattern="[0-9]+" placeholder="Prefix" name="cert_fax2[]" maxlength="3" ng-model="certificate.cert_fax2">
              <input class="textinput span4 top-10" type="text" pattern="[0-9]+" placeholder="Sub" name="cert_fax3[]" maxlength="5" ng-model="certificate.cert_fax3">
            </span>
          </div>
		  <div class="row-fluid">          
           
          </div>
		  <div class="row-fluid">
           
            <span class="span6">
              <label class="control-label span5">Any Additional insured(s)?</label>
              <select class="pull-left pull-left-5 width_option" name="Additional_ins[]" ng-model="certificate.Additional_ins" style="float:left; margin:0px;" required>
                 <option value="" >Select</option>                   
                 <option value="No"  >No</option>
                 <option value="Yes">Yes</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label"></label>
            </span>
            </div>
           </div>
          </div>
      
		 </div> 
        </div> 
       <input type="hidden" value="{{insured_add.app_add_ins_id}}" name="app_add_ins_id"/>
       <div class="row-fluid mt_15">
            <span class="span6">
              <label class="control-label pull-left "><span class="numbers"></span> Is the scheduled vehicle(s) driven by the owner(s)?&nbsp;&nbsp;</label>
              <select class="pull-left pull-left-2 width_option" name="owner_vehicle" ng-model="insured_add.owner_vehicle" style="float:left;" required>
                <option value="" >Select</option>
                <option value="No">No</option>
                <option value="Yes">Yes</option>
              </select>   
      </span>

       </div>
                      
   <div class="container-fluid" ng-if="insured_add.owner_vehicle=='No'">
      <div class="page-header">
        <h1><!--GHI-6 --><small>Driver / Owner exclusion supplement form</small>
        </h1>
      </div>
      <div class="well">
        <label class="control-label control-label-1">Named Insured / DBA </label>       
        <input class="textinput span11 " type="text" name="supplement_dba" id="" value="{{insured_add.supplement_dba}}"ng-mogel="insured_add.supplement_dba"  placeholder="">
      </div>
      <p>
        <br>It is hereby understood and agreed that all coverages and our obligation to defend under this policy shall not apply nor accrue to the benefit of any INSURED or any third party claimant while any VEHICLE or MOBILE EQUIPMENT described in the policy or
        any other VEHICLE or MOBILE EQUIPMENT, to which the terms of the policy are extended, is being driven, used or operated by any person designated below.</p>
      <p>
        <br>The driver exclusion shall be binding upon every INSURED to whom such policy or endorsements provisions apply while such policy is in force and shall continue to be binding with respect to any continuation, renewal or replacement of such policy by the
        Named Insured or with respect to any reinstatement of such policy within 30 days of any lapse thereof. This DRIVER EXCLUSION provision shall conform to State statutes and laws.</p>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">How many additional persons?</label>
        </span>
        <span class="span1">
          <input class="textinput span12 supplement_val" type="text" name="supplement_val" id="supplement_val" ng-model="insured_add.supplement_val" ng-change="add_row_i('sups')" placeholder="" pattern="[0-9]+"  maxlength="2">
        </span>
        <span class="span7">        
        </span>
      </div>
 <div ng-repeat="sup in rows.sups" ng-init="supIndex=$index"> 
  <div class="well top20" > 
   <div class="add_addition_row">	
	<input type="hidden" value="18" name="supplement_parent_id[]"/> 
      <input type="hidden" value="{{sup.supplement_id}}" name="supplement_id[]"/> 
     
        		<div class="row-fluid">      
                     <span class="span12">       
                         <button class="btn pull-right" style="margin-bottom:5px;" onclick="persons_delete2(<?php echo $o; ?>);">Delete</button>		
                            </span>		
                               </div>		   
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label"> Name of person excluded</label>
            <input class="textinput" type="text" ng-mobel="sup.supplement_fname" value="{{sup.supplement_fname}}" placeholder="First name"  name="supplement_fname[]">
            <input class="textinput" type="text" ng-mobel="sup.supplement_mname" value="{{sup.supplement_mname}}"  placeholder="Middle name" name="supplement_mname[]">
            <input class="textinput" type="text" ng-mobel="sup.supplement_lname" value="{{sup.supplement_lname}}" placeholder="Last name" name="supplement_lname[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left"> Date of Birth &nbsp;</label>
            <input class="textinput span7 datepick" ng-mobel="sup.supplement_dfrom" type="text" name="supplement_dfrom[]"  value="{{sup.supplement_dfrom}}" id="supplement_dfrom" placeholder="mm/dd/yyy">
          </span>
          <span class="span4">
            <label class="control-label pull-left"> SSN &nbsp;</label>
            <input class="textinput span10" ng-mobel="sup.supplement_dto" type="text" name="supplement_dto[]" value="{{sup.supplement_dto}}" id="supplement_dto" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left"> License number &nbsp;</label>
            <input class="textinput span7" ng-mobel="sup.supplement_lnum" type="text" placeholder="" value="{{sup.supplement_lnum}}" name="supplement_lnum[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label"> Reason for exclusion</label>
            <textarea class="span12" placeholder="" name="supplement_Reason[]">{{sup.supplement_Reason}}</textarea>
          </span>
        </div>		
      </div>    
	<div>
      <!--  <div class="form-actions">
          <button class="btn btn-primary" onclick="fancybox_close();">Save</button>
          <button class="btn" onclick="reload_close();">Cancel</button>
        </div>-->
      </div>
     </div>
    </div>
   </div>
   
   <div class="row-fluid">
            <span class="span6" >
              <label class="control-label pull-left span8"><span class="numbers"></span> Do you have workmen &nbsp;compensation insurance?</label>
              <select class="pull-left pull-left-2 width_option" name="insurance_val" ng-model="insured_add.insurance_val"  id="insurance_val_id"  required>
                 <option value="" >Select</option>
                 <option value="No" >No</option>
                 <option value="Yes" >Yes</option>
              </select>
			  
            </span>
			</div>
		
   <div class="row-fluid" ng-if="insured_add.insurance_val=='Yes'">
            <span class="span2">
              <label class="control-label">Policy Number</label>
              <input class="textinput span12" type="text" placeholder="" ng-model="insured_add.ins_Policy_num"   name="ins_Policy_num">
            </span>
            <span class="span5">
              <label class="control-label">Insurance company for workmen compensation</label>
              <input class="textinput span12" type="text" placeholder="" ng-model="insured_add.ins_Policy_comp"  name="ins_Policy_comp">
            </span>
            <span class="span2">
              <label class="control-label">Effective Date</label>
              <input  maxlength="10" type="text" name="effective_from_ins" ng-model="insured_add.effective_from_ins"  id="date_from_eff"  jqdatepicker end="date_to_exp" class="span12 " pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}">
            </span>
            <span class="span2">
              <label class="control-label">Expiring Date</label>
              <input maxlength="10" type="text" name="expiration_from_ins"  id="date_to_exp" ng-model="insured_add.expiration_from_ins" jqdatepicker end="date_to_exp"    class="span12 " pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}">
            </span>
            
             <div class="row-fluid">
             
			   <div class="span6 lightblue">
                      <label >Please send the certificates &nbsp;</label>                                 
                      <input type="file" id="first_upload_second" name="file_upload_second" value="{{insured_add.first_upload_second}}">
                      <a href="<?= base_url('uploads/docs')?>{{insured_add.first_upload_second}}">{{insured_add.first_upload_second}}</a>
                    </div>            
           </div>
            
      </div>
   
    <div class="row-fluid">
            <span class="span6" >
              <label class="control-label pull-left span8"><span class="numbers"></span> Does the owner(s) have a commercial license(s)? &nbsp;</label>
              <select class="pull-left pull-left-2 width_option" name="owner_licenses" ng-model="insured_add.owner_license"  id="owner_id12"  required>
                 <option value="" >Select</option>
                 <option value="No" >No</option>
                 <option value="Yes" >Yes</option>
              </select>
			  
            </span>
	</div>     
          
   <div class="container-fluid" ng-if="insured_add.owner_license=='No'">
      <div class="page-header">
        <h1><!--GHI-6--> <small>Driver / Owner exclusion supplement form</small>
        </h1>
      </div>
      <div class="well">
        <label class="control-label control-label-1"> Named Insured / DBA</label>

        <input class="textinput span11 supplement_dba" type="text"  name="sup_dba" ng-model="insured_add.sup_dba" id="supplement_dba2"  placeholder="">
      </div>
      <p>
        <br>It is hereby understood and agreed that all coverages and our obligation to defend under this policy shall not apply nor accrue to the benefit of any INSURED or any third party claimant while any VEHICLE or MOBILE EQUIPMENT described in the policy or
        any other VEHICLE or MOBILE EQUIPMENT, to which the terms of the policy are extended, is being driven, used or operated by any person designated below.</p>
      <p>
        <br>The driver exclusion shall be binding upon every INSURED to whom such policy or endorsements provisions apply while such policy is in force and shall continue to be binding with respect to any continuation, renewal or replacement of such policy by the
        Named Insured or with respect to any reinstatement of such policy within 30 days of any lapse thereof. This DRIVER EXCLUSION provision shall conform to State statutes and laws.</p>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">How many additional persons?</label>
        </span>
        <span class="span1">
          <input class="textinput span12 supplement_val" type="text" name="sup_val" ng-model="insured_add.sup_val" id="supplement_val1" ng-change="add_row_i('supps')" placeholder="" pattern="[0-9]+"  maxlength="2">
        </span>
        <span class="span7">
        <!--  <button class="btn"> <i class="icon icon-plus"></i> Add</button>-->
        </span>
      </div>      
      
 <div ng-repeat="sup in rows.supps" ng-init="suppIndex=$index"> 
  <div class="well top20" > 
   <div class="add_addition_row">	
	<input type="hidden" value="19" name="supplement_parent_id[]"/> 
      <input type="hidden" value="{{sup.supplement_id}}" name="supplement_id[]"/> 
     
        		<div class="row-fluid">      
                     <span class="span12">       
                         <button class="btn pull-right" style="margin-bottom:5px;" onclick="persons_delete2(<?php echo $o; ?>);">Delete</button>		
                            </span>		
                               </div>		   
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label"> Name of person excluded</label>
            <input class="textinput" type="text" ng-mobel="sup.supplement_fname" value="{{sup.supplement_fname}}" placeholder="First name"  name="supplement_fname[]">
            <input class="textinput" type="text" ng-mobel="sup.supplement_mname" value="{{sup.supplement_mname}}" placeholder="Middle name" name="supplement_mname[]">
            <input class="textinput" type="text" ng-mobel="sup.supplement_lname" value="{{sup.supplement_lname}}" placeholder="Last name" name="supplement_lname[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left"> Date of Birth &nbsp;</label>
            <input class="textinput span7 datepick" ng-mobel="sup.supplement_dfrom" value="{{sup.supplement_dfrom}}" type="text" name="supplement_dfrom[]" id="supplement_dfrom" placeholder="mm/dd/yyy">
          </span>
          <span class="span4">
            <label class="control-label pull-left"> SSN &nbsp;</label>
            <input class="textinput span10" ng-mobel="sup.supplement_dto" type="text" value="{{sup.supplement_dto}}" name="supplement_dto[]" id="supplement_dto" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left"> License number &nbsp;</label>
            <input class="textinput span7" ng-mobel="sup.supplement_lnum" type="text" value="{{sup.supplement_lnum}}" placeholder="" name="supplement_lnum[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label"> Reason for exclusion</label>
            <textarea class="span12" placeholder="" name="supplement_Reason[]">{{sup.supplement_Reason}}</textarea>
          </span>
        </div>		
      </div> 
     </div>
    </div>
  
      
    </div>   
   <div class="row-fluid top20">
            <span class="span12" >
              <label class="control-label pull-left span8"><span class="numbers"></span> Is applicant the registered owner(s) of all the vehicle(s) listed on this application, other than the unidentified trailer(s)? &nbsp;</label>
              <select class="pull-left pull-left-2 width_option" name="owner_applicant" ng-model="insured_add.owner_applicant"  id="owner_applicant"  required>
                 <option value="" >Select</option>
                 <option value="No" >No</option>
                 <option value="Yes" >Yes</option>
              </select>
			  
            </span>
	</div> 
    <div class="container-fluid top20" ng-if="insured_add.owner_applicant=='Yes'">
    
      <div class="page-header ">
        <h1><!--GHI-9--> <small>Registered owner info supplement form</small>
        </h1>
      </div>
      <div class="row-fluid">
        <span class="span5">
          <label class="control-label"> How many additional Registered Owner or Partner's name?</label>
        </span>
        <span class="span1">
          <input class="textinput span12" type="text" ng-model="insured_add.persons_val" name="persons_val" id="persons_val" ng-change="add_row_i('persons')" placeholder=""  pattern="[0-9]+"  maxlength="2">
        </span>
        <span class="span7">
       <!--   <button class="btn" onclick="persons_add();"> <i class="icon icon-plus"></i> Add</button>-->
        </span>
      </div>
      <div ng-repeat="person in rows.persons" ng-init="perIndex=$index">
      <div class="well top20" > 	
       <input type="hidden" value="{{person.reg_id}}" name="reg_id[]" /> 	  
      <div class="row-fluid">           
      <span class="span12">          
      <!-- <button class="btn pull-right" style="margin-bottom:5px;" onclick="persons_delete12(<?php echo $g; ?>);">Delete</button>	-->	   
       </span>		  
       </div>			
        <div class="row-fluid">           
         <span class="span4">           
          <label class="control-label pull-left span10" style="margin-bottom:-5px;"> Registered Owner or Partners name &nbsp;</label>            
          <input class="textinput span4 val_inserts" type="text" id="first_name{{perIndex}}"  ng-model="person.reg_fname" onchange="" name="reg_fname[]" value="" placeholder="First name">            
          <input class="textinput span4 val_inserts" type="text" id="middle_name{{perIndex}}" ng-model="person.reg_mname"  onchange="" name="reg_mname[]" value="" placeholder="Middle name">            
          <input class="textinput span4 val_inserts" type="text" id="last_name{{perIndex}}" ng-model="person.reg_lname"  onchange="" name="reg_lname[]" value="" placeholder="Last name">         
           </span>		
           <span class="span2">            
           <label class="control-label span10"> Address</label>            
           <input class="textinput span12" type="text" placeholder=""  ng-model="person.reg_address" name="reg_address[]">          
           </span>      
            <span class="span1" style="width:10%">           
            <label class="control-label span10">City</label>            
            <input class="textinput span12" type="text" name="reg_city[]" ng-model="person.reg_city" placeholder="">          
            </span>       
            <span class="span1" style="width:10%">          
              <label class="control-label span10">Country</label> 
            
               <?php echo  getCountryDropDown1($name='reg_country[]', $att=' " id="reg_country{{perIndex}}" ng-model="person.reg_country" class="span9" ng-change="getState(person.reg_country, &quot;state_shows_{{perIndex}}&quot;, &quot;reg_state[]&quot; )"','', 'AD'); ?>

                  
                </span>        
            <span class="span1" style="width:10%">		  
             <label class="control-label span12"> State</label>
                          
               <?php echo getStateDropDown('AD', $name='reg_state[]', $att='id="state_shows_{{perIndex}}" ng-model="person.reg_state"', '', ''); ?>
                         
            
          </span>          
          <span class="span2">           
             <label class="control-label span10"> Zip</label>           
             <input class="textinput span5" type="text" placeholder="" ng-model="person.reg_zip" name="reg_zip[]"  pattern="[0-9]+"  maxlength="5">			
             <input class="textinput span5" type="text" placeholder="" ng-model="person.reg_zip1" name="reg_zip1[]"   pattern="[0-9]+"  maxlength="4">         
          </span>        
         </div>     
          <div class="row-fluid">         
             <span class="span3">            
              <label class="control-label span10">License Number</label>            
              <input class="textinput val_inserts" type="text" placeholder="" id="lnumber{{perIndex}}" onchange="" ng-model="person.reg_lnum"  name="reg_lnum[]">          
            </span>          
            <span class="span3 ">            
              <label class="control-label span10"> S.S.N</label>            
              <input class="textinput val_inserts" type="text" placeholder="" id="Snumber{{perIndex}}" onchange="" ng-model="person.reg_ssn" name="reg_ssn[]">         
            </span>          
            <span class="span3">            
               <label class="control-label span10">Commercial License</label>                    
               <select class="pull-left pull-left-1 width_option" name="reg_r0[]" id="radio_1{{perIndex}}" ng-model="person.reg_r1" style="float:left;" required>
		       <option value="No" >No</option>
               <option value="Yes">Yes</option>
               </select>       
             </span>     
           </div>    

						   
            <div class="row-fluid">     
                     <span class="span12">      
                           <label class="control-label pull-left"> Please provide a photo copy of license and MVR &nbsp;</label> 
                          <!-- <input class="textinput" type="file" placeholder="Upload file" name="" value="" >-->			
                          <input type="file" name="MVR_file[]" id="first_loadeds" value="{{person.MVR_file}}">	
                            <a href="<?= base_url('uploads/docs')?>{{person.MVR_file}}">{{person.MVR_file}}</a>
                            </span>       
                           </div>
   
    
                         
    <div class="container-fluid" ng-if="person.reg_r1=='Yes'">
      <div class="page-header">
        <h1><!--GHI-6--> <small>Driver / Owner exclusion supplement form</small>
        </h1>
      </div>
      <div class="well">
        <label class="control-label control-label-1"> Named Insured / DBA</label>

        <input class="textinput span11 supplement_dba" type="text"  name="supp_dba[]" ng-model="person.supp_dba" id="supplement_dba2"  placeholder="">
      </div>
      <p>
        <br>It is hereby understood and agreed that all coverages and our obligation to defend under this policy shall not apply nor accrue to the benefit of any INSURED or any third party claimant while any VEHICLE or MOBILE EQUIPMENT described in the policy or
        any other VEHICLE or MOBILE EQUIPMENT, to which the terms of the policy are extended, is being driven, used or operated by any person designated below.</p>
      <p>
        <br>The driver exclusion shall be binding upon every INSURED to whom such policy or endorsements provisions apply while such policy is in force and shall continue to be binding with respect to any continuation, renewal or replacement of such policy by the
        Named Insured or with respect to any reinstatement of such policy within 30 days of any lapse thereof. This DRIVER EXCLUSION provision shall conform to State statutes and laws.</p>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">How many additional persons?</label>
        </span>
        <span class="span1">
          <input class="textinput span12 supplement_val" type="text" name="supp_val[]" ng-model="person.supp_val" id="supplement_val1" ng-change="add_row_i('suppss',person.supp_val,perIndex)" placeholder="" pattern="[0-9]+"  maxlength="2">
        </span>
        <span class="span7">
        <!--  <button class="btn"> <i class="icon icon-plus"></i> Add</button>-->
        </span>
      </div>      
      
 <div ng-repeat="supps in person.suppss" ng-init="suppIndex=$index"> 
  <div class="well top20" > 
   <div class="add_addition_row">	
	<input type="hidden" value="20" name="supplement_parent_id[]"/> 
      <input type="hidden" value="{{supps.supplement_id}}" name="supplement_id[]"/> 
     
        		<div class="row-fluid">      
                     <span class="span12">       
                       <!--  <button class="btn pull-right" style="margin-bottom:5px;" onclick="persons_delete2(<?php echo $o; ?>);">Delete</button>	-->	
                            </span>		
                               </div>		   
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label"> Name of person excluded</label>
            <input class="textinput" type="text" ng-mobel="supps.supplement_fname" placeholder="First name" value="{{supps.supplement_fname}}"  name="supplement_fname[]">
            <input class="textinput" type="text" ng-mobel="supps.supplement_mname"  placeholder="Middle name" value="{{supps.supplement_mname}}" name="supplement_mname[]">
            <input class="textinput" type="text" ng-mobel="supps.supplement_lname" placeholder="Last name" value="{{supps.supplement_lname}}" name="supplement_lname[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left"> Date of Birth &nbsp;</label>
            <input class="textinput span7 datepick" ng-mobel="supps.supplement_dfrom" type="text"  value="{{supps.supplement_dfrom}}" name="supplement_dfrom[]" id="supplement_dfrom" placeholder="mm/dd/yyy">
          </span>
          <span class="span4">
            <label class="control-label pull-left"> SSN &nbsp;</label>
            <input class="textinput span10" ng-mobel="supps.supplement_dto" type="text" name="supplement_dto[]" value="{{supps.supplement_dto}}" id="supplement_dto" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left"> License number &nbsp;</label>
            <input class="textinput span7" ng-mobel="supps.supplement_lnum" type="text" placeholder="" value="{{supps.supplement_lnum}}" name="supplement_lnum[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label"> Reason for exclusion</label>
            <textarea class="span12" placeholder="" name="supplement_Reason[]">{{supps.supplement_Reason}}</textarea>
          </span>
         </div>		
       </div> 
      </div>
     </div>       
    </div>                            
  </div>
      
           </div>
   
	</div>
    
       <div class="row-fluid top20">
            <span class="span12" >
              <label class="control-label pull-left span8"><span class="numbers"></span>Does the applicant own any vehicle(s) not scheduled on this application? &nbsp;</label>
              <select class="pull-left pull-left-2 width_option" name="applicant_scheduled" ng-model="insured_add.applicant_scheduled"  id="applicant_scheduled"  required>
                 <option value="" >Select</option>
                 <option value="No" >No</option>
                 <option value="Yes" >Yes</option>
              </select>
			  
            </span>
	</div> 
      
    <div class="container-fluid" ng-if="insured_add.applicant_scheduled=='Yes'" >
      <div class="page-header ">
        <h1><!--GHI-11 --><small>Non-operational supplement form</small>
        </h1>
      </div>
      <div class="row-fluid">
        <span class="span12">
          <label class="control-label">Exclusion</label>
        </span>
      </div>
      <p>The Named Insured agrees that the said Policy shall not and does not protect the Named Insured from claims for injury, damage or loss sustained by any person when caused by a motor vehicle not specified in said policy, and if the company shall be obliged
        to pay any claim that it would not otherwise be obligated to pay but for the attachment of any endorsements required by any State or Federal authority, the Insured agrees to reimburse the company in the amount paid and all sums including costs and expenses
        which shall have been paid in connection with such claims.</p>
      <div class="row-fluid">
        <span class="span3">
          <label class="control-label"> How many additional vehicles?</label>
        </span>
        <span class="span1">
          <input class="textinput span12" type="text" name="ex_id" ng-model="insured_add.ex_id"  ng-change="add_row_i('exclusions')" placeholder=""  pattern="[0-9]+"  maxlength="2">
        </span>
        
      </div>
   <div ng-repeat="exclusion in rows.exclusions" ng-init="exIndex=$index">
    <div class="well">       
	 <div class="row-fluid">
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label pull-left span4"> VIN &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="vehicles_VIN[]" ng-model="exclusion.vehicles_VIN">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4"> GVW &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_GVW[]" placeholder="" ng-model="exclusion.vehicles_GVW">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4"> Year &nbsp;</label>
            <input class="textinput span7" type="text" placeholder=""   pattern="^\d{4}$" ng-model="exclusion.vehicles_Year"  maxlength="4" name="vehicles_Year[]">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4"> Make &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="vehicles_make[]" ng-model="exclusion.vehicles_make">
          </span>
        </div>
        <div class="row-fluid">
          
          <span class="span3">
            <label class="control-label pull-left span4"> Body Type &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_body[]" placeholder="" ng-model="exclusion.vehicles_body">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4"> Model  &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_model[]" value="" ng-model="exclusion.vehicles_model" placeholder="Tractors, Trailers, etc">
          </span>
           <span class="span3">
            <label class="control-label pull-left span5"> Reason &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_reason[]" ng-model="exclusion.vehicles_reason" placeholder="" value="">
          </span>
        </div>
	</div>
	
	
  </div>
       </div>
 </div>
    
      <div class="row-fluid top20">
            <span class="span12" >
            
              <label class="control-label pull-left span8"><span class="numbers"></span>Does the applicant rent, lease, or sub haul vehicle(s) to others? &nbsp;</label>
              <select class="pull-left pull-left-2 width_option" name="applicant_rent" ng-model="insured_add.applicant_rent"  id="applicant_rent"  required>
                 <option value="" >Select</option>
                 <option value="No" >No</option>
                 <option value="Yes" >Yes</option>
              </select>
			  
            </span>
	</div> 
    
    <div class="container-fluid" ng-if="insured_add.applicant_rent=='Yes'">
      <div class="page-header">
        <h1><!--GHI-5--> <small>Driver/Sub-Hauler supplement form</small>
        </h1>
      </div>
      <div class="well">
	  <input type="hidden" name="ghi5_parent_id" value="25"/>
	  <input type="hidden" value="{{insured_gh5.haul_id}}" name="hauled_n_id" />
		
		<label class="control-label">Check all practices used by your company: (Give full explanation of each question. Use separate sheet, if necessary)</label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">1.&nbsp;</label>
            <label class="checkbox pull-left">{{insured_gh5.hauler_MVR}}
              <input type="checkbox" value="true" name="hauler_MVR" ng-model="insured_gh5.hauler_MVR"  ng-checked="insured_gh5.hauler_MVR">
              <span>MVR check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Roa" ng-model="insured_gh5.hauler_Roa" ng-checked="insured_gh5.hauler_Roa" >
              <span>Road Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Wri" ng-model="insured_gh5.hauler_Wri" ng-checked="insured_gh5.hauler_Wri">
              <span>Written application&nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Phy" ng-model="insured_gh5.hauler_Phy" ng-checked="insured_gh5.hauler_Phy">
              <span>Physical exam &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Drug" ng-model="insured_gh5.hauler_Drug" ng-checked="insured_gh5.hauler_Drug">
              <span>Drug Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Ref" ng-model="insured_gh5.hauler_Ref" ng-checked="insured_gh5.hauler_Ref">
              <span>Reference Check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Ver" ng-model="insured_gh5.hauler_Ver" ng-checked="insured_gh5.hauler_Ver">
              <span>Employment Verification &nbsp;</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span10">
            <label class="control-label pull-left">2. Describe acceptability for hiring drivers:&nbsp;</label>
            <textarea class="span12 span12-1" placeholder="" name="hauler_hir_name">{{insured_gh5.hauler_hir_name}}</textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left ">3. Use Owner/Operators? &nbsp;</label>
            <select name="hauler_Own" class="span2" ng-model="insured_gh5.hauler_Own">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
             
            </select>

            <input class="textinput  h5-active ui-state-valid" ng-if="insured_gh5.hauler_Own=='Yes'" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="% of Revenues" name="hauler_Rev" ng-model="insured_gh5.hauler_Rev" id="hauler_Rev">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">4. Use team drivers? &nbsp;</label>
            <select name="hauler_team" class="span2" ng-model="insured_gh5.hauler_team">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
             
            </select>
            <input class="textinput" ng-if="insured_gh5.hauler_team=='Yes'" type="text" placeholder="Number / Teams" ng-model="insured_gh5.hauler_Num" name="hauler_Num" id="hauler_Num">
            <textarea name="hauler_Num_y"  ng-if="insured_gh5.hauler_team=='Yes'" id="hauler_Num_y" class="span12" placeholder="If yes, please explain">{{insured_gh5.hauler_Num_y}}</textarea>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">5. Are Motor Vehicle Reports of employed drivers pulled and reviewed? &nbsp;</label>
            <select name="hauler_Vehi" class="span2" id="hauler_Vehi" ng-model="insured_gh5.hauler_Vehi">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
              
            </select>
            <br/>
            <input class="textinput" style="float: left;" ng-if="insured_gh5.hauler_Vehi=='Yes'" ng-model="insured_gh5.hauler_often" id="hauler_often" type="text" placeholder="If yes, how often" name="hauler_often">
            <span ng-if="insured_gh5.hauler_Vehi=='Yes'" >
            <input type="file"  name="hauler_attach" value="{{insured_gh5.hauler_attach}}">
							<a href="<?= base_url()?>uploads/docs/{{insured_gh5.hauler_attach}}">{{insured_gh5.hauler_attach}}</a>		
                  </span>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">6. Are all drivers covered under worker's Compensation? &nbsp;</label>
            <select class="span2" name="hauler_drivers" ng-model="insured_gh5.hauler_drivers">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" >
          <span class="span3">
            <label class="control-label">Name of insurance co.</label>
            <input class="textinput" type="text" placeholder="" name="hauler_ins" ng-model="insured_gh5.hauler_ins">
          </span>
          <span class="span3">
            <label class="control-label">Policy no.</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Pol" ng-model="insured_gh5.hauler_Pol">
          </span>
          <span class="span3">
            <label class="control-label">Effective date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" jqdatepicker end="hauler_Exp" name="hauler_Effect" id="hauler_Effect" ng-model="insured_gh5.hauler_Effect">
          </span>
          <span class="span3">
            <label class="control-label">Expiry date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" jqdatepicker name="hauler_Exp" id="hauler_Exp" ng-model="insured_gh5.hauler_Exp">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">7. Driver Turnover in the past year. &nbsp;</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label row-fluid">Hired</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" jqdatepicker name="hauler_Hir" ng-model="insured_gh5.hauler_Hir">
          </span>
          <span class="span3">
            <label class="control-label">Terminated</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" jqdatepicker name="hauler_Ter" ng-model="insured_gh5.hauler_Ter">
          </span>
          <span class="span3">
            <label class="control-label row-fluid">Quit</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" jqdatepicker name="hauler_Quit" ng-model="insured_gh5.hauler_Quit">
          </span>
          <span class="span3">
            <label class="control-label row-fluid">Others</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" jqdatepicker name="hauler_Oth" ng-model="insured_gh5.hauler_Oth">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span4">
            <label class="control-label">8. Max hours driven per day</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Max" ng-model="insured_gh5.hauler_Max">
          </span>
          <span class="span4">
            <label class="control-label">Per week</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Per" ng-model="insured_gh5.hauler_Per">
          </span>
          <span class="span4">
            <label class="control-label">(5 day week or 7 day)</label>
            <input class="textinput" type="text" placeholder="" name="hauler_day" pattern="[0-9]+" ng-model="insured_gh5.hauler_day" >
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">9. How are drivers compensated? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Hourly" name="hauler_comp" ng-model="insured_gh5.hauler_comp">
              <span>Hourly &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Per trip" name="hauler_comp" ng-model="insured_gh5.hauler_comp">
              <span>Per trip &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Salary" name="hauler_comp" ng-model="insured_gh5.hauler_comp">
              <span>Salary &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" name="hauler_comp" ng-model="insured_gh5.hauler_comp">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" name="hauler_Others" ng-model="insured_gh5.hauler_Others">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">10. What hours of the day do you drivers operate?</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">10.A. &nbsp;6 AM to 2 PM</label>
            <input class="textinput  h5-active ui-state-valid" pattern="([1-9]{1}|(10)|[0-9]{2,2})" type="text" placeholder="%" name="hauler_hours" ng-model="insured_gh5.hauler_hours">
          </span>
          <span class="span4">
            <label class="control-label">10.B. &nbsp;2 PM to 10 PM</label>
            <input class="textinput h5-active ui-state-valid" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="%" name="hauler_hour" ng-model="insured_gh5.hauler_hour">
          </span>
          <span class="span4">
            <label class="control-label">10.C. &nbsp;10 PM to 6 AM</label>
            <input class="textinput h5-active ui-state-valid" type="text"  pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="%" name="hauler_hou" ng-model="insured_gh5.hauler_hou">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">11. Where do your drivers sleep when they are on a trip? &nbsp;</label>
            <label class="radio pull-left radio-1">
              <input type="radio" value="At Home" name="hauler_sleep" ng-model="insured_gh5.hauler_sleep">
              <span>At Home &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Motel" name="hauler_sleep" ng-model="insured_gh5.hauler_sleep">
              <span>Motel &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="In the cab" name="hauler_sleep" ng-model="insured_gh5.hauler_sleep">
              <span>In the cab &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" name="hauler_sleep" ng-model="insured_gh5.hauler_sleep">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" name="hauler_hother" ng-model="insured_gh5.hauler_hother">
            <label class="radio pull-left">
              <input type="radio" value="Hourly" name="hauler_sleep" ng-model="insured_gh5.hauler_sleep">
              <span>Hourly &nbsp;</span>
            </label>
          </span>
        </div>
        <p>You must inform the company before hiring any new driver. You should have confirmation in writing regarding the acceptability of the driver by GHI.</p>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">12. Is your operation subject to time restraints when delivering the commodity? &nbsp;</label>
            <select name="hauler_rest" class="span2" ng-model="insured_gh5.hauler_rest">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">13. If not hauling for others, will the vehicles be parked at a jobsite most of the day? &nbsp;</label>
            <select class="span2" name="hauler_roth" ng-model="insured_gh5.hauler_roth">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">14. Do you have haul for others? &nbsp;</label>
            <select class="span2" name="hauler_haul"  id="hauler_haul" ng-model="insured_gh5.hauler_haul">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid"  id="hauler_haul12" ng-if="insured_gh5.hauler_haul=='Yes'">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Is there any written agreement?</th>
                  <th>Copy attached?</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                <tr >
                  <td>
                    <input class="textinput" type="text" placeholder="" name="haul_name" ng-model="insured_gh5.haul_name">
                  </td>
                  <td>
                    <select  name="haul_agree" class="span6" ng-model="insured_gh5.haul_agree">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                    <select name="haul_attach" class="span6" id="haul_attach" ng-model="insured_gh5.haul_attach" >
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
       
                  </td>
				  <td>
				  <input type="file" id="first_uploaded1" name="haul_attach_file" ng-if="insured_gh5.haul_attach=='Yes'" value="{{insured_gh5.haul_attach_file}}">
								<a href="<?= base_url()?>uploads/docs/{{insured_gh5.haul_attach_file}}">{{insured_gh5.haul_attach_file}}</a>								
				  
				  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">15. Gross receipts - Estimate this year:&nbsp;</label>
          </span>
          <span class="span4">
            <label class="control-label">Last year:</label>
            <input class="textinput " type="text" placeholder="$" format name="haul_Last" ng-model="insured_gh5.haul_Last">
          </span>
          <span class="span4">
            <label class="control-label">Next year:</label>
            <input class="textinput " type="text" placeholder="$" format name="haul_Next" ng-model="insured_gh5.haul_Next">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">16. (i) Are any vehicles or equipment loaned, rented or leased to others? &nbsp;</label>
            <select name="haul_equi" class="span2" ng-model="insured_gh5.haul_equi">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <label class="control-label pull-left">&nbsp; &nbsp; &nbsp;(ii) Do you lease, hire, rent or borrow any vehicles from others? &nbsp;</label>
        <label class="radio pull-left">
          <input type="radio" value="Yes" name="haul_rent" ng-model="insured_gh5.haul_rent" ng-checked="insured_gh5.haul_rent">
          <span>Yes &nbsp;</span>
        </label>
        <label class="radio pull-left">
          <input type="radio" value="No" name="haul_rent" ng-model="insured_gh5.haul_rent" ng-checked="insured_gh5.haul_rent">
          <span>No &nbsp;</span>
        </label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; What is the average length of the lease? &nbsp;</label>
            <input class="textinput" type="text" placeholder="" name="haul_lease" ng-model="insured_gh5.haul_lease">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_agreement" id="haul_agreement" ng-model="insured_gh5.haul_agreement">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
      
         <!--   <input class="textinput pull-left" type="text" placeholder="Upload a copy of the agreement" name="">-->
			
			
			      <input type="file" id="first_uploaded2" name="haul_agree_file" ng-if="insured_gh5.haul_agreement=='Yes'" class="up_file" value="{{insured_gh5.haul_agree_file}}">
									
				  <a href="<?= base_url()?>uploads/docs/{{insured_gh5.haul_agree_file}}">{{insured_gh5.haul_agree_file}}</a>
			
			
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">17. What is the cost to lease, hire, rent or borrow vehicles? &nbsp;</label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span6">
            <label class="control-label">Per month</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_month" format ng-model="insured_gh5.haul_month">
          </span>
          <span class="span6">
            <label class="control-label">Per year</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_year" format ng-model="insured_gh5.haul_year">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">18. What type of vehicles do you lease, hire, rent or borrow? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="haul_bor" ng-model="insured_gh5.haul_bor">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">19. Do you use owner/operators (sub-hauler)? &nbsp;</label>
            <select name="haul_ope" class="span2"  id="haul_ope" ng-model="insured_gh5.haul_ope">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh5.haul_ope=='Yes'">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp;If yes, is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_yagree" id="haul_yagree" ng-model="insured_gh5.haul_yagree">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
                        
			      <input type="file"  name="haul_yagree_file" ng-if="insured_gh5.haul_yagree=='yes'" class="up_file" value="{{insured_gh5.haul_yagree_file}}">
									
				  <a ng-if="insured_gh5.haul_yagree=='yes'" href="<?= base_url()?>uploads/docs/{{insured_gh5.haul_yagree_file}}">{{insured_gh5.haul_yagree_file}}</a>
			
						
			 </span>
        </div>
            <div class="row-fluid">
              <span class="span9">
                <label class="control-label pull-left">20. Owner operator/sub-hauler have their own insurance? &nbsp;</label>
                <select  class="span2" name="haul_yins" id="haul_yins" ng-model="insured_gh5.haul_yins" >
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </span>
            </div>
            <div class="row-fluid" ng-if="insured_gh5.haul_yins=='Yes'">
              <span class="span9">
			  <input type="button"  class="btn btn-small" ng-click="add_row_g('datas');" value="Add insurance"/>
            <!--    <button class="btn btn-small" onclick="add_insurance();">Add insurance</button>-->
              </span>
            </div>
         
        <table class="table" ng-if="insured_gh5.haul_yins=='Yes'" ng-show="show_type==true">
          <thead>
            <tr>
              <th>Name</th>
              <th>Insurance Carrier</th>
              <th>Insurance Certificate Attached</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody id="add_new_table1">
            <tr ng-repeat="data in rows.datas">
              <input class="textinput" type="hidden" placeholder="" name="add_ins_name[]" value="{{data.add_ins_id}}" ng-model="data.add_ins_id">
              <td>
                <input class="textinput" type="text" placeholder="" name="add_ins_name[]" ng-model="data.add_ins_name">
              </td>
              <td>
                <input class="textinput" type="text" placeholder="" name="add_ins_carrier[]" ng-model="data.add_ins_carrier">
              </td>
              <td>
                <select name="ins_attach_y[]" class="span6" id="ins_attach_y" ng-model="data.ins_attach_y" >
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </td>
              <td >

			  <input type="file" id="first_uploaded4" name="add_ins_attach[]" value="{{insured_gh5.add_ins_attach}}" ng-if="data.ins_attach_y=='Yes'">
			  <a href="<?= base_url()?>uploads/docs/{{insured_gh5.add_ins_attach}}" ng-if="data.ins_attach_y=='Yes'">{{insured_gh5.add_ins_attach}}</a>
					
              
              </td>
              <td><!-- <i class="icon icon-remove"></i> -->
			  <a class="btn btn-mini btn-1 remove_ins_row" id="remove_phone_row" ng-click="remove_row_g($index,'datas');"><i class="icon icon-remove"></i></a>
              </td>
            </tr>
		
          </tbody>
        </table>
		 
		<table class="table" >
		</table>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">21. Will owner/operators(sub-haulers) be scheduled on your policy? &nbsp;</label>
            <select name="ins_pol" class="span2" id="ins_pol" ng-model="insured_gh5.ins_pol">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh5.ins_pol=='Yes'">
          <span class="span12">
		   <input type="button" class="btn btn-small" ng-click="add_row_g('datas_p')" value="Add owner/operator"/>
       <!--     <button class="btn btn-small" onclick="add_owner();">Add owner/operator</button>-->
          </span>
        </div>
        <div class="row-fluid"  ng-if="insured_gh5.ins_pol=='Yes'" ng-show="show_type_p==true">
          <span class="span12">
            <table class="table" id="add_owner_table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Agreement Attached?</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody >
                <tr ng-repeat="data in rows.datas_p">
                    <input class="textinput span11" type="hidden" placeholder="" name="add_owner_id[]" value="{{data.add_owner_id}}" ng-model="data.add_owner_id">
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_owner_name[]" ng-model="data.add_owner_name">
                  </td>
                  <td>
                    <select name="add_owner_y[]" id="add_owner_y" class="span5" ng-model="data.add_owner_y">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
           			  
				   <input type="file" ng-if="data.add_owner_y=='Yes'" value="{{data.add_owner_attach}}">
				   <a href="<?= base_url()?>uploads/docs/{{data.add_owner_attach}}" ng-if="data.add_owner_attach=='Yes'">{{data.add_owner_attach}}</a>
							  
                  </td>
                  <td> <!--<i class="icon icon-remove"></i>-->
				  <a class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row" ng-click="remove_row_g($index,'datas_p');"><i class="icon icon-remove"></i></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">22. Do you use subcontractors? &nbsp;</label>
            <select class="span2" name="subcon_y" ng-model="insured_gh5.subcon_y" id="subcon_y"> 
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div ng-if="insured_gh5.subcon_y=='Yes'">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">a. Who are your subcontractors? &nbsp;</label>
            <input class="textinput span9" type="text" placeholder="" name="subcon_name" ng-model="insured_gh5.subcon_name">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">b. Are subcontractors required to provide Certificates of Insurance? &nbsp;</label>
            <select class="span2" name="subcon_cert" ng-model="insured_gh5.subcon_cert">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span   class="span12">
            <label class="control-label pull-left">c. What limit of Auto Liability are subcontractors required to carry? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_lia" ng-model="insured_gh5.subcon_lia">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">d. What job duties are performed by the subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_duties" ng-model="insured_gh5.subcon_duties">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">e. What is your cost to use subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_cost" ng-model="insured_gh5.subcon_cost">
          </span>
        </div>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">23. At any time will your employees, subcontractors, or owner/operators lease vehicles on your behalf? &nbsp;</label>
            <select class="span2" name="subcon_emp_y" ng-model="insured_gh5.subcon_emp_y" id="subcon_emp_y">
            
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="subcon_emp_y12" ng-if="insured_gh5.subcon_emp_y=='Yes'">
          <span class="span12">
		  <input type="button" class="btn btn-small" ng-click="add_row_g('datas_e')"  value="Add"/>
  <!--          <button class="btn btn-small" onclick="add_subcontract();">Add</button>-->
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh5.subcon_emp_y=='Yes'" ng-show="show_type_e==true">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Type of Vehicle</th>
                  <th>Lease agreement attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody >
                <tr ng-repeat="data in rows.datas_e">
                  <input class="textinput" type="hidden" placeholder="" name="subcon_add_id[]" ng-model="data.subcon_add_id" value="{{data.subcon_add_id}}">
             
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_name[]" ng-model="data.subcon_add_name">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_vehicle[]" ng-model="data.subcon_add_vehicle">
                  </td>
                  <td>
                    <select name="subcon_add_y[]" class="span6" id="subcon_add_y" ng-model="data.subcon_add_y">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                   <input type="file" ng-if="data.subcon_add_y=='Yes'" ng-if="data.subcon_add_y=='Yes'" name="subcon_add_attach[]" value="{{data.subcon_add_attach}}">
				   <a href="<?= base_url()?>uploads/docs/{{data.subcon_add_attach}}" ng-if="data.subcon_add_y=='Yes'">{{data.subcon_add_attach}}</a>
					
                  </td>
                     <td><!-- <i class="icon icon-remove"></i> -->
				       <a class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row" ng-click="remove_row_g($index,'datas_e');"><i class="icon icon-remove"></i></a>
                 	 </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">24. Do any employees, subcontractors or sub-haulers use their vehicles while conducting your business? &nbsp;</label>
           <select class="span2" name="sub_haul_y" ng-model="insured_gh5.sub_haul_y"  id="sub_haul_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y11" ng-if="insured_gh5.sub_haul_y=='Yes'">
          <span class="span12">
		  <input type="button" class="btn btn-small" ng-click="add_row_g('datas_s')"  value="Add" />
        <!--    <button class="btn btn-small" onclick="add_conduct1();">Add</button>-->
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y12" ng-if="insured_gh5.sub_haul_y=='Yes'" ng-show="show_type_s==true">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Insurance Carrier</th>
                  <th class="th-1">Limit of Liability Ins.</th>
                  <th class="th-1">Insurance Certificate Attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody >
                <tr ng-repeat="data in rows.datas_s">
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="sub_haul_add_name[]" ng-model="data.sub_haul_add_name">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="sub_haul_add_carrier[]" ng-model="data.sub_haul_add_carrier">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="$" format name="sub_haul_add_limit[]" ng-model="data.sub_haul_add_limit">
                  </td>
                  <td>
                    <select name="sub_haul_add_y[]" class="span6" id="sub_haul_add_y" ng-model="data.sub_haul_add_y">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
         
                  <input type="file" name="sub_haul_add_attach[]" id="first_uploaded7" ng-if="data.sub_haul_add_y=='Yes'" value="{{data.sub_haul_add_attach}}">
				   <a href="<?= base_url()?>uploads/docs/{{data.sub_haul_add_attach}}" ng-if="data.sub_haul_add_y=='Yes'">{{data.sub_haul_add_attach}}</a>
					  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				     <a class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row" ng-click="remove_row_g($index,'datas_s');"><i class="icon icon-remove"></i></a>
                 	  
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">25. Do you understand that we may audit your records? &nbsp;</label>
            <select class="span2" name="audit_y" ng-model="insured_gh5.audit_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">26. Do any of your family members use the vehicles? &nbsp;</label>
            <select class="span2" name="vehicles_y" id="vehicles_y" ng-model="insured_gh5.vehicles_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh5.vehicles_y=='Yes'">
          <span class="span12">
		  <input type="button" ng-click="add_row_g('datas_y')" class="btn btn-small" value="Add"/>
          <!--  <button class="btn btn-small" onclick="add_members();">Add</button>-->
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh5.vehicles_y=='Yes'" if-show="show_type_y==true">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Relationship</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_members_row1">
                <tr ng-repeat="data in rows.datas_y">
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_name[]" ng-model="data.add_mem_name">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_rel[]" ng-model="data.add_mem_rel">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				  <a class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row" ng-click="remove_row_g($index,'datas_y');"><i class="icon icon-remove"></i></a>
                 	  
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">27. Do you allow passengers to ride in your vehicles? &nbsp;</label>
            <select class="span2" name="ride_y" ng-model="insured_gh5.ride_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">28. Are you familiar with the U.S Department of Transportation driver requirements? &nbsp;</label>
            <select class="span2" name="driver_y" ng-model="insured_gh5.driver_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">29. (i) Do you mantain driver activity files? &nbsp;</label>
            <select class="span2" name="activity_y" ng-model="insured_gh5.activity_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">

          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iI) Do you review current MVRs on all drivers prior to hiring? &nbsp;</label>
            <select class="span2" name="prior_y" ng-model="insured_gh5.prior_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iii) Is there a formal driver hiring procedure? &nbsp;</label>
            <label class="radio pull-left" >
              <input type="radio" value="Yes" name="procedure_y" ng-model="insured_gh5.procedure_y"  >
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" name="procedure_y" ng-model="insured_gh5.procedure_y" >
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iv) Drug Screening? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" name="Drug_y" ng-model="insured_gh5.Drug_y" ng-checked="insured_gh5.Drug_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" name="Drug_y" ng-model="insured_gh5.Drug_y" ng-checked="insured_gh5.Drug_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(v) If you have a formal driver hiring/training program, provide a copy with this application? &nbsp;</label>
                <input type="file" id="first_uploaded8" name="hiring_file" value="{{insured_gh5.hiring_file}}">
                  <a href="<?= base_url()?>uploads/docs/{{insured_gh5.hiring_file}}" >{{insured_gh5.hiring_file}}</a>
				               		
		     </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">30. Are all drivers employees? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" ng-model="insured_gh5.employees_y"  name="employees_y" id="employees_11">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" ng-model="insured_gh5.employees_y"  name="employees_y" id="employees_12">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh5.employees_y=='No'">
          <span class="span12">
            <label class="control-label pull-left">If no, please explain &nbsp;</label>
            <input class="textinput span10" type="text" placeholder="" name="explain_no">
          </span>
        </div>
	  <span id="yes_employee1" ng-if="insured_gh5.employees_y=='Yes'">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">If yes, list below &nbsp;</label>
            <input type="button" class="btn btn-small" ng-click="add_row_g('datas_emp')" value="Add">
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh5.employees_y=='Yes'" ng-show="show_type_emp==true">
          <span class="span12">
            <table class="table" >
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Length of employment</th>
                  <th class="th-1">Years/Months</th>
                  <th></th>
                </tr>
              </thead>
              <tbody >
                <tr ng-repeat="data in rows.datas_emp">
                   <input class="textinput span11" type="text" placeholder="" name="add_list_id[]" ng-model="data.add_list_id" value="{{data.add_list_id}}">
                 
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_name[]" ng-model="data.add_list_name">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_length[]" ng-model="data.add_list_length">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_year[]" ng-model="data.add_list_year">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <a class="btn btn-mini btn-1 remove_phone_row" ng-click="remove_row_g($index,'datas_emp')" id="remove_phone_row"><i class="icon icon-remove"></i></a>
				   </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
		</span>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">31. Is there a formal safety program? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" ng-model="insured_gh5.safety_y" id="safety_y" value="Yes" name="safety_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" ng-model="insured_gh5.safety_y" id="safety_y1" value="No" name="safety_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh5.safety_y=='Yes'">
          <span class="span12">
            <label class="control-label pull-left">If yes, provide details or a copy:</label>
            	  <input type="file" name="safety_attach_file" id="first_uploaded16"  value="{{insured_gh5.safety_attach_file}}">
				  <a href="<?= base_url()?>uploads/docs/{{insured_gh5.safety_attach_file}}" >{{insured_gh5.safety_attach_file}}</a>
				   				
						
            <textarea class="span12 span12-1" placeholder="" name="details_y" > {{insured_gh5.details_y}} </textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">32. Provide details of your maintenance program:</label>
            <textarea class="span10 span12-1" placeholder="" name="program_detail" >{{insured_gh5.program_detail}}</textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">33. Do you agree to screen and report all potential operators immediately upon hiring before giving them a load? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" name="load_y" ng-model="insured_gh5.load_y">
              <span>Yes &nbsp;</span>
            </label>

            <label class="radio pull-left">
              <input type="radio" value="No" name="load_y" ng-model="insured_gh5.load_y">
              <span>No</span>
            </label>
          </span>
        </div>
		</div>
    
    </div>
    
          <div class="row-fluid top20">
            <span class="span12" >
              <label class="control-label pull-left span8"><span class="numbers"></span>Does the applicant hire vehicle(s), owner operator(s) or vehicle(s) owned by other parties? &nbsp;</label>
              <select class="pull-left pull-left-2 width_option" name="owner_operator" ng-model="insured_add.owner_operator"  id="owner_operator"  required>
                 <option value="" >Select</option>
                 <option value="No" >No</option>
                 <option value="Yes" >Yes</option>
              </select>			  
            </span>
	      </div> 
      <div class="container-fluid" ng-if="insured_add.owner_operator=='Yes'">
      <div class="page-header">
        <h1><!--GHI-5--> <small>Driver/Sub-Hauler supplement form</small>
        </h1>
      </div>
      <div class="well">
	  <input type="hidden" name="ghi5_parent_id1" value="26"/>
	  <input type="hidden" value="{{insured_gh51.haul_id}}" name="hauled_n_id1" />

		
		<label class="control-label">Check all practices used by your company: (Give full explanation of each question. Use separate sheet, if necessary)</label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">1.&nbsp;</label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_MVR1" ng-model="insured_gh51.hauler_MVR" ng-checked="insured_gh51.hauler_MVR">
              <span>MVR check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Roa1" ng-model="insured_gh51.hauler_Roa" ng-checked="insured_gh51.hauler_Roa">
              <span>Road Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Wri1" ng-model="insured_gh51.hauler_Wri" ng-checked="insured_gh51.hauler_Wri">
              <span>Written application&nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Phy1" ng-model="insured_gh51.hauler_Phy" ng-checked="insured_gh51.hauler_Phy">
              <span>Physical exam &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Drug1" ng-model="insured_gh51.hauler_Drug" ng-checked="insured_gh51.hauler_Drug">
              <span>Drug Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Ref1" ng-model="insured_gh51.hauler_Ref" ng-checked="insured_gh51.hauler_Ref">
              <span>Reference Check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="true" name="hauler_Ver1" ng-model="insured_gh51.hauler_Ver" ng-checked="insured_gh51.hauler_Ver">
              <span>Employment Verification &nbsp;</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span10">
            <label class="control-label pull-left">2. Describe acceptability for hiring drivers:&nbsp;</label>
            <textarea class="span12 span12-1" placeholder="" name="hauler_hir_name1">{{insured_gh51.hauler_hir_name}}</textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left ">3. Use Owner/Operators? &nbsp;</label>
            <select name="hauler_Own1" class="span2" ng-model="insured_gh51.hauler_Own">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
             
            </select>

            <input class="textinput  h5-active ui-state-valid" ng-if="insured_gh51.hauler_Own=='Yes'" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="% of Revenues" name="hauler_Rev1" ng-model="insured_gh51.hauler_Rev" id="hauler_Rev">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">4. Use team drivers? &nbsp;</label>
            <select name="hauler_team1" class="span2" ng-model="insured_gh51.hauler_team">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
             
            </select>
            <input class="textinput" ng-if="insured_gh51.hauler_team=='Yes'" type="text" placeholder="Number / Teams" ng-model="insured_gh51.hauler_Num" name="hauler_Num1" id="hauler_Num">
            <textarea name="hauler_Num_y1"  ng-if="insured_gh51.hauler_team=='Yes'" id="hauler_Num_y1" class="span12" placeholder="If yes, please explain">{{insured_gh51.hauler_Num_y}}</textarea>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">5. Are Motor Vehicle Reports of employed drivers pulled and reviewed? &nbsp;</label>
            <select name="hauler_Vehi1" class="span2" id="hauler_Vehi1" ng-model="insured_gh51.hauler_Vehi">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
              
            </select>
            <br/>
            <input class="textinput" style="float: left;" ng-if="insured_gh51.hauler_Vehi=='Yes'" ng-model="insured_gh51.hauler_often" id="hauler_often" type="text" placeholder="If yes, how often" name="hauler_often1">
            <span ng-if="insured_gh5.hauler_Vehi=='Yes'" >
            <input type="file"  name="hauler_attach1" value="{{insured_gh51.hauler_attach}}">
							<a href="<?= base_url()?>uploads/docs/{{insured_gh51.hauler_attach}}">{{insured_gh51.hauler_attach}}</a>		
                  </span>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">6. Are all drivers covered under worker's Compensation? &nbsp;</label>
            <select class="span2" name="hauler_drivers1" ng-model="insured_gh51.hauler_drivers">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" >
          <span class="span3">
            <label class="control-label">Name of insurance co.</label>
            <input class="textinput" type="text" placeholder="" name="hauler_ins1" ng-model="insured_gh51.hauler_ins">
          </span>
          <span class="span3">
            <label class="control-label">Policy no.</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Pol1" ng-model="insured_gh51.hauler_Pol">
          </span>
          <span class="span3">
            <label class="control-label">Effective date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" name="hauler_Effect1" jqdatepicker end="hauler_Exp1" id="hauler_Effect1" ng-model="insured_gh51.hauler_Effect">
          </span>
          <span class="span3">
            <label class="control-label">Expiry date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" name="hauler_Exp1" jqdatepicker end="hauler_Exp1" id="hauler_Exp1" ng-model="insured_gh51.hauler_Exp">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">7. Driver Turnover in the past year. &nbsp;</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label row-fluid">Hired</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Hir1" jqdatepicker ng-model="insured_gh51.hauler_Hir">
          </span>
          <span class="span3">
            <label class="control-label">Terminated</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Ter1" jqdatepicker ng-model="insured_gh51.hauler_Ter">
          </span>
          <span class="span3">
            <label class="control-label row-fluid">Quit</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Quit1" jqdatepicker ng-model="insured_gh51.hauler_Quit">
          </span>
          <span class="span3">
            <label class="control-label row-fluid">Others</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Oth1" jqdatepicker  ng-model="insured_gh51.hauler_Oth">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span4">
            <label class="control-label">8. Max hours driven per day</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Max1" ng-model="insured_gh51.hauler_Max">
          </span>
          <span class="span4">
            <label class="control-label">Per week</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Per1" ng-model="insured_gh51.hauler_Per">
          </span>
          <span class="span4">
            <label class="control-label">(5 day week or 7 day)</label>
            <input class="textinput" type="text" placeholder="" name="hauler_day1" pattern="[0-9]+" ng-model="insured_gh51.hauler_day" >
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">9. How are drivers compensated? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Hourly" name="hauler_comp1" ng-model="insured_gh51.hauler_comp">
              <span>Hourly &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Per trip" name="hauler_comp1" ng-model="insured_gh51.hauler_comp">
              <span>Per trip &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Salary" name="hauler_comp1" ng-model="insured_gh51.hauler_comp">
              <span>Salary &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" name="hauler_comp1" ng-model="insured_gh51.hauler_comp">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" name="hauler_Others1" ng-model="insured_gh51.hauler_Others">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">10. What hours of the day do you drivers operate?</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">10.A. &nbsp;6 AM to 2 PM</label>
            <input class="textinput  h5-active ui-state-valid" pattern="([1-9]{1}|(10)|[0-9]{2,2})" type="text" placeholder="%" name="hauler_hours1" ng-model="insured_gh51.hauler_hours">
          </span>
          <span class="span4">
            <label class="control-label">10.B. &nbsp;2 PM to 10 PM</label>
            <input class="textinput h5-active ui-state-valid" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="%" name="hauler_hour1" ng-model="insured_gh51.hauler_hour">
          </span>
          <span class="span4">
            <label class="control-label">10.C. &nbsp;10 PM to 6 AM</label>
            <input class="textinput h5-active ui-state-valid" type="text"  pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="%" name="hauler_hou1" ng-model="insured_gh51.hauler_hou">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">11. Where do your drivers sleep when they are on a trip? &nbsp;</label>
            <label class="radio pull-left radio-1">
              <input type="radio" value="At Home" name="hauler_sleep1" ng-model="insured_gh51.hauler_sleep">
              <span>At Home &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Motel" name="hauler_sleep1" ng-model="insured_gh51.hauler_sleep">
              <span>Motel &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="In the cab" name="hauler_sleep1" ng-model="insured_gh51.hauler_sleep">
              <span>In the cab &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" name="hauler_sleep1" ng-model="insured_gh51.hauler_sleep">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" name="hauler_hother1" ng-model="insured_gh51.hauler_hother">
            <label class="radio pull-left">
              <input type="radio" value="Hourly" name="hauler_sleep1" ng-model="insured_gh51.hauler_sleep">
              <span>Hourly &nbsp;</span>
            </label>
          </span>
        </div>
        <p>You must inform the company before hiring any new driver. You should have confirmation in writing regarding the acceptability of the driver by GHI.</p>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">12. Is your operation subject to time restraints when delivering the commodity? &nbsp;</label>
            <select name="hauler_rest1" class="span2" ng-model="insured_gh51.hauler_rest">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">13. If not hauling for others, will the vehicles be parked at a jobsite most of the day? &nbsp;</label>
            <select class="span2" name="hauler_roth1" ng-model="insured_gh51.hauler_roth">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">14. Do you have haul for others? &nbsp;</label>
            <select class="span2" name="hauler_haul1"  id="hauler_haul" ng-model="insured_gh51.hauler_haul">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid"  id="hauler_haul12" ng-if="insured_gh51.hauler_haul=='Yes'">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Is there any written agreement?</th>
                  <th>Copy attached?</th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                <tr >
                  <td>
                    <input class="textinput" type="text" placeholder="" name="haul_name1" ng-model="insured_gh51.haul_name">
                  </td>
                  <td>
                    <select  name="haul_agree1" class="span6" ng-model="insured_gh51.haul_agree">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                    <select name="haul_attach1" class="span6" id="haul_attach1" ng-model="insured_gh51.haul_attach" >
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
       
                  </td>
				  <td>
				  <input type="file" id="first_uploaded1" name="haul_attach_file1" ng-if="insured_gh51.haul_attach=='Yes'" value="{{insured_gh5.haul_attach_file}}">
					<a href="<?= base_url()?>uploads/docs/{{insured_gh5.haul_attach_file}}">{{insured_gh5.haul_attach_file}}</a>								
				  
				  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">15. Gross receipts - Estimate this year:&nbsp;</label>
          </span>
          <span class="span4">
            <label class="control-label">Last year:</label>
            <input class="textinput " type="text" placeholder="$" format name="haul_Last1" ng-model="insured_gh51.haul_Last">
          </span>
          <span class="span4">
            <label class="control-label">Next year:</label>
            <input class="textinput " type="text" placeholder="$" format name="haul_Next1" ng-model="insured_gh51.haul_Next">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">16. (i) Are any vehicles or equipment loaned, rented or leased to others? &nbsp;</label>
            <select name="haul_equi1" class="span2" ng-model="insured_gh51.haul_equi">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <label class="control-label pull-left">&nbsp; &nbsp; &nbsp;(ii) Do you lease, hire, rent or borrow any vehicles from others? &nbsp;</label>
        <label class="radio pull-left">
          <input type="radio" value="Yes" name="haul_rent1" ng-model="insured_gh51.haul_rent" ng-checked="insured_gh51.haul_rent">
          <span>Yes &nbsp;</span>
        </label>
        <label class="radio pull-left">
          <input type="radio" value="No" name="haul_rent1" ng-model="insured_gh51.haul_rent" ng-checked="insured_gh51.haul_rent">
          <span>No &nbsp;</span>
        </label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; What is the average length of the lease? &nbsp;</label>
            <input class="textinput" type="text" placeholder="" name="haul_lease1" ng-model="insured_gh51.haul_lease">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_agreement1" id="haul_agreement1" ng-model="insured_gh51.haul_agreement">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
      
         <!--   <input class="textinput pull-left" type="text" placeholder="Upload a copy of the agreement" name="">-->
			
			
			      <input type="file" id="first_uploaded2" name="haul_agree_file1" ng-if="insured_gh51.haul_agreement=='Yes'" class="up_file" value="{{insured_gh51.haul_agree_file}">
									
				  <a href="<?= base_url()?>uploads/docs/{{insured_gh5.haul_agree_file}}">{{insured_gh51.haul_agree_file}}</a>
			
			
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">17. What is the cost to lease, hire, rent or borrow vehicles? &nbsp;</label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span6">
            <label class="control-label">Per month</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_month1" format ng-model="insured_gh51.haul_month">
          </span>
          <span class="span6">
            <label class="control-label">Per year</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_year1" format ng-model="insured_gh51.haul_year">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">18. What type of vehicles do you lease, hire, rent or borrow? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="haul_bor1" ng-model="insured_gh51.haul_bor">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">19. Do you use owner/operators (sub-hauler)? &nbsp;</label>
            <select name="haul_ope1" class="span2"  id="haul_ope1" ng-model="insured_gh51.haul_ope">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh51.haul_ope=='Yes'">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp;If yes, is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_yagree1" id="haul_yagree" ng-model="insured_gh51.haul_yagree">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
                        
			      <input type="file"  name="haul_yagree_file1" ng-if="insured_gh51.haul_yagree=='Yes'" class="up_file" value="{{insured_gh51.haul_yagree_file}}">
									
				  <a href="<?= base_url()?>uploads/docs/{{insured_gh5.haul_yagree_file}}" ng-if="insured_gh51.haul_yagree=='yes'">{{insured_gh51.haul_yagree_file}}</a>
			
						
			 </span>
        </div>
            <div class="row-fluid">
              <span class="span9">
                <label class="control-label pull-left">20. Owner operator/sub-hauler have their own insurance? &nbsp;</label>
                <select  class="span2" name="haul_yins1" id="haul_yins1" ng-model="insured_gh51.haul_yins" >
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </span>
            </div>
            <div class="row-fluid" ng-if="insured_gh51.haul_yins=='Yes'">
              <span class="span9">
			  <input type="button"  class="btn btn-small" ng-click="add_row_g('datas_gh');" value="Add insurance"/>
            <!--    <button class="btn btn-small" onclick="add_insurance();">Add insurance</button>-->
              </span>
            </div>
         
        <table class="table" ng-if="insured_gh51.haul_yins=='Yes'" ng-show="show_type_gh==true">
          <thead>
            <tr>
              <th>Name</th>
              <th>Insurance Carrier</th>
              <th>Insurance Certificate Attached</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody id="add_new_table1">
            <tr ng-repeat="data in rows.datas_gh">
              <input class="textinput" type="hidden" placeholder="" name="add_ins_name1[]" value="{{data.add_ins_id}}" ng-model="data.add_ins_id">
              <td>
                <input class="textinput" type="text" placeholder="" name="add_ins_name1[]" ng-model="data.add_ins_name">
              </td>
              <td>
                <input class="textinput" type="text" placeholder="" name="add_ins_carrier1[]" ng-model="data.add_ins_carrier">
              </td>
              <td>
                <select name="ins_attach_y1[]" class="span6" id="ins_attach_y" ng-model="data.ins_attach_y" >
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </td>
              <td >

			  <input type="file" id="first_uploaded4" name="add_ins_attach1[]" value="{{insured_gh5.add_ins_attach}}" ng-if="data.ins_attach_y=='Yes'">
			  <a href="<?= base_url()?>uploads/docs/{{insured_gh5.add_ins_attach}}" ng-if="data.ins_attach_y=='Yes'">{{insured_gh5.add_ins_attach}}</a>
					
              
              </td>
              <td><!-- <i class="icon icon-remove"></i> -->
			  <a class="btn btn-mini btn-1 remove_ins_row" id="remove_phone_row" ng-click="remove_row_g($index,'datas_gh');"><i class="icon icon-remove"></i></a>
              </td>
            </tr>
		
          </tbody>
        </table>
		 
		<table class="table" >
		</table>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">21. Will owner/operators(sub-haulers) be scheduled on your policy? &nbsp;</label>
            <select name="ins_pol1" class="span2" id="ins_pol1" ng-model="insured_gh51.ins_pol">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh51.ins_pol=='Yes'">
          <span class="span12">
		   <input type="button" class="btn btn-small" ng-click="add_row_g('datas_p_gh')" value="Add owner/operator"/>
       <!--     <button class="btn btn-small" onclick="add_owner();">Add owner/operator</button>-->
          </span>
        </div>
        <div class="row-fluid"  ng-if="insured_gh51.ins_pol=='Yes'" ng-show="show_type_p_gh==true">
          <span class="span12">
            <table class="table" id="add_owner_table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Agreement Attached?</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody >
                <tr ng-repeat="data in rows.datas_p_gh">
                 <input class="textinput span11" type="hidden" placeholder="" name="add_owner_id1[]" value="{{data.add_owner_id}}" ng-model="data.add_owner_id">
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_owner_name1[]" ng-model="data.add_owner_name">
                  </td>
                  <td>
                    <select name="add_owner_y1[]" id="add_owner_y" class="span5" ng-model="data.add_owner_y">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
           			  
				   <input type="file" ng-if="data.add_owner_y=='Yes'" value="{{data.add_owner_attach}}">
				   <a href="<?= base_url()?>uploads/docs/{{data.add_owner_attach}}" ng-if="data.add_owner_attach=='Yes'">{{data.add_owner_attach}}</a>
							  
                  </td>
                  <td> <!--<i class="icon icon-remove"></i>-->
				  <a class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row" ng-click="remove_row_g($index,'datas_p_gh');"><i class="icon icon-remove"></i></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">22. Do you use subcontractors? &nbsp;</label>
            <select class="span2" name="subcon_y1" ng-model="insured_gh51.subcon_y" id="subcon_y1"> 
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div ng-if="insured_gh51.subcon_y=='Yes'">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">a. Who are your subcontractors? &nbsp;</label>
            <input class="textinput span9" type="text" placeholder="" name="subcon_name1" ng-model="insured_gh51.subcon_name">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">b. Are subcontractors required to provide Certificates of Insurance? &nbsp;</label>
            <select class="span2" name="subcon_cert1" ng-model="insured_gh51.subcon_cert">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span   class="span12">
            <label class="control-label pull-left">c. What limit of Auto Liability are subcontractors required to carry? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_lia1" ng-model="insured_gh51.subcon_lia">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">d. What job duties are performed by the subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_duties1" ng-model="insured_gh51.subcon_duties">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">e. What is your cost to use subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_cost1" ng-model="insured_gh51.subcon_cost">
          </span>
        </div>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">23. At any time will your employees, subcontractors, or owner/operators lease vehicles on your behalf? &nbsp;</label>
            <select class="span2" name="subcon_emp_y1" ng-model="insured_gh51.subcon_emp_y" id="subcon_emp_y1">
            
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="subcon_emp_y12" ng-if="insured_gh51.subcon_emp_y=='Yes'">
          <span class="span12">
		  <input type="button" class="btn btn-small" ng-click="add_row_g('datas_e_gh')"  value="Add"/>
  <!--          <button class="btn btn-small" onclick="add_subcontract();">Add</button>-->
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh51.subcon_emp_y=='Yes'" ng-show="show_type_e_gh==true">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Type of Vehicle</th>
                  <th>Lease agreement attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody >
                <tr ng-repeat="data in rows.datas_e_gh">
                <input class="textinput" type="hidden" placeholder="" name="subcon_add_id1[]" ng-model="data.subcon_add_id" value="{{data.subcon_add_id}}">
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_name1[]" ng-model="data.subcon_add_name">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_vehicle1[]" ng-model="data.subcon_add_vehicle">
                  </td>
                  <td>
                    <select name="subcon_add_y1[]" class="span6" id="subcon_add_y" ng-model="data.subcon_add_y">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                   <input type="file" ng-if="data.subcon_add_y=='Yes'" ng-if="data.subcon_add_y=='Yes'" name="subcon_add_attach[]" value="{{data.subcon_add_attach}}">
				   <a href="<?= base_url()?>uploads/docs/{{data.subcon_add_attach}}" ng-if="data.subcon_add_y=='Yes'">{{data.subcon_add_attach}}</a>
					
                  </td>
                     <td><!-- <i class="icon icon-remove"></i> -->
				       <a class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row" ng-click="remove_row_g($index,'datas_e_gh');"><i class="icon icon-remove"></i></a>
                 	 </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">24. Do any employees, subcontractors or sub-haulers use their vehicles while conducting your business? &nbsp;</label>
           <select class="span2" name="sub_haul_y1" ng-model="insured_gh51.sub_haul_y"  id="sub_haul_y1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y11" ng-if="insured_gh51.sub_haul_y=='Yes'">
          <span class="span12">
		  <input type="button" class="btn btn-small" ng-click="add_row_g('datas_s_gh')"  value="Add" />
        <!--    <button class="btn btn-small" onclick="add_conduct1();">Add</button>-->
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y12" ng-if="insured_gh51.sub_haul_y=='Yes'" ng-show="show_type_s_gh==true">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Insurance Carrier</th>
                  <th class="th-1">Limit of Liability Ins.</th>
                  <th class="th-1">Insurance Certificate Attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody >
                <tr ng-repeat="data in rows.datas_s_gh">
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="sub_haul_add_name1[]" ng-model="data.sub_haul_add_name">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="sub_haul_add_carrier1[]" ng-model="data.sub_haul_add_carrier">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="$" format name="sub_haul_add_limit1[]" ng-model="data.sub_haul_add_limit">
                  </td>
                  <td>
                    <select name="sub_haul_add_y1[]" class="span6" id="sub_haul_add_y" ng-model="data.sub_haul_add_y">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
         
                  <input type="file" name="sub_haul_add_attach1[]" id="first_uploaded7" ng-if="data.sub_haul_add_y=='Yes'" value="{{data.sub_haul_add_attach}}">
				   <a href="<?= base_url()?>uploads/docs/{{data.sub_haul_add_attach}}" ng-if="data.sub_haul_add_y=='Yes'">{{data.sub_haul_add_attach}}</a>
					  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				     <a class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row" ng-click="remove_row_g($index,'datas_s_gh');"><i class="icon icon-remove"></i></a>
                 	  
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">25. Do you understand that we may audit your records? &nbsp;</label>
            <select class="span2" name="audit_y1" ng-model="insured_gh51.audit_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">26. Do any of your family members use the vehicles? &nbsp;</label>
            <select class="span2" name="vehicles_y1" id="vehicles_y" ng-model="insured_gh51.vehicles_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh51.vehicles_y=='Yes'">
          <span class="span12">
		  <input type="button" ng-click="add_row_g('datas_y_gh')" class="btn btn-small" value="Add"/>
          <!--  <button class="btn btn-small" onclick="add_members();">Add</button>-->
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh51.vehicles_y=='Yes'" if-show="show_type_y_gh==true">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Relationship</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_members_row1">
                <tr ng-repeat="data in rows.datas_y_gh">
                  <input class="textinput span11" type="text" placeholder="" name="add_mem_id1[]" ng-model="data.add_mem_id" value="{{data.add_mem_id}}">
                
                  
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_name1[]" ng-model="data.add_mem_name">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_rel1[]" ng-model="data.add_mem_rel">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				  <a class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row" ng-click="remove_row_g($index,'datas_y_gh');"><i class="icon icon-remove"></i></a>
                 	  
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">27. Do you allow passengers to ride in your vehicles? &nbsp;</label>
            <select class="span2" name="ride_y1" ng-model="insured_gh51.ride_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">28. Are you familiar with the U.S Department of Transportation driver requirements? &nbsp;</label>
            <select class="span2" name="driver_y1" ng-model="insured_gh51.driver_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">29. (i) Do you mantain driver activity files? &nbsp;</label>
            <select class="span2" name="activity_y1" ng-model="insured_gh51.activity_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">

          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iI) Do you review current MVRs on all drivers prior to hiring? &nbsp;</label>
            <select class="span2" name="prior_y1" ng-model="insured_gh51.prior_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iii) Is there a formal driver hiring procedure? &nbsp;</label>
            <label class="radio pull-left" >
              <input type="radio" value="Yes" name="procedure_y1" ng-model="insured_gh51.procedure_y"  >
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" name="procedure_y1" ng-model="insured_gh51.procedure_y" >
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iv) Drug Screening? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" name="Drug_y1" ng-model="insured_gh51.Drug_y" ng-checked="insured_gh51.Drug_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" name="Drug_y1" ng-model="insured_gh5.Drug_y1" ng-checked="insured_gh51.Drug_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(v) If you have a formal driver hiring/training program, provide a copy with this application? &nbsp;</label>
                <input type="file" id="first_uploaded8" name="hiring_file1" value="{{insured_gh51.hiring_file}}">
                  <a href="<?= base_url()?>uploads/docs/{{insured_gh51.hiring_file}}" >{{insured_gh51.hiring_file}}</a>
				               		
		     </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">30. Are all drivers employees? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" ng-model="insured_gh51.employees_y"  name="employees_y1" id="employees_11">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" ng-model="insured_gh51.employees_y"  name="employees_y1" id="employees_12">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh51.employees_y=='No'">
          <span class="span12">
            <label class="control-label pull-left">If no, please explain &nbsp;</label>
            <input class="textinput span10" type="text" placeholder="" name="explain_no">
          </span>
        </div>
	  <span id="yes_employee1" ng-if="insured_gh51.employees_y=='Yes'">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">If yes, list below &nbsp;</label>
            <input type="button" class="btn btn-small" ng-click="add_row_g('datas_emp_gh')" value="Add">
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh51.employees_y=='Yes'" ng-show="show_type_emp_gh==true">
          <span class="span12">
            <table class="table" >
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Length of employment</th>
                  <th class="th-1">Years/Months</th>
                  <th></th>
                </tr>
              </thead>
              <tbody >
                <tr ng-repeat="data in rows.datas_emp_gh">
                 <input class="textinput span11" type="text" placeholder="" name="add_list_id1[]" ng-model="data.add_list_id" value="{{data.add_list_id}}">
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_name1[]" ng-model="data.add_list_name">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_length1[]" ng-model="data.add_list_length">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_year1[]" ng-model="data.add_list_year">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <a class="btn btn-mini btn-1 remove_phone_row" ng-click="remove_row_g($index,'datas_emp_gh')" id="remove_phone_row"><i class="icon icon-remove"></i></a>
				   </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
		</span>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">31. Is there a formal safety program? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" ng-model="insured_gh51.safety_y" id="safety_y" value="Yes" name="safety_y1">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" ng-model="insured_gh51.safety_y" id="safety_y1" value="No" name="safety_y1">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" ng-if="insured_gh5.safety_y=='Yes'">
          <span class="span12">
            <label class="control-label pull-left">If yes, provide details or a copy:</label>
            	  <input type="file" name="safety_attach_file1" id="first_uploaded16"  value="{{insured_gh51.safety_attach_file}}">
				  <a href="<?= base_url()?>uploads/docs/{{insured_gh51.safety_attach_file}}" >{{insured_gh51.safety_attach_file}}</a>
				   				
						
            <textarea class="span12 span12-1" placeholder="" name="details_y1" > {{insured_gh51.details_y}} </textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">32. Provide details of your maintenance program:</label>
            <textarea class="span10 span12-1" placeholder="" name="program_detail1" >{{insured_gh51.program_detail}}</textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">33. Do you agree to screen and report all potential operators immediately upon hiring before giving them a load? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" name="load_y1" ng-model="insured_gh51.load_y">
              <span>Yes &nbsp;</span>
            </label>

            <label class="radio pull-left">
              <input type="radio" value="No" name="load_y1" ng-model="insured_gh51.load_y">
              <span>No</span>
            </label>
          </span>
        </div>
		</div>
    
    </div>
    
   </div>
<!-- Insured Application Section End Ashvin Patel 26/April/2015-->
</div>
   <div class="row-fluid top20">
   <input type="button" class="btn-primary pull-right" onclick="pdf_click('ins_divs');" value="Pdf" name="save"/>&nbsp;&nbsp;
    <input type="button" class="btn-primary pull-right" onclick="sec_info_save('insured_info');" value="Save" name="save"/>
  </div>
</fieldset>


<script>

$( ".zip_code_i" ).change(function(event) {
  var city=$('#in_city').val();
  var state=$('#in_state').val();
  var county=$('#in_county').val();  
  var zip_attr='class="span12" id="in_zip"  ng-model="insured_info.in_zip"';
  var zip_name='in_zip';
  var baseurl = $("#baseurls").val();
	$.ajax({
		type: "POST",
		url: baseurl+"index.php/quote/getZipcode", 
		data:"city="+city+"&state="+state+"&county="+county+"&attr="+zip_attr+"&name="+zip_name+"",	
		success: function(json) 
		{	
		   $('#in_zip_span').html(''); 	   
		   $('#in_zip_span').html(json); 
		 	 
		}
	});

    var county_attr='class="span12" onChange="getZipCodes_i(this.value)" id="in_county"  ng-model="insured_info.in_county"';
	var county_name='in_county';
	var baseurl = $("#baseurls").val();
	$.ajax({
		type: "POST",
		url: baseurl+"index.php/quote/getcounty", 
		data:"city="+city+"&state="+state+"&attr="+county_attr+"&name="+county_name+"",
		success: function(json) 
		{  
		   if(json.toLowerCase().indexOf('select')>=0){
		   $('#in_county_span').html(''); 	   
		   $('#in_county_span').html(json); 
		   }else{
			 if(city!='' && state!=''){
			  event.stopPropagation();
			  alert('No matches found');
			  }
		      
			}
		 
		}
	});
  
});
</script>