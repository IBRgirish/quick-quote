<?php $this->load->view('includes/head_style_script')?>
<?php 
if(isset($losses)){
	if(is_array($losses)){		
	  ?>
	  <table class="table">
	  
	  <tr>
		<th colspan="3" >Period of losses</th>
		<th rowspan="2" >Amount</th>
		<th rowspan="2" >Company</th>
        <th rowspan="2" >General <br /> Agent</th>
        <th rowspan="2" >Date of Loss</th>
	  </tr>
	  <tr colspan="1" >
      	<th>Losse</th>
		<th>From (mm/dd/yyyy)</th>
		<th>To (mm/dd/yyyy)</th>
	  </tr> 
      <?php
      foreach($losses as $key => $losse){
	  ?>
	  <tr>
      	  <td>Losse #<?= $key+1; ?></td>
		  <td><?php echo date('m/d/Y', strtotime($losse->losses_from_date)); ?></td>
		  <td><?php echo date('m/d/Y', strtotime($losse->losses_to_date)); ?></td>
		  <td><?php echo $losse->losses_amount; ?></td>
		  <td><?php echo $losse->losses_company; ?></td>
		  <td><?php echo $losse->losses_general_agent; ?></td>
		  <td><?php echo date('m/d/Y', strtotime($losse->date_of_loss)); ?></td>
	  </tr>
	  <?php
	  }
	}
}
?>