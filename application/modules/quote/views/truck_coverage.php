<div class="row-fluid bottom5 truck_box" ng-repeat="truck in rows.trucks" ng-init="truckIndex=$index" ng-if="coverages.truck_no>0">
	<div class="well">
      	<div class="row-fluid heading_border">
        	<div class="row-fluid ">
            	<label class="heading_vehicles">
                  <div class="row-fluid img_1" >
                   <img src="<?= base_url('images/truck.jpg'); ?>" width="70" > <span style="color: #176CB3;font-weight: bolder;">This for the Truck  #{{$index+1}}</span>
                <h6 style="margin: -6% 0% 0% 35%; width: 100%;color: #377814;">This is only one vehicle information</h6>
                  </div>
           
                </label>
                 <div class="pull-right icon_show_b">
                    <span class="green" id="show-cov_t{{truckIndex}}" title="Show" style="display:none" ng-click="cov_section_t('show',truckIndex)"> <img src="<?= base_url()?>/images/hide.png"></span>
                    <span class="red" id="hide-cov_t{{truckIndex}}" title="Hide" ng-click="cov_section_t('hide',truckIndex)"> <img src="<?= base_url()?>/images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                 </div>
                
                 <div class="row-fluid pull-right">
            	 <label>Vehicle Info <?php if($underwriter=='underwriter') { ?><img ng-if="truck.vh_salvage =='Yes'" src="<?php echo base_url()?>images/flag-red_24x24.png" /><?php } ?></label>
            	
                </div>
               
            </div>
           
        </div>
      <div class="row-fluid" id="cov_div_t{{truckIndex}}">
        <?php $vhit = 0;?>
      	<div class="row-fluid">  	
          <div class="span1 span_7dot6"> 
          <input type="hidden" value="truck" name="vehicle_type[]"/>     
          <input type="hidden" value="{{truck.vehicle_id}}" ng-model="truck.vehicle_id" name="vehicle_id[]"/>   
            <label class="label_vehicle_year_vh"><span class="numbers"><?= ++$vhit; ?> </span>Vehicle Year</label>          
            <select class="span12 myclass" name="vehicle_year_vh[]" id="vehicle_year_vh" ng-model="truck.vehicle_year_vh">
              <?php 
              $year=$vehicle->vehicle_year;
              $current_year = date("Y");
			   echo "<option value=''>Select</option>";
              for($i = 1985; $i <= $current_year + 1; $i++){
				 
                echo "<option value='$i'>$i</option>";
              } 
              ?>
            </select>
          </div>
          <div class="span2 span_7dot6">
            <label class="label_make_truck"><span class="numbers"><?= ++$vhit; ?> </span> Make</label>
            <?php //echo $make_model_truck;
			//echo getMakeModelVehicles('make_truck[]', 'class="span12" ng-model="truck.make_truck" ng-change="make_pic_vh(truck.make_truck,44,truckIndex)"','',30,'');
            echo getMakeModelVehicles('make_truck[]', 'class="span12" ng-model="truck.make_truck"','',30,'');?>
            <input type="text" maxlength="19" class="span12 myclass" name="make_truck_other[]" id="vin_vh" ng-if="truck.make_truck=='Other'" ng-model="truck.make_truck_other">
            <input type="text" maxlength="19" class="span12 myclass" name="make_truck_other[]" id="vin_vh" ng-if="truck.make_truck=='other'" ng-model="truck.make_truck_other">
          </div>
           <div class="span2 span_7dot6">
            <label class="label_model_tuck"><span class="numbers"><?= ++$vhit; ?> </span> Model</label>
                        <?php //echo $make_model_truck;
			echo getMakeModelVehicles('model_tuck[]', 'class="span12" ng-model="truck.model_tuck"','',31,'');?>
            <input type="text" maxlength="19" class="span12 myclass" name="model_truck_other[]" id="vin_vh" ng-if="truck.model_tuck=='Other'" ng-model="truck.model_tuck_other">
            <input type="text" maxlength="19" class="span12 myclass" name="model_truck_other[]" id="vin_vh" ng-if="truck.model_tuck=='other'" ng-model="truck.model_tuck_other">
          </div>
           <div class="span2 span_7dot6">
            <label class="label_model_color"><span class="numbers"><?= ++$vhit; ?> </span> color</label>
                        <?php //echo $make_model_truck;
			echo getMakeModelVehicles('model_color[]', 'class="span12" ng-model="truck.model_color"','',48,'');?>
             <input type="text" maxlength="19" class="span12 myclass" name="model_color_other[]" id="vin_vh" ng-if="truck.model_color=='Other'" ng-model="truck.model_color_other">
            <input type="text" maxlength="19" class="span12 myclass" name="model_color_other[]" id="vin_vh" ng-if="truck.model_color=='other'" ng-model="truck.model_color_other">
          </div>
          <div class="span2 span_11 lightblue">
            <label class="label_gvw_vh"><span class="numbers"><?= ++$vhit; ?> </span>GVW</label>
            <select class="span12 myclass" name="gvw_vh[]" id="gvw_vh" ng-model="truck.gvw_vh">
              <option value="">Select</option>
              <option value="0-10,000 Lb">0-10,000 Lb</option>
              <option value="10,001-26,000 Lb">10,001-26,000 Lb</option>
              <option value="26,001-40,000 Lb">26,001-40,000 Lb</option>
              <option value="40,001-80,000 Lb">40,001-80,000 Lb</option>
              <option value="Over 80,000 Lb">Over 80,000 Lb</option>
            </select>
          </div>
          <div class="span2 lightblue span_15">
            <label class="label_vin_vh"><span class="numbers truck_number_vehicle"><?= ++$vhit; ?> </span>VIN</label>
            <input type="text" maxlength="19" class="span12 myclass" name="vin_vh[]" id="vin_vh" ng-model="truck.vin_vh">
          </div>
          <!--<div class="span2 lightblue">
            <label>Special req.</label>
            <input type="text" class="span12 myclass" name="value_amount_vh[]" id="value_amount_vh" value="">
          </div>-->
          <div class="span2 span_9">
              <label class="label_truck_number_of_axles"><span class="numbers truck_number_vehicle"><?= ++$vhit; ?> </span>Number of Axles</label>
              <?php //echo $truck_number_of_axles;
			        echo num_of_axle('class="span9" ng-model="truck.truck_number_of_axles" ng-if="truck.truck_number_of_axles" ','truck_number_of_axles[]');
			        echo num_of_axle('class="span9" ng-model="truck.truck_number_of_axles" ng-if="!truck.truck_number_of_axles" ng-init="truck.truck_number_of_axles=&quot;2 Axle&quot;"','truck_number_of_axles[]');
			    ?>             
          </div>
          <div class="span1">
          	<label class="label_vh_salvage"><span class="numbers truck_number_vehicle"><?= ++$vhit; ?> </span>Salvage</label>
            <select name="vh_salvage[]" class="span12" ng-model="truck.vh_salvage">
            	<option value="">Select</option>
                <option value="No" >No</option>
                <option value="Yes" >Yes</option>
            </select>
          </div>
          <div class="span2 span_11 lightblue ">
            <label class="label_truck_radius_of_operation"><span class="numbers truck_number_vehicle"><?= ++$vhit; ?> </span>Radius of Operation</label>            
            <select class="span12 rofo truck_radius_of_operation myclass" name="truck_radius_of_operation[]" ng-model="truck.truck_radius_of_operation" id="radius_of_operation"  ng-change="radius_ope(&quot;trucks&quot;,truckIndex,truck.truck_radius_of_operation)">
                <option value="">Select</option>
                <option value="100" >0-100 miles</option>
                <option value="300"  >101-500 miles</option>           	
                <option value="501"  >501+ miles</option>
                <option value="other"  >Other</option>
            </select>
            <input type="text" name="radius_of_operation_other[]" ng-if="truck.truck_radius_of_operation=='other'" ng-model="truck.radius_of_operation_other" class="span12"/>   
          </div>
          <div class="span2 span_10">
          	<label class="label_vh_nature_of_business"><span class="numbers truck_number_vehicle"><?= ++$vhit; ?> </span>Nature of business</label>
            <input type="text" name="vh_nature_of_business[]" ng-model="truck.vh_nature_of_business" class="span12 nature_bus">
          </div>          
        </div>
        <div class="row-fluid">
          <span id="vh_make_{{truckIndex}}" class="span6">
               <input type="hidden" name="img_name_make[]" id="img_name_make_{{truckIndex}}" ng-if="truck.img_name_make" value="{{truck.img_name_make}}" ng-model="truck.img_name_make"/>
               <img src="<?php echo base_url();?>uploads/file/{{truck.img_name_make}}" ng-if="truck.img_name_make"/>
         </span>
         
      <span class="mapping" id="mapping_{{truckIndex}}" class="span6" <?php if(!$this->session->userdata('underwriter_id')) { echo 'style="display:none"'; }?>>
             <!--<span class="">
             <label>Show Google Map</label>
              <select ng-change="map_address(truckIndex,map)" id="google_map_id_{{truckIndex}}" ng-model="map"class="span10" ng-init="map='Yes'">
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
               
             </span>-->
             <span class="pull-right icon_show_m">
                    <span class="green" id="show-cov_map{{truckIndex}}" title="Show" style="display:none" ng-click="cov_section_map('show',truckIndex)"> <img src="<?= base_url()?>/images/max.png"></span>
                    <span class="red" id="hide-cov_map{{truckIndex}}" title="Hide" ng-click="cov_section_map('hide',truckIndex)"> <img src="<?= base_url()?>/images/min.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                 </span>
              <div id="map-address-{{truckIndex}}" class="Map" style="width:500px;height:380px;"></div>
               
          </span>
         
        </div>
        <div class="row-fluid">
        	<div class="span2 span_8 truck-parent">
              <label class="label_truck_trailer_owned_vehicle"><span class="numbers truck_number_ownvehicle"><?= ++$vhit; ?> </span>Owned Vehicle</label>
              <select name="truck_trailer_owned_vehicle[]" id="truck_owned_vehicle" ng-model="truck.truck_trailer_owned_vehicle"  class="span7">
                  <option>Yes</option>
                  <option>No</option>
              </select>
          	</div>
            <div class="span2" ng-if="truck.truck_trailer_owned_vehicle=='No'">
          
            	<label >&nbsp;</label>
            	<label class="span12">Please provide the owner</label>
              
            </div>
             <div  ng-if="truck.truck_trailer_owned_vehicle=='No'">
               <div class="pull-right icon_show_c">
                    <span class="green" id="show-cov_tv{{truckIndex}}" title="Show" style="display:none" ng-click="cov_section_tv('show',truckIndex)"> <img src="<?= base_url()?>/images/hide.png"></span>
                    <span class="red" id="hide-cov_tv{{truckIndex}}" title="Hide" ng-click="cov_section_tv('hide',truckIndex)"> <img src="<?= base_url()?>/images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                 </div>
             </div>
        </div>
        <div class="row-fluid" >
         <div class="row-fluid" ng-if="truck.truck_trailer_owned_vehicle=='No'" id="cov_div_tv{{truckIndex}}">
           
         <div class="row-fluid bottom5" ng-repeat="lessor in truck.lessor" ng-init="lessorIndex=$index">     
               <input type="hidden" value="truck" name="vh_types[]"/>  
            	<div class="well">
                 <div class="row-fluid">
            	  <div class="span6">
                  <label>Sequence</label>
                 <!-- <input type="text" name="vh_owner_sq" class="span1" ng-change="add_loss_row('trucks', $index)" ng-model="truck.lessor_n" >-->
                  <input type="text" name="vh_owner_sq" class="span1"  value="{{$index+1}}" readonly>
                   <label>Lessor Type</label>                   
                   <?php echo getCatDropDownss('lessor_type[]', 'ng-model="lessor.lessor_type" class="span2"', 39,'') ?>
                </div>
                <div class="span6 pull-right">
                    	<label class="span12">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_loss_row('trucks', truckIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a> 
                		<a href="javscript:;" ng-click="remove_type('trucks', truckIndex,$index)">
                        <img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
               </div>
               <?php $vhitow = 0; ?>
            	<div class="row-fluid">
                        <input type="hidden" name="vh_ls_id[]" class="span12" ng-model="lessor.vh_ls_id" value="{{lessor.vh_ls_id}}">                
                	<div class="span2 span_10">
                    	
                    	<label class="label_vh_ls_name"><span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>{{lessor.lessor_type}} name</label>
                        <input type="text" name="vh_ls_name[]" class="span12" ng-model="lessor.vh_ls_name">
                    </div>
                    <div class="span2 span_8">
                    	
                    	<label class="label_vh_ls_m_name"><span class=""></span>Middle name</label>
                        <input type="text" name="vh_ls_m_name[]" class="span12" ng-model="lessor.vh_ls_m_name">
                    </div>
                    <div class="span2 span_10">
                    	
                    	<label class="label_vh_ls_l_name"><span class=""></span>Last name</label>
                        <input type="text" name="vh_ls_l_name[]" class="span12" ng-model="lessor.vh_ls_l_name">
                    </div>
                    <div class="span3 span_10">
                    	
                    	<label class="label_vh_ls_dba"><span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>{{lessor.lessor_type}} DBA</label>
                        <input type="text" name="vh_ls_dba[]" class="span12" ng-model="lessor.vh_ls_dba">
                    </div>
                    <div class="span3 span_15">
                    
                    	<label class="label_vh_ls_address"><span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Street</label>
                        <input type="text" name="vh_ls_address[]" class="span12" ng-model="lessor.vh_ls_address">
                    </div>
                    <div class="span1 span_8">
                    
                    	<label class="label_vh_ls_city"><span class="numbers_child_child "></span>City</label>
                        <input type="text" name="vh_ls_city[]" class="span12" ng-model="lessor.vh_ls_city" ng-change="zip_code_ls('1')" id="1_vh_ls_city">
                    </div>
                    <div class="span1">
                   
                    	<label> <span class="numbers_child_child "></span>State</label>
                        <?php  echo get_state_dropdown('vh_ls_state[]','', $attr='class="span12" ng-model="lessor.vh_ls_state" id="1_vh_ls_state" ng-change="zip_code_ls(&quot;1&quot;)"') ?>
                        
                    </div>
                    <div class="span2 span_10">
                  
                    	<label class="label_vh_ls_county"> <span class="numbers_child_child "></span>County</label>
                        <span id="1_vh_ls_county_span">
                        <input type="text" name="vh_ls_county[]" class="span12" ng-model="lessor.vh_ls_county" id="1_vh_ls_county">
                        </span>
                    </div>
                    <div class="span1">
                   
                    	<label class="label_vh_ls_zip"> <span class="numbers_child_child "></span>Zip</label>
                        <span id="1_vh_ls_zip_span">
                        <input type="text" name="vh_ls_zip[]" class="span12" ng-model="lessor.vh_ls_zip" id="1_vh_ls_zip">
                        </span>
                    </div>
                    <div class="span1">
                   
                    	<label class="label_vh_ls_country"> <span class="numbers_child_child"></span>Country</label>
                         <?php echo getCountryDropDown1('vh_ls_country[]', 'class="span12" id="person_pri_country" ng-model="lessor.vh_ls_country"','',''); ?>
                         
                    </div>
                </div>
                <!--<div class="row-fluid">
                	
                </div>-->
                <div class="row-fluid">
                	<div class="span2 span_8" style="width: 7.7%!important;">
                   
                    	<label class="label_vh_ls_ph_type"> <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Phone type</label>                       
                        <?php echo getCatlogValue('vh_ls_ph_type[]', 'class="span9"  ng-model="lessor.vh_ls_ph_type"','',35,''); ?>
                           
                    </div>
                    <div class="span2 span_10">
                   
                    	<label class="label_vh_ls_country_code"><span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Country code</label>
                         <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
					   <?php
                       $selected =  '1'; 
					   echo  getCountryCodeDropDown($name='vh_ls_country_code[]', $att='class="span12"  ng-model="lessor.vh_ls_country_code"', 'code', $selected); //'.$highlighted.'
                     ?>
                        <?php //echo getCatlogValue('vh_ls_country_code[]', 'class="span12"  ng-model="lessor.vh_ls_country_code"','',36,''); ?>
                        
                    </div>
                    <div class="span2 span_7dot6">
                   
                    	<label class="label_vh_ls_areacode"> <span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Area code</label>
                        <input type="text" name="vh_ls_areacode[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Area" ng-model="lessor.vh_ls_areacode">
                    </div> 
                    <div class="span1">
                   
                    	<label class="label_vh_ls_prefix"> <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Prefix</label>
                        <input type="text" name="vh_ls_prefix[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Prefix" ng-model="lessor.vh_ls_prefix">
                    </div>
                    <div class="span1">
                   
                    	<label class="label_vh_ls_suffixe"> <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Suffix</label>
                        <input type="text" name="vh_ls_suffixe[]" class="span8" pattern="[0-9]+" maxlength="4" size="4" placeholder="Suffix" ng-model="lessor.vh_ls_suffixe">
                    </div>
                    <div class="span2 span_7dot6">
                   
                    	<label class="label_vh_ls_ext"> <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Extension</label>
                         <input type="text" name="vh_ls_ext[]" class="span8" pattern="[0-9]+" maxlength="4" size="4" placeholder="Ext" ng-model="lessor.vh_ls_ext">
                     </div> 
                    <div class="span2 span_12">
                   
                    	<label class="label_vh_ls_ph_pri_sequence"> <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Priority sequence</label>
                        <?php echo getCatlogValue('vh_ls_ph_pri_sequence[]', 'class="span6"  ng-model="lessor.pri_country"','',37,''); ?>
                     </div>   
                       <input type="hidden" value="truck" name="vh_garage_type[]"/>
                <div class="row-fluid" ng-repeat="email in lessor.emails">
                <input type="hidden" ng-model="email.email_id" name="vh_ls_email_id_{{lessorIndex}}[]" value="{{email.email_id}}"/>
                	<div class="span2 span_9">
                     
                    	<label class="label_vh_ls_email_type_{{lessorIndex}}"><span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Email Type</label>
                        <?php echo getCatlogValue('vh_ls_email_type_{{lessorIndex}}[]', 'class="span9"  ng-model="email.email_type"','',34,''); ?>
                     
                    </div>
                    <div class="span2" style="width: 10.5%!important;">
                    
                    	<label class="label_vh_ls_email_{{lessorIndex}}"> <span class="numbers_child_child"><?= $vhit.'.'.++$vhitow; ?> </span>Email Address</label>
                        <input type="text" name="vh_ls_email_{{lessorIndex}}[]" class="span12" ng-model="email.email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$">
                    </div>
                    <div class="span2 span_12">
                   
                    	<label class="label_vh_ls_em_pri_sequence_{{lessorIndex}}">  <span class="numbers_child_child "><?= $vhit.'.'.++$vhitow; ?> </span>Priority sequence</label>
                        <?php echo getCatlogValue('vh_ls_em_pri_sequence_{{lessorIndex}}[]', 'class="span5"  ng-model="email.priority_email"','',37,''); ?>
                    </div>
                    <input type="hidden" value="truck" name="vh_em_type[]"/>
                    <div class="span1">
                    
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_email('trucks', truckIndex, lessorIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                		<a href="javscript:;" ng-click="remove_email('trucks', truckIndex, lessorIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
                </div>                 
                </div>
              
              <!--  <div class="fluid-row"> 
               	
                  <label> <span class="numbers_child_child"> </span>Do you have any fax number?</label>
                    <select name="in_truck_fax[]" class="span1" ng-model="lessor.in_truck_fax">
                      <option>Yes</option>
                   <option selected="selected">No</option>
                 </select>
                </div>-->
               <div class="fluid-row" > 
                <div class="row-fluid" ng-repeat="fax in lessor.faxs">
                 <input type="hidden" ng-model="fax.fax_id" name="vh_fx_id_{{lessorIndex}}[]" value="{{fax.fax_id}}"/>
                	<div class="span2 span_10">
                   
                    	<label class="vh_ls_fx_type_{{lessorIndex}}"> <span class="numbers_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Fax Type</label>
                         <?php echo getCatlogValue('vh_ls_fx_type_{{lessorIndex}}[]', 'class="span12"  ng-model="fax.fax_type"','',38,''); ?>
                     
                    </div>
                    <div class="span2 span_10">
                   		<label class="vh_ls_fx_country_code_{{lessorIndex}}"><span class="numbers_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Country code</label>
                        <?php //echo getCatlogValue('vh_ls_fx_country_code[]', 'class="span12"  ng-model="fax.code_type_fax"','',36,''); ?>
                        <?= isset($vncc_obj_data) ? $vncc_obj_data : ''?>
					   <?php
                       $selected =  '1'; //isset($voice_no[$i]->country_code) ? decrypt($voice_no[$i]->country_code) :
                      // $highlighted = isset($vncc_highlighted) ? $vncc_highlighted : '';		
                         getCountryCodeDropDown($name='vh_ls_fx_country_code_{{lessorIndex}}[]', $att='class="span12"  ng-model="fax.code_type_fax" ', 'code', $selected); //'.$highlighted.'
                     ?>
                    </div>
                    <div class="span2 span_8">
 						
                    	<label class="label_vh_ls_fx_areacode_{{lessorIndex}}"><span class="numbers_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Area code</label>
                        <input type="text" name="vh_ls_fx_areacode_{{lessorIndex}}[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Area" ng-model="fax.area_code_fax">
                    </div>
                    <div class="span1">
                    
                    	<label class="label_vh_ls_fx_prefix_{{lessorIndex}}"><span class="numbers_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Prefix</label>
                        <input type="text" name="vh_ls_fx_prefix_{{lessorIndex}}[]" class="span8" pattern="[0-9]+" maxlength="3" size="3" placeholder="Prefix" ng-model="fax.prefix_fax">
                    </div>
                    <div class="span1">
                   
                    	<label class="label_vh_ls_fx_suffix_{{lessorIndex}}"><span class="numbers_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Suffix</label>
                        <input type="text" name="vh_ls_fx_suffix_{{lessorIndex}}[]" class="span9" pattern="[0-9]+" maxlength="4" size="4" placeholder="Suffix" ng-model="fax.suffix_fax">
                    </div>
                    <div class="span2 span_15">
                  
                    	<label class="label_vh_ls_fx_pri_sequence_{{lessorIndex}}"><span class="numbers_child truck_fax_number_child"><?= $vhit.'.'.++$vhitow; ?></span> Priority sequence</label>
                        <?php echo getCatlogValue('vh_ls_fx_pri_sequence_{{lessorIndex}}[]', 'class="span6"  ng-model="fax.country_fax"','',37,''); ?>
                    
                    </div>
                    <input type="hidden" value="truck" name="vh_fx_type[]"/>
                    <div class="span1">
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_fax('trucks', truckIndex, lessorIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                		<a href="javscript:;" ng-click="remove_fax('trucks', truckIndex, lessorIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
                </div>
                </div>
              </div>
            </div>
            
            <script>
                setTimeout(function(){
		
				  $('input').blur(function() {	
					 if($(this).val()!=''){	
					  $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
					 }else{
					  $(this).removeAttr('style');
					  }
				   });
				
				 $('select').change(function() {	
				
					if($(this).val()!=''){
					  $(this).css({'border-color':'rgba(177, 72, 173, 0.8)','box-shadow':'inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(236, 88, 198, 0.6)'});
				   }else{
					   $(this).removeAttr('style');
					 } });
		 },200);
         </script>
           </div>
          
         <?php /*?> <!--  <div class="row-fluid top20">
            	<div class="row-fluid">
                <span>
                	<label><span class="numbers truck_equipment_numbers"><?= ++$vhit; ?> </span>Do you pull any equipment with this vehicle?</label>
                    <select name="vh_equipment_pull[]" class="span1 no_margin" ng-model="truck.vh_equipment_pull">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    </span>
                <span  ng-if="truck.vh_equipment_pull=='Yes'"> 
                 
                    	<label class="span12 no_margin">&nbsp;</label>
                    	<a href="javscript:;" ng-click="add_eqp('trucks', truckIndex)" ng-if="$index==0"><img src="<?= base_url('images/add.png'); ?>"></a>
                        <div class="pull-right icon_show_ce">
                         <span class="green" id="show-cov_tve{{truckIndex}}" title="Show" style="display:none" ng-click="cov_section_tve('show',truckIndex)"> <img src="http://gcib.net/quick_quote//images/hide.png"></span>
                         <span class="red" id="hide-cov_tve{{truckIndex}}" title="Hide" ng-click="cov_section_tve('hide',truckIndex)"> <img src="http://gcib.net/quick_quote//images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                       </div>
                   </span>
                </div>
                <?php $vhteqp = 0; ?>
               
                <div class="row-fluid top10" id="cov_div_tve{{truckIndex}}" ng-if="truck.vh_equipment_pull=='Yes'"> 
                  <div class="row-fluid top20" ng-repeat="vh_eqpmnt in truck.vh_eqpmnts">                
                  <input type="hidden" ng-model="vh_eqpmnt.vh_eqp_id" value="{{vh_eqpmnt.vh_eqp_id}}" name="vh_eqp_id[]"/>
                   <?php if($underwriter=='underwriter') { ?><img ng-if="vh_eqpmnt.vh_eqp_salvage =='Yes'" src="<?php echo base_url()?>images/flag-red_24x24.png" /><?php } ?>
                  <div class="row-fluid">
                   <div class="left_section">                                	
                	<div class="span2 ">
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>Equipment Type</label>
                        <select name="vh_eqp_type[]" class="span10" ng-model="vh_eqpmnt.vh_eqp_type">
                        <option value="">Select</option>
                        <option >Equipment</option>	
                        </select>
                    </div>
                    <div class="span1 span_11">
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>Vehicle Year</label>
                        <select name="vh_eqp_year[]" class="span10" ng-model="vh_eqpmnt.vh_eqp_year">
                        <option value="">Select</option>
						<?php 						 
						  $current_year = date("Y");
						  
						  for($i = 1985; $i <= $current_year + 1; $i++){
							echo "<option value='$i'>$i</option>";
						  } 
						?>
                        </select>
                    </div>
                    <div class="span2 span_10">
                     
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>Make</label>
						<?php echo getMakeModelVehicles('make_model_vh[]', 'class="span12" ng-model="vh_eqpmnt.make_model_vh"','',30,'');?>
                    </div>
                    <div class="span2 span_10">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>Model</label>
                        <?php echo getMakeModelVehicles('vh_eqp_model[]', 'class="span12" ng-model="vh_eqpmnt.vh_eqp_model"','',31,'');?>
                    </div>
                    <div class="span2">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>GVW</label>
                        <select class="span12" name="gvw_vh[]" id="gvw_vh" ng-model="vh_eqpmnt.gvw_vh">
                          <option value="">Select</option>
                          <option value="0-10,000 pounds">0-10,000 pounds</option>
                          <option value="10,001-26,000 pounds">10,001-26,000 pounds</option>
                          <option value="26,001-40,000 pounds">26,001-40,000 pounds</option>
                          <option value="40,001-80,000 pounds">40,001-80,000 pounds</option>
                          <option value="Over 80,000 pounds">Over 80,000 pounds</option>
                        </select>
                    </div>
                    <div class="span2">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>VIN</label>
                        <input type="text" name="vh_eqp_vin[]" class="span12" ng-model="vh_eqpmnt.vh_eqp_vin">
                    </div>
                    <div class="span2 span_10">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.++$vhteqp; ?> </span>Salvage</label>
                        <select name="vh_eqp_salvage[]" class="span12" ng-model="vh_eqpmnt.vh_eqp_salvage">
                         <option value="">Select</option>
               			 <option value="No" >No</option>
                		 <option value="Yes" >Yes</option>
                        </select>
                    </div>
                    <div class="span1">
                    	<label class="span12 no_margin">&nbsp;</label>
                    	
                		<a href="javscript:;" ng-click="remove_eqp('trucks', truckIndex, $index)"><img src="<?= base_url('images/remove.png'); ?>"></a>
                    </div>
              <div class="row-fluid top20 liability_color">
            	<div class="row-fluid">
            	<p>
            	<span class="span2 span_2 truck_liability_numbers"> </span>   </p>
                	<label><span class="numbers"><?= $vhit.'.'.++$vhteqp; ?> </span>Liability coverage</label>
                    <select name="vh_lib_coverage[]" class="span1 no_margin" ng-model="vh_eqpmnt.vh_lib_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span>
                </div>
                <?php $vhteqplib = 0;?>
               
                <div class="row-fluid top10" ng-if="vh_eqpmnt.vh_lib_coverage=='Yes'">
                	<div class="span2 span_11">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqplib; ?> </span>Liability</label>
                        <select class="span12" name="liability_vh[]" id="liability_vh_{{truckIndex}}" ng-model="vh_eqpmnt.liability_vh" ng-change="lia_first_v">
                  			<option value="">Select</option>
                            <option value="$1,000,000">$1,000,000</option>
                            <option value="$750,000">$750,000</option>
                            <option value="$500,000">$500,000</option>
                        </select>
                    </div>
                    <div class="span2 ">
                    
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqplib; ?> </span>Liability Ded</label>
                     
                        <select class="span10" name="liability_ded_vh[]" id="liability_ded_vh_{{truckIndex}}" ng-model="vh_eqpmnt.liability_ded_vh" ng-change="lia_first_ded_v">                        
                            <option value="">Select</option>
                            <option value="$1,000">$1,000</option>
                            <option value="$1,500">$1,500</option>
                            <option value="$2,000">$2,000</option>
                            <option value="$2,500">$2,500</option>
                        </select>
                    </div>
                    <div class="span2 span_10">
                    <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqplib; ?> </span>UM</label>
                     <?php
					   $um = '';                                                               
					   $state = 'CA';
					   $att = 'class="span12 ui-state-valid myclass um_changes_option" ng-model="vh_eqpmnt.truck_um"'; 
					   echo getumcoverages('truck_um[]',$att,$um,$state);
                     ?>
                   </div>
                  <div class="span2 span_10">
                
                    <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqplib; ?> </span>PIP</label>
              
                    <?php   
					  $pip = '';                                                              
					  $state = 'CA';
					  $att = 'class="span12 ui-state-valid myclass pip_changes_option" ng-model="vh_eqpmnt.truck_pip"'; 
					  echo getpipcoverages('truck_pip[]',$att,$pip,$state);
                    ?>
                  </div>                                  
                  <div class="span2 span_10">
                 
                    <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqplib; ?> </span>UIM</label>
                    <?php   
					  $uim = '';
					  $state = 'CA';
					  $att = 'class="span12 ui-state-valid myclass uim_changes_option" ng-model="vh_eqpmnt.truck_uim"'; 
					  echo getuimcoverages('truck_uim[]',$att,$uim,$state);
                    ?>
                  </div>
                </div>
            </div>
            
            <?php $vhteqppd = 0; ?>
              <div class="row-fluid top20 pd_color">
            	<div class="row-fluid">
               
                	<label><span class="numbers"><?= $vhit.'.'.++$vhteqp; ?> </span>PD coverage</label>
                    <select name="vh_pd_coverage[]" class="span1 no_margin" ng-model="vh_eqpmnt.vh_pd_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span>
                </div>
                <div class="row-fluid top10" ng-if="vh_eqpmnt.vh_pd_coverage=='Yes'">
                	 <div class="span2 span_10">
                    
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqppd; ?> </span>ACV</label>
                        <input type="text" class="span12 " ng-model="vh_eqpmnt.pd_vh" maxlength="13" format    name="pd_vh[]" id="pd_vh" >
                    </div>
                    <div class="span2 span_10">
                   
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqppd; ?> </span>PD Ded</label>
                        <input type="text" class="span12 " ng-model="vh_eqpmnt.ph_ded_vh" maxlength="13" format name="ph_ded_vh[]" id="ph_ded_vh" value="">
                    </div>
                    <div class="span3">
                    
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqppd; ?> </span>Remarks</label>
                        <input type="text" name="vh_pd_remark[]" ng-model="vh_eqpmnt.vh_pd_remark" class="span12">
                    </div>
                       <div class="span3">
                       
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqppd; ?> </span> Do you want :</label>
                        <select name="pd_want[]" ng-model="vh_eqpmnt.pd_want" class="span9">
                        <option value="">Select</option>
                        <option value="primary liability">Primary Liability</option>
                        <option value="bobtail">Bobtail</option>
                        <option value="deadhead">Deadhead</option>
                        <option value="under company policy">Under company policy</option>                        
                        </select>
                    </div>
                </div>
                
            </div>
            
            <?php $vhteqpcargo = 0; ?>
              <div class="row-fluid top20 cargo_color">
            	<div class="row-fluid">
                
                	<label><span class="numbers"><?= $vhit.'.'.++$vhteqp; ?> </span>Cargo coverage</label>
                    <select name="vh_cargo_coverage[]" class="span1 no_margin" ng-model="vh_eqpmnt.vh_cargo_coverage">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span>
                </div>
                <div class="row-fluid top10" ng-if="vh_eqpmnt.vh_cargo_coverage=='Yes'">
                	<div class="span2 span_11">
                      
                      <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqpcargo; ?> </span>Cargo limit</label>
                      <input type="text" class="span12 myclass" ng-model="vh_eqpmnt.cargo_vh" maxlength="13" format name="cargo_vh[]" id="cargo_vh" >
                    </div>
                    <div class="span2 span_11">
                     
                        <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqpcargo; ?> </span>Cargo Ded</label>
                        <input type="text" class="span12  myclass" ng-model="vh_eqpmnt.cargo_ded_vh" maxlength="13" format name="cargo_ded_vh[]" id="cargo_ded_vh" >
                    </div>
                      <div class="span3 cargo"> 
                       
                    	<label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqpcargo; ?> </span>Refrigeration breakdown:</label>	
                        <input type="checkbox" name="refrigerated_breakdown[]" ng-model="vh_eqpmnt.refrigerated_breakdown" id="refrigerated_breakdown" ng-checked="vh_eqpmnt.refrigerated_breakdown">                       
                        <div class="refrigerated_breakdown span12" ng-if="vh_eqpmnt.refrigerated_breakdown==true || vh_eqpmnt.refrigerated_breakdown=='on'">
                         	<label>Deductible : </label>
                            <input type="text" class="span6 " maxlength="13"  format name="deductible[]" ng-model="vh_eqpmnt.deductible" id="deductible">
                         </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span12">
                     
                     <label><span class="numbers_child_child"><?= $vhit.'.'.$vhteqp.'.'.++$vhteqpcargo; ?> </span>Commodities Hauled</label>
                     <input type="text" name="commodities_haulted_truck[]" ng-model="vh_eqpmnt.commodities_haulted" class="span6 commhauled2 ui-state-valid myclass">  
                     <label>if you are requesting cargo coverage, please be as specific as possible, For Example, Do not list Dry Goods, General Freight or Dry Freight </label>
                   </div>   
                 </div>
                </div>
                 
             </div>
                <div class="row-fluid">
                <input type="hidden" name="vh_eqp_types[]" value="truck"/>
              
        <label><span class="numbers truck_needcargo_numbers"></span>Please upload any registration file from the vehicle:</label>
                <input type="file" multiple name="vehicle_file[][]" ng-model="vh_eqpmnt.vehicle_file" class="span1 span_25">
                
                <a href="<?php echo base_url();?>uploads/vehicle_file/{{vh_eqpmnt.vehicle_file}}" download>{{vh_eqpmnt.vehicle_file}}</a>
           </div>
          </div>
         <div class="right_section" <?php  if($underwriter=="underwriter"){}else{echo'style="display:none"';}; ?>>
            <div class="row-fluid top20">
            	<table class="table_css_w">
                 <tr>&nbsp;</tr>
                 <tr>&nbsp;</tr>
                 <tr>&nbsp;</tr>
                  <tr>
                     <td style="width:40%">
                	   Liability Price
                       </td>
                     <td>
                     <input type="text" name="liability[]" ng-model="vh_eqpmnt.liability_vh" ng-if="vh_eqpmnt.liability==NULL" format class="span10"/>
                     <input type="text" name="liability[]" ng-model="vh_eqpmnt.liability" ng-if="vh_eqpmnt.liability!=NULL" format class="span10"/>
                    </td>                    
                  </tr>
                  <tr>
                    <td>                     	
                	  UM
                     </td>
                     <td>
                        <input type="text" name="um[]" ng-model="vh_eqpmnt.truck_um" ng-if="vh_eqpmnt.um==NULL"  class="span10"/>
                        <input type="text" name="um[]" ng-model="vh_eqpmnt.um" ng-if="vh_eqpmnt.um!=NULL" class="span10"/>
                    </td>
                  </tr>
                     <td>    	
                	   PIP
                     </td>
                     <td> 
                        <input type="text" name="pip[]" ng-model="vh_eqpmnt.truck_pip" ng-if="vh_eqpmnt.pip==NULL" format class="span10"/>
                        <input type="text" name="pip[]" ng-model="vh_eqpmnt.pip" ng-if="vh_eqpmnt.pip!=NULL" format class="span10"/>
                    </td>
                </tr>
                <tr>
                    <td>                     	
                	   UIM
                    </td>
                    <td>
                        <input type="text" name="uim[]" ng-model="vh_eqpmnt.truck_uim" ng-if="vh_eqpmnt.uim==NULL" format class="span10"/>
                        <input type="text" name="uim[]" ng-model="vh_eqpmnt.uim" ng-if="vh_eqpmnt.uim!=NULL" format class="span10"/>
                    </td>
                </tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>  
                 <tr>
                    <td>                     	
                	   PD
                    </td>
                    <td>
                        <input type="text" name="pd[]" ng-model="vh_eqpmnt.pd_vh" ng-if="vh_eqpmnt.pd==NULL" class="span10" maxlength="13" format/>
                        <input type="text" name="pd[]" ng-model="vh_eqpmnt.pd" ng-if="vh_eqpmnt.pd!=NULL" class="span10" maxlength="13" format/>
                    </td>
                </tr> 
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                 <tr>
                    <td>                     	
                	   Cargo
                    </td>
                    <td>
                        <input type="text" name="cargo[]" ng-model="vh_eqpmnt.cargo_vh" ng-if="vh_eqpmnt.cargo!=NULL"class="span10" maxlength="13" format/>
                        <input type="text" name="cargo[]" ng-model="vh_eqpmnt.cargo" ng-if="vh_eqpmnt.cargo==NULL"class="span10" maxlength="13" format/>
                    </td>
                </tr> 
                
            </table>          
          </div>           
        </div>
       </div>
                   <div class="row-fluid border_equp" <?php  if($underwriter=="underwriter"){}else{echo'style="display:none"';}; ?>>
                        <span class="pull-right" style="  margin-right: -6px;">
                              <input type="text" name="total_equipment[]" ng-model="vh_eqpmnt.total_equipment" ng-if="vh_eqpmnt.total_equip==NULL" class="span10"/>
                              <input type="text" name="total_equip[]" ng-model="vh_eqpmnt.total_equip" ng-if="vh_eqpmnt.total_equip!=NULL" class="span10"/>
                          </span>
                          <span class="pull-right" style="  margin-right: 6px;">                    	
                             Total For equipment {{$index+1}}
                        </span >
                   
                       </div> 
                    </div>
                </div>
            </div>--><?php */?>
            
            
            <?php $vhitlib = 0; ?>
             <div class="row-fluid top20" >
             <div class="left_section">
              <div class="row-fluid top20 liability_color">
            	<div class="row-fluid">
                	<label id="label_t_vh_lib_coverage"><span class="numbers"><?= ++$vhit; ?> </span>Liability coverage</label>
                    <select name="t_vh_lib_coverage[]" class="span1 no_margin" ng-model="truck.vh_lib_coverage" id="lib_coverage_{{truckIndex}}" ng-if="truckIndex==0" ng-change="lib_coverage()">
                     	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <select name="t_vh_lib_coverage[]" class="span1 no_margin" ng-model="truck.vh_lib_coverage" id="lib_coverage_{{truckIndex}}" ng-if="truckIndex!=0" >
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span>
                </div>
               
                <div class="row-fluid top10" ng-if="truck.vh_lib_coverage=='Yes'" >
                	<div class="span2 span_12">
                    	<label id="label_t_vh_lib_coverage"><span class="numbers"><?= $vhit.'.'.++$vhitlib;?> </span> Liability</label>
                         <select class="span12 lia_vh" name="t_liability_vh[]" ng-model="truck.liability_vh" ng-if="truckIndex==0" ng-change="lia_first_v()" id="liability_vh_{{truckIndex}}" >
                  			<option value="">Select</option>
                            <option value="$1,000,000">$1,000,000</option>
                            <option value="$750,000">$750,000</option>
                            <option value="$500,000">$500,000</option>
                        </select>
                        <select class="span12 lia_vh" name="t_liability_vh[]" ng-model="truck.liability_vh" ng-if="truckIndex!=0" id="liability_vh_{{truckIndex}}" >
                  			<option value="">Select</option>
                            <option value="$1,000,000">$1,000,000</option>
                            <option value="$750,000">$750,000</option>
                            <option value="$500,000">$500,000</option>
                        </select>
                    </div>
                    <div class="span2 span_8">
                        <label id="label_t_liability_ded_vh"><span class="numbers"><?= $vhit.'.'.++$vhitlib;?> </span> Liability Ded</label>
                        <select class="span12 lia_ded_vh" name="t_liability_ded_vh[]" ng-model="truck.liability_ded_vh" ng-if="truckIndex==0" ng-change="lia_first_ded_v()" id="liability_ded_vh_{{truckIndex}}">                        
                            <option value="">Select</option>
                            <option value="$1,000">$1,000</option>
                            <option value="$1,500">$1,500</option>
                            <option value="$2,000">$2,000</option>
                            <option value="$2,500">$2,500</option>
                        </select>
                         <select class="span12 lia_ded_vh" name="t_liability_ded_vh[]" ng-model="truck.liability_ded_vh" ng-if="truckIndex!=0" id="liability_ded_vh_{{truckIndex}}">                        
                            <option value="">Select</option>
                            <option value="$1,000">$1,000</option>
                            <option value="$1,500">$1,500</option>
                            <option value="$2,000">$2,000</option>
                            <option value="$2,500">$2,500</option>
                        </select>
                    </div>
                    <div class="span2 span_10">
                     <label id="label_t_truck_um"><span class="numbers"><?= $vhit.'.'.++$vhitlib;?> </span>UM</label>
                     <?php
					   $um = '';                                                               
					   $state = 'CA';				 
					   echo getumcoverages('t_truck_um[]','class="span12 ui-state-valid myclass um_changes_option" ng-model="truck.truck_um" id="truck_um_{{truckIndex}}" ng-if="truckIndex==0" ng-change="truck_um_first()"',$um,$state);
                   
					   echo getumcoverages('t_truck_um[]','class="span12 ui-state-valid myclass um_changes_option" ng-model="truck.truck_um" id="truck_um_{{truckIndex}}" ng-if="truckIndex!=0"',$um,$state);
                     ?>
                   </div>
                  <div class="span2 span_10">
                    <label id="label_t_truck_pip"><span class="numbers"><?= $vhit.'.'.++$vhitlib;?> </span>PIP</label>
                     <select class="span12 ui-state-valid myclass pip_changes_option" id="truck_pip_{{truckIndex}}" ng-model="truck.truck_pip" ng-if="truckIndex==0 && myServ.state_pip==&quot;CA&quot;" ng-init="truck.truck_pip='Not available'" >
                       <option>Not available</option>
                    </select>
					
					<?php   
					  $pip = '';                                                              
					  $state = 'CA';					 
					  echo getpipcoverages('t_truck_pip[]','class="span12 ui-state-valid myclass pip_changes_option" ng-model="truck.truck_pip" id="truck_pip_{{truckIndex}}" ng-if="truckIndex==0 && myServ.state_pip!=&quot;CA&quot;" ng-change="truck_pip_first()"',$pip,$state);
                      echo getpipcoverages('t_truck_pip[]','class="span12 ui-state-valid myclass pip_changes_option" ng-model="truck.truck_pip" id="truck_pip_{{truckIndex}}" ng-if="truckIndex!=0 && myServ.state_pip!=&quot;CA&quot;" ',$pip,$state);
					  
				     // echo getpipcoverages('t_truck_pip[]','class="span12 ui-state-valid myclass pip_changes_option" ng-model="truck.truck_pip" id="truck_pip_{{truckIndex}}" ng-if="truckIndex==0 && state_pip!=&quot;CA&quot;" ng-change="truck_pip_first()"',$pip,$state);
                     // echo getpipcoverages('t_truck_pip[]','class="span12 ui-state-valid myclass pip_changes_option" ng-model="truck.truck_pip" id="truck_pip_{{truckIndex}}" ng-if="truckIndex!=0 && state_pip!=&quot;CA&quot;" ',$pip,$state);
					?>
               
                   
                  
                  </div>                                  
                  <div class="span2 span_10">
                    <label id="label_t_truck_uim"><span class="numbers"><?= $vhit.'.'.++$vhitlib;?></span> UIM</label>
                    <?php   
					  $uim = '';
					  $state = 'CA';
					  echo getuimcoverages('t_truck_uim[]','class="span12 ui-state-valid myclass uim_changes_option" ng-model="truck.truck_uim" id="truck_uim_{{truckIndex}}" ng-if="truckIndex==0" ng-change="truck_uim_first()"',$uim,$state);
                   	  echo getuimcoverages('t_truck_uim[]','class="span12 ui-state-valid myclass uim_changes_option" ng-model="truck.truck_uim" id="truck_uim_{{truckIndex}}" ng-if="truckIndex!=0"',$uim,$state);
                    ?>
                  </div>
                </div>
            </div>
            <?php $vhitpd = 0; ?>
              <div class="row-fluid top20 pd_color">
            	<div class="row-fluid">
                	<label id="label_t_vh_pd_coverage"><span class="numbers "><?= ++$vhit; ?> </span>PD coverage {{truck.vehicle_year_vh}}<span ng-if="truck.make_truck">, {{truck.make_truck}}</span><span ng-if="truck.vin_vh">,{{truck.vin_vh}}</span></label>
                    <select name="t_vh_pd_coverage[]" class="span1 no_margin" ng-model="truck.vh_pd_coverage" onchange="quick_quote()">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only </span><span ng-if="truck.vh_pd_coverage=='No'" style="color:#000;font-size:9px;">(PD coverage was not selected, but you can select it for this vehicle)</span>
                </div>
                <div class="row-fluid top10" ng-if="truck.vh_pd_coverage=='Yes'">
                	 <div class="span2 span_10">
                        <label id="label_t_pd_vh"><span class="numbers"><?= $vhit.'.'.++$vhitpd;?> </span>ACV</label>
                        <input type="text" class="span12 " ng-model="truck.pd_vh" format  name="t_pd_vh[]" id="pd_vh"  >
                    </div>
                    <div class="span2 span_12" >
                        <label id="label_t_ph_ded_vh"><span class="numbers"><?= $vhit.'.'.++$vhitpd;?> </span>PD Ded</label>
                        <input type="text" class="span12 " name="t_ph_ded_vh[]" ng-blur="min_max_val(truck.ph_ded_vh,truckIndex,'t')" title="$1,000/$10,000" ng-model="truck.ph_ded_vh" format id="t_ph_ded_vh_{{truckIndex}}">
                        <span style="color:red;display:none;font-size:10px;" id="t_ph_ded_vh_span_{{truckIndex}}">Min/Max $1,000/$10,000</span>
                    </div>
                    <div class="span3">
                    	<label id="label_t_vh_pd_remark"><span class="numbers"><?= $vhit.'.'.++$vhitpd;?> </span>Remarks</label>
                        <input type="text" name="t_vh_pd_remark[]" ng-model="truck.vh_pd_remark" class="span12">
                    </div>
                      <div class="span3">
                        <label id="label_t_pd_want"><span class="numbers"><?= $vhit.'.'.++$vhitpd;?> </span>Do you want :</label>
                        <select name="t_pd_want[]" ng-model="truck.pd_want" class="span9" disabled="disabled">
                        <option value="">Select</option>
                        <option value="primary liability">Primary Liability</option>
                        <option value="bobtail">Bobtail</option>
                        <option value="deadhead">Deadhead</option>
                        <option value="under company policy">Under company policy</option>                        
                        </select>
                    </div>                    
            
                </div>
            </div>
            <?php $vhitcargo = 0; ?>
              <div class="row-fluid top20 cargo_color">
            	<div class="row-fluid">
                	<label id="label_t_vh_cargo_coverage"><span class="numbers "><?= ++$vhit; ?> </span>Cargo coverage {{truck.vehicle_year_vh}}<span ng-if="truck.make_truck">,{{truck.make_truck}}</span><span ng-if="truck.make_truck">,{{truck.vin_vh}}</span></label>
                    <select name="t_vh_cargo_coverage[]" class="span1 no_margin" ng-model="truck.vh_cargo_coverage" onchange="quick_quote()">
                    	<option>Yes</option>
                        <option>No</option>
                    </select>
                    <span style="font-size:9px">If you want to take this coverage off, please select No on this vehicle only</span> <span ng-if="truck.vh_cargo_coverage=='No'" style="color:#000;font-size:9px;">(Cargo coverage was not selected, but you can select it for this vehicle)</span>
                </div>
                <div class="row-fluid top10" ng-if="truck.vh_cargo_coverage=='Yes'">
                	<div class="span2 span_10">
                      <label id="label_t_cargo_vh"><span class="numbers"><?= $vhit.'.'.++$vhitcargo;?> </span>Cargo limit</label>
                      <?php   echo getMakeModelVehicles('t_cargo_vh[]', 'class="span12" ng-model="truck.cargo_vh"','',49,'');?>

                    <!--  <input type="text" class="span12  myclass" ng-model="truck.cargo_vh"  maxlength="13" format  name="t_cargo_vh[]" id="cargo_vh">-->
                    </div>
                    <div class="span2 span_10">
                        <label id="label_t_cargo_ded_vh"><span class="numbers"><?= $vhit.'.'.++$vhitcargo;?> </span>Cargo Ded</label>
                        <input type="text" class="span12  myclass" ng-model="truck.cargo_ded_vh" maxlength="13" format  name="t_cargo_ded_vh[]" id="cargo_ded_vh" >
                    </div>
                     <div class="span4 cargo" style="margin-top: -10px;"> 
                      <label id="label_t_refrigerated_breakdown"><span class="numbers"><?= $vhit.'.'.++$vhitcargo;?> </span>Refrigeration breakdown:</label>
                      <!-- <input type="checkbox" name="t_refrigerated_breakdown[]" ng-model="truck.refrigerated_breakdown" id="refrigerated_breakdown" ng-checked="truck.refrigerated_breakdown">-->            
                      <select name="t_refrigerated_breakdown[]" ng-model="truck.refrigerated_breakdown" class="span3">
                          <option>Yes</option>
                          <option>No</option>
                      </select>
                      <!-- <div class="refrigerated_breakdown span12" ng-if="truck.refrigerated_breakdown==true || truck.refrigerated_breakdown=='on'">-->
                        <div class="refrigerated_breakdown span12" ng-if="truck.refrigerated_breakdown=='Yes'">
                         	<label id="label_t_deductible">Deductible : </label>
                            <input type="text" class="span6 " maxlength="13" ng-model="truck.deductible" format name="t_deductible[]" id="deductible">
                         </div>
                    </div>
                    <div class="row-fluid">
                      <div class="span12">
                     <label><span class="numbers"><?= $vhit.'.'.++$vhitcargo;?> </span>Commodities Hauled</label>
                     <input type="text" name="t_commodities_haulted_truck[]" ng-model="truck.commodities_haulted" class="span6 commhauled2 ui-state-valid myclass" >  
                     <label>if you are requesting cargo coverage, please be as specific as possible, For Example, Do not list Dry Goods, General Freight or Dry Freight </label>
                   </div>   
                 </div>
            
                </div>
                 
             </div>
              <div class="row-fluid">
              <input type="hidden"  name="t_vh_eqp_types[]" value="truck">
              <label>Please upload any registration file from the vehicle:</label>
              <input type="file" multiple name="t_vehicle_file[]" class="span1 span_25" value="{{other.vehicle_file}}">
               <?php $files_t=($truck_vh)?!empty($truck_vh[0]['vehicle_file'])?explode(',',$truck_vh[0]['vehicle_file']):'':''; 
				 
				 if($files_t){
					 foreach($files_t as $file){
				?>
                 <br/><a href="<?php echo base_url();?>uploads/vehicle_file/<?= $file;?>" download><?= $file;?></a>
               <?php } } ?>
             <!-- <a href="<?php echo base_url();?>uploads/vehicle_file/{{truck.vehicle_file}}" download>{{truck.vehicle_file}}</a>-->
           </div>
            </div>
           <div class="right_section" <?php  if($underwriter=="underwriter"){}else{echo'style="display:none"';}; ?>>
            <div class="row-fluid top20" >
            	<table class="table_css_w">
                 <tr>&nbsp;</tr>
                 <tr>&nbsp;</tr>
                 <tr>&nbsp;</tr>
                  <tr>
                     <td style="width:40%">
                	   Liability Price
                       </td>
                     <td>
                      <input type="text" name="liability_price[]" ng-model="truck.liability_vh" ng-if="truck.liability==NULL" format   class="span10"/>
                       <input type="text" name="liability_price[]" ng-model="truck.liability" ng-if="truck.liability!=NULL" format   class="span10"/>
                    </td>                    
                  </tr>
                  <tr>
                    <td>                     	
                	  UM
                     </td>
                     <td>
                        <input type="text" name="um_price[]" ng-model="truck.truck_um" ng-if="truck.um==NULL" class="span10"/>
                        <input type="text" name="um_price[]" ng-model="truck.um" ng-if="truck.um!=NULL" format   class="span10"/>
                    </td>
                  </tr>
                     <td>    	
                	   PIP
                     </td>
                     <td> 
                        <input type="text" name="pip_price[]" ng-model="truck.truck_pip" ng-if="truck.pip==NULL" class="span10"/>
                        <input type="text" name="pip_price[]" ng-model="truck.pip" ng-if="truck.pip!=NULL" format   class="span10"/>
                    </td>
                </tr>
                <tr>
                    <td>                     	
                	   UIM
                    </td>
                    <td>
                        <input type="text" name="uim_price[]" ng-model="truck.truck_uim" ng-if="truck.uim==NULL" class="span10"/>
                        <input type="text" name="uim_price[]" ng-model="truck.uim" ng-if="truck.uim!=NULL" class="span10"/>
                    </td>
                </tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>  
                 <tr>
                    <td>                     	
                	   PD
                    </td>
                    <td>
                        <input type="text" name="pd_price[]" ng-model="truck.pd_vh" ng-if="truck.pd==NULL" format class="span10"/>
                        <input type="text" name="pd_price[]" ng-model="truck.pd" ng-if="truck.pd!=NULL" format class="span10"/>
                    </td>
                </tr> 
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                <tr>&nbsp;</tr>
                <tr>
                    <td>                     	
                	   Cargo
                    </td>
                    <td>
                        <input type="text" name="cargo_price[]" ng-model="truck.cargo_vh" ng-if="truck.cargo==NULL" format class="span10"/>
                        <input type="text" name="cargo_price[]" ng-model="truck.cargo" ng-if="truck.cargo!=NULL" format class="span10"/>
                    </td>
                </tr> 
                
            </table>          
          </div>          
        </div>       
       </div>
            <div class="row-fluid border_equp" <?php  if($underwriter=="underwriter"){}else{echo'style="display:none"';}; ?>>
                        <span class="pull-right" style="  margin-right: -6px;">
                              <input type="text" name="sub_total_price[]" maxlength="13" ng-model="truck.sub_total_price" ng-if="truck.total_price==NULL" format class="span10"/>
                              <input type="text" name="sub_total_price[]" maxlength="13" ng-model="truck.total_price"  ng-if="truck.total_price!=NULL" format class="span10"/>
                          </span>
                          <span class="pull-right" style="margin-right: 6px;">                    	
                            Sub Total Vehicle #{{$index+1}} 
                        </span >
                      </div>
                    </div>
                   </div>
                   <!-- <div class="row-fluid black">
                       NOTE: If you want OR do not want a particular coverage then you might check them on or check them off
                   </div>-->
                </div>
           </div>
    