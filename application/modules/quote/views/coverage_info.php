<!-- Coverage Section End Ashvin Patel 26/April/2015-->
<fieldset class="coverage_info_box top10" ng-controller="coverageCtrl" id="cov_divs">
<legend >

<div><?php if($underwriter_u=="underwriter") { ?><span class="characters_css_l" ><label style="color:white" ng-if="coverages.vh_type">Vehicle Type :</label><label style="color:rgb(190, 136, 136);" ng-if="coverages.vh_type.indexOf('Truck') >= 0">Truck</label><label style="color:rgb(190, 136, 136);" ng-if="coverages.vh_type.indexOf('Tractor') >= 0">,Tractor</label><label style="color:rgb(190, 136, 136);" ng-if="coverages.vh_type.indexOf('Trailer') >= 0">,Trailer</label><label style="color:rgb(190, 136, 136);" ng-if="coverages.vh_type.indexOf('Other') >= 0">,Other</label></span><?php } ?><span class="characters"></span> C Coverage info<?php if($underwriter_u=="underwriter") { ?><span class="characters_css_r"><label style="color:white" ng-if="coverages.truck_no">Truck :</label><label ng-if="coverages.truck_no" style="margin-right:2px;color:rgb(190, 136, 136);">{{coverages.truck_no}}</label>&nbsp;&nbsp;<label style="color:white" ng-if="coverages.tractor_no">Tractor :</label><label ng-if="coverages.tractor_no" style="margin-right:2px;color:rgb(190, 136, 136);">{{coverages.tractor_no}}</label>&nbsp;&nbsp;<label style="color:white" ng-if="coverages.trailer_no">Trailer :</label><label ng-if="coverages.trailer_no" style="margin-right:2px;color:rgb(190, 136, 136)">{{coverages.trailer_no}}</label>&nbsp;&nbsp;<label style="color:white" ng-if="coverages.other_no">Other :</label><label style="margin-right:2px;color:rgb(190, 136, 136)" ng-if="coverages.other_no">{{coverages.other_no}}</label></span><?php } ?>
</div><div class="pull-right icon_show">
          <span class="green" id="show-cov" title="Show" style="display:none" onclick="cov_section('show')"> <img src="<?= base_url();?>/images/hide.png"></span>
          <span class="red" id="hide-cov" title="Hide" onclick="cov_section('hide')"> <img src="<?= base_url();?>/images/show.png"></span>&nbsp;&nbsp;&nbsp;&nbsp;
         </div>

 </legend>
<div class="well" id="cov_div">
<input type="hidden" name="coverage_id" ng-model="coverages.coverage_id" value="{{coverages.coverage_id}}"/>
  <div class="row-fluid">
  	<div class="span2 span_19 ">
    	<label id="label_coverage"><span class="numbers"></span>Coverage</label>    <br/>   
         <?=  getMakeModelVehicles('coverage[]', 'chosen multiple="multiple" ng-model="coverages.coverage" ng-change="coverage_change(coverages.coverage);" myDirective class="span12 no_margin chzn-select" id="coverage"','',51,'');?>
       <!-- <select name="coverage[]" chosen multiple="multiple" ng-model="coverages.coverage" ng-change="coverage_change(coverages.coverage);" myDirective class="span8 no_margin chzn-select" id="coverage">        	
            <option >Liability</option>
            <option>PD</option>
            <option>Cargo</option>
        </select>-->
    </div>
    <div class="span2 span_25 ">
    	<label id="label_vh_type"><span class="numbers"></span>Vehicle Type</label><br/>
        <select name="vh_type[]" chosen multiple="multiple" ng-model="coverages.vh_type"  class="span12 no_margin chzn-select" id="vh_type">        	
            <option>Truck</option>
            <option>Tractor</option>
            <option>Trailer</option>
            <option>Other</option>
        </select>
    </div> 
    <div class="span1 truck_box" ng-if="coverages.vh_type.indexOf('Truck') >= 0">
    	<div class="row-fluid img" id="label_truck_no"><img src="<?= base_url('images/truck.jpg'); ?>" width="70" ></div>
       <div class="row-fluid"> <input type="text" name="truck_no" class="span12" maxlength="2" size="2" title="Enter the number of vehicles" pattern="[0-9]+" ng-change="add_row_('{{coverages.truck_no}}', 'trucks')" ng-model="coverages.truck_no"></div>
    </div>
    <div class="span1 tractor_box" ng-if="coverages.vh_type.indexOf('Tractor') >= 0">
    	<div class="row-fluid img" id="label_tractor_no"><img src="<?= base_url('images/tractor.jpg'); ?>" width="70" class="img"></div>
        <input type="text" name="tractor_no" class="span12" maxlength="2" size="2" pattern="[0-9]+" title="Enter the number of vehicles" ng-change="add_row_('{{coverages.tractor_no}}', 'tractors')" ng-model="coverages.tractor_no">
    </div>
    <div class="span1 trailer_box" ng-if="coverages.vh_type.indexOf('Trailer') >= 0">
    	<div class="row-fluid img" id="label_trailer_no"><img src="<?= base_url('images/trailer.jpg'); ?>" width="70" class="img"></div>
        <input type="text" name="trailer_no" class="span12" maxlength="2" size="2" pattern="[0-9]+" title="Enter the number of vehicles" ng-change="add_row_('{{coverages.trailer_no}}', 'trailers')" ng-model="coverages.trailer_no">
    </div>
    <div class="span1 other_box" ng-if="coverages.vh_type.indexOf('Other') >= 0">
    	<label class="img" id="label_other_no">Other</label>
        <input type="text" name="other_no" class="span12" maxlength="2" size="2"  pattern="[0-9]+" title="Enter the number of vehicles" ng-change="add_row_('{{coverages.other_no}}', 'others')" ng-model="coverages.other_no">
    </div>
    <div class="span3 other_box" ng-if="coverages.vh_type.indexOf('Other') >= 0">
    	<label>Please specify Other Vehicle Name: </label>
        <input type="text" name="other_vh_name" class="span8"  ng-model="coverages.other_vh_name">
    </div>
  </div> 
  
  <div class="row-fluid">
  	<div class="row-fluid">   
      <?php $this->load->view('truck_coverage'); ?>      
      <?php $this->load->view('tractor_coverage'); ?>
      <?php $this->load->view('trailer_coverage'); ?>
      <?php $this->load->view('other_coverage'); ?>
    </div>
  </div>
 <div class="row-fluid top20">
  <input type="button" class="btn-primary pull-right" onclick="pdf_click('cov_divs');" value="Pdf" name="save"/>&nbsp;&nbsp;
  <input type="button" class="btn-primary pull-right" onclick="sec_info_save('coverage_info');" value="Save" name="save"/>
  </div>
</div>
 
</fieldset>

<!-- Coverage Section End Ashvin Patel 26/April/2015-->

    <?php $this->load->view('owner_driver'); ?>  

 
   
  