<?php 
	if (strtolower($this->input->server('HTTP_X_Pending_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
   //load_underwriter_list();
    $("#underwriterList").dataTable({
	 	 "sPaginationType": "full_numbers",
						/* "aoColumnDefs" : [ {
							'bSortable' : false,
							'aTargets' : [ 0 ]
						} ],*/
						"aaSorting": [[ 0, "desc" ]]
	 });
	 $('.container').css('width', '1250px');
});

function load_underwriter_list() {
    $("#underwriterList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo site_url(); ?>quote/ajax_quote_list/"+$("#Pending_by").val(),

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [{
                "sClass": "center"
            },

            //{"sClass": "center"},
            {
                "fnRender": function (oObj) {
                    var a = oObj.aData[1];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[4];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
					var a = oObj.aData[5];
					var d = new Date(a);
					var day = d.getDate();
					var month = d.getMonth() + 1;
					var year = d.getFullYear();
				
                    return (month + "/" + day + "/" + year);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[6];
                    return (a);
				}
			}, {
			"fnRender": function (oObj) {
				if(oObj.aData[8] == 'Released') {
					b = '<a class="btn btn-primary"" href="<?php echo base_url(); ?>ratesheet/rate_by_quote/'+oObj.aData[7]+'">View</a>';
					return (b);
					}	
					else
					{
						b = '-';
						return (b);
					}
				}
				
			}
			
        ]
    });

}
function sortStatus(val)
{
	$.ajax({
		url: "<?php echo base_url();?>quote/quote/sort_by_status",
		type: "post",
		data: "status="+val,
		success: function(json){
			 var obj = $.parseJSON(json);  
			//alert(obj['table_data']);
			var table_body = obj['table_data'];
			//var table_body = $(json).find('.table_data').html();
			//var export_link = $(json).find('.export_link').html();
			//alert(table_data);
			//var table_data = $(table_body).find('.table_body').html();
			$("#data_table").html(table_body);
			//$("#export_div").html(export_link);
			 $("#underwriterList").dataTable({
				 "sPaginationType": "full_numbers",
						/* "aoColumnDefs" : [ {
							'bSortable' : false,
							'aTargets' : [ 0 ]
						} ],*/
						"aaSorting": [[ 0, "desc" ]]
			 });
		}
	});
}

function ajax_change_status(status, id){
		if(status == 0){
			status = 'Disable';
		}else {
			status = 'Active';
		}
		$.ajax({
		type: "POST",
		url: "<?php echo base_url('quote/ajax_change_status'); ?>",
		data:{status:status,id:id},
		success:function(data){
			
			if(data=="done")
			{
				location.reload();
			} 
			
	}});

}

function sortStatus1(val)
{
	$.ajax({
		url: "<?php echo base_url();?>quote/quote/sort_by_status1",
		type: "post",
		data: "status="+val,
		success: function(json){
			 var obj = $.parseJSON(json);  
			//alert(obj['table_data']);
			var table_body = obj['table_data'];
			//console.log(table_body);
			//var table_body = $(json).find('.table_data').html();
			//var export_link = $(json).find('.export_link').html();
			//alert(table_data);
			//var table_data = $(table_body).find('.table_body').html();
			$("#data_table").html(table_body);
			//$("#export_div").html(export_link);
			 $("#underwriterList").dataTable({
				 "sPaginationType": "full_numbers",
						/* "aoColumnDefs" : [ {
							'bSortable' : false,
							'aTargets' : [ 0 ]
						} ],*/
						"aaSorting": [[ 0, "desc" ]]
			 });
		}
	});
}

function viewQuotePopup(id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo base_url('quote/QuoteView/');?>/'+id,
    	autoSize: true,
    	closeBtn: true,
    	width: '850',
    	/*height: '150',*/
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
}
/**By Ashvin Patel popup for Quoteview 13/may/2014**/


</script>
<br>
<h1>Quote Status</h1>
<?php //print_r($this->session->all_userdata());?>
<div class="table"> 
<!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> 
<!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<!--<a class="fancybox-add btn btn-primary" onclick="javascript:void(0)" href="">Add New</a>-->

  <!-- Quote List -->
<input type="hidden" name="Pending_by" id="Pending_by" value="<?php echo $user ?>" />
<select onchange="sortStatus(this.value)" style="margin: 0;">
    <option value="">Sort By Status</option>
    <option value="Draft">Draft</option>
    <option value="Pending">Pending</option>
    <option value="Rejected">Rejected</option>
    <option value="Released">Released</option>    
    <option value="Requested">Requested</option>
    <option value="Revise Request">Revise Request</option>
</select>
<select onchange="sortStatus1(this.value)" style="margin: 0;">
    <option value="">Sort By Status</option>
    <option value="">Active</option>
    <option value="deactivate">Deactivate</option>
</select>

<div id="data_table">
  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">Quote Number</th>
        <th width="12%">Date Submitted</th>
        <th width="15%">Agency Full Name/DBA</th>
        <th width="15%">Agency Phone Number</th>
		
		<th width="12%">Insured Full Name/DBA</th>
        <th width="12%">Coverage Date</th>
        <th>Status</th>
		<th width="30%">Action</th>
      </tr>
    </thead>
    <tbody>
		<?php ?>
		<?php  $cnt= 0; krsort($quote); foreach($quote as $row)
		{
			if($row->status!="deactivate"){
			$cnt++;
				?>
				<tr>
					<td><?php echo $row->quote_id; ?></td>
					
					<td><?php echo date("m/d/Y",strtotime($row->date_added)); ?></td>
					<td><?php echo get_agency_detail($row->requester_id, 'first_name')?></td>
					<td><?php echo get_agency_detail($row->requester_id, 'phone_number')?></td>
					<td><?php echo $row->contact_name.' '. $row->contact_middle_name.' '. $row->contact_last_name?></td>
                    <td><?php echo date("m/d/Y",strtotime($row->coverage_date)); ?></td>
					
                    
                    
                    <td><?php if($row->bundle_status==''){ echo 'Pending'; }else { echo $row->bundle_status; }?>
					<?php //if($row->bundle_status==''){ echo 'Pending'; }else { echo $row->bundle_status; }
					         /*  if($row->status == "Active"){
								   $Disable= "Disable";
									echo'<img src="'.base_url('images/green-dot.png').'" onClick="ajax_change_status(0,'.$row->quote_id.')" class="cursor-pointer"/>&nbsp;(Active)';	
								}else{
									$Active ="Active";
									echo'<img src="'.base_url('images/red-dot.png').'" onClick="ajax_change_status(1,'.$row->quote_id.')" class="cursor-pointer"/>&nbsp;(Disable)';	
								}*/
						
						?>
                    </td>
                    
                    
                    
					<?php 
						if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id'))){	
					?>
						<!--<td><?php if($row->bundle_status == '') { ?>Pending<?php } else { echo $row->bundle_status; } ?></td>-->
						<td><?php if($row->bundle_id == 0) { ?><!--<a href="<?php echo base_url('ratesheet/generate_ratesheet');?>/<?php echo $row->bundle_id; ?>">Generate Ratesheet</a>--><?php } else { ?><a href="<?php echo base_url(); ?>ratesheet/view_quote_rate1/<?php echo $row->bundle_id; ?>" class="btn btn-primary">View Ratesheet</a></a><?php } ?>
                       	<a href="javascript:;" onclick="viewQuotePopup(<?= $row->quote_id;?>)" class="btn btn">View Request</a>
                        
                        </td>
					<?php
					}  else if($this->session->userdata('member_id')){	
					?>
						<!--<td>
						<?php if($row->bundle_status == '') { ?>Pending<?php }
						 else {
							 if($row->bundle_status=='Released')
							 {
							 	$cd1 = strtotime($row->update_dt);
								$cd2 = strtotime(date("Y-m-d"));
								$cd3 = ($cd2-$cd1)/(60*60*24);
								if($cd3>=30)
								{
									$status =  'Expired';
									echo $status;
								}
								else{
								$status = '';
								echo $row->bundle_status; 
								}	
							 }
							 else{
							 	if($row->perma_reject=='Reject'){ echo 'Pending';} else {echo $row->bundle_status;}
							 }
						  
						  } ?></td>-->
						<td>
						<?php
						//echo	$row->bundle_status;
						 if($row->bundle_status != 'Released' && $row->bundle_status != 'Accepted') { 
							//echo $row->update_dt;
							echo '';
						} 
						else 
						{
							if($row->bundle_status!='')
							{
								if($status!='Expired')
								{
								 ?><a class="btn btn-primary" href="<?php echo base_url(); ?>ratesheet/view_quote_rate1/<?php echo $row->bundle_id; ?>">View Quote</a><?php 
								 }
								 else
								 {
									echo '';
								 }
							}
							 else
								 {
									echo '';
								 }
						 } ?>
                          <a href="javascript:;" onclick="viewQuotePopup(<?= $row->quote_id;?>)" class="btn btn">View Request</a>
                          <a class="btn btn-primary" href="<?php echo base_url(); ?>quote/quote_view_edit/<?php echo $row->quote_id; ?>">Edit</a>
                          <?php if($row->main_quote_id){ ?>
                          <a class="btn btn-primary" target="_blank" href="<?php echo base_url('quote/get_revision_pdf/'.$row->quote_id.'')?>">Get Revision</a>
                         
					<?php
					}
					?>
					 <a class="btn btn" target="_blank" href="<?php echo base_url('quote/QuoteView_Download/'.$row->quote_id.'')?>">Download Request</a>
                          <?php } ?>
                         </td>
				</tr>
				<?php 
		} }
		foreach($manual_quote as $quote1)
		{
			//print_r($quote1);
				$cnt++;
				?>
				<tr>
					<td><?php echo $quote1->quote_id; ?></td>
					
					<td><?php echo date("m/d/Y",strtotime($quote1->creation_date)); ?></td>
					<td><?php echo get_agency_detail($quote1->producer_email, 'first_name', 'email');?></td>
					<td><?php echo get_agency_detail($quote1->producer_email, 'phone_number', 'email');?></td>
					<td><?php echo $quote1->insured; ?></td>
                    <td></td>
                    <td><?php echo $quote1->status; ?></td>
					<?php 
						if(($this->session->userdata('admin_id')) || ($this->session->userdata('underwriter_id'))){	
					?>
						
						<td><?php /*if($quote1->bundle_id == 0) { ?><!--<a href="<?php echo base_url('ratesheet/generate_ratesheet');?>/<?php echo $quote1->bundle_id; ?>">Generate Ratesheet</a>--><?php } else { ?><a href="<?php echo base_url(); ?>ratesheet/view_quote_rate1/<?php echo $quote1->bundle_id; ?>">View</a></a><?php }*/ ?></td>
					<?php
					}  else if($this->session->userdata('member_id')){	
					?>
						<!--<td>
						<?php if($quote1->status == '') { ?>Pending<?php } else { echo $quote1->status; } ?></td>-->
						<td>
						<?php
						//echo	$row->bundle_status;
						 if($row->status != 'Released' && $row->status != 'Accepted') { 
							//echo $row->update_dt;
							echo '<a class="btn btn-primary" href="'.base_url().'ratesheet/view_quote_rate1/'.$quote1->id.'">View Quote</a>&nbsp;</a>';
						} 
						else 
						{
							if($row->bundle_status!='')
							{
								if($status!='Expired')
								{
								 ?><a class="btn btn-primary" href="<?php echo base_url(); ?>ratesheet/view_quote_rate1/<?php echo $row->bundle_id; ?>">View Quote</a>&nbsp;</a><?php 
								 }
								 else
								 {
									echo '-';
								 }
							}
							 else
								 {
									echo '-';
								 }
						 } ?></td>
					<?php
					}
					?>
					
				</tr>
                <?php
		} 
		?>
    </tbody>
  </table>
</div>
   <?php /*if($cnt==0){
  	//echo '<div class="not_data_tble">No Quotes Found</div>';
  }*/?>
  <!-- Quote List -->
  <?php //print_r($manual_quote); ?>
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_Pending_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
;