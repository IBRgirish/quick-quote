<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<script>
var base_url = '<?= base_url(); ?>';
$(document).ready(function(e) {
    $('.fancybox').fancybox();
});
</script>
<script type="text/javascript" src="<?php echo base_url('js/version_control.js'); ?>"></script>
<style>
	legend { font-family:Tahoma, Geneva, sans-serif; font-size:18px;}
	table 
	{
		border:1px solid #CCC;
		width:800px;
	}
	tr th { background:none repeat scroll 0 0 #FFF;color:#000;font-family:Arial, Helvetica, sans-serif;font-size:14px;padding:4px 3px; }
	
	tr td { background:none repeat scroll 0 0 #E7EEF5;color:#666;font-family:Arial, Helvetica, sans-serif;font-size:13px;padding:4px 3px;text-align:center; }
	
	fieldset, div { width:810px; }
	legend {  color:#000 !important; }
	.highlighted {border: 1px solid #F7E20D;}
</style>
<?php

function currency_format($value){
	return "$ ".number_format($value, 0);
}
function get_revisions($id, $log){
	$CI = & get_instance();
	?>
    <a href="#<?= $id; ?>" class="fancybox"><?php echo (count($log)>0) ? revision_controll_icon($log) : ''; ?></a>
    <div id="<?= $id; ?>" style="display:none;width:auto;">
	  <?php $data['data'] = $log;?>
      <?php $CI->load->view('includes/show-field-history', $data); ?>
    </div>	
    <?php
}
/*if($_POST)
{ */
	
	
	//echo "<pre>";print_r($_POST);echo "</pre>";
	//Tractor
	$tot_tractor_liability_vh = 0;
	$tot_tractor_liability_ded_vh = 0;
	$tot_tractor_pd_vh = 0;
	$tot_tractor_ph_ded_vh = 0;
	$tot_tractor_cargo_vh = 0;
	$tot_tractor_cargo_ded_vh = 0;
	
	//Trailer
	$tot_trailer_liability_vh = 0;
	$tot_trailer_liability_ded_vh = 0;
	$tot_trailer_pd_vh = 0;
	$tot_trailer_ph_ded_vh = 0;
	$tot_trailer_cargo_vh = 0;
	$tot_trailer_cargo_ded_vh = 0;
	
	//Trucks
	$tot_liability_vh = 0;
	$tot_liability_ded_vh = 0;
	$tot_pd_vh = 0;
	$tot_ph_ded_vh = 0;
	$tot_cargo_vh = 0;
	$tot_cargo_ded_vh = 0;

	
?>

<table>
	<tr align="left">
		<th rowspan="3">Coverage Summary</th>
	</tr>
    <tr align="left">
		<td>Number of Tractors</td>
        <td>Number of Trailers</td>
        <td>Number of Trucks</td>
	</tr>
    <tr align="left">
		<td><?php echo count($tractors); ?></td>
        <td><?php echo count($trailers);?></td>
        <td><?php echo count($trucks); ?></td>
	</tr>
</table>

<!----------------------------------------------------------------------------------------------->

<?php if(count($tractors) > 0 ) { ?>
<table cellspacing="1">
	<tr align="left">
		<th colspan="7">Tractor</th>
	</tr>
    <tr align="left">
		<th>Vehicle</th>
        <?php /* if($tractor->liability!=''&&$tractor->liability_ded!='') { */?>
		<th>Liability</th>
        <th>Liability Deductible</th>
		<?php /*}*/ ?>
        <th>PD</th>
        <th>PD Deductible</th>
        <th>Cargo</th>
        <th>Cargo Deductible</th>
	</tr>
	
	
<?php //for($i = 0; $i < count($total_tractors); $i++) { echo $total_tractors.'tractor count + '.$i;?>
<?php $trac_count = count($tractors); $i =0; foreach($tractors as  $tractor){  ?>
	<tr>
    	<td><?php echo $i+1;?></td>
		<?php /*if($tractor->liability!=''&&$tractor->liability_ded!='')  {*/ ?>
		<td><?php echo isset($tractor->liability) ? $tractor->liability : ''; ?></td>
        <td><?php  echo isset($tractor->liability_ded) ? $tractor->liability_ded : ''; ?></td>
		<?php /*}*/ ?>
        <td><?php echo isset($tractor->pd) ? $tractor->pd : ''; ?></td>
        <td><?php echo isset($tractor->pd_ded) ? $tractor->pd_ded : ''; ?></td>
        <td><?php  echo isset($tractor->cargo) ? $tractor->cargo : ''; ?></td>
        <td><?php echo isset($tractor->cargo_ded) ? $tractor->cargo_ded : ''; ?></td>
	</tr>
      
<?php
		//Calculating the total
		if(isset($tractor->liability))
		{
		$tot_tractor_liability_vh = $tot_tractor_liability_vh + str_replace(',','',str_replace('$','',$tractor->liability)); 
		}
		if(isset($tractor->liability_ded))
		{
		$tot_tractor_liability_ded_vh = $tot_tractor_liability_ded_vh + str_replace(',','',str_replace('$','',$tractor->liability_ded)); 
		}
		if(isset($tractor->pd))
		{
		$tot_tractor_pd_vh = $tot_tractor_pd_vh + str_replace(',','',str_replace('$','',$tractor->pd)); 
		}
		if(isset($tractor->pd_ded))
		{
		$tot_tractor_ph_ded_vh = $tot_tractor_ph_ded_vh + str_replace(',','',str_replace('$','',$tractor->pd_ded)); 
		}
		if(isset($tractor->cargo))
		{
		$tot_tractor_cargo_vh = $tot_tractor_cargo_vh + str_replace(',','',str_replace('$','',$tractor->cargo)); 
		}
		if(isset($tractor->cargo_ded))
		{
		$tot_tractor_cargo_ded_vh = $tot_tractor_cargo_ded_vh + str_replace(',','',str_replace('$','',$tractor->cargo_ded)); 
		} 
		$i++;
		$trac_count--;
		
	}  
?>
	<tr>
    	<th>Total</th>
		<?php /*if($liability_checked) { */?>
        <td><?php echo currency_format($tot_tractor_liability_vh); ?></td>
        <td><?php echo currency_format($tot_tractor_liability_ded_vh);?></td>
		<?php /*}*/ ?>
        <td><?php echo currency_format($tot_tractor_pd_vh);?></td>
        <td><?php echo currency_format($tot_tractor_ph_ded_vh);?></td>
        <td><?php echo currency_format($tot_tractor_cargo_vh);?></td>
        <td><?php echo currency_format($tot_tractor_cargo_ded_vh); ?></td>
    </tr>
</table>
<?php } ?>

<!----------------------------------------------------------------------------------------------->

<?php if(count($trailers) > 0 ) { ?>
<table>
	<tr align="left">
		<th colspan="7">Trailer</th>
	</tr>
    <tr align="left">
		<th>Vehicle</th>
		<?php /*if($liability_checked) { */?>
        <th>Liability</th>
        <th>Liability Deductible</th>
		<?php /*}*/ ?>
        <th>PD</th>
        <th>PD Deductible</th>
        <th>Cargo</th>
        <th>Cargo Deductible</th>
	</tr>
	<?php $j = 0; foreach($trailers as $trailer) { ?>
	<tr>
    	<td><?php echo $j+1;?></td>
		<?php /*if($liability_checked) {*/ ?>
		<td><?php echo isset($trailer->liability) ? $trailer->liability : ''; ?></td>
        <td><?php  echo isset($trailer->liability_ded) ? $trailer->liability_ded : ''; ?></td>
		<?php /*}*/ ?>
        <td><?php echo isset($trailer->pd) ? $trailer->pd : ''; ?></td>
        <td><?php echo isset($trailer->pd_ded) ? $trailer->pd_ded : ''; ?></td>
        <td><?php echo isset($trailer->cargo) ? $trailer->cargo : ''; ?></td>
        <td><?php echo isset($trailer->cargo_ded) ? $trailer->cargo_ded : ''; ?></td>
	</tr>
<?php 
		//Calculating the total
		if(isset($trailer->liability))
		{
		$tot_trailer_liability_vh = $tot_trailer_liability_vh + str_replace(',','',str_replace('$','',$trailer->liability)); 
		}
		if(isset($trailer->liability_ded))
		{
		$tot_trailer_liability_ded_vh = $tot_trailer_liability_ded_vh + str_replace(',','',str_replace('$','',$trailer->liability_ded)); 
		}
		if(isset($trailer->pd))
		{
		$tot_trailer_pd_vh = $tot_trailer_pd_vh + str_replace(',','',str_replace('$','',$trailer->pd)); 
		}
		if(isset($trailer->pd_ded))
		{
		$tot_trailer_ph_ded_vh = $tot_trailer_ph_ded_vh + str_replace(',','',str_replace('$','',$trailer->pd_ded)); 
		}
		if(isset($trailer->cargo))
		{
		$tot_trailer_cargo_vh = $tot_trailer_cargo_vh + str_replace(',','',str_replace('$','',$trailer->cargo)); 
		}
		if(isset($trailer->cargo_ded))
		{
		$tot_trailer_cargo_ded_vh = $tot_trailer_cargo_ded_vh + str_replace(',','',str_replace('$','',$trailer->cargo_ded)); 
		}	
	} ?>
	<tr>
    	<th>Total</th>
		<?php /*if($liability_checked) {*/ ?>
    	<td><?php echo currency_format($tot_trailer_liability_vh);?></td>
        <td><?php echo currency_format($tot_trailer_liability_ded_vh);?></td>
		<?php /*}*/ ?>
        <td><?php echo currency_format($tot_trailer_pd_vh);?></td>
        <td><?php echo currency_format($tot_trailer_ph_ded_vh);?></td>
        <td><?php echo currency_format($tot_trailer_cargo_vh);?></td>
        <td><?php echo currency_format($tot_trailer_cargo_ded_vh);?></td>
    </tr>
</table>
<?php } ?>

<!----------------------------------------------------------------------------------------------->

<?php if(count($trucks) > 0 ) { ?>
<table>
	<tr align="left">
		<th colspan="7">Truck</th>
	</tr>
    <tr align="left">
		<th>Vehicle</th>
		<?php /*if($liability_checked) {*/ ?>
        <th>Liability</th>
        <th>Liability Deductible</th>
		<?php /*}*/ ?>
        <th>PD</th>
        <th>PD Deductible</th>
        <th>Cargo</th>
        <th>Cargo Deductible</th>
	</tr>
	<?php $i = 0; foreach($trucks as $truck) { ?>
	<tr>
    <td><?php echo $i+1;?></td>
		<?php /*if($liability_checked) {*/ ?>
		<td><?php echo isset($truck->liability) ? $truck->liability : ''; ?></td>
        <td><?php  echo isset($truck->liability_ded) ? $truck->liability_ded : ''; ?></td>
		<?php /*}*/ ?>
        <td><?php echo isset($truck->pd) ? $truck->pd : ''; ?></td>
        <td><?php echo isset($truck->pd_ded) ? $truck->pd_ded : ''; ?></td>
        <td><?php echo isset($truck->cargo) ? $truck->cargo : ''; ?></td>
        <td><?php echo isset($truck->cargo_ded) ? $truck->cargo_ded : ''; ?></td>
	</tr>
<?php 
		//Calculating the total trucks
		if(isset($truck->liability))
		{
		$tot_liability_vh = $tot_liability_vh + str_replace(',','',str_replace('$','',$truck->liability)); 
		}
		if(isset($truck->liability_ded))
		{
		$tot_liability_ded_vh = $tot_liability_ded_vh + str_replace(',','',str_replace('$','',$truck->liability_ded)); 
		}
		if(isset($truck->pd))
		{
		$tot_pd_vh = $tot_pd_vh + str_replace(',','',str_replace('$','',$truck->pd)); 
		}
		if(isset($truck->pd_ded))
		{
		$tot_ph_ded_vh = $tot_ph_ded_vh + str_replace(',','',str_replace('$','',$truck->pd_ded)); 
		}
		if(isset($truck->cargo))
		{
		$tot_cargo_vh = $tot_cargo_vh + str_replace(',','',str_replace('$','',$truck->cargo)); 
		}
		if(isset($truck->cargo_ded))
		{
		$tot_cargo_ded_vh = $tot_cargo_ded_vh + str_replace(',','',str_replace('$','',$truck->cargo_ded)); 
		}
	} ?>
	<tr>
    	<th>Total</th>
		<?php /*if($liability_checked) {*/ ?>
        <td><?php echo currency_format($tot_liability_vh);?></td>
        <td><?php echo currency_format($tot_liability_ded_vh);?></td>
		<?php /*}*/ ?>
        <td><?php echo currency_format($tot_pd_vh);?></td>
        <td><?php echo currency_format($tot_ph_ded_vh);?></td>
        <td><?php echo currency_format($tot_cargo_vh);?></td>
        <td><?php echo currency_format($tot_cargo_ded_vh);?></td>
    </tr>
</table>
<?php } ?>

<!----------------------------------------------------------------------------------------------->
<br>

<fieldset>
<legend>Broker Information</legend>
<table>
	<tr align="left">
		<th>Today's Date</th>
		<th>Time</th>
		<th>Coverage Date</th>
         <?php
		$p = '';
		
		for($i=0;$i<count($previous_quote_ids);$i++){
			$p_1 = isset($logs['contact_name'][$i]) ?  $logs['contact_name'][$i] : '';
			$p_2 = isset($logs['contact_middle_name'][$i]) ?  $logs['contact_middle_name'][$i] : '';
			$p_3 = isset($logs['contact_last_name'][$i]) ?  $logs['contact_last_name'][$i] : '';
			$pr = isset($p_1->value) ? $p_1->value : '';
			$pr .= isset($p_2->value) ? $p_2->value : '';
			$pr .= isset($p_3->value) ? $p_3->value : '';
			$created_0 = isset($p_1->created_by_name) ? $p_1->created_by_name : '';
			$created_1 = isset($p_2->created_by_name) ? $p_2->created_by_name : '';
			$created_2 = isset($p_3->created_by_name) ? $p_3->created_by_name : '';	
			$created_3 = ($created_1) ? $created_1 : ''; 	 
			$created_by_name = ($created_0) ? $created_0 : ($created_2) ? $created_2 : $created_3 ; 
			
			$created_n0 = isset($p_1->created_on) ? $p_1->created_on : '';
			$created_n1 = isset($p_2->created_on) ? $p_2->created_on : '';
			$created_n2 = isset($p_3->created_on) ? $p_3->created_on : '';
			$created_n3 = ($created_n2) ? $created_n2 : '';
			$created_on = ($created_n0) ? $created_n0 : ($created_n2) ? $created_n2 : $created_n3 ;
			
			if($pr){
			  $val = array('value' => $pr, 'created_by_name' => $created_by_name, 'created_on' => $created_on);
			  
			  $p[] = (object) $val;
			}
		}
		$data['data'] = $p;		
		?>  
		<th class="<?php echo ($p) ? 'highlighted' : ''; ?>">Person submitting business
		<a href="#p_submiting_b" class="fancybox"><?php echo (count($p)>0) ? revision_controll_icon($p) : ''; ?></a>
        <div id="p_submiting_b" style="display:none;width:auto;">
        	<?php $this->load->view('includes/show-field-history', $data); ?>
        </div>
        </th>
	</tr>
	<tr>
		<td><?php echo date('m/d/Y', time());?></td>
		<td><?php echo date('h:i:s a', time());?></td>
		<td><?php echo date('m/d/Y', strtotime($quote[0]->coverage_date));?></td> 
		<td><?php echo $quote[0]->contact_name.' '.$quote[0]->contact_middle_name.' '.$quote[0]->contact_last_name;?></td>
        
	</tr>
</table>
<table>
	<tr>
    	<th>Cab Number</th>
		<th>Agency Name</th>
		<th>Phone Number</th>
		<th>Cell Ph. Number</th>
	</tr>
	<tr>
    	<td><?php echo $quote[0]->cab;?></td>
		<td><?php echo $quote[0]->agency;?></td>
		<td><?php echo $quote[0]->phone_no;?></td>
		<td><?php echo $quote[0]->cell_no;?></td>
	</tr>
</table>
<table>
	<tr>
    	<th><?php echo $quote[0]->contact_field; ?></th>
		<th>Email</th>
        <th>Zip</th>
    </tr>
    <tr>
    	<td><?php echo $quote[0]->contact_field_value; ?></td>
        <td><?php echo $quote[0]->email;?></td>
        <td><?php echo $quote[0]->zip;?></td>
    </tr>
</table>
</fieldset>

<!----------------------------------------------------------------------------------------------->
<br>
<fieldset>
<legend>Insured Information</legend>
<table>
	<tr align="left">
    <?php
		$p = '';
		$r = count($previous_quote_ids);
		for($i=0;$i<$r;$i++){
			$p_1 = isset($insured_logs['insured_fname'][$i]) ?  $insured_logs['insured_fname'][$i] : '';
			$p_2 = isset($insured_logs['insured_mname'][$i]) ?  $insured_logs['insured_mname'][$i] : '';
			$p_3 = isset($insured_logs['insured_lname'][$i]) ?  $insured_logs['insured_lname'][$i] : '';
			
			$pr = isset($p_1->value) ? $p_1->value : '';
			$pr .= isset($p_2->value) ? ' '.$p_2->value : '';
			$pr .= isset($p_3->value) ? ' '.$p_3->value : '';
			$created_0 = isset($p_1->created_by_name) ? $p_1->created_by_name : '';
			$created_1 = isset($p_2->created_by_name) ? $p_2->created_by_name : '';
			$created_2 = isset($p_3->created_by_name) ? $p_3->created_by_name : '';	
			$created_3 = ($created_1) ? $created_1 : ''; 	 
			$created_by_name = ($created_0) ? $created_0 : ($created_2) ? $created_2 : $created_3 ; 
			
			$created_n0 = isset($p_1->created_on) ? $p_1->created_on : '';
			$created_n1 = isset($p_2->created_on) ? $p_2->created_on : '';
			$created_n2 = isset($p_3->created_on) ? $p_3->created_on : '';
			$created_n3 = ($created_n2) ? $created_n2 : '';
			$created_on = ($created_n0) ? $created_n0 : ($created_n2) ? $created_n2 : $created_n3 ;
			
			if($pr){
			  $val = array('value' => $pr, 'created_by_name' => $created_by_name, 'created_on' => $created_on);
			  $p[] = (object) $val;
			}
		}
		$data['data'] = $p;		
		?>  
    	<th class="<?php echo ($p) ? 'highlighted' : ''; ?>">Insured
		<a href="#i_name" class="fancybox"><?php echo (count($p)>0) ? revision_controll_icon($p) : ''; ?></a>
        <div id="i_name" style="display:none;width:auto;">
        	<?php $this->load->view('includes/show-field-history', $data); ?>
        </div>
        </th>
        <th class="<?php echo isset($insured_logs['insured_dba']) ? (count($insured_logs['insured_dba'])>0) ? 'highlighted' : '' : ''; ?>">DBA
        <?php 
		$log = isset($insured_logs['insured_dba']) ? $insured_logs['insured_dba'] : '';
		get_revisions('i_dba', $log);
		?>
        </th>
        <th class="<?php echo isset($insured_logs['insured_email']) ? (count($insured_logs['insured_email'])>0) ? 'highlighted' : '' : ''; ?>">Email
        <?php 
		$log = isset($insured_logs['insured_email']) ? $insured_logs['insured_email'] : '';
		get_revisions('i_email', $log);
		?>
        </th>				
	</tr>
	<tr>
    	<td><?php echo $insured->insured_fname.' '.$insured->insured_mname.' '.$insured->insured_lname;?></td>
        <td><?php echo $insured->insured_dba;?></td>
        <td><?php echo $insured->insured_email;?></td>
	</tr>
</table>

<table>
	<tr align="left">
    	<th class="<?php echo isset($insured_logs['insured_telephone']) ? (count($insured_logs['insured_telephone'])>0) ? 'highlighted' : '' : ''; ?>">Phone Number
        <?php 
		$log = isset($insured_logs['insured_telephone']) ? $insured_logs['insured_telephone'] : '';
		get_revisions('i_telephone', $log);
		?>
        </th>
    	<th class="<?php echo isset($insured_logs['insured_garaging_city']) ? (count($insured_logs['insured_garaging_city'])>0) ? 'highlighted' : '' : ''; ?>">Garaging City
        <?php 
		$log = isset($insured_logs['insured_garaging_city']) ? $insured_logs['insured_garaging_city'] : '';
		get_revisions('i_g_city', $log);
		?>
        </th>
		<th class="<?php echo isset($insured_logs['insured_state']) ? (count($insured_logs['insured_state'])>0) ? 'highlighted' : '' : ''; ?>">State
        <?php 
		$log = isset($insured_logs['insured_state']) ? $insured_logs['insured_state'] : '';
		get_revisions('i_state', $log);
		?>
        </th>
    	<th class="<?php echo isset($insured_logs['insured_zip']) ? (count($insured_logs['insured_zip'])>0) ? 'highlighted' : '' : ''; ?>">Zip
        <?php 
		$log = isset($insured_logs['insured_zip']) ? $insured_logs['insured_zip'] : '';
		get_revisions('i_zip', $log);
		?>
        </th>
	</tr>
	<tr>
    	<td><?php echo $insured->insured_telephone;?></td>
        <td><?php echo $insured->insured_garaging_city;?></td>
		<td><?php echo $insured->insured_state;?></td>
    	<td><?php echo $insured->insured_zip;?></td>
	</tr>
</table>
<table>
	<tr>
    	<th class="<?php echo isset($insured_logs['nature_business']) ? (count($insured_logs['nature_business'])>0) ? 'highlighted' : '' : ''; ?>">Nature of Business
        <?php 
		$log = isset($insured_logs['nature_business']) ? $insured_logs['nature_business'] : '';
		get_revisions('i_nat_b', $log);
		?>
        </th>
		<th class="<?php echo isset($insured_logs['commodities_haulted']) ? (count($insured_logs['commodities_haulted'])>0) ? 'highlighted' : '' : ''; ?>">Commodities Hauled
         <?php 
		$log = isset($insured_logs['commodities_haulted']) ? $insured_logs['commodities_haulted'] : '';
		get_revisions('i_com_h', $log);
		?>
        </th>
		<th class="<?php echo isset($insured_logs['year_in_business']) ? (count($insured_logs['year_in_business'])>0) ? 'highlighted' : '' : ''; ?>">Years in Business
         <?php 
		$log = isset($insured_logs['year_in_business']) ? $insured_logs['year_in_business'] : '';
		get_revisions('i_yib', $log);
		?>
        </th>
    </tr>
	
    <tr>
    	<td><?php echo  $insured->nature_business; ?></td>
		<td><?php echo  $insured->commodities_haulted; ?></td>
		<td><?php echo  $insured->year_in_business; ?></td>
    </tr>
</table>
</fieldset>

<!----------------------------------------------------------------------------------------------->
<br>

<fieldset>
<legend>Loss(es) Information</legend>
<?php
if(count($previous_quote_ids)>1){
	$this->load->model('quote_model');
?>
<div class="row-fluid">
<label><b>Vehicls Versions</b></legend>
<table style="margin-bottom: 9px;margin-top: 5px;">
  <thead>
    <tr>
      <th>Versions</th>
      <th>Liability Losses</th>
      <th>PD Losses</th>
      <th>Cargo Losses</th>
      <th>Other1 Losses</th>
      <th>Other2 Losses</th>
    </tr>
  </thead>
  <tbody>
	<?php 
	$l_count1 = count($losses_liability);
	$p_count1 = count($losses_pd);
	$c_count1 = count($losses_cargo);
	$o_count1 = count($losses_other1);
	$oo_count1 = count($losses_other2);
	
    $previous_quote_ids_1 = $previous_quote_ids;
    $v=count($previous_quote_ids_1)-2;
	//$v=0;
    rsort($previous_quote_ids_1);    
    foreach($previous_quote_ids_1 as $key => $previous_quote_id){
	  if($quote_id!=$previous_quote_id){					
		$v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
		
	   //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
	   $no_liability = $this->quote_model->get_losses_by_type($previous_quote_id, 'losses_liability');
	   $no_pd = $this->quote_model->get_losses_by_type($previous_quote_id, 'losses_pd');
	   $no_cargo = $this->quote_model->get_losses_by_type($previous_quote_id, 'losses_cargo');
	   $no_other1 = $this->quote_model->get_losses_by_type($previous_quote_id, 'losses_other1_specify_other');
	   $no_other2 = $this->quote_model->get_losses_by_type($previous_quote_id, 'losses_other2_specify_other');
	   
	   $l_count = count($no_liability);
	   $p_count = count($no_pd);
	   $c_count = count($no_cargo);
	   $o_count = count($no_other1);
	   $oo_count = count($no_other2);
	   
	   $l_version_diff = '';$p_version_diff = '';$c_version_diff = '';$o_version_diff = '';$oo_version_diff = '';
	   
	   if($l_count!=$l_count1){
	   	$l_version_diff = 'onclick="losse_version_diff('.$previous_quote_id.', '.$v_1.', &quot;losses_liability&quot;)"';
	   }
	   if($p_count!=$p_count1){
	   	$p_version_diff = 'onclick="losse_version_diff('.$previous_quote_id.', '.$v_1.', &quot;losses_pd&quot;)"';
	   }
	   if($c_count!=$c_count1){
	   	$c_version_diff = 'onclick="losse_version_diff('.$previous_quote_id.', '.$v_1.', &quot;losses_cargo&quot;)"';
	   }
	   if($o_count!=$o_count1){
	   	$o_version_diff = 'onclick="losse_version_diff('.$previous_quote_id.', '.$v_1.', &quot;losses_other1_specify_other&quot;)"';
	   }
	   if($oo_count!=$oo_count1){
	   	$oo_version_diff = 'onclick="losse_version_diff('.$previous_quote_id.', '.$v_1.', &quot;losses_other2_specify_other&quot;)"';
	   }
		?>
		<tr>
			<td>v<?= $v; ?></td>
			<td <?= $l_version_diff; ?> style="cursor: pointer;"><?= count($no_liability); ?></td>
			<td <?= $p_version_diff; ?> style="cursor: pointer;"><?= count($no_pd) ?></td>
            <td <?= $c_version_diff; ?> style="cursor: pointer;"><?= count($no_cargo) ?></td>
            <td <?= $o_version_diff; ?> style="cursor: pointer;"><?= count($no_other1) ?></td>
            <td <?= $oo_version_diff; ?> style="cursor: pointer;"><?= count($no_other1) ?></td>
		</tr>
		<?php 
		$l_count1 = count($no_liability);
	    $p_count1 = count($no_pd);
		$c_count1 = count($no_cargo);
		$o_count1 = count($no_other1);
		$oo_count1 = count($no_other1);
		$v--;
		//$v++;
	  }
    }
    ?>
  </tbody>
</table>
</div>
<?php
}
?>
<table>
	<tr align="left">
		<th>Number of Losses</th>
		<td>Liability : <?php echo count($losses_liability); ?></td>
		<td>PD   : <?php echo count($losses_pd); ?></td>
        <td>Cargo   : <?php echo count($losses_cargo); ?></td>
        <td>Other1   : <?php echo count($losses_other1); ?></td>
        <td>Other2   : <?php echo count($losses_other2); ?></td>
	</tr>
</table>
<?php losses($losses_liability, $previous_quote_ids, $losses_liability_diff, 'losses_liability'); ?>
<?php losses($losses_pd, $previous_quote_ids, $losses_pd_diff, 'losses_pd'); ?>
<?php losses($losses_cargo, $previous_quote_ids, $losses_cargo_diff, 'losses_cargo'); ?>
<?php losses($losses_other1, $previous_quote_ids, $losses_other1_diff, 'losses_other1'); ?>
<?php losses($losses_other2, $previous_quote_ids, $losses_other2_diff, 'losses_other2'); ?>
</fieldset>
<?php
function losses($losses, $previous_quote_ids, $diff_data, $type=''){
	if(is_array($losses)){
		if(!empty($losses)){
	?>
	<table class="table">
	  <tr>
		  <th colspan="6" style="text-align:left">Losses Type</th>
		  <td colspan="1"><?php echo $type;  ?></td>
	  </tr>
	  <tr>
		<th colspan="3" >Period of losses</th>
		<th rowspan="2" >Amount</th>
		<th rowspan="2" >Company</th>
        <th rowspan="2" >General <br /> Agent</th>
        <th rowspan="2" >Date of Loss</th>
	  </tr>
	  <tr colspan="1" >
      	<th>Versions</th>
		<th>From (mm/dd/yyyy)</th>
		<th>To (mm/dd/yyyy)</th>
	  </tr>        
  <?php $i = 0; foreach($losses as $losse) { ?>
  	 <tr style="display:none;" id="<?= $type.'_'.$i;?>">
      <td>
      <table>
      <tr>
		  <th colspan="6" style="text-align:left">Losses Type</th>
		  <td colspan="1"><?php echo $type;  ?></td>
	  </tr>
	  <tr>
		<th colspan="3" >Period of losses</th>
		<th rowspan="2" >Amount</th>
		<th rowspan="2" >Company</th>
        <th rowspan="2" >General <br /> Agent</th>
        <th rowspan="2" >Date of Loss</th>
	  </tr>
	  <tr colspan="1" >
      	<th>Versions</th>
		<th>From (mm/dd/yyyy)</th>
		<th>To (mm/dd/yyyy)</th>
	  </tr>  
      <?php
      	$j = 0;
		$r_1 = 0;
		//rsort($previous_quote_ids);
		if(isset($previous_quote_ids)){
		  if(isset($previous_quote_ids)){
			foreach($previous_quote_ids as $previous_quote_id){
			  if(isset($diff_data[$previous_quote_id][$i])){
				$diff_field = $diff_data[$previous_quote_id][$i]; 
				if(count($diff_field)>2){
				?>
                <tr>
                  <td>v<?= $j?></td>
                  <td><?php echo isset($diff_field['losses_from_date']) ? date('m/d/Y', strtotime($diff_field['losses_from_date'])) : ''; ?></td>
                  <td><?php echo isset($diff_field['losses_to_date']) ? date('m/d/Y', strtotime($diff_field['losses_to_date'])) : ''; ?></td>
                  <td><?php echo isset($diff_field['losses_amount']) ? $diff_field['losses_amount'] : ''; ?></td>
                  <td><?php echo isset($diff_field['losses_company']) ? $diff_field['losses_company'] : ''; ?></td>
                  <td><?php echo isset($diff_field['losses_general_agent']) ? $diff_field['losses_general_agent'] : ''; ?></td>
                  <td><?php echo isset($diff_field['date_of_loss']) ? date('m/d/Y', strtotime($diff_field['date_of_loss'])) : ''; ?></td>
                </tr>
                <?php
				$r_1++;
				}
			  }
			  $j++;
			}
		  }
		}
	  ?>
      </table>
      </td>
      </tr>
	  <tr class="<?php echo ($r_1) ? 'highlighted' : ''; ?>">
      	  <td>Current</td>
		  <td><?php echo date('m/d/Y', strtotime($losse->losses_from_date)); ?></td>
		  <td><?php echo date('m/d/Y', strtotime($losse->losses_to_date)); ?></td>
		  <td><?php echo $losse->losses_amount; ?></td>
		  <td><?php echo $losse->losses_company; ?></td>
		  <td><?php echo $losse->losses_general_agent; ?></td>
		  <td><?php echo date('m/d/Y', strtotime($losse->date_of_loss)); ?>
		  <a href="#<?= $type.'_'.$i;?>" class="fancybox"><?php echo revision_controll_icon(1); ?></a></td>
	  </tr>
      
  <?php $i++; } //IF Ends?>
  </table>	
  <?php
		}
  }
}
?>
<!----------------------------------------------------------------------------------------------->
<br>
<fieldset>
<legend>Vehicle Schedule Section</legend>
<?php
if(count($previous_quote_ids)>1){
	$this->load->model('quote_model');
?>
<div class="row-fluid">
<label><b>Vehicls Versions</b></legend>
<table style="margin-bottom: 9px;margin-top: 5px;">
  <thead>
    <tr>
      <th>Versions</th>
      <th>No of Tractors</th>
      <th>No of Trucks</th>
    </tr>
  </thead>
  <tbody>
	<?php 
	$tra_count1 = count($tractors);
	$tru_count1 = count($trucks);
    $previous_quote_ids_1 = $previous_quote_ids;
    $v=count($previous_quote_ids_1)-2;
	//$v=0;
    rsort($previous_quote_ids_1);    
    foreach($previous_quote_ids_1 as $key => $previous_quote_id){
	  if($quote_id!=$previous_quote_id){					
		$v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
		
	   //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
	   $no_tractors = $this->quote_model->get_all_vehicle($previous_quote_id, 'TRACTOR');
	   $no_trucks = $this->quote_model->get_all_vehicle($previous_quote_id, 'TRUCK');
	   $tra_count = count($no_tractors);
	   $tru_count = count($no_trucks);
	   $tra_version_diff = '';
	   $tru_version_diff = '';
	   if($tra_count!=$tra_count1){
	   	$tra_version_diff = 'onclick="version_diff('.$previous_quote_id.', '.$v_1.', &quot;TRACTOR&quot;)"';
	   }
	   if($tru_count!=$tru_count1){
	   	$tru_version_diff = 'onclick="version_diff('.$previous_quote_id.', '.$v_1.', &quot;TRUCK&quot;)"';
	   }
		?>
		<tr>
			<td>v<?= $v; ?></td>
			<td <?= $tra_version_diff; ?> style="cursor: pointer;"><?= count($no_tractors); ?></td>
			<td <?= $tru_version_diff; ?> style="cursor: pointer;"><?= count($no_trucks) ?></td>
		</tr>
		<?php 
		$tra_count1 = count($no_tractors);
	    $tru_count1 = count($no_trucks);
		$v--;
		//$v++;
	  }
    }
    ?>
  </tbody>
</table>
</div>
<?php
}
?>
<table>
	<tr align="left">
		<th>Number of Vehicles</th>
		<td>Tractor : <?php echo count($tractors); ?></td>
		<td>Truck   : <?php echo count($trucks); ?></td>
	</tr>
</table>
<br/>


<!-- For Tractors and Trailers -->

<?php 
function vehicle_rows($vehicles='', $previous_quote_ids, $diff_data, $v_type='Tractors', $i=0){
	$CI = & get_instance();
	$data = '';
	$data['radiou_of_operation'] = '';$data['radius_of_operation_other']='';$data['request_for_radius_of_operation']='';$data['number_of_axles']='';$data['spacial_request']='';$data['vehicle_year']='';$data['make_model']='';$data['gvw']='';$data['vin']='';$data['liability']='';$data['liability_ded']='';$data['um']='';$data['pip']='';$data['pd']='';$data['pd_ded']='';$data['cargo']='';$data['cargo_ded']='';$data['commodities_haulted']='';$data['refrigerated_break_down']='';$data['break_down_deductible'] ='';$data['put_on_truck']='';
if(is_array($diff_data)) {
			$j = 0;
			if(isset($previous_quote_ids)){
			  if(isset($previous_quote_ids)){
				foreach($previous_quote_ids as $key => $previous_quote_id){
				  if(isset($diff_data[$previous_quote_id][$i])){
					$diff_field = $diff_data[$previous_quote_id][$i]; 
					$next_quote_id = isset($previous_quote_ids[$key+1]) ? $previous_quote_ids[$key+1]  :'';
					$version_detail = get_version_detail($next_quote_id);
					$created_by_name = isset($version_detail->requester_id) ? get_username($version_detail->requester_id) : '';
					$created_on = isset($version_detail->date_added) ? $version_detail->date_added : '';
					if(isset($diff_field['radius_of_operation'])){						
						$rop = array('value' =>$diff_field['radius_of_operation'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						 $data['radiou_of_operation'][] = (object)$rop;
					}
					if(isset($diff_field['radius_of_operation_other'])){
						$ropo = array('value' => $diff_field['radius_of_operation_other'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['radius_of_operation_other'][] = (object)$ropo;
					}
					if(isset($diff_field['request_for_radius_of_operation'])){
						$rrop = array('value' =>$diff_field['request_for_radius_of_operation'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['request_for_radius_of_operation'][] = (object)$rrop;
					}
					if(isset($diff_field['number_of_axles'])){
						$nax = array('value' => $diff_field['number_of_axles'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['number_of_axles'][] = (object)$nax;
					}
					if(isset($diff_field['vaule'])){
						$sp = array('value' => $diff_field['vaule'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['spacial_request'][] = (object)$sp;
					}
					if(isset($diff_field['vehicle_year'])){
						$vhy = array('value' => $diff_field['vehicle_year'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['vehicle_year'][] = (object)$vhy;
					}
					if(isset($diff_field['make_model'])){
						$vmkm = array('value' => $diff_field['make_model'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['make_model'][] = (object)$vmkm;
					}
					if(isset($diff_field['gvw'])){
						$gvw = array('value' => $diff_field['gvw'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['gvw'][] = (object)$gvw;
					}
					if(isset($diff_field['vin'])){
						$vin = array('value' => $diff_field['vin'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['vin'][] = (object)$vin;
					}
					if(isset($diff_field['liability'])){
						$liability = array('value' => $diff_field['liability'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['liability'][] = (object)$liability;
					}
					if(isset($diff_field['liability_ded'])){
						$liab_ded = array('value' => $diff_field['liability_ded'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['liability_ded'][] = (object)$liab_ded;
					}
					if(isset($diff_field['um'])){
						$um = array('value' => $diff_field['um'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['um'][] = (object)$um;
					}
					if(isset($diff_field['pip'])){
						$pip = array('value' => $diff_field['pip'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['pip'][] = (object)$pip;
					}
					if(isset($diff_field['pd'])){
						$pd = array('value' => $diff_field['pd'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['pd'][] = (object)$pd;
					}
					if(isset($diff_field['pd_ded'])){
						$pd_ded = array('value' => $diff_field['pd_ded'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['pd_ded'][] = (object)$pd_ded;
					}
					if(isset($diff_field['cargo'])){
						$cargo = array('value' => $diff_field['cargo'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['cargo'][] = (object)$cargo;
					}
					if(isset($diff_field['cargo_ded'])){
						$cargo_ded = array('value' => $diff_field['cargo_ded'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['cargo_ded'][] = (object)$cargo_ded;
					}
					
					if(isset($diff_field['refrigerated_break_down'])){
						$refrigerated_break_down = array('value' => $diff_field['refrigerated_break_down'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['refrigerated_break_down'][] = (object)$refrigerated_break_down;
					}
					if(isset($diff_field['break_down_deductible'])){
						$break_down_deductible = array('value' => $diff_field['break_down_deductible'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['break_down_deductible'][] = (object)$break_down_deductible;
					}
					if(isset($diff_field['put_on_truck'])){
						$put_on_truck = array('value' => $diff_field['put_on_truck'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['put_on_truck'][] = (object)$put_on_truck;
					}
					
					
					if(isset($diff_field['commodities_haulted'])){
						$com_hl = array('value' => $diff_field['commodities_haulted'], 'created_by_name' => $created_by_name, 'created_on' => $created_on);
						$data['commodities_haulted'][] = (object)$com_hl;
					}
				  }
				  $j++;
				}
			  }
			}
			return $data;
		}	
}
$trailer_start = 0;
$i = 0;
$i_1 = 0;

?>

<?php
foreach($tractors as $tractor) { ?>
<!--<div style="border:2px solid #7BB33D;">-->
<table>
	<tr><th colspan="5">Tractor #<?php echo $i_1+1;?></th></tr>
	<tr align="left">
		<th>Coverage selected:</th>
		<?php if($tractor->liability!='') { ?><td>Liability</td><?php } ?>
		<?php if(($tractor->pd != '') || ($tractor->pd_ded != '')) { ?><td>PD</td><?php } ?>
		<?php if(($tractor->cargo!= '') || ($tractor->cargo_ded != '')) { ?><td>Cargo</td><?php } ?>
	</tr>
</table>
<br/>	
<?php $tractor_log = vehicle_rows('', $previous_quote_ids, $tractors_diff, $v_type='Tractors', $i_1);?>

<table>
    <tr align="left">
		<!--th>How many trailors associated with this tractor?</th-->
		<th class="<?php echo ($tractor_log['radiou_of_operation']) ? 'highlighted' : ''; ?>">Radius of Operation
         <?php 
		$log = isset($tractor_log['radiou_of_operation']) ? $tractor_log['radiou_of_operation'] : '';
		get_revisions('vt_rop_'.$i_1, $log);
		?>
        </th>
		<?php if($tractor->radius_of_operation_other!= '') { echo '<th>Specify Other</th>'; } ?>	
	    <th class="<?php echo ($tractor_log['request_for_radius_of_operation']) ? 'highlighted' : ''; ?>">Any Special Request for Radius
        <?php 
		$log = isset($tractor_log['request_for_radius_of_operation']) ? $tractor_log['request_for_radius_of_operation'] : '';
		get_revisions('vt_rrp_'.$i_1, $log);
		?>
        </th>
		<th class="<?php echo ($tractor_log['number_of_axles']) ? 'highlighted' : ''; ?>">Number of Axles
        <?php 
		$log = isset($tractor_log['number_of_axles']) ? $tractor_log['number_of_axles'] : '';
		get_revisions('vt_nax_'.$i_1, $log);
		?>
        </th>
	</tr>
	<tr>
		<!--td><?php echo $total_trailers; ?></td-->
		<td><?php echo $tractor->radius_of_operation ?></td>
	    <?php if($tractor->radius_of_operation_other!= '') echo '<td>'.$tractor->radius_of_operation_other.'</td>'; ?>
		<td><?php echo $tractor->request_for_radius_of_operation; ?></td>
		<td><?php echo $tractor->number_of_axles;?></td>
	</tr>
</table>
<table>
	<tr align="left">
		<th class="<?php echo ($tractor_log['vehicle_year']) ? 'highlighted' : ''; ?>">Vehicle Year
        <?php 
		$log = isset($tractor_log['vehicle_year']) ? $tractor_log['vehicle_year'] : '';
		get_revisions('vt_vhy_'.$i_1, $log);
		?>
        </th>
		<th class="<?php echo ($tractor_log['make_model']) ? 'highlighted' : ''; ?>">Make Model
        <?php 
		$log = isset($tractor_log['make_model']) ? $tractor_log['make_model'] : '';
		get_revisions('vt_vmkm_'.$i_1, $log);
		?>
        </th>
		<th class="<?php echo ($tractor_log['gvw']) ? 'highlighted' : ''; ?>">GVW
        <?php 
		$log = isset($tractor_log['gvw']) ? $tractor_log['gvw'] : '';
		get_revisions('vt_gvw_'.$i_1, $log);
		?>
        </th>
        <th class="<?php echo ($tractor_log['vin']) ? 'highlighted' : ''; ?>">VIN
        <?php 
		$log = isset($tractor_log['vin']) ? $tractor_log['vin'] : '';
		get_revisions('vt_vin_'.$i_1, $log);
		?>
        </th>
        <?php if($tractor->vaule != '') {?><th class="<?php echo ($tractor_log['spacial_request']) ? 'highlighted' : ''; ?>">Special Request
         <?php 
		$log = isset($tractor_log['spacial_request']) ? $tractor_log['spacial_request'] : '';
		get_revisions('vt_sp_'.$i_1, $log);
		?>
        </th><?php } ?>
	</tr>
	<tr>
		<td><?php echo $tractor->vehicle_year; ?></td>
        <td><?php echo $tractor->make_model; ?></td>
        <td><?php echo $tractor->gvw; ?></td>
        <td><?php echo $tractor->vin; ?></td>
        <?php if($tractor->vaule!= '') { ?>
			<td><?php echo $tractor->vaule; ?></td>
		<?php } ?>
	</tr>
</table>
<?php if($tractor->liability!= ''||$tractor->liability_ded != ''||$tractor->um != ''||$tractor->pip != '') { ?>
<table>
    <tr>
    	<?php if($tractor->liability != '') {?> <th class="<?php echo ($tractor_log['liability']) ? 'highlighted' : ''; ?>">Liability
        <?php 
		$log = isset($tractor_log['liability']) ? $tractor_log['liability'] : '';
		get_revisions('vt_liab_'.$i_1, $log);
		?>
        </th><?php } ?>
        <?php if($tractor->liability_ded != '') {?> <th class="<?php echo ($tractor_log['liability_ded']) ? 'highlighted' : ''; ?>">Liability Deductible
        <?php 
		$log = isset($tractor_log['liability_ded']) ? $tractor_log['liability_ded'] : '';
		get_revisions('vt_liab_ded_'.$i_1, $log);
		?>
        </th><?php } ?>
		<?php if($tractor->um != ''){ ?> <th class="<?php echo ($tractor_log['um']) ? 'highlighted' : ''; ?>">UM
        <?php 
		$log = isset($tractor_log['um']) ? $tractor_log['um'] : '';
		get_revisions('vt_um_'.$i_1, $log);
		?>
        </th><?php } ?>
		<?php if($tractor->pip != '') {?><th class="<?php echo ($tractor_log['pip']) ? 'highlighted' : ''; ?>">PIP
        <?php 
		$log = isset($tractor_log['pip']) ? $tractor_log['pip'] : '';
		get_revisions('vt_pip_'.$i_1, $log);
		?>
        </th><?php } ?>
    </tr>
    <tr>
		<?php if($tractor->liability != '') echo '<td>'.$tractor->liability.'</td>'; ?>
        <?php if($tractor->liability_ded != '') echo '<td>'.$tractor->liability_ded.'</td>'; ?>
		<?php if($tractor->um != '') echo '<td>'.$tractor->um.'</td>'; ?>
		<?php if($tractor->pip != '') echo '<td>'.$tractor->pip.'</td>'; ?>
    </tr>
</table>
<?php }  ?>

<?php if($tractor->pd != ''||$tractor->pd_ded != '') {?>
<table>
	<tr>
		<?php if($tractor->pd != ''){?> <th class="<?php echo ($tractor_log['pd']) ? 'highlighted' : ''; ?>">PD
        <?php 
		$log = isset($tractor_log['pd']) ? $tractor_log['pd'] : '';
		get_revisions('vt_pd_'.$i_1, $log);
		?>
        </th><?php } ?>
        <?php if($tractor->pd_ded!= '') {?> <th class="<?php echo ($tractor_log['pd_ded']) ? 'highlighted' : ''; ?>">PD Deductible
        <?php 
		$log = isset($tractor_log['pd_ded']) ? $tractor_log['pd_ded'] : '';
		get_revisions('vt_pd_d_'.$i_1, $log);
		?>
        </th><?php } ?>
	</tr>
	<tr>
		<?php if($tractor->pd != '') echo '<td>'.$tractor->pd.'</td>'; ?>
        <?php if($tractor->pd_ded != '') echo '<td>'.$tractor->pd_ded.'</td>'; ?>
	</tr>
</table>
<?php } ?>

<?php if($tractor->cargo != ''||$tractor->cargo_ded != '') { ?>
<table>
	<tr>
		<?php if($tractor->cargo != '') { ?> <th class="<?php echo ($tractor_log['cargo']) ? 'highlighted' : ''; ?>">Cargo
        <?php 
		$log = isset($tractor_log['cargo']) ? $tractor_log['cargo'] : '';
		get_revisions('vt_car_'.$i_1, $log);
		?>
        </th><?php } ?>
        <?php if($tractor->cargo_ded != '') { ?> <th class="<?php echo ($tractor_log['cargo_ded']) ? 'highlighted' : ''; ?>">Cargo Deductible
        <?php 
		$log = isset($tractor_log['cargo_ded']) ? $tractor_log['cargo_ded'] : '';
		get_revisions('vt_car_d_'.$i_1, $log);
		?>
        </th><?php } ?>
	</tr>
	<tr>
		<?php if($tractor->cargo != '') echo '<td>'.$tractor->cargo.'</td>'; ?>
        <?php if($tractor->cargo_ded != '') echo '<td>'.$tractor->cargo_ded.'</td>'; ?>		
	</tr>
</table>
<?php } ?>
<table>
	<tr>
		<th class="<?php echo ($tractor_log['commodities_haulted']) ? 'highlighted' : ''; ?>" >Commodities hauled
        <?php 
		$log = isset($tractor_log['commodities_haulted']) ? $tractor_log['commodities_haulted'] : '';
		get_revisions('vt_com_h_'.$i_1, $log);
		?>
        </th>
		<th>Number of Trailers Pulled with this tractor</th>
	</tr>
	<tr>
		<td><?php if($tractor->commodities_haulted) echo $tractor->commodities_haulted; ?></td>
		<td><?php echo count($trailers); ?></td>
	</tr>
</table>

<!--<table>
	<tr>
    	<th>Refrigerated Breakdown</th>
        <th>Deductible</th>
        <th>Type of Cargo Carried</th>
    </tr>
	<tr>
    	<td><?php echo isset($_POST['tractor_refrigerated_breakdown'][$i])? "YES": "NO";?></td>
        <td><?php echo isset($_POST['tractor_refrigerated_breakdown'][$i])? $_POST['tractor_deductible'][$i]: "";?></td>
        <td><?php echo isset($_POST[($i+1).'tractor_cargo_specifier'])? implode(',',$_POST[($i+1).'tractor_cargo_specifier']): "";?></td>
    </tr>
</table>-->
<br>
<?php 
if(isset($_POST['vehicle_trailer_no'][$i])) { 
	$tractor_trailer = $_POST['vehicle_trailer_no'][$i] + $trailer_start; 
} else { 
	$tractor_trailer = '0'; 
}
$trailer_number = 1;


$trailer_start_in = $trailer_start;
$i = 1;
foreach($trailers as $trailer) { ?>
<!--<div style="border:2px solid #F60;">-->
<table>
	<tr><th colspan="5">Trailer #<?php echo $i;?></th></tr>
	<tr align="left">
		<th>Coverage selected:</th>
		<?php if($trailer->liability!='') { ?><td>Liability</td><?php } ?>
		<?php if(($trailer->pd != '') || ($trailer->pd_ded != '')) { ?><td>PD</td><?php } ?>
		<?php if(($trailer->cargo != '') || ($trailer->cargo_ded != '') || ($trailer->refrigerated_break_down != '')) { ?><td>Cargo</td><?php } ?>
	</tr>
</table>
<table>
    <tr align="left">
		<th>Vehicle Year</th>
		<th>Make Model</th>
		<th>GVW</th>
        <th>VIN</th>
		<th>Trailer Type</th>
		<th>Owned Vehicle</th>
        <!--<th>Special Request</th>-->
	</tr>
	<tr>
		<td><?php echo $trailer->vehicle_year; ?></td>
        <td><?php echo $trailer->make_model; ?></td>
        <td><?php echo $trailer->gvw; ?></td>
        <td><?php echo $trailer->vin; ?></td>
		<td><?php echo $trailer->type_of_trailer; ?></td>
		<td><?php echo $trailer->owned_vehicle; ?></td>
        <!--<td><?php if(isset($_POST['trailer_value_amount_vh'][$j])) echo $_POST['trailer_value_amount_vh'][$j]; ?></td>-->
	</tr>
</table>

<table>
    <tr>
		<?php if($trailer->liability != '') echo '<th>Liability</th>'; ?>
        <?php if($trailer->liability_ded != '') echo '<th>Liability Deductible</th>'; ?>
    
        <?php if($trailer->pd != '') echo '<th>PD</th>'; ?>
        <?php if($trailer->pd_ded != '') echo '<th>PD Deductible</th>'; ?>		
    </tr>
    <tr>
		<?php if($trailer->liability != '') echo '<td>'.$trailer->liability.'</td>'; ?>
        <?php if($trailer->liability_ded != '') echo '<td>'.$trailer->liability_ded.'</td>'; ?>
    
        <?php if($trailer->pd != '') echo '<td>'.$trailer->pd.'</td>'; ?>
        <?php if($trailer->pd_ded != '') echo '<td>'.$trailer->pd_ded.'</td>'; ?>
	</tr>
</table>
<?php if(($trailer->cargo != '') || ($trailer->cargo_ded != '')) { ?>
<table>
    <tr align="left">
		<?php if($trailer->cargo != '') { echo '<th>Cargo</th>'; } ?>
        <?php if($trailer->cargo_ded != '') { echo '<th>Cargo Deductible</th>'; } ?>
		<?php echo '<th>Refrigeration Breakdown</th>';  ?>
		<?php if($trailer->refrigerated_break_down != '') { echo '<th>Deductible</th>'; } ?>
	</tr>
	<tr>
		<td><?php echo $trailer->cargo; ?></td>
        <td><?php echo $trailer->cargo_ded; ?></td>
        <td><?php echo isset($trailer->refrigerated_break_down) && $trailer->refrigerated_break_down != '' ? "YES": "NO";?></td>
        <td><?php echo $trailer->refrigerated_break_down;?></td>
	</tr>
</table>
<?php } ?>

<!--</div>--><br>
<?php $trailer_start++; 
$trailer_number ++;
}//Inner Loop Ends (Trailors) ?>
<!--</div>-->
<br>
<?php $i_1++; } //Outer Loop Ends (Tractors) ?>
<!--END For Tractors and Trailers -->


<!-- For Trucks-->
<?php $i = 0; $i_1 = 0; foreach($trucks as $truck) { ?>
<!--<div style="border:2px solid #3DAAE9;">-->
<table>
	<tr><th colspan="5">Truck #<?php echo $i+1;?></th></tr>
	<tr align="left">
		<th>Coverage selected:</th>
		<?php if($truck->liability) { echo '<td>Liability</td>'; } ?>
		<?php if(($truck->pd != '') || ($truck->pd_ded != '')) { echo '<td>PD</td>'; } ?>
		<?php if(($truck->cargo != '') || ($truck->cargo_ded != '')) { echo '<td>Cargo</td>'; } ?>
	</tr>
</table>
<br/>	
<?php $truck_log = vehicle_rows('', $previous_quote_ids, $trcuk_diff, $v_type='Trucks', $i_1);?>

<table>
    <tr align="left">
		<th class="<?php echo isset($truck_log['radiou_of_operation']) ? ($truck_log['radiou_of_operation']) ? 'highlighted' : '' : ''; ?>">Radius of Operation
        <?php 
		$log = isset($truck_log['radiou_of_operation']) ? $truck_log['radiou_of_operation'] : '';
		get_revisions('vt1_rop_h_'.$i_1, $log);
		?>
        </th>
		<?php if ($truck->radius_of_operation_other != '') { ?> 
        <th class="<?php echo isset($truck_log['radius_of_operation_other']) ? ($truck_log['radius_of_operation_other']) ? 'highlighted' : '' : ''; ?>">Specify Other
        <?php 
		$log = isset($truck_log['radius_of_operation_other']) ? $truck_log['radius_of_operation_other'] : '';
		get_revisions('vt1_ropo_h_'.$i_1, $log);
		?>
        </th><?php } ?>
		<th class="<?php echo isset($truck_log['request_for_radius_of_operation']) ? ($truck_log['request_for_radius_of_operation']) ? 'highlighted' : '' : ''; ?>">Any Special Request for Radius
        <?php 
		$log = isset($truck_log['request_for_radius_of_operation']) ? $truck_log['request_for_radius_of_operation'] : '';
		get_revisions('vt1_rrop_h_'.$i_1, $log);
		?>
        </th>
        <th class="<?php echo isset($truck_log['number_of_axles']) ? ($truck_log['number_of_axles']) ? 'highlighted' : '' : ''; ?>">Number of Axles
        <?php 
		$log = isset($truck_log['number_of_axles']) ? $truck_log['number_of_axles'] : '';
		get_revisions('vt1_nax_h_'.$i_1, $log);
		?>
        </th>
	</tr>
	<tr>
		<td><?php echo $truck->radius_of_operation; ?></td>
		<?php if ($truck->radius_of_operation_other != '') echo  '<td>'.$truck->radius_of_operation_other.'</td>'; ?>
		<td><?php echo $truck->request_for_radius_of_operation; ?></td>
        <td><?php echo $truck->number_of_axles; ?></td>
	</tr>
</table>
<table>
	<tr align="left">
		<th class="<?php echo isset($truck_log['vehicle_year']) ? ($truck_log['vehicle_year']) ? 'highlighted' : '' : ''; ?>">Vehicle Year
        <?php 
		$log = isset($truck_log['vehicle_year']) ? $truck_log['vehicle_year'] : '';
		get_revisions('vt1_vy_h_'.$i_1, $log);
		?>
        </th>
		<th class="<?php echo isset($truck_log['make_model']) ? ($truck_log['make_model']) ? 'highlighted' : '' : ''; ?>">Make Model
        <?php 
		$log = isset($truck_log['make_model']) ? $truck_log['make_model'] : '';
		get_revisions('vt1_mkm_h_'.$i_1, $log);
		?>
        </th>
		<th class="<?php echo isset($truck_log['gvw']) ? ($truck_log['gvw']) ? 'highlighted' : '' : ''; ?>">GVW
        <?php 
		$log = isset($truck_log['gvw']) ? $truck_log['gvw'] : '';
		get_revisions('vt1_gvw_h_'.$i_1, $log);
		?>
        </th>
        <th class="<?php echo isset($truck_log['vin']) ? ($truck_log['vin']) ? 'highlighted' : '' : ''; ?>">VIN
        <?php 
		$log = isset($truck_log['vin']) ? $truck_log['vin'] : '';
		get_revisions('vt1_vin_h_'.$i_1, $log);
		?>
        </th>
		<?php if($truck->vaule) { ?> 
        <th class="<?php echo isset($truck_log['spacial_request']) ? ($truck_log['spacial_request']) ? 'highlighted' : '' : ''; ?>">Special req.
        <?php 
		$log = isset($truck_log['spacial_request']) ? $truck_log['spacial_request'] : '';
		get_revisions('vt1_sp_h_'.$i_1, $log);
		?>
        </th><?php } ?>
	</tr>
	<tr>
		<td><?php echo $truck->vehicle_year; ?></td>
        <td><?php echo $truck->make_model; ?></td>
        <td><?php echo $truck->gvw; ?></td>
        <td><?php echo $truck->vin; ?></td>
		<?php if($truck->vaule!='') { ?>
			<td><?php echo $truck->vaule; ?></td>
		<?php } ?>
	</tr>
</table>

<?php if($truck->liability != '' || $truck->liability_ded!='' || $truck->um!='' || $truck->pip!='') { ?>
<table>
    <tr>
    	<?php if($truck->liability != '') {?>
         <th class="<?php echo isset($truck_log['liability']) ? ($truck_log['liability']) ? 'highlighted' : '' : ''; ?>">Liability
        <?php 
		$log = isset($truck_log['liability']) ? $truck_log['liability'] : '';
		get_revisions('vt1_lia_h_'.$i_1, $log);
		?>
        </th><?php } ?>
        <?php if($truck->liability_ded != '') { ?>
        <th class="<?php echo isset($truck_log['liability_ded']) ? ($truck_log['liability_ded']) ? 'highlighted' : '' : ''; ?>">Liability Deductible
        <?php 
		$log = isset($truck_log['liability_ded']) ? $truck_log['liability_ded'] : '';
		get_revisions('vt1_rliab_ded_h_'.$i_1, $log);
		?>
        </th><?php } ?>
		<?php if($truck->um != '') { ?>
        <th class="<?php echo isset($truck_log['um']) ? ($truck_log['um']) ? 'highlighted' : '' : ''; ?>">UM
        <?php 
		$log = isset($truck_log['um']) ? $truck_log['um'] : '';
		get_revisions('vt1_um_h_'.$i_1, $log);
		?>
        </th><?php }  ?>
		<?php if($truck->pip != '') {?>
        <th class="<?php echo isset($truck_log['pip']) ? ($truck_log['pip']) ? 'highlighted' : '' : ''; ?>">PIP
        <?php 
		$log = isset($truck_log['pip']) ? $truck_log['pip'] : '';
		get_revisions('vt1_pip_h_'.$i_1, $log);
		?>
        </th><?php } ?>
    </tr>
    <tr>
		<?php  echo '<td>'.$truck->liability.'</td>'; ?>
        <?php  echo '<td>'.$truck->liability_ded.'</td>'; ?>
		<?php  echo '<td>'.$truck->um.'</td>'; ?>
		<?php  echo '<td>'.$truck->pip.'</td>'; ?>
	</tr>
</table>
<?php } ?>

<?php if(($truck->pd != '') || ($truck->pd_ded != '')) { ?>
<table>
	<tr>
		<th class="<?php echo isset($truck_log['pd']) ? ($truck_log['pd']) ? 'highlighted' : '' : ''; ?>">PD
        <?php 
		$log = isset($truck_log['pd']) ? $truck_log['pd'] : '';
		get_revisions('vt1_pd_h_'.$i_1, $log);
		?>
        </th>
        <th class="<?php echo isset($truck_log['pd_ded']) ? ($truck_log['pd_ded']) ? 'highlighted' : '' : ''; ?>">PD Deductible
        <?php 
		$log = isset($truck_log['pd_ded']) ? $truck_log['pd_ded'] : '';
		get_revisions('vt1_pd_d_h_'.$i_1, $log);
		?>
        </th>
	</tr>
	<tr>
		<?php  echo '<td>'.$truck->pd.'</td>'; ?>
        <?php  echo '<td>'.$truck->pd_ded.'</td>'; ?>
	</tr>
</table>
<?php } ?>

<?php if(($truck->cargo != '') || ($truck->cargo_ded != '')) { ?>
<table>
	<tr>
		<th class="<?php echo isset($truck_log['cargo']) ? ($truck_log['cargo']) ? 'highlighted' : '' : ''; ?>">Cargo
        <?php 
		$log = isset($truck_log['cargo']) ? $truck_log['cargo'] : '';
		get_revisions('vt1_car_h_'.$i_1, $log);
		?>
        </th>
        <th class="<?php echo isset($truck_log['cargo_ded']) ? ($truck_log['cargo_ded']) ? 'highlighted' : '' : ''; ?>">Cargo Deductible
        <?php 
		$log = isset($truck_log['cargo_ded']) ? $truck_log['cargo_ded'] : '';
		get_revisions('vt1_car_ded_h_'.$i_1, $log);
		?>
        </th>
    	<th class="<?php echo isset($truck_log['refrigerated_break_down']) ? ($truck_log['refrigerated_break_down']) ? 'highlighted' : '' : ''; ?>">Refrigeration Breakdown
        <?php 
		$log = isset($truck_log['refrigerated_break_down']) ? $truck_log['refrigerated_break_down'] : '';
		get_revisions('vt1_rfg_h_'.$i_1, $log);
		?>
        </th>
        <th class="<?php echo isset($truck_log['break_down_deductible']) ? ($truck_log['refrigerated_break_down']) ? 'highlighted' : '' : ''; ?>">Deductible
        <?php 
		$log = isset($truck_log['break_down_deductible']) ? $truck_log['break_down_deductible'] : '';
		get_revisions('vt1_rfg_d_h_'.$i_1, $log);
		?>
        </th>
    </tr>
    <tr>
		<?php echo '<td>'.$truck->cargo.'</td>'; ?>
        <?php echo '<td>'.$truck->cargo_ded.'</td>'; ?>
    	<?php /*echo isset($truck->refrigerated_break_down) && ($truck->refrigerated_break_down != '') ? "<td>YES</td>": "<td>NO</td>";*/?>
        <?php echo "<td>$truck->refrigerated_break_down</td>";?>
        <?php echo  '<td>'.$truck->break_down_deductible.'</td>';?>
    </tr>
</table>
<?php } ?>
<table>
	<tr>
		<th class="<?php echo isset($truck_log['commodities_haulted']) ? ($truck_log['commodities_haulted']) ? 'highlighted' : '' : ''; ?>">Commodities hauled
        <?php 
		$log = isset($truck_log['commodities_haulted']) ? $truck_log['commodities_haulted'] : '';
		get_revisions('vt1_rop_h_'.$i_1, $log);
		?>
        </th>
        <th class="<?php echo isset($truck_log['put_on_truck']) ? ($truck_log['put_on_truck']) ? 'highlighted' : '' : ''; ?>">Do you pull anything with this truck?
        <?php 
		$log = isset($truck_log['put_on_truck']) ? $truck_log['put_on_truck'] : '';
		get_revisions('vt1_pot_h_'.$i_1, $log);
		?>
        </th>
	</tr>
	<tr>
		<td><?php echo $truck->commodities_haulted; ?></td>
		<td><?php echo $truck->put_on_truck; ?></td>
	</tr>
</table>
<!--</div>-->
<br>
<?php $i_1++; } //Loop Ends (Trucks) ?>
<!-- END For Trucks-->
</fieldset>

<!----------------------------------------------------------------------------------------------->


<fieldset>
<legend>Schedule of Drivers</legend>
<?php $dr_count1 = count($drivers); ?>
<?php
if(count($previous_quote_ids)>1){
	$this->load->model('quote_model');
?>
<div class="row-fluid">
<label><b>Driver Versions</b></label>
<table style="margin-bottom: 9px;margin-top: 5px;">
  <thead>
    <tr>
      <th>Versions</th>
      <th>Number of Drivers</th>
    </tr>
  </thead>
  <tbody>
	<?php 
	$tra_count1 = count($tractors);
	$tru_count1 = count($trucks);
    $previous_quote_ids_1 = $previous_quote_ids;
    $v=count($previous_quote_ids_1)-2;
	//$v=0;
    rsort($previous_quote_ids_1);    
    foreach($previous_quote_ids_1 as $key => $previous_quote_id){
	  if($quote_id!=$previous_quote_id){					
		$v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
		
	   //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
	   $no_drivers = $this->quote_model->get_all_drivers($previous_quote_id);
	  
	   $dr_count = count($no_drivers);
	   $dr_version_diff = '';
	  
	   if($dr_count!=$dr_count1){
	   	$dr_version_diff = 'onclick="driver_version_diff('.$previous_quote_id.', '.$v_1.', &quot;Driver&quot;)"';
	   }
	  
		?>
		<tr>
			<td>v<?= $v; ?></td>
			<td <?= $dr_version_diff; ?> style="cursor: pointer;"><?= count($no_drivers); ?></td>
		</tr>        
		<?php 
		$dr_count1 = count($no_drivers);
		$v--;
		//$v++;
	  }
    }
    ?>
  </tbody>
</table>
</div>
<?php
}
?>
<table>
	<tr align="left">
		<th>Number of Drivers</th>
		<td><?php echo count($drivers); ?></td>
    </tr>
</table>
<?php if (count($drivers) > 0)  { ?>
<table>
	<tr align="left">
		<th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
		<th>Driver License number</th>
		<th>License Class</th>
		<th>Date Hired</th>
		<th>DOB</th>
		<th>Years Class A license</th>
		<th>License Issue State</th>
	</tr>
    <?php $i = 0; foreach($drivers as $driver){ 	?>
    <tr id="driver_<?= $i ?>" style="display:none;">
    <td>
    <table>
    <tr align="left">
        <th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
        <th>Driver License number</th>
        <th>License Class</th>
        <th>Date Hired</th>
        <th>DOB</th>
        <th>Years Class A license</th>
        <th>License Issue State</th>
    </tr>
	<?php
			$j = 0;
			$r_d = 0;
			
			if(isset($previous_quote_ids)){
			  if(isset($previous_quote_ids)){
				foreach($previous_quote_ids as $previous_quote_id){
				  if(isset($driver_diff[$previous_quote_id][$i])){
					$diff_field = $driver_diff[$previous_quote_id][$i]; 
					
					if(count($diff_field)>2){
					?>
                    
                     <tr>
                     	<!--<td>v<?= $j; ?></td>-->
                        <td><?php echo isset($diff_field['driver_name']) ? $diff_field['driver_name'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_middle_name']) ? $diff_field['driver_middle_name'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_last_name']) ? $diff_field['driver_last_name'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_licence_no']) ? $diff_field['driver_licence_no'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_license_class']) ? $diff_field['driver_license_class'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_license_issue_date']) ? date('m/d/Y', strtotime($diff_field['driver_license_issue_date'])) : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_dob']) ? date('m/d/Y', strtotime($diff_field['driver_dob'])) : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_class_a_years']) ? $diff_field['driver_class_a_years'] : ''; ?></td>
                        <td><?php echo isset($diff_field['driver_license_issue_state']) ? $diff_field['driver_license_issue_state'] : ''; ?></td>
                    </tr>
                    <?php	
					$r_d++;				
					}
				  }
				  $j++;
				}
			  }
			}
		?>
        </table>
        </td>
      </tr>
	<tr>
		<td><?php echo $driver->driver_name; ?></td>
        <td><?php echo $driver->driver_middle_name; ?></td>
        <td><?php echo $driver->driver_last_name; ?></td>
        <td><?php echo $driver->driver_licence_no; ?></td>
		<td><?php echo $driver->driver_license_class; ?></td>
		<td><?php echo date('m/d/Y', strtotime($driver->driver_license_issue_date)); ?></td>
		<td><?php echo date('m/d/Y', strtotime($driver->driver_dob)); ?></td>
		<td><?php echo $driver->driver_class_a_years; ?></td>
		<td><?php echo $driver->driver_license_issue_state; ?>
        	<a href="#driver_<?= $i ?>" class="fancybox"><?php echo ($r_d) ? revision_controll_icon($r_d) : '';?></a>
        </td>
    </tr> 
    
    <?php $i++; } ?>
</table>
<?php } ?>
</fieldset>

<!----------------------------------------------------------------------------------------------->

<fieldset>
<legend>Number of Owner</legend>
<?php $or_count1 = count($owners); ?>
<?php
if(count($previous_quote_ids)>1){
	$this->load->model('quote_model');
?>
<div class="row-fluid">
<label><b>Owner Versions</b></label>
<table style="margin-bottom: 9px;margin-top: 5px;">
  <thead>
    <tr>
      <th>Versions</th>
      <th>Number of Owners</th>
    </tr>
  </thead>
  <tbody>
	<?php
    $previous_quote_ids_1 = $previous_quote_ids;
    $v=count($previous_quote_ids_1)-2;
	//$v=0;
    rsort($previous_quote_ids_1);    
    foreach($previous_quote_ids_1 as $key => $previous_quote_id){
	  if($quote_id!=$previous_quote_id){					
		$v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
		
	   //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
	   $no_owners = $this->quote_model->get_all_owner($previous_quote_id);
	  
	   $or_count = count($no_owners);
	   $or_version_diff = '';
	  
	   if($or_count!=$or_count1){
	   	$or_version_diff = 'onclick="owner_version_diff('.$previous_quote_id.', '.$v_1.', &quot;Owner&quot;)"';
	   }
	  
		?>
		<tr>
			<td>v<?= $v; ?></td>
			<td <?= $or_version_diff; ?> style="cursor: pointer;"><?= count($no_owners); ?></td>
		</tr>
		<?php 
		$or_count1 = count($no_owners);
		$v--;
		//$v++;
	  }
    }
    ?>
  </tbody>
</table>
</div>
<?php
}
?>
<table>
	<tr align="left">
		<th>Number of Owner?</th>
		<td><?php echo count($owners);?></td>
    </tr>
</table>

<?php if(count($owners) > 0 ) { ?>
<table>
	<tr align="left">
		<th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
		<th>Driver</th>
		<th>Driver License number</th>
		<th>License Issue State</th>
	</tr>
    <?php $i = 0; foreach($owners as $owner) { ?>
    <tr id="owner_<?= $i; ?>" style="display:none;">
    <td>
    <table class="table">
    <tr align="left">
		<th>First Name</th>
        <th>Middle Name</th>
        <th>Last Name</th>
		<th>Driver</th>
		<th>Driver License number</th>
		<th>License Issue State</th>
	</tr>
    <?php
	$j = 0;
	$w_r = 0;
	if(isset($previous_quote_ids)){
	  if(isset($previous_quote_ids)){
		foreach($previous_quote_ids as $previous_quote_id){
		  if(isset($owner_diff[$previous_quote_id][$i])){
			$diff_field = $owner_diff[$previous_quote_id][$i]; 
			
			if(count($diff_field)>2){
			?>
			 <tr>
			  <!--<td>v<?= $j; ?></td>-->
			  <td><?php echo isset($diff_field['owner_name']) ? $diff_field['owner_name'] : ''; ?></td>
			  <td><?php echo isset($diff_field['owner_middle_name']) ? $diff_field['owner_middle_name'] : ''; ?></td>
			  <td><?php echo isset($diff_field['owner_last_name']) ? $diff_field['owner_last_name'] : ''; ?></td>
			  <td><?php echo isset($diff_field['is_driver']) ? $diff_field['is_driver'] : ''; ?></td>
			  <td><?php echo isset($diff_field['driver_licence_no']) ? $diff_field['driver_licence_no'] : ''; ?></td>
			  <td><?php echo isset($diff_field['license_issue_state']) ? $diff_field['license_issue_state'] : ''; ?></td>
			</tr>
			<?php	
			$w_r++;				
			}
		  }
		  $j++;
		}
	  }
	}
	?>
    </table>
    </td>
    </tr>
	<tr>
		<td><?php echo $owner->owner_name; ?></td>
        <td><?php echo $owner->owner_middle_name; ?></td>
        <td><?php echo $owner->owner_last_name; ?></td>
        <td><?php echo $owner->is_driver; ?></td>
        <td><?php echo $owner->driver_licence_no; ?></td>
		<td><?php echo $owner->license_issue_state; ?>
        <a href="#owner_<?= $i ?>" class="fancybox"><?php echo ($w_r) ? revision_controll_icon($w_r) : '';?></a>
        </td>
	</tr>
    <?php $i++; } ?>
</table>
<?php } ?>
</fieldset>

<!----------------------------------------------------------------------------------------------->

<fieldset>
<legend>Filings Section</legend>
<?php $fil_count1 = count($fillings); ?>
<?php
if(count($previous_quote_ids)>1){
	$this->load->model('quote_model');
?>
<div class="row-fluid">
<label><b>Filings Versions</b></label>
<table style="margin-bottom: 9px;margin-top: 5px;">
  <thead>
    <tr>
      <th>Versions</th>
      <th>Number of Filings</th>
    </tr>
  </thead>
  <tbody>
	<?php
    $previous_quote_ids_1 = $previous_quote_ids;
    $v=count($previous_quote_ids_1)-2;
	//$v=0;
    rsort($previous_quote_ids_1);    
    foreach($previous_quote_ids_1 as $key => $previous_quote_id){
	  if($quote_id!=$previous_quote_id){					
		$v_1 = isset($previous_quote_ids_1[$key-1]) ? $previous_quote_ids_1[$key-1] : '';
		
	   //$v_1 = isset($previous_quote_ids_1[$key+1]) ? $previous_quote_ids_1[$key+1] : '';				
	   $no_filings = $this->quote_model->get_all_fillings($previous_quote_id);
	  
	   $fil_count = count($no_filings);
	   $fil_version_diff = '';
	  
	   if($fil_count!=$fil_count1){
	   	$fil_version_diff = 'onclick="filing_version_diff('.$previous_quote_id.', '.$v_1.', &quot;Owner&quot;)"';
	   }
	  
		?>
		<tr>
			<td>v<?= $v; ?></td>
			<td <?= $fil_version_diff; ?> style="cursor: pointer;"><?= count($no_filings); ?></td>
		</tr>
		<?php 
		$fil_count1 = count($no_filings);
		$v--;
		//$v++;
	  }
    }
    ?>
  </tbody>
</table>
</div>
<?php
}
?>
<table>
	<tr align="left">
		<th>Number of Filings</th>
		<td><?php echo count($fillings);?></td>
    </tr>
</table>

<?php if( count($fillings) > 0 ) { ?>
<table>
	<tr align="left">
		<th>Filing Type </th>
		<th>Filing #</th>
	</tr>
    <?php $i = 0; foreach($fillings as $filling) { ?>
    <tr id="filing_<?= $i; ?>" style="display:none;">
      <td>
      <table cellspacing="1">
      <tr align="left">
		<th>Filing Type </th>
		<th>Filing #</th>
	</tr>
		<?php
        $j = 0;
        $f_r = 0;
        //rsort($previous_quote_ids);
        if(isset($previous_quote_ids)){
          if(isset($previous_quote_ids)){
            foreach($previous_quote_ids as $previous_quote_id){
              if(isset($filling_diff[$previous_quote_id][$i])){
                $diff_field = $filling_diff[$previous_quote_id][$i]; 
                //print_r($diff_field);
                if(count($diff_field)>2){
                ?>
                 <tr>
                 <!-- <td>v<?= $j; ?></td>-->
                  <td><?php echo isset($diff_field['filings_type']) ? $diff_field['filings_type'] : ''; ?></td>
                  <td><?php echo isset($diff_field['filing_no']) ? $diff_field['filing_no'] : ''; ?></td>
                </tr>
                <?php
                $f_r++;
                }
              }
              $j++;
            }
          }
        }
        ?>
        </table>
      </td>
    </tr>
	<tr>
		<td><?php echo $filling->filings_type; ?> <?php if($filling->filings_type=='Other'){ echo " : - ".(isset($filling->filing_type_other) ? $filling->filing_type_other : ''); }?></td>
        <td><?php echo $filling->filing_no; ?>
        <a href="#filing_<?= $i ?>" class="fancybox"><?php echo ($f_r) ? revision_controll_icon($f_r) : '';?></a>
        </td>
	</tr>
    <?php $i++; } ?>
</table>
<?php  } ?>
</fieldset>
<?php 
//if(($this->session->userdata('admin_id')) ||($this->session->userdata('underwriter_id'))){	
?>
<fieldset>
<legend >Comments Section</legend><?php //echo $insured->Comments_request;?>
<label class="<?php echo isset($insured_logs['Comments_request']) ? (count($insured_logs['Comments_request'])>0) ? 'highlighted' : '' : ''; ?>" style="display:block;">Comment
<?php 
$log = isset($insured_logs['Comments_request']) ? $insured_logs['Comments_request'] : '';
get_revisions('i_cmt', $log);
?>
</label>

<?php echo $insured->Comments_request; ?>
</fieldset>
<?php // } ?>
<?php 
if (isset($mvr_link) && $mvr_link != '')
	echo $mvr_link;
if (isset($losses_link) && $losses_link != '')
	echo $losses_link;

//}//End Post IF
?>