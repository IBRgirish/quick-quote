<?php //print_r($radiou_of_operation); ?>
<div class="row-fluid">
            	<table class="table">
                    <tr align="left">
                        <!--th>How many trailors associated with this data?</th-->
                        <th>Radius of Operation</th>
                        <?php if(isset($data->radius_of_operation_other)){ if($data->radius_of_operation_other!= '') { echo '<th>Specify Other</th>'; }} ?>	
                        <th>Any Special Request for Radius</th>
                        <th>Number of Axles</th>
                    </tr>
                    <tr>
                        <!--td><?php //echo $total_trailers; ?></td-->
                        <td>
						<?php echo isset($data->radius_of_operation) ? $data->radius_of_operation : '';  ?>
                        <b><?php echo isset($radiou_of_operation) ? is_array($radiou_of_operation) ?  ', '.implode(', ', $radiou_of_operation) : '' : ''?></b>
                        </td>
                        <?php if(isset($data->radius_of_operation_other)){ if($data->radius_of_operation_other!= ''){ echo '<td>'.$data->radius_of_operation_other.'</td>';
						?>
						<b><?php echo isset($radius_of_operation_other) ? is_array($radius_of_operation_other) ? ', '.implode(', ', $radius_of_operation_other) : '' : ''?></b>
						<?php } }?>
                        <td><?php echo isset($data->request_for_radius_of_operation) ? $data->request_for_radius_of_operation : ''; ?>
                        <b><?php echo isset($request_for_radius_of_operation) ? is_array($request_for_radius_of_operation) ? ', '.implode(', ', $request_for_radius_of_operation) : '' : ''; ?></b>
                        </td>
                        <td>
						<?php echo isset($data->number_of_axles) ? $data->number_of_axles : '';?>
                        <b><?php echo isset($number_of_axles) ? is_array($number_of_axles) ? ', '.implode(', ', $number_of_axles) : '' : '';?></b>
                        </td>
                    </tr>
                </table>
                <table class="table">
                    <tr align="left">
                        <th>Vehicle Year</th>
                        <th>Make Model</th>
                        <th>GVW</th>
                        <th>VIN</th>
                        <?php if(isset($data->vaule)) { if($data->vaule != '') { echo '<th>Special Request</th>'; }} ?>
                    </tr>
                    <tr>
                        <td><?php echo isset($data->vehicle_year) ? $data->vehicle_year : ''; ?>
                        <b><?php echo isset($vehicle_year) ? is_array($vehicle_year) ? ', '.implode(', ', $vehicle_year) : '' : '';?></b>
                        </td>
                        <td><?php echo isset($data->make_model) ? $data->make_model : ''; ?>
                        <b><?php echo isset($make_model) ? is_array($make_model) ? ', '.implode(', ', $make_model) : '' : '';?></b>
                        </td>
                        <td><?php echo isset($data->gvw) ? $data->gvw : ''; ?>
                        <b><?php echo isset($gvw) ? is_array($gvw) ? ', '.implode(', ', $gvw) : '' : '';?></b>
                        </td>
                        <td><?php echo isset($data->vin) ? $data->vin : ''; ?>
                        <b><?php echo isset($vin) ? is_array($vin) ? ', '.implode(', ', $vin) : '' : '';?></b>
                        </td>
                        <?php if(isset($data->vaule)) { if($data->vaule!= '') { ?>
                            <td><?php echo $data->vaule; ?>
                            <b><?php echo isset($spacial_request) ? is_array($spacial_request) ? ', '.implode(', ', $spacial_request) : '' : '';?></b>
                            </td>
                        <?php } }?>
                    </tr>
                </table>
                <?php
				if(isset($data->liability)||isset($data->liability_ded)||isset($data->um)||isset($data->pip)) { 
				 if($data->liability!= ''||$data->liability_ded != ''||$data->um != ''||$data->pip != '') { ?>
                <table class="table">
                    <tr>
                        <?php if(isset($data->liability)){ if($data->liability != '') echo '<th>Liability</th>'; }?>
                        <?php if(isset($data->liability_ded)){ if($data->liability_ded != '') echo '<th>Liability Deductible</th>';} ?>
                        <?php if(isset($data->um)){ if($data->um != '') echo '<th>UM</th>'; }?>
                        <?php if(isset($data->pip)){ if($data->pip != '') echo '<th>PIP</th>' ;} ?>
                    </tr>
                    <tr>
                        <?php if(isset($data->liability)){ if($data->liability != '') {echo '<td>'.$data->liability; ?>
						<b><?php echo isset($liability) ? is_array($liability) ? ', '.implode(', ', $liability) : '' : '';?></b></td>
                        <?php
						} }?>
                        <?php if(isset($data->liability_ded)){  if($data->liability_ded != '') { echo '<td>'.$data->liability_ded; ?>
						<b><?php echo isset($liability_ded) ? is_array($liability_ded) ? ', '.implode(', ', $liability_ded) : '' : '';?></b></td>
                        <?php
						}} ?>
                        <?php if(isset($data->um)){ if($data->um != ''){  echo '<td>'.$data->um; ?>
						<b><?php echo isset($um) ? is_array($um) ? ', '.implode(', ', $um) : '' : '';?></b></td>
                        <?php
						} }?>
                        <?php if(isset($data->pip)){ if($data->pip != ''){ echo '<td>'.$data->pip; ?>
						<b><?php echo isset($pip) ? is_array($pip) ? ', '.implode(', ', $pip) : '' : '';?></b></td>
                        <?php
						}} ?>
                    </tr>
                </table>
                <?php } } ?>
                <?php 
				if(isset($data->pd)||isset($data->pd_ded)) {
				if($data->pd != ''||$data->pd_ded != '') {?>
                <table class="table">
                    <tr>
                        <?php if(isset($data->pd)){ if($data->pd != '') echo '<th>PD</th>';} ?>
                        <?php if(isset($data->pd_ded)){if($data->pd_ded!= '') echo '<th>PD Deductible</th>';} ?>
                    </tr>
                    <tr>
                        <?php if(isset($data->pd)){if($data->pd != ''){ echo '<td>'.$data->pd; ?>
						<b><?php echo isset($pd) ? is_array($pd) ? ', '.implode(', ', $pd) : '' : '';?></b></td>
                        <?php
						}} ?>
                        <?php if(isset($data->pd_ded)){if($data->pd_ded != ''){ echo '<td>'.$data->pd_ded; ?>
						<b><?php echo isset($pd_ded) ? is_array($pd_ded) ? ', '.implode(', ', $pd_ded) : '' : '';?></b></td>
                        <?php
						} }?>
                    </tr>
                </table>
                <?php } 
				}
				?>
                <?php 
				if(isset($data->cargo)||isset($data->cargo_ded)) { 
				if($data->cargo != ''||$data->cargo_ded != '') { ?>
                <table class="table">
                    <tr>
                        <?php if(isset($data->cargo)) { if($data->cargo != '') echo '<th>Cargo</th>'; }?>
                        <?php if(isset($data->cargo_ded)) { if($data->cargo_ded != '') echo '<th>Cargo Deductible</th>'; }?>
                    </tr>
                    <tr>
                        <?php if(isset($data->cargo)) { if($data->cargo != '') { echo '<td>'.$data->cargo; ?>
						<b><?php echo isset($cargo) ? is_array($cargo) ? ', '.implode(', ', $cargo) : '' : '';?></b></td>
                        <?php
						} }?>
                        <?php if(isset($data->cargo_ded)) { if($data->cargo_ded != ''){ echo '<td>'.$data->cargo_ded; ?>
						<b><?php echo isset($cargo_ded) ? is_array($cargo_ded) ? ', '.implode(', ', $cargo_ded) : '' : '';?></b></td>
                        <?php
						}} ?>		
                    </tr>
                </table>
                <?php } } ?>
                <table class="table">
                    <tr>
                        <th>Commodities hauled</th>
                        <!--<th>Number of Trailers Pulled with this data</th>-->
                    </tr>
                    <tr>
                        <td><?php if($data->commodities_haulted) echo $data->commodities_haulted; ?>
                        <b><?php echo isset($commodities_haulted) ? is_array($commodities_haulted) ? ', '.implode(', ', $commodities_haulted) : '' : '';?></b>
                        </td>
                        <!--<td><?php //echo count($trailers); ?></td>-->
                    </tr>
                </table>
            </div>