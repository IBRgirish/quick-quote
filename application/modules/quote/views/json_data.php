<script>

		var broker_info = '<?= form_safe_json(json_encode($broker_info)); ?>';
	    
		var broker_email = '<?= form_safe_json(json_encode($broker_email)); ?>';
		var broker_phone = '<?= form_safe_json(json_encode($broker_phone)); ?>';
		var broker_fax = '<?= form_safe_json(json_encode($broker_fax)); ?>';
		
		var person_info = '<?= form_safe_json(json_encode($person_info)); ?>';
		var person_email = '<?= form_safe_json(json_encode($person_email)); ?>';
		var person_phone = '<?= form_safe_json(json_encode($person_phone)); ?>';
		var person_fax = '<?= form_safe_json(json_encode($person_fax)); ?>';
		
		var insured_info = '<?= form_safe_json(json_encode($insured_info)); ?>';
		var insured_email = '<?= form_safe_json(json_encode($insured_email)); ?>';
		var insured_phone = '<?= form_safe_json(json_encode($insured_phone)); ?>';
		var insured_fax = '<?= form_safe_json(json_encode($insured_fax)); ?>';
		
		
		var insured_b = '<?= form_safe_json(json_encode($insured_b)); ?>';
		var insured_gh8 = '<?= form_safe_json(json_encode($insured_gh8)); ?>';
		var insured_t = '<?= form_safe_json(json_encode($insured_t)); ?>';
		var insured_add = '<?= form_safe_json(json_encode($insured_a)); ?>';
		
		var certificate = '<?= form_safe_json(json_encode($certificate)); ?>';
		var insured_gh6 = '<?= form_safe_json(json_encode($insured_gh6)); ?>';
		var insured_gh61 = '<?= form_safe_json(json_encode($insured_gh61)); ?>';
		var insured_gh62 = '<?= form_safe_json(json_encode($insured_gh62)); ?>';
		
		var insured_gh9 = '<?= form_safe_json(json_encode($insured_gh9)); ?>';
		var insured_gh11 = '<?= form_safe_json(json_encode($insured_gh11)); ?>';
		var insured_gh5 = '<?= form_safe_json(json_encode($insured_gh5a)); ?>';
		var insured_gh51 = '<?= form_safe_json(json_encode($insured_gh51a)); ?>';
		
		var insured_gh5b = '<?= form_safe_json(json_encode($insured_gh5b)); ?>';
		var insured_gh51b = '<?= form_safe_json(json_encode($insured_gh51b)); ?>';
		
		var insured_gh5c = '<?= form_safe_json(json_encode($insured_gh5c)); ?>';
		var insured_gh51c = '<?= form_safe_json(json_encode($insured_gh51c)); ?>';
		
		var insured_gh5d = '<?= form_safe_json(json_encode($insured_gh5d)); ?>';
		var insured_gh51d = '<?= form_safe_json(json_encode($insured_gh51d)); ?>';
		
		var insured_gh5e = '<?= form_safe_json(json_encode($insured_gh5e)); ?>';
		var insured_gh51e = '<?= form_safe_json(json_encode($insured_gh51e)); ?>';
		
		var insured_gh5f = '<?= form_safe_json(json_encode($insured_gh5f)); ?>';
		var insured_gh51f = '<?= form_safe_json(json_encode($insured_gh51f)); ?>';
		
		var insured_gh5g = '<?= form_safe_json(json_encode($insured_gh5g)); ?>';
		var insured_gh51g = '<?= form_safe_json(json_encode($insured_gh51g)); ?>';
		
		
		var ins_info = '<?= form_safe_json(json_encode($ins_info)); ?>';
		var ins_email = '<?= form_safe_json(json_encode($ins_email)); ?>';
		var ins_phone = '<?= form_safe_json(json_encode($ins_phone)); ?>';
		var ins_fax = '<?= form_safe_json(json_encode($ins_fax)); ?>';
		var filing = '<?= form_safe_json(json_encode($filing)); ?>';
		var filings = '<?= form_safe_json(json_encode($filings)); ?>';
		
		var coverage = '<?= form_safe_json(json_encode($coverage)); ?>';
	
		
		var truck_vh = '<?= form_safe_json(json_encode($truck_vh)); ?>';
		var tractor_vh = '<?= form_safe_json(json_encode($tractor_vh)); ?>';				
		var trailer_vh = '<?= form_safe_json(json_encode($trailer_vh)); ?>';
		var other_vh = '<?= form_safe_json(json_encode($other_vh)); ?>';
	    
		var tractor_vh_eqpmntss = '<?= form_safe_json(json_encode($tractor_vh_eqpmntss)); ?>';
	    var other_vh_eqpmntss = '<?= form_safe_json(json_encode($other_vh_eqpmntss)); ?>';

	    var truck_lessor = '<?= form_safe_json(json_encode($truck_lessor)); ?>';
		
		var tractor_lessor = '<?= form_safe_json(json_encode($tractor_lessor)); ?>';
		var trailer_lessor = '<?= form_safe_json(json_encode($trailer_lessor)); ?>';
		var other_lessor = '<?= form_safe_json(json_encode($other_lessor)); ?>';
      
	    var truck_vh_eqp = '<?= form_safe_json(json_encode($truck_vh_eqp)); ?>';
	
		var tractor_vh_eqp = '<?= form_safe_json(json_encode($tractor_vh_eqp)); ?>';
		var trailer_vh_eqp = '<?= form_safe_json(json_encode($trailer_vh_eqp)); ?>';
		var other_vh_eqp = '<?= form_safe_json(json_encode($other_vh_eqp)); ?>';

        var truck_email = '<?= form_safe_json(json_encode($truck_email)); ?>';
		var tractor_email = '<?= form_safe_json(json_encode($tractor_email)); ?>';
		var trailer_email = '<?= form_safe_json(json_encode($trailer_email)); ?>';
		var other_email = '<?= form_safe_json(json_encode($other_email)); ?>';
		
		var truck_fax = '<?= form_safe_json(json_encode($truck_fax)); ?>';
		
		var tractor_fax = '<?= form_safe_json(json_encode($tractor_fax)); ?>';
		var trailer_fax = '<?= form_safe_json(json_encode($trailer_fax)); ?>';
		var other_fax = '<?= form_safe_json(json_encode($other_fax)); ?>';
		
		var owner = '<?= form_safe_json(json_encode($owner)); ?>';
		var driver = '<?= form_safe_json(json_encode($driver)); ?>';
		var owners='<?= form_safe_json(json_encode($owners)); ?>';
		var drivers='<?= form_safe_json(json_encode($drivers)); ?>';
		
		var losses_lia='<?= form_safe_json(json_encode($losses_lia)); ?>';
		var losses_pd='<?= form_safe_json(json_encode($losses_pd)); ?>';
		var losses_cargo='<?= form_safe_json(json_encode($losses_cargo)); ?>';
		var losses='<?= form_safe_json(json_encode($losses)); ?>';
		
		
</script>
 