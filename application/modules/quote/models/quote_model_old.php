<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quote_model extends CI_Model {

    public function &__get($key) {
        $CI = & get_instance();
        return $CI->$key;
        $this->load->database();
    }
	
	//ADDED BY MAYANK DATE 21 JULY//
	function get_catlog_options($catlog_type=''){
		$this->db->where('catlog_type', $catlog_type);
		$this->db->order_by('catlog_type', 'asc');
		$query = $this->db->get('rq_rqf_quote_catlogs');
		if($query->num_rows() > 0)
			return $query->result();
		else
			return FALSE;
	}
	//END OF ADDED BY MAYANK DATE 21 JULY//

    function get_quote($id = '') {

		$sql = "select * from rq_rqf_quote as q ";
		
		if($userid = $this->session->userdata('underwriter_id')){
			 //$sql .= "WHERE LOCATE(CONCAT(',',$userid,','),CONCAT(',',q.assigned_underwriter,','))>0";
			// $sql .= "WHERE q.assigned_underwriter";
			// $userid = $this->session->userdata('underwriter_id');
			 $sql .= "WHERE q.assigned_underwriter IN($userid)";
			
			 //$this->db->where_in("QB.assigned_underwriter", $userid);
			 	if($id > 0) 
				$sql .= " and q.quote_id = ". $id;
		} else if($memberid = $this->session->userdata('member_id'))
		{
			$sql .= "WHERE q.requester_id = '$memberid'";
				if($id > 0) 
				$sql .= " and q.quote_id = ". $id;
		}
		else if($this->session->userdata('admin_id'))
		{
			if($id > 0) 
				$sql .= "WHERE q.quote_id = ". $id;
		}
		$query = $this->db->query($sql);
		//echo $this->db->last_query();
		$this->data['quote'] = $query->result();
		//print_r($query->result());
		return $query->result();
       
    }
	
	/*Ashvin Patel 8/may/2014*/
	function get_manual_quote($id = '') {

	
		$sql = "select * from rq_rqf_quote_bundle as qb ";
		$this->db->select('QB.*');
		$this->db->select('R.insured');
		$this->db->join('rq_ratesheet as R','R.id = QB.cargo_id');
		//$this->db->join('rq_rqf_quote as Q','Q.quote_id = QB.quote_id');	
		$this->db->from('rq_rqf_quote_bundle as QB');
		$this->db->where('QB.producer_email', $this->session->userdata('username'));
		$this->db->where('QB.is_manual', 1);
		$query = $this->db->get();
		//echo $this->db->last_query();
		$this->data['manual_quote'] = $query->result();
		//print_r($query->result());
		return $query->result();
       
    }
	/*Ashvin Patel 8/may/2014*/
	
	/*Ashvin Patel 3/may/2014*/
	function get_quote1($id = '') {

		$sql = "select * from rq_rqf_quote_bundle as qb ";
		$this->db->select('QB.*');
		$this->db->select('R.insured,R.id,R.created_by');
		if ($this->session->userdata('underwriter_id')) {
			$this->db->where_in("QB.underwriters", $this->session->userdata('underwriter_id'));
		}
		$this->db->where('QB.quote_id', 0);
		$this->db->join('rq_ratesheet as R','R.id = QB.cargo_id');
		//$this->db->join('rq_rqf_quote as Q','Q.quote_id = QB.quote_id');	
		$this->db->from('rq_rqf_quote_bundle as QB');
		$query = $this->db->get();
		//echo $this->db->last_query();
		$this->data['quote'] = $query->result();
		//print_r($query->result());
		return $query->result();
       
    }
	/*Ashvin Patel 3/may/2014*/
	
	function get_xls_data($id = '') {

		$sql = "select * from rq_rqf_quote as q ";			
	    $sql .= " where q.quote_id = ". $id;
		$query = $this->db->query($sql);		
		$data['quote'] = $query->row();		
		
		$this->db->select('*');
		$this->db->from('rq_rqf_insured_info');	
		$this->db->where('quote_id', $id);
		$query = $this->db->get();
		$data['insured'] = $query->row();
		
		return $data;
       
    }
	function get_sheet_rate($id = '',$type)
	{
		if($type == 'cargo') $tablename = 'rq_ratesheet';
		else if($type == 'pd') $tablename = 'rq_ratesheet_damage_ratesheet';
		else if($type == 'liability') $tablename = 'rq_ratesheet_liability';
		
		$this->db->select('R.*');   
		$this->db->from($tablename.' as R');
		if($id > 0){
			$where_array = array('R.id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	function get_quote_bundle($id = '') {
		$this->db->select('QB.*');   
		$this->db->from('rq_rqf_quote_bundle as QB');
		if($id > 0){
			$where_array = array('QB.id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result_array();
		
		return $result;
	}
	
	/**By Ashvin patel 17/may/2014**/
	function get_quote_bundle_manual($id = '') {
		$this->db->select('QB.*');   
		$this->db->from('rq_rqf_quote_bundle as QB');
		if($id > 0){
			$where_array = array('QB.id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result_array();
		
		return $result;
	}
	/**By Ashvin patel 17/may/2014**/
	
	function get_bundle_by_quote($id)
	{
		
		$this->db->select('QB.*');  
		$this->db->join('rq_rqf_quote as Q','Q.quote_id = QB.quote_id');		
		$this->db->from('rq_rqf_quote_bundle as QB');
		$where_array = array('QB.quote_id' => $id);
		$this->db->where($where_array);
		$this->db->order_by('creation_date');
		//$this->db->limit(1);
		$query = $this->db->get();
		$result = $query->result_array();
		$result1 = array();
		//print_r($result);
		foreach($result as $res)
		{
			$qrid = $res['id'];
			$this->db->select('QB.id');   
			$this->db->select('QB.status');  
			$this->db->select('Q.quote_id');
			$this->db->select('Q.email');
			$this->db->select('Q.perma_reject');	
			$this->db->select('QB.creation_date');				
			$this->db->join('rq_rqf_quote as Q','Q.quote_id = QB.quote_id');
			if($res['cargo_id'] != '' && $res['cargo_id'] != 0)
			{
				$this->db->select('R.total_cargo_premium'); 
				$this->db->join('rq_ratesheet as R','R.id = IFNULL(`QB`.`cargo_id`, "0")');
			}
			if($res['pd_id'] != '' && $res['pd_id'] != 0)
			{
				$this->db->select('PD.total_pd_premium'); 
				$this->db->join('rq_ratesheet_damage_ratesheet as PD','PD.id = IFNULL(`QB`.`pd_id`, "0")');
			}
			if($res['liability_id'] != '' && $res['liability_id'] != 0)
			{
				$this->db->select('L.total_liability_premium'); 
				$this->db->join('rq_ratesheet_liability as L','L.id = QB.liability_id');
			}
			$this->db->from('rq_rqf_quote_bundle as QB');
			$where_array = array('QB.id' => $qrid);
			$this->db->where($where_array);
			$query1 = $this->db->get();
			$result1[] = $query1->result_array();
			
		}
		//print_r($result1);
		return $result1;
	}
	function get_bundle_by_quote1($id)
	{
		
		$this->db->select('QB.*');  
		$this->db->join('rq_rqf_quote as Q','Q.quote_id = QB.quote_id');		
		$this->db->from('rq_rqf_quote_bundle as QB');
		$where_array = array('QB.quote_ref' => $id);
		$this->db->where($where_array);
		$this->db->order_by('creation_date');
		
		$query = $this->db->get();
		$result = $query->result_array();
		$result1 = array();
		foreach($result as $res)
		{
			$qrid = $res['id'];
			$this->db->select('QB.id');   
			$this->db->select('QB.status');  
			$this->db->select('Q.quote_id');
			$this->db->select('Q.email');
			$this->db->select('Q.perma_reject');	
			$this->db->select('QB.creation_date');				
			$this->db->join('rq_rqf_quote as Q','Q.quote_id = QB.quote_id');
			if($res['cargo_id'] != '' && $res['cargo_id'] != 0)
			{
				$this->db->select('R.total_cargo_premium'); 
				$this->db->join('rq_ratesheet as R','R.id = IFNULL(`QB`.`cargo_id`, "0")');
			}
			if($res['pd_id'] != '' && $res['pd_id'] != 0)
			{
				$this->db->select('PD.total_pd_premium'); 
				$this->db->join('rq_ratesheet_damage_ratesheet as PD','PD.id = IFNULL(`QB`.`pd_id`, "0")');
			}
			if($res['liability_id'] != '' && $res['liability_id'] != 0)
			{
				$this->db->select('L.total_liability_premium'); 
				$this->db->join('rq_ratesheet_liability as L','L.id = QB.liability_id');
			}
			$this->db->from('rq_rqf_quote_bundle as QB');
			$where_array = array('QB.id' => $qrid);
			$this->db->where($where_array);
			$query1 = $this->db->get();
			$result1[] = $query1->result_array();
			
		}
		
		return $result1;
	}
	
	/**Ashvin Patel 9/may/2014**/
	function get_bundle_by_quote2($id)
	{
		
		$this->db->select('QB.*,R.insured');  
		$this->db->join('rq_ratesheet as R','R.id = QB.cargo_id');		
		$this->db->from('rq_rqf_quote_bundle as QB');
		$where_array = array('QB.quote_ref' => $id);
		$this->db->where($where_array);
		//$this->db->order_by('creation_date');
		
		$query = $this->db->get();
		$result = $query->result();
		$result1 = array();		
		
		return $result;
	}
	/**Ashvin Patel 9/may/2014**/
	
	function get_quote_rate($id = '',$limit = '') {
	
		$this->db->select('QB.*');   
		
		$this->db->order_by('QB.creation_date', 'desc');
		$this->db->group_by('QB.quote_ref');
		$this->db->from('rq_rqf_quote_bundle as QB');
		$this->db->group_by('QB.quote_id');		
		//$this->db->limit(1);
		$query = $this->db->get();
		//echo $this->db->last_query();
		$resultbundle = $query->result_array();
		if(!empty($resultbundle))
		{
			foreach($resultbundle as $res)
			{
				
				$qrid = $res['id'];
				$this->db->select('QB.id,QB.quote_id,QB.created_by,QB.producer_email as email,QB.quote_ref as quote_ref');   
				$this->db->select('QB.status,QB.is_manual,QB.underwriters');  
				$this->db->select('QB.creation_date');		
					
				
				if($res['cargo_id'] != '' && $res['cargo_id'] != 0)
				{
					$this->db->select('R.total_cargo_premium,R.insured'); 
					$this->db->join('rq_ratesheet as R','R.id = IFNULL(`QB`.`cargo_id`, "0")');
				}
				if($res['pd_id'] != '' && $res['pd_id'] != 0)
				{
					$this->db->select('PD.total_pd_premium'); 
					$this->db->join('rq_ratesheet_damage_ratesheet as PD','PD.id = IFNULL(`QB`.`pd_id`, "0")');
				}
				if($res['liability_id'] != '' && $res['liability_id'] != 0)
				{
					$this->db->select('L.total_liability_premium'); 
					$this->db->join('rq_ratesheet_liability as L','L.id = QB.liability_id');
				}
				$this->db->from('rq_rqf_quote_bundle as QB');
				$where_array = array('QB.id' => $qrid);
				$this->db->where($where_array);
				$query1 = $this->db->get();
				
				$result1[] = $query1->result_array();
				
			}
		}
		else
		{
			$result1 = '';
		}
		return $result1;
	}
	 function get_quote_full($id = '') {

        //     $created_by=   $this->session->userdata['flexi_auth'] ['user_id']; 
	
	        $where_array = array('Q.quote_id' => $id);
        // $this->db->select('tbl_services.*,tbl_services_type.services_type_name,tbl_currency.currencyname,tbl_costs.cost_price as base_cost');
        $this->db->select('Q.*');   
		$this->db->select('I.*');   
		$this->db->join('rq_rqf_insured_info as I','I.quote_id = Q.quote_id');
        $this->db->from('rq_rqf_quote as Q');
     	$this->db->where($where_array);
        $query = $this->db->get();
		
        // print_r($this->db->last_query());
        $this->data['quote'] = $query->row();
		return $query->row();
        //   print_r($query->result());
        // $this->db->from('tbl_variable');
        //     $this->db->where('services_id', $id);
        //   $this->db->select('tbl_variable.*');
        //       $query1 = $this->db->get();
        //    $this->data['services_variable'] = $query1->result();
    }
	
	function get_insured_info($id = '') {
        //  $created_by=   $this->session->userdata['flexi_auth'] ['user_id']; 
      /*  $where_array = array('rq_rqf_quote_insured.quote_id' => $id);
        $this->db->select('rq_rqf_quote_insured.*');
        $this->db->from('rq_rqf_quote_insured');
        $this->db->where($where_array);

        $query = $this->db->get();
        $this->data['insured'] = $query->result();*/
		
		$where_array = array('rq_rqf_insured_info.quote_id' => $id);
        $this->db->select('rq_rqf_insured_info.*');
        $this->db->from('rq_rqf_insured_info');
		if($id!='')
		{
        	$this->db->where($where_array);
		}
	
        $query = $this->db->get();	//echo $this->db->last_query();
        $this->data = $query->result();
		if(isset($this->data[0]))
		{
        	return $this->data[0];
		}
		else
		{
			return;
		}

		//  print_r($query->result());
    }
	
    function get_losses($id = '') {
        //  $created_by=   $this->session->userdata['flexi_auth'] ['user_id']; 
        $where_array = array('rq_rqf_quote_losses.quote_id' => $id);
        $this->db->select('rq_rqf_quote_losses.*');
        $this->db->from('rq_rqf_quote_losses');
        $this->db->where($where_array);

        $query = $this->db->get();
        $this->data['losses'] = $query->result();
        //  print_r($query->result());
		return $query->result();
    }

    function get_all_drivers($id = '') {
        //  $created_by=   $this->session->userdata['flexi_auth'] ['user_id']; 
        $where_array = array('rq_rqf_quote_drivers.quote_id' => $id);
        $this->db->select('rq_rqf_quote_drivers.*');
        $this->db->from('rq_rqf_quote_drivers');
        $this->db->where($where_array);

        $query = $this->db->get();
        $this->data['drivers'] = $query->result();
        //  print_r($query->result());
		return $query->result();
    }

    function get_all_fillings($id = '') {
        //  $created_by=   $this->session->userdata['flexi_auth'] ['user_id']; 
        $where_array = array('rq_rqf_quote_filings.quote_id' => $id);
        $this->db->select('rq_rqf_quote_filings.*');
        $this->db->from('rq_rqf_quote_filings');
        $this->db->where($where_array);

        $query = $this->db->get();
        $this->data['fillings'] = $query->result();
        //  print_r($query->result());
		return $query->result();
    }

    function get_all_owner($id = '') {
        //  $created_by=   $this->session->userdata['flexi_auth'] ['user_id']; 
        $where_array = array('rq_rqf_quote_owner.quote_id' => $id);
        $this->db->select('rq_rqf_quote_owner.*');
        $this->db->from('rq_rqf_quote_owner');
        $this->db->where($where_array);

        $query = $this->db->get();
        $this->data['owner'] = $query->result();
        //  print_r($query->result());
		return $query->result();
    }

    function get_all_vehicle($id = '',$vehicle_type='') {
        //  $created_by=   $this->session->userdata['flexi_auth'] ['user_id']; 
        //$where_array = array('rq_rqf_quote_vehicle.quote_id' => $id);
		$this->db->where('rq_rqf_quote_vehicle.quote_id',$id);
		if($vehicle_type != '')
		{
			//$where_array = array('rq_rqf_quote_vehicle.vehicle_type' => $vehicle_type);
			$this->db->where('rq_rqf_quote_vehicle.vehicle_type',$vehicle_type);
		}

        $this->db->select('rq_rqf_quote_vehicle.*');
        $this->db->from('rq_rqf_quote_vehicle');
       

        $query = $this->db->get();
        $this->data['vehicle'] = $query->result();
        return $this->data['vehicle'];
    }

    
	
	
	function insert_quote($upload_path) {
        //echo "<pre>";
		//print_r($_POST);
		//print_r($_FILES);
		//echo "</pre>";
		//die('test');
		if ($_POST) 
		{
		
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('coverage_date', 'Coverage Date', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');
            
			if ($this->form_validation->run()) 
			{
				//$vehicle_liability = $this->input->post('vehicle_liability'); 
                //$vehicle_pd = $this->input->post('vehicle_pd');
                //$vehicle_cargo = $this->input->post('vehicle_cargo');
            
								
			/*-------------------------------------------------------------------------------------------*/
			//Start Broker Section 		
			$insert_data['date_added'] = date("Y-m-d H:i:s");
			$insert_data['coverage_date'] = date("Y-m-d", strtotime($this->input->post("coverage_date", true)));
			$insert_data['contact_name'] = trim($this->input->post("name", true));
			$insert_data['contact_middle_name'] = trim($this->input->post("middle_name", true));
			$insert_data['contact_last_name'] = trim($this->input->post("last_name", true));
			$insert_data['cab'] = trim($this->input->post("cab", true));
			$insert_data['status'] = 'Active';
			$insert_data['agency'] = trim($this->input->post("agency", true));
			
			$tel_ext = $this->input->post('tel_ext', true);
			$telephone_no = $this->input->post("telephone", true);
			$insert_data['phone_no'] = $telephone_no[1] . '-' . $telephone_no[2] . '-' . $telephone_no[3]. '-' .$tel_ext;
			
			$cell = $this->input->post("cell", true);
			$insert_data['cell_no'] = $cell[1] . '-' . $cell[2] . '-' . $cell[3];
			
			$insert_data['email'] = trim($this->input->post("email", true));
			
			
			if($this->input->post('contact_field') != '')
			{
				$insert_data['contact_field_value'] = $this->input->post('contact_field'); 
				
				if($this->input->post("fax") != FALSE)
				{
					$fax_no = $this->input->post("fax", true);
					 $insert_data['contact_field_value'] = $fax_no[1] . '-' . $fax_no[2] . '-' . $fax_no[3];
				}
				if($this->input->post("other") != FALSE)
				{
					$other_tele = $this->input->post("other", true);
					$insert_data['contact_field_value'] = $other_tele[1] . '-' .$other_tele[2]. '-' .$other_tele[3];
				}
				if($this->input->post('broker_other_email')!= FALSE)
				{
					$insert_data['contact_field_value'] = $this->input->post('broker_other_email');
				}
			}
			
			$zip = $this->input->post("zip", true);
			$insert_data['zip'] = $zip[1] . '-' . $zip[2];
			
			//$insert_data['is_vehicle_liability'] = $vehicle_liability?'Y':'N';
			//$insert_data['is_vehicle_pd'] =  $vehicle_pd?'Y':'N';
			//$insert_data['is_vehicle_cargo'] =  $vehicle_cargo?'Y':'N';
			
			if($this->session->userdata('member_id')){		
				$insert_data['requester_id'] = $this->session->userdata('member_id');
				$insert_data['requested_by'] = 'member';
				
			} elseif($this->session->userdata('underwriter_id')){
				$insert_data['requester_id'] = $this->session->userdata('underwriter_id');
				$insert_data['requested_by'] = 'underwriter';
			}

			$where_array = array('rq_members.email' => $insert_data['email']);
			$this->db->select('rq_members.underwriter');
			$this->db->from('rq_members');
			$this->db->where($where_array);

			$query = $this->db->get();
			$underwriter = $query->result();
			$keys = explode(',', $underwriter[0]->underwriter);
			list($primary, $uw) = explode(':', $keys[0]);
			//echo $primary;die();
			$insert_data['assigned_underwriter'] = $primary;
			$this->db->insert('rq_rqf_quote', $insert_data);
			
			$result_id_quote = $this->db->insert_id();
			$result =  $result_id_quote;
		
		//End Broker Section
		/*-------------------------------------------------------------------------------------------*/
				
		//Start Insured Information
		/*-------------------------------------------------------------------------------------------*/
		$insert_data_ins['quote_id'] = $result_id_quote;
		$insert_data_ins['insured_fname'] = trim($this->input->post("insured_name", true));
		$insert_data_ins['insured_mname'] = trim($this->input->post("insured_middle_name", true));
		$insert_data_ins['insured_lname'] = trim($this->input->post("insured_last_name", true));
		$insert_data_ins['insured_dba'] = trim($this->input->post("dba", true));
		$insert_data_ins['insured_email'] = trim($this->input->post("insured_email", true));
		
		$insured_telephone = $this->input->post("insured_telephone", true);
		$insert_data_ins['insured_telephone'] = $insured_telephone[1].'-'.$insured_telephone[2].'-'.$insured_telephone[3].'-'.trim($this->input->post('insured_tel_ext'));
		
		$insert_data_ins['insured_garaging_city'] = trim($this->input->post("insured_garaging_city", true));
		$insured_state_post = $this->input->post("insured_state");
		$insured_state_val = isset($insured_state_post[0]) ? $insured_state_post[0] : '';
		
		$insert_data_ins['insured_state'] = $insured_state_val;
		$insured_zip = $this->input->post("insured_zip", true);
		$insert_data_ins['insured_zip'] = $insured_zip[1] . '-' . $insured_zip[2];

		$nature_of_business = $this->input->post("nature_of_business", true);
		$commodities_haulted = $this->input->post("commodities_haulted", true);
		$insert_data_ins['nature_business'] = !empty($nature_of_business) ? implode(',',$nature_of_business) : '';
		$insert_data_ins['commodities_haulted'] = !empty($commodities_haulted) ? implode(',',$commodities_haulted) : ''; 
		$insert_data_ins['year_in_business'] = trim($this->input->post("business_years", true));
		$this->db->insert('rq_rqf_insured_info', $insert_data_ins);
		
		$upload_location = $upload_path.$result_id_quote;
		if (!is_dir($upload_location)) {
			mkdir($upload_location);       
		}
		
		
		//End Insuerd Information
		/*-------------------------------------------------------------------------------------------*/	
				
		/* //Start File Uploadings 
		
		//For File Uploadings 
		$upload_location = $upload_path.$result_id_quote;
		mkdir($upload_location);
		//print_r($_FILES); die;
		foreach($_FILES['file_sec_1']['error'] as $key=>$err_val){
			if($err_val == 4 ){
				unset($_FILES['file_sec_1']['error'][$key]);
				unset($_FILES['file_sec_1']['name'][$key]);
				unset($_FILES['file_sec_1']['type'][$key]);
				unset($_FILES['file_sec_1']['tmp_name'][$key]);
				unset($_FILES['file_sec_1']['size'][$key]);
			}	
		}
		$_FILES['file_sec_1']['error'] = array_values($_FILES['file_sec_1']['error']); 
		$_FILES['file_sec_1']['name'] = array_values($_FILES['file_sec_1']['name']); 
		$_FILES['file_sec_1']['type'] = array_values($_FILES['file_sec_1']['type']); 
		$_FILES['file_sec_1']['size'] = array_values($_FILES['file_sec_1']['size']); 
		$_FILES['file_sec_1']['tmp_name'] = array_values($_FILES['file_sec_1']['tmp_name']); 
		
		foreach($_FILES['file_sec_3']['error'] as $key=>$err_val){
			if($err_val == 4 ){
				unset($_FILES['file_sec_3']['error'][$key]);
				unset($_FILES['file_sec_3']['name'][$key]);
				unset($_FILES['file_sec_3']['type'][$key]);
				unset($_FILES['file_sec_3']['tmp_name'][$key]);
				unset($_FILES['file_sec_3']['size'][$key]);
			}	
		}
		$_FILES['file_sec_3']['error'] = array_values($_FILES['file_sec_3']['error']); 
		$_FILES['file_sec_3']['name'] = array_values($_FILES['file_sec_3']['name']); 
		$_FILES['file_sec_3']['type'] = array_values($_FILES['file_sec_3']['type']); 
		$_FILES['file_sec_3']['size'] = array_values($_FILES['file_sec_3']['size']); 
		$_FILES['file_sec_3']['tmp_name'] = array_values($_FILES['file_sec_3']['tmp_name']); 
		
		$uploadInfoSec1 = $this->file_upload_one($upload_location);
		$uploadInfoSec3 = $this->file_upload_three($upload_location);
		
		
		$uploadInfoSec1 =	explode (',', $uploadInfoSec1);
		$uploadInfoSec3 = explode (',',	$uploadInfoSec3);
		
		$uploadInfoSec3 = array_diff($uploadInfoSec3, $uploadInfoSec1); 
		
		$uploadInfoSec3 = implode(',', $uploadInfoSec3);
		$uploadInfoSec1 = implode(',', $uploadInfoSec1); */
		
		$uploadInfoSec1 = $this->input->post("uploaded_files_values", true);
		$uploadInfoSec3 = $this->input->post("uploaded_files_values_second", true);
		
		$file_name['loss_report_filename'] = $uploadInfoSec1;
		
		$file_name['driver_mvr_filename'] = $uploadInfoSec3;
			
		$return_res['uploaded_file']['losses'] = $uploadInfoSec1;
		$return_res['uploaded_file']['mvr'] = $uploadInfoSec3;
		$return_res['quote_id'] = $result_id_quote;
		
		$this->db->where('quote_id', $result_id_quote);
		$this->db->update('rq_rqf_quote', $file_name);
						
		//End File Uploadings		
	    
		/*-------------------------------------------------------------------------------------------*/
		
		//Start losses section
		$losses_fields = array('losses_liability','losses_pd','losses_cargo','losses_other1','losses_other2');

			foreach($losses_fields as $heading)
			{
				$losses_from = $this->input->post($heading.'_from');
				$losses_to = $this->input->post($heading.'_to');
				$losses_amount = $this->input->post($heading.'_amount');
				$losses_company = $this->input->post($heading.'_company');
				$losses_general_agent = $this->input->post($heading.'_general_agent');
				$losses_date_of_loss = $this->input->post($heading.'_date_of_loss');
			
				for ($i = 0; $i < count($losses_from); $i++) 
				{
					if ($losses_from[$i] != '') 
					{ 
						if($heading=='losses_other1' || $heading=='losses_other2')
						{
							$heading = $heading.'_specify_other';
						}
						$insert_data1['quote_id'] = $result_id_quote;
						$insert_data1['losses_type'] =  $heading;
						$insert_data1['losses_from_date'] = date("Y-m-d", strtotime($losses_from[$i]));
						$insert_data1['losses_to_date'] = date("Y-m-d", strtotime($losses_to[$i]));
						$insert_data1['losses_amount'] = $losses_amount[$i];
						$insert_data1['losses_company'] = $losses_amount[$i];
						$insert_data1['losses_general_agent'] = $losses_general_agent[$i];
						$insert_data1['date_of_loss'] = date("Y-m-d", strtotime($losses_date_of_loss[$i]));
						
						$this->db->insert('rq_rqf_quote_losses', $insert_data1);
					}
				}
			}
			
		//End Losses Section
		
		/*-------------------------------------------------------------------------------------------*/		
			//Truck Section End	
			$truck_radius_of_operation = $this->input->post('truck_radius_of_operation');
			$truck_request_for_radius_of_operation = $this->input->post('truck_request_for_radius_of_operation');
			$truck_radius_of_operation_other = $this->input->post('truck_radius_of_operation_other');
			$number_of_axles = $this->input->post('truck_number_of_axles');
			
			$vehicle_year_vh = $this->input->post('vehicle_year_vh');
			$make_model_vh = $this->input->post('make_model_vh');
			$gvw_vh = $this->input->post('gvw_vh');
			$vin_vh = $this->input->post('vin_vh');
			
			$liability_vh = $this->input->post('liability_vh');
			$liability_ded_vh = $this->input->post('liability_ded_vh');
			$truck_um = $this->input->post('truck_um');
			$truck_pip = $this->input->post('truck_pip');
			
			$value_amount_vh = $this->input->post('value_amount_vh');
			$pd_vh = $this->input->post('pd_vh');
			$ph_ded_vh = $this->input->post('ph_ded_vh');
			
			
			$cargo_vh = $this->input->post('cargo_vh');
			$cargo_ded_vh = $this->input->post('cargo_ded_vh');
			$refrigerated_breakdown = $this->input->post('refrigerated_breakdown');
			$deductible = $this->input->post('deductible');
			//$trailers = $this->input->post('trailers');
			//print_r($truck_radius_of_operation);
			$put_on_truck = $this->input->post('put_on_truck');
			$commodities_haulted_truck = $this->input->post('commodities_haulted_truck');
			
			for ($j = 0; $j < count($truck_radius_of_operation); $j++) {

				if ($truck_radius_of_operation[$j] != '') {
					$insert_data2['quote_id'] = $result;
					$insert_data2['vehicle_type'] = 'TRUCK';
			
					$insert_data2['radius_of_operation'] = $truck_radius_of_operation[$j];
					$insert_data2['radius_of_operation_other'] = $truck_radius_of_operation_other[$j];
					$insert_data2['request_for_radius_of_operation'] = $truck_request_for_radius_of_operation[$j];
					$insert_data2['number_of_axles'] = $number_of_axles[$j];
					
					$insert_data2['vehicle_year'] = $vehicle_year_vh[$j];
					$insert_data2['make_model'] = $make_model_vh[$j];
					$insert_data2['gvw'] = $gvw_vh[$j];
					$insert_data2['vin'] = $vin_vh[$j];
					
					
					$insert_data2['liability'] = $liability_vh[$j];
					$insert_data2['liability_ded'] = $liability_ded_vh[$j];
					$insert_data2['um'] = $truck_um[$j];
					$insert_data2['pip'] = $truck_pip[$j];
					
					$insert_data2['pd'] = $pd_vh[$j];
					$insert_data2['pd_ded'] = $ph_ded_vh[$j];
					$insert_data2['vaule'] = $value_amount_vh[$j];
					
					$insert_data2['cargo'] = $cargo_vh[$j];
					$insert_data2['cargo_ded'] = $cargo_ded_vh[$j];
					$insert_data2['refrigerated_break_down'] = isset($refrigerated_breakdown[$j]) ? "YES" : "NO";
					$insert_data2['break_down_deductible'] = $deductible[$j];
					//$insert_data2['type_of_trailer'] = $trailers[$j];
					
					$insert_data2['put_on_truck'] = $put_on_truck[$j];
					$insert_data2['commodities_haulted'] = $commodities_haulted_truck[$j];
		
					/*
					$cargo_spec_post = $this->input->post(($j+1).'truck_cargo_specifier');
					//$cargo_specifier = isset() ? $this->input->post(($j+1).'truck_cargo_specifier') : '';
					
					if(!empty($cargo_spec_post)){
						$insert_data2['cargo_specifier'] = implode(',', $cargo_spec_post );
					} else {
						$insert_data2['cargo_specifier'] = '';
					}*/
					
					$this->db->insert('rq_rqf_quote_vehicle', $insert_data2);
				}
			}
			//Truck Section End
		/*-------------------------------------------------------------------------------------------*/		
			
		
				// FOR TRAILER VALUES
				
				$number_of_trailers_associated_to_tractor = $this->input->post('vehicle_trailer_no');
                $trailer_vehicle_year_vh = $this->input->post('trailer_vehicle_year_vh');
                $trailer_make_model_vh = $this->input->post('trailer_make_model_vh');
                $trailer_gvw_vh = $this->input->post('trailer_gvw_vh');
                $trailer_vin_vh = $this->input->post('trailer_vin_vh');
                $trailer_type = $this->input->post('trailer_type');
                $trailer_owned_vehicle = $this->input->post('trailer_owned_vehicle');
				
                $trailer_liability_vh = $this->input->post('trailer_liability_vh');
                $trailer_liability_ded_vh = $this->input->post('trailer_liability_ded_vh');
                $trailer_pd_vh = $this->input->post('trailer_pd_vh');
                $trailer_ph_ded_vh = $this->input->post('trailer_ph_ded_vh');
				
                $trailer_cargo_vh = $this->input->post('trailer_cargo_vh');
                $trailer_cargo_ded_vh = $this->input->post('trailer_cargo_ded_vh');
                $trailer_refrigerated_breakdown = $this->input->post('trailer_refrigerated_breakdown');
                $trailer_deductible = $this->input->post('trailer_deductible');
                // TRAILER  END

				
                // For TRACTOR
				//print_r($this->input->post('tractor_radius_of_operation'));
                $tractor_radius_of_operation = $this->input->post('tractor_radius_of_operation');
				$tractor_radius_of_operation_other = $this->input->post('tractor_radius_of_operation_other');
				$tractor_request_for_radius_of_operation = $this->input->post('tractor_request_for_radius_of_operation');
				$number_of_axles = $this->input->post('tractor_number_of_axles');
				
                /*$tractor_trailers = $this->input->post('tractor_trailers');*/
				
                $tractor_vehicle_year_vh = $this->input->post('tractor_vehicle_year_vh');
                $tractor_make_model_vh = $this->input->post('tractor_make_model_vh');
                $tractor_gvw_vh = $this->input->post('tractor_gvw_vh');
                $tractor_vin_vh = $this->input->post('tractor_vin_vh'); 
				
                $tractor_liability_vh = $this->input->post('tractor_liability_vh');
                $tractor_liability_ded_vh = $this->input->post('tractor_liability_ded_vh');
				$tractor_um = $this->input->post('tractor_um');
				$tractor_pip = $this->input->post('tractor_pip');
				
				$tractor_pd_vh = $this->input->post('tractor_pd_vh');
                $tractor_ph_ded_vh = $this->input->post('tractor_ph_ded_vh');
                $tractor_value_amount_vh = $this->input->post('tractor_value_amount_vh');
				
				$tractor_cargo_vh = $this->input->post('tractor_cargo_vh');
				$tractor_cargo_ded_vh = $this->input->post('tractor_cargo_ded_vh');
				
				$commodities_haulted_tractor = $this->input->post('commodities_haulted_tractor');
                
				
				/*
                $tractor_refrigerated_breakdown = $this->input->post('tractor_refrigerated_breakdown');
                $tractor_deductible = $this->input->post('tractor_deductible');*/
				
				//print_r($this->input->post());
				$trailer_start_count = 0;
				//Start Loop to handle tractor
				for ($k = 0; $k < count($tractor_vehicle_year_vh); $k++) {
				
					$parent_tractor_id = 0;
					//echo $tractor_radius_of_operation[$k];
					 if ($tractor_vehicle_year_vh[$k] != '') {
						$insert_data3['quote_id'] = $result;
                        $insert_data3['vehicle_type'] = 'TRACTOR';
						
						$insert_data3['radius_of_operation'] = $tractor_radius_of_operation[$k];
						$insert_data3['radius_of_operation_other'] = $tractor_radius_of_operation_other[$k];
						$insert_data3['request_for_radius_of_operation'] = $tractor_request_for_radius_of_operation[$k];
						$insert_data3['number_of_axles'] = $number_of_axles[$k];
						
						$insert_data3['vehicle_year'] = $tractor_vehicle_year_vh[$k];
						$insert_data3['make_model'] = $tractor_make_model_vh[$k];
						$insert_data3['gvw'] = $tractor_gvw_vh[$k];
						$insert_data3['vin'] = $tractor_vin_vh[$k];
						
						$insert_data3['liability'] = $tractor_liability_vh[$k];
						$insert_data3['liability_ded'] = $tractor_liability_ded_vh[$k];
						$insert_data3['um'] = $tractor_um[$k];
						$insert_data3['pip'] = $tractor_pip[$k];
						
						$insert_data3['pd'] = $tractor_pd_vh[$k];
						$insert_data3['pd_ded'] = $tractor_ph_ded_vh[$k];
						$insert_data3['vaule'] = $tractor_value_amount_vh[$k];
						
						$insert_data3['cargo'] = $tractor_cargo_vh[$k];
						$insert_data3['cargo_ded'] = $tractor_cargo_ded_vh[$k];
						
						$insert_data3['commodities_haulted'] = $commodities_haulted_tractor[$k];
						
						$this->db->insert('rq_rqf_quote_vehicle', $insert_data3);
						$parent_tractor_id = $this->db->insert_id();
						
						if(isset($number_of_trailers_associated_to_tractor[$k]) && $number_of_trailers_associated_to_tractor[$k] !='') { 
							$tractor_trailer = $number_of_trailers_associated_to_tractor[$k] + $trailer_start_count; 
						} else { 
							$tractor_trailer = '0'; 
						}
						

						$trailer_start_in = $trailer_start_count;
						//Start of Loop to handle trailers
						for($m = $trailer_start_in; $m < $tractor_trailer; $m++) {
							
							$insert_data_trailer['quote_id'] = $result;
							$insert_data_trailer['vehicle_type'] = 'TRAILER';
							$insert_data_trailer['parent_id'] = $parent_tractor_id;

							$insert_data_trailer['vehicle_year'] = $trailer_vehicle_year_vh[$m];
							$insert_data_trailer['make_model'] =	$trailer_make_model_vh[$m];						
							$insert_data_trailer['gvw'] = $trailer_gvw_vh[$m];
							$insert_data_trailer['vin'] = $trailer_vin_vh[$m];
							$insert_data_trailer['type_of_trailer'] = $trailer_type[$m];
							$insert_data_trailer['owned_vehicle'] = $trailer_owned_vehicle[$m];
							
							$insert_data_trailer['liability'] = $trailer_liability_vh[$m];
							$insert_data_trailer['liability_ded'] = $trailer_liability_ded_vh[$m];
							$insert_data_trailer['pd'] = $trailer_pd_vh[$m];
							$insert_data_trailer['pd_ded'] = $trailer_ph_ded_vh[$m];
							
							$insert_data_trailer['cargo'] = $trailer_cargo_vh[$m];
							$insert_data_trailer['cargo_ded'] = $trailer_cargo_ded_vh[$m];
							$insert_data_trailer['refrigerated_break_down'] = isset($trailer_refrigerated_breakdown[$m]) ? "YES" : "NO";
							$insert_data_trailer['break_down_deductible'] = $trailer_deductible[$m];
							
							$this->db->insert('rq_rqf_quote_vehicle', $insert_data_trailer);
							
							$trailer_start_count++;
						 }//End of loop to handle trailers
						 
					}//End IF
				}//End Loop to handle tractor
				
				
				
		/*------------------------------------------------------------------------------------------------------*/

		// Start Driver Section
			$driver_name = $this->input->post('driver_name');
			$driver_middle_name = $this->input->post('driver_middle_name');
			$driver_last_name = $this->input->post('driver_last_name');
			$driver_license = $this->input->post('driver_license');
			$driver_license_class = $this->input->post('driver_license_class');
			$driver_license_issue_date = $this->input->post('driver_license_issue_date');
			$driver_license_issue_state = $this->input->post('driver_license_issue_state');
			$driver_dob = $this->input->post('driver_dob');
			$driver_class_a_years = $this->input->post('driver_class_a_years');
			
			if(count($driver_name)){
				for ($l = 0; $l < count($driver_name); $l++) 
				{
					if ($driver_name[$l] != '') 
					{
						$insert_data4['quote_id'] = $result_id_quote;
						$insert_data4['driver_name'] = $driver_name[$l];
						$insert_data4['driver_middle_name'] = $driver_middle_name[$l];
						$insert_data4['driver_last_name'] = $driver_last_name[$l];
						$insert_data4['driver_licence_no'] = $driver_license[$l];
						$insert_data4['driver_license_class'] = $driver_license_class[$l];
						$insert_data4['driver_license_issue_date'] = date("Y-m-d", strtotime($driver_license_issue_date[$l]));
						$insert_data4['driver_license_issue_state'] = $driver_license_issue_state[$l];
						$insert_data4['driver_dob'] = date("Y-m-d", strtotime($driver_dob[$l]));
						$insert_data4['driver_class_a_years'] = $driver_class_a_years[$l];
						
						$this->db->insert('rq_rqf_quote_drivers', $insert_data4);
					}
				}
			}
			//End Driver Section
			
		/*------------------------------------------------------------------------------------------------------*/
			// Start Owner Section 
			$owner_name = $this->input->post('owner_name');
			$owner_middle_name = $this->input->post('owner_middle_name');
			$owner_last_name = $this->input->post('owner_last_name');
			$owner_is_driver = $this->input->post('owner_is_driver');
			$owner_license = $this->input->post('owner_license');
			$owner_licnese_issue_state = $this->input->post('license_issue_state');
			if(count($owner_name) > 0) {	
				for ($m = 0; $m < count($owner_name); $m++) {

					if ($owner_name[$m] != '') 
					{
						$insert_data5['quote_id'] = $result_id_quote;
						$insert_data5['owner_name'] = $owner_name[$m];
						$insert_data5['owner_middle_name'] = $owner_middle_name[$m];
						$insert_data5['owner_last_name'] = $owner_last_name[$m];
						$insert_data5['is_driver'] = $owner_is_driver[$m];
						$insert_data5['driver_licence_no'] = $owner_license[$m];
						$insert_data5['license_issue_state'] = $owner_licnese_issue_state[$m];
						$this->db->insert('rq_rqf_quote_owner', $insert_data5);
					}
				}
			}
			// End Owner Section
				
		/*------------------------------------------------------------------------------------------------------*/				
			// Start Filing Section
			$filing_type = $this->input->post('filing_type');
			$filing_numbers = $this->input->post('filing_numbers');
			if(count($filing_type) > 0 && $this->input->post('filing_type') != '') { 
				for ($n = 0; $n < count($filing_type); $n++) {

					if (isset($owner_name[$n]) && $owner_name[$n] != '') {
						$insert_data6['quote_id'] = $result_id_quote;
						$insert_data6['filings_type'] = $filing_type[$n];
						$insert_data6['filing_no'] = $filing_numbers[$n];
						$this->db->insert('rq_rqf_quote_filings', $insert_data6);
					}
				}
			}
			//End of Filing Section
		
		/*------------------------------------------------------------------------------------------------------*/
		
                $this->session->set_flashdata('message', '<p class="status_msg">Data Inserted Successfully.</p>');
				return ($return_res);
               
            } else {

                $this->data['message'] = validation_errors('<p class="error_msg">', '</p>');
                //   $this->session->set_flashdata('message', '<p class="status_msg">data inserted successfully.</p>');
            }
        }
		
		
    }
	
	  function  get_underwriter_email($member_id){

        $where_array = array('rq_members.id' => $member_id);
        $this->db->select('rq_members.underwriter');
        $this->db->from('rq_members');
        $this->db->where($where_array);

        $query = $this->db->get();
        $this->data['owner'] = $query->result();
		$email = array();
		foreach($this->data['owner'] as $underwriter)
		{
			$keys = explode(',', $underwriter->underwriter);
			for($i =0;$i<count($keys);$i++)
			{
				$where_array = array('rq_underwriter.id' => $keys[$i]);
				$this->db->select('rq_underwriter.email');
				$this->db->where($where_array);
				$this->db->from('rq_underwriter');
				$query_underwriter = $this->db->get();
				
				foreach($query_underwriter->result() as $result){
					$email[] = $result->email;
				}
			}
		}
		return $email;
	}
	
	public function get_underwriter_email_address($member_id)
	{
		$where_array = array('rq_members.id' => $member_id);
        $this->db->select('rq_members.underwriter');
        $this->db->from('rq_members');
        $this->db->where($where_array);
		$query = $this->db->get();
		$this->data['owner'] = $query->result();
		$emailAdd = array();
		foreach($this->data['owner'] as $underwriter)
		{
			list($primary, $uw) = explode(':', $underwriter->underwriter);
			
			$priquery = $this->db->get_where('rq_underwriter', array('id'=>$primary));
			$queryval = $priquery->row();
			$emailAdd['email'] = $queryval->email;
			
			$keys = explode(',', $uw);
			for($i =0;$i<count($keys);$i++)
			{
				if ($keys[$i]!=$primary)
				{
					$where_array = array('rq_underwriter.id' => $keys[$i]);
					$this->db->select('rq_underwriter.email');
					$this->db->where($where_array);
					$this->db->from('rq_underwriter');
					$query_underwriter = $this->db->get();
				
					foreach($query_underwriter->result() as $result){
						$emailAdd['ccEmail'][] = $result->email;
					}
				}
			}
		}
		return $emailAdd;
	}
	
		
	public function get_supervisor_email()
	{
		$this->db->where('key','supervisor_email');
		$this->db->where('status',1);
		$query = $this->db->get($this->config->item('config_table'));
		if($query->num_rows == 1) 
	  	{
			$result = $query->row();
			return $result->value;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function file_upload_one($upload_location)
	{
		/* mkdir('./uploads/request_quote/'.$quote_id);
		$config['upload_path'] = './uploads/request_quote/'.$quote_id; */
		$config['upload_path'] = $upload_location;
		//$config['allowed_types'] = 'gif|jpg|png|doc|txt|docx|xls|xlsx|bmp|rtf|pdf';
		$config['allowed_types'] = '*';
		
		//$config['overwrite'] = FALSE; //overwrite user avatar
        //$config['encrypt_name'] = TRUE;
		
		//$config['max_size']	= '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';

		$this->load->library('upload', $config);
		//echo $field_name; die;
		if ($this->upload->do_multi_upload('file_sec_1'))
		{
			$uploadInfoSec1 = $this->upload->get_multi_upload_data();
			$fileNamesSec1Commaseparated = '';
			$counter = 1;
			foreach($uploadInfoSec1 as $file)
			{
				$fileNamesSec1Commaseparated .= $file['file_name'];
				
				if(count($uploadInfoSec1) != $counter)
					$fileNamesSec1Commaseparated .= ', ';
				
				$counter++;
			}
			return $fileNamesSec1Commaseparated;
		}	
	}
	
	public function file_upload_three($upload_location)
	{
		/* mkdir('./uploads/request_quote/'.$quote_id);
		$config['upload_path'] = './uploads/request_quote/'.$quote_id; */
		$config['upload_path'] = $upload_location;
		//$config['allowed_types'] = 'gif|jpg|png|doc|txt|docx|xls|xlsx|bmp|rtf|pdf';
		$config['allowed_types'] = '*';
		
		//$config['overwrite'] = FALSE; //overwrite user avatar
        //$config['encrypt_name'] = TRUE;
		
		//$config['max_size']	= '100';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';

		$this->load->library('upload', $config);
		//echo $field_name; die;
		if ($this->upload->do_multi_upload('file_sec_3'))
		{
			$uploadInfoSec2 = $this->upload->get_multi_upload_data();
			$fileNamesSec1Commaseparated = '';
			$counter = 1;
			foreach($uploadInfoSec2 as $file)
			{
				$fileNamesSec1Commaseparated .= $file['file_name'];
				
				if(count($uploadInfoSec2) != $counter)
					$fileNamesSec1Commaseparated .= ',';
				
				$counter++;
			}
			return $fileNamesSec1Commaseparated;
		}	
	}
	
	function get_quote_data(){
		$output = '';
		if($this->session->userdata('member_id')){
			$member_id = $this->session->userdata('member_id');
			$where_array = array('rq_members.id' => $member_id);
			$this->db->where($where_array);
			$this->db->from('rq_members');
			$query_quote_member_data = $this->db->get();
			$output = $query_quote_member_data->row();
		}
		return $output;
	}

}

/* End of file quote_model.php */
/* Location: ./application/models/quote_model.php */