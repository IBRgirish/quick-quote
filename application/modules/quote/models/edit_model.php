<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class edit_model extends CI_Model {




    public function &__get($key) {
        $CI = & get_instance();
        return $CI->$key;
        $this->load->database();
		
		$this->load->model('new_quote_model');
    }
	
	
	/**
	* Upload file using CI
	* @$data return uploaded file name
	* @$error return errors during file uploading
	*/
	public function do_upload($file_types='*',$name, $path, $file_name='') {
        $config['upload_path'] = 'uploads/'.$path;
        $config['allowed_types'] = $file_types;
    
		if($file_name){
			$config['file_name'] = $file_name;
		}
      

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($name)) {
            $error = array( 'error' => $this->upload->display_errors() );
            
            return $error;
        } else {
            $data = array( 'upload_data' => $this->upload->data() );
            return $data['upload_data'];
           
        }
    }	
	
	public function application_ghi1a($id=""){
	$insert_id=$this->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	
	$mail_id = $this->input->post('mail_id');
	

	$data_mail['quote_id'] =  $quote_id;	
	$data_mail['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_mail['mail_address'] = $this->input->post('mail_address');
	$data_mail['mail_city'] = $this->input->post('mail_city');
	$data_mail['mail_country'] = $this->input->post('mail_country');
	$data_mail['mail_state'] = $this->input->post('mail_state');
	$data_mail['mail_zip'] = $this->input->post('mail_zip');
	$data_mail['contact_fname'] = $this->input->post('contact_fname');
	$data_mail['contact_mname'] = $this->input->post('contact_mname');	
	$data_mail['contact_lname'] = $this->input->post('contact_lname');
	$data_mail['contact_tel'] = $this->input->post('contact_tel');
	$data_mail['contact_cell'] = $this->input->post('contact_cell');
	
	$data_mail['contact_fax'] = $this->input->post('contact_fax');
	$data_mail['contact_email'] = $this->input->post('contact_email');

		if($mail_id!=''){
					$this->_updateapp($id, $data_mail,'application_ghi1a');
				
					}else{
					
					$this->db->insert('application_ghi1a', $data_mail);
					
				}
	
	
	
	
	}
	
public function application_ghi1b($id=""){
	$insert_id=$this->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	$garage_id = $this->input->post('garage_id');
	$garage_location = $this->input->post('garage_location');
	$garage_city = $this->input->post('garage_city');
	$garage_country = $this->input->post('garage_country');
	$garage_state = $this->input->post('garage_state');
	$garage_zip = $this->input->post('garage_zip');
	$contacts_fname = $this->input->post('contacts_fname');
	
	$contacts_mname = $this->input->post('contacts_mname');
	$contacts_lname = $this->input->post('contacts_lname');
	
	$contacts_tel = $this->input->post('contacts_tel');
	$contacts_cell = $this->input->post('contacts_cell');
	$contacts_fax = $this->input->post('contacts_fax');
	$contacts_email = $this->input->post('contacts_email');
	
	$a1=1;
    $garage_old_id=array();
	$garage_new_id=array();
      for ($i = 0; $i < count($garage_location); $i++) 
				{
					if ($garage_location[$i] != '') 
					 { 
	$contacts_checkbox1 = $this->input->post($a1.'contacts_checkbox');
	$contacts_checkbox=!empty($contacts_checkbox1)? implode(',',$contacts_checkbox1):NULL;	
		
	$data_garage['quote_id'] =  $quote_id;	
	$data_garage['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_garage['garage_location'] = isset($garage_location[$i])? $garage_location[$i]:NULL;
	$data_garage['garage_city'] = isset($garage_city[$i]) ? $garage_city[$i]:NULL;
	$data_garage['garage_country'] = isset($garage_country[$i]) ? $garage_country[$i]:NULL;
	$data_garage['garage_state'] = isset($garage_state[$i]) ? $garage_state[$i]:NULL;
	$data_garage['garage_zip'] = isset($garage_zip[$i]) ? $garage_zip[$i]:NULL;
	$data_garage['contacts_fname'] = isset($contacts_fname[$i]) ? $contacts_fname[$i]:NULL;	
	$data_garage['contacts_mname'] = isset($contacts_mname[$i]) ? $contacts_mname[$i]:NULL;	
	$data_garage['contacts_lname'] = isset($contacts_lname[$i]) ? $contacts_lname[$i]:NULL;
	$data_garage['contacts_tel'] = isset($contacts_tel[$i]) ? $contacts_tel[$i]:NULL;
	$data_garage['contacts_cell'] = isset($contacts_cell[$i]) ? $contacts_cell[$i]:NULL;
	$data_garage['contacts_fax'] = isset($contacts_fax[$i]) ? $contacts_fax[$i]:NULL;
	$data_garage['contacts_email'] = isset($contacts_email[$i]) ? $contacts_email[$i]:NULL;
	$data_garage['contacts_checkbox'] = isset($contacts_checkbox) ? $contacts_checkbox:NULL;

						
						
					if(!empty($garage_id[$i])){
					$this->application_model->_updateapp1($id, $data_garage,'application_ghi1b',$garage_id[$i],'garage_id');
					$garage_old_id[]=$garage_id[$i];
					}else{
				
					$this->db->insert('application_ghi1b', $data_garage);
					$garage_new_id[]=$this->db->insert_id();
				}
						
					$a1++;	
						
					}
					
				}
		$this->is_delete($garage_old_id,$garage_new_id,'application_ghi1b','garage_id',$id);

	
	}
	
	
	
	public function app_edit_db($id=""){
	$insert_id=$this->getid();
    $ref_id=$insert_id[0]->id;
	$quote_id = $this->input->post('quote_id');
	
	$app_edit_id = $this->input->post('app_edit_id');	

	$zip=$this->input->post('applicant_zip1').'-'.$this->input->post('applicant_zip2');
	$data_applicant['quote_id'] =  !empty($id) ? $id : $ref_id;
	$data_applicant['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_applicant['applicant_fname'] = $this->input->post('applicant_fname');
	$data_applicant['applicant_mname'] = $this->input->post('applicant_mname');
	$data_applicant['applicant_lname'] = $this->input->post('applicant_lname');
	$data_applicant['applicant_dba'] = $this->input->post('applicant_dba');
	$data_applicant['applicant_address'] = $this->input->post('applicant_address');
	$data_applicant['applicant_city'] = $this->input->post('applicant_city');
	$data_applicant['applicant_country'] = $this->input->post('applicant_country');
	
	$data_applicant['applicant_state'] = $this->input->post('applicant_state');
	$data_applicant['applicant_zip1'] = $this->input->post('applicant_zip1') ;
	$data_applicant['applicant_zip2'] = $this->input->post('applicant_zip2') ;
	$data_applicant['applicant_from'] = $this->input->post('applicant_from');
	$data_applicant['applicant_to'] = $this->input->post('applicant_to');
	$data_applicant['applicant_ca'] = $this->input->post('applicant_ca');
	
	$data_applicant['applicant_ma'] = $this->input->post('applicant_ma');
	$data_applicant['applicant_dot'] = $this->input->post('applicant_dot');
	$data_applicant['applicant_entity'] = $this->input->post('applicant_entity');
	$data_applicant['applicant_role'] = $this->input->post('applicant_role');
	$data_applicant['applicant_explain'] = $this->input->post('applicant_explain');
	
	
		if($app_edit_id!=''){
					$this->_updateapp($id, $data_applicant,'rq_rqf_quick_quote_gh8');
					}else{					
					$this->db->insert('rq_rqf_quick_quote_gh8', $data_applicant);
					
				}
	
	}
	
	
	public function app_edit_ghi6($id=""){
	
	$insert_id=$this->getid();
    $ref_id=$insert_id[0]->id;
	$quote_id = $this->input->post('quote_id');
	
	$supplement_id = $this->input->post('supplement_id');
	$supplement_fname = $this->input->post('supplement_fname');
	$supplement_mname = $this->input->post('supplement_mname');
	$supplement_lname = $this->input->post('supplement_lname');
	$supplement_dfrom = $this->input->post('supplement_dfrom');
	$supplement_dto = $this->input->post('supplement_dto');
	

	$supplement_lnum = $this->input->post('supplement_lnum');
	$supplement_Reason = $this->input->post('supplement_Reason');
    $supplement_parent_id = $this->input->post('supplement_parent_id');
	$supplement_old_id=array();
	$supplement_new_id=array();
	
	

      for ($i = 0; $i < count($supplement_fname); $i++) 
				{
		if ($supplement_fname[$i] != '') 
					{ 
					
	$data_supplement['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_supplement['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_supplement['supplement_fname'] = $supplement_fname[$i];
	$data_supplement['supplement_mname'] = $supplement_mname[$i];
	$data_supplement['supplement_lname'] = $supplement_lname[$i];
	$data_supplement['supplement_dfrom'] = $supplement_dfrom[$i];
	$data_supplement['supplement_dto'] = $supplement_dto[$i];
	$data_supplement['supplement_lnum'] = $supplement_lnum[$i];
	$data_supplement['supplement_Reason'] = $supplement_Reason[$i];
    $data_supplement['supplement_parent_id'] = $supplement_parent_id[$i];
						
				
					if(!empty($supplement_id[$i])){
					$this->_updateapp1($id, $data_supplement,'rq_rqf_quick_quote_gh6',$supplement_id[$i],'supplement_id');
					$supplement_old_id[]=$supplement_id[$i];
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh6', $data_supplement);
					
					$supplement_new_id[]=$this->db->insert_id();
				}
					
						
					}
					
				}
				

	if(!empty($supplement_old_id)){
	 $result=array();
	 $adresses=$this->get_id_inform($id,'rq_rqf_quick_quote_gh6');
	   foreach($adresses as $values){
	
		   $result[]=$values->supplement_id;
		
	   }
	$flag_set= array_diff($result,$supplement_old_id,$supplement_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'rq_rqf_quick_quote_gh6',$vlaues,'supplement_id');
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform($id,'rq_rqf_quick_quote_gh6');
	   foreach($array as $values){
	  
		   $result[]=$values->supplement_id;
		
	   }
	
	$flag_set= array_diff($result,$supplement_new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($id, $insert_data130,'rq_rqf_quick_quote_gh6',$vlaues,'supplement_id');
	
	     }
	    }
	  
	  }
	
	
	}	
				
				
				
	}
	
	public function app_edit_ghi9($id=""){
	
	$insert_id=$this->getid();
    $ref_id=$insert_id[0]->id;
	$quote_id = $this->input->post('quote_id');
	$file_types=$this->config->item('doc_image_file_types');
	
	$reg_id = $this->input->post('reg_id');
	$reg_fname = $this->input->post('reg_fname');
	$reg_mname = $this->input->post('reg_mname');
	$reg_lname = $this->input->post('reg_lname');
	$reg_address = $this->input->post('reg_address');
	$reg_city = $this->input->post('reg_city');
	
	$reg_country = $this->input->post('reg_country');
	$reg_state = $this->input->post('reg_state');
	
	$reg_zip = $this->input->post('reg_zip');
	$reg_zip1 = $this->input->post('reg_zip1');
	$reg_lnum = $this->input->post('reg_lnum');
	$reg_ssn = $this->input->post('reg_ssn');
	
	$MVR_file = $this->input->post('MVR_file');
	$reg_explain = $this->input->post('reg_explain');
	
	$supp_dba = $this->input->post('supp_dba');
	$supp_val = $this->input->post('supp_val');
	 
    $reg_r1 = $this->input->post('reg_r0');
    $MVR_file = isset($_FILES['MVR_file']) ? $this->new_quote_model->upload_files($file_types,'docs', $_FILES['MVR_file']) : '';
	
	$reg_old_id=array();
	$reg_new_id=array();	        
      for ($i = 0; $i < count($reg_fname); $i++) 
				{
					if ($reg_fname[$i] != '') 
					{ 
		
	$data_reg['quote_id'] =  !empty($id) ? $id : $ref_id;
	$data_reg['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_reg['reg_fname'] = !empty($reg_fname[$i])?$reg_fname[$i]:NULL;
	$data_reg['reg_mname'] = !empty($reg_mname[$i])?$reg_mname[$i]:NULL;
	$data_reg['reg_lname'] = !empty($reg_lname[$i])?$reg_lname[$i]:NULL;
	$data_reg['reg_address'] = !empty($reg_address[$i])?$reg_address[$i]:NULL;
	$data_reg['reg_city'] = !empty($reg_city[$i])?$reg_city[$i]:NULL;
	$data_reg['reg_country'] = !empty($reg_country[$i])?$reg_country[$i]:NULL;
	$data_reg['reg_state'] = !empty($reg_state[$i])?$reg_state[$i]:NULL;
	$data_reg['reg_zip'] = !empty($reg_zip[$i])?$reg_zip[$i]:NULL;
	$data_reg['reg_zip1'] = isset($reg_zip1[$i])?$reg_zip1[$i]:NULL;
	$data_reg['reg_lnum'] = !empty($reg_lnum[$i])?$reg_lnum[$i]:NULL;
	$data_reg['reg_ssn'] = !empty($reg_ssn[$i])?$reg_ssn[$i]:NULL;
	$data_reg['reg_r1'] = !empty($reg_r1[$i])?$reg_r1[$i]:NULL;	   
	$data_reg['MVR_file'] =!empty($MVR_file[$i])?$MVR_file[$i]:NULL;
	$data_reg['reg_explain'] = !empty($reg_explain[$i])?$reg_explain[$i]:NULL;
	$data_reg['supp_dba']=!empty($supp_dba[$i])?$supp_dba[$i]:NULL;
	$data_reg['supp_val']=!empty($supp_val[$i])?$supp_val[$i]:NULL;					
						
					if(!empty($reg_id[$i])){
					$this->_updateapp1($id, $data_reg,'rq_rqf_quick_quote_gh9',$reg_id[$i],'reg_id');
					$reg_old_id[]=$reg_id[$i];
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh9', $data_reg);
				    $reg_new_id[]=$this->db->insert_id();
				}
						
						
						
					}
					
				}

				
				$this->is_delete($reg_old_id,$reg_new_id,'rq_rqf_quick_quote_gh9','reg_id',$id);
	
	
	
	
	
	}
	
	
	public function app_edit_ghi11($id=""){
	$insert_id=$this->getid();
    $ref_id=$insert_id[0]->id;
	$quote_id = $this->input->post('quote_id');
	$vehicles_id = $this->input->post('vehicles_id');

	$vehicles_VIN = $this->input->post('vehicles_VIN');
	$vehicles_GVW = $this->input->post('vehicles_GVW');
	$vehicles_Year = $this->input->post('vehicles_Year');
	$vehicles_make = $this->input->post('vehicles_make');
	$vehicles_body = $this->input->post('vehicles_body');
	$vehicles_model = $this->input->post('vehicles_model');
	$vehicles_reason = $this->input->post('vehicles_reason');
	
	$vehicles_old_id=array();
	$vehicles_new_id=array();
      for ($i = 0; $i < count($vehicles_VIN); $i++) 
				{
					if ($vehicles_VIN[$i] != '') 
					{ 
					
	$data_vehicles['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_vehicles['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_vehicles['vehicles_VIN'] = $vehicles_VIN[$i];
	$data_vehicles['vehicles_GVW'] = $vehicles_GVW[$i];
	$data_vehicles['vehicles_Year'] = $vehicles_Year[$i];
	$data_vehicles['vehicles_make'] = $vehicles_make[$i];
	$data_vehicles['vehicles_body'] = $vehicles_body[$i];
	$data_vehicles['vehicles_model'] = $vehicles_model[$i];	
	$data_vehicles['vehicles_reason'] = $vehicles_reason[$i];	

								if(!empty($vehicles_id[$i])){
					$this->_updateapp1($id, $data_vehicles,'rq_rqf_quick_quote_gh11',$vehicles_id[$i],'vehicles_id');
					$vehicles_old_id[]=$vehicles_id[$i];
					}else{
			
					$this->db->insert('rq_rqf_quick_quote_gh11', $data_vehicles);
				    $vehicles_new_id[]=$this->db->insert_id();
				         }
					
						
					}
					
				}

	$this->is_delete($vehicles_old_id,$vehicles_new_id,'rq_rqf_quick_quote_gh11','vehicles_id',$id);
	
	
	
	
	}
	
	
public function app_edit_ghi13($id=""){
	$carriers_tele=array();
	$retail_tele=array();
	$insert_id=$this->getid();
    $ref_id=$insert_id[0]->general_id;
	$quote_id = $this->input->post('quote_id');
	
	$carriers_id = $this->input->post('carriers_id');
	
	$carriers_name = $this->input->post('carriers_name');
	$carriers_tel = $this->input->post('carriers_tel');
	$carriers_ga = $this->input->post('carriers_ga');

	

	
	//$carriers_tele[] =	isset($carriers_tele1)? implode("-", $carriers_tele1) : '';
	$retail_broker = $this->input->post('retail_broker');
	
	//$retail_tele[] = isset($retail_tele1)? implode("-", $retail_tele1) : '';
	
	$retail_reason = $this->input->post('retail_reason');

    $carriers_old_id=array();
	$carriers_new_id=array();
	$b=1;
      for ($i = 0; $i < count($carriers_name);$i++) 
				{
					if ($carriers_name[$i] != '') 
					{
						
	$data_carriers['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_carriers['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_carriers['carriers_name'] = $carriers_name[$i];
	$data_carriers['carriers_tel'] = $carriers_tel[$i];
	$data_carriers['carriers_ga'] = $carriers_ga[$i];
	$carriers_tele=$this->input->post($b.'carriers_telephone');
	$data_carriers['carriers_telephone'] = isset($carriers_tele)? implode("-", $carriers_tele) : '';
	$data_carriers['retail_broker'] = $retail_broker[$i];
	$retail_tele=$this->input->post($b.'retail_telephone');
	$data_carriers['retail_telephone'] = isset($retail_tele)? implode("-", $retail_tele) : '';
	$data_carriers['retail_reason'] = $retail_reason[$i];	
		if(!empty($carriers_id[$i])){
					$this->application_model->_updateapp1($id, $data_carriers,'application_ghi13',$carriers_id[$i],'carriers_id');
					
					$carriers_old_id[]=$carriers_id[$i];
					}else{
					
					$this->db->insert('application_ghi13', $data_carriers);
					$carriers_new_id[]=$this->db->insert_id();
				}
						
					$b++;	
						
					}
					
				}

	        $this->is_delete($carriers_old_id,$carriers_new_id,'application_ghi13','carriers_id',$id);
	
	
	
	
	}
	
	
	
		
	public function app_edit_ghi51($id=""){		
	$insert_id=$this->getid();
    $ref_id=$insert_id[0]->id;
	$quote_id = $this->input->post('quote_id');		
	
	$hauled_n_id1 = $this->input->post('hauled_n_id1');
	$ghi5_parent_id1=$this->input->post('ghi5_parent_id1');
	$file_types = $this->config->item('doc_image_file_types');
	$data_hauler1['quote_id'] =  !empty($id) ? $id : $ref_id;
	$data_hauler1['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_hauler1['hauler_MVR'] = ($this->input->post('hauler_MVR1'))?$this->input->post('hauler_MVR1'):NULL;
	$data_hauler1['hauler_Roa'] = ($this->input->post('hauler_Roa1'))?$this->input->post('hauler_Roa1'):NULL;
	$data_hauler1['hauler_Wri'] = ($this->input->post('hauler_Wri1'))?$this->input->post('hauler_Wri1'):NULL;
	$data_hauler1['hauler_Phy'] = ($this->input->post('hauler_Phy1'))?$this->input->post('hauler_Phy1'):NULL;
	$data_hauler1['hauler_Drug'] = ($this->input->post('hauler_Drug1'))?$this->input->post('hauler_Drug1'):NULL;
	$data_hauler1['hauler_Ref'] = ($this->input->post('hauler_Ref1'))?$this->input->post('hauler_Ref1'):NULL;
	$data_hauler1['hauler_Ver'] = ($this->input->post('hauler_Ver1'))?$this->input->post('hauler_Ver1'):NULL;
	
	$data_hauler1['ghi5_parent_id'] = $this->input->post('ghi5_parent_id1');
	
	$data_hauler1['hauler_hir_name'] = ($this->input->post('hauler_hir_name1'))?$this->input->post('hauler_hir_name1'):NULL;
	$data_hauler1['hauler_Own'] = ($this->input->post('hauler_Own1'))?$this->input->post('hauler_Own1'):NULL;
	$data_hauler1['hauler_Rev'] = ($this->input->post('hauler_Rev1'))?$this->input->post('hauler_Rev1'):NULL;
	$data_hauler1['hauler_team'] = ($this->input->post('hauler_team1'))?$this->input->post('hauler_team1'):NULL;
	$data_hauler1['hauler_Num'] = ($this->input->post('hauler_Num1'))?$this->input->post('hauler_Num1'):NULL;
	$data_hauler1['hauler_Num_y'] = ($this->input->post('hauler_Num_y1'))?$this->input->post('hauler_Num_y1'):NULL;
	$data_hauler1['hauler_Vehi'] = ($this->input->post('hauler_Vehi1'))?$this->input->post('hauler_Vehi1'):NULL;
	
	
	$data_hauler1['hauler_often'] = ($this->input->post('hauler_often1'))?$this->input->post('hauler_often1'):NULL;
	$data_hauler1['hauler_attach'] = ($this->input->post('hauler_attach1'))?$this->input->post('hauler_attach1'):NULL;
	$data_hauler1['hauler_drivers'] = ($this->input->post('hauler_drivers1'))?$this->input->post('hauler_drivers1'):NULL;
	$data_hauler1['hauler_ins'] = ($this->input->post('hauler_ins1'))?$this->input->post('hauler_ins1'):NULL;
	$data_hauler1['hauler_Pol'] = ($this->input->post('hauler_Pol1'))?$this->input->post('hauler_Pol1'):NULL;
	$data_hauler1['hauler_Effect']= ($this->input->post('hauler_Effect1'))?$this->input->post('hauler_Effect1'):NULL;
	$data_hauler1['hauler_Exp'] = ($this->input->post('hauler_Exp1'))?$this->input->post('hauler_Exp1'):NULL;
	
	
	
	$data_hauler1['hauler_Hir'] = ($this->input->post('hauler_Hir1'))?$this->input->post('hauler_Hir1'):NULL;
	$data_hauler1['hauler_Ter'] = ($this->input->post('hauler_Ter1'))?$this->input->post('hauler_Ter1'):NULL;
	$data_hauler1['hauler_Quit'] = ($this->input->post('hauler_Quit1'))?$this->input->post('hauler_Quit1'):NULL;
	$data_hauler1['hauler_Oth'] = ($this->input->post('hauler_Oth1'))?$this->input->post('hauler_Oth1'):NULL;
	$data_hauler1['hauler_Max'] = ($this->input->post('hauler_Max1'))?$this->input->post('hauler_Max1'):NULL;
	$data_hauler1['hauler_Per']= ($this->input->post('hauler_Per1'))?$this->input->post('hauler_Per1'):NULL;
	$data_hauler1['hauler_day'] = ($this->input->post('hauler_day1'))?$this->input->post('hauler_day1'):NULL;
	
	
	$data_hauler1['hauler_comp'] = ($this->input->post('hauler_comp1'))?$this->input->post('hauler_comp1'):NULL;
	$data_hauler1['hauler_Others'] = ($this->input->post('hauler_Others1'))?$this->input->post('hauler_Others1'):NULL;
	$data_hauler1['hauler_hours'] = ($this->input->post('hauler_hours1'))?$this->input->post('hauler_hours1'):NULL;
	$data_hauler1['hauler_hour'] = ($this->input->post('hauler_hour1'))?$this->input->post('hauler_hour1'):NULL;
	$data_hauler1['hauler_hou'] = ($this->input->post('hauler_hou1'))?$this->input->post('hauler_hou1'):NULL;
	$data_hauler1['hauler_sleep']= ($this->input->post('hauler_sleep1'))?$this->input->post('hauler_sleep1'):NULL;
	//$data_hauler['hauler_Motel'] = $this->input->post('hauler_Motel');
	
	$data_hauler1['hauler_hother'] = ($this->input->post('hauler_hother1'))?$this->input->post('hauler_hother1'):NULL;	
	$data_hauler1['hauler_rest'] = ($this->input->post('hauler_rest1'))?$this->input->post('hauler_rest1'):NULL;
	$data_hauler1['hauler_roth']= ($this->input->post('hauler_roth1'))?$this->input->post('hauler_roth1'):NULL;
	$data_hauler1['hauler_haul'] = ($this->input->post('hauler_haul1'))?$this->input->post('hauler_haul1'):NULL;
	
	$data_hauler1['haul_name'] = ($this->input->post('haul_name1'))?$this->input->post('haul_name1'):NULL;
	$data_hauler1['haul_agree'] = ($this->input->post('haul_agree1'))?$this->input->post('haul_agree1'):NULL;
	$data_hauler1['haul_attach'] = ($this->input->post('haul_attach1'))?$this->input->post('haul_attach1'):NULL;
	$data_hauler1['haul_Last'] = ($this->input->post('haul_Last1'))?$this->input->post('haul_Last1'):NULL;
	$data_hauler1['haul_Next'] = ($this->input->post('haul_Next1'))?$this->input->post('haul_Next1'):NULL;
	
	$a_file1 = !empty($_FILES['haul_attach_file1']) ? $this->do_upload($file_types,'haul_attach_file1','docs') : NULL;

	$data_hauler1['haul_attach_file'] = !empty($a_file1['file_name']) ? str_replace(' ', '_', $a_file1['file_name']) : NULL;

	
	
	$data_hauler1['haul_equi'] = ($this->input->post('haul_equi1'))?$this->input->post('haul_ahaul_equi1ttach_file1'):NULL;
	$data_hauler1['haul_rent'] = ($this->input->post('haul_rent1'))?$this->input->post('haul_rent1'):NULL;
	$data_hauler1['haul_lease'] = ($this->input->post('haul_lease1'))?$this->input->post('haul_lease1'):NULL;
	$data_hauler1['haul_agreement'] = ($this->input->post('haul_agreement1'))?$this->input->post('haul_agreement1'):NULL;
	
	$b_file1= !empty($_FILES['haul_agree_file1']) ? $this->do_upload($file_types,'haul_agree_file1','docs') : NULL;

	$data_hauler1['haul_agree_file'] = !empty($b_file1['file_name']) ? str_replace(' ', '_', $b_file1['file_name']) : NULL;
	
	$data_hauler1['haul_month']= ($this->input->post('haul_month1'))?$this->input->post('haul_month1'):NULL;
	$data_hauler1['haul_year'] = ($this->input->post('haul_year1'))?$this->input->post('haul_year1'):NULL;
	$data_hauler1['haul_bor']= ($this->input->post('haul_bor1'))?$this->input->post('haul_bor1'):NULL;
	$data_hauler1['haul_ope'] = ($this->input->post('haul_ope1'))?$this->input->post('haul_ope1'):NULL;
	
	
	$data_hauler1['haul_yagree'] = ($this->input->post('haul_yagree1'))?$this->input->post('haul_yagree1'):NULL;
	
	$_file1 = !empty($_FILES['haul_yagree_file1']) ? $this->do_upload($file_types,'haul_yagree_file1','docs') : NULL;

	$data_hauler1['haul_yagree_file'] = !empty($_file1['file_name']) ? str_replace(' ', '_', $_file1['file_name']) : NULL;
	
	$data_hauler1['haul_yins'] = ($this->input->post('haul_yins1'))?$this->input->post('haul_yins1'):NULL;
	
	
		
	$data_hauler1['subcon_y'] = ($this->input->post('subcon_y1'))?$this->input->post('subcon_y1'):NULL;
	$data_hauler1['subcon_name'] = ($this->input->post('subcon_name1'))?$this->input->post('subcon_name1'):NULL;
	$data_hauler1['subcon_cert'] = ($this->input->post('subcon_cert1'))?$this->input->post('subcon_cert1'):NULL;
	$data_hauler1['subcon_lia'] = ($this->input->post('subcon_lia1'))?$this->input->post('subcon_lia1'):NULL;
	$data_hauler1['subcon_duties'] = ($this->input->post('subcon_duties1'))?$this->input->post('subcon_duties1'):NULL;
	$data_hauler1['subcon_cost'] = ($this->input->post('subcon_cost1'))?$this->input->post('subcon_cost1'):NULL;
	$data_hauler1['subcon_emp_y'] = ($this->input->post('subcon_emp_y1'))?$this->input->post('subcon_emp_y1'):NULL;
	
	
	$data_hauler1['sub_haul_y'] = ($this->input->post('sub_haul_y1'))?$this->input->post('sub_haul_y1'):NULL;
	$data_hauler1['audit_y'] = ($this->input->post('audit_y1'))?$this->input->post('audit_y1'):NULL;
	$data_hauler1['vehicles_y'] = ($this->input->post('vehicles_y1'))?$this->input->post('vehicles_y1'):NULL;
	$data_hauler1['ride_y'] = ($this->input->post('ride_y1'))?$this->input->post('ride_y1'):NULL;
	$data_hauler1['driver_y'] = ($this->input->post('driver_y1'))?$this->input->post('driver_y1'):NULL;
	$data_hauler1['activity_y'] = ($this->input->post('activity_y1'))?$this->input->post('activity_y1'):NULL;
	
	$data_hauler1['prior_y'] = ($this->input->post('prior_y1'))?$this->input->post('prior_y1'):NULL;
	$data_hauler1['procedure_y'] = ($this->input->post('procedure_y1'))?$this->input->post('procedure_y1'):NULL;
	$data_hauler1['Drug_y'] = ($this->input->post('Drug_y1'))?$this->input->post('Drug_y1'):NULL;
	
	$c_file1['file_name'] = !empty($_FILES['hiring_file1']) ? $this->do_upload($file_types,'hiring_file1','docs') : NULL;
	$data_hauler1['hiring_file'] = !empty($c_file1['file_name']) ? str_replace(' ', '_', $c_file1['file_name']) : NULL;
	
	$data_hauler1['employees_y'] = ($this->input->post('employees_y1'))?$this->input->post('employees_y1'):NULL;
	$data_hauler1['explain_no'] = ($this->input->post('explain_no1'))?$this->input->post('explain_no1'):NULL;
	
	$data_hauler1['safety_y'] = ($this->input->post('safety_y1'))?$this->input->post('safety_y1'):NULL;
	$data_hauler1['details_y'] = ($this->input->post('details_y1'))?$this->input->post('details_y1'):NULL;
	$data_hauler1['program_detail'] = ($this->input->post('program_detail1'))?$this->input->post('program_detail1'):NULL;
	$data_hauler1['load_y'] = ($this->input->post('load_y1'))?$this->input->post('load_y1'):NULL;
	$data_hauler1['ins_pol'] = ($this->input->post('ins_pol1'))?$this->input->post('ins_pol1'):NULL;
	$data_hauler1['safety_attach_file'] = ($this->input->post('safety_attach_file1'))?$this->input->post('safety_attach_file1'):NULL;
//echo '----------------------------------------------';

			
				
			  if($hauled_n_id1!=''){	  
				
					$this->_updateapp1($id,$data_hauler1,'rq_rqf_quick_quote_gh5a',$hauled_n_id1,'haul_id');
				  }else{
					  $this->db->insert('rq_rqf_quick_quote_gh5a',$data_hauler1);
					  $insert_h_id1=$this->db->insert_id();
					  }
				
					
	   
	  
	   
	   
	$haul_id= $this->input->post('haul_ins_id');   
	$add_ins_id1= $this->input->post('add_ins_id1'); 	   
	$add_ins_name1= $this->input->post('add_ins_name1');
	$add_ins_carrier1= $this->input->post('add_ins_carrier1');
	$ins_attach_y1= $this->input->post('ins_attach_y1');
	//$add_ins_attach1 = $this->input->post('add_ins_attach1');
	$add_ins_attach1 = !empty($_FILES['add_ins_attach1']) ? $this->new_quote_model->upload_files($file_types,'docs', $_FILES['add_ins_attach1']) : '';
	
	$add_ins_old_id=array();
	$add_ins_new_id=array();
	
		
      for ($i = 0; $i < count($add_ins_name1); $i++) 
				{
					
	$data_insert121['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	$data_insert121['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : NULL ;
    $data_insert121['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert121['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_insert121['add_ins_name'] = !empty($add_ins_name1[$i]) ? $add_ins_name1[$i] :NULL;
	$data_insert121['add_ins_carrier'] = !empty($add_ins_carrier1[$i]) ? $add_ins_carrier1[$i]:NULL;
	$data_insert121['ins_attach_y'] = !empty($ins_attach_y1[$i]) ? $ins_attach_y1[$i]:NULL;
	$data_insert121['add_ins_attach'] = !empty($add_ins_attach1[$i]) ? $add_ins_attach1[$i] :NULL;
	
				
					if(!empty($add_ins_id1[$i])){
					$this->application_model->_updateapp1($id, $data_insert121,'rq_rqf_quick_quote_gh5b',$add_ins_id1[$i],'add_ins_id');
					$add_ins_old_id[]=$add_ins_id1[$i];
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh5b', $data_insert121);
					$add_ins_new_id[]=$this->db->insert_id();
				         }
			
				}
				
	$this->is_delete($add_ins_old_id,$add_ins_new_id,'rq_rqf_quick_quote_gh5b','add_ins_id',$id,$ghi5_parent_id1);	
	
	
	$add_owner_id1= $this->input->post('add_owner_id1'); 
				   
	$add_owner_name1= $this->input->post('add_owner_name1');
	$add_owner_y1= $this->input->post('add_owner_y1');
	//$add_owner_attach1= $this->input->post('add_owner_attach1');
	$add_owner_attach1 = !empty($_FILES['add_owner_attach1']) ? $this->new_quote_model->upload_files($file_types,'docs', $_FILES['add_owner_attach1']) : '';
	
	$add_owner_old_id=array();
	$add_owner_new_id=array();
		
      for ($j = 0; $j < count($add_owner_name1); $j++) 
				{
			
	$data_insert131['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	$data_insert131['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : NULL ;
    $data_insert131['quote_id'] =  !empty($id) ? $id : $ref_id;;	
	$data_insert131['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_insert131['add_owner_name'] = !empty($add_owner_name1[$j]) ? $add_owner_name1[$j]:NULL;
	$data_insert131['add_owner_y'] = !empty($add_owner_y1[$j]) ? $add_owner_y1[$j]:NULL;
	$data_insert131['add_owner_attach'] = !empty($add_owner_attach1[$j]) ? $add_owner_attach1[$j]:NULL;
	
	
			if(!empty($add_owner_id1[$j])){
					$this->_updateapp1($id, $data_insert131,'rq_rqf_quick_quote_gh5c',$add_owner_id1[$j],'add_owner_id');
					$add_owner_old_id[]=$add_owner_id1[$j];
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh5c', $data_insert131);
					$add_owner_new_id[]=$this->db->insert_id();
				}
	   
					
	    }
				
	$this->is_delete($add_owner_old_id,$add_owner_new_id,'rq_rqf_quick_quote_gh5c','add_owner_id',$id,$ghi5_parent_id1);
		
	
	$subcon_add_id1= $this->input->post('subcon_add_id1'); 
				   
	$subcon_add_name1= $this->input->post('subcon_add_name1');
	$subcon_add_vehicle1= $this->input->post('subcon_add_vehicle1');
	$subcon_add_y1= $this->input->post('subcon_add_y1');
	//$subcon_add_attach1 = $this->input->post('subcon_add_attach1');
	$subcon_add_attach1 = !empty($_FILES['subcon_add_attach1']) ? $this->new_quote_model->upload_files($file_types,'docs', $_FILES['subcon_add_attach1']) : '';
	
	$subcon_add_old_id=array();	
	$subcon_add_new_id=array();	
	
      for ($k = 0; $k < count($subcon_add_name1); $k++) 
				{
					
	$data_insert171['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	$data_insert171['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : NULL ;	
    $data_insert171['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert171['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert171['subcon_add_name'] = !empty($subcon_add_name1[$k]) ? $subcon_add_name1[$k]:NULL;
	$data_insert171['subcon_add_vehicle'] = !empty($subcon_add_vehicle1[$k])?$subcon_add_vehicle1[$k] :NULL;
	$data_insert171['subcon_add_y'] = !empty($subcon_add_y1[$k])? $subcon_add_y1[$k]:NULL;
	$data_insert171['subcon_add_attach'] = !empty($subcon_add_attach1[$k]) ? $subcon_add_attach1[$k]:NULL;
	
						
					
			
				if(!empty($subcon_add_id1[$k])){
					$this->_updateapp1($id, $data_insert171,'rq_rqf_quick_quote_gh5d',$subcon_add_id1[$k],'subcon_add_id');
					$subcon_add_old_id[]=$subcon_add_id1[$k];
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh5d', $data_insert171);
				    $subcon_add_new_id[]=$this->db->insert_id(); 
				      }
				
					
				}
	$this->is_delete($subcon_add_old_id,$subcon_add_new_id,'rq_rqf_quick_quote_gh5d','subcon_add_id',$id,$ghi5_parent_id1);		
	
	

	$sub_haul_id1= $this->input->post('sub_haul_id1');

	$sub_haul_add_name1= $this->input->post('sub_haul_add_name1');
	$sub_haul_add_carrier1= $this->input->post('sub_haul_add_carrier1');
	$sub_haul_add_limit1= $this->input->post('sub_haul_add_limit1');
	$sub_haul_add_y1 = $this->input->post('sub_haul_add_y1');
	//$sub_haul_add_attach1= $this->input->post('sub_haul_add_attach1');
	$sub_haul_add_attach1 = !empty($_FILES['sub_haul_add_attach1']) ? $this->new_quote_model->upload_files($file_types,'docs', $_FILES['sub_haul_add_attach1']) : '';
	
	$sub_haul_old_id=array();	
	$sub_haul_new_id=array();	
	
      for ($l = 0; $l < count($sub_haul_add_name1); $l++) 
				{
				
		$data_insert141['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	 $data_insert141['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : NULL ;			
    $data_insert141['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert141['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert141['sub_haul_add_name'] = !empty($sub_haul_add_name1[$l]) ? $sub_haul_add_name1[$l]:NULL ;
	$data_insert141['sub_haul_add_carrier'] = !empty($sub_haul_add_carrier1[$l]) ? $sub_haul_add_carrier1[$l] : NULL;
	$data_insert141['sub_haul_add_limit'] = !empty($sub_haul_add_limit1[$l]) ? $sub_haul_add_limit1[$l] :NULL ;
	$data_insert141['sub_haul_add_y'] = !empty($sub_haul_add_y1[$l])? $sub_haul_add_y1[$l]:NULL;
	$data_insert141['sub_haul_add_attach'] = !empty($sub_haul_add_attach1[$l]) ? $sub_haul_add_attach1[$l]:NULL;
	
				
						
					if(!empty($sub_haul_id1[$l])){
				    	$this->_updateapp1($id, $data_insert141,'rq_rqf_quick_quote_gh5e',$sub_haul_id1[$l],'sub_haul_id');
						$sub_haul_old_id[]=$sub_haul_id1[$l];	
	
					}else{
				
					$this->db->insert('rq_rqf_quick_quote_gh5e', $data_insert141);
				     	
	                   $sub_haul_new_id[]=$this->db->insert_id();
				}
						
			}
					
				
	$this->is_delete($sub_haul_old_id,$sub_haul_new_id,'rq_rqf_quick_quote_gh5e','sub_haul_id',$id,$ghi5_parent_id1);			

	
	
	$add_mem_id1= $this->input->post('add_mem_id1');
				
	$add_mem_name1= $this->input->post('add_mem_name1');
	$add_mem_rel1= $this->input->post('add_mem_rel1');

	$add_mem_old_id=array();
	$add_mem_new_id=array();	
      for ($m = 0; $m < count($add_mem_name1); $m++) 
				{
					 
	$data_insert151['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	$data_insert151['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : NULL ;			
    $data_insert151['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert151['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert151['add_mem_name'] = !empty($add_mem_name1[$m])? $add_mem_name1[$m] :NULL;
	$data_insert151['add_mem_rel'] = !empty($add_mem_rel1[$m]) ? $add_mem_rel1[$m]:NULL;

	
	
							if(!empty($add_mem_id1[$m])){
					$this->_updateapp1($id, $data_insert151,'rq_rqf_quick_quote_gh5f',$add_mem_id1[$m],'add_mem_id');
					$add_mem_old_id[]=$add_mem_id1[$m];
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh5f', $data_insert151);
					$add_mem_new_id[]=$this->db->insert_id();
				
						
					}
					
				}
				
	$this->is_delete($add_mem_old_id,$add_mem_new_id,'rq_rqf_quick_quote_gh5f','add_mem_id',$id,$ghi5_parent_id1);
	
	
	$add_list_id1= $this->input->post('add_list_id1'); 
    $hauled_n_id1= $this->input->post('hauled_n_id1'); 				
	$add_list_name1= $this->input->post('add_list_name1');
	$add_list_length1= $this->input->post('add_list_length1');
	$add_list_year1= $this->input->post('add_list_year1');
	$add_list_old_id=array();
	$add_list_new_id=array();	
		
      for ($n = 0; $n < count($add_list_name1); $n++) 
				{
					
    $data_insert161['haul_id'] =  !empty($hauled_n_id1) ? $hauled_n_id1 : $insert_h_id1 ;
	$data_insert161['ghi5_parent_id'] =  !empty($ghi5_parent_id1) ? $ghi5_parent_id1 : NULL;				
		
    $data_insert161['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert161['ref_id'] = !empty($id) ? $id : $ref_id;		
	$data_insert161['add_list_name'] = !empty($add_list_name1[$n]) ? $add_list_name1[$n]:NULL;
	$data_insert161['add_list_length'] = !empty($add_list_length1[$n]) ? $add_list_length1[$n]:NULL;
	$data_insert161['add_list_year'] = !empty($add_list_year1[$n]) ? $add_list_year1[$n]:NULL;
	
	
					if(!empty($add_list_id1[$n])){
					$this->_updateapp1($id, $data_insert161,'rq_rqf_quick_quote_gh5g',$add_list_id1[$n],'add_list_id');
					$add_list_old_id[]=$add_list_id1[$n];
					}else{
			
					$this->db->insert('rq_rqf_quick_quote_gh5g', $data_insert161);
				    $add_list_new_id[]=$this->db->insert_id();
				         }
					
					
				}
	$this->is_delete($add_list_old_id,$add_list_new_id,'rq_rqf_quick_quote_gh5g','add_list_id',$id,$ghi5_parent_id1);
				
	}
	
	
	
	
	public function app_edit_ghi5($id=""){		
		
	$insert_id=$this->getid();
    $ref_id=$insert_id[0]->id;
	$quote_id = $this->input->post('quote_id');	
	$hauled_n_id = $this->input->post('hauled_n_id');	
	$insert_h_id='';
	$file_types = $this->config->item('doc_image_file_types');
	$ghi5_parent_id=$this->input->post('ghi5_parent_id');
	
	$data_hauler['quote_id'] =  !empty($id) ? $id : $ref_id;
	$data_hauler['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_hauler['hauler_MVR'] = ($this->input->post('hauler_MVR'))?$this->input->post('hauler_MVR'):NULL;
	$data_hauler['hauler_Roa'] = ($this->input->post('hauler_Roa'))?$this->input->post('hauler_Roa'):NULL;
	$data_hauler['hauler_Wri'] = ($this->input->post('hauler_Wri'))?$this->input->post('hauler_Wri'):NULL;
	$data_hauler['hauler_Phy'] = ($this->input->post('hauler_Phy'))?$this->input->post('hauler_Phy'):NULL;
	$data_hauler['hauler_Drug'] = ($this->input->post('hauler_Drug'))?$this->input->post('hauler_Drug'):NULL;
	$data_hauler['hauler_Ref'] = ($this->input->post('hauler_Ref'))?$this->input->post('hauler_Ref'):NULL;
	$data_hauler['hauler_Ver'] = ($this->input->post('hauler_Ver'))?$this->input->post('hauler_Ver'):NULL;
	
	$data_hauler['ghi5_parent_id'] = $this->input->post('ghi5_parent_id');
	
	$data_hauler['hauler_hir_name'] = ($this->input->post('hauler_hir_name'))?$this->input->post('hauler_hir_name'):NULL;
	$data_hauler['hauler_Own'] = ($this->input->post('hauler_Own'))?$this->input->post('hauler_Own'):NULL;
	$data_hauler['hauler_Rev'] = ($this->input->post('hauler_Rev'))?$this->input->post('hauler_Rev'):NULL;
	$data_hauler['hauler_team'] = ($this->input->post('hauler_team'))?$this->input->post('hauler_team'):NULL;
	$data_hauler['hauler_Num'] = ($this->input->post('hauler_Num'))?$this->input->post('hauler_Num'):NULL;
	$data_hauler['hauler_Num_y'] = ($this->input->post('hauler_Num_y'))?$this->input->post('hauler_Num_y'):NULL;
	$data_hauler['hauler_Vehi'] = ($this->input->post('hauler_Vehi'))?$this->input->post('hauler_Vehi'):NULL;
	
	
	$data_hauler['hauler_often'] = ($this->input->post('hauler_often'))?$this->input->post('hauler_often'):NULL;
	$data_hauler['hauler_attach'] = ($this->input->post('hauler_attach'))?$this->input->post('hauler_attach'):NULL;
	$data_hauler['hauler_drivers'] = ($this->input->post('hauler_drivers'))?$this->input->post('hauler_drivers'):NULL;
	$data_hauler['hauler_ins'] = ($this->input->post('hauler_ins'))?$this->input->post('hauler_ins'):NULL;
	$data_hauler['hauler_Pol'] = ($this->input->post('hauler_Pol'))?$this->input->post('hauler_Pol'):NULL;
	$data_hauler['hauler_Effect']= ($this->input->post('hauler_Effect'))?$this->input->post('hauler_Effect'):NULL;
	$data_hauler['hauler_Exp'] = ($this->input->post('hauler_Exp'))?$this->input->post('hauler_Exp'):NULL;
	
	
	
	$data_hauler['hauler_Hir'] = ($this->input->post('hauler_Hir'))?$this->input->post('hauler_Hir'):NULL;
	$data_hauler['hauler_Ter'] = ($this->input->post('hauler_Ter'))?$this->input->post('hauler_Ter'):NULL;
	$data_hauler['hauler_Quit'] = ($this->input->post('hauler_Quit'))?$this->input->post('hauler_Quit'):NULL;
	$data_hauler['hauler_Oth'] = ($this->input->post('hauler_Oth'))?$this->input->post('hauler_Oth'):NULL;
	$data_hauler['hauler_Max'] = ($this->input->post('hauler_Max'))?$this->input->post('hauler_Max'):NULL;
	$data_hauler['hauler_Per']= ($this->input->post('hauler_Per'))?$this->input->post('hauler_Per'):NULL;
	$data_hauler['hauler_day'] = ($this->input->post('hauler_day'))?$this->input->post('hauler_day'):NULL;
	
	
	$data_hauler['hauler_comp'] = ($this->input->post('hauler_comp'))?$this->input->post('hauler_comp'):NULL;
	$data_hauler['hauler_Others'] = ($this->input->post('hauler_Others'))?$this->input->post('hauler_Others'):NULL;
	$data_hauler['hauler_hours'] = ($this->input->post('hauler_hours'))?$this->input->post('hauler_hours'):NULL;
	$data_hauler['hauler_hour'] = ($this->input->post('hauler_hour'))?$this->input->post('hauler_hour'):NULL;
	$data_hauler['hauler_hou'] = ($this->input->post('hauler_hou'))?$this->input->post('hauler_hou'):NULL;
	$data_hauler['hauler_sleep']= ($this->input->post('hauler_sleep'))?$this->input->post('hauler_sleep'):NULL;
	//$data_hauler['hauler_Motel'] = $this->input->post('hauler_Motel');
	
	$data_hauler['hauler_hother'] = ($this->input->post('hauler_hother'))?$this->input->post('hauler_hother'):NULL;	
	$data_hauler['hauler_rest'] = ($this->input->post('hauler_rest'))?$this->input->post('hauler_rest'):NULL;
	$data_hauler['hauler_roth']= ($this->input->post('hauler_roth'))?$this->input->post('hauler_roth'):NULL;
	$data_hauler['hauler_haul'] = ($this->input->post('hauler_haul'))?$this->input->post('hauler_haul'):NULL;
	
	$data_hauler['haul_name'] = ($this->input->post('haul_name'))?$this->input->post('haul_name'):NULL;
	$data_hauler['haul_agree'] = ($this->input->post('haul_agree'))?$this->input->post('haul_agree'):NULL;
	$data_hauler['haul_attach'] = ($this->input->post('haul_attach'))?$this->input->post('haul_attach'):NULL;
	$data_hauler['haul_Last'] = ($this->input->post('haul_Last'))?$this->input->post('haul_Last'):NULL;
	$data_hauler['haul_Next'] = ($this->input->post('haul_Next'))?$this->input->post('haul_Next'):NULL;
	
	$a_file = !empty($_FILES['haul_attach_file']) ? $this->do_upload($file_types,'haul_attach_file','docs') : NULL;

	$data_hauler['haul_attach_file'] = !empty($a_file['file_name']) ? str_replace(' ', '_', $a_file['file_name']) : NULL;

	
	
	$data_hauler['haul_equi'] = ($this->input->post('haul_equi'))?$this->input->post('haul_ahaul_equi1ttach_file'):NULL;
	$data_hauler['haul_rent'] = ($this->input->post('haul_rent'))?$this->input->post('haul_rent'):NULL;
	$data_hauler['haul_lease'] = ($this->input->post('haul_lease'))?$this->input->post('haul_lease'):NULL;
	$data_hauler['haul_agreement'] = ($this->input->post('haul_agreement'))?$this->input->post('haul_agreement'):NULL;
	
	$b_file['file_name'] = !empty($_FILES['haul_agree_file']) ? $this->do_upload($file_types,'haul_agree_file','docs') : NULL;

	$data_hauler['haul_agree_file'] = !empty($b_file['file_name']) ? str_replace(' ', '_', $b_file['file_name']) : NULL;

	$data_hauler['haul_month']= ($this->input->post('haul_month'))?$this->input->post('haul_month'):NULL;
	$data_hauler['haul_year'] = ($this->input->post('haul_year'))?$this->input->post('haul_year'):NULL;
	$data_hauler['haul_bor']= ($this->input->post('haul_bor'))?$this->input->post('haul_bor'):NULL;
	$data_hauler['haul_ope'] = ($this->input->post('haul_ope'))?$this->input->post('haul_ope'):NULL;
	
    $data_hauler['haul_yagree'] = ($this->input->post('haul_yagree'))?$this->input->post('haul_yagree'):NULL;
	
	
	$_file = !empty($_FILES['haul_yagree_file']) ? $this->do_upload($file_types,'haul_yagree_file','docs') : NULL;
	$data_hauler['haul_yagree_file'] = !empty($_file['file_name']) ? str_replace(' ', '_', $_file['file_name']) : NULL;
	
	$data_hauler['haul_yins'] = ($this->input->post('haul_yins'))?$this->input->post('haul_yins'):NULL;
	
	
	
	$data_hauler['subcon_y'] = ($this->input->post('subcon_y'))?$this->input->post('subcon_y'):NULL;
	$data_hauler['subcon_name'] = ($this->input->post('subcon_name'))?$this->input->post('subcon_name'):NULL;
	$data_hauler['subcon_cert'] = ($this->input->post('subcon_cert'))?$this->input->post('subcon_cert'):NULL;
	$data_hauler['subcon_lia'] = ($this->input->post('subcon_lia'))?$this->input->post('subcon_lia'):NULL;
	$data_hauler['subcon_duties'] = ($this->input->post('subcon_duties'))?$this->input->post('subcon_duties'):NULL;
	$data_hauler['subcon_cost'] = ($this->input->post('subcon_cost'))?$this->input->post('subcon_cost'):NULL;
	$data_hauler['subcon_emp_y'] = ($this->input->post('subcon_emp_y'))?$this->input->post('subcon_emp_y'):NULL;
	
	
	$data_hauler['sub_haul_y'] = ($this->input->post('sub_haul_y'))?$this->input->post('sub_haul_y'):NULL;
	$data_hauler['audit_y'] = ($this->input->post('audit_y'))?$this->input->post('audit_y'):NULL;
	$data_hauler['vehicles_y'] = ($this->input->post('vehicles_y'))?$this->input->post('vehicles_y'):NULL;
	$data_hauler['ride_y'] = ($this->input->post('ride_y'))?$this->input->post('ride_y'):NULL;
	$data_hauler['driver_y'] = ($this->input->post('driver_y'))?$this->input->post('driver_y'):NULL;
	$data_hauler['activity_y'] = ($this->input->post('activity_y'))?$this->input->post('activity_y'):NULL;
	
	$data_hauler['prior_y'] = ($this->input->post('prior_y'))?$this->input->post('prior_y'):NULL;
	$data_hauler['procedure_y'] = ($this->input->post('procedure_y'))?$this->input->post('procedure_y'):NULL;
	$data_hauler['Drug_y'] = ($this->input->post('Drug_y'))?$this->input->post('Drug_y'):NULL;
	
	$c_file = !empty($_FILES['hiring_file']) ? $this->do_upload($file_types,'hiring_file','docs') : NULL;
	
	$data_hauler['hiring_file'] = !empty($c_file['file_name']) ? str_replace(' ', '_', $c_file['file_name']) : NULL;
	
	$data_hauler['employees_y'] = ($this->input->post('employees_y'))?$this->input->post('employees_y'):NULL;
	$data_hauler['explain_no'] = ($this->input->post('explain_no'))?$this->input->post('explain_no'):NULL;
	
	$data_hauler['safety_y'] = ($this->input->post('safety_y'))?$this->input->post('safety_y'):NULL;
	$data_hauler['details_y'] = ($this->input->post('details_y'))?$this->input->post('details_y'):NULL;
	$data_hauler['program_detail'] = ($this->input->post('program_detail'))?$this->input->post('program_detail'):NULL;
	$data_hauler['load_y'] = ($this->input->post('load_y'))?$this->input->post('load_y'):NULL;
	$data_hauler['ins_pol'] = ($this->input->post('ins_pol'))?$this->input->post('ins_pol'):NULL;
	$data_hauler['safety_attach_file'] = ($this->input->post('safety_attach_file'))?$this->input->post('safety_attach_file'):NULL;
	
		if($hauled_n_id!=''){
			  $this->_updateapp1($id,$data_hauler,'rq_rqf_quick_quote_gh5a',$hauled_n_id,'haul_id');
		}else{
			$this->db->insert('rq_rqf_quick_quote_gh5a',$data_hauler);
			$insert_h_id=$this->db->insert_id();
	    }
  
	$add_ins_id= $this->input->post('add_ins_id'); 
	   
	$add_ins_name= $this->input->post('add_ins_name');
	$add_ins_carrier= $this->input->post('add_ins_carrier');
	$ins_attach_y= $this->input->post('ins_attach_y');
	//$add_ins_attach = $this->input->post('add_ins_attach');
	$add_ins_attach = !empty($_FILES['add_ins_attach']) ? $this->new_quote_model->upload_files($file_types,'docs', $_FILES['add_ins_attach']) : '';
	
	
    $add_ins_old_id1=array();
	$add_ins_new_id1=array();
		
      for ($i = 0; $i < count($add_ins_name); $i++) 
				{
				
	$data_insert12['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;
	$data_insert12['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id : NULL ;
    $data_insert12['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert12['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_insert12['add_ins_name'] = !empty($add_ins_name[$i]) ? $add_ins_name[$i] :NULL;
	$data_insert12['add_ins_carrier'] = !empty($add_ins_carrier[$i]) ? $add_ins_carrier[$i]:NULL;
	$data_insert12['ins_attach_y'] = !empty($ins_attach_y[$i]) ? $ins_attach_y[$i]:NULL;
	$data_insert12['add_ins_attach'] = !empty($add_ins_attach[$i]) ? $add_ins_attach[$i]:NULL;
	
						
						
						
					if(!empty($add_ins_id[$i])){
					$this->_updateapp1($id, $data_insert12,'rq_rqf_quick_quote_gh5b',$add_ins_id[$i],'add_ins_id');
					$add_ins_old_id1[]=$add_ins_id[$i];
	
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh5b', $data_insert12);
					$add_ins_new_id1[]=$this->db->insert_id();
				}
					
			
				}
				
	$this->is_delete($add_ins_old_id1,$add_ins_new_id1,'rq_rqf_quick_quote_gh5b','add_ins_id',$id,$ghi5_parent_id);

	
	$add_owner_id= $this->input->post('add_owner_id'); 
				   
	$add_owner_name= $this->input->post('add_owner_name');
	$add_owner_y= $this->input->post('add_owner_y');
//	$add_owner_attach= $this->input->post('add_owner_attach');
	$add_owner_attach = !empty($_FILES['add_owner_attach']) ? $this->new_quote_model->upload_files($file_types,'docs', $_FILES['add_owner_attach']) : '';
	$add_owner_old_id1=array();
	$add_owner_new_id1=array();
		
      for ($j = 0; $j < count($add_owner_name); $j++) 
				{
					
	$data_insert13['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;
	$data_insert13['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id :NULL ;
    $data_insert13['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert13['ref_id'] = !empty($id) ? $id : $ref_id;
	$data_insert13['add_owner_name'] = !empty($add_owner_name[$j]) ?  $add_owner_name[$j] :NULL;
	$data_insert13['add_owner_y'] = !empty($add_owner_y[$j]) ? $add_owner_y[$j]:NULL ;
	$data_insert13['add_owner_attach'] = !empty($add_owner_attach[$j]) ? $add_owner_attach[$j]:NULL;
	
	
					if(!empty($add_owner_id[$j])){
					$this->_updateapp1($id, $data_insert13,'rq_rqf_quick_quote_gh5c',$add_owner_id[$j],'add_owner_id');
					$add_owner_old_id1[]=$add_owner_id[$j];
	
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh5c', $data_insert13);
					$add_owner_new_id1[]=$this->db->insert_id();
				     }
					
					
				}
	$this->is_delete($add_owner_old_id1,$add_owner_new_id1,'rq_rqf_quick_quote_gh5c','add_owner_id',$id,$ghi5_parent_id);		
				
	
	$subcon_add_id= $this->input->post('subcon_add_id'); 
				   
	$subcon_add_name= $this->input->post('subcon_add_name');
	$subcon_add_vehicle= $this->input->post('subcon_add_vehicle');
	$subcon_add_y= $this->input->post('subcon_add_y');
	//$subcon_add_attach = $this->input->post('subcon_add_attach');
	$subcon_add_attach = !empty($_FILES['subcon_add_attach']) ? $this->new_quote_model->upload_files($file_types,'docs', $_FILES['subcon_add_attach']) : '';
	$subcon_old_id1=array();
	$subcon_new_id1=array();
	
      for ($k = 0; $k < count($subcon_add_name); $k++) 
				{
				 
    $data_insert17['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;
	$data_insert17['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id : NULL ;
    $data_insert17['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert17['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert17['subcon_add_name'] = !empty($subcon_add_name[$k])? $subcon_add_name[$k] :NULL;
	$data_insert17['subcon_add_vehicle'] = !empty($subcon_add_vehicle[$k])? $subcon_add_vehicle[$k] :NULL;
	$data_insert17['subcon_add_y'] = !empty($subcon_add_y[$k]) ? $subcon_add_y[$k] :NULL;
	$data_insert17['subcon_add_attach'] = !empty($subcon_add_attach[$k]) ? $subcon_add_attach[$k] :NULL ;
	
						
					
				if(!empty($subcon_add_id[$k])){
					$this->_updateapp1($id, $data_insert17,'rq_rqf_quick_quote_gh5d',$subcon_add_id[$k],'subcon_add_id');
					$subcon_old_id1[]=$subcon_add_id[$k];
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh5d', $data_insert17);
					$subcon_new_id1[]=$this->db->insert_id();
				        }
				
				}
				
	$this->is_delete($subcon_old_id1,$subcon_new_id1,'rq_rqf_quick_quote_gh5d','subcon_add_id',$id,$ghi5_parent_id);
	
	 
	$sub_haul_id= $this->input->post('sub_haul_id');

	$sub_haul_add_name= $this->input->post('sub_haul_add_name');
	$sub_haul_add_carrier= $this->input->post('sub_haul_add_carrier');
	$sub_haul_add_limit= $this->input->post('sub_haul_add_limit');
	$sub_haul_add_y = $this->input->post('sub_haul_add_y');
	//$sub_haul_add_attach= $this->input->post('sub_haul_add_attach');
	$sub_haul_add_attach = !empty($_FILES['sub_haul_add_attach']) ? $this->new_quote_model->upload_files($file_types,'docs', $_FILES['sub_haul_add_attach']) : '';
	
	$sub_haul_old_id1=array();	
	$sub_haul_new_id1=array();
      for ($l = 0; $l < count($sub_haul_add_name); $l++) 
				{
			
	$data_insert14['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;
	
	$data_insert14['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id : NULL ;
	
    $data_insert14['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert14['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert14['sub_haul_add_name'] = !empty($sub_haul_add_name[$l]) ? $sub_haul_add_name[$l] :NULL;
	$data_insert14['sub_haul_add_carrier'] = !empty($sub_haul_add_carrier[$l]) ? $sub_haul_add_carrier[$l]:NULL;
	$data_insert14['sub_haul_add_limit'] = !empty($sub_haul_add_limit[$l]) ? $sub_haul_add_limit[$l] :NULL;
	$data_insert14['sub_haul_add_y'] = !empty($sub_haul_add_y[$l]) ? $sub_haul_add_y[$l] :NULL;
	$data_insert14['sub_haul_add_attach'] = !empty($sub_haul_add_attach[$l]) ? $sub_haul_add_attach[$l] :NULL;
	
		if(!empty($sub_haul_id[$l])){
					$this->application_model->_updateapp1($id, $data_insert14,'rq_rqf_quick_quote_gh5e',$sub_haul_id[$l],'sub_haul_id');
					$sub_haul_old_id1[]=$sub_haul_id[$l];
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh5e', $data_insert14);
					$sub_haul_new_id1[]=$this->db->insert_id();
				}
						
			
			}
	$this->is_delete($sub_haul_old_id1,$sub_haul_new_id1,'rq_rqf_quick_quote_gh5e','sub_haul_id',$id,$ghi5_parent_id);			
				
	
	$add_mem_id= $this->input->post('add_mem_id');
				
	$add_mem_name= $this->input->post('add_mem_name');
	$add_mem_rel= $this->input->post('add_mem_rel');
    $add_mem_old_id1=array();
	$add_mem_new_id1=array();
	
		
      for ($m = 0; $m < count($add_mem_name); $m++) 
				{
			
	$data_insert15['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;	
    $data_insert15['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id :NULL ;	
    $data_insert15['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert15['ref_id'] = !empty($id) ? $id : $ref_id;	
	$data_insert15['add_mem_name'] = !empty($add_mem_name[$m]) ? $add_mem_name[$m]:NULL;
	$data_insert15['add_mem_rel'] = !empty($add_mem_rel[$m]) ? $add_mem_rel[$m] :NULL;
	
	
	
	
					if(!empty($add_mem_id[$m])){
					$this->_updateapp1($id, $data_insert15,'rq_rqf_quick_quote_gh5f',$add_mem_id[$m],'add_mem_id');
					$add_mem_old_id1[]=$add_mem_id[$m];
					}else{
					
					$this->db->insert('rq_rqf_quick_quote_gh5f', $data_insert15);
					$add_mem_new_id1[]=$this->db->insert_id();
				    }
				
					
				}
	$this->is_delete($add_mem_old_id1,$add_mem_new_id1,'rq_rqf_quick_quote_gh5f','add_mem_id',$id,$ghi5_parent_id);			
				
	$haul_id= $this->input->post('haul_list_id');
	$add_list_id= $this->input->post('add_list_id'); 
						
	$add_list_name= $this->input->post('add_list_name');
	$add_list_length= $this->input->post('add_list_length');
	$add_list_year= $this->input->post('add_list_year');
	$add_list_old_id1=array();
	$add_list_new_id1=array();
		
      for ($n = 0; $n < count($add_list_name); $n++) 
				{
				
	$data_insert16['haul_id'] =  !empty($hauled_n_id) ? $hauled_n_id : $insert_h_id ;	
    $data_insert16['ghi5_parent_id'] =  !empty($ghi5_parent_id) ? $ghi5_parent_id : NULL ;	
    $data_insert16['quote_id'] =  !empty($id) ? $id : $ref_id;	
	$data_insert16['ref_id'] = !empty($id) ? $id : $ref_id;		
	$data_insert16['add_list_name'] = !empty($add_list_name[$n]) ? $add_list_name[$n] : NULL;
	$data_insert16['add_list_length'] = !empty($add_list_length[$n]) ? $add_list_length[$n] :NULL;
	$data_insert16['add_list_year'] = !empty($add_list_year[$n]) ? $add_list_year[$n] :NULL ;
	
	
						if(!empty($add_list_id[$n])){
					    $this->_updateapp1($id, $data_insert16,'rq_rqf_quick_quote_gh5g',$add_list_id[$n],'add_list_id');
					    $add_list_old_id1[]=$add_list_id[$n];
					}else{
				      	$this->db->insert('rq_rqf_quick_quote_gh5g', $data_insert16);
				        $add_list_new_id1[]=$this->db->insert_id();
				          }				
					
				}
				
			$this->is_delete($add_list_old_id1,$add_list_new_id1,'rq_rqf_quick_quote_gh5g','add_list_id',$id,$ghi5_parent_id);	
				
		 }
public function is_delete($old_id='',$new_id='',$table='',$table_id='',$ref_id='',$condition=''){		 
if(!empty($old_id)){
	 $result=array();
	 $adresses=$this->get_id_inform1($ref_id,$table,$condition);
	   foreach($adresses as $values){
	
		   $result[]=$values->$table_id;
		
	   }
	$flag_set= array_diff($result,$old_id,$new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	
	if(isset($flag_set)){
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($ref_id, $insert_data130,$table,$vlaues,$table_id);
	
	     }
	    }
	   }
	  }
	}else{
	  
	   $result=array();
	   $array=$this->get_id_inform1($ref_id,$table,$condition);
	
	   foreach($array as $values){

		   $result[]=$values->$table_id;
		
	   }
	
	$flag_set= array_diff($result,$new_id);
 
	foreach ($flag_set as $Keys=>$vlaues) {
	if($vlaues!=''){
	
    $insert_data130['is_delete'] = 1;
	
	 
     if(!empty($vlaues)){
		
	  $this->_updateapp1($ref_id, $insert_data130,$table,$vlaues,$table_id);
	
	     }
	    }
	  
	  }
	
	
	}
}	
		public function getid($id='')
{
	$this->db->select('*');
	$this->db->from('rq_rqf_quick_quote');
	if($id!='')
	{
		$this->db->where('id', $id);
	}
	
	$this->db->order_by('id', 'desc');
	$this->db->limit('1');
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
}

			public function _updateapp($id, $data, $table)
	{
	
	
		$this->db->where('quote_id', $id);
	
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
	public function get_id_inform($id='',$table,$condition=''){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	      {
		    $this->db->where('quote_id', $id);
     	  }
		   if($condition!='')
	      {
		    $this->db->where('vehicle_type', $condition);
     	  }
	
	       $query = $this->db->get();
   
	       $result = $query->result();
	
	     return $result;
		
		}	
		public function _updateapp1($id, $data, $table,$id1,$table_id)
	{
	
	
		$this->db->where('quote_id', $id);
		if($table_id!=''){
		$this->db->where($table_id, $id1);
	
			}	
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	} 
 	public function get_id_inform1($id='',$table,$condition=''){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	      {
		    $this->db->where('quote_id', $id);
     	  }
		   if($condition!='')
	      {
		    $this->db->where('ghi5_parent_id', $condition);
     	  }
	
	       $query = $this->db->get();
   
	       $result = $query->result();
	
	     return $result;
		
		}
}
?>
