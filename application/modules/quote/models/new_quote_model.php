<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class New_quote_model extends CI_Model {

    public function &__get($key) {
        $CI = & get_instance();
        return $CI->$key;
        $this->load->database();
		$quote_id=NULL;
		$this->load->model('application_form/application_model');
    }
	
	/**
	* Upload multiple files at time using CI
	* @$images return uploaded files name
	* @$errors return errors during file uploading
	*/
	public function upload_files($file_types='*',$path, $files){
        $config = array(
            'upload_path'   => 'uploads/'.$path,
            'allowed_types' => $file_types,
            'overwrite'     => 1,                       
        );
		
	
        $this->load->library('upload', $config);
        $images = array();       
        foreach ($files['name'] as $key => $image) {
            $_FILES['files[]']['name']= $files['name'][$key];
            $_FILES['files[]']['type']= $files['type'][$key];
            $_FILES['files[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['files[]']['error']= $files['error'][$key];
            $_FILES['files[]']['size']= $files['size'][$key];
            $fileName = preg_replace('/[^a-zA-Z0-9\/_|+ .-]/', '-', $image);
            $images[] = str_replace(' ', '_', $fileName);
            $config['file_name'] = $fileName;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('files[]')) {
                $data = $this->upload->data();
            } else {
               $errors = $this->upload->display_errors();
            }
        }
		
		
        return $images;
    }
	
	
	private function upload_files_multiple($file_types='*',$path, $files){
        $config = array(
            'upload_path'   => 'uploads/'.$path,
            'allowed_types' => $file_types,
            'overwrite'     => 1,                       
        );
	
	
        $this->load->library('upload', $config);
        $images = array();  
		     
        foreach ($files['name'] as $key=>$img) {			
		   foreach ($img as $keys=>$image) {
			   
            $_FILES['files[]']['name']= $files['name'][$key][$keys];
            $_FILES['files[]']['type']= $files['type'][$key][$keys];
            $_FILES['files[]']['tmp_name']= $files['tmp_name'][$key][$keys];
            $_FILES['files[]']['error']= $files['error'][$key][$keys];
            $_FILES['files[]']['size']= $files['size'][$key][$keys];
            $fileName = preg_replace('/[^a-zA-Z0-9\/_|+ .-]/', '-', $image);
            $images[][] = str_replace(' ', '_', $fileName);
            $config['file_name'] = $fileName;
			
            $this->upload->initialize($config);
            if ($this->upload->do_upload('files[]')) {
                $data = $this->upload->data();
            } else {
               $errors = $this->upload->display_errors();
            }
		  }
		}
        return $images;
    }
	
   /**
	* Upload file using CI
	* @$data return uploaded file name
	* @$error return errors during file uploading
	*/
	public function do_upload($file_types='*',$name, $path, $file_name='') {
        $config['upload_path'] = 'uploads/'.$path;
        $config['allowed_types'] = $file_types;
    
		if($file_name){
			$config['file_name'] = $file_name;
		}
      

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($name)) {
            $error = array( 'error' => $this->upload->display_errors() );
            
            return $error;
        } else {
            $data = array( 'upload_data' => $this->upload->data() );
            return $data['upload_data'];
           
        }
    }	
	public function insert_broker_quote($id='',$saved=''){
		        
				$phone_old_id=array();
				$phone_new_id=array();
				$email_new_id=array();
				$fax_new_id=array();
				$email_old_id=array();
				$fax_old_id=array();
				
				$quote_id=!empty($id)?$id:$this->get_quote_id();
	
				
		        $post = $this->input->post();	
						
		        $result['fistname'] = trim($this->input->post("bro_fistname"));
				$result['middlename'] = trim($this->input->post("bro_middlename"));
				$result['lastname'] = trim($this->input->post("bro_lastname"));
				$result['dba'] = trim($this->input->post("bro_dba"));
				$result['street'] = trim($this->input->post("bro_Street"));
				$result['city'] = trim($this->input->post("bro_city"));
				$result['state'] = str_replace('?','',str_replace('? string:','',$this->input->post("bro_state")));
				$result['county'] = trim($this->input->post("bro_county"));					
				$result['zip'] = trim($this->input->post("bro_zip"));
				$result['country'] =str_replace('?','',str_replace('? string:','',$this->input->post("bro_country"))); 
				$result['fax']= trim($this->input->post("in_broker_fax"));
				$result['special_remark']= trim($this->input->post("bro_special_remark"));
				$result['status']= !empty($saved)?'Draft':NULL;
				$requester_id=$this->input->post('requester_id');
				$result['requester_id']=!empty($requester_id)?$requester_id:$this->session->userdata('member_id');
			
				$email_u =$this->input->post("email_u");
				$where_array = array('rq_members.email' => $email_u);
				$this->db->select('rq_members.underwriter');
				$this->db->from('rq_members');
				$this->db->where($where_array);

				$query = $this->db->get();
				$underwriter = $query->result();
			
				if(isset($underwriter[0]->underwriter)){
							$keys = str_replace(':',',',$underwriter[0]->underwriter);
							$result['assigned_underwriter'] = $keys;
						
				}
			
				if(!empty($id)){
						$this->db->where('id',$id);				
						$this->db->update('rq_rqf_quick_quote',$result);
					}else{
						$this->db->insert('rq_rqf_quick_quote', $result);
					echo $quote_id=$this->db->insert_id();
					}
					
					
		if (is_array($this->input->post('bro_phone_type'))){
			$i = 0;
			 foreach($this->input->post('bro_phone_type') as $bro_phone_type){
				$result_1['quote_id'] =!empty($id)?$id:$quote_id;
				$result_1['phone_type'] = isset($post['bro_phone_type'][$i]) ? $post['bro_phone_type'][$i] : NULL;
				$result_1['code_type'] = isset($post['bro_code_type'][$i]) ? $post['bro_code_type'][$i] : NULL;
				$result_1['area_code'] = isset($post['bro_area_code'][$i]) ? $post['bro_area_code'][$i] : NULL;				
				$result_1['prefix'] = isset($post['bro_prefix'][$i]) ? $post['bro_prefix'][$i] : NULL;
				$result_1['suffix'] = isset($post['bro_suffix'][$i]) ? $post['bro_suffix'][$i] : NULL;
				$result_1['extension'] = isset($post['bro_extension'][$i]) ? $post['bro_extension'][$i] : NULL;
				$result_1['pri_country'] = isset($post['bro_pri_country'][$i]) ? $post['bro_pri_country'][$i] : NULL;
				$result_1['type'] = "broker";				
				$phone_id=$this->input->post('phone_id');			
			
				if(!empty($phone_id[$i])){
						$this->db->where('phone_id',$phone_id[$i]);
						$this->db->update('rq_rqf_quick_quote_phone',$result_1);
						$phone_old_id[]=$phone_id[$i];
					}else{
						$this->db->insert('rq_rqf_quick_quote_phone', $result_1);
						$phone_new_id[]=$this->db->insert_id();
					}
				$result_bro = array(
				'agency_id' => $this->input->post('agency_id'),
				'first_name' => $this->input->post('bro_fistname'),
				'middle_name'=>$this->input->post('bro_middlename'),
				'last_name'=>$this->input->post('bro_lastname'),
				'dba_name'=>$this->input->post('bro_dba'),
				'address' => $this->input->post('bro_Street'),
				'city' => $this->input->post('bro_city'),
				'state' => $this->input->post('bro_state'),
				'country' => $this->input->post('bro_country'),
				'zip_code' => $this->input->post('bro_zip'),			
			   );
			   if($i>0){
			   $result_b1['area_code'] = isset($post['bro_area_code'][$i]) ? $post['bro_area_code'][$i] : NULL;
			   $result_b1['prefix'] = isset($post['bro_prefix'][$i]) ? $post['bro_prefix'][$i] : NULL;
			   $result_b1['suffix'] = isset($post['bro_suffix'][$i]) ? $post['bro_suffix'][$i] : NULL;
			   $result_b1['extension'] = isset($post['bro_extension'][$i]) ? $post['bro_extension'][$i] : NULL;
			   $result_b[]=$result_b1['area_code'].'-'.$result_b1['prefix'].'-'.$result_b1['prefix'].'-'.$result_b1['extension'];	
				
					}
					$i++;
			 }
			 
			 $result_bro['add_phone_number']=!empty($result_b)?implode(',',$result_b):'';
			
			 $bro_id=$this->session->userdata('member_id');			
			 	if(!empty($bro_id)){
						$this->db->where('id',$bro_id);
						$this->db->update('rq_members',$result_bro);
						
				}
			 $this->is_delete($phone_old_id,$phone_new_id,'rq_rqf_quick_quote_phone','phone_id',$quote_id,'broker');
		}
		
		if (is_array($this->input->post('bro_email_type'))){
			$j = 0;
			 foreach($this->input->post('bro_email_type') as $bro_email_type){
				$result_2['quote_id'] =!empty($id)?$id:$quote_id;
				$result_2['email_type'] = isset($post['bro_email_type'][$j]) ? $post['bro_email_type'][$j] : NULL;
				$result_2['email'] = isset($post['bro_email'][$j]) ? $post['bro_email'][$j] : NULL;
				$result_2['priority_email'] = isset($post['bro_priority_email'][$j]) ? $post['bro_priority_email'][$j] : NULL;
				$result_2['type'] = "broker";
				$email_id=$this->input->post('email_id');
				if(!empty($email_id[$j])){
						$this->db->where('email_id',$email_id[$j]);
						$this->db->update('rq_rqf_quick_quote_email',$result_2);
						$email_old_id[]=$email_id[$j];
					}else{
						$this->db->insert('rq_rqf_quick_quote_email', $result_2);
						$email_new_id[]=$this->db->insert_id();
					}
					$j++;
			 }
			  $this->is_delete($email_old_id,$email_new_id,'rq_rqf_quick_quote_email','email_id',$quote_id,'broker');
		}
				
		 if (is_array($this->input->post('bro_fax_type'))){
			$k = 0;
			 foreach($this->input->post('bro_fax_type') as $bro_fax_type){
				$result_3['quote_id'] =!empty($id)?$id:$quote_id;
				$result_3['fax_type'] = isset($post['bro_fax_type'][$k]) ? $post['bro_fax_type'][$k] : NULL;
				$result_3['code_type_fax'] = isset($post['bro_code_type_fax'][$k]) ? $post['bro_code_type_fax'][$k] : NULL;
				$result_3['area_code_fax'] = isset($post['bro_area_code_fax'][$k]) ? $post['bro_area_code_fax'][$k] : NULL;
				$result_3['prefix_fax'] = isset($post['bro_prefix_fax'][$k]) ? $post['bro_prefix_fax'][$k] : NULL;
				$result_3['suffix_fax'] = isset($post['bro_suffix_fax'][$k]) ? $post['bro_suffix_fax'][$k] : NULL;
				$result_3['country_fax'] = isset($post['bro_country_fax'][$k]) ? $post['bro_country_fax'][$k] : NULL;
				$result_3['type'] = "broker";
				$fax_id=$this->input->post('fax_id');				
				if(!empty($fax_id[$k])){
						$this->db->where('fax_id',$fax_id[$k]);
						$this->db->update('rq_rqf_quick_quote_fax',$result_3);
						$fax_old_id[]=$fax_id[$k];
					}else{
						$this->db->insert('rq_rqf_quick_quote_fax', $result_3);
						$fax_new_id[]=$this->db->insert_id();
					}
				$k++;
			 }
		 $this->is_delete($fax_old_id,$fax_new_id,'rq_rqf_quick_quote_fax','fax_id',$quote_id,'broker');
		}
		if($this->session->userdata('underwriter_id')){
		if(!empty($id)){
			  $version=$this->input->post('version');
			  $ver['quote_id']=$id;
			  $ver['version']=$version+1;
			  $ver['status']='Released';
			  $ver_id=$this->input->post('ver_id');
			  if(!empty($ver_id)){
				        $data_ver['status']='in_active';
						$this->db->where('ver_id',$ver_id);				
						$this->db->update('rq_rqf_quick_quote_ver',$data_ver);
						
					}
					
			$this->db->insert('rq_rqf_quick_quote_ver', $ver);	
			$result_s['status']='Released';
			$this->db->where('id',$id);				
			$this->db->update('rq_rqf_quick_quote',$result_s);					
					
			}
		}
	}
	
	
		public function insert_person_quote($id=''){	
				$phone_old_id1=array();
				$phone_new_id1=array();
				$email_new_id1=array();
				$fax_new_id1=array();
				$email_old_id1=array();
				$fax_old_id1=array();
						
				$post = $this->input->post();
				$quote_id=!empty($id) ? $id: $this->get_quote_id();
				
				$person_id="person_id";
				$result_p['quote_id'] =$quote_id;	 				
		        $result_p['fistname'] = trim($this->input->post("per_fistname", true));
				$result_p['middlename'] = trim($this->input->post("per_middlename", true));
				$result_p['lastname'] = trim($this->input->post("per_lastname", true));				
				$result_p['street'] = trim($this->input->post("per_Street", true));
				$result_p['city'] = trim($this->input->post("per_city", true));
				$result_p['state'] = trim($this->input->post("per_state", true));
				$result_p['county'] = trim($this->input->post("per_county", true));
				$result_p['zip'] = trim($this->input->post("per_zip", true));
				$result_p['country'] = trim($this->input->post("per_pri_country", true));
				$result_p['fax']= trim($this->input->post("in_per_fax", true));
				$result_p['special_remark']= trim($this->input->post("per_special_remark", true));
				$result_p['info_submitted_broker']= trim($this->input->post("infosubmitted", true));
				$result_p['assigned_agency']= $this->session->userdata('member_id');
				
				$person_id = $this->input->post("person_id");
				if(!empty($person_id)){
						$this->db->where('person_id',$person_id);
						$this->db->update('rq_rqf_quick_quote_person',$result_p);
					}else{
						$this->db->insert('rq_rqf_quick_quote_person', $result_p);
						
						
					}
					
		if (is_array($this->input->post('per_phone_type'))){
			
		 $i = 0;
			 foreach($this->input->post('per_phone_type') as $per_phone_type){			
				$result_p_1['quote_id'] =$quote_id;
				$result_p_1['phone_type'] = isset($post['per_phone_type'][$i]) ? $post['per_phone_type'][$i] : NULL;
				$result_p_1['code_type'] = isset($post['per_code_type'][$i]) ? $post['per_code_type'][$i] : NULL;
				$result_p_1['area_code'] = isset($post['per_area_code'][$i]) ? $post['per_area_code'][$i] : NULL;
				$result_p_1['prefix'] = isset($post['per_prefix'][$i]) ? $post['per_prefix'][$i] : NULL;
				$result_p_1['suffix'] = isset($post['per_suffix'][$i]) ? $post['per_suffix'][$i] : NULL;
				$result_p_1['extension'] = isset($post['per_extension'][$i]) ? $post['per_extension'][$i] : NULL;
				$result_p_1['pri_country'] = isset($post['per_country'][$i]) ? $post['per_country'][$i] : NULL;
				$result_p_1['type'] = "person";
				
				$phone_id=$this->input->post('per_phone_id');
				
				
				if(!empty($phone_id[$i])){
						$this->db->where('phone_id',$phone_id[$i]);
						$this->db->update('rq_rqf_quick_quote_phone',$result_p_1);
						$phone_old_id1[]=$phone_id[$i];
					}else{
						$this->db->insert('rq_rqf_quick_quote_phone', $result_p_1);
						$phone_new_id1[]=$this->db->insert_id();
					}
					$i++;
			 }
			 $this->is_delete($phone_old_id1,$phone_new_id1,'rq_rqf_quick_quote_phone','phone_id',$quote_id,'person');
		}
		
		if (is_array($this->input->post('per_email_type'))){
			$j = 0;
			 foreach($this->input->post('per_email_type') as $per_email_type){
				$result_p_2['quote_id'] =$quote_id;
				$result_p_2['email_type'] = isset($post['per_email_type'][$j]) ? $post['per_email_type'][$j] : NULL;
				$result_p_2['email'] = isset($post['per_email'][$j]) ? $post['per_email'][$j] : NULL;
				$result_p_2['priority_email'] = isset($post['per_priority_email'][$j]) ? $post['per_priority_email'][$j] : NULL;
				$result_p_2['type'] = "person";
				
				$email_id=$this->input->post('per_email_id');
				
				if(!empty($email_id[$j])){
						$this->db->where('email_id',$email_id[$j]);
						$this->db->update('rq_rqf_quick_quote_email',$result_p_2);	
						$email_old_id1[]=$email_id[$j];					
					}else{
						$this->db->insert('rq_rqf_quick_quote_email', $result_p_2);
						$email_new_id1[]=$this->db->insert_id();
					}
					$j++;
			 }
			 $this->is_delete($email_old_id1,$email_new_id1,'rq_rqf_quick_quote_email','email_id',$quote_id,'person');
		}
				
		 if (is_array($this->input->post('per_fax_type'))){
			$k = 0;
			 foreach($this->input->post('per_fax_type') as $per_fax_type){
				$result_p_3['quote_id'] =$quote_id;
				$result_p_3['fax_type'] = isset($post['per_fax_type'][$k]) ? $post['per_fax_type'][$k] : NULL;
				$result_p_3['code_type_fax'] = isset($post['per_code_type_fax'][$k]) ? $post['per_code_type_fax'][$k] : NULL;
				$result_p_3['area_code_fax'] = isset($post['per_area_code_fax'][$k]) ? $post['per_area_code_fax'][$k] : NULL;
				$result_p_3['prefix_fax'] = isset($post['per_prefix_fax'][$k]) ? $post['per_prefix_fax'][$k] : NULL;
				$result_p_3['suffix_fax'] = isset($post['per_suffix_fax'][$k]) ? $post['per_suffix_fax'][$k] : NULL;
				$result_p_3['country_fax'] = isset($post['person_country_fax'][$k]) ? $post['person_country_fax'][$k] : NULL;
				$result_p_3['type'] = "person";
				
				$fax_id=$this->input->post('per_fax_id');
				
				if(!empty($fax_id[$k])){
						$this->db->where('fax_id',$fax_id[$k]);
						$this->db->update('rq_rqf_quick_quote_fax',$result_p_3);
						$fax_old_id1[]=$fax_id[$k];
					}else{
						$this->db->insert('rq_rqf_quick_quote_fax', $result_p_3);
						$fax_new_id1[]=$this->db->insert_id();
					}
					$k++;
			 }
			 $this->is_delete($fax_old_id1,$fax_new_id1,'rq_rqf_quick_quote_fax','fax_id',$quote_id,'person');
		}
		
	}
	
	public function insert_insured_quote($id=''){	
				$phone_old_id2=array();
				$phone_new_id2=array();
				$email_new_id2=array();
				$fax_new_id2=array();
				$email_old_id2=array();
				$fax_old_id2=array();
				
				$phone_old_id3=array();
				$phone_new_id3=array();
				$email_new_id3=array();
				$fax_new_id3=array();
				$email_old_id3=array();
				$fax_old_id3=array();
				
				$in_ga_old_id=array();
				$in_ga_new_id=array();	
				
				$filing_new_id=array();  
				$filing_old_id=array(); 
				
				$fil_new_id =array();		
				$fil_old_id =array();
	     		
				$post = $this->input->post();	
				$quote_id=!empty($id) ? $id: $this->get_quote_id(); 
				$result_i['quote_id']=$quote_id;				
		        $result_i['in_this_is'] = trim($this->input->post("in_this_is", true));
				$result_i['insured_name'] = trim($this->input->post("insured_name", true));
				$result_i['insured_middle_name'] = trim($this->input->post("insured_middle_name", true));				
				$result_i['insured_last_name'] = trim($this->input->post("insured_last_name", true));
				$result_i['dba'] = trim($this->input->post("dba", true));
				$result_i['in_address'] = trim($this->input->post("in_address", true));
				$result_i['in_city'] = trim($this->input->post("in_city", true));
				$result_i['in_state'] = trim($this->input->post("in_state", true));
				$result_i['in_county'] = trim($this->input->post("in_county", true));
				$result_i['in_zip']= trim($this->input->post("in_zip", true));
				$result_i['in_country']= trim($this->input->post("in_country", true));
				$result_i['in_garag_in']= trim($this->input->post("in_garag_in", true));
				$result_i['in_garag_fax']= $this->input->post("in_garag_faxs");
				$nature_of_business= $this->input->post("nature_of_business");
				$result_i['nature_of_business']=!empty($nature_of_business) ? implode(';',$nature_of_business) :'';
				$result_i['nature_of_business_other']= trim($this->input->post("nature_of_business_other", true));
				$commodities_haulted= $this->input->post("commodities_haulted");
				$result_i['commodities_haulted']=!empty($commodities_haulted) ? implode(',',$commodities_haulted) :'';				
				$result_i['commodities_haulted_other']= trim($this->input->post("commodities_haulted_other", true));
				$result_i['business_years']=trim($this->input->post("business_years", true));
				$result_i['operation']=$this->input->post("operation");
				$result_i['terms_name']=trim($this->input->post("terms_name", true));
				$result_i['terms_other']=trim($this->input->post("terms_other", true));
				$result_i['terms_from']=trim($this->input->post("terms_from", true));
				$result_i['terms_to']=trim($this->input->post("terms_to", true));
				$result_i['filings_need']=trim($this->input->post("filings_need", true));
				$insured_id=$this->input->post("insured_id");
				
			  if(!empty($result_i['commodities_haulted_other'])){
				$res['catlog_type']=1;
				$res['catlog_value']=trim($this->input->post("commodities_haulted_other", true));
				$res['status']=0;
				$this->db->insert('rq_rqf_quote_catlogs', $res);
				}
				if(!empty($insured_id)){
						$this->db->where('insured_id',$insured_id);
						$this->db->update('rq_rqf_quick_quote_insured',$result_i);
						
					}else{
						$this->db->insert('rq_rqf_quick_quote_insured', $result_i);
						
						
					}
					
		if (is_array($this->input->post('insured_phone_type'))){
			
		 $i = 0;
			 foreach($this->input->post('insured_phone_type') as $insured_phone_type){			
				$result_i_1['quote_id'] =$quote_id;
				$result_i_1['phone_type'] = isset($post['insured_phone_type'][$i]) ? $post['insured_phone_type'][$i] : NULL;
				$result_i_1['code_type'] = isset($post['insured_code_type'][$i]) ? $post['insured_code_type'][$i] : NULL;
				$result_i_1['area_code'] = isset($post['insured_area_code'][$i]) ? $post['insured_area_code'][$i] : NULL;
				$result_i_1['prefix'] = isset($post['insured_prefix'][$i]) ? $post['insured_prefix'][$i] : NULL;
				$result_i_1['suffix'] = isset($post['insured_suffix'][$i]) ? $post['insured_suffix'][$i] : NULL;
				$result_i_1['extension'] = isset($post['insured_extension'][$i]) ? $post['insured_extension'][$i] : NULL;				
				$result_i_1['pri_country'] = isset($post['insured_country'][$i]) ? $post['insured_country'][$i] : NULL;
				$result_i_1['type'] = "insured";
				
				$phone_id=$this->input->post('insured_phone_id');
				
				
				if(!empty($phone_id[$i])){
						$this->db->where('phone_id',$phone_id[$i]);
						$this->db->update('rq_rqf_quick_quote_phone',$result_i_1);
						$phone_old_id2[]=$phone_id[$i];
					}else{
						$this->db->insert('rq_rqf_quick_quote_phone', $result_i_1);
					    $phone_new_id2[]=$this->db->insert_id();
					}
					$i++;
			 }
			 $this->is_delete($phone_old_id2,$phone_new_id2,'rq_rqf_quick_quote_phone','phone_id',$quote_id,'insured');
		}
		
		if (is_array($this->input->post('insured_email_type'))){
			$j = 0;
			 foreach($this->input->post('insured_email_type') as $insured_email_type){
				$result_i_2['quote_id'] =$quote_id;
				$result_i_2['email_type'] = isset($post['insured_email_type'][$j]) ? $post['insured_email_type'][$j] : NULL;
				$result_i_2['email'] = isset($post['insured_email'][$j]) ? $post['insured_email'][$j] : NULL;
				$result_i_2['priority_email'] = isset($post['insured_priority_email'][$j]) ? $post['insured_priority_email'][$j] : NULL;
				$result_i_2['type'] = "insured";
				$email_id=$this->input->post('insured_email_id');
				
				if(!empty($email_id[$j])){
						$this->db->where('email_id',$email_id[$j]);
						$this->db->update('rq_rqf_quick_quote_email',$result_i_2);
						$email_old_id2[]=$email_id[$j];
					}else{
						$this->db->insert('rq_rqf_quick_quote_email', $result_i_2);
						$email_new_id2[]=$this->db->insert_id();
					}
					$j++;
				
			 }
			 $this->is_delete($email_old_id2,$email_new_id2,'rq_rqf_quick_quote_email','email_id',$quote_id,'insured');
		}
				
		 if (is_array($this->input->post('insured_fax_type'))){
			$k = 0;
			 foreach($this->input->post('insured_fax_type') as $insured_fax_type){
				$result_i_3['quote_id'] =$quote_id;
				$result_i_3['fax_type'] = isset($post['insured_fax_type'][$k]) ? $post['insured_fax_type'][$k] : NULL;
				$result_i_3['code_type_fax'] = isset($post['insured_code_type_fax'][$k]) ? $post['insured_code_type_fax'][$k] : NULL;
				$result_i_3['area_code_fax'] = isset($post['insured_area_code_fax'][$k]) ? $post['insured_area_code_fax'][$k] : NULL;
				$result_i_3['prefix_fax'] = isset($post['insured_prefix_fax'][$k]) ? $post['insured_prefix_fax'][$k] : NULL;
				$result_i_3['suffix_fax'] = isset($post['insured_suffix_fax'][$k]) ? $post['insured_suffix_fax'][$k] : NULL;
				$result_i_3['country_fax'] = isset($post['insured_country_fax'][$k]) ? $post['insured_country_fax'][$k] : NULL;
				$result_i_3['type'] = "insured";
				$fax_id=$this->input->post('insured_fax_id');
				
				if(!empty($fax_id[$k])){
						$this->db->where('fax_id',$fax_id[$k]);
						$this->db->update('rq_rqf_quick_quote_fax',$result_i_3);
						$fax_old_id2[]=$fax_id[$k];
					}else{
						$this->db->insert('rq_rqf_quick_quote_fax', $result_i_3);
						$fax_new_id2[]=$this->db->insert_id();
					}
					$k++;
			 }
			$this->is_delete($fax_old_id2,$fax_new_id2,'rq_rqf_quick_quote_fax','fax_id',$quote_id,'insured');
		}
		
	
		
		if (is_array($this->input->post('in_ga_sequence'))){			
		 $i = 0;
			 foreach($this->input->post('in_ga_sequence') as $in_ga_sequence){			
				$result_ga['quote_id'] =$quote_id;
				$result_ga['in_ga_fname'] = isset($post['in_ga_fname'][$i]) ? $post['in_ga_fname'][$i] : NULL;
				$result_ga['in_ga_mname'] = isset($post['in_ga_mname'][$i]) ? $post['in_ga_mname'][$i] : NULL;
				$result_ga['in_ga_lname'] = isset($post['in_ga_lname'][$i]) ? $post['in_ga_lname'][$i] : NULL;
				$result_ga['in_ga_sequence'] = isset($post['in_ga_sequence'][$i]) ? $post['in_ga_sequence'][$i] : NULL;
				$result_ga['in_ga_address'] = isset($post['in_ga_address'][$i]) ? $post['in_ga_address'][$i] : NULL;
				$result_ga['in_ga_city'] = isset($post['in_ga_city'][$i]) ? $post['in_ga_city'][$i] : NULL;
				$result_ga['in_ga_state'] = isset($post['in_ga_state'][$i]) ? $post['in_ga_state'][$i] : NULL;
				$result_ga['in_ga_county'] = isset($post['in_ga_county'][$i]) ? $post['in_ga_county'][$i] : NULL;
				$result_ga['in_ga_zip'] = isset($post['in_ga_zip'][$i]) ? $post['in_ga_zip'][$i] : NULL;
				$result_ga['in_ga_country'] = isset($post['in_ga_country'][$i]) ? $post['in_ga_country'][$i] : NULL;
				$result_ga['in_garag_fax']=isset($post['in_garag_fax'][$i]) ? $post['in_garag_fax'][$i] : NULL;
				$result_ga['type']=isset($post['vh_garage_type'][$i]) ? $post['vh_garage_type'][$i] : NULL;
							
				$in_ga_id=$this->input->post('in_ga_id');
				
				
				if(!empty($in_ga_id[$i])){
						$this->db->where('in_ga_id',$in_ga_id[$i]);
						$this->db->update('rq_rqf_quick_quote_garage',$result_ga);
						$in_ga_old_id[]=$in_ga_id[$i];
					}else{
						$this->db->insert('rq_rqf_quick_quote_garage', $result_ga);
						$parent_id=	$this->db->insert_id();
						$in_ga_new_id[]=$this->db->insert_id();
					}
		if (is_array($this->input->post('ins_phone_type_'.$i.''))){
			
		 $u = 0;
			 foreach($this->input->post('ins_phone_type_'.$i.'') as $insured_phone_type){			
				$result_ga_1['quote_id'] =$quote_id;
				$result_ga_1['phone_type'] = isset($post['ins_phone_type_'.$i.''][$u]) ? $post['ins_phone_type_'.$i.''][$u] : NULL;
				$result_ga_1['code_type'] = isset($post['ins_code_type_'.$i.''][$u]) ? $post['ins_code_type_'.$i.''][$u] : NULL;
				$result_ga_1['area_code'] = isset($post['ins_area_code_'.$i.''][$u]) ? $post['ins_area_code_'.$i.''][$u] : NULL;
				$result_ga_1['prefix'] = isset($post['ins_prefix_'.$i.''][$u]) ? $post['ins_prefix_'.$i.''][$u] : NULL;
				$result_ga_1['suffix'] = isset($post['ins_suffix_'.$i.''][$u]) ? $post['ins_suffix_'.$i.''][$u] : NULL;
				$result_ga_1['extension'] = isset($post['ins_extension_'.$i.''][$u]) ? $post['ins_extension_'.$i.''][$u] : NULL;
				$result_ga_1['pri_country'] = isset($post['ins_country_'.$i.''][$u]) ? $post['ins_country_'.$i.''][$u] : NULL;
				$result_ga_1['type'] = "ins";
				$result_ga_1['parent_id'] = !empty($post['in_ga_id'][$i]) ? $post['in_ga_id'][$i] : $parent_id; 
				
			
				$phone_id=$this->input->post('ins_phone_id_'.$i.'');				
				
				if(!empty($phone_id[$u])){
						$this->db->where('phone_id',$phone_id[$u]);						
						$this->db->update('rq_rqf_quick_quote_phone',$result_ga_1);
						$phone_old_id3[]=$phone_id[$u];
					}else{
						$this->db->insert('rq_rqf_quick_quote_phone', $result_ga_1);
						$phone_new_id3[]=$this->db->insert_id();
					}
					$u++;
			 }
			 $this->is_delete($phone_old_id3,$phone_new_id3,'rq_rqf_quick_quote_phone','phone_id',$quote_id,'ins');
		}
		
		if (is_array($this->input->post('ins_email_type_'.$i.''))){
			$j = 0;
			 foreach($this->input->post('ins_email_type_'.$i.'') as $ins_email_type){
				$result_ga_2['quote_id'] =$quote_id;
				$result_ga_2['email_type'] = isset($post['ins_email_type_'.$i.''][$j]) ? $post['ins_email_type_'.$i.''][$j] : NULL;
				$result_ga_2['email'] = isset($post['ins_email_'.$i.''][$j]) ? $post['ins_email_'.$i.''][$j] : NULL;
				$result_ga_2['priority_email'] = isset($post['ins_priority_email_'.$i.''][$j]) ? $post['ins_priority_email_'.$i.''][$j] : NULL;
				$result_ga_2['parent_id'] =  !empty($post['in_ga_id'][$i]) ? $post['in_ga_id'][$i] : $parent_id; 
				$result_ga_2['type'] = "ins";
				
				$email_id=$this->input->post('ins_email_id_'.$i.'');
				
				if(!empty($email_id[$j])){
						$this->db->where('email_id',$email_id[$j]);						
						$this->db->update('rq_rqf_quick_quote_email',$result_ga_2);
						$email_old_id3[]=$email_id[$j];
					}else{
						$this->db->insert('rq_rqf_quick_quote_email', $result_ga_2);
						$email_new_id3[]=$this->db->insert_id();
					}
					$j++;
			 }
			$this->is_delete($email_old_id3,$email_new_id3,'rq_rqf_quick_quote_email','email_id',$quote_id,'ins');
		}
				
		 if (is_array($this->input->post('ins_fax_type_'.$i.''))){
			$k = 0;
			 foreach($this->input->post('ins_fax_type_'.$i.'') as $ins_fax_type){
				$result_ga_3['quote_id'] =$quote_id;
				$result_ga_3['fax_type'] = isset($post['ins_fax_type_'.$i.''][$k]) ? $post['ins_fax_type_'.$i.''][$k] : NULL;
				$result_ga_3['code_type_fax'] = isset($post['ins_code_type_fax_'.$i.''][$k]) ? $post['ins_code_type_fax_'.$i.''][$k] : NULL;
				$result_ga_3['area_code_fax'] = isset($post['ins_area_code_fax_'.$i.''][$k]) ? $post['ins_area_code_fax_'.$i.''][$k] : NULL;
				$result_ga_3['prefix_fax'] = isset($post['ins_prefix_fax_'.$i.''][$k]) ? $post['ins_prefix_fax_'.$i.''][$k] : NULL;
				$result_ga_3['suffix_fax'] = isset($post['ins_suffix_fax_'.$i.''][$k]) ? $post['ins_suffix_fax_'.$i.''][$k] : NULL;
				$result_ga_3['country_fax'] = isset($post['ins_country_fax_'.$i.''][$k]) ? $post['ins_country_fax_'.$i.''][$k] : NULL;
				$result_ga_3['parent_id'] =  !empty($post['in_ga_id'][$i]) ? $post['in_ga_id'][$i] : $parent_id; 
				$result_ga_3['type'] = "ins";
				
				$fax_id=$this->input->post('ins_fax_id');
				
				if(!empty($fax_id[$k])){
						$this->db->where('fax_id',$fax_id[$k]);				
						$this->db->update('rq_rqf_quick_quote_fax',$result_ga_3);
						$fax_old_id3[]=$fax_id[$k];
					}else{
						$this->db->insert('rq_rqf_quick_quote_fax', $result_ga_3);
						$fax_new_id3[]=$this->db->insert_id();
					}
					$k++;
			 }
			 $this->is_delete($fax_old_id3,$fax_new_id3,'rq_rqf_quick_quote_fax','fax_id',$quote_id,'ins');
		 }
	   $i++;
	  }
	    
		$this->is_delete($in_ga_old_id,$in_ga_new_id,'rq_rqf_quick_quote_garage','in_ga_id',$quote_id);
	}
		

	   if (is_array($this->input->post('filing_type'))){
		  $k = 0;
		   foreach($this->input->post('filing_type') as $filing_type){
			  $result_f['quote_id'] =$quote_id;
			  $result_f['filing_type'] = isset($post['filing_type'][$k]) ? $post['filing_type'][$k] : NULL;
			  $result_f['filing_numbers'] = isset($post['filing_numbers'][$k]) ? $post['filing_numbers'][$k] : NULL;
			  $result_f['filing_other'] = isset($post['filing_type_other'][$k]) ? $post['filing_type_other'][$k] : NULL;
			  $result_f['filing_remark'] = isset($post['filing_remark'][$k]) ? $post['filing_remark'][$k] : NULL;
			
			  $filing_id=$this->input->post('filing_id');
			  
			  if(!empty($filing_id[$k])){
					  $this->db->where('filing_id',$filing_id[$k]);
					  $this->db->update('rq_rqf_quick_quote_filing',$result_f);
					  $filing_old_id[]=$filing_id[$k];
				  }else{
					  $this->db->insert('rq_rqf_quick_quote_filing', $result_f);
					  $filing_new_id[]=$this->db->insert_id();
				  }
				  $k++;
		   }
		   $this->is_delete($filing_old_id,$filing_new_id,'rq_rqf_quick_quote_filing','filing_id',$quote_id);
	  }
	  if (is_array($this->input->post('in_fi_filing_number'))){
			$k = 0;
			 foreach($this->input->post('in_fi_filing_number') as $in_fi_filing_number){
				$result_fi['quote_id'] =$quote_id;
				$result_fi['fil_type'] = isset($post['fil_type'][$k]) ? $post['fil_type'][$k] : NULL;
				$result_fi['fil_type_other'] = isset($post['fil_type_other'][$k]) ? $post['fil_type_other'][$k] : NULL;
				$result_fi['in_fi_filing_number'] = isset($post['in_fi_filing_number'][$k]) ? $post['in_fi_filing_number'][$k] : NULL;
				$result_fi['in_fi_dba'] = isset($post['in_fi_dba'][$k]) ? $post['in_fi_dba'][$k] : NULL;
				$result_fi['in_fi_address'] = isset($post['in_fi_address'][$k]) ? $post['in_fi_address'][$k] : NULL;
				$result_fi['in_fi_city'] = isset($post['in_fi_city'][$k]) ? $post['in_fi_city'][$k] : NULL;
				$result_fi['in_fi_state'] = isset($post['in_fi_state'][$k]) ? $post['in_fi_state'][$k] : NULL;
				$result_fi['in_fi_zip'] = isset($post['in_fi_zip'][$k]) ? $post['in_fi_zip'][$k] : NULL;
				$result_fi['in_fi_remark'] = isset($post['in_fi_remark'][$k]) ? $post['in_fi_remark'][$k] : NULL;
							
				$in_fi_id=$this->input->post('in_fi_id');
				
				if(!empty($in_fi_id[$k])){
						$this->db->where('in_fi_id',$in_fi_id[$k]);
						$this->db->update('rq_rqf_quick_quote_filings',$result_fi);
						$fil_old_id[]=$in_fi_id[$k];
					}else{
						$this->db->insert('rq_rqf_quick_quote_filings', $result_fi);
						$fil_new_id[]=$this->db->insert_id();
					}
					$k++;
					$this->is_delete($fil_old_id,$fil_new_id,'rq_rqf_quick_quote_filings','in_fi_id',$quote_id);
			 }
		}		
	$b_id = $this->input->post('b_id');
	if ($_POST) 
		{
		$insert_data_b = array(	
		'quote_id'=>$quote_id,
		'applicant_ever' => $this->input->post('applicant_ever'),
		'explain_business' => $this->input->post('explain_business'),
		'applicant_conducted' => $this->input->post('applicant_conducted'),
		'explain_conducted' => $this->input->post('explain_conducted'),
		'applicant_transportation' => $this->input->post('applicant_transportation'),
		'exp_year' => $this->input->post('exp_year'),
		'exp_case' => $this->input->post('exp_case'),
		'exp_number' => $this->input->post('exp_number'),
		'exp_county' => $this->input->post('exp_county'),
		'exp_state' => $this->input->post('exp_state'),
		);
	
		if($b_id!=''){		
		    $this->db->where('b_id',$b_id);
			$this->db->update('rq_rqf_quick_quote_bussiness',$insert_data_b);	
	    	
		 }else{
	    	$this->db->insert('rq_rqf_quick_quote_bussiness',$insert_data_b);
	     }		
       }  
	$trucking_comp_name=$this->input->post('trucking_comp_name');
	$trucking_comp=!empty($trucking_comp_name)?implode(',',$trucking_comp_name):NULL;
	$t_id = $this->input->post('t_id');  
	$insert_data_transported = array(	
	'quote_id'=>$quote_id,
	'transported_name' => ($this->input->post('transported_name'))?$this->input->post('transported_name'):'',
	'passengers_name' => ($this->input->post('passengers_name'))?$this->input->post('transported_name'):'',
	
	
	'insured_f_name' => ($this->input->post('insured_f_name'))?$this->input->post('insured_f_name'):NULL,
	'insured_m_name' => ($this->input->post('insured_m_name'))?$this->input->post('insured_m_name'):NULL,
	'insured_l_name' => ($this->input->post('insured_l_name'))?$this->input->post('insured_l_name'):NULL,	
	'insured_d' => ($this->input->post('insured_d'))?$this->input->post('insured_d'):NULL,
	
	
	'insuerd_a' => ($this->input->post('insuerd_a'))?$this->input->post('insuerd_a'):NULL,
	'insuerd_c' => ($this->input->post('insuerd_c'))?$this->input->post('insuerd_c'):NULL,
	'state_app1' => ($this->input->post('state_app1'))?$this->input->post('state_app1'):NULL,	
	'country_app1' => ($this->input->post('country_app1'))?$this->input->post('country_app1'):NULL,	
	
	'insured_z1' => ($this->input->post('insured_z1'))?$this->input->post('insured_z1'):NULL,
	'insured_z2' => ($this->input->post('insured_z2'))?$this->input->post('insured_z2'):NULL,
	'insured_tel1' => ($this->input->post('insured_tel1'))?$this->input->post('insured_tel1'):NULL,
	'insured_tel2' => ($this->input->post('insured_tel2'))?$this->input->post('insured_tel2'):NULL,
	'insured_tel3' => ($this->input->post('insured_tel3'))?$this->input->post('insured_tel3'):NULL,
	'insured_tel4' => ($this->input->post('insured_tel_ext1'))?$this->input->post('insured_tel_ext1'):NULL,	
	'insured_em' => ($this->input->post('insured_em'))?$this->input->post('insured_em'):NULL,
	'transported_desc' => ($this->input->post('transported_desc'))?$this->input->post('transported_desc'):NULL,
	'trucking_comp_name' => $trucking_comp,
	'trucking_comp_other' => ($this->input->post('trucking_comp_other'))?$this->input->post('trucking_comp_other'):NULL,	
	'certificate_name' => ($this->input->post('certificate_name'))?$this->input->post('certificate_name'):NULL,
	'cert_num' => ($this->input->post('cert_num'))?$this->input->post('cert_num'):NULL,
	
	
	);


  if($t_id!='')
	{
	    $this->db->where('t_id',$t_id);
		$this->db->update('rq_rqf_quick_quote_transported',$insert_data_transported);	
	} else {
	
	$this->db->insert('rq_rqf_quick_quote_transported',$insert_data_transported);
     }
	
	
	
	$cert_id = $this->input->post('cert_id');		
	$cert_comp = $this->input->post('cert_comp');
	$cert_attent = $this->input->post('cert_attent');	
	$cert_add = $this->input->post('cert_add');
	$cert_city = $this->input->post('cert_city');
	$cert_email = $this->input->post('cert_email');	
	$cert_telephone1 = $this->input->post('cert_telephone1');
	$cert_telephone2 = $this->input->post('cert_telephone2');
	$cert_telephone3 = $this->input->post('cert_telephone3');
	$cert_telephone_ext = $this->input->post('cert_telephone_ext');
	$cert_fax1 = $this->input->post('cert_fax1');
	$cert_fax2 = $this->input->post('cert_fax2');
	$cert_fax3 = $this->input->post('cert_fax3');
    $state_cert = $this->input->post('state_cert');		

	$cert_zip = $this->input->post('cert_zip');
    $Additional_ins = $this->input->post('Additional_ins');

	$cert_old_id=array();
	$cert_new_id=array();
	
	for ($p = 0; $p < count($Additional_ins); $p++) {

	if ($Additional_ins[$p] != '') {	
	$insert_data_certificate['quote_id']=$quote_id;
	$insert_data_certificate['cert_comp']=isset($cert_comp[$p]) ? $cert_comp[$p] : '';
	$insert_data_certificate['cert_attent']=isset($cert_attent[$p]) ? $cert_attent[$p] : '';
	$insert_data_certificate['cert_add']=isset($cert_add[$p]) ? $cert_add[$p] : '';
	$insert_data_certificate['cert_city']=isset($cert_city[$p]) ? $cert_city[$p] : '';
	$insert_data_certificate['cert_email']=isset($cert_email[$p]) ? $cert_email[$p] : '';
	$insert_data_certificate['cert_telephone1']=isset($cert_telephone1[$p]) ? $cert_telephone1[$p] : '';
	$insert_data_certificate['cert_telephone2']=isset($cert_telephone2[$p]) ? $cert_telephone2[$p] : '';
	$insert_data_certificate['cert_telephone3']=isset($cert_telephone3[$p]) ? $cert_telephone3[$p] : '';
	$insert_data_certificate['cert_telephone_ext']=isset($cert_telephone_ext[$p]) ? $cert_telephone_ext[$p] : '';
	$insert_data_certificate['cert_fax1']=isset($cert_fax1[$p]) ? $cert_fax1[$p] : '';
	$insert_data_certificate['cert_fax2']=isset($cert_fax2[$p]) ? $cert_fax2[$p] : '';
	$insert_data_certificate['cert_fax3']=isset($cert_fax3[$p]) ? $cert_fax3[$p] : '';
	$insert_data_certificate['state_cert']=isset($state_cert[$p]) ? $state_cert[$p] : '';
	$insert_data_certificate['cert_zip']=isset($cert_zip[$p]) ? $cert_zip[$p] : '';
	$insert_data_certificate['Additional_ins']=isset($Additional_ins[$p]) ? $Additional_ins[$p] : '';
	
	 if(!empty($cert_id[$p]))
	{

	$this->_updateapp1($id, $insert_data_certificate,'rq_rqf_quick_quote_certificate',$cert_id[$p],'cert_id');
	$cert_old_id[]=$cert_id[$p];
	} else {

	$this->db->insert('rq_rqf_quick_quote_certificate',$insert_data_certificate);
     
	 $cert_new_id[]=$this->db->insert_id();
	  }
    }
  }
  $this->is_delete($cert_old_id,$cert_new_id,'rq_rqf_quick_quote_certificate','cert_id',$quote_id);
	$file_types = $this->config->item('doc_image_file_types');
	
	$app_com_id=$this->input->post('app_add_ins_id');
   
    $_file = !empty($_FILES['file_upload_second']) ? $this->do_upload($file_types,'file_upload_second','docs') : NULL;
  
    $_files_1= !empty($_file['file_name']) ? str_replace(' ', '_', $_file['file_name']) : NULL;

	$insert_data_Additional = array(
	'ref_id' =>$quote_id,
	'quote_id'=>$quote_id,		
	'owner_vehicle' => ($this->input->post('owner_vehicle'))?$this->input->post('owner_vehicle'):NULL,
	'insurance_val' => ($this->input->post('insurance_val'))?$this->input->post('insurance_val'):NULL,
	'ins_Policy_num' => ($this->input->post('ins_Policy_num'))?$this->input->post('ins_Policy_num'):NULL,
	'ins_Policy_comp' => ($this->input->post('ins_Policy_comp'))?$this->input->post('ins_Policy_comp'):NULL,	
	'effective_from_ins' => ($this->input->post('effective_from_ins'))?$this->input->post('effective_from_ins'):NULL,
	'expiration_from_ins' => ($this->input->post('expiration_from_ins'))?$this->input->post('expiration_from_ins'):NULL,
	'first_upload_second' =>($_files_1) ? $_files_1 : NULL,
	'uploaded_files_values_second' => ($this->input->post('uploaded_files_values_second'))?$this->input->post('uploaded_files_values_second'):NULL,	
	'owner_license' => ($this->input->post('owner_licenses'))?$this->input->post('owner_licenses'):NULL,
	'owner_applicant' => ($this->input->post('owner_applicant'))?$this->input->post('owner_applicant'):NULL,
	'persons_val'=> ($this->input->post('persons_val'))?$this->input->post('persons_val'):NULL,
	'applicant_scheduled' => ($this->input->post('applicant_scheduled'))?$this->input->post('applicant_scheduled'):NULL,
	'applicant_rent' => ($this->input->post('applicant_rent'))?$this->input->post('applicant_rent'):NULL,
	'owner_operator' => ($this->input->post('owner_operator'))?$this->input->post('owner_operator'):NULL,
	'ex_id' => ($this->input->post('ex_id'))?$this->input->post('ex_id'):NULL,			
	'supplement_dba' => ($this->input->post('supplement_dba'))?$this->input->post('supplement_dba'):NULL,
	'sup_dba' => ($this->input->post('sup_dba'))?$this->input->post('sup_dba'):NULL,		
	'supplement_val' => ($this->input->post('supplement_val'))?$this->input->post('supplement_val'):NULL,
	'sup_val' => ($this->input->post('sup_val'))?$this->input->post('sup_val'):NULL,	
	);		

	 if($app_com_id!='')
	{
		$this->db->where('app_add_ins_id',$app_com_id);
		$this->db->update('rq_rqf_quick_quote_additional',$insert_data_Additional);		
	
	} else {
	
	   $this->db->insert('rq_rqf_quick_quote_additional',$insert_data_Additional);
     }
	
	    
}
   	public function insert_coverage_quote($id=''){	
	
	
	$vh_ls_old_id=array();
	$vh_ls_new_id=array();	 
	$email_old_id4=array();
	$email_new_id4=array(); 
	$fax_old_id4=array();
	$fax_new_id4=array();  
	$vh_eqp_old_id=array();
	$vh_eqp_new_id=array();  		
	
				$post = $this->input->post();	
				$quote_id=!empty($id) ? $id: $this->get_quote_id(); 
				$file_types = $this->config->item('doc_image_file_types');
				$result_c['quote_id']=$quote_id;
				$coverage= $this->input->post("coverage");				
		        $result_c['coverage'] =!empty($coverage)?implode(',',$coverage):'';				
				$vh_type= $this->input->post("vh_type");				
		        $result_c['vh_type'] =!empty($vh_type)?implode(',',$vh_type):'';				
				$result_c['truck_no'] = trim($this->input->post("truck_no", true));
				$result_c['tractor_no'] = trim($this->input->post("tractor_no", true));				
				$result_c['trailer_no'] = trim($this->input->post("trailer_no", true));
				$result_c['other_no'] = trim($this->input->post("other_no", true));
				$result_c['other_vh_name'] = trim($this->input->post("other_vh_name", true));
				$coverage_id = $this->input->post("coverage_id");
					if(!empty($coverage_id)){
						$this->db->where('coverage_id',$coverage_id);
						$this->db->update('rq_rqf_quick_quote_coverage',$result_c);
					}else{
						$this->db->insert('rq_rqf_quick_quote_coverage', $result_c);
					}
			 
	if (is_array($this->input->post('vehicle_year_vh'))){			
		 $v = 0;
			 foreach($this->input->post('vehicle_year_vh') as $vehicle_year_vh){			
				$result_c_1['quote_id'] =$quote_id;
				$result_c_1['vehicle_year_vh'] = isset($post['vehicle_year_vh'][$v]) ? $post['vehicle_year_vh'][$v] : NULL;
				$result_c_1['make_truck'] = isset($post['make_truck'][$v]) ? $post['make_truck'][$v] : NULL;
				$result_c_1['model_tuck'] = isset($post['model_tuck'][$v]) ? $post['model_tuck'][$v] : NULL;
				$result_c_1['model_color'] = isset($post['model_color'][$v]) ? $post['model_color'][$v] : NULL;
				$result_c_1['make_truck_other'] = isset($post['make_truck_other'][$v]) ? $post['make_truck_other'][$v] : NULL;
				$result_c_1['model_tuck_other'] = isset($post['model_truck_other'][$v]) ? $post['model_truck_other'][$v] : NULL;
				$result_c_1['model_color_other'] = isset($post['model_color_other'][$v]) ? $post['model_color_other'][$v] : NULL;
				
				
				$result_c_1['gvw_vh'] = isset($post['gvw_vh'][$v]) ? $post['gvw_vh'][$v] : NULL;
				$result_c_1['vin_vh'] = isset($post['vin_vh'][$v]) ? $post['vin_vh'][$v] : NULL;
				$result_c_1['truck_number_of_axles'] = isset($post['truck_number_of_axles'][$v]) ? $post['truck_number_of_axles'][$v] : NULL;
				$result_c_1['vh_salvage'] = isset($post['vh_salvage'][$v]) ? $post['vh_salvage'][$v] : NULL;
				$result_c_1['truck_radius_of_operation']=isset($post['truck_radius_of_operation'][$v]) ? $post['truck_radius_of_operation'][$v] : NULL;
				$result_c_1['img_name_make']=isset($post['img_name_make'][$v]) ? $post['img_name_make'][$v] : NULL;
				$result_c_1['radius_of_operation_other']=isset($post['radius_of_operation_other'][$v]) ? $post['radius_of_operation_other'][$v] : NULL;
				$result_c_1['vh_nature_of_business']=isset($post['vh_nature_of_business'][$v]) ? $post['vh_nature_of_business'][$v] : NULL;
				
				$result_c_1['truck_trailer_owned_vehicle']=isset($post['truck_trailer_owned_vehicle'][$v]) ? $post['truck_trailer_owned_vehicle'][$v] : NULL;
				$result_c_1['vh_equipment_pull']=isset($post['vh_equipment_pull'][$v]) ? $post['vh_equipment_pull'][$v] : NULL;	
				$result_c_1['vh_lib_coverage']= isset($post['t_vh_lib_coverage'][$v]) ? $post['t_vh_lib_coverage'][$v] : NULL;
               
			    $result_c_1['liability_vh'] 	= isset($post['t_liability_vh'][$v]) ? $post['t_liability_vh'][$v] : NULL;
                $result_c_1['liability_ded_vh'] = isset($post['t_liability_ded_vh'][$v]) ? $post['t_liability_ded_vh'][$v] : NULL;
               	$result_c_1['truck_um'] 		= isset($post['t_truck_um'][$v]) ? $post['t_truck_um'][$v] : NULL;
				$result_c_1['truck_pip'] 	= isset($post['t_truck_pip'][$v]) ? $post['t_truck_pip'][$v] : NULL;
				$result_c_1['truck_uim'] 	= isset($post['t_truck_uim'][$v]) ? $post['t_truck_uim'][$v] : NULL;
				$result_c_1['vh_pd_coverage']= isset($post['t_vh_pd_coverage'][$v]) ? $post['t_vh_pd_coverage'][$v] : NULL;
				$result_c_1['pd_vh'] 		= isset($post['t_pd_vh'][$v]) ? $post['t_pd_vh'][$v] : NULL;
				$result_c_1['ph_ded_vh'] 	= isset($post['t_ph_ded_vh'][$v]) ? $post['t_ph_ded_vh'][$v] : NULL;
				$result_c_1['vh_pd_remark'] 	= isset($post['t_vh_pd_remark'][$v]) ? $post['t_vh_pd_remark'][$v] : NULL;
				$result_c_1['pd_want'] 		= isset($post['t_pd_want'][$v]) ? $post['t_pd_want'][$v] : NULL;
				$result_c_1['vh_cargo_coverage'] = isset($post['t_vh_cargo_coverage'][$v]) ? $post['t_vh_cargo_coverage'][$v] : NULL;
				$result_c_1['cargo_vh'] 		= isset($post['t_cargo_vh'][$v]) ? $post['t_cargo_vh'][$v] : NULL;
				$result_c_1['cargo_ded_vh'] 	= isset($post['t_cargo_ded_vh'][$v]) ? $post['t_cargo_ded_vh'][$v] : NULL;
				$result_c_1['refrigerated_breakdown'] = isset($post['t_refrigerated_breakdown'][$v]) ? $post['t_refrigerated_breakdown'][$v] : NULL;
				$result_c_1['deductible'] 	= isset($post['t_deductible'][$v]) ? $post['t_deductible'][$v] : NULL;
				$result_c_1['commodities_haulted'] 	= isset($post['t_commodities_haulted_truck'][$v]) ? $post['t_commodities_haulted_truck'][$v] : NULL;
				
				$t_vehicle_file = isset($_FILES['t_vehicle_file']) ? $this->upload_files($file_types,'vehicle_file', $_FILES['t_vehicle_file']) : '';
			   
			   
			   
			   	$result_c_1['liability'] 	= isset($post['liability_price'][$v]) ? $post['liability_price'][$v] : NULL;
				$result_c_1['uim'] 	= isset($post['uim_price'][$v]) ? $post['uim_price'][$v] : NULL;
				$result_c_1['pd'] 		= isset($post['pd_price'][$v]) ? $post['pd_price'][$v] : NULL;
				$result_c_1['pip'] = isset($post['pip_price'][$v]) ? $post['pip_price'][$v] : NULL;
				$result_c_1['um'] 		= isset($post['um_price'][$v]) ? $post['um_price'][$v] : NULL;
				$result_c_1['cargo'] 	= isset($post['cargo_price'][$v]) ? $post['cargo_price'][$v] : NULL;
				$result_c_1['total_equipment'] = isset($post['sub_total_price'][$v]) ? $post['sub_total_price'][$v] : NULL;
				
				$result_c_1['vh_equipment_trailers'] = isset($post['vh_equipment_trailers'][$v]) ? $post['vh_equipment_trailers'][$v] : NULL;
			   
				$result_c_1['vehicle_file'] = ($t_vehicle_file) ? is_array($t_vehicle_file) ? implode(',', $t_vehicle_file) : NULL : NULL;
				
				$result_c_1['type'] = isset($post['vehicle_type'][$v]) ? $post['vehicle_type'][$v] : NULL;	
				
				
				$vehicle_id=$this->input->post('vehicle_id');
				
				
				if(!empty($vehicle_id[$v])){
						$this->db->where('vehicle_id',$vehicle_id[$v]);
						$this->db->update('rq_rqf_quick_quote_vh',$result_c_1);
					}else{
						$this->db->insert('rq_rqf_quick_quote_vh', $result_c_1);
						//$parent_id=	$this->db->insert_id();
					}
				
					
					$v++;
					
			 }
		}
		
		    $vh_eqp_old_id_tr=array();
			$vh_eqp_new_id_tr=array();
			$ks=0;
			$kt=0;
			if (is_array($this->input->post('vh_eqp_type'))){			
		 $i = 0;
			 foreach($this->input->post('vh_eqp_type') as $vh_eqp_type){			
				$result_ca['quote_id'] 		=$quote_id;
				$result_ca['vh_eqp_type'] 	= isset($post['vh_eqp_type'][$i]) ? $post['vh_eqp_type'][$i] : NULL;
				$result_ca['vh_eqp_year'] 	= isset($post['vh_eqp_year'][$i]) ? $post['vh_eqp_year'][$i] : NULL;
				$result_ca['make_model_vh'] = isset($post['make_model_vh'][$i]) ? $post['make_model_vh'][$i] : NULL;
				$result_ca['vh_eqp_model'] 	= isset($post['vh_eqp_model'][$i]) ? $post['vh_eqp_model'][$i] : NULL;
				$result_ca['gvw_vh'] 		= isset($post['gvw_vh'][$i]) ? $post['gvw_vh'][$i] : NULL;
                $result_ca['vh_eqp_vin'] 	= isset($post['vh_eqp_vin'][$i]) ? $post['vh_eqp_vin'][$i] : NULL;
				$result_ca['vh_eqp_salvage']= isset($post['vh_eqp_salvage'][$i]) ? $post['vh_eqp_salvage'][$i] : NULL;
                $result_ca['vh_lib_coverage']= isset($post['vh_lib_coverage'][$i]) ? $post['vh_lib_coverage'][$i] : NULL;
                $result_ca['liability_vh'] 	= isset($post['liability_vh'][$i]) ? $post['liability_vh'][$i] : NULL;
                $result_ca['liability_ded_vh'] = isset($post['liability_ded_vh'][$i]) ? $post['liability_ded_vh'][$i] : NULL;
                $result_ca['truck_um'] 		= isset($post['truck_um'][$i]) ? $post['truck_um'][$i] : NULL;
				$result_ca['truck_pip'] 	= isset($post['truck_pip'][$i]) ? $post['truck_pip'][$i] : NULL;
				$result_ca['truck_uim'] 	= isset($post['truck_uim'][$i]) ? $post['truck_uim'][$i] : NULL;
				$result_ca['vh_pd_coverage']= isset($post['vh_pd_coverage'][$i]) ? $post['vh_pd_coverage'][$i] : NULL;
				$result_ca['pd_vh'] 		= isset($post['pd_vh'][$i]) ? $post['pd_vh'][$i] : NULL;
				$result_ca['ph_ded_vh'] 	= isset($post['ph_ded_vh'][$i]) ? $post['ph_ded_vh'][$i] : NULL;
				$result_ca['vh_pd_remark'] 	= isset($post['vh_pd_remark'][$i]) ? $post['vh_pd_remark'][$i] : NULL;
				$result_ca['pd_want'] 		= isset($post['pd_want'][$i]) ? $post['pd_want'][$i] : NULL;
				$result_ca['vh_cargo_coverage'] = isset($post['vh_cargo_coverage'][$i]) ? $post['vh_cargo_coverage'][$i] : NULL;
				$result_ca['cargo_vh'] 		= isset($post['cargo_vh'][$i]) ? $post['cargo_vh'][$i] : NULL;
				$result_ca['cargo_ded_vh'] 	= isset($post['cargo_ded_vh'][$i]) ? $post['cargo_ded_vh'][$i] : NULL;
				$result_ca['refrigerated_breakdown'] = isset($post['refrigerated_breakdown'][$i]) ? $post['refrigerated_breakdown'][$i] : NULL;
				$result_ca['deductible'] 	= isset($post['deductible'][$i]) ? $post['deductible'][$i] : NULL;
				$result_ca['commodities_haulted'] 	= isset($post['commodities_haulted_truck'][$i]) ? $post['commodities_haulted_truck'][$i] : NULL;			
				$vehicle_file = isset($_FILES['vehicle_file']) ? $this->upload_files_multiple($file_types,'vehicle_file', $_FILES['vehicle_file']) : '';			    
				$result_ca['vehicle_file'] = ($vehicle_file[$i]) ? is_array($vehicle_file[$i]) ? implode(',', $vehicle_file[$i]) : NULL : NULL;			
				
				$result_ca['vh_equip_trailer'] = isset($post['vh_equip_trailer'][$i]) ? $post['vh_equip_trailer'][$i] : NULL;
				$result_ca['vh_equipment_number'] = isset($post['vh_equipment_number'][$i]) ? $post['vh_equipment_number'][$i] : NULL;
				
				
				
				$result_ca['liability'] 	= isset($post['liability'][$i]) ? $post['liability'][$i] : NULL;
				$result_ca['uim'] 	= isset($post['uim'][$i]) ? $post['uim'][$i] : NULL;
				$result_ca['pd'] 		= isset($post['pd'][$i]) ? $post['pd'][$i] : NULL;
				$result_ca['pip'] = isset($post['pip'][$i]) ? $post['pip'][$i] : NULL;
				$result_ca['um'] 		= isset($post['um'][$i]) ? $post['um'][$i] : NULL;
				$result_ca['cargo'] 	= isset($post['cargo'][$i]) ? $post['cargo'][$i] : NULL;
				$result_ca['total_equip'] = isset($post['total_equip'][$i]) ? $post['total_equip'][$i] : NULL;
				
				$result_ca['type'] = isset($post['vh_eqp_types'][$i]) ? $post['vh_eqp_types'][$i] : NULL;			
				
				$vh_eqp_id=$this->input->post('vh_eqp_id');
				
				
				if(!empty($vh_eqp_id[$i])){
						$this->db->where('vh_eqp_id',$vh_eqp_id[$i]);
						$this->db->update('rq_rqf_quick_quote_vh_eqp',$result_ca);
						$vh_eqp_old_id[]=$vh_eqp_id[$i];
					}else{
						$this->db->insert('rq_rqf_quick_quote_vh_eqp', $result_ca);
						$vh_eqp_new_id[]=$this->db->insert_id();						
					}
					
				
				if($result_ca['type']=='tractor'){
				 $result_ca['vh_equipment_number'] = isset($post['vh_equipment_number'][$i]) ? $post['vh_equipment_number'][$i] : NULL;
				
			   
				for($k=0; $k <  $result_ca['vh_equipment_number']; $k++){
				$result_ca_tr['type'] = isset($post['vh_eqp_types_'.$kt.''][$k]) ? $post['vh_eqp_types_'.$kt.''][$k] : NULL;		
			  
				$result_ca_tr['vh_equipment_number'] = isset($post['vh_equipment_number'][$i]) ? $post['vh_equipment_number'][$i] : NULL;				
				$result_ca_tr['quote_id'] 		=$quote_id;
				$result_ca_tr['parent_id'] 		=!empty($vh_eqp_new_id[$i])?$vh_eqp_new_id[$i]:NULL;
				$result_ca_tr['vh_eqp_type'] 	= isset($post['vh_eqp_typess_tr'][$k]) ? $post['vh_eqp_typess_tr'][$k] : NULL;
				$result_ca_tr['vh_eqp_year'] 	= isset($post['vh_eqp_year_'.$kt.''][$k]) ? $post['vh_eqp_year_'.$kt.''][$k] : NULL;
				$result_ca_tr['make_model_vh'] = isset($post['make_model_vh_'.$kt.''][$k]) ? $post['make_model_vh_'.$kt.''][$k] : NULL;
				$result_ca_tr['vh_eqp_model'] 	= isset($post['vh_eqp_model_'.$kt.''][$k]) ? $post['vh_eqp_model_'.$kt.''][$k] : NULL;
				$result_ca_tr['gvw_vh'] 		= isset($post['gvw_vh_'.$kt.''][$k]) ? $post['gvw_vh_'.$kt.''][$k] : NULL;
                $result_ca_tr['vh_eqp_vin'] 	= isset($post['vh_eqp_vin_'.$kt.''][$k]) ? $post['vh_eqp_vin_'.$kt.''][$k] : NULL;
				$result_ca_tr['vh_eqp_salvage']= isset($post['vh_eqp_salvage_'.$kt.''][$k]) ? $post['vh_eqp_salvage_'.$kt.''][$k] : NULL;
                $result_ca_tr['vh_lib_coverage']= isset($post['vh_lib_coverage_'.$kt.''][$k]) ? $post['vh_lib_coverage_'.$kt.''][$k] : NULL;
             	$result_ca_tr['liability_vh'] 	= isset($post['liability_vh_'.$kt.''][$k]) ? $post['liability_vh_'.$kt.''][$k] : NULL;
                $result_ca_tr['liability_ded_vh'] = isset($post['liability_ded_vh_'.$kt.''][$k]) ? $post['liability_ded_vh_'.$kt.''][$k] : NULL;
                $result_ca_tr['truck_um'] 		= isset($post['truck_um_'.$kt.''][$k]) ? $post['truck_um_'.$kt.''][$k] : NULL;
				$result_ca_tr['truck_pip'] 	= isset($post['truck_pip_'.$kt.''][$k]) ? $post['truck_pip_'.$kt.''][$k] : NULL;
				$result_ca_tr['truck_uim'] 	= isset($post['truck_uim_'.$kt.''][$k]) ? $post['truck_uim_'.$kt.''][$k] : NULL;
				$result_ca_tr['vh_pd_coverage']= isset($post['vh_pd_coverage_'.$kt.''][$k]) ? $post['vh_pd_coverage_'.$kt.''][$k] : NULL;
				$result_ca_tr['pd_vh'] 		= isset($post['pd_vh_'.$kt.''][$k]) ? $post['pd_vh_'.$kt.''][$k] : NULL;
				$result_ca_tr['ph_ded_vh'] 	= isset($post['ph_ded_vh_'.$kt.''][$k]) ? $post['ph_ded_vh_'.$kt.''][$k] : NULL;
				$result_ca_tr['vh_pd_remark'] 	= isset($post['vh_pd_remark_'.$kt.''][$k]) ? $post['vh_pd_remark_'.$kt.''][$k] : NULL;
				$result_ca_tr['pd_want'] 		= isset($post['pd_want_'.$kt.''][$k]) ? $post['pd_want_'.$kt.''][$k] : NULL;
				$result_ca_tr['vh_cargo_coverage'] = isset($post['vh_cargo_coverage_'.$kt.''][$k]) ? $post['vh_cargo_coverage_'.$kt.''][$k] : NULL;
				$result_ca_tr['cargo_vh'] 		= isset($post['cargo_vh_'.$kt.''][$k]) ? $post['cargo_vh_'.$kt.''][$k] : NULL;
				$result_ca_tr['cargo_ded_vh'] 	= isset($post['cargo_ded_vh_'.$kt.''][$k]) ? $post['cargo_ded_vh_'.$kt.''][$k] : NULL;
				$result_ca_tr['refrigerated_breakdown'] = isset($post['refrigerated_breakdown_'.$kt.''][$k]) ? $post['refrigerated_breakdown_'.$kt.''][$k] : NULL;
				$result_ca_tr['deductible'] 	= isset($post['deductible_'.$kt.''][$k]) ? $post['deductible_'.$kt.''][$k] : NULL;
				$result_ca_tr['commodities_haulted'] 	= isset($post['commodities_haulted_truck_'.$kt.''][$k]) ? $post['commodities_haulted_truck_'.$kt.''][$k] : NULL;			
				$vehicle_file_tr = isset($_FILES['vehicle_file_'.$kt.'']) ? $this->upload_files_multiple($file_types,'vehicle_file', $_FILES['vehicle_file_'.$kt.'']) : '';			    
				$result_ca_tr['vehicle_file'] = !empty($vehicle_file_tr[$k]) ? is_array($vehicle_file_tr[$k]) ? implode(',', $vehicle_file_tr[$k]) : NULL : NULL;
				
				
			/*	$result_ca['liability'] 	= isset($post['liability'][$k]) ? $post['liability'][$k] : NULL;
				$result_ca['uim'] 	= isset($post['uim'][$k]) ? $post['uim'][$k] : NULL;
				$result_ca['pd'] 		= isset($post['pd'][$k]) ? $post['pd'][$k] : NULL;
				$result_ca['pip'] = isset($post['pip'][$k]) ? $post['pip'][$k] : NULL;
				$result_ca['um'] 		= isset($post['um'][$k]) ? $post['um'][$k] : NULL;
				$result_ca['cargo'] 	= isset($post['cargo'][$k]) ? $post['cargo'][$k] : NULL;
				$result_ca['total_equip'] = isset($post['total_equip'][$k]) ? $post['total_equip'][$k] : NULL;*/
				
						
				
				
				
				$vh_eqp_id_tr=$this->input->post('vh_eqp_id_'.$kt.'');
				
				
				if(!empty($vh_eqp_id_tr[$k])){
						$this->db->where('vh_eqp_id',$vh_eqp_id_tr[$k]);
						$this->db->update('rq_rqf_quick_quote_vh_eqps',$result_ca_tr);
						$vh_eqp_old_id_tr[]=$vh_eqp_id_tr[$k];
					}else{
						$this->db->insert('rq_rqf_quick_quote_vh_eqps', $result_ca_tr);
						$vh_eqp_new_id_tr[]=$this->db->insert_id();
					}
					
				  }				
				$kt++;
				
			}
				else if($result_ca['type']=='other'){
					
			  $result_ca['vh_equipment_number'] = isset($post['vh_equipment_number'][$i]) ? $post['vh_equipment_number'][$i] : NULL;
					
				for($k1=0; $k1 <  $result_ca['vh_equipment_number']; $k1++){
				
			    $result_ca_tr_o['type'] = isset($post['vh_eqp_types_o'.$ks.''][$k1]) ? $post['vh_eqp_types_o'.$ks.''][$k1] : NULL;		
			
				$result_ca_tr_o['vh_equipment_number'] = isset($post['vh_equipment_number'][$i]) ? $post['vh_equipment_number'][$i] : NULL;
						
				$result_ca_tr_o['quote_id'] 		=$quote_id;
				$result_ca_tr_o['parent_id'] 		=!empty($vh_eqp_new_id[$i])?$vh_eqp_new_id[$i]:NULL;
				$result_ca_tr_o['vh_eqp_type'] 	= isset($post['vh_eqp_typess_tr_o'][$k1]) ? $post['vh_eqp_typess_tr_o'][$k1] : NULL;
				$result_ca_tr_o['vh_eqp_year'] 	= isset($post['vh_eqp_year_o'.$ks.''][$k1]) ? $post['vh_eqp_year_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['make_model_vh'] = isset($post['make_model_vh_o'.$ks.''][$k1]) ? $post['make_model_vh_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['vh_eqp_model'] 	= isset($post['vh_eqp_model_o'.$ks.''][$k1]) ? $post['vh_eqp_model_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['gvw_vh'] 		= isset($post['gvw_vh_o'.$ks.''][$k1]) ? $post['gvw_vh_o'.$ks.''][$k1] : NULL;
                $result_ca_tr_o['vh_eqp_vin'] 	= isset($post['vh_eqp_vin_o'.$ks.''][$k1]) ? $post['vh_eqp_vin_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['vh_eqp_salvage']= isset($post['vh_eqp_salvage_o'.$ks.''][$k1]) ? $post['vh_eqp_salvage_o'.$ks.''][$k1] : NULL;
                $result_ca_tr_o['vh_lib_coverage']= isset($post['vh_lib_coverage_o'.$ks.''][$k1]) ? $post['vh_lib_coverage_o'.$ks.''][$k1] : NULL;
             	$result_ca_tr_o['liability_vh'] 	= isset($post['liability_vh_o'.$ks.''][$k1]) ? $post['liability_vh_o'.$ks.''][$k1] : NULL;
                $result_ca_tr_o['liability_ded_vh'] = isset($post['liability_ded_vh_o'.$ks.''][$k1]) ? $post['liability_ded_vh_o'.$ks.''][$k1] : NULL;
                $result_ca_tr_o['truck_um'] 		= isset($post['truck_um_o'.$ks.''][$k1]) ? $post['truck_um_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['truck_pip'] 	= isset($post['truck_pip_o'.$ks.''][$k1]) ? $post['truck_pip_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['truck_uim'] 	= isset($post['truck_uim_o'.$ks.''][$k1]) ? $post['truck_uim_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['vh_pd_coverage']= isset($post['vh_pd_coverage_o'.$ks.''][$k1]) ? $post['vh_pd_coverage_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['pd_vh'] 		= isset($post['pd_vh_o'.$ks.''][$k1]) ? $post['pd_vh_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['ph_ded_vh'] 	= isset($post['ph_ded_vh_o'.$ks.''][$k1]) ? $post['ph_ded_vh_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['vh_pd_remark'] 	= isset($post['vh_pd_remark_o'.$ks.''][$k1]) ? $post['vh_pd_remark_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['pd_want'] 		= isset($post['pd_want_o'.$ks.''][$k1]) ? $post['pd_want_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['vh_cargo_coverage'] = isset($post['vh_cargo_coverage_o'.$ks.''][$k1]) ? $post['vh_cargo_coverage_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['cargo_vh'] 		= isset($post['cargo_vh_o'.$ks.''][$k1]) ? $post['cargo_vh_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['cargo_ded_vh'] 	= isset($post['cargo_ded_vh_o'.$ks.''][$k1]) ? $post['cargo_ded_vh_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['refrigerated_breakdown'] = isset($post['refrigerated_breakdown_o'.$ks.''][$k1]) ? $post['refrigerated_breakdown_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['deductible'] 	= isset($post['deductible_o'.$ks.''][$k1]) ? $post['deductible_o'.$ks.''][$k1] : NULL;
				$result_ca_tr_o['commodities_haulted'] 	= isset($post['commodities_haulted_truck_o'.$ks.''][$k1]) ? $post['commodities_haulted_truck_o'.$ks.''][$k1] : NULL;			
				$vehicle_file_tr_o = isset($_FILES['vehicle_file_o'.$ks.'']) ? $this->upload_files_multiple($file_types,'vehicle_file', $_FILES['vehicle_file_o'.$ks.'']) : '';			    
				$result_ca_tr_o['vehicle_file'] = !empty($vehicle_file_tr_o[$k1]) ? is_array($vehicle_file_tr_o[$k1]) ? implode(',', $vehicle_file_tr_o[$k1]) : NULL : NULL;
				
				
			/*	$result_ca['liability'] 	= isset($post['liability'][$k1]) ? $post['liability'][$k1] : NULL;
				$result_ca['uim'] 	= isset($post['uim'][$k1]) ? $post['uim'][$k1] : NULL;
				$result_ca['pd'] 		= isset($post['pd'][$k1]) ? $post['pd'][$k1] : NULL;
				$result_ca['pip'] = isset($post['pip'][$k1]) ? $post['pip'][$k1] : NULL;
				$result_ca['um'] 		= isset($post['um'][$k1]) ? $post['um'][$k1] : NULL;
				$result_ca['cargo'] 	= isset($post['cargo'][$k1]) ? $post['cargo'][$k1] : NULL;
				$result_ca['total_equip'] = isset($post['total_equip'][$k1]) ? $post['total_equip'][$k1] : NULL;*/
				
						
				
				$vh_eqp_id_tr_o=$this->input->post('vh_eqp_id_o'.$ks.'');				
				
				if(!empty($vh_eqp_id_tr_o[$k1])){
						$this->db->where('vh_eqp_id',$vh_eqp_id_tr_o[$k1]);
						$this->db->update('rq_rqf_quick_quote_vh_eqps',$result_ca_tr_o);
						$vh_eqp_old_id_tr_o[]=$vh_eqp_id_tr_o[$k1];
					}else{
						$this->db->insert('rq_rqf_quick_quote_vh_eqps', $result_ca_tr_o);
						$vh_eqp_new_id_tr_o[]=$this->db->insert_id();
					}
					
				  }				
				$ks++;
			  }
			$i++;
		 }
			
		
		 // $this->is_delete($vh_eqp_old_id_tr,$vh_eqp_new_id_tr,'rq_rqf_quick_quote_vh_eqp','vh_eqp_id',$quote_id,$post['vehicle_type'][$v]);          
	}
		
	if (is_array($this->input->post('vh_ls_name'))){			
		 $i = 0;
			 foreach($this->input->post('vh_ls_name') as $vh_ls_name){			
				$result_ca_1['quote_id'] =$quote_id;
				$result_ca_1['lessor_type'] = isset($post['lessor_type'][$i]) ? $post['lessor_type'][$i] : NULL;
				$result_ca_1['vh_ls_name'] = isset($post['vh_ls_name'][$i]) ? $post['vh_ls_name'][$i] : NULL;
				$result_ca_1['vh_ls_m_name'] = isset($post['vh_ls_m_name'][$i]) ? $post['vh_ls_m_name'][$i] : NULL;
				$result_ca_1['vh_ls_l_name'] = isset($post['vh_ls_l_name'][$i]) ? $post['vh_ls_l_name'][$i] : NULL;
				$result_ca_1['vh_ls_dba'] = isset($post['vh_ls_dba'][$i]) ? $post['vh_ls_dba'][$i] : NULL;
				$result_ca_1['vh_ls_address'] = isset($post['vh_ls_address'][$i]) ? $post['vh_ls_address'][$i] : NULL;
                $result_ca_1['vh_ls_city'] = isset($post['vh_ls_city'][$i]) ? $post['vh_ls_city'][$i] : NULL;
				$result_ca_1['vh_ls_state'] = isset($post['vh_ls_state'][$i]) ? $post['vh_ls_state'][$i] : NULL;
                $result_ca_1['vh_ls_county'] = isset($post['vh_ls_county'][$i]) ? $post['vh_ls_county'][$i] : NULL;
                $result_ca_1['vh_ls_zip'] = isset($post['vh_ls_zip'][$i]) ? $post['vh_ls_zip'][$i] : NULL;
                $result_ca_1['vh_ls_country'] = isset($post['vh_ls_country'][$i]) ? $post['vh_ls_country'][$i] : NULL;                
				$result_ca_1['vh_ls_ph_type'] = isset($post['vh_ls_ph_type'][$i]) ? $post['vh_ls_ph_type'][$i] : NULL;
				$result_ca_1['vh_ls_country_code'] = isset($post['vh_ls_country_code'][$i]) ? $post['vh_ls_country_code'][$i] : NULL;
				$result_ca_1['vh_ls_areacode'] = isset($post['vh_ls_areacode'][$i]) ? $post['vh_ls_areacode'][$i] : NULL;
				$result_ca_1['vh_ls_prefix'] = isset($post['vh_ls_prefix'][$i]) ? $post['vh_ls_prefix'][$i] : NULL;
				$result_ca_1['vh_ls_suffixe'] = isset($post['vh_ls_suffixe'][$i]) ? $post['vh_ls_suffixe'][$i] : NULL;
				$result_ca_1['vh_ls_ext'] = isset($post['vh_ls_ext'][$i]) ? $post['vh_ls_ext'][$i] : NULL;
				$result_ca_1['pri_country'] = isset($post['vh_ls_ph_pri_sequence'][$i]) ? $post['vh_ls_ph_pri_sequence'][$i] : NULL;
				$result_ca_1['in_truck_fax'] = isset($post['in_truck_fax'][$i]) ? $post['in_truck_fax'][$i] : NULL;
				
				$result_ca_1['type'] = isset($post['vh_types'][$i]) ? $post['vh_types'][$i] : NULL;			
				
				$vh_ls_id=$this->input->post('vh_ls_id');
				
				
				if(!empty($vh_ls_id[$i])){
						$this->db->where('vh_ls_id',$vh_ls_id[$i]);
						$this->db->update('rq_rqf_quick_quote_vh_garage',$result_ca_1);
						$vh_ls_old_id[]=$vh_ls_id[$i];
					}else{
						$this->db->insert('rq_rqf_quick_quote_vh_garage', $result_ca_1);
						$vh_ls_new_id[]=$this->db->insert_id();
						$parent_id=	$this->db->insert_id();
					}
			 $this->is_delete($vh_ls_old_id,$vh_ls_new_id,'rq_rqf_quick_quote_vh_garage','vh_ls_id',$quote_id,$post['vh_type'][$i]);  
			if (is_array($this->input->post('vh_ls_email_type_'.$i.''))){
			$j = 0;
			 foreach($this->input->post('vh_ls_email_type_'.$i.'') as $vh_ls_email_type){
				$result_ca_2['quote_id'] =$quote_id;
				$result_ca_2['email_type'] = isset($post['vh_ls_email_type_'.$i.''][$j]) ? $post['vh_ls_email_type_'.$i.''][$j] : NULL;
				$result_ca_2['email'] = isset($post['vh_ls_email_'.$i.''][$j]) ? $post['vh_ls_email_'.$i.''][$j] : NULL;
				$result_ca_2['priority_email'] = isset($post['vh_ls_em_pri_sequence_'.$i.''][$j]) ? $post['vh_ls_em_pri_sequence_'.$i.''][$j] : NULL;
				$result_ca_2['type'] = isset($post['vh_em_type'][$j]) ? $post['vh_em_type'][$j] : NULL;
				$result_ca_2['parent_id'] = isset($parent_id)?$parent_id:$vh_ls_id[$i];
				$email_id=$this->input->post('vh_ls_email_id_'.$i.'');
				
				if(!empty($email_id[$j])){
						$this->db->where('email_id',$email_id[$j]);
						$email_old_id4[]=$email_id[$j];
						$this->db->update('rq_rqf_quick_quote_email',$result_ca_2);
					}else{
						$this->db->insert('rq_rqf_quick_quote_email', $result_ca_2);
						$email_new_id4[]=$this->db->insert_id();
					}
					$j++;
			 }
		  $this->is_delete($email_old_id4,$email_new_id4,'rq_rqf_quick_quote_email','email_id',$quote_id,$post['vh_type'][$i]);
		}
				
		 if (is_array($this->input->post('vh_ls_fx_type_'.$i.''))){
			$k = 0;
			 foreach($this->input->post('vh_ls_fx_type_'.$i.'') as $vh_ls_fx_type){
				$result_ca_3['quote_id'] =$quote_id;
				$result_ca_3['fax_type'] = isset($post['vh_ls_fx_type_'.$i.''][$k]) ? $post['vh_ls_fx_type_'.$i.''][$k] : NULL;
				$result_ca_3['code_type_fax'] = isset($post['vh_ls_fx_country_code_'.$i.''][$k]) ? $post['vh_ls_fx_country_code_'.$i.''][$k] : NULL;
				$result_ca_3['area_code_fax'] = isset($post['vh_ls_fx_areacode_'.$i.''][$k]) ? $post['vh_ls_fx_areacode_'.$i.''][$k] : NULL;
				$result_ca_3['prefix_fax'] = isset($post['vh_ls_fx_prefix_'.$i.''][$k]) ? $post['vh_ls_fx_prefix_'.$i.''][$k] : NULL;
				$result_ca_3['suffix_fax'] = isset($post['vh_ls_fx_suffix_'.$i.''][$k]) ? $post['vh_ls_fx_suffix_'.$i.''][$k] : NULL;
				$result_ca_3['country_fax'] = isset($post['vh_ls_fx_pri_sequence_'.$i.''][$k]) ? $post['vh_ls_fx_pri_sequence_'.$i.''][$k] : NULL;
				$result_ca_3['type'] = isset($post['vh_fx_type'][$k]) ? $post['vh_fx_type'][$k] : NULL;
				$result_ca_3['parent_id'] = isset($parent_id)?$parent_id:$vh_ls_id[$i];
				$fax_id=$this->input->post('vh_fx_id_'.$i.'');
				
				if(!empty($fax_id[$k])){
						$this->db->where('fax_id',$fax_id[$k]);
						$this->db->update('rq_rqf_quick_quote_fax',$result_ca_3);
						$fax_old_id4[]=$fax_id[$k];
					}else{
						$this->db->insert('rq_rqf_quick_quote_fax', $result_ca_3);
						$fax_new_id4[]=$this->db->insert_id();
					}
					$k++;
			 }
			   $this->is_delete($fax_old_id4,$fax_new_id4,'rq_rqf_quick_quote_fax','fax_id',$quote_id,$post['vh_type'][$i]);
		  }
					$i++;
			 }
			 	
	   }
			
	}
   
   
	public function insert_driver_quote($id=''){
		$driver_new_id=array();		
		$driver_old_id=array();	
		    $post = $this->input->post();	
			$quote_id=!empty($id) ? $id: $this->get_quote_id();
			
			$file_types = $this->config->item('doc_image_file_types');			
		    $data['driver_count']=$this->input->post('driver_count');			
			//$file_copy = $this->do_upload($file_types,'driv_mvr_file', 'file');
			$file_copy = isset($_FILES['driv_mvr_file']) ? $this->upload_files($file_types,'file', $_FILES['driv_mvr_file']) : '';
			$data['driv_mvr_file'] = ($file_copy) ? is_array($file_copy) ? implode(',', $file_copy) : NULL : NULL;
				
			$data['quote_id']= $quote_id;	
		    $driver_ids=$this->input->post('driver_ids');	
			if($data['driver_count']>0){
		    if(!empty($driver_ids)){
						$this->db->where('drivers_id',$driver_ids);
						$this->db->update('rq_rqf_quick_quote_drivers',$data);
					}else{
						$this->db->insert('rq_rqf_quick_quote_drivers', $data);
						
					}
		
			}
		    if (is_array($this->input->post('driver_name'))){			
		    $i = 0;
			 foreach($this->input->post('driver_name') as $driver_name){			
				$result_d['quote_id'] 		= !empty($id) ? $id: $quote_id;
				$result_d['driver_name'] 	= isset($post['driver_name'][$i]) ? $post['driver_name'][$i] : NULL;
				$result_d['driver_middle_name'] 	= isset($post['driver_middle_name'][$i]) ? $post['driver_middle_name'][$i] : NULL;
				$result_d['driver_last_name'] = isset($post['driver_last_name'][$i]) ? $post['driver_last_name'][$i] : NULL;
				$result_d['driver_license'] 	= isset($post['driver_license'][$i]) ? $post['driver_license'][$i] : NULL;
				$result_d['driver_license_issue_date'] 		= isset($post['driver_license_issue_date'][$i]) ? $post['driver_license_issue_date'][$i] : NULL;
                $result_d['driver_dob'] 	= isset($post['driver_dob'][$i]) ? $post['driver_dob'][$i] : NULL;
				$result_d['driver_license_class']= isset($post['driver_license_class'][$i]) ? $post['driver_license_class'][$i] : NULL;
                $result_d['driver_class_a_years']= isset($post['driver_class_a_years'][$i]) ? $post['driver_class_a_years'][$i] : NULL;
                $result_d['driver_license_issue_state'] 	= isset($post['driver_license_issue_state'][$i]) ? $post['driver_license_issue_state'][$i] : NULL;
               
				$driver_id=$this->input->post('driver_id');
				
				if(!empty($driver_id[$i])){
						$this->db->where('driver_id',$driver_id[$i]);
						$driver_old_id[]= $driver_id[$i];
						$this->db->update('rq_rqf_quick_quote_driver',$result_d);
					}else{
						$this->db->insert('rq_rqf_quick_quote_driver', $result_d);
						$driver_new_id[]=$this->db->insert_id();
					}
				}
					$i++;
			
			 $this->is_delete($driver_old_id,$driver_new_id,'rq_rqf_quick_quote_driver','driver_id',$quote_id);
		}
		
	}
	
	public function insert_owner_quote($id=''){
		    $post = $this->input->post();
			$owner_new_id=array();		
		    $owner_old_id=array();		
			$quote_id=!empty($id) ? $id: $this->get_quote_id(); 
			$file_types = $this->config->item('doc_image_file_types');
			$data_1['number_of_owner']=$this->input->post('number_of_owner');			
		    $file_copy = isset($_FILES['own_mvr_file']) ? $this->upload_files($file_types,'file', $_FILES['own_mvr_file']) : '';
			$data_1['own_mvr_file'] = ($file_copy) ? is_array($file_copy) ? implode(',', $file_copy) : NULL : NULL;
			
			$data_1['quote_id']=$quote_id;
			$owner_ids=$this->input->post('owner_ids');
			if($data_1['number_of_owner']>0){
		    if(!empty($owner_ids)){
						$this->db->where('owner_ids',$owner_ids);
						$this->db->update('rq_rqf_quick_quote_owners',$data_1);
						
					}else{
						$this->db->insert('rq_rqf_quick_quote_owners', $data_1);
						
					}
			}
		    if (is_array($this->input->post('owner_name'))){			
		    $i = 0;
			 foreach($this->input->post('owner_name') as $owner_name){			
				$result_o['quote_id'] 		=!empty($id) ?$id:$quote_id;
				$result_o['owner_name'] 	= isset($post['owner_name'][$i]) ? $post['owner_name'][$i] : NULL;
				$result_o['owner_middle_name'] 	= isset($post['owner_middle_name'][$i]) ? $post['owner_middle_name'][$i] : NULL;
				$result_o['owner_last_name'] = isset($post['owner_last_name'][$i]) ? $post['owner_last_name'][$i] : NULL;
				$result_o['owner_is_driver'] 	= isset($post['owner_is_driver'][$i]) ? $post['owner_is_driver'][$i] : NULL;
				$result_o['owner_license'] 		= isset($post['owner_license'][$i]) ? $post['owner_license'][$i] : NULL;
                $result_o['license_issue_state'] 	= isset($post['license_issue_state'][$i]) ? $post['license_issue_state'][$i] : NULL;
				
				$owner_id=$this->input->post('owner_id');
				
				
				if(!empty($owner_id[$i])){
						$this->db->where('owner_id',$owner_id[$i]);
						$owner_old_id[]= $owner_id[$i];
						$this->db->update('rq_rqf_quick_quote_owner',$result_o);
					}else{
						$this->db->insert('rq_rqf_quick_quote_owner', $result_o);
						$owner_new_id[]=$this->db->insert_id();
					}
					$i++;
			 }
			  $this->is_delete($owner_old_id,$owner_new_id,'rq_rqf_quick_quote_owner','owner_id',$quote_id);
		}
		
	}
	
	
	public function insert_losses_quote($id=''){
		    $file_types = $this->config->item('doc_image_file_types');
		    $post = $this->input->post();
			$file_copy = isset($_FILES['loss_report_file']) ? $this->upload_files($file_types,'file', $_FILES['loss_report_file']) : '';
			
			    $quote_id=!empty($id) ? $id: $this->get_quote_id(); 
			    $result_l_1['losses_need'] = $this->input->post("losses_need");
				$result_l_1['liability'] = $this->input->post("loss_liability");
				$result_l_1['pd'] = $this->input->post("loss_pd");				
				$result_l_1['cargo'] = $this->input->post("loss_cargo");
				$result_l_1['quote_id'] =!empty($id) ? $id:$quote_id;
				$result_l_1['loss_report_file'] = ($file_copy) ? is_array($file_copy) ? implode(',', $file_copy) : NULL : NULL;
			
		
				
			
				$loss_id = $this->input->post("loss_id");
				
				if(!empty($loss_id)){
						$this->db->where('loss_id',$loss_id);
						$this->db->update('rq_rqf_quick_quote_loss',$result_l_1);
					}else{
						$this->db->insert('rq_rqf_quick_quote_loss', $result_l_1);
						
					}
				
			
		    if (is_array($this->input->post('date_from_losses'))){			
		    $i = 0;
			 foreach($this->input->post('date_from_losses') as $date_from_losses){			
				$result_l['quote_id'] 		=!empty($id) ? $id:$quote_id;
				$result_l['date_from_losses'] 	= isset($post['date_from_losses'][$i]) ? $post['date_from_losses'][$i] : NULL;
				$result_l['date_to_losses'] 	= isset($post['date_to_losses'][$i]) ? $post['date_to_losses'][$i] : NULL;
				$result_l['losses_amount'] = isset($post['losses_amount'][$i]) ? $post['losses_amount'][$i] : NULL;
				$result_l['losses_company'] 	= isset($post['losses_company'][$i]) ? $post['losses_company'][$i] : NULL;
				$result_l['losses_general_agent'] 		= isset($post['losses_general_agent'][$i]) ? $post['losses_general_agent'][$i] : NULL;
				$result_l['losses_date_of_loss'] 		= isset($post['losses_date_of_loss'][$i]) ? $post['losses_date_of_loss'][$i] : NULL;
				$result_l['losses_fault'] 		= isset($post['losses_fault'][$i]) ? $post['losses_fault'][$i] : NULL;
				$result_l['losses_fault_per'] 		= isset($post['losses_fault_per'][$i]) ? $post['losses_fault_per'][$i] : NULL;
                $result_l['type'] 		= isset($post['losses_type'][$i]) ? $post['losses_type'][$i] : NULL;
				
				$losses_id=$this->input->post('losses_id');				
				
				if(!empty($losses_id[$i])){
						$this->db->where('losses_id',$losses_id[$i]);
						$this->db->update('rq_rqf_quick_quote_losses',$result_l);
					}else{
						$this->db->insert('rq_rqf_quick_quote_losses', $result_l);
						
					}
					$i++;
			 }
		}
		
	}
	public function get_quotes_info($id = '') {
		$this->db->select('q.id,q.status,q.submit_date,q.u_pdf_name,ins.insured_name,ins.insured_middle_name,ins.insured_last_name,ins.dba');   
		$this->db->join('rq_rqf_quick_quote_insured as ins','q.id=ins.quote_id');
		$this->db->from('rq_rqf_quick_quote as q');
		if($id > 0){
			$where_array = array('q.requester_id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result_array();
		
		return $result;
	}

	public function get_quote_id(){			
			$this->db->select('id')->from('rq_rqf_quick_quote');
			$this->db->order_by("id", "desc");
			$this->db->limit(1);			
			$query_result = $this->db->get();
			$result = $query_result->result();
			if(!empty($result)){		
			return $result[0]->id;	 
			}
		
		}
		
	public function broker_info($id = '') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote');
		if($id > 0){
			$where_array = array('id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	public function person_info($id = '') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_person');
		if($id > 0){
			$where_array = array('quote_id' => $id);
			$this->db->where($where_array);
		}
		$this->db->order_by("person_id","desc");
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	
	public function email_info($id = '',$type='') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_email');
		if($id > 0){
			$where_array = array('quote_id' => $id,'type'=>$type,'is_delete'=>0);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	public function phone_info($id = '',$type='') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_phone');
		if($id > 0){
			$where_array = array('quote_id' => $id,'type'=>$type,'is_delete'=>0);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	public function fax_info($id='',$type='') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_fax');
		if($id > 0){
			$where_array = array('quote_id' => $id,'type'=>$type,'is_delete'=>0);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
   	public function insured_info($id='') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_insured');
		if($id > 0){
			$where_array = array('quote_id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	public function ins_info($id=''){
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_garage');
		if($id > 0){
			$where_array = array('quote_id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
	   $result = $query->result_array();
	   if(!empty($result)){
		 foreach($result as $res ){			
            $array2 = array( "phones" => array(), "emails" => array(), "faxs" => array());
            $results[] = array_merge($res, $array2);          
			
			}
	   
		return $results;
	   }
	
	}
	
	
	public function quick_quote_info($id='',$table='',$type=''){
		
		$this->db->select('*');   
		$this->db->from($table);
		if($id > 0){
			$where_array = array('quote_id' => $id,'is_delete'=>0);
			$this->db->where($where_array);
		}
		if(!empty($type)){
			$this->db->where('type',$type);
		}
		$query = $this->db->get();
		
		$result = $query->result();
	    return $result;
		}
	public function quick_quote_info_array($id='',$table='',$type=''){
		
		$this->db->select('*');   
		$this->db->from($table);
		if($id > 0){
			$where_array = array('quote_id' => $id,'is_delete'=>0);
			$this->db->where($where_array);
		}
		if(!empty($type)){
			$this->db->where('type',$type);
		}
		$query = $this->db->get();
		
		$result = $query->result_array();
	    return $result;
		}		
public function quick_quote_vh_info($id='',$table='',$type=''){
	
	$this->db->select('*');   
	$this->db->from($table);
	if($id > 0){
		$where_array = array('quote_id' => $id);
		$this->db->where($where_array);
	}
	if(!empty($type)){
		
		$this->db->where('type',$type);
	}
	$query = $this->db->get();
	$result = $query->result_array();
	$results='';	
	$array1 = array("lessor" => array(), "vh_eqpmnts" => array());	
	 if(!empty($result)){
	  foreach($result as $res){			
	   $array2 = array("lessor" => array(), "vh_eqpmnts" => array());
	   $results[] = array_merge($array2, $res);      
		
		}
		
	  }
	return !empty($results)?$results:$array1;  
	}
	
public function quick_quote_vh_infos($id='',$table='',$type=''){
	
	$this->db->select('*');   
	$this->db->from($table);
	if($id > 0){
		$where_array = array('quote_id' => $id);
		$this->db->where($where_array);
	}
	if(!empty($type)){
		
		$this->db->where('type',$type);
	}
	$query = $this->db->get();
	$result = $query->result_array();
	$results='';	
	$array1 = array("vh_eqpmntss" => array());	
	 if(!empty($result)){
	  foreach($result as $res){			
	   $array2 = array("vh_eqpmntss" => array());
	   $results[] = array_merge($array2, $res);      
		
		}
		
	  }
	return !empty($results)?$results:$array1;  
	}
	
 public function quick_quote_lessor_info($id='',$table='',$type=''){
		$this->db->select('*');   
		$this->db->from($table);
		if($id > 0){
			$where_array = array('quote_id' => $id);
			$this->db->where($where_array);
		}
		if(!empty($type)){
			
			$this->db->where('type',$type);
		}
		$query = $this->db->get();
		$result = $query->result_array();		
		if(!empty($result)){
		foreach($result as $res){			
		   $array2 = array( "emails" => array(), "faxs" => array());
		   $results[] = array_merge($array2, $res);      
		
		  }
			return $results;
		 }
		  
		 
		 
	 }
	 public function get_all_broker()
	 {      
	    
		$this->db->select('quote_id,fistname,middlename,lastname,assigned_agency,person_id');	
		$this->db->distinct('fistname');
		$this->db->group_by('fistname');	
		$query = $this->db->get('rq_rqf_quick_quote_person'); 
		
		$rows = $query->result();
	
		return $rows;
	}
	 public function getSingleperonInfo($quote_id)
	 {
	
		$query = $this->db->get_where('rq_rqf_quick_quote_person',array('quote_id' => $quote_id)); 
		$row = $query->row_array();
		return $row;
	}
	public function getPeronPhone($quote_id)
	{
		$this->db->select();
		$this->db->where('quote_id',$quote_id);
		$this->db->where('type','person');
		$query = $this->db->get('rq_rqf_quick_quote_phone'); 
		$rows = $query->result();
		return $rows;
	}
	public function getPeronFax($quote_id){
		$this->db->select();
		$this->db->where('quote_id',$quote_id);
		$this->db->where('type','person');
		$query = $this->db->get('rq_rqf_quick_quote_fax'); 
		$rows = $query->result();
		return $rows;
	}
	public function getPeronemail($quote_id){
		$this->db->select();
		$this->db->where('quote_id',$quote_id);
		$this->db->where('type','person');
		$query = $this->db->get('rq_rqf_quick_quote_email'); 
		$rows = $query->result();
		return $rows;
	}
	public function is_delete($old_id='',$new_id='',$table='',$table_id='',$ref_id='',$condition='',$parent_id=''){		 
		  if(!empty($old_id)){
			   $result=array();
			   $adresses=$this->get_id_inform($ref_id,$table,$condition,$parent_id);

				 foreach($adresses as $values){
			  
					 $result[]=$values->$table_id;
				  
				 }
			  $flag_set= array_diff($result,$old_id,$new_id);
		    
			  foreach ($flag_set as $Keys=>$vlaues) {
			  
			  if(isset($flag_set)){
			  if($vlaues!=''){
			  
			  $insert_data130['is_delete'] = 1;
			  
			   
			   if(!empty($vlaues)){
				  
				$this->_updateapp1($ref_id, $insert_data130,$table,$vlaues,$table_id);
			  
				     }
				   }
				  }
				}
			  }else{
				
			$result=array();
			$array=$this->get_id_inform($ref_id,$table,$condition,$parent_id);
			  foreach($array as $values){
		  
					 $result[]=$values->$table_id;
				  
				 }
			  
			  $flag_set= array_diff($result,$new_id);
		   
			  foreach ($flag_set as $Keys=>$vlaues) {
			  if($vlaues!=''){
			  
			  $insert_data130['is_delete'] = 1;
			  
			   
			   if(!empty($vlaues)){
				  
				$this->_updateapp1($ref_id, $insert_data130,$table,$vlaues,$table_id);
			  
				   }
			    }
			} 
		 }
      }
	  
	  	public function get_id_inform($id='',$table,$condition='',$parent_id=''){
		
		      $this->db->select('*');
			  $this->db->from($table);
				 if($id!='')
				  {
					$this->db->where('quote_id', $id);
				  }
				   if($condition!='')
				  {
					$this->db->where('type', $condition);
				  }
				  if($parent_id!='')
				  {
					$this->db->where('parent_id', $parent_id);
					  
				  }
			
				   $query = $this->db->get();
		   
				$result = $query->result();

			 return $result;
		
		}
		
	public function _updateapp1($id, $data, $table,$id1,$table_id)
	{
		$this->db->where('quote_id', $id);
		if($table_id!=''){
		    $this->db->where($table_id, $id1);	
			}	
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
	 public function get_quote($id = '') {
		$sql = "select q.*,i.insured_name,i.insured_middle_name,i.insured_last_name,i.dba,i.commodities_haulted  from rq_rqf_quick_quote as q LEFT JOIN rq_rqf_quick_quote_insured as i ON q.id = i.quote_id ";
		
		if($userid = $this->session->userdata('underwriter_id')){
	     //$sql .="WHERE find_in_set($userid,q.assigned_underwriter)";    
		   $sql .="WHERE q.assigned_underwriter LIKE '%$userid%'";    	
		  
			 	if($id > 0) 
				$sql .= " and q.id = ". $id;
		} else if($memberid = $this->session->userdata('member_id'))
		{
			$sql .= "WHERE q.requester_id = '$memberid'";
				if($id > 0) 
				$sql .= " and q.id = ". $id;
		}
		else if($this->session->userdata('admin_id'))
		{
			if($id > 0) 
				$sql .= "WHERE q.id = ". $id;
		}
		
		$query = $this->db->query($sql);
		
		$this->data['quote'] = $query->result();
		  

		return $query->result();
       
    }
	
	 public function rq_members_info($id='',$table=''){
		
		$this->db->select('*');   
		$this->db->from($table);
		if($id > 0){
			$where_array = array('id' => $id);
			$this->db->where($where_array);
		}
	
		$query = $this->db->get();
		
		$result = $query->result();
	    return $result;
		}
	
    public function quick_quote_ver($id='',$table=''){	
	    $this->db->select('*');   
		$this->db->from($table);
		if($id > 0){
			$where_array = array('quote_id' => $id,'status'=>'Released');
			$this->db->where($where_array);
		}
	
		$query = $this->db->get();
		
		$result = $query->result();
		
	    return $result;
	
	}
	public function get_img_name($scope='',$table='',$model=''){
		  
			$this->db->select('*');   
			$this->db->from($table);
			if($scope){
				$where_array = array('catlog_value' => $scope,'status'=>1,'catlog_type'=>$model);
				$this->db->where($where_array);
			}
		
			$query = $this->db->get();
			
			$result = $query->result();
			
			return $result;
		}
		public function get_database_inform6($id='',$table,$id1=''){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		   $this->db->where('quote_id', $id);
		   $this->db->where('is_delete', 0);
     	}
		

        if($id1!='')
	    {
		   $this->db->where('supplement_parent_id', $id1);
     	}

	      $query = $this->db->get();
   
	      $result = $query->result();
	
	return $result;
		
		}
		
	public function get_database_inform5($id='',$table,$id1=''){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		   $this->db->where('quote_id', $id);
		   $this->db->where('is_delete', 0);
     	}
		

        if($id1!='')
	    {
		   $this->db->where('ghi5_parent_id', $id1);
     	}

	      $query = $this->db->get();
   
	      $result = $query->result();
	
	return $result;
		
		}
	
	public function getAgencyBroker($agency_id){
		if($agency_id){
			
			$this->db->select('quote_id,fistname,middlename,lastname,assigned_agency,person_id');	
			$this->db->where('assigned_agency', $agency_id);
			$this->db->distinct('fistname');
			$this->db->group_by('fistname');	
			$query = $this->db->get('rq_rqf_quick_quote_person');
			$rows = $query->result();
			return $rows;
		}
	}
		
}
/* End of file quote_model.php */
/* Location: ./application/models/new_quote_model.php */