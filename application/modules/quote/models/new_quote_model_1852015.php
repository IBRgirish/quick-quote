<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class New_quote_model extends CI_Model {

    public function &__get($key) {
        $CI = & get_instance();
        return $CI->$key;
        $this->load->database();
		$quote_id=NULL;
    }
	
	/**
	* Upload multiple files at time using CI
	* @$images return uploaded files name
	* @$errors return errors during file uploading
	*/
	private function upload_files($file_types='*',$path, $files){
        $config = array(
            'upload_path'   => 'uploads/'.$path,
            'allowed_types' => $file_types,
            'overwrite'     => 1,                       
        );
		
	
        $this->load->library('upload', $config);
        $images = array();       
        foreach ($files['name'] as $key => $image) {
            $_FILES['files[]']['name']= $files['name'][$key];
            $_FILES['files[]']['type']= $files['type'][$key];
            $_FILES['files[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['files[]']['error']= $files['error'][$key];
            $_FILES['files[]']['size']= $files['size'][$key];
            $fileName = preg_replace('/[^a-zA-Z0-9\/_|+ .-]/', '-', $image);
            $images[] = str_replace(' ', '_', $fileName);
            $config['file_name'] = $fileName;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('files[]')) {
                $data = $this->upload->data();
            } else {
               $errors = $this->upload->display_errors();
            }
        }
		
		
        return $images;
    }
	
	
	private function upload_files_multiple($file_types='*',$path, $files){
        $config = array(
            'upload_path'   => 'uploads/'.$path,
            'allowed_types' => $file_types,
            'overwrite'     => 1,                       
        );
	
	
        $this->load->library('upload', $config);
        $images = array();  
		     
        foreach ($files['name'] as $key=>$img) {			
		   foreach ($img as $keys=>$image) {
			   
            $_FILES['files[]']['name']= $files['name'][$key][$keys];
            $_FILES['files[]']['type']= $files['type'][$key][$keys];
            $_FILES['files[]']['tmp_name']= $files['tmp_name'][$key][$keys];
            $_FILES['files[]']['error']= $files['error'][$key][$keys];
            $_FILES['files[]']['size']= $files['size'][$key][$keys];
            $fileName = preg_replace('/[^a-zA-Z0-9\/_|+ .-]/', '-', $image);
            $images[][] = str_replace(' ', '_', $fileName);
            $config['file_name'] = $fileName;
			
            $this->upload->initialize($config);
            if ($this->upload->do_upload('files[]')) {
                $data = $this->upload->data();
            } else {
               $errors = $this->upload->display_errors();
            }
		  }
		}
        return $images;
    }
	
		/**
	* Upload file using CI
	* @$data return uploaded file name
	* @$error return errors during file uploading
	*/
	public function do_upload($file_types='*',$name, $path, $file_name='') {
        $config['upload_path'] = 'uploads/'.$path;
        $config['allowed_types'] = $file_types;
    
		if($file_name){
			$config['file_name'] = $file_name;
		}
      

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload($name)) {
            $error = array( 'error' => $this->upload->display_errors() );
            
            return $error;
        } else {
            $data = array( 'upload_data' => $this->upload->data() );
            return $data['upload_data'];
           
        }
    }	
	public function insert_broker_quote($quote_id=''){
		        $post = $this->input->post();			
		        $result['fistname'] = trim($this->input->post("bro_fistname", true));
				$result['middlename'] = trim($this->input->post("bro_middlename", true));
				$result['lastname'] = trim($this->input->post("bro_lastname", true));
				$result['dba'] = trim($this->input->post("bro_dba", true));
				$result['street'] = trim($this->input->post("bro_Street", true));
				$result['city'] = trim($this->input->post("bro_city", true));
				$result['state'] = trim($this->input->post("bro_state", true));
				$result['county'] = trim($this->input->post("bro_county", true));
				$result['zip'] = trim($this->input->post("bro_zip", true));
				$result['country'] = trim($this->input->post("bro_country", true));
				$result['fax']= trim($this->input->post("in_broker_fax", true));
				$result['special_remark']= trim($this->input->post("bro_special_remark", true));
				
				if(!empty($quote_id)){
						$this->db->where('id',$quote_id);
						$this->db->update('rq_rqf_quick_quote',$result);
					}else{
						$this->db->insert('rq_rqf_quick_quote', $result);
						$quote_id=$this->db->insert_id();
					}
		if (is_array($this->input->post('bro_phone_type'))){
			$i = 0;
			 foreach($this->input->post('bro_phone_type') as $bro_phone_type){
				$result_1['quote_id'] =$quote_id;
				$result_1['phone_type'] = isset($post['bro_phone_type'][$i]) ? $post['bro_phone_type'][$i] : NULL;
				$result_1['code_type'] = isset($post['bro_code_type'][$i]) ? $post['bro_code_type'][$i] : NULL;
				$result_1['area_code'] = isset($post['bro_area_code'][$i]) ? $post['bro_area_code'][$i] : NULL;
				$result_1['prefix'] = isset($post['bro_prefix'][$i]) ? $post['bro_prefix'][$i] : NULL;
				$result_1['suffix'] = isset($post['bro_suffix'][$i]) ? $post['bro_suffix'][$i] : NULL;
				$result_1['pri_country'] = isset($post['bro_pri_country'][$i]) ? $post['bro_pri_country'][$i] : NULL;
				$result_1['type'] = "broker";
				$phone_id=$this->input->post('phone_id');
				$i++;
				if(!empty($phone_id[$i])){
						$this->db->where('id',$phone_id[$i]);
						$this->db->update('rq_rqf_quick_quote_phone',$result_1);
					}else{
						$this->db->insert('rq_rqf_quick_quote_phone', $result_1);
					}
			 }
		}
		
		if (is_array($this->input->post('bro_email_type'))){
			$j = 0;
			 foreach($this->input->post('bro_email_type') as $bro_email_type){
				$result_2['quote_id'] =$quote_id;
				$result_2['email_type'] = isset($post['bro_email_type'][$j]) ? $post['bro_email_type'][$j] : NULL;
				$result_2['email'] = isset($post['bro_email'][$j]) ? $post['bro_email'][$j] : NULL;
				$result_2['priority_email'] = isset($post['bro_priority_email'][$j]) ? $post['bro_priority_email'][$j] : NULL;
				$result_2['type'] = "broker";
				$j++;
				$email_id=$this->input->post('email_id');
				if(!empty($email_id[$j])){
						$this->db->where('email_id',$email_id[$j]);
						$this->db->update('rq_rqf_quick_quote_email',$result_2);
					}else{
						$this->db->insert('rq_rqf_quick_quote_email', $result_2);
					}
			 }
		}
				
		 if (is_array($this->input->post('bro_fax_type'))){
			$k = 0;
			 foreach($this->input->post('bro_fax_type') as $bro_fax_type){
				$result_3['quote_id'] =$quote_id;
				$result_3['fax_type'] = isset($post['bro_fax_type'][$k]) ? $post['bro_fax_type'][$k] : NULL;
				$result_3['code_type_fax'] = isset($post['bro_code_type_fax'][$k]) ? $post['bro_code_type_fax'][$k] : NULL;
				$result_3['area_code_fax'] = isset($post['bro_area_code_fax'][$k]) ? $post['bro_area_code_fax'][$k] : NULL;
				$result_3['prefix_fax'] = isset($post['bro_prefix_fax'][$k]) ? $post['bro_prefix_fax'][$k] : NULL;
				$result_3['suffix_fax'] = isset($post['bro_suffix_fax'][$k]) ? $post['bro_suffix_fax'][$k] : NULL;
				$result_3['country_fax'] = isset($post['bro_country_fax'][$k]) ? $post['bro_country_fax'][$k] : NULL;
				$result_3['type'] = "broker";
				$k++;
				$fax_id=$this->input->post('fax_id');
				
				if(!empty($fax_id[$k])){
						$this->db->where('fax_id',$fax_id[$k]);
						$this->db->update('rq_rqf_quick_quote_fax',$result_3);
					}else{
						$this->db->insert('rq_rqf_quick_quote_fax', $result_3);
					}
			 }
		}
		
	}
	
	
		public function insert_person_quote($quote_id=''){		
				$post = $this->input->post();
				$quote_id=$this->get_quote_id();
				$person_id="person_id";
				$result_p['quote_id'] =$quote_id;	 				
		        $result_p['fistname'] = trim($this->input->post("per_fistname", true));
				$result_p['middlename'] = trim($this->input->post("per_middlename", true));
				$result_p['lastname'] = trim($this->input->post("per_lastname", true));				
				$result_p['street'] = trim($this->input->post("per_Street", true));
				$result_p['city'] = trim($this->input->post("per_city", true));
				$result_p['state'] = trim($this->input->post("per_state", true));
				$result_p['county'] = trim($this->input->post("per_county", true));
				$result_p['zip'] = trim($this->input->post("per_zip", true));
				$result_p['country'] = trim($this->input->post("per_pri_country", true));
				$result_p['fax']= trim($this->input->post("in_per_fax", true));
				$result_p['special_remark']= trim($this->input->post("per_special_remark", true));
				$person_id=$this->input->post("person_id");
				if(!empty($person_id)){
						$this->db->where('person_id',$person_id);
						$this->db->update('rq_rqf_quick_quote_person',$result_p);
					}else{
						$this->db->insert('rq_rqf_quick_quote_person', $result_p);
						
					}
					
		if (is_array($this->input->post('per_phone_type'))){
			
		 $i = 0;
			 foreach($this->input->post('per_phone_type') as $per_phone_type){			
				$result_p_1['quote_id'] =$quote_id;
				$result_p_1['phone_type'] = isset($post['per_phone_type'][$i]) ? $post['per_phone_type'][$i] : NULL;
				$result_p_1['code_type'] = isset($post['per_code_type'][$i]) ? $post['per_code_type'][$i] : NULL;
				$result_p_1['area_code'] = isset($post['per_area_code'][$i]) ? $post['per_area_code'][$i] : NULL;
				$result_p_1['prefix'] = isset($post['per_prefix'][$i]) ? $post['per_prefix'][$i] : NULL;
				$result_p_1['suffix'] = isset($post['per_suffix'][$i]) ? $post['per_suffix'][$i] : NULL;
				$result_p_1['pri_country'] = isset($post['per_pri_country'][$i]) ? $post['per_pri_country'][$i] : NULL;
				$result_p_1['type'] = "person";
				
				$phone_id=$this->input->post('per_phone_id');
				$i++;
				
				if(!empty($phone_id[$i])){
						$this->db->where('id',$phone_id[$i]);
						$this->db->update('rq_rqf_quick_quote_phone',$result_p_1);
					}else{
						$this->db->insert('rq_rqf_quick_quote_phone', $result_p_1);
					}
			 }
		}
		
		if (is_array($this->input->post('per_email_type'))){
			$j = 0;
			 foreach($this->input->post('per_email_type') as $per_email_type){
				$result_p_2['quote_id'] =$quote_id;
				$result_p_2['email_type'] = isset($post['per_email_type'][$j]) ? $post['per_email_type'][$j] : NULL;
				$result_p_2['email'] = isset($post['per_email'][$j]) ? $post['per_email'][$j] : NULL;
				$result_p_2['priority_email'] = isset($post['per_priority_email'][$j]) ? $post['per_priority_email'][$j] : NULL;
				$result_p_2['type'] = "person";
				$j++;
				$phone_id=$this->input->post('per_email_id');
				
				if(!empty($email_id[$j])){
						$this->db->where('email_id',$email_id[$j]);
						$this->db->update('rq_rqf_quick_quote_email',$result_p_2);
					}else{
						$this->db->insert('rq_rqf_quick_quote_email', $result_p_2);
					}
			 }
		}
				
		 if (is_array($this->input->post('per_fax_type'))){
			$k = 0;
			 foreach($this->input->post('per_fax_type') as $per_fax_type){
				$result_p_3['quote_id'] =$quote_id;
				$result_p_3['fax_type'] = isset($post['per_fax_type'][$k]) ? $post['per_fax_type'][$k] : NULL;
				$result_p_3['code_type_fax'] = isset($post['per_code_type_fax'][$k]) ? $post['per_code_type_fax'][$k] : NULL;
				$result_p_3['area_code_fax'] = isset($post['per_area_code_fax'][$k]) ? $post['per_area_code_fax'][$k] : NULL;
				$result_p_3['prefix_fax'] = isset($post['per_prefix_fax'][$k]) ? $post['per_prefix_fax'][$k] : NULL;
				$result_p_3['suffix_fax'] = isset($post['per_suffix_fax'][$k]) ? $post['per_suffix_fax'][$k] : NULL;
				$result_p_3['country_fax'] = isset($post['per_country_fax'][$k]) ? $post['per_country_fax'][$k] : NULL;
				$result_p_3['type'] = "person";
				$k++;
				$phone_id=$this->input->post('per_fax_id');
				
				if(!empty($fax_id[$k])){
						$this->db->where('fax_id',$fax_id[$k]);
						$this->db->update('rq_rqf_quick_quote_fax',$result_p_3);
					}else{
						$this->db->insert('rq_rqf_quick_quote_fax', $result_p_3);
					}
			 }
		}
		
	}
	
	public function insert_insured_quote($quote_id=''){		     		
				$post = $this->input->post();	
				$quote_id=$this->get_quote_id(); 
				$result_i['quote_id']= $quote_id;				
		        $result_i['in_this_is'] = trim($this->input->post("in_this_is", true));
				$result_i['insured_name'] = trim($this->input->post("insured_name", true));
				$result_i['insured_middle_name'] = trim($this->input->post("insured_middle_name", true));				
				$result_i['insured_last_name'] = trim($this->input->post("insured_last_name", true));
				$result_i['dba'] = trim($this->input->post("dba", true));
				$result_i['in_address'] = trim($this->input->post("in_address", true));
				$result_i['in_city'] = trim($this->input->post("in_city", true));
				$result_i['in_state'] = trim($this->input->post("in_state", true));
				$result_i['in_county'] = trim($this->input->post("in_county", true));
				$result_i['in_zip']= trim($this->input->post("in_zip", true));
				$result_i['in_country']= trim($this->input->post("in_country", true));
				$result_i['in_garag_in']= trim($this->input->post("in_garag_in", true));
				$result_i['in_garag_fax']= $this->input->post("in_garag_faxs");
				$result_i['nature_of_business']= trim($this->input->post("nature_of_business", true));
				$result_i['nature_of_business_other']= trim($this->input->post("nature_of_business_other", true));
				$commodities_haulted= $this->input->post("commodities_haulted");
				$result_i['commodities_haulted']=!empty($commodities_haulted) ? implode(',',$commodities_haulted) :'';				
				$result_i['commodities_haulted_other']= trim($this->input->post("commodities_haulted_other", true));
				$result_i['business_years']=trim($this->input->post("business_years", true));
				$result_i['terms_name']=trim($this->input->post("terms_name", true));
				$result_i['terms_other']=trim($this->input->post("terms_other", true));
				$result_i['terms_from']=trim($this->input->post("terms_from", true));
				$result_i['terms_to']=trim($this->input->post("terms_to", true));
				$result_i['filings_need']=trim($this->input->post("filings_need", true));
				$insured_id=$this->input->post("insured_id");
				
				if(!empty($insured_id)){
						$this->db->where('insured_id',$insured_id);
						$this->db->update('rq_rqf_quick_quote_insured',$result_i);
					}else{
						$this->db->insert('rq_rqf_quick_quote_insured', $result_i);
						
					}
					
		if (is_array($this->input->post('insured_phone_type'))){
			
		 $i = 0;
			 foreach($this->input->post('insured_phone_type') as $insured_phone_type){			
				$result_i_1['quote_id'] =$quote_id;
				$result_i_1['phone_type'] = isset($post['insured_phone_type'][$i]) ? $post['insured_phone_type'][$i] : NULL;
				$result_i_1['code_type'] = isset($post['insured_code_type'][$i]) ? $post['insured_code_type'][$i] : NULL;
				$result_i_1['area_code'] = isset($post['insured_area_code'][$i]) ? $post['insured_area_code'][$i] : NULL;
				$result_i_1['prefix'] = isset($post['insured_prefix'][$i]) ? $post['insured_prefix'][$i] : NULL;
				$result_i_1['suffix'] = isset($post['insured_suffix'][$i]) ? $post['insured_suffix'][$i] : NULL;
				$result_i_1['pri_country'] = isset($post['insured_pri_country'][$i]) ? $post['insured_pri_country'][$i] : NULL;
				$result_i_1['type'] = "insured";
				
				$phone_id=$this->input->post('insured_phone_id');
				$i++;
				
				if(!empty($phone_id[$i])){
						$this->db->where('id',$phone_id[$i]);
						$this->db->update('rq_rqf_quick_quote_phone',$result_i_1);
					}else{
						$this->db->insert('rq_rqf_quick_quote_phone', $result_i_1);
					}
			 }
		}
		
		if (is_array($this->input->post('insured_email_type'))){
			$j = 0;
			 foreach($this->input->post('insured_email_type') as $insured_email_type){
				$result_i_2['quote_id'] =$quote_id;
				$result_i_2['email_type'] = isset($post['insured_email_type'][$j]) ? $post['insured_email_type'][$j] : NULL;
				$result_i_2['email'] = isset($post['insured_email'][$j]) ? $post['insured_email'][$j] : NULL;
				$result_i_2['priority_email'] = isset($post['insured_priority_email'][$j]) ? $post['insured_priority_email'][$j] : NULL;
				$result_i_2['type'] = "insured";
				$email_id=$this->input->post('insured_email_id');
				$j++;
				if(!empty($email_id[$j])){
						$this->db->where('email_id',$email_id[$j]);
						$this->db->update('rq_rqf_quick_quote_email',$result_i_2);
					}else{
						$this->db->insert('rq_rqf_quick_quote_email', $result_i_2);
					}
			 }
		}
				
		 if (is_array($this->input->post('insured_fax_type'))){
			$k = 0;
			 foreach($this->input->post('insured_fax_type') as $insured_fax_type){
				$result_i_3['quote_id'] =$quote_id;
				$result_i_3['fax_type'] = isset($post['insured_fax_type'][$k]) ? $post['insured_fax_type'][$k] : NULL;
				$result_i_3['code_type_fax'] = isset($post['insured_code_type_fax'][$k]) ? $post['insured_code_type_fax'][$k] : NULL;
				$result_i_3['area_code_fax'] = isset($post['insured_area_code_fax'][$k]) ? $post['insured_area_code_fax'][$k] : NULL;
				$result_i_3['prefix_fax'] = isset($post['insured_prefix_fax'][$k]) ? $post['insured_prefix_fax'][$k] : NULL;
				$result_i_3['suffix_fax'] = isset($post['insured_suffix_fax'][$k]) ? $post['insured_suffix_fax'][$k] : NULL;
				$result_i_3['country_fax'] = isset($post['insured_country_fax'][$k]) ? $post['insured_country_fax'][$k] : NULL;
				$result_i_3['type'] = "insured";
				$fax_id=$this->input->post('insured_fax_id');
				$k++;
				if(!empty($fax_id[$k])){
						$this->db->where('fax_id',$fax_id[$k]);
						$this->db->update('rq_rqf_quick_quote_fax',$result_i_3);
					}else{
						$this->db->insert('rq_rqf_quick_quote_fax', $result_i_3);
					}
			 }
		}
		
	
		
		if (is_array($this->input->post('in_ga_sequence'))){			
		 $i = 0;
			 foreach($this->input->post('in_ga_sequence') as $in_ga_sequence){			
				$result_ga['quote_id'] =$quote_id;
				$result_ga['in_ga_sequence'] = isset($post['in_ga_sequence'][$i]) ? $post['in_ga_sequence'][$i] : NULL;
				$result_ga['in_ga_address'] = isset($post['in_ga_address'][$i]) ? $post['in_ga_address'][$i] : NULL;
				$result_ga['in_ga_city'] = isset($post['in_ga_city'][$i]) ? $post['in_ga_city'][$i] : NULL;
				$result_ga['in_ga_state'] = isset($post['in_ga_state'][$i]) ? $post['in_ga_state'][$i] : NULL;
				$result_ga['in_ga_county'] = isset($post['in_ga_county'][$i]) ? $post['in_ga_county'][$i] : NULL;
				$result_ga['in_ga_zip'] = isset($post['in_ga_zip'][$i]) ? $post['in_ga_zip'][$i] : NULL;
				$result_ga['in_ga_country'] = isset($post['in_ga_country'][$i]) ? $post['in_ga_country'][$i] : NULL;
				$result_ga['in_garag_fax']=isset($post['in_garag_fax'][$i]) ? $post['in_garag_fax'][$i] : NULL;
				$result_ga['type']=isset($post['vh_garage_type'][$i]) ? $post['vh_garage_type'][$i] : NULL;
							
				$in_ga_id=$this->input->post('in_ga_id');
				$i++;
				
				if(!empty($in_ga_id[$i])){
						$this->db->where('in_ga_id',$in_ga_id[$i]);
						$this->db->update('rq_rqf_quick_quote_garage',$result_ga);
					}else{
						$this->db->insert('rq_rqf_quick_quote_garage', $result_ga);
						$parent_id=	$this->db->insert_id();
					}
			 }
		}
		
	if (is_array($this->input->post('ins_phone_type'))){
			
		 $i = 0;
			 foreach($this->input->post('ins_phone_type') as $insured_phone_type){			
				$result_ga_1['quote_id'] =$quote_id;
				$result_ga_1['phone_type'] = isset($post['ins_phone_type'][$i]) ? $post['ins_phone_type'][$i] : NULL;
				$result_ga_1['code_type'] = isset($post['ins_code_type'][$i]) ? $post['ins_code_type'][$i] : NULL;
				$result_ga_1['area_code'] = isset($post['ins_area_code'][$i]) ? $post['ins_area_code'][$i] : NULL;
				$result_ga_1['prefix'] = isset($post['ins_prefix'][$i]) ? $post['ins_prefix'][$i] : NULL;
				$result_ga_1['suffix'] = isset($post['ins_suffix'][$i]) ? $post['ins_suffix'][$i] : NULL;
				$result_ga_1['pri_country'] = isset($post['ins_pri_country'][$i]) ? $post['ins_pri_country'][$i] : NULL;
				$result_ga_1['type'] = "ins";
				$result_ga_1['parent_id'] = $parent_id;
				
				$phone_id=$this->input->post('ins_phone_id');
				$i++;
				
				if(!empty($phone_id[$i])){
						$this->db->where('id',$phone_id[$i]);
						$this->db->update('rq_rqf_quick_quote_phone',$result_ga_1);
					}else{
						$this->db->insert('rq_rqf_quick_quote_phone', $result_ga_1);
					}
			 }
		}
		
		if (is_array($this->input->post('ins_email_type'))){
			$j = 0;
			 foreach($this->input->post('ins_email_type') as $ins_email_type){
				$result_ga_2['quote_id'] =$quote_id;
				$result_ga_2['email_type'] = isset($post['ins_email_type'][$j]) ? $post['ins_email_type'][$j] : NULL;
				$result_ga_2['email'] = isset($post['ins_email'][$j]) ? $post['ins_email'][$j] : NULL;
				$result_ga_2['priority_email'] = isset($post['ins_priority_email'][$j]) ? $post['ins_priority_email'][$j] : NULL;
				$result_ga_2['type'] = "ins";
				$result_ga_2['parent_id'] = $parent_id;
				$email_id=$this->input->post('ins_email_id');
				$j++;
				if(!empty($email_id[$j])){
						$this->db->where('email_id',$email_id[$j]);
						$this->db->update('rq_rqf_quick_quote_email',$result_ga_2);
					}else{
						$this->db->insert('rq_rqf_quick_quote_email', $result_ga_2);
					}
			 }
		}
				
		 if (is_array($this->input->post('ins_fax_type'))){
			$k = 0;
			 foreach($this->input->post('ins_fax_type') as $ins_fax_type){
				$result_ga_3['quote_id'] =$quote_id;
				$result_ga_3['fax_type'] = isset($post['ins_fax_type'][$k]) ? $post['ins_fax_type'][$k] : NULL;
				$result_ga_3['code_type_fax'] = isset($post['ins_code_type_fax'][$k]) ? $post['ins_code_type_fax'][$k] : NULL;
				$result_ga_3['area_code_fax'] = isset($post['ins_area_code_fax'][$k]) ? $post['ins_area_code_fax'][$k] : NULL;
				$result_ga_3['prefix_fax'] = isset($post['ins_prefix_fax'][$k]) ? $post['ins_prefix_fax'][$k] : NULL;
				$result_ga_3['suffix_fax'] = isset($post['ins_suffix_fax'][$k]) ? $post['ins_suffix_fax'][$k] : NULL;
				$result_ga_3['country_fax'] = isset($post['ins_country_fax'][$k]) ? $post['ins_country_fax'][$k] : NULL;
				$result_ga_3['type'] = "ins";
				$result_ga_3['parent_id'] = $parent_id;
				$fax_id=$this->input->post('ins_fax_id');
				$k++;
				if(!empty($fax_id[$k])){
						$this->db->where('fax_id',$fax_id[$k]);
						$this->db->update('rq_rqf_quick_quote_fax',$result_ga_3);
					}else{
						$this->db->insert('rq_rqf_quick_quote_fax', $result_ga_3);
					}
			 }
		}
	   if (is_array($this->input->post('filing_type'))){
		  $k = 0;
		   foreach($this->input->post('filing_type') as $filing_type){
			  $result_f['quote_id'] =$quote_id;
			  $result_f['filing_type'] = isset($post['filing_type'][$k]) ? $post['filing_type'][$k] : NULL;
			  $result_f['filing_numbers'] = isset($post['filing_numbers'][$k]) ? $post['filing_numbers'][$k] : NULL;
			
			  $filing_id=$this->input->post('filing_id');
			  $k++;
			  if(!empty($filing_id[$k])){
					  $this->db->where('filing_id',$filing_id[$k]);
					  $this->db->update('rq_rqf_quick_quote_filing',$result_f);
				  }else{
					  $this->db->insert('rq_rqf_quick_quote_filing', $result_f);
				  }
		   }
	  }
	  if (is_array($this->input->post('in_fi_filing_number'))){
			$k = 0;
			 foreach($this->input->post('in_fi_filing_number') as $ins_fax_type){
				$result_fi['quote_id'] =$quote_id;
				$result_fi['in_fi_filing_number'] = isset($post['in_fi_filing_number'][$k]) ? $post['in_fi_filing_number'][$k] : NULL;
				$result_fi['in_fi_dba'] = isset($post['in_fi_dba'][$k]) ? $post['in_fi_dba'][$k] : NULL;
				$result_fi['in_fi_address'] = isset($post['in_fi_address'][$k]) ? $post['in_fi_address'][$k] : NULL;
				$result_fi['in_fi_city'] = isset($post['in_fi_city'][$k]) ? $post['in_fi_city'][$k] : NULL;
				$result_fi['in_fi_state'] = isset($post['in_fi_state'][$k]) ? $post['in_fi_state'][$k] : NULL;
				$result_fi['in_fi_zip'] = isset($post['in_fi_zip'][$k]) ? $post['in_fi_zip'][$k] : NULL;
								
				$in_fi_id=$this->input->post('in_fi_id');
				$k++;
				if(!empty($in_fi_id[$k])){
						$this->db->where('fax_id',$in_fi_id[$k]);
						$this->db->update('rq_rqf_quick_quote_filings',$result_fi);
					}else{
						$this->db->insert('rq_rqf_quick_quote_filings', $result_fi);
					}
			 }
		}
   }
   
   
   	public function insert_coverage_quote($quote_id=''){		     		
				$post = $this->input->post();	
				$quote_id=$this->get_quote_id(); 
				$file_types = $this->config->item('doc_image_file_types');
				$result_c['quote_id']= $quote_id;
				$coverage= $this->input->post("coverage");				
		        $result_c['coverage'] =!empty($coverage)?implode(',',$coverage):'';				
				$vh_type= $this->input->post("vh_type");				
		        $result_c['vh_type'] =!empty($vh_type)?implode(',',$vh_type):'';				
				$result_c['truck_no'] = trim($this->input->post("truck_no", true));
				$result_c['tractor_no'] = trim($this->input->post("tractor_no", true));				
				$result_c['trailer_no'] = trim($this->input->post("trailer_no", true));
				$result_c['other_no'] = trim($this->input->post("other_no", true));
				$result_c['other_vh_name'] = trim($this->input->post("other_vh_name", true));
				$coverage_id = trim($this->input->post("coverage_id", true));
					if(!empty($coverage_id)){
						$this->db->where('coverage_id',$coverage_id);
						$this->db->update('rq_rqf_quick_quote_coverage',$result_c);
					}else{
						$this->db->insert('rq_rqf_quick_quote_coverage', $result_c);
					}
			 
	if (is_array($this->input->post('vehicle_year_vh'))){			
		 $i = 0;
			 foreach($this->input->post('vehicle_year_vh') as $vehicle_year_vh){			
				$result_c_1['quote_id'] =$quote_id;
				$result_c_1['vehicle_year_vh'] = isset($post['vehicle_year_vh'][$i]) ? $post['vehicle_year_vh'][$i] : NULL;
				$result_c_1['make_truck'] = isset($post['make_truck'][$i]) ? $post['make_truck'][$i] : NULL;
				$result_c_1['model_tuck'] = isset($post['model_tuck'][$i]) ? $post['model_tuck'][$i] : NULL;
				$result_c_1['gvw_vh'] = isset($post['gvw_vh'][$i]) ? $post['gvw_vh'][$i] : NULL;
				$result_c_1['vin_vh'] = isset($post['vin_vh'][$i]) ? $post['vin_vh'][$i] : NULL;
				$result_c_1['truck_number_of_axles'] = isset($post['truck_number_of_axles'][$i]) ? $post['truck_number_of_axles'][$i] : NULL;
				$result_c_1['vh_salvage'] = isset($post['vh_salvage'][$i]) ? $post['vh_salvage'][$i] : NULL;
				$result_c_1['truck_radius_of_operation']=isset($post['truck_radius_of_operation'][$i]) ? $post['truck_radius_of_operation'][$i] : NULL;
				$result_c_1['vh_nature_of_business']=isset($post['vh_nature_of_business'][$i]) ? $post['vh_nature_of_business'][$i] : NULL;
				
				$result_c_1['truck_trailer_owned_vehicle']=isset($post['truck_trailer_owned_vehicle'][$i]) ? $post['truck_trailer_owned_vehicle'][$i] : NULL;
				$result_c_1['vh_equipment_pull']=isset($post['vh_equipment_pull'][$i]) ? $post['vh_equipment_pull'][$i] : NULL;	
				$result_c_1['vh_lib_coverage']= isset($post['t_vh_lib_coverage'][$i]) ? $post['t_vh_lib_coverage'][$i] : NULL;
               
			    $result_c_1['liability_vh'] 	= isset($post['t_liability_vh'][$i]) ? $post['t_liability_vh'][$i] : NULL;
                $result_c_1['liability_ded_vh'] = isset($post['t_liability_ded_vh'][$i]) ? $post['t_liability_ded_vh'][$i] : NULL;
               	$result_c_1['truck_um'] 		= isset($post['t_truck_um'][$i]) ? $post['t_truck_um'][$i] : NULL;
				$result_c_1['truck_pip'] 	= isset($post['t_truck_pip'][$i]) ? $post['t_truck_pip'][$i] : NULL;
				$result_c_1['truck_uim'] 	= isset($post['t_truck_uim'][$i]) ? $post['t_truck_uim'][$i] : NULL;
				$result_c_1['vh_pd_coverage']= isset($post['t_vh_pd_coverage'][$i]) ? $post['t_vh_pd_coverage'][$i] : NULL;
				$result_c_1['pd_vh'] 		= isset($post['t_pd_vh'][$i]) ? $post['t_pd_vh'][$i] : NULL;
				$result_c_1['ph_ded_vh'] 	= isset($post['t_ph_ded_vh'][$i]) ? $post['t_ph_ded_vh'][$i] : NULL;
				$result_c_1['vh_pd_remark'] 	= isset($post['t_vh_pd_remark'][$i]) ? $post['t_vh_pd_remark'][$i] : NULL;
				$result_c_1['pd_want'] 		= isset($post['t_pd_want'][$i]) ? $post['t_pd_want'][$i] : NULL;
				$result_c_1['vh_cargo_coverage'] = isset($post['t_vh_cargo_coverage'][$i]) ? $post['t_vh_cargo_coverage'][$i] : NULL;
				$result_c_1['cargo_vh'] 		= isset($post['t_cargo_vh'][$i]) ? $post['t_cargo_vh'][$i] : NULL;
				$result_c_1['cargo_ded_vh'] 	= isset($post['t_cargo_ded_vh'][$i]) ? $post['t_cargo_ded_vh'][$i] : NULL;
				$result_c_1['refrigerated_breakdown'] = isset($post['t_refrigerated_breakdown'][$i]) ? $post['t_refrigerated_breakdown'][$i] : NULL;
				$result_c_1['deductible'] 	= isset($post['t_deductible'][$i]) ? $post['t_deductible'][$i] : NULL;
				$result_c_1['commodities_haulted'] 	= isset($post['t_commodities_haulted_truck'][$i]) ? $post['t_commodities_haulted_truck'][$i] : NULL;
				
				$t_vehicle_file = isset($_FILES['t_vehicle_file']) ? $this->upload_files($file_types,'vehicle_file', $_FILES['t_vehicle_file']) : '';
			   
				$result_c_1['vehicle_file'] = ($t_vehicle_file) ? is_array($t_vehicle_file) ? implode(',', $t_vehicle_file) : NULL : NULL;
				$result_c_1['type'] = isset($post['vehicle_type'][$i]) ? $post['vehicle_type'][$i] : NULL;	
				
				
				$vehicle_id=$this->input->post('vehicle_id');
				$i++;
				
				if(!empty($vehicle_id[$i])){
						$this->db->where('vehicle_id',$vehicle_id[$i]);
						$this->db->update('rq_rqf_quick_quote_vh',$result_c_1);
					}else{
						$this->db->insert('rq_rqf_quick_quote_vh', $result_c_1);
						//$parent_id=	$this->db->insert_id();
					}
					
					print_r($result_c_1); die();
			 }
		}
		
		
		if (is_array($this->input->post('vh_eqp_type'))){			
		 $i = 0;
			 foreach($this->input->post('vh_eqp_type') as $vh_eqp_type){			
				$result_ca['quote_id'] 		= $quote_id;
				$result_ca['vh_eqp_type'] 	= isset($post['vh_eqp_type'][$i]) ? $post['vh_eqp_type'][$i] : NULL;
				$result_ca['vh_eqp_year'] 	= isset($post['vh_eqp_year'][$i]) ? $post['vh_eqp_year'][$i] : NULL;
				$result_ca['make_model_vh'] = isset($post['make_model_vh'][$i]) ? $post['make_model_vh'][$i] : NULL;
				$result_ca['vh_eqp_model'] 	= isset($post['vh_eqp_model'][$i]) ? $post['vh_eqp_model'][$i] : NULL;
				$result_ca['gvw_vh'] 		= isset($post['gvw_vh'][$i]) ? $post['gvw_vh'][$i] : NULL;
                $result_ca['vh_eqp_vin'] 	= isset($post['vh_eqp_vin'][$i]) ? $post['vh_eqp_vin'][$i] : NULL;
				$result_ca['vh_eqp_salvage']= isset($post['vh_eqp_salvage'][$i]) ? $post['vh_eqp_salvage'][$i] : NULL;
                $result_ca['vh_lib_coverage']= isset($post['vh_lib_coverage'][$i]) ? $post['vh_lib_coverage'][$i] : NULL;
                $result_ca['liability_vh'] 	= isset($post['liability_vh'][$i]) ? $post['liability_vh'][$i] : NULL;
                $result_ca['liability_ded_vh'] = isset($post['liability_ded_vh'][$i]) ? $post['liability_ded_vh'][$i] : NULL;
                $result_ca['truck_um'] 		= isset($post['truck_um'][$i]) ? $post['truck_um'][$i] : NULL;
				$result_ca['truck_pip'] 	= isset($post['truck_pip'][$i]) ? $post['truck_pip'][$i] : NULL;
				$result_ca['truck_uim'] 	= isset($post['truck_uim'][$i]) ? $post['truck_uim'][$i] : NULL;
				$result_ca['vh_pd_coverage']= isset($post['vh_pd_coverage'][$i]) ? $post['vh_pd_coverage'][$i] : NULL;
				$result_ca['pd_vh'] 		= isset($post['pd_vh'][$i]) ? $post['pd_vh'][$i] : NULL;
				$result_ca['ph_ded_vh'] 	= isset($post['ph_ded_vh'][$i]) ? $post['ph_ded_vh'][$i] : NULL;
				$result_ca['vh_pd_remark'] 	= isset($post['vh_pd_remark'][$i]) ? $post['vh_pd_remark'][$i] : NULL;
				$result_ca['pd_want'] 		= isset($post['pd_want'][$i]) ? $post['pd_want'][$i] : NULL;
				$result_ca['vh_cargo_coverage'] = isset($post['vh_cargo_coverage'][$i]) ? $post['vh_cargo_coverage'][$i] : NULL;
				$result_ca['cargo_vh'] 		= isset($post['cargo_vh'][$i]) ? $post['cargo_vh'][$i] : NULL;
				$result_ca['cargo_ded_vh'] 	= isset($post['cargo_ded_vh'][$i]) ? $post['cargo_ded_vh'][$i] : NULL;
				$result_ca['refrigerated_breakdown'] = isset($post['refrigerated_breakdown'][$i]) ? $post['refrigerated_breakdown'][$i] : NULL;
				$result_ca['deductible'] 	= isset($post['deductible'][$i]) ? $post['deductible'][$i] : NULL;
				$result_ca['commodities_haulted'] 	= isset($post['commodities_haulted_truck'][$i]) ? $post['commodities_haulted_truck'][$i] : NULL;			
				$vehicle_file = isset($_FILES['vehicle_file']) ? $this->upload_files_multiple($file_types,'vehicle_file', $_FILES['vehicle_file']) : '';			    
				$result_ca['vehicle_file'] = ($vehicle_file[$i]) ? is_array($vehicle_file[$i]) ? implode(',', $vehicle_file[$i]) : NULL : NULL;
				
				$result_ca['type'] = isset($post['vh_eqp_types'][$i]) ? $post['vh_eqp_types'][$i] : NULL;			
				$vh_eqp_id=$this->input->post('vh_eqp_id');
				$i++;
				
				if(!empty($vh_eqp_id[$i])){
						$this->db->where('vh_eqp_id',$vh_eqp_id[$i]);
						$this->db->update('rq_rqf_quick_quote_vh_eqp',$result_ca);
					}else{
						$this->db->insert('rq_rqf_quick_quote_vh_eqp', $result_ca);
						
					}
			 }
		}
		
		
	if (is_array($this->input->post('vh_ls_name'))){			
		 $i = 0;
			 foreach($this->input->post('vh_ls_name') as $vh_ls_name){			
				$result_ca_1['quote_id'] =$quote_id;
				$result_ca_1['vh_ls_name'] = isset($post['vh_ls_name'][$i]) ? $post['vh_ls_name'][$i] : NULL;
				$result_ca_1['vh_ls_m_name'] = isset($post['vh_ls_m_name'][$i]) ? $post['vh_ls_m_name'][$i] : NULL;
				$result_ca_1['vh_ls_l_name'] = isset($post['vh_ls_l_name'][$i]) ? $post['vh_ls_l_name'][$i] : NULL;
				$result_ca_1['vh_ls_dba'] = isset($post['vh_ls_dba'][$i]) ? $post['vh_ls_dba'][$i] : NULL;
				$result_ca_1['vh_ls_address'] = isset($post['vh_ls_address'][$i]) ? $post['vh_ls_address'][$i] : NULL;
                $result_ca_1['vh_ls_city'] = isset($post['vh_ls_city'][$i]) ? $post['vh_ls_city'][$i] : NULL;
				$result_ca_1['vh_ls_state'] = isset($post['vh_ls_state'][$i]) ? $post['vh_ls_state'][$i] : NULL;
                $result_ca_1['vh_ls_county'] = isset($post['vh_ls_county'][$i]) ? $post['vh_ls_county'][$i] : NULL;
                $result_ca_1['vh_ls_zip'] = isset($post['vh_ls_zip'][$i]) ? $post['vh_ls_zip'][$i] : NULL;
                $result_ca_1['vh_ls_country'] = isset($post['vh_ls_country'][$i]) ? $post['vh_ls_country'][$i] : NULL;
                
				$result_ca_1['vh_ls_ph_type'] = isset($post['vh_ls_ph_type'][$i]) ? $post['vh_ls_ph_type'][$i] : NULL;
				$result_ca_1['vh_ls_country_code'] = isset($post['vh_ls_country_code'][$i]) ? $post['vh_ls_country_code'][$i] : NULL;
				$result_ca_1['vh_ls_areacode'] = isset($post['vh_ls_areacode'][$i]) ? $post['vh_ls_areacode'][$i] : NULL;
				$result_ca_1['vh_ls_prefix'] = isset($post['vh_ls_prefix'][$i]) ? $post['vh_ls_prefix'][$i] : NULL;
				$result_ca_1['vh_ls_suffixe'] = isset($post['vh_ls_suffixe'][$i]) ? $post['vh_ls_suffixe'][$i] : NULL;
				$result_ca_1['pri_country'] = isset($post['vh_ls_ph_pri_sequence'][$i]) ? $post['vh_ls_ph_pri_sequence'][$i] : NULL;
				$result_ca_1['in_truck_fax'] = isset($post['in_truck_fax'][$i]) ? $post['in_truck_fax'][$i] : NULL;
				
				$result_ca_1['type'] = isset($post['vh_type'][$i]) ? $post['vh_type'][$i] : NULL;			
				$vh_ls_id=$this->input->post('vh_ls_id');
				$i++;
				
				if(!empty($vh_ls_id[$i])){
						$this->db->where('vh_ls_id',$vh_ls_id[$i]);
						$this->db->update('rq_rqf_quick_quote_vh_garage',$result_ca_1);
					}else{
						$this->db->insert('rq_rqf_quick_quote_vh_garage', $result_ca_1);
						$parent_id=	$this->db->insert_id();
					}
			 }
		}
		
			
	
				
		if (is_array($this->input->post('vh_ls_email_type'))){
			$j = 0;
			 foreach($this->input->post('vh_ls_email_type') as $vh_ls_email_type){
				$result_ca_2['quote_id'] =$quote_id;
				$result_ca_2['email_type'] = isset($post['vh_ls_email_type'][$j]) ? $post['vh_ls_email_type'][$j] : NULL;
				$result_ca_2['email'] = isset($post['vh_ls_email'][$j]) ? $post['vh_ls_email'][$j] : NULL;
				$result_ca_2['priority_email'] = isset($post['vh_ls_em_pri_sequence'][$j]) ? $post['vh_ls_em_pri_sequence'][$j] : NULL;
				$result_ca_2['type'] = isset($post['vh_em_type'][$j]) ? $post['vh_em_type'][$j] : NULL;
				$result_ca_2['parent_id'] = $parent_id;
				$email_id=$this->input->post('vh_ls_email_id');
				$j++;
				if(!empty($email_id[$j])){
						$this->db->where('email_id',$email_id[$j]);
						$this->db->update('rq_rqf_quick_quote_email',$result_ca_2);
					}else{
						$this->db->insert('rq_rqf_quick_quote_email', $result_ca_2);
					}
			 }
		}
				
		 if (is_array($this->input->post('vh_ls_fx_type'))){
			$k = 0;
			 foreach($this->input->post('vh_ls_fx_type') as $vh_ls_fx_type){
				$result_ca_3['quote_id'] =$quote_id;
				$result_ca_3['fax_type'] = isset($post['vh_ls_fx_type'][$k]) ? $post['vh_ls_fx_type'][$k] : NULL;
				$result_ca_3['code_type_fax'] = isset($post['vh_ls_fx_country_code'][$k]) ? $post['vh_ls_fx_country_code'][$k] : NULL;
				$result_ca_3['area_code_fax'] = isset($post['vh_ls_fx_areacode'][$k]) ? $post['vh_ls_fx_areacode'][$k] : NULL;
				$result_ca_3['prefix_fax'] = isset($post['vh_ls_fx_prefix'][$k]) ? $post['vh_ls_fx_prefix'][$k] : NULL;
				$result_ca_3['suffix_fax'] = isset($post['vh_ls_fx_suffix'][$k]) ? $post['vh_ls_fx_suffix'][$k] : NULL;
				$result_ca_3['country_fax'] = isset($post['vh_ls_fx_pri_sequence'][$k]) ? $post['vh_ls_fx_pri_sequence'][$k] : NULL;
				$result_ca_3['type'] = isset($post['vh_fx_type'][$k]) ? $post['vh_fx_type'][$k] : NULL;
				$result_ca_3['parent_id'] = $parent_id;
				$fax_id=$this->input->post('vh_fx_id');
				$k++;
				if(!empty($fax_id[$k])){
						$this->db->where('fax_id',$fax_id[$k]);
						$this->db->update('rq_rqf_quick_quote_fax',$result_ca_3);
					}else{
						$this->db->insert('rq_rqf_quick_quote_fax', $result_ca_3);
					}
			 }
		}
			
	}
   
   
	public function insert_driver_quote($quote_id=''){
		    $post = $this->input->post();	
			$quote_id=$this->get_quote_id(); 
			$file_types = $this->config->item('doc_image_file_types');			
		    $data['driver_count']=$this->input->post('driver_count');			
			$file_copy = $this->do_upload($file_types,'driv_mvr_file', 'file');
			$data['driv_mvr_file'] = isset($file_copy['file_name']) ? str_replace(' ', '_', $file_copy['file_name']) : NULL;
		
			$data['quote_id']=$quote_id;
			$driver_ids=$this->input->post('driver_ids');
			
		    if(!empty($driver_ids)){
						$this->db->where('driver_ids',$driver_ids);
						$this->db->update('rq_rqf_quick_quote_drivers',$data);
					}else{
						$this->db->insert('rq_rqf_quick_quote_drivers', $data);
						
					}
		
		    if (is_array($this->input->post('driver_name'))){			
		    $i = 0;
			 foreach($this->input->post('driver_name') as $driver_name){			
				$result_d['quote_id'] 		= $quote_id;
				$result_d['driver_name'] 	= isset($post['driver_name'][$i]) ? $post['driver_name'][$i] : NULL;
				$result_d['driver_middle_name'] 	= isset($post['driver_middle_name'][$i]) ? $post['driver_middle_name'][$i] : NULL;
				$result_d['driver_last_name'] = isset($post['driver_last_name'][$i]) ? $post['driver_last_name'][$i] : NULL;
				$result_d['driver_license'] 	= isset($post['driver_license'][$i]) ? $post['driver_license'][$i] : NULL;
				$result_d['driver_license_issue_date'] 		= isset($post['driver_license_issue_date'][$i]) ? $post['driver_license_issue_date'][$i] : NULL;
                $result_d['driver_dob'] 	= isset($post['driver_dob'][$i]) ? $post['driver_dob'][$i] : NULL;
				$result_d['driver_license_class']= isset($post['driver_license_class'][$i]) ? $post['driver_license_class'][$i] : NULL;
                $result_d['driver_class_a_years']= isset($post['driver_class_a_years'][$i]) ? $post['driver_class_a_years'][$i] : NULL;
                $result_d['driver_license_issue_state'] 	= isset($post['driver_license_issue_state'][$i]) ? $post['driver_license_issue_state'][$i] : NULL;
               
				$driver_id=$this->input->post('driver_id');
				$i++;
				
				if(!empty($driver_id[$i])){
						$this->db->where('driver_id',$driver_id[$i]);
						$this->db->update('rq_rqf_quick_quote_driver',$result_d);
					}else{
						$this->db->insert('rq_rqf_quick_quote_driver', $result_d);
						
					}
			 }
		}
		
	}
	
	public function insert_owner_quote($quote_id=''){
		    $post = $this->input->post();	
			$quote_id=$this->get_quote_id(); 
			$file_types = $this->config->item('doc_image_file_types');
			$data_1['number_of_owner']=$this->input->post('number_of_owner');
			
			$file_copy = $this->do_upload($file_types,'own_mvr_file', 'file');
			$data_1['own_mvr_file'] = isset($file_copy['file_name']) ? str_replace(' ', '_', $file_copy['file_name']) : NULL;
		
			$data_1['quote_id']=$quote_id;
			$owner_ids=$this->input->post('owner_ids');
		    if(!empty($owner_ids)){
						$this->db->where('owner_ids',$owner_ids);
						$this->db->update('rq_rqf_quick_quote_owners',$data_1);
					}else{
						$this->db->insert('rq_rqf_quick_quote_owners', $data_1);
						
					}
		    if (is_array($this->input->post('owner_name'))){			
		    $i = 0;
			 foreach($this->input->post('owner_name') as $owner_name){			
				$result_o['quote_id'] 		= $quote_id;
				$result_o['owner_name'] 	= isset($post['owner_name'][$i]) ? $post['owner_name'][$i] : NULL;
				$result_o['owner_middle_name'] 	= isset($post['owner_middle_name'][$i]) ? $post['owner_middle_name'][$i] : NULL;
				$result_o['owner_last_name'] = isset($post['owner_last_name'][$i]) ? $post['owner_last_name'][$i] : NULL;
				$result_o['owner_is_driver'] 	= isset($post['owner_is_driver'][$i]) ? $post['owner_is_driver'][$i] : NULL;
				$result_o['owner_license'] 		= isset($post['owner_license'][$i]) ? $post['owner_license'][$i] : NULL;
               
				$owner_id=$this->input->post('owner_id');
				$i++;
				
				if(!empty($owner_id[$i])){
						$this->db->where('owner_id',$owner_id[$i]);
						$this->db->update('rq_rqf_quick_quote_owner',$result_o);
					}else{
						$this->db->insert('rq_rqf_quick_quote_owner', $result_o);
						
					}
			 }
		}
		
	}
	
	
	public function insert_losses_quote($quote_id=''){
		    $post = $this->input->post();	
			$quote_id=$this->get_quote_id(); 
			    $result_l_1['losses_need'] = $this->input->post("losses_need");
				$result_l_1['liability'] = $this->input->post("liability");
				$result_l_1['pd'] = $this->input->post("pd");				
				$result_l_1['cargo'] = $this->input->post("cargos");
				$result_l_1['quote_id'] = $quote_id;
				
				$loss_id = $this->input->post("loss_id");
				
				if(!empty($loss_id)){
						$this->db->where('loss_id',$loss_id);
						$this->db->update('rq_rqf_quick_quote_loss',$result_l_1);
					}else{
						$this->db->insert('rq_rqf_quick_quote_loss', $result_l_1);
						
					}
				
			$file_types = $this->config->item('doc_image_file_types');
		    if (is_array($this->input->post('date_from_losses'))){			
		    $i = 0;
			 foreach($this->input->post('date_from_losses') as $date_from_losses){			
				$result_l['quote_id'] 		= $quote_id;
				$result_l['date_from_losses'] 	= isset($post['date_from_losses'][$i]) ? $post['date_from_losses'][$i] : NULL;
				$result_l['date_to_losses'] 	= isset($post['date_to_losses'][$i]) ? $post['date_to_losses'][$i] : NULL;
				$result_l['losses_amount'] = isset($post['losses_amount'][$i]) ? $post['losses_amount'][$i] : NULL;
				$result_l['losses_company'] 	= isset($post['losses_company'][$i]) ? $post['losses_company'][$i] : NULL;
				$result_l['losses_general_agent'] 		= isset($post['losses_general_agent'][$i]) ? $post['losses_general_agent'][$i] : NULL;
				$result_l['losses_date_of_loss'] 		= isset($post['losses_date_of_loss'][$i]) ? $post['losses_date_of_loss'][$i] : NULL;
                $result_l['type'] 		= isset($post['losses_type'][$i]) ? $post['losses_type'][$i] : NULL;
				
				$losses_id=$this->input->post('losses_id');
				$i++;
				
				if(!empty($losses_id[$i])){
						$this->db->where('losses_id',$losses_id[$i]);
						$this->db->update('rq_rqf_quick_quote_losses',$result_l);
					}else{
						$this->db->insert('rq_rqf_quick_quote_losses', $result_l);
						
					}
			 }
		}
		
	}
	public function get_quotes_info($id = '') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote');
		if($id > 0){
			$where_array = array('id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result_array();
		
		return $result;
	}

	public function get_quote_id(){			
			$this->db->select('id')->from('rq_rqf_quick_quote');
			$this->db->order_by("id", "desc");
			$this->db->limit(1);			
			$query_result = $this->db->get();
			$result = $query_result->result();
			if(!empty($result)){		
			return $result[0]->id;	 
			}
		
		}
		
	public function broker_info($id = '') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote');
		if($id > 0){
			$where_array = array('id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	public function person_info($id = '') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_person');
		if($id > 0){
			$where_array = array('quote_id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	
	public function email_info($id = '',$type='') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_email');
		if($id > 0){
			$where_array = array('quote_id' => $id,'type'=>$type);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	public function phone_info($id = '',$type='') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_phone');
		if($id > 0){
			$where_array = array('quote_id' => $id,'type'=>$type);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	public function fax_info($id='',$type='') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_fax');
		if($id > 0){
			$where_array = array('quote_id' => $id,'type'=>$type);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
   	public function insured_info($id='') {
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_insured');
		if($id > 0){
			$where_array = array('quote_id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
		$result = $query->result();
		
		return $result;
	}
	public function ins_info($id=''){
		$this->db->select('*');   
		$this->db->from('rq_rqf_quick_quote_garage');
		if($id > 0){
			$where_array = array('quote_id' => $id);
			$this->db->where($where_array);
		}
		$query = $this->db->get();
		
	   $result = $query->result_array();
	   if(!empty($result)){
		 foreach($result as $res ){			
            $array2 = array( "phones" => array(), "emails" => array(), "faxs" => array());
            $results[] = array_merge($res, $array2);          
			
			}
	   
		return $results;
	   }
	
	}
	
	
	public function quick_quote_info($id='',$table='',$type=''){
		
		$this->db->select('*');   
		$this->db->from($table);
		if($id > 0){
			$where_array = array('quote_id' => $id);
			$this->db->where($where_array);
		}
		if(!empty($type)){
			$this->db->where('type',$type);
		}
		$query = $this->db->get();
		
		$result = $query->result();
	    return $result;
		}
		
public function quick_quote_vh_info($id='',$table='',$type=''){
	
	$this->db->select('*');   
	$this->db->from($table);
	if($id > 0){
		$where_array = array('quote_id' => $id);
		$this->db->where($where_array);
	}
	if(!empty($type)){
		
		$this->db->where('type',$type);
	}
	$query = $this->db->get();
	$result = $query->result_array();
	$results='';	
	$array1 = array("lessor" => array(), "vh_eqpmnts" => array());	
	 if(!empty($result)){
	  foreach($result as $res){			
	   $array2 = array("lessor" => array(), "vh_eqpmnts" => array());
	   $results[] = array_merge($array2, $res);      
		
		}
		
	  }
	return !empty($results)?$results:$array1;  
	}
 public function quick_quote_lessor_info($id='',$table='',$type=''){
		$this->db->select('*');   
		$this->db->from($table);
		if($id > 0){
			$where_array = array('quote_id' => $id);
			$this->db->where($where_array);
		}
		if(!empty($type)){
			
			$this->db->where('type',$type);
		}
		$query = $this->db->get();
		$result = $query->result_array();		
		 if(!empty($result)){
		  foreach($result as $res){			
		   $array2 = array( "emails" => array(), "faxs" => array());
		   $results[] = array_merge($array2, $res);      
		
			}
			return $results;
		  }
		  
		 
		 
	 }
}

/* End of file quote_model.php */
/* Location: ./application/models/new_quote_model.php */