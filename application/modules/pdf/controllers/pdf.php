<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class pdf extends RQ_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
 public function __construct()
	{
		parent::__construct();
		$this->load->model('pdf_model');
		$this->load->model('quote/quote_model');
		$this->load->model('administration/catlog_model');
		$this->load->helper('my_helper');
		$this->load->model('application_form/application_model');
		$this->load->model('application_form/edit_model');
	}
	
	public function index()
	{		
		$this->load->view('pdf',$data); 
				
	}


 
	
 public function GeneratePDF($pdf_id = '')
	{
	
		
			if($pdf_id){
				$this->load->view('pdf', $this->data);
			}else { 
				
				
				$this->load->view('MPDF53/mpdf.php');	
				$mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0);
 
				$mpdf->SetDisplayMode('fullpage');
				 
				$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
				 
				/* $php = base_url().'pdfreport.php';
									;*/
				$stylesheet = base_url('css').'/style.css';
				$stylesheetcont = file_get_contents($stylesheet);
				
				$data['ajax_req'] = TRUE;
				
				$filecont = $this->load->view('pdfreport.php',$data);
				$stylesheetcont = file_get_contents($stylesheet);
				$path_to_view = get_view_path('pdfreport');
			
				$filecont = file_get_contents($path_to_view);
				
				$mpdf->WriteHTML($stylesheetcont,1);			
				$mpdf->WriteHTML(file_get_contents(base_url().'application/modules/view/pdf/pdfreport.php'));
			
				$mpdf->WriteHTML($filecont);
				 
				$mpdf->Output();
							
			}
	}
	
	
	
public function GeneratePDF1($pdf_id = '')
	{
		ini_set('allow_url_fopen', 1);
				
				$this->load->view('MPDF53/mpdf.php');	
				$mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0);
 
				$mpdf->SetDisplayMode('fullpage');
				 
				$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
				 
				
				$stylesheet = base_url('css').'/style.css';
				$stylesheetcont = file_get_contents($stylesheet);
				
				$data['ajax_req'] = TRUE;
				if($this->session->userdata('underwriter_id')){$pdf='pdf_u='.$_GET['check'];}else{$pdf='pdf';}
				$url_view = base_url('ratesheet/view_quote_rate1/'.$pdf_id.'?'.$pdf);
				$filecont = file_get_contents($url_view);
				//$filecont = $this->load->view('pdfreport',$data, TRUE);
				$stylesheetcont = file_get_contents($stylesheet);
				$path_to_view = get_view_path('pdfreport');
			
				//$filecont = file_get_contents($path_to_view);
				
				$mpdf->WriteHTML($stylesheetcont,1);			
			
				$mpdf->WriteHTML($filecont);
				 
				$mpdf->Output();
							
		/*	}*/
	}

public function GeneratePDF1_amin($pdf_id = '')
	{
		ini_set('allow_url_fopen', 1);
		//$this->load->library('form_validation');
		//$this->quote_model->get_quote($quote_id);
		
			/*if($pdf_id){
				$this->load->view('pdf', $this->data);
			}else { */
		$data['quote_id']=$this->quote_model->get_bundle_quote($pdf_id);		
		$data['row'] = new stdClass();
		//$data['row'] = $this->admin_init_elements->set_post_vals($this->input->post());
		$data['row']->id = $quote_id= $data['quote_id'][0]->quote_id;	
		//die;
		$data['user'] = 'member';
		$data['object'] = $this;		
		
		
		//print_r($data['quote_id']);
	 	
		
		$data['bundleid'] = $pdf_id;
		$data['all_full'] = $this->quote_model->get_quote($data['row']->id);
		$data['quote_full'] = $this->quote_model->get_quote_full($data['row']->id);
		
		$data['quote'] = $this->quote_model->get_insured_info($data['row']->id);
		$data['quote_id'] = $pdf_id;
		
		/**Get all vehicle**/
		$data['vehicles'] = $this->quote_model->get_all_vehicle($data['row']->id);
		/**get drivers**/
		$data['drivers'] = $this->quote_model->get_all_drivers($data['row']->id);
		/**get losses**/
		$data['losses'] = $this->quote_model->get_losses($data['row']->id);
		/**get all fillings**/
		$data['fillings'] = $this->quote_model->get_all_fillings($data['row']->id);
		//echo $data['row']->id;
		//print_r($data['quote_bundle']);
		$data['quote_bundle'] = $this->quote_model->get_quote_bundle($pdf_id);
	//	print_r($data['quote_bundle']);
	//	die;
		//print_r($data['quote_bundle']);
		$data['prodemail'] = $data['quote_bundle'][0]['producer_email'];
		$q = $data['quote_bundle'][0];
		
		$data['cargo_rate'] = $this->quote_model->get_sheet_rate($q['cargo_id'],'cargo');
	
		$data['pd_rate'] = $this->quote_model->get_sheet_rate($q['pd_id'],'pd');
		$data['liability_rate'] = $this->quote_model->get_sheet_rate($q['liability_id'],'liability');
		
		$data['carriers'] = $this->catlog_model->get_carrier_by_state($data['quote']->insured_state);
				
				
				$this->load->view('MPDF53/mpdf.php');	
				$mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0);
 
				$mpdf->SetDisplayMode('fullpage');
				 
				$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
				 
				/* $php = base_url().'pdfreport.php';
									;*/
				$stylesheet = base_url('css').'/style.css';
				$stylesheetcont = file_get_contents($stylesheet);
				
				$data['ajax_req'] = TRUE;
				
				$filecont = $this->load->view('pdfreport',$data, TRUE);
				$stylesheetcont = file_get_contents($stylesheet);
				$path_to_view = get_view_path('pdfreport');
			
				//$filecont = file_get_contents($path_to_view);
				
				$mpdf->WriteHTML($stylesheetcont,1);			
				//$mpdf->WriteHTML(file_get_contents(base_url().'application/modules/view/pdf/pdfreport.php'));
			
				$mpdf->WriteHTML($filecont);
				 
				$mpdf->Output();
							
		/*	}*/
	}
	
	 public function GeneratePDF2($pdf_id = '')
	{
		ini_set('allow_url_fopen', 1);
		//$this->load->library('form_validation');
		//$this->quote_model->get_quote($quote_id);
		
			/*if($pdf_id){
				$this->load->view('pdf', $this->data);
			}else { */
				
		$data['row'] = new stdClass();
		//$data['row'] = $this->admin_init_elements->set_post_vals($this->input->post());
		$data['row']->id = $pdf_id;	
		$id=$pdf_id;
	$data['app']=$this->application_model->get_data($id);
	$data['app_applicant']=$this->application_model->get_data_applicant($id);
	$data['app_tel_add']=$this->application_model->get_data_tel_add($id);
	
	$data['app_email_add']=$this->application_model->get_data_email_add($id);
	//$data['broker_inform'] = $this->application_model->get_broker_inform();
	
    $data['broker_insured'] = $this->application_model->get_broker_insured($id);
	
	   // $data['quote_id'] = $this->input->post('quote_id');
			   $data['app_load']=$this->application_model->get_database_inform($id,'application_loaded');
			   $data['app_cov']=$this->application_model->get_database_inform($id,'application_coverage');
               $data['app_veh1']=$this->application_model->get_database_inform2($id,'application_vehicle','TRUCK');
			   $data['app_veh2']=$this->application_model->get_database_inform2($id,'application_vehicle','TRACTOR');
	    	   $data['app_veh3']=$this->application_model->get_database_inform2($id,'application_vehicle','TRAILER');
			   			  
						
			   $data['app_veh_pd']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRUCK');
			   $data['app_veh_pd1']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRACTOR');
			   $data['app_ghi13']=$this->application_model->get_database_inform($id,'application_ghi13');
		       $data['app_driver']=$this->application_model->get_database_inform($id,'application_driver');
			
			   $data['app_com']=$this->application_model->get_database_inform($id,'application_commodity');
			   $data['app_trans']=$this->application_model->get_database_inform($id,'application_transported');
			   $data['app_cert']=$this->application_model->get_database_inform($id,'application_certificate');
			   $data['app_add']=$this->application_model->get_database_inform($id,'application_additional');
			   $data['app_own']=$this->application_model->get_database_inform($id,'application_owner_operator');
			   $data['app_fil']=$this->application_model->get_database_inform($id,'application_filings');
			   $data['edit_app']=$this->application_model->get_database_inform($id,'application_ghi11');
			   $data['app_ghi6']=$this->application_model->get_database_inform($id,'application_ghi6');
			   $data['app_ghi5a']=$this->application_model->get_database_inform($id,'application_ghi5a');
			   $data['app_ghi5b']=$this->application_model->get_database_inform($id,'application_ghi5b');
			   $data['app_ghi5c']=$this->application_model->get_database_inform($id,'application_ghi5c');
			   $data['app_ghi5d']=$this->application_model->get_database_inform($id,'application_ghi5d');
			   $data['app_ghi5e']=$this->application_model->get_database_inform($id,'application_ghi5e');
			   $data['app_ghi5f']=$this->application_model->get_database_inform($id,'application_ghi5f');
			   $data['app_ghi9']=$this->application_model->get_database_inform($id,'application_ghi9');
			   
			   
			      $data['app_load']=$this->application_model->get_database_inform($id,'application_loaded');
			   $data['app_cov']=$this->application_model->get_database_inform($id,'application_coverage');
               $data['app_veh1']=$this->application_model->get_database_inform2($id,'application_vehicle','TRUCK');
			   $data['app_veh2']=$this->application_model->get_database_inform2($id,'application_vehicle','TRACTOR');
	    	   $data['app_veh3']=$this->application_model->get_database_inform2($id,'application_vehicle','TRAILER');
			   			  
						
			   $data['app_veh_pd']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRUCK');
			   $data['app_veh_pd1']=$this->application_model->get_database_inform2($id,'application_vehicle_pd','TRACTOR');
			   $data['app_ghi13']=$this->application_model->get_database_inform($id,'application_ghi13');
		       $data['app_driver']=$this->application_model->get_database_inform($id,'application_driver');
			
			
			
			
		//$data['app_loss']=$this->application_model->get_database_inform($data['row']->id,'application_losses');
	    // print_r($data['app_loss']);
	 //	die;	
				$this->load->view('MPDF53/mpdf.php');	
				$mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0);
 
				$mpdf->SetDisplayMode('fullpage');
				 
				$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
				 
				/* $php = base_url().'pdfreport.php';
									;*/
				$stylesheet = base_url('css').'/style.css';
				$stylesheetcont = file_get_contents($stylesheet);
				
				$data['ajax_req'] = TRUE;
				
				$filecont = $this->load->view('pdfapp',$data, TRUE);
				$stylesheetcont = file_get_contents($stylesheet);
				$path_to_view = get_view_path('pdfapp');
			
				//$filecont = file_get_contents($path_to_view);
				
				$mpdf->WriteHTML($stylesheetcont,1);			
				//$mpdf->WriteHTML(file_get_contents(base_url().'application/modules/view/pdf/pdfreport.php'));
			
				$mpdf->WriteHTML($filecont);
				 
				$mpdf->Output();
							
		/*	}*/
	}
	
	
	
}