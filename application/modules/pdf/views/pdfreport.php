<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		//$this->load->view('includes/header');
	}

	//include("MPDF53/mpdf.php");
	//require(base_url().'application/helpers/MPDF53/mpdf.php'); //this is to include mPDF folder
	//include_once('MPDF53/mpdf.php');
	
	/*$mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0);
 
	$mpdf->SetDisplayMode('fullpage');
	 
	$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
						
	$mpdf->WriteHTML(file_get_contents('pdfreport'));
	
	/*$mpdf->WriteHTML('<p>Hello Tudip Technologies</p>');
	 
	$mpdf->Output();*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title> PDF Generate </title>
    <link href="<?php echo base_url('css');?>/style.css" rel="stylesheet">
    
</head>
<body>

<div id="wrapper">
    <div class="row_fluid">
		<div class='left_area' style="width: 167px;"> 
        	<b>2575 Collier Canyon Road <br />
        	Livermore CA 94551<br /> www.gcib.net</b>
        </div>
        
        <div class='middle_area'> 
        	<img src="<?php echo base_url('images');?>/LOGORATESHEET.png"  style=" width: 207px;"/>
        </div>
        
        <div class='right_area' style="width: 125px;"> 
        	<b> 925.493.7525 Tel.<br />925.493.7526 Fax<br />License #0E52042</b>
        </div>
    </div>
    <br>
    <div class="row_fluid">
    	Attn : <?php  echo $quote_full->contact_name .''.$quote_full->contact_middle_name.''.$quote_full->contact_last_name.' / '.$quote_full->agency; ?>
        <br>
        Re : <?php echo  isset($quote->insured_dba) ? ($quote->insured_dba!='') ? $quote->insured_dba : $quote->insured_fname .''.$quote->insured_mname.''.$quote->insured_lname : $quote->insured_fname .''.$quote->insured_mname.''.$quote->insured_lname;
          ?>
        <br>
        Quote : <?= isset($quote_id) ? $quote_id : ''; ?>
    </div>
    
    <div class="row_fluid">
    	<h4 style="text-decoration:underline;"> This quote was based on the following information </h4>
    </div>
      <?php $cargo1 = $cargo_rate[0]; $cargo =  (array) $cargo1; //print_r($cargo);?>
    <?php $pd1 = $pd_rate[0]; $pd =  (array) $pd1; //print_r($pd);?>
    <?php $liability1 = $liability_rate[0];  $liability = (array) $liability1//print_r($liability);?>
    <div class="row_fluid">
    	<!--Garaging City : <?= isset($quote->insured_garaging_city) ? ($quote->insured_garaging_city!='') ? $quote->insured_garaging_city : 'None' : 'None'; ?>
        <br>-->
        Garaging City : <?php echo  isset($quote->insured_garaging_city) ? ($quote->insured_garaging_city!='') ? $quote->insured_garaging_city : 'None' : 'None'; echo"&nbsp; "; echo  isset($quote->insured_state) ? ($quote->insured_state!='') ? $quote->insured_state : 'None' : 'None'; 
       // print_r($quote);
       ?>
        <br>
        <!--Vehicles : <?= isset($vehicles) ? count($vehicles) : ''; ?>-->
         Vehicles : <?php //echo isset($vehicles) ? count($vehicles) : ''; // print_r($vehicles);
		if(isset($drivers)){
			$TRAC=0;
			$TRAI=0;
			$TRUCk=0;
			foreach($vehicles as $vehicles1){ 
		  if($vehicles1->vehicle_type=="TRUCK")
			{ 
			//$a=$vehicles1->vehicle_type;
			$TRUCk++;
			}
			if($vehicles1->vehicle_type=="TRACTOR")
			{ 
			//$a=$vehicles1->vehicle_type;
			$TRAC++;
			}
			if($vehicles1->vehicle_type=="TRAILER")
			{ 
			//$a=$vehicles1->vehicle_type;
			$TRAI++;
			}
			
			//print_r($vehicles1);
			}
			echo isset($TRUCk) ? $TRUCk ." TRUCK, &nbsp;&nbsp;": "0 TRUCK, &nbsp;&nbsp;";
			echo isset($TRAC) ? $TRAC ." TRACTOR, &nbsp;&nbsp;": "0 TRACTOR, &nbsp;&nbsp;";
			echo isset($TRAI) ? $TRAI ." TRAILER ": "0 TRAILER";
			
			//echo $TRAC." TRACTOR &nbsp;&nbsp;".$TRAI." TRAILER";
		}
		?>
        <br>
        <!--Drivers : <?= isset($drivers) ? count($drivers) : ''; ?>-->
        Drivers :  <?php 
		if(isset($drivers)){
			foreach($drivers as $drivers1){ 
			echo $drivers1->driver_name." ".$drivers1->driver_middle_name." ".$drivers1->driver_last_name.",";
			}
		} 
		?>
        <br>
        Radius :  <?php echo $vehicles[0]->radius_of_operation; ?>
        
         <?php /*$libi = 0; $caro_limit = 0; $pd_limit = 0; $libi_ded = 0; $caro_ded = 0; $pd_ded = 0; 
		if(isset($vehicles)){ 
         $radius = '';  foreach($vehicles as $vehicle){
        	
			$libi = $libi + str_replace(',', '', (str_replace('.', '', str_replace('$', '', $vehicle->liability))));
			$caro_limit = $caro_limit + str_replace(',', '', (str_replace('.', '', str_replace('$', '', $vehicle->cargo))));
			$pd_limit = $pd_limit + str_replace(',', '', (str_replace('.', '', str_replace('$', '', $vehicle->pd))));
			
			$libi_ded = $libi_ded + str_replace(',', '', (str_replace('.', '', str_replace('$', '', $vehicle->liability_ded))));
			$caro_ded = $caro_ded + str_replace(',', '', (str_replace('.', '', str_replace('$', '', $vehicle->cargo_ded))));
			$pd_ded = $pd_ded + str_replace(',', '', (str_replace('.', '', str_replace('$', '', $vehicle->pd_ded))));
			 
        	 if($vehicle->radius_of_operation!=$radius){
					if($radius=='')
					{
						echo $vehicle->radius_of_operation;
					}
					else
					{
						echo ', '.$vehicle->radius_of_operation;
					}
					$radius = $vehicle->radius_of_operation;
					}
				
          } 
         } */ ?>
        <br>
        Commodities : <?= $cargo['commodities']; ?>
        <?= isset($pd['commodities']) ? ', '.$pd['commodities'] : ''; ?>
        <?= isset($liability['commodities']) ? ', '.$liability['commodities'] : ''; ?>
        <br>
        Pull : <?php  echo $vehicles[0]->put_on_truck;
		
		/*if(isset($vehicles)){ $i = 0;
         $trailer_typ = ''; foreach($vehicles as $vehicle){
        	
        	 if($vehicle->vehicle_type=='TRAILER'){
        	 if($vehicle->type_of_trailer!=$trailer_typ){
			
					if($trailer_typ=='')
					{
						echo $vehicle->type_of_trailer;
					}
					else
					{
						echo ', '.$vehicle->type_of_trailer;
					}
					$trailer_typ = $vehicle->type_of_trailer;
					$i++;
					}
				
            } 
        } 
      } 
     if($i==0){ echo 'None'; }*/ ?>
        <br>
        Losses : <?php echo "losses surcharge: ".$liability['losses_surcharge'].","." losses premium: ".$liability['losses_premium'].","." losses percentage: ".$liability['losses_percentage'];
		/*if(isset($losses)){ $i = 0; 
         foreach($losses as $loss){        	
        	 
				if($i==0)
				{
					echo $loss->losses_type;
				}
				else
				{
					echo ', '.$loss->losses_type;
				}	
				$i++;			
			
         } 
        }    
        if($i==0){ echo 'None'; }*/?>
        <br>
        Filling : <?php //echo isset($fillings[0]->filings_type) ? $fillings[0]->filings_type : " ";
		//print_r($fillings);
        if(!empty($fillings)){
		echo isset($fillings[0]->filings_type) ? $fillings[0]->filings_type : " ";
		if($fillings[0]->filings_type=='Other'){
			echo isset($fillings[0]->filing_type_other) ? "&nbsp; ".$fillings[0]->filing_type_other ."&nbsp; ": " ";
		}
		echo isset($fillings[0]->filing_no) ? $fillings[0]->filing_no: " ";
		}
		/*if(isset($fillings)){ $i = 0; 
         foreach($fillings as $filling){        	
        	 
				$filling->filings_type;	
				$i++;				
			
         } 
        }    
       if($i==0){ echo 'None'; }*/?> 
        <br>
        U.M : <?php echo "um surcharge:".$liability['um_coverage_surcharge'].","." um cost:".$liability['um_coverage_cost'].","." um premium".$liability['um_coverage_premium']; /*if(isset($liability['um_coverage_surcharge'])){ if($liability['um_coverage_surcharge']=='Rejected'){ echo $liability['um_coverage_surcharge']; } else { echo $liability['um_coverage_premium']; } } else { echo 'None'; }*/?>
        <br>
        P.I.P : <?php echo "pip surcharge:".$liability['pip_coverage_surcharge'].","."pip cost:".$liability['pip_coverage_cost'].","."pip premium".$liability['pip_coverage_premium'];/*if(isset($liability['pip_coverage_surcharge'])){ if($liability['pip_coverage_surcharge']=='Rejected'){ echo $liability['pip_coverage_surcharge']; } else { echo $liability['pip_coverage_premium']; } } else { echo 'None'; }*/?>
        <br>
        Yrs. in Bus. : <?php //echo isset($quote->insured_garaging_city) ? ($quote->insured_garaging_city!='') ? $quote->insured_garaging_city : 'None' : 'None'; ?><?= isset($quote->year_in_business) ? ($quote->year_in_business!='') ? $quote->year_in_business : 'None' : 'None';?>
        <br> <br>
    </div>
<?php /*?>    <div class="row_fluid row_table">
    	<table border="1" cellpadding="0" cellspacing="0" RULES="ROWS">
        	<tr>
            	<td width="200px"><b> Liability Carrier :</b> <?= isset($liability['carrier']) ? $liability['carrier'] : 'None'; ?> </td>
                <td width="200px"><b> Limit :</b> <label><?= '$ '.$libi; ?> </label> </td>
                <td width="200px"><b> Deductible :</b> <label> <?= '$ '.$libi_ded; ?> </label> </td>
                <td width="200px"> 
                	<b>Premium :</b> <label> <?= isset($liability['total_liability_premium']) ? $liability['total_liability_premium'] : 'None'; ?></label><br>
                   <b> Policy Fee : </b><label> <?= isset($liability['liability_policy_fee']) ? $liability['liability_policy_fee'] : 'None'; ?> </label><br>
                   <b> Filling Fee :</b> <label> <?= isset($liability['liablity_filing_fee']) ? $liability['liablity_filing_fee'] : 'None'; ?> </label><br>           	
                </td>
            </tr>
            <tr>
            	<td width="200px"> <b>Cargo Carrier :</b>   <?= isset($cargo['carrier']) ? $cargo['carrier'] : 'None'; ?> </td>
                <td width="200px"> <b>Limit : </b> <label><?= '$ '.$caro_limit ?></label> </td>
                <td width="200px"> 
                	<b>Deductible : </b> <label> <?= '$ '.$caro_ded ?>  </label> <br>
                	<b>Reefer Ded :</b>  <label> <?= '$ '.$caro_ded ?> </label> 
                </td>
                <td width="200px"> 
                	<b>Premium : </b> <label> <?= isset($cargo['total_cargo_premium']) ? $cargo['total_cargo_premium'] : 'None'; ?> </label><br>
                    <b>Policy Fee :</b>  <label> <?= isset($cargo['cargo_policy_fee']) ? $cargo['cargo_policy_fee'] : 'None'; ?> </label><br>
                    <b>SLA Tax :</b>  <label> <?= isset($cargo['cargo_sla_tax']) ? $cargo['cargo_sla_tax'] : 'None'; ?> </label><br>           	
                </td>
            </tr>
            <tr>
            	<td width="200px"> <b>Physical Damage Carrier :</b> </br> <?= isset($pd['carrier']) ? $pd['carrier'] : 'None'; ?> </td>
                <td width="200px"> <b>Limit : </b> <label><?= '$ '.$pd_limit ?> </label> </td>
                <td width="200px"> <b>Deductible :</b>  <label> <?= '$ '.$pd_ded ?> </label> </td>
                <td width="200px"> 
                	<b>Premium : </b> <label> <?= isset($pd['total_pd_premium']) ? $pd['total_pd_premium'] : 'None'; ?> </label><br>
                   <b> Policy Fee : </b> <label> <?= isset($pd['pd_policy_fee']) ? $pd['pd_policy_fee'] : 'None'; ?> </label><br>
                   <b> SLA Tax : </b> <label> <?= isset($pd['pd_sla_tax']) ? $pd['pd_sla_tax'] : 'None'; ?> </label><br>       	
                </td>
            </tr>            
        </table>	
        <?php $liability_pre = isset($liability['total_liability_premium']) ? $liability['total_liability_premium'] : '0'; ?>
        <?php $cargo_pre =  isset($cargo['total_cargo_premium']) ? $cargo['total_cargo_premium'] : '0'; ?>
        <?php $pd_pre =  isset($pd['total_pd_premium']) ? $pd['total_pd_premium'] : '0'; ?>
        
        <?php $liability_p_fee = isset($liability['liability_policy_fee']) ? $liability['liability_policy_fee'] : '0'; ?>
        <?php $cargo_p_fee = isset($cargo['cargo_policy_fee']) ? $cargo['cargo_policy_fee'] : '0'; ?>
        <?php $pd_p_fee =  isset($pd['pd_policy_fee']) ? $pd['pd_policy_fee'] : '0'; ?>
        
        <?php $cargo_tax = isset($cargo['cargo_sla_tax']) ? $cargo['cargo_sla_tax'] : '0'; ?>
        <?php $pd_tax =  isset($pd['pd_sla_tax']) ? $pd['pd_sla_tax'] : '0'; ?>
        <div class="total">
            <b>  Total of Premium : </b> <label> <?php echo '$ '; echo $liability_pre+$cargo_pre+$pd_pre; ?> </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>  Total Fees : </b> <label> <?php echo '$ '; echo $liability_p_fee+$cargo_p_fee+$pd_p_fee; ?> </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>  Total Tax :</b>  <label> $ <?= $cargo_tax+$pd_tax; ?> </label>
        </div>
        
    </div><?php */
	
	?>
    <div class="row_fluid row_table">
    	<table border="1" cellpadding="0" cellspacing="0" RULES="ROWS" style="border:1px solid #000;">
        	<tr style=" border-bottom:1px solid #000;">
            	<td width="200"><div style="width:100%; background-color:#CCC;"><b>&nbsp; Liability</b> </div>
                <b>&nbsp; Carrier :</b><?= isset($liability['carrier']) ? $liability['carrier'] : 'None'; ?></td>
                
                <td width="200"><b> Limit :</b><?= '$ '.isset($liability['combined_single_limit']) ? $liability['combined_single_limit'] : ''; ?> </td>
                <td width="200"><b> Deductible :</b><?= '$ '.isset($liability['deductible']) ? $liability['deductible'] : ''; ?></td>
                <td width="200"> 
                	<b >Premium :    &nbsp;</b><?= isset($liability['total_liability_premium']) ? $liability['total_liability_premium'] : 'None'; ?><br>
                   <b> Policy Fee :</b><?= isset($liability['liability_policy_fee']) ? $liability['liability_policy_fee'] : 'None'; ?><br>
                   <b> Filling Fee :</b><?= isset($liability['liablity_filing_fee']) ? $liability['liablity_filing_fee'] : 'None'; ?><br>           	
                </td>
            </tr>
            <tr style=" border-bottom:1px solid #000;">
            	<td width="200"><div style="width:100%; background-color:#CCC;"><b>&nbsp;Cargo </b></div><b>&nbsp;Carrier :</b>   <?= isset($cargo['carrier']) ? $cargo['carrier'] : 'None'; ?></td>
                <td width="200"> <b>Limit : </b><?= '$ '.isset($cargo['limit']) ? $cargo['limit'] : ''; ?></td>
                <td width="200"> 
                	<b>Deductible : </b><?= '$ '.isset($cargo['deductible']) ? $liability['deductible'] : ''; ?><br>
                	
                </td>
                <td width="200"> 
                	<b>Premium : &nbsp;</b><?= isset($cargo['total_cargo_premium']) ? $cargo['total_cargo_premium'] : 'None'; ?><br>
                    <b>Policy Fee :</b><?= isset($cargo['cargo_policy_fee']) ? $cargo['cargo_policy_fee'] : 'None'; ?><br>
                    <b>SLA Tax : &nbsp;&nbsp;  </b><?= isset($cargo['cargo_sla_tax']) ? $cargo['cargo_sla_tax'] : 'None'; ?><br>           	
                </td>
            </tr>
            <?php // print_r($pd);?>
            <tr style=" border-bottom:1px solid #000;">
            	<td width="200"><div style="width:100%; background-color:#CCC;"><b>&nbsp;Physical Damage :</b></div>
                <b>&nbsp;Carrier :</b>  <?= isset($pd['carrier']) ? $pd['carrier'] : 'None'; ?></td>
                <td width="200"> <b>Limit : </b> <?= '$ '.isset($pd['limit']) ? $pd['limit'] : ''; ?> </td>
                <td width="200"> <b>Deductible :</b>    <?= '$ '.isset($pd['deductible']) ? $pd['deductible'] : ''; ?> 
                <b>Reefer Ded :</b><?= '$ '.isset($pd['reffer_deductible']) ? $pd['reffer_deductible'] : ''; ?>
                 </td>
                <td width="200"> 
                	<b>Premium : &nbsp;</b> <?= isset($pd['total_pd_premium']) ? $pd['total_pd_premium'] : 'None'; ?> <br>
                   <b> Policy Fee : </b> <?= isset($pd['pd_policy_fee']) ? $pd['pd_policy_fee'] : 'None'; ?> <br>
                   <b> SLA Tax : &nbsp;&nbsp;</b>  <?= isset($pd['pd_sla_tax']) ? $pd['pd_sla_tax'] : 'None'; ?> <br>       	
                </td>
            </tr>
            <tr style="border:1px solid #000;"> <?php $liability_pre = isset($liability['total_liability_premium']) ? $liability['total_liability_premium'] : '0'; ?>
        <?php $cargo_pre =  isset($cargo['total_cargo_premium']) ? $cargo['total_cargo_premium'] : '0'; ?>
        <?php $pd_pre =  isset($pd['total_pd_premium']) ? $pd['total_pd_premium'] : '0'; ?>
        
        <?php $liability_p_fee = isset($liability['liability_policy_fee']) ? $liability['liability_policy_fee'] : '0'; ?>
        <?php $cargo_p_fee = isset($cargo['cargo_policy_fee']) ? $cargo['cargo_policy_fee'] : '0'; ?>
        <?php $pd_p_fee =  isset($pd['pd_policy_fee']) ? $pd['pd_policy_fee'] : '0'; ?>
        
        <?php $cargo_tax = isset($cargo['cargo_sla_tax']) ? $cargo['cargo_sla_tax'] : '0'; ?>
        <?php $pd_tax =  isset($pd['pd_sla_tax']) ? $pd['pd_sla_tax'] : '0'; ?>
            	<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <b>Total of Premium : </b><?php echo '$ '; echo $liability_pre+$cargo_pre+$pd_pre; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <b>Total Fees :</b> <?php echo '$ '; echo $liability_p_fee+$cargo_p_fee+$pd_p_fee; ?>
                </td>
                <td width="200"><strong>Total Tax :</strong> $ <?= $cargo_tax+$pd_tax; ?></td>
            </tr>            
        </table>	
        
        <!--<div class="total">
            <b>  Total of Premium : </b> <label> <?php echo '$ '; echo $liability_pre+$cargo_pre+$pd_pre; ?> </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <b>  Total Fees : </b> <label> <?php echo '$ '; echo $liability_p_fee+$cargo_p_fee+$pd_p_fee; ?> </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <b>  Total Tax :</b>  <label> $ <?= $cargo_tax+$pd_tax; ?> </label>
        </div>-->
        
    </div>
    <br><br>
    <div class="row_fluid" style="text-align:center;">
    	Down Payment Break Down: 25% of Premium - 10% Broker Commission = 15% of Premium (including taxes and fees)<br>
        ** Check should be made payable to Global Century Insurance **
    </div>
    
    <div class="row_fluid row_checkbox">
    <?php 
if(($this->session->userdata('underwriter_id'))){?>
    	<h2 style="text-decoration:underline;"> Bind Check List </h2>
        <div class="col_fluid">
            <div class="sng_check"><input type="checkbox" name="liability_app" value="" checked="checked"> Liability Application </div>
            <div class="sng_check"> <input type="checkbox" name="down_payment" value="" checked="checked"> Down Payment </div>
            <div class="sng_check"> <input type="checkbox" name="auth_deposit" value="" checked="checked"> Auth. to Deposit Form </div>
            <div class="sng_check"> <input type="checkbox" name="finance_agr" value="" checked="checked"> Finance Agreement </div>

            <div class="sng_check"> <input type="checkbox" name="other" value=""> Other: </div>
        </div>
        
        <div class="col_fluid">
            <div class="sng_check"><input type="checkbox" name="safe_upd" value="" > SAFER Update </div>
            <div class="sng_check"> <input type="checkbox" name="txdot_upd" value="" > TxDOT Update </div>
            <div class="sng_check"> <input type="checkbox" name="phy_dmg_ap" value="" > Physical Damage Applicatior </div>
            <div class="sng_check"> <input type="checkbox" name="cargo_ap" value="" > Cargo Application </div>
        </div>
        
        <div class="col_fluid">
            <div class="sng_check"><input type="checkbox" name="gh7" value="" > GHI-7 / GHI-10 </div>
            <div class="sng_check"> <input type="checkbox" name="slad1" value="" > SLA D-1(Texas) </div>
            <div class="sng_check"> <input type="checkbox" name="sladd" value="" > SLA D-1(D.C) </div>
            <div class="sng_check"> <input type="checkbox" name="crl" value="" > Updated / Current Loss </div>
        </div>
        
        <div class="col_fluid">
            <div class="sng_check"><input type="checkbox" name="ghi3" value="" checked="checked"> GHI-3 </div>
            <div class="sng_check"><input type="checkbox" name="ghi6" value="" > GHI-6 </div>
            <div class="sng_check"><input type="checkbox" name="ghi9" value="" checked="checked"> GHI-9 </div>
            <div class="sng_check"><input type="checkbox" name="ghi48" value="" > GHI-48 </div>
            <div class="sng_check"><input type="checkbox" name="ghibc" value="" checked="checked"> GHI-48 b&c </div>
        </div>
        
         <div class="col_fluid">
            <div class="sng_check"><input type="checkbox" name="vehicle_reg" value="" checked="checked"> Vehicle Registration </div>
            <div class="sng_check"> <input type="checkbox" name="non_own" value=""> Proof of Non-Ownerst </div>
            <div class="sng_check"> <input type="checkbox" name="own_mvr" value="" checked="checked"> Owner MVR </div>
            <div class="sng_check"> <input type="checkbox" name="acpt_driver" value="" checked="checked"> Acceptable Driver </div>
        </div>
        <br><br>
        <?php	
}else{
}
?>
        <div class="row_fluid row_checkbox">
        <div class="row_fluid">
        <h2 style="text-decoration:underline; "> Comments in Request : </h2>
        <?php echo  isset($quote->Comments_request) ? ($quote->Comments_request!='') ? $quote->Comments_request : 'None' : 'None'; ?>
        </div>
        <br><br>
    	<h2 style="text-decoration:underline;"> NOTICE TO BROKER </h2>
        <p style="font-size:11px;">
   		    We reserve the right to amend or withdraw this quotation at any time without any notice,or if we receive information which we believe is not reflected in the terms and 				       		conditions quoted above.
        </p>
        </div>
    </div>   
</body>
</html>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>