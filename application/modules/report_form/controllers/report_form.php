<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_form extends RQ_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('report_model');
		$this->load->model('application_form/application_model');
	}
	
	public function index()
	{   
	    $data['broker_inform'] = $this->application_model->get_broker_inform();
		$data['underwriter_inform']=$this->report_model->get_underwriter_details();
		//echo '<pre>';print_r($data['underwriter_inform']);
		$data['select_value']='';
		$data['broker_email']='';
		$data['informations']='';
		$data['select_underwriter_id']='';
		$data['select_status']='';
		
		$this->load->view('report',$data);
	}
	
	public function show_report(){
		  $data['broker_inform'] = $this->application_model->get_broker_inform();
		  $data['underwriter_inform']=$this->report_model->get_underwriter_details();
          $data['broker_email']=$this->input->post('broker_name');
		  $data['select_value']=$this->input->post('select_value');
		  $data['select_underwriter_id']=$this->input->post('underwriter_name');
		  $data['underwriters']=$this->report_model->get_underwriter_details($data['select_underwriter_id']);
		  $data['select_status']=$this->input->post('select_status');
		  $data['date_from']=$this->input->post('date_from');
		  $data['date_to']=$this->input->post('date_to');
		  $data['informations']=$this->report_model->get_information();
          
		  $this->load->view('report',$data);
		  
		}
	
}