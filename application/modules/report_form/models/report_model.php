<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Report_model extends CI_Model {

	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
	public function validate_underwriter()
	{
	  $this->db->where('email', $this->input->post('username'));
	  $this->db->where('password', $this->input->post('password'));
	  $query = $this->db->get($this->config->item('underwriter_table'));
	  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else  
	  { 
			return FALSE;
	  }
	}
	
	public function get_underwriter_detail($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->config->item('underwriter_table'));
	  
	  	if($query->num_rows == 1) 
	  	{
			return $query->row();
	  	}
	  	else  
	  	{ 
			return FALSE;
	  	}
	}
	
	public function get_underwriter_details($id='')
	{  
	    $this->db->select('*');
		$this->db->from('rq_underwriter');  
		
	  if($id!=''){
		$this->db->where('id', $id);
	    }

		 $this->db->where('status', 1);
		 $this->db->where('delete_status', 0);
		 $query = $this->db->get();
		 if($id!=''){
		 $result = $query->result_array();
		 }else{
		  $result = $query->result(); 	 
	    }
     return $result;
	}
	
	public function get_information(){
		
		$select_value=$this->input->post('select_value');		
		$broker_name=$this->input->post('broker_name');
		$underwriter_name=$this->input->post('underwriter_name');
		$select_status=$this->input->post('select_status');
		  if($select_value==1){
			  $current_date =date("Y-m-d");			
			  $sql = "SELECT * FROM `rq_rqf_quote` WHERE `date_added` LIKE '$current_date%'";
	
			  }
		  if($select_value==2){
			  $current_month =date("Y-m-");
			  
			  $sql = "SELECT * FROM `rq_rqf_quote` WHERE `date_added` LIKE '%$current_month%'";
		 
			  }
		  if($select_value==3){
			  $current_week_sdate = date("Y-m-d",strtotime('monday this week'));
              $current_week_edate = date("Y-m-d",strtotime("sunday this week"));
			  
			  $sql = "SELECT * FROM `rq_rqf_quote`WHERE `date_added` >= '$current_week_sdate 00:00:00'AND   `date_added` <  '$current_week_edate 00:00:00'";
	
			  }
		  if($select_value==4){
			  $date_from=date("Y-m-d",strtotime($this->input->post('date_from')));
              $date_to=date("Y-m-d",strtotime($this->input->post('date_to')));
			  
			  $sql = "SELECT * FROM `rq_rqf_quote`WHERE `date_added` >= '$date_from 00:00:00'AND   `date_added` <  '$date_to 00:00:00'";
		 
			  }
		 if($broker_name != ''){			  
              
			   $sql .= "AND  `email` LIKE '%$broker_name%'";
		     
			  }
	     if($underwriter_name!=''){			  
              	
			   $sql .= "AND  `assigned_underwriter` LIKE '%$underwriter_name%'";
		   
			  }
	     if($select_status!='Pending'){			  
              	
			   $sql .= "AND  `bundle_status` LIKE '%$select_status%'";
		   
			  }	else{
			 	  
			   $sql .= "AND  `bundle_status` LIKE ''";
			}		  
		      $query = $this->db->query($sql);		
		      $result= $query->result();
			  
			return $result;
			 
		}
	
}