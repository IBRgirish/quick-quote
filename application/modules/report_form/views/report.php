<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/header');
}
?>
<link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('css');?>/dataTables.tableTools.css" type="text/css"/>
<script src="<?php echo base_url('js/jquery.dataTables.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('js/dataTables.bootstrap.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('js');?>/dataTables.tableTools.js" type="text/javascript"></script>
<script src="<?php echo base_url('js');?>/TableTools.js" type="text/javascript"></script>
<script src="<?php echo base_url('js');?>/ZeroClipboard.js" type="text/javascript"></script>
<script src="<?php echo base_url('js');?>/chosen.jquery.min.js" type="text/javascript"></script>
<style>
input.btn.btn-primary {
  margin-top: -1px;
  margin-left: 10px;
}
div#officerList_paginate {
  display: none;
}
</style>
<script type="text/javascript">

$(document).ready(function () {



	 $("#officerList").dataTable({
		   "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                    "iDisplayLength": -1,
      "order": [[ 0, "asc" ]],
	  "dom" : 'CT<"clear">lfrtip',
            "oTableTools" : {
		         "sSwfPath":"<?php echo base_url();?>/swf/copy_csv_xls_pdf.swf",
				 "sRowSelect": "multi",
				 "aButtons" : [
				 {
                    "sExtends": "print",
                    "sPdfOrientation": "landscape"
                  },"select_all","select_none",
				  {
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    "sPdfMessage": "List of Quote.",
					"order": [[ 0, "desc" ]],
                 },
				  { 
				   "sExtends": "csv",
				   "sFieldBoundary": '"',				   				 
				   "sRowSelectOnly":true,
				   "order": 'current'
                   },
					]
					
            },
			
			
	  
});
var oTT = TableTools.fnGetInstance( 'officerList' );
    oTT.fnSelectAll();

//$('#officerList tbody tr').toggleClass('DTTT_selected selected');
//$('#ToolTables_officerList_1').toggleClass('DTTT_disabled');
		// $('.row_selected').toggleClass('selected');
			  var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
		
  /*  $('#officerList tbody').on( 'click', 'tr', function () {
       $('#officerList tbody').toggleClass('selected');
		//alert( table.rows('.selected').data().length +' row(s) selected' );
    } );*/
	
				
	    var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		$('#date_from').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

			$('#date_to').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		
});


function edit_shift(offId, day, month, year, shiftno ) {
	
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo site_url(); ?>/administration/schedule/exchange_shifts/?officerId='+offId+'&day='+day+'&month='+month+'&year='+year+'&shift='+shiftno,
    	autoSize: false,
    	closeBtn: true,
    	width: '600',
    	height: '500',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
	$(".fancybox-skin").easydrag();
	$(".fancybox-skin").css('padding','5');
	
}

function edit_officer(id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo site_url(); ?>/administration/officer/add_officer/'+id,
    	autoSize: false,
    	closeBtn: true,
    	width: '550',
    	height: '500',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
	$(".fancybox-skin").easydrag();
	$(".fancybox-skin").css('padding','5');
	//$(".fancybox-skin").css('overflow','hidden');

}


function view_officer(id)
{
	$.fancybox({
    	type: 'iframe',
    	href: '<?php echo site_url(); ?>/administration/officer/view_officer/'+id,
    	autoSize: false,
    	closeBtn: true,
    	width: '750',
    	height: '400',
    	closeClick: true,
    	enableEscapeButton: true,
    	beforeLoad: function () {},
	});
	$(".fancybox-skin").easydrag();
	$(".fancybox-skin").css('padding','5');
	//$(".fancybox-skin").css('overflow','hidden');

}
 function select_value_show(val){
	 
	 $('.date_show').hide(); 
	 $('#date_show_'+val).show();
	
      
		
	 }
</script>

<br>

<h1>Reports</h1>
  <div class="row-fluid">

    <form action="<?php echo base_url(); ?>report_form/show_report" method="post"
    >
    <div class="row-fluid">
    <div class="span2">    
    <select name="select_value" class="span12" onchange="select_value_show(this.value)">
    <option value="1" <?php if($select_value==1){echo 'selected';}?>>Today</option>
    <option value="2" <?php if($select_value==2){echo 'selected';}?>>Current Month</option>
    <option value="3" <?php if($select_value==3){echo 'selected';}?>>Current Week</option>
    <option value="4" <?php if($select_value==4){echo 'selected';}?>>Date of Ranges</option>
 
    </select>
    </div>
    <div class="span2 date_show" <?php if($select_value==2){ }else{echo 'style="display:none;"';}?> id="date_show_2" > 
    
     <?php $current_month =  date('F', mktime(0, 0, 0, date('m'), 1)); ?>    
    <input type="text" class="span10" name="week_sdate"  value="<?php echo $current_month?>" readonly="readonly"/>
    </div>
    <div class="span3 date_show" <?php if($select_value==3){ }else{echo 'style="display:none;"';}?> id="date_show_3" > 
      
     <?php              
$current_week_sdate = date("Y-m-d",strtotime('monday this week'));

$current_week_edate = date("Y-m-d",strtotime("sunday this week"));

?> 
<input type="text" name="week_sdate" class="span5" value="<?php echo $current_week_sdate?>" readonly="readonly"/>&nbsp;&nbsp;To&nbsp;&nbsp;
<input type="text" name="week_edate" class="span5" value="<?php echo $current_week_edate?>" readonly="readonly"/>

    </div>
    
    <div class="span3 date_show" <?php if($select_value==4){ }else{echo 'style="display:none;"';}?> id="date_show_4" >   
  
     
    <span class="span6" > 
    Start Date <br/><input type="text" name="date_from" value="<?php  echo  isset($date_from) ? $date_from : '';?>" id="date_from" class="span12"/>
    </span>

    <span class="span6" > 
    End Date<br/> <input type="text" name="date_to" value="<?php  echo  isset($date_to) ? $date_to : '';?>" id="date_to" class="span12"/>
    </span>
    </div>
    
    <div class="span2"> 
    <select name="broker_name" class="left_pull span12 chzn-select" id="broker_name"  >
    <option <?php if($broker_email == '' ) echo 'selected="selected"'; ?> value="" >Select</option>        
					 <?php
					
								foreach($broker_inform as $broker){
							    $broker_name1= isset($broker->first_name) ? $broker->first_name : '';
								$broker_name2= isset($broker->middle_name) ? $broker->middle_name : '';
								$broker_name3=isset($broker->last_name) ? $broker->last_name : '';
								
								if($broker_name2!=0 && $broker_name2!= 'null'){
								
								$broker_name4=$broker_name2;
								
								}
								if($broker_name3!=0 && $broker_name3!= 'null'){
								
								$broker_name5=$broker_name3;
								
								}
								$broker_name=$broker_name1.' '.$broker_name4.' '.$broker_name5;
								
							
								
								?>
                
                <option <?php if($broker->email == $broker_email ) echo 'selected="selected"'; ?> value="<?php  echo  isset($broker->email) ? $broker->email : '';?>" ><?php echo  isset($broker_name) ? $broker_name : ''; ?></option>
                       <?php }   ?>
				   </select>
    </div>
    <div class="span2" > 
    <select name="underwriter_name" class="left_pull span12 chzn-select" id="underwriter_name"  >
    <option <?php if($select_underwriter_id == '' ) echo 'selected="selected"'; ?> value="" >Select</option> 
					 <?php
					
						foreach($underwriter_inform as $underwriter){
							 $underwriter_name= isset($underwriter->name) ? $underwriter->name : '';                             $underwriter_id= isset($underwriter->id) ? $underwriter->id : '';
												
								?>
                               
                <option <?php if($underwriter_id == $select_underwriter_id ) echo 'selected="selected"'; ?> value="<?php  echo  isset($underwriter_id) ? $underwriter_id : '';?>" ><?php echo  isset($underwriter_name) ? $underwriter_name : ''; ?></option>
                       <?php }   ?>
				   </select>
    </div>
     <div class="span2">    
    <select name="select_status" class="span12" >
    <option value="" <?php if($select_value==""){echo 'selected';}?>>Select</option>
    <option value="Pending" <?php if($select_status=="Pending"){echo 'selected';}?>>Pending</option>
    <option value="Released" <?php if($select_status=="Released"){echo 'selected';}?>>Released</option>
    <option value="Accepted" <?php if($select_status=="Accepted"){echo 'selected';}?>>Accepted</option>
    <option value="Rejected" <?php if($select_status=="Rejected"){echo 'selected';}?>>Rejected</option>  
     <option value="Cancelled" <?php if($select_status=="Cancelled"){echo 'selected';}?>>Cancelled</option>
     <option value="Revision Requested" <?php if($select_status=="Revision Requested"){echo 'selected';}?>>Revision Requested</option>
 
    </select>
    </div>
    <input type="submit" class="btn btn-primary" name="submit" value="Submit"/>
    </form>
    </div>

<div class="table1">
<label style="font-size:13px; color:green"></label>
  <!-- siteuser List -->
  <table id="officerList" width="100%" class="dataTable table table-striped table-bordered">
    <thead>
      <tr>
        <th width="10%">Quote No.</th>
        <th width="10%">Date Submitted</th>
        <th width="10%">Person Submitting</th>
        <th width="10%">Agency Name</th>
        <th width="15%">Coverage Date</th>
        <th width="10%">Agency Phone No.</th>
        <th width="10%">Insured Full Name/DBA</th>
        <th width="10%">Underwriter Name</th>
        <th width="10%">Status</th>
 
      </tr>
    </thead>
    <tbody>
      
    <?php
       if(!empty($informations)){
		  
		foreach($informations as $information) { 
		  echo"<tr>";
			echo"<td>".$information->quote_id."</td>";
	    	echo"<td>".date('m/d/Y', strtotime( $information->date_added))."</td>";
			echo"<td>".$information->contact_name.' '.$information->contact_middle_name.' '.$information->contact_last_name."</td>"; 
			echo"<td>".get_agency_detail($information->requester_id, 'first_name')."</td>";
			echo"<td>".$information->coverage_date."</td>";
			echo"<td>".get_agency_detail($information->requester_id, 'phone_number')."</td>";
			echo"<td>".get_agency_details($information->quote_id,'insured_fname').' '.get_agency_details($information->quote_id,'insured_mname').' '.get_agency_details($information->quote_id,'insured_lname').'/'.get_agency_details($information->quote_id,'insured_dba')."</td>";
			if($select_underwriter_id!=''){
			echo"<td>".get_underwriter_detail($select_underwriter_id, 'name')."</td>";
			}else{
			echo"<td>".get_underwriter_details($information->assigned_underwriter, 'name')."</td>";	
			}
			if($information->bundle_status!=''){
			echo"<td>".$information->bundle_status."</td>";
			}else{
			echo"<td>Pending</td>";	
			}
         echo"</tr>";
		} }
	?>		
  
    </tbody>
  </table>

  
</div>

<style>
.selected
{
	background-color:#E1E1E1;	
}
.fancybox-skin {
    padding: 20px !important;
}
div#date_show_4 {
  margin-top: -20px;
}
</style>