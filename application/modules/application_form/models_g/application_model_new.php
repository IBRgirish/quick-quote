<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class application_model extends CI_Model {


    public function &__get($key) {
        $CI = & get_instance();
        return $CI->$key;
        $this->load->database();
    }
	public function getAmount($money)
	{
		$cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
		$onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);
	
		$separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;
	
		$stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
		$removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);
	
		return (float) str_replace(',', '.', $removedThousendSeparator);
	}
	
	protected function _update_phone($id, $data, $table)
	{
		$this->db->where('quote_id', $id); 
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}
        
		return FALSE;
		
	}
		protected function _update_email($id, $data, $table)
	{
		$this->db->where('quote_id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
		}
		
	
		protected function _updatequote($id, $data, $table)
	{
		$this->db->where('general_id', $id);
		
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
		public function _updateapp1($id, $data, $table,$id1,$table_id)
	{
	
	
		$this->db->where('ref_id', $id);
		if($table_id!=''){
		$this->db->where($table_id, $id1);
	
			}	
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
			protected function _updateapp2($id, $data, $table,$id1,$table_id,$type_loss)
	{
	
	
		$this->db->where('ref_id', $id);
		if($table_id!=''){
		$this->db->where($table_id, $id1);
		     
			}	
			
			if($type_loss=='losses_liability'){
			$this->db->where('losses_liability', $type_loss);
			
			}
			
			
			if($type_loss=='losses_pd'){
			$this->db->where('losses_pd', $type_loss);
			
			}
			if($type_loss=='losses_cargo'){
			$this->db->where('losses_cargo', $type_loss);
			
			}
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
			public function _updateapp($id, $data, $table)
	{
	
	
		$this->db->where('ref_id', $id);
	
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	

	
		public function get_database_inform($id='',$table){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('ref_id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
			public function get_database_inform3($id='',$table,$id1=''){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('ref_id', $id);
     	}
		    if($id1!='')
	    {
		$this->db->where('vehicle_id', $id1);
     	}
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
			public function get_database_inform1($id='',$table,$condition){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('ref_id', $id);
		$this->db->where('losses_type', $condition);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
	
			public function get_database_inform2($id='',$table,$condition){
		
		  $this->db->select('*');
	      $this->db->from($table);
	     if($id!='')
	    {
		$this->db->where('ref_id', $id);
		$this->db->where('vehicle_type', $condition);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
	
	/*public function	insertGeneral($data,$table_name){
	//print_r($data);
	$data1 = array_filter($data);
		if(!empty($data1)){
		$this->db->insert($table_name, $data);
		
		// 
		}
		}*/
		
		public function get_broker_inform($id=''){
		
		  $this->db->select('*');
	      $this->db->from('rq_members');
	     if($id!='')
	    {
		$this->db->where('id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		function get_broker_information($id=''){
		
		
		  $this->db->select('*');
	      $this->db->from('app_broker_inform');
	     if($id!='')
	    {
		$this->db->where('broker_id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		
				public function get_broker_quote($id=''){
		
		  $this->db->select('*');
	      $this->db->from('rq_rqf_quote');
	     if($id!='')
	    {
		$this->db->where('quote_id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		
	public function get_broker_insured($id=''){
		
		  $this->db->select('*');
	      $this->db->from('rq_rqf_insured_info');
	     if($id!='')
	    {
		$this->db->where('quote_id', $id);
     	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
		
		}
		
		function replaceArrayToString($arr)
 {

 $array['quote'] = $this->input->post($arr);
	//print_r($array['quote']);
	if(empty($array['quote'])){
		
	}else{
foreach($array['quote'] as $value)
{
    $array_temp[] =  $value;
	
}
$arr = implode(',', $array_temp);

 return $arr;
 }
 }
 
 function replaceArrayToString1($arr)
 {

 $array['quote'] = $this->input->post($arr);
	//print_r($array['quote']);
	if(empty($array['quote'])){
		
	}else{
foreach($array['quote'] as $value)
{
    $array_temp[] =  $value;
	
}
$arr = implode('-', $array_temp);

 return $arr;
 }
 }
/*	function quote_id()
 {
    $this->db->select('*');
	$this->db->from('application_general');
		
	$this->db->order_by('applicant_id', 'desc');
	$query = $this->db->get();
   
	$result = $query->result();
	return $result;
 }*/
	
	function app_applicant_db($id='') {
    $app_name = $this->input->post('app_name');
	
	$quote_id = $this->input->post('quote_id');
	
	
	if ($_POST) 
		{
    $insert_data_general['quote_id'] = $quote_id;
	$insert_data_general['company'] = $this->input->post('company');	
	$insert_data_general['app_name']=isset($app_name) ? implode(',',$app_name) : '';
	$insert_data_general['Quote_num'] = $this->input->post('Quote_num');
	$insert_data_general['Quote_num_new'] = $this->input->post('Quote_num_new');	
	$insert_data_general['effective_from'] = $this->input->post('effective_from');
	$insert_data_general['expiration_from'] = $this->input->post('expiration_from');
	$insert_data_general['audit_name'] = $this->input->post('audit_name');
	$insert_data_general['Specify_oth'] = $this->input->post('Specify_oth');
	$insert_data_general['remarks'] = $this->input->post('remarks');
	$insert_data_general['status'] = 'Draft';
	
	
	//die;
	//if($id!='')
	//{
	//$this->_updatequote($id, $insert_data_general,'application_general');
//	} else {
	$this->db->insert('application_general',$insert_data_general);
//	$insert_id=$this->db->insert_id();
  //   }
	$brker_tel= $this->input->post('brokr_tel');
	 
	$insert_data_broker['ref_id'] = isset($insert_id) ? $insert_id : $id;
	$insert_data_broker['quote_id'] = $quote_id;
	$insert_data_broker['broker_name'] = $this->input->post('broker_name');	
	$insert_data_broker['broker_name_dba'] = $this->input->post('broker_name_dba');
	$insert_data_broker['broker_address'] = $this->input->post('broker_address');	
	$insert_data_broker['brokr_tel'] = isset($brker_tel) ? implode('',$brker_tel) : ''; 
	$insert_data_broker['broker_tel_ext'] = $this->input->post('broker_tel_ext');
	$insert_data_broker['broker_email'] = $this->input->post('broker_email');
	$insert_data_broker['broker_remarks'] = $this->input->post('broker_remarks');
	
	// if($id!='')
	//{
	//$this->_updateapp($id, $insert_data_broker,'app_broker_inform');
	
	//} else {
	$this->db->insert('app_broker_inform',$insert_data_broker);
	
   //  }
	 
	 
	//$up_insert_id=$this->input->post('up_insert_id', true);
	
	
	$insert_data_applicant['ref_id'] = isset($insert_id) ? $insert_id : $id;
	$insert_data_applicant['quote_id'] = $quote_id;	
	$insert_data_applicant['insured_first_name'] = $this->input->post('insured_first_name');
	$insert_data_applicant['insured_middle_name'] = $this->input->post('insured_middle_name');
	$insert_data_applicant['insured_last_name'] = $this->input->post('insured_last_name');
	$insert_data_applicant['dba'] = $this->input->post('dba');
	$insert_data_applicant['app_address'] = $this->input->post('app_address');
	$insert_data_applicant['app_city'] = $this->input->post('app_city');
	$insert_data_applicant['state_app'] = $this->input->post('state_app');
	$insured_zip1 = $this->input->post('insured_zip1', true);
    $insured_zip2 = $this->input->post('insured_zip2', true);
	 $insured_zip=$insured_zip1.'-'.$insured_zip2;
//	die;
	$insert_data_applicant['insured_zip']=isset($insured_zip) ? $insured_zip  : '';
    $insured_telephone = $this->input->post('insured_telephone', true);
	$insert_data_applicant['insured_telephone']=isset($insured_telephone) ? implode('-',$insured_telephone) : '';	
	//$insert_data_applicant['insured_zip'] = $this->replaceArrayToString1('insured_zip');
	//$insert_data_applicant['insured_telephone'] = $this->replaceArrayToString1('insured_telephone');	
	$insert_data_applicant['insured_tel_ext'] = $this->input->post('insured_tel_ext');
	$insert_data_applicant['insured_email'] = $this->input->post('insured_email');
	$insert_data_applicant['other_contact'] = $this->input->post('Other_contact');	

	


	 
	// if($id!='')
	//{
	//$apid = $this->input->post('applicant_id', true);
	
	$this->_updateapp($id, $insert_data_applicant,'application_applicant');
//	} else {
	//$this->db->insert('application_general',$insert_data_general);
	$this->db->insert('application_applicant',$insert_data_applicant);
	
 //    }
	 
	 
	 $insured_telephone_add1 = $this->input->post('insured_telephone_add1', true);
	 $insured_telephone_add2 = $this->input->post('insured_telephone_add2', true);
	 $insured_telephone_add3 = $this->input->post('insured_telephone_add3', true);
	 $insured_telephone_type = $this->input->post('insured_telephone_type', true);
	 $insured_tel_ext_add = $this->input->post('insured_tel_ext_add', true);
	 
	 
	 $insured_telephone_attention = $this->input->post('insured_telephone_attention', true);
	
	 $insured_email_add = $this->input->post('insured_email_add', true);
	
	 $insured_email_type = $this->input->post('insured_email_type', true);
		
	 $insured_email_attention = $this->input->post('insured_email_attention', true);
	 $tel_id = $this->input->post('tel_id', true);
	 $email_id = $this->input->post('email_id', true);
	
	  // print_r($insured_telephone_attention);
	 // $email_tel_id = $this->input->post('email_tel_id', true);
	
	
	for ($m=0; $m < count($insured_telephone_add1); $m++) {
    
	if($insured_telephone_add1[$m]!=''){
	
    $insert_data_applicant1['ref_id'] = isset($insert_id) ? $insert_id : $id;
	$insert_data_applicant1['quote_id'] = $quote_id;	
	$insert_data_applicant1['insured_telephone_add']=isset($insured_telephone_add1[$m]) ? $insured_telephone_add1[$m].'-'.$insured_telephone_add2[$m].'-'.$insured_telephone_add3[$m] : '';	
	$insert_data_applicant1['insured_telephone_type']=isset($insured_telephone_type[$m]) ? $insured_telephone_type[$m] : '';	
	$insert_data_applicant1['insured_telephone_attention']=isset($insured_telephone_attention[$m]) ? trim($insured_telephone_attention[$m]) : '';
    $insert_data_applicant1['insured_tel_ext_add']=isset($insured_tel_ext_add[$m]) ? $insured_tel_ext_add[$m] : '';		
   
   
   
//	if(!empty($tel_id[$m])){
		
	//$this->_updateapp1($id, $insert_data_applicant1,'application_tel_add',$tel_id[$m],'tel_id');
	
//	}else{
	
	$this->db->insert('application_tel_add',$insert_data_applicant1);
	
	//	}
	
	}
	}
	//print_r($insert_data_applicant1);
	//die;
/*
 for ($o = $m; $o < count($insured_telephone_type); $o++) {
    //print_r($insured_telephone_type);die();
	
	if($insured_telephone_type[$o]!=''){
	//$insert_data_applicant1['tel_id'] = isset($tel_id[$o]) ? $tel_id[$o] :'' ;
	$insert_data_applicant1['ref_id'] = $insert_id;
	$insert_data_applicant1['quote_id'] = $quote_id;
	$insert_data_applicant1['insured_telephone_add']=isset($insured_telephone_add1[$o]) ? $insured_telephone_add1[$o].'-'.$insured_telephone_add2[$o].'-'.$insured_telephone_add3[$o] : '';	
	$insert_data_applicant1['insured_telephone_type']=isset($insured_telephone_type[$o]) ? $insured_telephone_type[$o] : '';	
	$insert_data_applicant1['insured_telephone_attention']=isset($insured_telephone_attention[$o]) ? $insured_telephone_attention[$o] : '';	
     $insert_data_applicant1['insured_tel_ext_add']=isset($insured_tel_ext_add[$o]) ? $insured_tel_ext_add[$o] : '';

	$this->db->insert('application_tel_add',$insert_data_applicant1);
	//return  $result_id;
	
	}
	}*/
	
	
	for ($n=0 ; $n < count($insured_email_add); $n++) {
 
	if($insured_email_add[$n]!=''){
	//$insert_data_applicant12['email_id'] = isset($email_id[$n]) ? $email_id[$n] :'' ;
	$insert_data_applicant12['ref_id'] = isset($insert_id) ? $insert_id : $id;
	$insert_data_applicant12['quote_id'] = $quote_id;
	$insert_data_applicant12['insured_email_add']=isset($insured_email_add[$n]) ? $insured_email_add[$n] : '';	
	$insert_data_applicant12['insured_email_type']=isset($insured_email_type[$n]) ? $insured_email_type[$n] : '';
	$insert_data_applicant12['insured_email_attention']=isset($insured_email_attention[$n]) ? $insured_email_attention[$n] : '';
	
 //  if(!empty($email_id[$n])){
	$this->_updateapp1($id, $insert_data_applicant12,'application_email_add',$email_id[$n],'email_id');
	
	//}else{
	$this->db->insert('application_email_add',$insert_data_applicant12);
	
	//	}
	//$this->db->insert('application_email_add',$insert_data_applicant12);
	
	}
	}
	//print_r($insert_data_applicant12);
	

	/*for ($l =$n; $l < count($insured_email_add); $l++) {
   if($insured_email_add[$l]!=''){
	
	$insert_data_applicant12['quote_id'] = $quote_id;
	$insert_data_applicant12['ref_id'] = $insert_id;
    $insert_data_applicant12['insured_email_add']=isset($insured_email_add[$l]) ? $insured_email_add[$l] : '';	
	$insert_data_applicant12['insured_email_type']=isset($insured_email_type[$l]) ? $insured_email_type[$l] : '';
	$insert_data_applicant12['insured_email_attention']=isset($insured_email_attention[$l]) ? $insured_email_attention[$l] : '';
	

	$this->db->insert('application_email_add',$insert_data_applicant12);
	}
	}*/
	
	

	
	
	  
	 }

	 
	}

	
	
	
function app_losses_db($id='') {

  $insert_id=$this->getid();
  //print_r($insert_id);

$quote_id = $this->input->post('quote_id');
$losses_id = $this->input->post('losses_id');
 
	if ($_POST) 
		{
	$insert_data_losses = array(
	'ref_id' => !empty($id) ?$id : $insert_id[0]->general_id,
	'quote_id'=>$quote_id,
	'app_garage'=>$this->input->post('app_garage'),
	'audit_name_losses' => $this->input->post('audit_name_losses'),
	'audit_name_other' => $this->input->post('audit_name_other'),
	'audit_SSN' => $this->input->post('audit_SSN'),
	'applicant_ever' => $this->input->post('applicant_ever'),
	'explain_business' => $this->input->post('explain_business'),
	'applicant_conducted' => $this->input->post('applicant_conducted'),
	'explain_conducted' => $this->input->post('explain_conducted'),
	'applicant_transportation' => $this->input->post('applicant_transportation'),	
	'losses_need' => $this->input->post('losses_need'),
	'losses_name' => $this->input->post('losses_name')
	
	);
	
	
	
	
	
//	 if($id!='')
	//{
	//	if($losses_id!=''){
	//$this->_updateapp($id, $insert_data_losses,'application_losses');
	//print_r($insert_data_losses);
//	}else{
		$this->db->insert('application_losses',$insert_data_losses);
	//	}
//	} else {
//	$this->db->insert('application_losses',$insert_data_losses);
	//print_r($insert_data_losses);
	//die;
     }
	//print_r($insert_data_losses);
	
			//Start losses section
		$losses_fields = array('losses_liability','losses_pd','losses_cargo','losses_bobtail','losses_dead','losses_trunk','losses_excess');
     /*   $lia_id = $this->input->post('lia_id', true);
        $pd_id = $this->input->post('pd_id', true);
		$cargo_id = $this->input->post('cargo_id', true);
		$bobtail_id = $this->input->post('bobtail_id', true);
		$trunk_id = $this->input->post('trunk_id', true);
		$excess_id = $this->input->post('excess_id', true);
		$dead_id = $this->input->post('dead_id', true);*/
                $loss_id = $this->input->post('loss_id', true);
		        $from = $this->input->post('losses_from');
				$to = $this->input->post('losses_to');
				$amount = $this->input->post('losses_amount');
				$company = $this->input->post('losses_company');
				$general_agent = $this->input->post('losses_general_agent');
				$date_of_loss = $this->input->post('losses_date_of_loss');
				$files_values = $this->input->post('uploaded_losses_files_values');
			
	
	           
				//$losses_sec = $this->input->post('losses_sec');
//	print_r($loss_id);
		//die;
 /*if(!empty($loss_id))
	{		
    for ($j = 0; $j < count($loss_id); $j++) 
	
	
				{ 
				if ($from[$j] != '') 
					{ 
					
					
					   
	
				        $insert_data16['ref_id'] = !empty($id) ? $id : $insert_id[0]->general_id;
		
						
						$insert_data16['quote_id'] = $quote_id;
						
						$insert_data16['losses_from_date'] = isset($from[$j]) ? date("Y-m-d",strtotime($from[$j])) : '';
						$insert_data16['losses_to_date'] =  isset($to[$j]) ? date("Y-m-d", strtotime($to[$j])) :'';
						$insert_data16['losses_amount'] = isset($amount[$j]) ?$amount[$j] :'';
						$insert_data16['losses_company'] = isset($company[$j]) ?$company[$j] :'';
						$insert_data16['losses_general_agent'] = isset($general_agent[$j]) ?$general_agent[$j] :'';
						$insert_data16['date_of_loss'] = isset($date_of_loss[$j]) ? date("Y-m-d", strtotime($date_of_loss[$j])) :'';
						$insert_data16['uploaded_files_values'] = isset($files_values[$j]) ? $files_values[$j] :'';	
					
//	$this->_updateapp($id, $insert_data1,'application_quote_losses');
                  
            
	                 $this->_updateapp1($id, $insert_data16,'application_quote_losses',$loss_id[$j],'losses_id');
					
				    }
	
				}
	
	       } 
	
	
		} */
	
	    foreach($losses_fields as $heading)
			{
				//print_r($heading);
				$losses_from = $this->input->post($heading.'_from');
				$losses_to = $this->input->post($heading.'_to');
				$losses_amount = $this->input->post($heading.'_amount');
				$losses_company = $this->input->post($heading.'_company');
				$losses_general_agent = $this->input->post($heading.'_general_agent');
				$losses_date_of_loss = $this->input->post($heading.'_date_of_loss');
				$uploaded_files_values = $this->input->post('uploaded_files_values');
				$losses_sec = $this->input->post($heading.'_sec');
	
				for ($i = 0; $i < count($losses_from); $i++) 
				{
					if ($losses_from[$i] != '') 
					{ 
						if($heading=='losses_other1' || $heading=='losses_other2')
						{
							$heading = $heading.'_specify_other';
						}
					    $insert_data1['ref_id'] = !empty($id) ? $id : $insert_id[0]->general_id;
						$insert_data1['quote_id'] = $quote_id;
						
						$insert_data1['losses_from_date'] = isset($losses_from[$i]) ? date("Y-m-d",strtotime($losses_from[$i])) : '';
						$insert_data1['losses_to_date'] =  isset($losses_to[$i]) ? date("Y-m-d", strtotime($losses_to[$i])) :'';
						$insert_data1['losses_amount'] = isset($losses_amount[$i]) ?$losses_amount[$i] :'';
						$insert_data1['losses_company'] = isset($losses_company[$i]) ?$losses_company[$i] :'';
						$insert_data1['losses_general_agent'] = isset($losses_general_agent[$i]) ?$losses_general_agent[$i] :'';
						$insert_data1['date_of_loss'] = isset($losses_date_of_loss[$i]) ? date("Y-m-d", strtotime($losses_date_of_loss[$i])) :'';
						$insert_data1['uploaded_files_values'] = isset($uploaded_files_values[$i]) ? $uploaded_files_values[$i] :'';
					  // print_r($insert_data1);
					     $insert_data1['losses_type'] = $heading; 
	                   
                         $this->db->insert('application_quote_losses',$insert_data1);
	        
					   }
					     
		}
	}
}//End Losses Section
	
	//print_r($insert_uploaded_files);
function app_commodity_db($id='') {
$insert_id=$this->getid();
$quote_id = $this->input->post('quote_id');
$in_id=$insert_id[0]->general_id;

 $application_commodity_id = $this->input->post('application_commodity_id');



	if ($_POST) 
		{
	$commodity_cargo_per = $this->input->post('commodity_cargo_per');	
	$commodity_cargo_val = $this->input->post('commodity_cargo_val');
	//$insert_data_commodity = array(
	$insert_data_commodity['ref_id'] =!empty($id) ? $id: $insert_id[0]->general_id;
	$insert_data_commodity['quote_id']=$quote_id;
	$insert_data_commodity['commodity_number']=$this->input->post('commodity_number');
	$insert_data_commodity['aapli_name']=$this->input->post('aapli_name');
	$insert_data_commodity['commodity_cargo_per']=isset($commodity_cargo_per) ? implode(',',$commodity_cargo_per) : '';
	$insert_data_commodity['commodity_cargo_val']=isset($commodity_cargo_val) ? implode(',',$commodity_cargo_val) : '';
	$insert_data_commodity['aapli_name_other']=$this->input->post('aapli_name_other');
	$insert_data_commodity['commodity_other_per']=$this->input->post('commodity_other_per');
	$insert_data_commodity['commodity_other_val']=$this->input->post('commodity_other_val');
	
	
	
/*	 if($application_commodity_id!='')
	{
	$this->_updateapp($id, $insert_data_commodity,'application_commodity');
	
	} else {*/
	//$this->db->insert('application_losses',$insert_data_losses);
	// print_r($insert_data_commodity);
	// die;
	$this->db->insert('application_commodity',$insert_data_commodity);
    // }
	 
	
	$application_transported_id = $this->input->post('application_transported_id');
	
	$insert_data_transported = array(
	'ref_id' =>!empty($id) ? $id: $insert_id[0]->general_id,
	'quote_id'=>$quote_id,
	'transported_name' => $this->input->post('transported_name'),
	'transported_desc' => $this->input->post('transported_desc'),
	'hazmat_name' => $this->input->post('hazmat_name'),	
	'passengers_name' => $this->input->post('passengers_name'),
	);
	
	/*	 if($application_transported_id!='')
	{
	$this->_updateapp($id, $insert_data_transported,'application_transported');
	
	} else {*/
	//$this->db->insert('application_losses',$insert_data_losses);
	$this->db->insert('application_transported',$insert_data_transported);
  //   }
	
	
		$app_fill_id = $this->input->post('app_fill_id');
     
	
	$insert_data_filings = array(
	 'ref_id' =>!empty($id) ? $id: $insert_id[0]->general_id,
	'quote_id'=>$quote_id,
	'filings_name' => $this->input->post('filings_name'),
	'filings_number' => $this->input->post('filings_number'),		
	'filings_type' => $this->input->post('filings_type'),
	'filings_num' => $this->input->post('filings_num'),	
	'state_fill' => $this->input->post('state_fill'),
	'trucking_comp_name' =>$this->input->post('trucking_comp_name'),
	'trucking_comp_other' => $this->input->post('trucking_comp_other'),
	'certificate_name' => $this->input->post('certificate_name'),
	'cert_num' => $this->input->post('cert_num'),
	);
	
/*	
			 if($app_fill_id!='')
	{
	$this->_updateapp($id, $insert_data_filings,'application_filings');
	
	} else {*/
	//$this->db->insert('application_losses',$insert_data_losses);
	$this->db->insert('application_filings',$insert_data_filings);
  //   }
	
	
	$cert_id = $this->input->post('cert_id');		
	$cert_comp = $this->input->post('cert_comp');
	$cert_attent = $this->input->post('cert_attent');	
	$cert_add = $this->input->post('cert_add');
	$cert_city = $this->input->post('cert_city');
	$cert_email = $this->input->post('cert_email');	
	$cert_telephone = $this->input->post('cert_telephone');
	$cert_telephone_ext = $this->input->post('cert_telephone_ext');
	$cert_fax = $this->input->post('cert_fax');
    $state_cert = $this->input->post('state_cert');		
	$cert_zip = $this->input->post('cert_zip');

//die;
	for ($p = 0; $p < count($cert_comp); $p++) {

	if ($cert_comp[$p] != '') {
	$insert_data_certificate['ref_id'] =!empty($id) ? $id: $insert_id[0]->general_id;	
	$insert_data_certificate['quote_id']=$quote_id;
	$insert_data_certificate['cert_comp']=isset($cert_comp[$p]) ? $cert_comp[$p] : '';
	$insert_data_certificate['cert_attent']=isset($cert_attent[$p]) ? $cert_attent[$p] : '';
	$insert_data_certificate['cert_add']=isset($cert_add[$p]) ? $cert_add[$p] : '';
	$insert_data_certificate['cert_city']=isset($cert_city[$p]) ? $cert_city[$p] : '';
	$insert_data_certificate['cert_email']=isset($cert_email[$p]) ? $cert_email[$p] : '';
	$insert_data_certificate['cert_telephone']=isset($cert_telephone[$p]) ? implode('-',$cert_telephone) : '';
	$insert_data_certificate['cert_telephone_ext']=isset($cert_telephone_ext[$p]) ? $cert_telephone_ext[$p] : '';
	$insert_data_certificate['cert_fax']=isset($cert_fax[$p]) ? implode('-',$cert_fax) : '';
	$insert_data_certificate['state_cert']=isset($state_cert[$p]) ? $state_cert[$p] : '';
	$insert_data_certificate['cert_zip']=isset($cert_zip[$p]) ? $cert_zip[$p] : '';
	
	//print_r($insert_data_certificate);
/*	 if(!empty($cert_id[$p]))
	{


	$this->_updateapp1($id, $insert_data_certificate,'application_certificate',$cert_id[$p],'application_certificate_id');
	
	} else {*/
	//$this->db->insert('application_losses',$insert_data_losses);
	//$this->db->insert('application_filings',$insert_data_filings);
	$this->db->insert('application_certificate',$insert_data_certificate);
 //    }
	
	
    }
	}
	
	$application_add_ins_id = $this->input->post('application_add_ins_id');
	
	$insert_data_Additional = array(
	'ref_id' =>!empty($id) ? $id: $insert_id[0]->general_id,
	'quote_id'=>$quote_id,
	'Additional_ins' => $this->input->post('Additional_ins'),	
	'owner_vehicle' => $this->input->post('owner_vehicle'),
	'insurance_val' => $this->input->post('insurance_val'),
	'ins_Policy_num' => $this->input->post('ins_Policy_num'),
	'ins_Policy_comp' => $this->input->post('ins_Policy_comp'),	
	'effective_from_ins' => $this->input->post('effective_from_ins'),
	'expiration_from_ins' => $this->input->post('expiration_from_ins'),
	'uploaded_files_values_second' => $this->input->post('uploaded_files_values_second'),	
	'owner_license' => $this->input->post('owner_license'),
	'owner_applicant' => $this->input->post('owner_applicant'),
	'applicant_scheduled' => $this->input->post('applicant_scheduled'),
	'applicant_rent' => $this->input->post('applicant_rent'),
	
	);
		
	/*		 if($application_add_ins_id!='')
	{
	$this->_updateapp($id, $insert_data_Additional,'application_additional');
	
	} else {*/
	//$this->db->insert('application_losses',$insert_data_losses);
	$this->db->insert('application_additional',$insert_data_Additional);
 //    }
	
	$application_owner_op_id = $this->input->post('application_owner_op_id');
	
	$insert_data_owner_operator = array(
	'ref_id' =>!empty($id) ? $id: $insert_id[0]->general_id,
	'quote_id'=>$quote_id,
	'owner_operator' => $this->input->post('owner_operator'),	
	'broker_loads' => $this->input->post('broker_loads'),
	'inspection__name' => $this->input->post('inspection__name'),
	'if_yes_name' => $this->input->post('if_yes_name'),
	'Estimated_financial' => $this->input->post('Estimated_financial'),	
	'Gross_receipts' => $this->input->post('Gross_receipts'),
	'Estimated_next' => $this->input->post('Estimated_next'),
	);
	
	/*			 if($application_owner_op_id!='')
	{
	$this->_updateapp($id, $insert_data_owner_operator,'application_owner_operator');
	
	} else {*/
	//$this->db->insert('application_losses',$insert_data_losses);
	$this->db->insert('application_owner_operator',$insert_data_owner_operator);
 //    }

	
	}
}
	 // print_r($insert_data_general);
	  
	  //print_r($insert_data_applicant);
		//$this->db->insert('application_Additional',$insert_data_Additional);
function app_vehicle_db($id='') {
$insert_id=$this->getid();
$insert_v_id='';
$quote_id = $this->input->post('quote_id');
	if ($_POST) 
		{
	/*-------------------------------------------------------------------------------------------*/		
			//Truck Section End	
			
			
			$vehicle_id = $this->input->post('vehicle_id');
			$veh_id = $this->input->post('veh_id');
			$v_id = $this->input->post('v_id');
		//	print_r($v_id);
			//die;
			$select_vehicle_year = $this->input->post('select_vehicle_year');
			$select_model = $this->input->post('select_model');
			$select_GVW = $this->input->post('select_GVW');
			$select_VIN = $this->input->post('select_VIN');
			
			$select_Operation = $this->input->post('select_Operation');
			$select_Radius = $this->input->post('select_Radius');
			$state_driving = $this->input->post('state_driving');
			$select_applicant = $this->input->post('select_applicant');
			
			$select_applicant_driver = $this->input->post('select_applicant_driver');
			$select_ch_radius = $this->input->post('select_ch_radius');
			$select_ch_cities = $this->input->post('select_ch_cities');
			$select_ch_applicant = $this->input->post('select_ch_applicant');
			$select_Cities = $this->input->post('select_Cities');
			
			$select_special = $this->input->post('select_special');
			$select_Axels = $this->input->post('select_Axels');
			$select_num_drivers = $this->input->post('select_num_drivers');
			
			
			$select_equipment = $this->input->post('select_equipment');
			$put_on_truck = $this->input->post('put_on_truck');
		    $pd_damage = $this->input->post('pd_damage');
			$cd_damage = $this->input->post('cd_damage');
			
			 
			$pd_rate = $this->input->post('pd_rate');
			$pd_loss_payee = $this->input->post('pd_loss_payee');
			$pd_coverage = $this->input->post('pd_coverage');
			$pd_coverage_specify = $this->input->post('pd_coverage_specify');
		
			$pd_premium = $this->input->post('pd_premium');
			$pd_deductible = $this->input->post('pd_deductible');
			$pd_loss_name = $this->input->post('pd_loss_name');
			$pd_loss_address = $this->input->post('pd_loss_address');
			
			$pd_loss_city = $this->input->post('pd_loss_city');
			$pd_loss_state = $this->input->post('pd_loss_state');				
			$pd_loss_zip = $this->input->post('pd_loss_zip');
			$pd_loss_remarks = $this->input->post('pd_loss_remarks');
			
			$pd_amount_limit = $this->input->post('pd_amount_limit');				
			$pd_deductible_limit = $this->input->post('pd_deductible_limit');
			$pd_premium_limit = $this->input->post('pd_premium_limit');
			
			
			$coverage_name = $this->input->post('coverage_name');			
			$cargo_avg_val = $this->input->post('cargo_avg_val');
			$cargo_avg_per = $this->input->post('cargo_avg_per');
			$cargo_max_val = $this->input->post('cargo_max_val');
			$cargo_max_per = $this->input->post('cargo_max_per');
			
			
				
			$cargo_factor = $this->input->post('cargo_factor');
			$cargo_prem = $this->input->post('cargo_prem');
			$cargo_deduct = $this->input->post('cargo_deduct');
			
			
			$cargo_amount_limit = $this->input->post('cargo_amount_limit');
			$cargo_amount_deduct = $this->input->post('cargo_amount_deduct');
			$cargo_amount_premium = $this->input->post('cargo_amount_premium');
			$cargo_breakdown_limit = $this->input->post('cargo_breakdown_limit');
			$cargo_breakdown_deduct = $this->input->post('cargo_breakdown_deduct');
			$cargo_breakdown_premium = $this->input->post('cargo_breakdown_premium');
			
			
			$garage_address = $this->input->post('garage_address');
			$garage_city = $this->input->post('garage_city');
			$garage_zip = $this->input->post('garage_zip');
			$garage_remarks = $this->input->post('garage_remarks');
			
			
			
			
			$driver_fname = $this->input->post('driver_fname');			
			$driver_mname = $this->input->post('driver_mname');
			$driver_lname = $this->input->post('driver_lname');
			$driver_license_num = $this->input->post('driver_license_num');
			
			$driver_license_class = $this->input->post('driver_license_class');
			$driver_date_hired = $this->input->post('driver_date_hired');
			$driver_class_A = $this->input->post('driver_class_A');
			$driver_remarks = $this->input->post('driver_remarks');
			$driver_surcharge = $this->input->post('driver_surcharge');
			
					
			for ($j = 0; $j < count($select_GVW)-1; $j++) {

				if ($select_vehicle_year[$j] != '') {
					$insert_data2['quote_id'] = $quote_id;
					$insert_data2['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
					$insert_data2['vehicle_type'] = 'TRUCK';
			
					$insert_data2['select_vehicle_year'] = $select_vehicle_year[$j];
					$insert_data2['select_model'] = $select_model[$j];
					$insert_data2['select_GVW'] = $select_GVW[$j];
					$insert_data2['select_VIN'] = $select_VIN[$j];
					$insert_data2['select_Operation'] = $select_Operation[$j];
					
					$insert_data2['select_Radius'] = isset($select_Radius[$j]) ? $select_Radius[$j] : '' ;
					$insert_data2['state_driving'] = isset($state_driving[$j]) ? $state_driving[$j] : '' ;
					$insert_data2['select_applicant'] = isset($select_applicant[$j]) ? $select_applicant[$j] : '' ;
					$insert_data2['select_applicant_driver'] = isset($select_applicant_driver[$j]) ? $select_applicant_driver[$j] : '' ;
					$insert_data2['select_Cities'] = isset($select_Cities[$j]) ? $select_Cities[$j] : '' ;
					
					$insert_data2['select_ch_radius'] = isset($select_ch_radius[$j]) ? $select_ch_radius[$j] : '' ;
					$insert_data2['select_ch_cities'] = isset($select_ch_cities[$j]) ? $select_ch_cities[$j] : '' ;
					$insert_data2['select_ch_applicant'] = isset($select_ch_applicant[$j]) ? $select_ch_applicant[$j] : '' ;
					$insert_data2['select_special'] = isset($select_special[$j]) ? $select_special[$j] : '' ;
					
					$insert_data2['select_Axels'] = isset($select_Axels[$j]) ? $select_Axels[$j] : '' ;
					$insert_data2['select_num_drivers'] = isset($select_num_drivers[$j]) ? $select_num_drivers[$j] : '' ;
					$insert_data2['select_equipment'] = isset($select_equipment[$j]) ? $select_equipment[$j] : '';
					$insert_data2['put_on_truck'] = isset($put_on_truck[$j]) ? $put_on_truck[$j] : '';
					
					$insert_data2['pd_damage'] = isset($pd_damage[$j]) ? $pd_damage[$j] : '';
					$insert_data2['cd_damage'] = isset($cd_damage[$j]) ? $cd_damage[$j] : '';
				   				
					//$insert_data2['pd_amount2'] = $pd_amount2[$j];

		
					
					$insert_data2['pd_loss_name'] = isset($pd_loss_name[$j]) ? $pd_loss_name[$j] : '';
					$insert_data2['pd_loss_address'] = isset($pd_loss_address[$j]) ? $pd_loss_address[$j] : '';			
					$insert_data2['pd_loss_city'] = isset($pd_loss_city[$j]) ? $pd_loss_city[$j] : '';
					$insert_data2['pd_loss_state'] = isset($pd_loss_state[$j]) ? $pd_loss_state[$j] : '';
					$insert_data2['pd_loss_zip'] = isset($pd_loss_zip[$j]) ? $pd_loss_zip[$j] : '';	
					
					
					$insert_data2['pd_loss_remarks'] = isset($pd_loss_remarks[$j]) ? $pd_loss_remarks[$j] : '';
					$insert_data2['pd_amount_limit'] = isset($pd_loss_name[$j]) ? $pd_amount_limit[$j] : '';
					$insert_data2['pd_deductible_limit'] = isset($pd_deductible_limit[$j]) ? $pd_deductible_limit[$j] : '';			
					$insert_data2['pd_premium_limit'] = isset($pd_premium_limit[$j]) ? $pd_premium_limit[$j] : '';
					
					
					
					$insert_data2['coverage_name'] = isset($coverage_name[$j]) ? $coverage_name[$j] : '';					
					$insert_data2['cargo_avg_val'] = isset($cargo_avg_val[$j])? $cargo_avg_val[$j] : '';
					$insert_data2['cargo_avg_per'] = isset($cargo_avg_per[$j])? $cargo_avg_per[$j] : '';
					$insert_data2['cargo_max_val'] = isset($cargo_max_val[$j])? $cargo_max_val[$j] : '';					
					$insert_data2['cargo_max_per'] = isset($cargo_max_val1[$j])? $cargo_max_per[$j] : '';
					
								
					$insert_data2['cargo_deduct'] = isset($cargo_deduct[$j])? $cargo_deduct[$j] : '';
					$insert_data2['cargo_factor'] = isset($cargo_factor[$j])? $cargo_factor[$j] : '';			
					
					
					
					
					$insert_data2['cargo_amount_limit'] = isset($cargo_amount_limit[$j])? $cargo_amount_limit[$j] : '';
					$insert_data2['cargo_amount_deduct'] = isset($cargo_amount_deduct[$j])? $cargo_amount_deduct[$j] : '';			
					$insert_data2['cargo_amount_premium'] = isset($cargo_amount_premium[$j])? $cargo_amount_premium[$j] : '';
					$insert_data2['cargo_breakdown_limit'] = isset($cargo_breakdown_limit[$j])? $cargo_breakdown_limit[$j] : '';
					$insert_data2['cargo_breakdown_deduct'] = isset($cargo_breakdown_deduct[$j])? $cargo_breakdown_deduct[$j] : '';			
					$insert_data2['cargo_breakdown_premium'] = isset($cargo_breakdown_premium[$j])? $cargo_breakdown_premium[$j] : '';
					
					$insert_data2['garage_address'] = isset($garage_address[$j])? $garage_address[$j] : '';
					$insert_data2['garage_city'] = isset($garage_city[$j])? $garage_city[$j] : '';				
					$insert_data2['garage_zip'] = isset($garage_zip[$j])? $garage_zip[$j] : '';
					$insert_data2['garage_remarks'] = isset($garage_remarks[$j])? $garage_remarks[$j] : '';					
					$insert_data2['driver_fname'] = isset($driver_fname[$j])? $driver_fname[$j] : '';
					
					$insert_data2['driver_mname'] = isset($driver_mname[$j])? $driver_mname[$j] : '';				
					$insert_data2['driver_lname'] = isset($driver_lname[$j])? $driver_lname[$j] : '';
					$insert_data2['driver_license_num'] = isset($driver_license_num[$j])? $driver_license_num[$j] : '';				
					$insert_data2['driver_license_class'] = isset($driver_license_class[$j])? $driver_license_class[$j] : '';	
					
					$insert_data2['driver_date_hired'] = isset($driver_date_hired[$j])? $driver_date_hired[$j] : '';					
					$insert_data2['driver_class_A'] = isset($driver_class_A[$j])? $driver_class_A[$j] : '';
					$insert_data2['driver_remarks'] = isset($driver_remarks[$j])? $driver_remarks[$j] : '';			
					$insert_data2['driver_surcharge'] = isset($driver_surcharge[$j])? $driver_surcharge[$j] : '';	
					
					//$insert_data2['commodities_haulted'] = $commodities_haulted_truck[$j];
		       //   print_r($insert_data2);
					/*
					$cargo_spec_post = $this->input->post(($j+1).'truck_cargo_specifier');
					//$cargo_specifier = isset() ? $this->input->post(($j+1).'truck_cargo_specifier') : '';
					
					if(!empty($cargo_spec_post)){
						$insert_data2['cargo_specifier'] = implode(',', $cargo_spec_post );
					} else {
						$insert_data2['cargo_specifier'] = '';
					}*/
				//	print_r($insert_data2);
			//
				
		/*		if(!empty($vehicle_id[$j])){
					$this->_updateapp1($id, $insert_data2,'application_vehicle',$vehicle_id[$j],'vehicle_id');
					
					}else{*/
					$insert_v_id=$this->db->insert_id();
					$this->db->insert('application_vehicle', $insert_data2);
					//print_r($result);
			//	}
				}
				
				
				
					
			}
			for ($t = 0; $t < count($pd_rate)-1; $t++) {
			
				if ($pd_rate[$t] != '') {
					$insert_data21['vehicle_id'] =isset($veh_id[$t]) ? $veh_id[$t] : $insert_v_id;
					$insert_data21['quote_id'] = $quote_id;
					$insert_data21['vehicle_type'] = 'TRUCK';
					$insert_data21['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
				    $insert_data21['pd_rate'] = isset($pd_rate[$t]) ? $pd_rate[$t] : '';
					$insert_data21['pd_coverage'] = isset($pd_coverage[$t]) ? $pd_coverage[$t] : '';
					
					
					$insert_data21['pd_coverage_specify'] = isset($pd_coverage_specify[$t]) ? $pd_coverage_specify[$t] : '';				
					$insert_data21['pd_deductible'] = isset($pd_deductible[$t]) ? $pd_deductible[$t] : '';
					$insert_data21['pd_premium'] = isset($pd_premium[$t]) ? $pd_premium[$t] : '';
					$insert_data21['pd_loss_payee'] = isset($pd_loss_payee[$t]) ? $pd_loss_payee[$t] : '';	
				/*		if(!empty($v_id[$t])){
					$this->_updateapp1($id, $insert_data21,'application_vehicle_pd',$v_id[$t],'v_id');
					
					}else{*/
					
					$this->db->insert('application_vehicle_pd', $insert_data21);
					//}
				  }
				}
				//print_r($insert_data21);
			//Truck Section End
		/*-------------------------------------------------------------------------------------------*/	
		
	    // For TRACTOR
			$t_vehicle_id = $this->input->post('t_vehicle_id');
			$t_veh_id = $this->input->post('t_veh_id');
			$t_v_id = $this->input->post('t_v_id');
		
			$t_select_vehicle_year = $this->input->post('t_select_vehicle_year');
			$t_select_model = $this->input->post('t_select_model');
			$t_select_GVW = $this->input->post('t_select_GVW');
			$t_select_VIN = $this->input->post('t_select_VIN');
			
			$t_select_Operation = $this->input->post('t_select_Operation');
			$t_select_Radius = $this->input->post('t_select_Radius');
			$t_state_driving = $this->input->post('t_state_driving');
			$t_select_applicant = $this->input->post('t_select_applicant');
			
			$t_select_applicant_driver = $this->input->post('t_select_applicant_driver');
			$t_select_ch_radius = $this->input->post('t_select_ch_radius');
			$t_select_ch_cities = $this->input->post('t_select_ch_cities');
			$t_select_ch_applicant = $this->input->post('t_select_ch_applicant');
			
			$t_select_special = $this->input->post('t_select_special');
			$t_select_Axels = $this->input->post('t_select_Axels');
			$t_select_num_drivers = $this->input->post('t_select_num_drivers');
			$t_select_Cities = $this->input->post('t_select_Cities');
			//$t_vehicle_trailer_no=$this->post('t_vehicle_trailer_no');
			$t_select_equipment = $this->input->post('t_select_equipment');
			$t_put_on_truck = $this->input->post('t_put_on_truck');
			$t_pd_damage = $this->input->post('t_pd_damage');
			$t_cd_damage = $this->input->post('t_cd_damage');
			
			$t_pd_rate = $this->input->post('t_pd_rate');
			$t_pd_loss_payee = $this->input->post('t_pd_loss_payee');
			$t_pd_coverage = $this->input->post('t_pd_coverage');
			$t_pd_coverage_specify = $this->input->post('t_pd_coverage_specify');
			
			$t_pd_premium = $this->input->post('t_pd_premium');
			$t_pd_deductible = $this->input->post('t_pd_deductible');
			$t_pd_loss_name = $this->input->post('t_pd_loss_name');
			$t_pd_loss_address = $this->input->post('t_pd_loss_address');
			
			$t_pd_loss_city = $this->input->post('t_pd_loss_city');
			$t_pd_loss_state = $this->input->post('t_pd_loss_state');				
			$t_pd_loss_zip = $this->input->post('t_pd_loss_zip');
			$t_pd_loss_remarks = $this->input->post('t_pd_loss_remarks');
			
			$t_pd_amount_limit = $this->input->post('t_pd_amount_limit');				
			$t_pd_deductible_limit = $this->input->post('t_pd_deductible_limit');
			$t_pd_premium_limit = $this->input->post('t_pd_premium_limit');
			
			
			$t_coverage_name = $this->input->post('t_coverage_name');			
			$t_cargo_avg_val = $this->input->post('t_cargo_avg_val');
			$t_cargo_avg_per = $this->input->post('t_cargo_avg_per');
			$t_cargo_max_val = $this->input->post('t_cargo_max_val');
			$t_cargo_max_per = $this->input->post('t_cargo_max_per');
			
			
				
			$t_cargo_factor = $this->input->post('t_cargo_factor');
			$t_cargo_prem = $this->input->post('t_cargo_prem');
			$t_cargo_deduct = $this->input->post('t_cargo_deduct');
			
			
			$t_cargo_amount_limit = $this->input->post('t_cargo_amount_limit');
			$t_cargo_amount_deduct = $this->input->post('t_cargo_amount_deduct');
			$t_cargo_amount_premium = $this->input->post('t_cargo_amount_premium');
			$t_cargo_breakdown_limit = $this->input->post('t_cargo_breakdown_limit');
			$t_cargo_breakdown_deduct = $this->input->post('t_cargo_breakdown_deduct');
			$t_cargo_breakdown_premium = $this->input->post('t_cargo_breakdown_premium');
		
		
		
			$t_garage_address = $this->input->post('t_garage_address');			
			$t_garage_city = $this->input->post('t_garage_city');
			$t_garage_zip = $this->input->post('t_garage_zip');
			$t_garage_remarks = $this->input->post('t_garage_remarks');
			$t_driver_fname = $this->input->post('t_driver_fname');
			
				
			$t_driver_mname = $this->input->post('t_driver_mname');
			$t_driver_lname = $this->input->post('t_driver_lname');
			$t_driver_license_num = $this->input->post('t_driver_license_num');
			
			$t_driver_license_class = $this->input->post('t_driver_license_class');
			$t_driver_date_hired = $this->input->post('t_driver_date_hired');
			$t_driver_class_A = $this->input->post('t_driver_class_A');
			$t_driver_remarks = $this->input->post('t_driver_remarks');
			$t_driver_surcharge = $this->input->post('t_driver_surcharge');
			
			for ($k = 0; $k < count($t_select_GVW)-1; $k++) {

				if ($t_select_vehicle_year[$k] != '') {
					$insert_data3['quote_id'] = $quote_id;
					$insert_data3['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
					$insert_data3['vehicle_type'] = 'TRACTOR';
			
					$insert_data3['select_vehicle_year'] = $t_select_vehicle_year[$k];
					$insert_data3['select_model'] = $t_select_model[$k];
					$insert_data3['select_GVW'] = $t_select_GVW[$k];
					$insert_data3['select_VIN'] = $t_select_VIN[$k];
					$insert_data3['select_Operation'] = $t_select_Operation[$k];
					
					$insert_data3['select_Radius'] = $t_select_Radius[$k];
					$insert_data3['state_driving'] = $t_state_driving[$k];
					$insert_data3['select_applicant'] = $t_select_applicant[$k];
					$insert_data3['select_applicant_driver'] = $t_select_applicant_driver[$k];
					
					
					$insert_data3['select_ch_radius'] = isset($t_select_ch_radius[$k]) ? $t_select_ch_radius[$k] : '' ;
					$insert_data3['select_ch_cities'] = isset($t_select_ch_cities[$k]) ? $t_select_ch_cities[$k] : '' ;
					$insert_data3['select_ch_applicant'] = isset($t_select_ch_applicant[$k]) ? $t_select_ch_applicant[$k] : '' ;
					$insert_data3['select_special'] = isset($t_select_special[$k]) ? $t_select_special[$k] : '' ;
					$insert_data3['select_Cities'] = isset($t_select_Cities[$k]) ? $t_select_Cities[$k] : '' ;
					$insert_data3['select_Axels'] = isset($t_select_Axels[$k]) ? $t_select_Axels[$k] : '' ;
					$insert_data3['select_num_drivers'] = isset($t_select_num_drivers[$k]) ? $t_select_num_drivers[$k] : '' ;
					$insert_data3['select_equipment'] = isset($t_select_equipment[$k]) ? $t_select_equipment[$k] : '';
					$insert_data3['put_on_truck'] = isset($t_put_on_truck[$k]) ? $t_put_on_truck[$k] : '';
				//	$insert_data3['vehicle_trailer_no'] = isset($t_vehicle_trailer_no[$k]) ? $t_vehicle_trailer_no[$k] : '';
					
					$insert_data3['pd_damage'] = isset($t_pd_damage[$k]) ? $t_pd_damage[$k] : '';
					$insert_data3['cd_damage'] = isset($t_cd_damage[$k]) ? $t_cd_damage[$k] : '';
								
					//$insert_data2['pd_amount2'] = $pd_amount2[$j];

		
					
					$insert_data3['pd_loss_name'] = isset($t_pd_loss_name[$k]) ? $t_pd_loss_name[$k] : '';
					$insert_data3['pd_loss_address'] = isset($t_pd_loss_address[$k]) ? $t_pd_loss_address[$k] : '';			
					$insert_data3['pd_loss_city'] = isset($t_pd_loss_city[$k]) ? $t_pd_loss_city[$k] : '';
					$insert_data3['pd_loss_state'] = isset($t_pd_loss_state[$k]) ? $t_pd_loss_state[$k] : '';
					$insert_data3['pd_loss_zip'] = isset($t_pd_loss_zip[$k]) ? $t_pd_loss_zip[$k] : '';	
					
					
					$insert_data3['pd_loss_remarks'] = isset($t_pd_loss_remarks[$k]) ? $t_pd_loss_remarks[$k] : '';
					$insert_data3['pd_amount_limit'] = isset($t_pd_amount_limit[$k]) ? $t_pd_amount_limit[$k] : '';
					$insert_data3['pd_deductible_limit'] = isset($t_pd_deductible_limit[$k]) ? $t_pd_deductible_limit[$k] : '';			
					$insert_data3['pd_premium_limit'] = isset($t_pd_premium_limit[$k]) ? $t_pd_premium_limit[$k] : '';
					
					
					
					$insert_data3['coverage_name'] = isset($t_coverage_name[$k]) ? $t_coverage_name[$k] : '';					
					$insert_data3['cargo_avg_val'] = isset($t_cargo_avg_val[$k])? $t_cargo_avg_val[$k] : '';
					$insert_data3['cargo_avg_per'] = isset($t_cargo_avg_per[$k])? $t_cargo_avg_per[$k] : '';
					$insert_data3['cargo_max_val'] = isset($t_cargo_max_val[$k])? $t_cargo_max_val[$k] : '';					
					$insert_data3['cargo_max_per'] = isset($t_cargo_max_val1[$k])? $t_cargo_max_per[$k] : '';
					
								
					$insert_data3['cargo_deduct'] = isset($t_cargo_deduct[$k])? $t_cargo_deduct[$k] : '';
					$insert_data3['cargo_factor'] = isset($t_cargo_factor[$k])? $t_cargo_factor[$k] : '';			
		
					
					
					
					$insert_data3['cargo_amount_limit'] = isset($t_cargo_amount_limit[$k])? $t_cargo_amount_limit[$k] : '';
					$insert_data3['cargo_amount_deduct'] = isset($t_cargo_amount_deduct[$k])? $t_cargo_amount_deduct[$k] : '';			
					$insert_data3['cargo_amount_premium'] = isset($t_cargo_amount_premium[$k])? $t_cargo_amount_premium[$k] : '';
					$insert_data3['cargo_breakdown_limit'] = isset($t_cargo_breakdown_limit[$k])? $t_cargo_breakdown_limit[$k] : '';
					$insert_data3['cargo_breakdown_deduct'] = isset($t_cargo_breakdown_deduct[$k])? $t_cargo_breakdown_deduct[$k] : '';			
					$insert_data3['cargo_breakdown_premium'] = isset($t_cargo_breakdown_premium[$k])? $t_cargo_breakdown_premium[$k] : '';
					

					
					$insert_data3['garage_address'] = isset($t_garage_address[$k]) ? $t_garage_address[$k] : '';
					$insert_data3['garage_city'] = isset($t_garage_city[$k]) ? $t_garage_city[$k] : '';					
					$insert_data3['garage_zip'] = isset($t_garage_zip[$k]) ? $t_garage_zip[$k] : '';
					$insert_data3['garage_remarks'] = isset($t_garage_remarks[$k]) ? $t_garage_remarks[$k] : '';					
					$insert_data3['driver_fname'] = isset($t_driver_fname[$k]) ? $t_driver_fname[$k] : '';
					
					$insert_data3['driver_mname'] = isset($t_driver_mname[$k]) ? $t_driver_mname[$k] : '';					
					$insert_data3['driver_lname'] = isset($t_driver_lname[$k]) ? $t_driver_lname[$k] : '';
					$insert_data3['driver_license_num'] = isset($t_driver_license_num[$k]) ? $t_driver_license_num[$k] : '';					
					$insert_data3['driver_license_class'] = isset($t_driver_license_class[$k]) ? $t_driver_license_class[$k] : '';
					
					$insert_data3['driver_date_hired'] = isset($t_driver_date_hired[$k]) ? $t_driver_date_hired[$k] : ''; 				
					$insert_data3['driver_class_A'] = isset($t_driver_class_A[$k]) ? $t_driver_class_A[$k] : ''; 
					$insert_data3['driver_remarks'] = isset($t_driver_remarks[$k]) ? $t_driver_remarks[$k] : '';				
					$insert_data3['driver_surcharge'] = isset($t_driver_surcharge[$k]) ? $t_driver_surcharge[$k] : '';
					
					/*$insert_data3['pd_damage_truck'] = $pd_damage_truck[$k];
					$insert_data3['cd_damage_truck'] = $cd_damage_truck[$k];
					$insert_data3['pd_val1'] = $pd_val1[$k];
					$insert_data3['pd_val2'] = $pd_val2[$k];
					
					
					$insert_data3['pd_rate1'] = $pd_rate1[$k];					
					$insert_data3['pd_rate2'] = $pd_rate2[$k];
					$insert_data3['pd_amount1'] = $pd_amount1[$k];					
					$insert_data3['pd_amount2'] = $pd_amount2[$k];
					
					$insert_data3['pd_deductible'] = $pd_deductible[$k];
					$insert_data3['pd_premium'] = $pd_premium[$k];
					$insert_data3['pd_loss_payee'] = $pd_loss_payee[$k];					
					//$insert_data3['pd_amount2'] = $pd_amount2[$k];
					
						print_r($insert_data2);
			//	die;
					
					//$insert_data2['commodities_haulted'] = $commodities_haulted_truck[$k];
		
					/*
					$cargo_spec_post = $this->input->post(($j+1).'truck_cargo_specifier');
					//$cargo_specifier = isset() ? $this->input->post(($j+1).'truck_cargo_specifier') : '';
					
					if(!empty($cargo_spec_post)){
						$insert_data2['cargo_specifier'] = implode(',', $cargo_spec_post );
					} else {
						$insert_data2['cargo_specifier'] = '';
					}*/
			
			/*			if(!empty($t_vehicle_id[$k])){
					$this->_updateapp1($id, $insert_data3,'application_vehicle',$t_vehicle_id[$k],'vehicle_id');
					
					}else{*/
					$insert_v_id=$this->db->insert_id();
					$this->db->insert('application_vehicle', $insert_data3);
					//print_r($result);
				//}
					//$this->db->insert('application_vehicle', $insert_data3);
					}
				}
						for ($q = 0; $q < count($t_pd_rate); $q++) {
			
				if ($t_pd_rate[$q] != '') {
					$insert_data31['vehicle_id'] =isset($t_veh_id[$q]) ? $t_veh_id[$q] : $insert_v_id;
					$insert_data31['quote_id'] = $quote_id;
					$insert_data31['vehicle_type'] = 'TRACTOR';
					$insert_data31['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
				    $insert_data31['pd_rate'] = isset($t_pd_rate[$q]) ? $t_pd_rate[$q] : '';
					$insert_data3['pd_coverage'] = isset($t_pd_coverage[$q]) ? $t_pd_coverage[$q] : '';
					
					
					$insert_data31['pd_coverage_specify'] = isset($t_pd_coverage_specify[$q]) ? $t_pd_coverage_specify[$q] : '';				
					$insert_data31['pd_deductible'] = isset($t_pd_deductible[$q]) ? $t_pd_deductible[$q] : '';
					$insert_data31['pd_premium'] = isset($t_pd_premium[$q]) ? $t_pd_premium[$q] : '';
					$insert_data31['pd_loss_payee'] = isset($t_pd_loss_payee[$q]) ? $t_pd_loss_payee[$q] : '';	
					//print_r($insert_data31);
					//print_r($t_v_id);
				//die;
			/*			if(!empty($t_v_id[$q])){
					$this->_updateapp1($id, $insert_data31,'application_vehicle_pd',$t_v_id[$q],'v_id');
					
					}else{
					*/
					$this->db->insert('application_vehicle_pd', $insert_data31);
				//	}
				  }
				}
				
				
				// End of tractor
				
				 // For Trailer
				 
			$tr_vehicle_id = $this->input->post('tr_vehicle_id');
			$tr_select_vehicle_year = $this->input->post('tr_select_vehicle_year');
			$tr_select_model = $this->input->post('tr_select_model');
			$tr_select_GVW = $this->input->post('tr_select_GVW');
			$tr_select_VIN = $this->input->post('tr_select_VIN');
			
			$tr_select_Operation = $this->input->post('tr_select_Operation');
			$tr_select_Radius = $this->input->post('tr_select_Radius');
			$tr_state_driving = $this->input->post('tr_state_driving');
			$tr_select_applicant = $this->input->post('tr_select_applicant');
			$tr_select_Cities = $this->input->post('tr_select_Cities');
			$tr_select_applicant_driver = $this->input->post('tr_select_applicant_driver');
			$tr_select_ch_radius = $this->input->post('tr_select_ch_radius');
			$tr_select_ch_cities = $this->input->post('tr_select_ch_cities');
			$tr_select_ch_applicant = $this->input->post('tr_select_ch_applicant');
			
			$tr_select_special = $this->input->post('tr_select_special');
			$tr_select_Axels = $this->input->post('tr_select_Axels');
			$tr_select_num_drivers = $this->input->post('tr_select_num_drivers');
			
			
	
			
			for ($m = 0; $m < count($tr_select_GVW)-1; $m++) {

				if ($tr_select_vehicle_year[$m] != '') {
					$insert_data4['quote_id'] = $quote_id;
					$insert_data4['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;
					$insert_data4['vehicle_type'] = 'TRAILER';
			
					$insert_data4['select_vehicle_year'] = $tr_select_vehicle_year[$m];
					$insert_data4['select_model'] = $tr_select_model[$m];
					$insert_data4['select_GVW'] = $tr_select_GVW[$m];
					$insert_data4['select_VIN'] = $tr_select_VIN[$m];
					$insert_data4['select_Operation'] = $tr_select_Operation[$m];
					
					$insert_data4['select_Radius'] = isset($tr_select_Radius[$m]) ? $tr_select_Radius[$m] : '' ;
					$insert_data4['state_driving'] = isset($tr_state_driving[$m]) ? $tr_state_driving[$m] : '' ;
					$insert_data4['select_applicant'] = isset($tr_select_applicant[$m]) ? $tr_select_applicant[$m] : '' ;
					$insert_data4['select_applicant_driver'] =isset($tr_select_applicant_driver[$m]) ? $tr_select_applicant_driver[$m] : '' ;
					$insert_data4['select_Cities'] = isset($tr_select_Cities[$m]) ? $tr_select_Cities[$m] : '' ;
					
					$insert_data4['select_ch_radius'] = isset($tr_select_ch_radius[$m]) ? $tr_select_ch_radius[$m] : '' ;
					$insert_data4['select_ch_cities'] = isset($tr_select_ch_cities[$m]) ? $tr_select_ch_cities[$m] : '' ;
					$insert_data4['select_ch_applicant'] = isset($tr_select_ch_applicant[$m]) ? $tr_select_ch_applicant[$m] : '' ;
					$insert_data4['select_special'] = isset($tr_select_special[$m]) ? $tr_select_special[$m] : '' ;
					
					$insert_data4['select_Axels'] = isset($tr_select_Axels[$m]) ? $tr_select_Axels[$m] : '' ;
					$insert_data4['select_num_drivers'] = isset($tr_select_num_drivers[$m]) ? $tr_select_num_drivers[$m] : '' ;
					
					/*$insert_data4['select_equipment'] = $tr_select_equipment[$m];
					$insert_data4['put_on_truck'] = $tr_put_on_truck[$m];
					
					$insert_data3['pd_damage_truck'] = $pd_damage_truck[$k];
					$insert_data3['cd_damage_truck'] = $cd_damage_truck[$k];
					$insert_data3['pd_val1'] = $pd_val1[$k];
					$insert_data3['pd_val2'] = $pd_val2[$k];
					
					
					$insert_data3['pd_rate1'] = $pd_rate1[$k];					
					$insert_data3['pd_rate2'] = $pd_rate2[$k];
					$insert_data3['pd_amount1'] = $pd_amount1[$k];					
					$insert_data3['pd_amount2'] = $pd_amount2[$k];
					
					$insert_data3['pd_deductible'] = $pd_deductible[$k];
					$insert_data3['pd_premium'] = $pd_premium[$k];
					$insert_data3['pd_loss_payee'] = $pd_loss_payee[$k];					
					//$insert_data3['pd_amount2'] = $pd_amount2[$k];
					
					
					
					//$insert_data2['commodities_haulted'] = $commodities_haulted_truck[$k];
		
					/*
					$cargo_spec_post = $this->input->post(($j+1).'truck_cargo_specifier');
					//$cargo_specifier = isset() ? $this->input->post(($j+1).'truck_cargo_specifier') : '';
					
					if(!empty($cargo_spec_post)){
						$insert_data2['cargo_specifier'] = implode(',', $cargo_spec_post );
					} else {
						$insert_data2['cargo_specifier'] = '';
					}*/
				//	print_r($insert_data2);
				
			
				/*			if(!empty($tr_vehicle_id[$m])){
					$this->_updateapp1($id, $insert_data4,'application_vehicle',$tr_vehicle_id[$m],'vehicle_id');
					
					}else{*/
					//$insert_v_id=$this->db->insert_id();
					$this->db->insert('application_vehicle', $insert_data4);
					//print_r($result);
				}
					
				
					//$this->db->insert('application_vehicle', $insert_data4);
					}
				}   
				
		$loaded__id = $this->input->post('loaded__id');	
			
	$insert_data['Vehicle_declined'] = $this->input->post('Vehicle_declined');
	$insert_data['Vehicle_hauling'] = $this->input->post('Vehicle_hauling');
	
	$insert_data['quote_id'] = $quote_id;
	$insert_data['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;			
	$insert_data['loaded_trucks_name'] = $this->input->post('loaded_trucks_name');
	$insert_data['freight_amount'] = $this->input->post('freight_amount');
	$insert_data['other_amount'] = $this->input->post('other_amount');
	$insert_data['clause_amount'] = $this->input->post('clause_amount');
	
	
	if($loaded__id!=''){
	  $this->_updateapp($id, $insert_data,'application_loaded');
		}else{
			
			$this->db->insert('application_loaded', $insert_data);
			
			}
	
	$cover_id = $this->input->post('cover_id');	
	
	$insert_data5['quote_id'] = $quote_id;
	$insert_data5['ref_id'] = !empty($id) ? $id: $insert_id[0]->general_id;		
	$insert_data5['primary_cover_name'] = $this->input->post('primary_cover_name');
	$insert_data5['combined_amount'] = $this->input->post('combined_amount');
	$insert_data5['combined_deductible'] = $this->input->post('combined_deductible');
	$insert_data5['each_person'] = $this->input->post('each_person');
	$insert_data5['each_accident'] = $this->input->post('each_accident');
	$insert_data5['each_property'] = $this->input->post('each_property');
	$insert_data5['split_deductible'] = $this->input->post('split_deductible');
	$insert_data5['split_PIP'] = $this->input->post('split_PIP');
	$insert_data5['combined_name'] = $this->input->post('combined_name');

	if($cover_id!=''){
	  $this->_updateapp($id, $insert_data5,'application_coverage');
		}else{
			
			$this->db->insert('application_coverage', $insert_data5);
			
			}
	
	}
		
	}	
	
	
	
	
	
	public function get_broker_losses($id=''){
	$this->db->select('*');
	$this->db->from('rq_rqf_quote_losses');
	if($id!='')
	{
		$this->db->where('quote_id', $id);
	}
	
	$this->db->order_by('quote_id', 'desc');
	//$this->db->limit('1');
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
	
	}
	public function getid($id='')
{
	//echo "hello";
	
	$this->db->select('*');
	$this->db->from('application_general');
	if($id!='')
	{
		$this->db->where('general_id', $id);
	}
	
	$this->db->order_by('general_id', 'desc');
	$this->db->limit('1');
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
}
function get_data($id=''){
    $this->db->select('*');
	$this->db->from('application_general');
	if($id!='')
	{
		$this->db->where('general_id', $id);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;

}
public function get_data_applicant($id=''){

  $this->db->select('*');
	$this->db->from('application_applicant');
	if($id!='')
	{
		$this->db->where('ref_id', $id);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;

}
public function get_data_tel_add($id=''){

  $this->db->select('*');
	$this->db->from('application_tel_add');
	if($id!='')
	{
		$this->db->where('ref_id', $id);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;

}


public function get_data_email_add($id=''){

  $this->db->select('*');
	$this->db->from('application_email_add');
	if($id!='')
	{
		$this->db->where('ref_id', $id);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;

}
	//print_r($insert_data_applicant);
	
	
 function get_losses_db($id=''){
	$this->db->select('*');
	$this->db->from('application_losses');
	if($id!='')
	{
		$this->db->where('ref_id', $id);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
	
	}
	
function get_quote_losses_db($id=''){
	$this->db->select('*');
	$this->db->from('application_quote_losses');
	if($id!='')
	{
		$this->db->where('ref_id', $id);
	}
		
	$query = $this->db->get();
   
	$result = $query->result();
	
	return $result;
	
	}
	
	
}


	













/* End of file quote_model.php */
/* Location: ./application/models/quote_model.php */