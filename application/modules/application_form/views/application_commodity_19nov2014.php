<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php
/*if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');	
} */
?>


        <style>
		 .span_width1{
			   width: 40px!important;
			   }
            .table thead th {
                text-align: center;
                vertical-align: inherit;
            }

            .table th, .table td {
                font-size: 0.8em;
            }

            .row_added_vehicle{
                margin:0px 0px 5px 0px;
            }
            .status_msg {
                color: #ff0000;
            }
			
			#upload_file_name_second{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second input{
				display: none;
			}
			
			#upload_file_name_second li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second li span{
				width: 15px;
				height: 12px;
				background: url('<?php echo base_url();?>images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second li.error p{
				color:red;
			}

				#upload_file_name_second_div{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second_div li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second_div input{
				display: none;
			}
			
			#upload_file_name_second_div li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second_div li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second_div li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second_div li span{
				width: 15px;
				height: 12px;
				background: url('images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second_div li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second_div li.error p{
				color:red;
			}

			
			
			#upload_file_name1{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name1 li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name1 input{
				display: none;
			}
			
			#upload_file_name1 li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name1 li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name1 li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name1 li span{
				width: 15px;
				height: 12px;
				background: url('<?php echo base_url();  ?>images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name1 li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name1 li.error p{
				color:red;
			}
			
			#upload_file li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file input{
				display: none;
			}
			
			#upload_file li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file li span{
				width: 15px;
				height: 12px;
				background: url('<?php echo base_url();?>images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file li.error p{
				color:red;
			}
			
			.upload_file_name1{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			.upload_file_name1 li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			.upload_file_name1 input{
				display: none;
			}
			
			.upload_file_name1 li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			.upload_file_name1 li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			.upload_file_name1 li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			.upload_file_name1 li span{
				width: 15px;
				height: 12px;
				background: url('<?php echo base_url();  ?>images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			.upload_file_name1 li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			.upload_file_name1 li.error p{
				color:red;
			}
			
        </style>	 

<script type="text/javascript">
	
	
			

function apply_on_all_elements(){
		
		//$('input').not('.datepick').autotab_magic().autotab_filter();
		//$('input').not('.require').autotab_magic().autotab_filter();
		//$('select').not('.require').autotab_magic().autotab_filter();
		
		//$('input').attr('required', 'required');
		
		$('.price_format').priceFormat({
		prefix: '$ ',
		centsLimit: 0,
		thousandsSeparator: ','
		});
		
		$(".license_issue_date").mask("99/99/9999");
		
		//  $('input').attr('required', 'required');  
		//$('select').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		
		$('#vehicle_tractor').removeAttr('required');
		$('#vehicle_truck').removeAttr('required');
		$('#zip2').removeAttr( 'required' );
		$('.zip2').removeAttr( 'required' );
		$('.filing_numbers').removeAttr('required');
		$('.filing_type').removeAttr('required');
		$('.last_name').removeAttr('required');
		$('.middle_name').removeAttr('required');
		
		//$('#vehicle_pd').removeAttr('required');
		//$('#vehicle_liability').removeAttr('required');
		//$('#vehicle_cargo').removeAttr('required');
		$('#cab').removeAttr('required');
		$('#tel_ext').removeAttr('required');
		$('#insured_email').removeAttr('required');
		$('#insured_tel_ext').removeAttr('required');
		//alert();
		
		// $('form').h5Validate(); 	
		//alert($(".chosen").val());
}


function first_loadeds12(){

alert();
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file').value = '';
				$('#drop_seconded a').parent().find('input').click();
}
	$(document).ready(function() {


		/*	$('#first_uploadeds').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values').value = '';
				$('#drop a').parent().find('input').click();
				//alert($('#drop a').parent());
				
    		});

$('#first_loadeds12').click(function(){
				
    		});

			
			$('#first_upload_second').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values_second').value = '';
				$('#drop_second a').parent().find('input').click();
    		});
			
			
			
			
				$('#first_uploaded1').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file1').value = '';
				$('#drop_second1 a').parent().find('input').click();
				//alert(1);
    		});
			
					$('#first_uploaded16').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file16').value = '';
				$('#drop_second16 a').parent().find('input').click();
				//alert(1);
    		});
			
			
					$('#first_uploaded15').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file15').value = '';
				$('#drop_second15 a').parent().find('input').click();
				//alert(1);
    		});
			
			
			
				$('#first_uploaded2').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file2').value = '';
				$('#drop_second2 a').parent().find('input').click();
    		});
			
			
				$('#first_uploaded3').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file3').value = '';
				$('#drop_second3 a').parent().find('input').click();
    		});
			
				$('#first_uploaded4').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file4').value = '';
				$('#drop_second4 a').parent().find('input').click();
    		});
			
			$('#first_uploaded5').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file5').value = '';
				$('#drop_second5 a').parent().find('input').click();
    		});
			
				$('#first_uploaded6').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file6').value = '';
				$('#drop_second6 a').parent().find('input').click();
    		});
			
			
			
				$('#first_uploaded7').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file7').value = '';
				$('#drop_second7 a').parent().find('input').click();
    		});
			
			
			
				$('#first_uploaded8').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('upload_file8').value = '';
				$('#drop_second8 a').parent().find('input').click();
    		});
			
			
		
			
			
		apply_on_all_elements();
		$("#coverage_date").mask("99/99/9999");
		
	//	$('form').h5Validate();
		
		//$('input').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		//$('input[type="checkbox"]').removeAttr( 'required' );
		$('input[type="file"]').removeAttr( 'required' ); 
		
/* 		$('#coverage_date').datepicker({
			format: 'mm/dd/yyyy'
		}); */
		//$("#coverage_date").mask("99/99/9999");
 	 	/* var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

		var checkin = $('#coverage_date').datepicker({
		onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
		}).on('changeDate', function(ev) {
		// $(this).blur();
		checkin.hide();		 
		}).data('datepicker');   */
		
		/* $('#coverage_date').blur(function() {
  			$('#coverage_date').datepicker("hide");
		});
		 */
		
		//Hide datepicker on Focus Out		
	/* 	$('#coverage_date').blur(function(){
			//setTimeout(function(){$('#coverage_date').datepicker('hide');},100)
		}); */
		
		var date = new Date();
		date.setDate(date.getDate()-0);
				
		 var inputs1 = $('#coverage_date').datepicker({
			startDate: date,
			format: 'mm/dd/yyyy',
			autoclose: true
		});  


		
		$('#dba').change(function(){
			//check_insured_dba_required();			
		});
		
		$('#insured_name').change(function(){
		//alert();
			//check_insured_dba_required();			
		});

		

    var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }

//$('.request_quote').validateForm();
/* 
		var hiddenEls = $("body").find(":hidden").not("script");

	$("body").find(":hidden").each(function(index, value) {
	selctor= this.id;
	if(selctor){
		$( '#'+selctor+' :input').removeAttr("required");
		}
		alert(this.id);
	}); */
	
	/*  $(".request_quote").validateForm({
		ignore: ":hidden",
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			
			$('#loss_report_upload1').click(function() {  
				//var up_id = $('.upload_select:last').attr('id');
				//alert(up_id);
				$('#fileup1_1').click();  
				//$('#'+up_id).click();  
			
			});
			 
			$('#mvr_upload').click(function() { 
			$('#fileup_1').click();  });
 


	});
	
	
			
		function form_sub(){
		/* $(".request_quote").validateForm({
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			//$('.request_quote').validateForm();
			
/* 			if($('#theid').css('display') == 'none'){ 
   $('#theid').show('slow'); 
} else { 
   $('#theid').hide('slow'); 
}
$("br[style$='display: none;']") */


			
	}
	
	function check_insured_dba_required(){
			dba_val = $('#dba').val().trim();
			insured_val = $('#insured_name').val().trim();
			if(dba_val){
				$('#insured_name').removeAttr('required'); 
				$('#insured_middle_name').removeAttr('required');
				$('#dba').attr('required', 'required'); 
			}else if(insured_val){
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').removeAttr('required'); 		
			} else {
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').attr('required', 'required'); 	
			}
		}
		
	
	<!-----updated by amit03-03-2014------>
	$("body").on("change",".rofo:first",function(){
				val = $('.rofo:first').val();
				$('.rofo:first').attr("readonly",false)
				$('.rofothers:first').attr("readonly",false)
				otherval = $('.rofothers:first').val();
				$(".tractor_radius_of_operation").val(val);
				$(".truck_radius_of_operation").val(val);
				if(val == 'other')
				{
					$('.rofothers').show();
					$('.rofothers').val(otherval);
					$('.rofothers').not(":first").attr("readonly",true);
					$(".show_v_others").show();
				}
				
				$(".truck_radius_of_operation").not(":first").attr("readonly",true);
				$(".tractor_radius_of_operation").not(":first").attr("readonly",true);
				$('.show_tractor_others').not(":first").attr("readonly",true);
				$('.show_truck_others').not(":first").attr("readonly",true);
			});
			$("body").on("keyup",".rofothers:first",function(){
				
				$(".rofothers").val($(this).val());
			});
	
			function rofoperation(val , id){
			
				if(id=='1tractor_radius_of_operation')
				{							
					$(".tractor_radius_of_operation").val(val);
					$(".tractor_radius_of_operation").not(":first").attr("disabled",true);
					if(val == 'other'){
						$('.show_tractor_others').show();
						$(".specify_other_cp").not(":first").attr("disabled",true);
					} else {
						$('.show_tractor_others').hide();
					}	
				}
				else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').show();
					} else {
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').hide();
					}	
					
				}
				
				
				
			/*  alert(id);
			alert(val);  */
			
			
			}
			
			
			
			function rofoperation2(val , id){
				
				if(id=='1truck_radius_of_operation')
				{
					$(".truck_radius_of_operation").val(val);
					if(val == 'other'){
						$('.show_truck_others').show();
					} else {
						$('.show_truck_others').hide();
					}
				}	
							else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').show();
					} else {
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').hide();
					}	
					
				}
			}

				
				
				
				
			
					
			
		function check_commodities_other(id){ 
			$("#"+id+" option:selected").each(function(){
			  val = $(this).text(); 
			  if(val == 'other'){ 
					$('.'+id+'_otr').show();
				} else { 
					$('.'+id+'_otr').hide();
				}
			});
		}
	
	
	$(document).ready(function(e) {
        $("body").on("keyup",".commhauled:first",function(){
			$(".commhauled").val($(this).val());
		});
		$("body").on("keyup",".commhauled2:first",function(){
			$(".commhauled2").val($(this).val());
		});
		
		
		 $("body").on("keyup",".cargo_class_price:first",function(){
			$(".cargo_class_price").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_price2:first",function(){
			$(".cargo_class_price2").val($(this).val());
		});
		
		
		$("body").on("keyup",".cargo_class_ded:first",function(){
			$(".cargo_class_ded").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_ded2:first",function(){
			$(".cargo_class_ded2").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp:first",function(){
			$(".specify_other_cp").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp2:first",function(){
			$(".specify_other_cp2").val($(this).val());
		});
			/**Dba required if name if blank or name required if dba is blank By Ashvin Patel 12/may/2014**/

		$('#insured_name').blur(function(){
			//alert();
			var f_name = $(this).val();
			var l_name = $('#insured_last_name').val();
			if(f_name==''||l_name=='')
			{
				$('#dba').attr('required', 'required');
				
			}
			else
			{
				$('#dba').removeAttr('required');
				$('#dba').removeClass('ui-state-error');
			}
		});
	
		$('#insured_last_name').blur(function(){
			//alert();
			var l_name = $(this).val();
			var f_name = $('#insured_name').val();
			if(f_name==''||l_name=='')
			{
				$('#dba').attr('required', 'required');
			}
			else
			{
				$('#dba').removeAttr('required');
				$('#dba').removeClass('ui-state-error');
			}
		});
		$('#dba').blur(function(){
			var dba = $(this).val();
			var f_name = $('#insured_name').val();
			if(dba=='')
			{
				$('#insured_name').attr('required', 'required');
				$('#insured_last_name').attr('required', 'required');
			}
			else
			{
				$('#insured_name').removeAttr('required');
				$('#insured_last_name').removeAttr('required');
				$('#insured_name').removeClass('ui-state-error');
				$('#insured_last_name').removeClass('ui-state-error');
			}
		});
    });
	

		
   
	
	
	
	<!-----updated by amit03-03-2014------>
</script>
       
       <script>
$(document).ready(function(){
	$("#agencies").change(function(){
		//window.location.href = window.location.href + "/" + $(this).val();
		$("#broker_email").val($(this).text());
		
		$.ajax({
			url: '<?php echo base_url();?>manualquote/setpublishersession',
			type: "POST",
			data: {
					action:'via_ajax',
					publisherid : $(this).val()
			},
			success: function(data) {
				
				window.location.reload(true);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(thrownError);
			}
		});
	});
});
function update_unknown(val){
	var owned_veh = $('#trailer_owned_vehicle'+val).val();


	if(owned_veh == 'no'){
		$("#vehicle_year_vh"+val).removeAttr("selected");
		$("#vehicle_year_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#make_model_vh"+val).removeAttr("selected");
		$("#make_model_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#trailer_type_veh"+val).removeAttr("selected");
		$("#trailer_type_veh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
	}else{
		$("#vehicle_year_vh"+val+" option[value='Unknown']").remove();
		$("#make_model_vh"+val+" option[value='Unknown']").remove();
		$("#trailer_type_veh"+val+" option[value='Unknown']").remove();
	}
	/* vehicle_year_vh1-2
	make_model_vh1-2
	trailer_type_veh1-2 */
	//alert();
}


	
	//$("#coverage_date").mask("99/99/9999");
	
	/* ('#dp3').datepicker({
			//format: 'mm/dd/yyyy'
		}); */

    /* $("input").each(function() {
      this.value = "";}) */
    //add_driver();
    //alert($(window).width()); 
	
/* 	$( ".number_limit" ).validate({
rules: {
field: {

range: [1, 99]
}
}
}); */
jQuery(document).ready(function(){
	
	
	
	
	$(".chzn-select").chosen({ allow_single_deselect: true });
	
	
	$('#hours').timepicker({
    'minTime': '6:00am',
    'maxTime': '2:00pm',
    'showDuration': true
});
	
	$('#hour').timepicker({
    'minTime': '2:00pm',
    'maxTime': '10:00pm',
    'showDuration': true
});
	
		$('#hou').timepicker({
    'minTime': '10:00pm',
    'maxTime': '6:00am',
    'showDuration': true
});
		
	
	
});
</script>

<!--<h2 style="text-align:center;">Application Sheet</h2>
<form action="<?php echo base_url();?>application_form/commodity/<?php echo isset($app[0]->general_id) ? $app[0]->general_id : '';?>" method="post" accept-charset="utf-8" id="main_form" enctype="multipart/form-data" novalidate="novalidate" id="app_form2"/>-->
<div class="well">
     <input type="hidden" name="quote_id" value="<?php echo isset($quote_id) ? $quote_id :'';?>"/>
<!--<input type="hidden" id="id_text_app_val" name="id_text_app_val[]" value="<?php echo implode(",",$app_val);?>"/>-->
 <input type="hidden" name="ref_id" value="<?php echo isset($ref_id[0]->general_id) ? $ref_id[0]->general_id : '';?>"/>
 <input type="hidden" name="insert_id" value="<?php echo isset($app[0]->general_id) ? $app[0]->general_id : '';?>" id="insert_id"/>
<!--<input type="hidden" name="ref_id" value="<?php echo isset($app[0]->general_id) ? $app[0]->general_id : '';?>"/>-->
<input type="hidden" id="id_text_app_val" name="id_text_app_val[]" value="<?php echo isset($app_val[0]) ? $app_val[0] : '';?>"/>
<input type="hidden" value="<?php echo isset($app_own[0]->application_owner_op_id) ? $app_own[0]->application_owner_op_id : ''; ?>" name="application_owner_op_id" />
                            <!-- Vehicle Schedule Section Start -->	
                            <div class="row-fluid"> <span style="height:5px">&nbsp;</span> 
                            <fieldset class="section2 vehicle_schedule_box">
                                <legend>11.Commodities section</legend>

                            <input type="hidden" name="application_commodity_id" value="<?php echo isset($app_com[0]->application_commodity_id) ? $app_com[0]->application_commodity_id :'';?>"/>
                                <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div>
                 <div id="com_haul" class="row-fluid">           
                                <div class="row-fluid">  	
                                    <div class="span12 lightblue">
                                       <div class="span5">
                                            <label class="left_pull">Commodities hauled &nbsp;</label>
											<input type="button" class="icon icon-plus" style="background-color:white;" id="add_com_row1" value="+ Add" >
                                           <!-- <input class="input-mini span2" type="text" placeholder=""  name="commodity_number" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" value="<?php   if($app_com[0]->commodity_number==0) {}else echo $app_com[0]->commodity_number ;?>" onchange="add_commodity(this.value);">-->
											</div> 

                                         </div>
                                </div>	
                                 
                                <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div> 
<div class="row-fluid">
            <span class="span8">
              <table class="table">
                <thead>
                  <tr>                 
                    <th>Commodity</th>
                    <th class="cargo_show" >%</th>
                    <th class="cargo_show" >Value </th>
                  </tr>
                </thead>
                <tbody class="add_commodity_row">
                   <?php $aapli_name = isset($app_com[0]->aapli_name) ? $app_com[0]->aapli_name : '';
					 $com2=explode(',',$aapli_name);
					// print_r($com2);
					$k=1;
					 for($i=0;$i<count($com2);$i++){
					?>
                  <tr>
              
                    <td>
                 
                    
                    <select name="aapli_name[]" id="appli_id<?php echo $k; ?>" onchange="aaplic_other(<?php echo $k; ?>);">
                      	  
                 <option value="Appliances" <?php if($com2[$i]  == 'Appliances') echo 'selected="selected"'; ?> >Appliances</option>
                 <option value="Other" <?php if($com2[$i] == 'Other') echo 'selected="selected"'; ?>>Other</option>
                  
                      </select>
                     <br/>       <?php $commodity_cargo_val= isset($app_com[0]->commodity_cargo_val) ? $app_com[0]->aapli_name_other : '';
					         $com3=explode(',',$commodity_cargo_val);
					  ?>  
					 <?php  if($com2[$i] == 'Other')  { ?> 
					 <input class="textinput span8"   id="aapli_name_other<?php echo $k; ?>" value="<?php echo $com3[$i];?>" type="text" placeholder="" name="aapli_name_other[]" >
					 
					 <?php } else { ?>         
								 <input class="textinput span8" style="display:none;"  id="aapli_name_other<?php echo $k; ?>" value="" type="text" placeholder="" name="aapli_name_other[]" >
								
                                <?php $k++; }  ?> 
                                 </td>
                    </td>
                    <td class="cargo_show" >
                    <?php $commodity_cargo_per = isset($app_com[0]->commodity_cargo_per) ? $app_com[0]->commodity_cargo_per : '';
					$com=explode(',',$commodity_cargo_per);
					
					?>
                      <input class="textinput input-mini" type="text" placeholder="" value="<?php  if($com[$i]>0) {echo $com[$i];} ?>"   name="commodity_cargo_per[]">
                      
                      
                    </td >
                    <td class="cargo_show" >
                    <?php $commodity_cargo_val = isset($app_com[0]->commodity_cargo_val) ? $app_com[0]->commodity_cargo_val : '';
					$com1=explode(',',$commodity_cargo_val);
					
					?>
                      <input class="textinput input-mini" type="text" placeholder="" value="<?php  if($com1[$i]>0) {echo $com1[$i];} ?>" name="commodity_cargo_val[]">
                     
                    </td>
                    <td>					<a class="btn btn-mini btn-1 remove_commodity_row" name="button" id="remove_commodity_row"><i class="icon icon-remove"></i> </a>					</td>
                  </tr>
                  <?php } ?>
				  <tbody id="add_commodity_row">
				  
				  </tbody>
                  <tr id="other_tr" style="display:none" >
                    <td class="td-1"></td>
                    <td>
                      <label class="control-label pull-left">Other&nbsp;</label>
                  <!--    <input class="textinput span9" type="text" placeholder="" value="<?php echo isset($app_com[0]->commodity_cargo_val) ? $app_com[0]->aapli_name_other : ''; ?>" name="aapli_name_other">-->
                    </td>
                    <td>
                      <input class="textinput input-mini" type="text" placeholder="" value="<?php echo isset($app_com[0]->commodity_other_per) ? $app_com[0]->commodity_other_per : ''; ?>" name="commodity_other_per">
                    </td>
                    <td>
                      <input class="textinput input-mini" type="text" placeholder="" value="<?php echo isset($app_com[0]->commodity_other_val) ? $app_com[0]->commodity_other_val : ''; ?>" name="commodity_other_val">
                    </td>
                  </tr>
                </tbody>
              </table>
            </span>
           
          </div>
		  
		  </div>
		  <div class="row-fluid ">  	
                                    <span class="span6 lightblue">
           <input type="hidden" name="application_transported_id" value="<?php echo isset($app_trans[0]->application_transported_id) ? $app_trans[0]->application_transported_id :'';?>"/>
                                            <label class="control-label pull-left ">12.Does the applicant own the cargo transported?&nbsp;&nbsp;</label>
										
                                             <select class="pull-left pull-left-3 width_option" required name="transported_name" id="transported_id" onChange="trans_val();">
               
	<?php echo  $transported_name = isset($app_trans[0]->transported_name) ? $app_trans[0]->transported_name : '';?>
                 <option value="" >Select</option>
                <option value="No" <?php if($transported_name  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($transported_name  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
				           </span>
							 </div>				
             <div id="text_no" style="display:none">   
             <div class="row-fluid">  
			
								
								<div class="span4 lightblue">
                                    <label> 12.A.Name </label>
									
                                   <input type="text" maxlength="255"  class="span4 first_name ui-state-valid " placeholder="First Name" name="insured_first_name1" id="" value="<?php echo isset($app_trans[0]->insured_first_name) ? $app_trans[0]->insured_first_name :''; ?>"/>
									<input type="text"  class="span4 middle_name" name="ins_middle_name1 " placeholder="Middle Name" id="" value="<?php echo isset($app_trans[0]->insured_middle_name) ? $app_trans[0]->insured_middle_name : '';?>">
									<input type="text"  class="span4 last_name " placeholder="Last Name" name="insured_last_name1" id="" value="<?php echo isset($app_trans[0]->insured_last_name) ? $app_trans[0]->insured_last_name : '';?>">
                             		</div>
                                    
							   <div class="span3 lightblue">
                                    <label>12.B.DBA(if applicable)</label>
                                   <input id="" name="dba1" type="text" class="span12 " maxlength="255" value="<?php echo isset($app_trans[0]->dba) ? $app_trans[0]->dba:'' ;?>">
                                </div>
							         
                                       <div class="span3 lightblue">
                                        <label>12.C.Address</label> 
									                                  
										<input class="textinput span12" type="text" placeholder="Address" name="app_address1" value="<?php //echo isset($broker_insured[0]->insured_dba) ? $broker_insured[0]->insured_dba : '';?><?php echo isset($app_trans[0]->app_address) ? $app_trans[0]->app_address : '';?>">
                                    
									
                                </div>		
                                <div class="span2 lightblue">
                                    <label >12.D.City</label>
                                    <input id="garaging_city" name="app_city1" type="text" class="span8" value="<?php echo isset($app_trans[0]->app_city) ? $app_trans[0]->app_city : '';?>">
                                </div>								

                            </div>	

                            <div class="row-fluid">  
							
							 <div class="span1 lightblue">
                                    <label style="margin-bottom: 11px;">12.E.Country</label>
                            	<?php 
								 $add_country='AD';
									//$attr = "class='span12 ui-state-valid'";
									$app_country = isset($app_trans[0]->country_app) ? $app_trans[0]->country_app : '';
									
									?>
									<?php //echo get_state_dropdown('state_app', $broker_state, $attr); ?>
                            <?php echo  getCountryDropDown1($name='country_app1', $att='style="width:100%;" id="country_app" onChange="getState(this.value, &quot;state_app1&quot;, &quot;state_app&quot; )"', '', $app_country);?>

                                </div>
							
							
								
								 <div class="span1 lightblue" >
                                    <label style="margin-bottom: 11px;">12.F.State</label>
                                    <div id="state_app1">
                            	<?php
									//$attr = "class='span12 ui-state-valid'";
									$app_state = isset($app_trans[0]->state_app) ? $app_trans[0]->state_app : '';
									
									?>
									<?php echo getStateDropDown($add_country, $name='state_app1', $att='class="span12"  id="state_app', '', $app_state); ?>
                                  </div>

                                </div>
							 <?php  // $broker_insured1=explode('-',$app_trans[0]->insured_zip);   ?>
							 <?php  $app_insd =  explode('-',$app_trans[0]->insured_zip)  ;    ?>
								 <div class="span2 lightblue">
                                    <label style="margin-bottom: 11px;">12.G.Zip</label>
                                    
                                     <input id="zip1" name="insured_zip11" type="text" class="span6" maxlength="5" pattern="[0-9]+" value="<?php echo !empty($app_insd[0]) ? $app_insd[0]: '' ; ?>">
									 <input id="zip2" name="insured_zip12" type="text" class="span6" maxlength="4" pattern="[0-9]+" value="<?php echo !empty($app_insd[1]) ? $app_insd[1]: '' ; ?>" >
                                        <!--<input id="zip2" name="insured_zip[]" type="text" class="span6 zip2" value="">-->
                                   
                                </div>
								
								       <div class="span3 lightblue">
                                            <div class="row-fluid">
                  <span class="span12">
                    <label class="control-label pull-left">12.H.Phone</label>
                    <!--<input type="button" id="click_add_phone" class="dom-link pull-right tdu click_add" value="Click for Additional phones">-->
                   <!-- <a class="dom-link pull-right tdu" href="#" id="click_add_phone">Click for Additional phones</a>-->
                  </span>
                </div>
				
                <div class="row-fluid">
            				         <div class="span12">
									    <?php $app_tel=explode('-',$app_trans[0]->insured_telephone);   ?>
										<?php  // $insured_telephone = isset($app_applicant[0]->insured_telephone) ? explode("-",$app_applicant[0]->insured_telephone) : ''; ?>
								 <input id="insured_telephone1" name="insured_telephone1[1]" type="text" class="span3" maxlength="3"  pattern="[0-9]+"  value="<?php echo isset($app_tel[0]) ? $app_tel[0] : '';?>"  >
                                        <input id="insured_telephone2" name="insured_telephone1[2]" type="text" class="span3"   pattern="[0-9]+"  maxlength="3" value="<?php echo isset($app_tel[1]) ? $app_tel[1] : '';?>"  >
                                        <input id="insured_telephone3" name="insured_telephone1[3]" type="text" class="span3"   pattern="[0-9]+"  maxlength="4" value="<?php echo isset($app_tel[2]) ? $app_tel[2] : '';?>"  >
									 <input id="insured_tel_ext" name="insured_tel_ext1" placeholder="Ext" type="text" pattern="[0-9]+"  class="span3" maxlength="5" pattern="[0-9]+" value="<?php echo isset($app_trans[0]->insured_tel_ext) ? $app_trans[0]->insured_tel_ext :'';?>"  >
                                    
                                    </div>
                </div>
									
                                </div>
                                <div class="span3 lightblue">
                               <div class="row-fluid">
                  <span class="span12">
                    <label class="control-label left_pull">12.I.Email</label>
                   <!-- <input type="button" id="click_add_email" class="dom-link left_pull tdu click_add " value="Click for Additional email"/>-->
                    <!--<a class="dom-link pull-right tdu" href="#" id="click_add_email">Click for Additional email</a>-->
                  </span>
                </div>
				 
                <input class="textinput span9" type="text" placeholder="" id="email" name="insured_email1" maxlength="100" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30" value="<?php echo isset($app_trans[0]->insured_email) ? $app_trans[0]->insured_email: '' ; ?>">
								</div>
					 <!--  <div class="span2 lightblue" style="margin-left:-15px;">
                                    <label style="margin-bottom: 8px;">Other contact info</label>
                            <input class="span12" type="text" placeholder="" name="Other_contact" value="<?php echo isset($app_applicant[0]->Other_contact) ? $app_applicant[0]->Other_contact : '';?>">       
                                </div>-->
								
								
							
					  </div>      
                 <textarea class="span10 span12-1" id="" placeholder="If no, who owns it" name="transported_desc" required="required"><?php echo isset($app_trans[0]->transported_desc) ? trim($app_trans[0]->transported_desc) : '';?></textarea>
                </div>
                               
	
       <div class="row-fluid mt_15">
            <span class="span6">
              <label class="control-label pull-left" >13.Does the applicant haul any kind of Hazmat? &nbsp;</label>
              <select class="pull-left pull-left-4 width_option" name="hazmat_name" required  >
			  <?php $hazmat_name = isset($app_trans[0]->hazmat_name) ? $app_trans[0]->hazmat_name : '';?>
              <option value="" >Select</option>
                <option value="No" <?php if($hazmat_name  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hazmat_name  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label pull-left">14.Does the applicant transport passengers? &nbsp; &nbsp;</label>
              <select class="pull-left pull-left-1 width_option" name="passengers_name" required>
			  
                <?php $passengers_name = isset($app_trans[0]->passengers_name) ? $app_trans[0]->passengers_name : '';?>
                <option value="" >Select</option>
                <option value="No" <?php if($passengers_name  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($passengers_name  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
            </span>
          </div>
		    <div class="row-fluid" style="" id="filings_id">
			<div class="row-fluid ">
			<!--  <span class="badge">Liability</span>-->
			</div>
            <input type="hidden" value="<?php echo isset($app_fil[0]->app_fill_id) ? $app_fil[0]->app_fill_id : ''; ?>" name="app_fill_id" /> 
			<div class="row-fluid ">
            <span class="span4">
              <label class="control-label pull-left ">15.Do you need any filings?</label>
              <select class="pull-left pull-left-5 width_option" name="filings_name" id="filings_name" onchange="filings_name1(this.value)" required>
			 
                <?php if(count($quote_filings)>0){ $quote_filing="Yes";}$filings_name = isset($app_fil[0]->filings_name) ? $app_fil[0]->filings_name : $quote_filing;?>
                <option value="" >Select</option>
                <option value="No" <?php if($filings_name  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($filings_name  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
            </span>
            
            <span class="span3" style="margin-left:-20px;" id="filings_number" style="display:none;">
              <label class="control-label pull-left span6">&nbsp; Number of filings : &nbsp;</label>
              <?php $app_filing=isset($app_fil[0]->filings_number) ? $app_fil[0]->filings_number  : count($quote_filings);?>
              <input class="textinput span3" type="text" onchange="add_filing(this.value)" placeholder="" pattern="[0-9]+" value="<?php   if($app_filing>0) { echo $app_filing; }  ?>" name="filings_number">
            </span>
           
            
              <div id="filings_add_show" style="display:none;">
           <?php
									$attr = "class='span7 ui-state-valid'";
									$broker_state = isset($state_fill[0]->filings_num) ? $state_fill[0]->filings_num : '';;
									
									?>
									<?php echo get_state_dropdown('state_fill[]', $broker_state, $attr); ?>
            </div>
            
            <script>
			 $(document).ready(function(){
																	
																     filings_name1($('#filings_name').val());
																	
																 });
			
			</script>
  
           </div>
  
            <?php  $filings_type= isset($app_fil[0]->filings_type) ? $app_fil[0]->filings_type : $quote_filings[0]->filings_type;
			$filings_num= isset($app_fil[0]->filings_num) ? $app_fil[0]->filings_num : $quote_filings[0]->filing_no;
			$filing_type_other= isset($app_fil[0]->state_fill) ? $app_fil[0]->state_fill : $quote_filings[0]->filing_type_other;
			
			$filing_num=isset($app_fil[0]->filings_number) ? $app_fil[0]->filings_number : count($quote_filings);
			
			
		 $fill=explode(',',$filings_type);
		 $fill_num=explode(',',$filings_num);
		 $fill_type_other=explode(',',$filing_type_other);
		 
		// print_r($filings_type);
		 $y=1;
		 
			 // print_r($filings_type);
		 ?>
         
         
         <div id="add_filling_row"  class="row-fluid add_filling_row" >
         
              
              
              
              
          <div class="span12 lightblue">
								<table class="table table-bordered table-striped table-hover span4">
														<thead>
															<tr>
																<th class="span3">Filing Type:</th>
																<th class="span3"> Filing Num.</th>
															</tr>
														</thead>
														<tbody id="add_filing_row">
                                                        
                                                     <?php  for($i=0;$i<$filing_num;$i++) {  ?>                             
                                                       <tr>
                                                        <td>
                                                        <select id="filing_type_other<?php echo $y; ?>" class="span12 filing_type filing_type_add ui-state-valid" name="filings_type[]" onchange="filling_other1(this.value,this.id)">
                                                        <option value="">Select</option>
                                                        <option value="CA" <?php if($fill[$i]=="CA"){ echo 'selected' ; }?>>CA</option>
                                                        <option value="MC" <?php if($fill[$i]=="MC"){ echo 'selected' ; }?>>MC</option>
                                                        <option value="TX" <?php if($fill[$i]=="TX"){ echo 'selected' ; }?>>TX</option>
                                                        <option value="Other" <?php if($fill[$i]=="Other"){ echo 'selected' ; }?>>Other</option>
                                                        </select><br>
                                                
                                                       
                                                        
                                                        <input style="display:none" id="filing_type_other<?php echo $y; ?>add" type="text" name="filing_type_other[]" value="<?php echo $fill_type_other[$i]; ?>" class="span12 filing_numbers ui-state-valid"></td>
                                                      
                                                       <td>
                                                       <input type="text" name="filings_num[]" value="<?php echo $fill_num[$i]; ?>" class="span12 filing_numbers"></td>														
                                                       </tr>
                                                       <script>
													   var mn=1;
													   	 $(document).ready(function(){
																	
																     filling_other1($('#filing_type_other'+mn).val(),'filing_type_other'+mn);
																	mn++;
																 });
													   </script>
                                                       <?php   $y++; } ?>
                                                       </tbody>
													</table>
												</div>
                                   </div>
                                   </div>
              
              
      
            
     <!--      <div  id="filing_table" style="display:none;">
          					<table  class="table table-bordered table-striped table-hover span4" >
														<thead>
															<tr>
																<th class="span3">Filing Type:</th>
																<th class="span3"> Filing Num.</th>
															</tr>
														</thead>
														<tbody id="add_filing_row">
                                                                                  
                                                       </tbody>
													</table>-->
											
           
   
    
         
		          <div class="row-fluid">
            <span class="span6">
              <label class="control-label pull-left span5" >16. Does the applicant haul for</label>
              <select class="pull-left pull-left-6 span4 chzn-select"  multiple name="trucking_comp_name[]" id="trucking_comp_id" onChange="Company_trucking1();">
            
                           <?php  $trucking_comp_name = isset($app_fil[0]->trucking_comp_name) ? explode(',',$app_fil[0]->trucking_comp_name) : '';?>
                          <?php if(!empty($trucking_comp_name)){ ?>
                <option value="Trucking Company" <?php if(in_array('Trucking Company',$trucking_comp_name )) echo 'selected="selected"'; ?> >Trucking Company</option>
                 <option value="Shippers" <?php if(in_array('Shippers',$trucking_comp_name)) echo 'selected="selected"'; ?>>Shippers</option>
                 <option value="Other" <?php if(in_array('Other',$trucking_comp_name )) echo 'selected="selected"'; ?>>Other</option>
                <?php  }else{ ?>
                   <option value="Trucking Company">Trucking Company</option>
                 <option value="Shippers">Shippers</option>
                 <option value="Other">Other</option>
                
                <?php }?>
              </select>
              <input class="textinput span3" value="<?php echo isset($app_fil[0]->trucking_comp_other) ? $app_fil[0]->trucking_comp_other : '';?>" type="text" placeholder="Please specify" id="trucking_specify" name="trucking_comp_other" style="display:none">
            </span>
            <script>
			$(document).ready(function(){
				
			 Company_trucking1();
			//filings_name1($('#filings_name').val());
			
			});
			</script>
            <span class="span6">
              <label class="control-label pull-left "> &nbsp;17.Does the applicant need any certificate(s)? &nbsp;</label>
              <select class="pull-left pull-left-7 width_option" name="certificate_name"  id="certificate_id_val" onChange="certificate_app_val();" required>
               
			        <?php $certificate_name = isset($app_fil[0]->certificate_name) ? $app_fil[0]->certificate_name : '';?>
                    <option value="" >Select</option>
                <option value="No" <?php if($certificate_name  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($certificate_name  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
            </span>
          </div>
		  <div id="certificate_app_id" style="display:none;">
		  <div class="row-fluid">
            <span class="span6">
              <label class="control-label pull-left ">How many certificates?&nbsp;</label>
              <input class="span_width1" type="text" placeholder="" name="cert_num"  pattern="[0-9]+" value="<?php if(count($app_cert)==0) {}else{echo count($app_cert);}?>" onChange="certificate_val(this.value);" maxlength="2">
            </span>
          </div>
		       <div id="state_certi" style="display:none"><?php
									$attr = "class=span5 ui-state-valid";
									$broker_state = "CA";
									?>
									<?php echo get_state_dropdown("state_cert", $broker_state, $attr); ?>
									</div>
		  <div class="" id="add_certificates_row">
		 	<?php	 foreach( $app_cert as $value ){ ?>
            <div id="certificates_row1" style="margin-bottom:7px;">
            <div class="well"> 
            <input type="hidden" name="cert_id[]" value="<?php echo $value->application_certificate_id;?>" />
			<div class="row-fluid">
            <span class="span2">
              <label class="control-label pull-left span5">Company  &nbsp;</label>
              <input class="textinput span7" value="<?php echo $value->cert_comp;?>" type="text" placeholder="" name="cert_comp[]">
            </span>
            <span class="span2">
              <label class="control-label span5">Attention &nbsp;</label>
              <input class="textinput span7" type="text" value="<?php echo $value->cert_attent;?>" placeholder="" name="cert_attent[]">
            </span>
             <span class="span2">
              <label class="control-label span5">Address</label>
              <input class="textinput span7" type="text" placeholder="" value="<?php echo $value->cert_add;?>" name="cert_add[]">
            </span>
            <span class="span2">
              <label class="control-label span3">City</label>
              <input class="textinput span7" type="text" placeholder="" value="<?php echo $value->cert_city;?>" name="cert_city[]">
            </span>
            <span class="span2" style="float:left; margin:0px;">
              <label class="control-label span3">State</label>
             <?php
									$attr = "class=span5 ui-state-valid";
									$broker_state = $value->state_cert;
									?>
									<?php echo get_state_dropdown("state_cert[]", $broker_state, $attr); ?>
									</span>
			 
            <span class="span1" style="margin: 0px;width: 12%;" >
              <label class="control-label span6">country</label>
              <input class="textinput span6" type="text" placeholder="" value="<?php echo $value->cert_zip;?>"  style="margin-left: -13px;"name="cert_zip[]" >
            </span>
			
          </div>
		  <div class="row-fluid">
            <span class="span2">
              <label class="control-label span4">Email&nbsp;</label>
              <input class="textinput span8" type="text" placeholder="" value="<?php echo $value->cert_email;?>" name="cert_email[]" id="email" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30">
            </span>
            <span class="span4" >
              <label class="control-label span2">Phone</label>
              <?php $tel=explode('-',$value->cert_telephone);?>
              <input class="textinput span2" type="text" placeholder="Area" name="cert_telephone[]" value="<?php echo $tel[0];?>"  id="insured_telephone1" maxlength="3"  pattern="[0-9]+"  value=""  >
              <input class="textinput span2" type="text" placeholder="Prefix" name="cert_telephone[]" value="<?php echo $tel[1];?>" id="insured_telephone2" maxlength="3"  pattern="[0-9]+"  value=""  >
              <input class="textinput span2" type="text" placeholder="Sub" name="cert_telephone[]" value="<?php echo $tel[2];?>" id="insured_telephone3" maxlength="4"  pattern="[0-9]+"  value=""  >
              <input class="textinput span3" type="text" placeholder="Ext" pattern="[0-9]+" value="<?php echo $value->cert_telephone_ext; ?>" pattern="[0-9]+" name="cert_telephone_ext" maxlength="5">
            </span>
            <span class="span3" style="float:left; margin:0px;">
              <label class="control-label span2">Fax</label>
              <?php $fax=explode('-',$value->cert_fax);?>
              <input class="textinput span3" type="text" pattern="[0-9]+" placeholder="Area" name="cert_fax[]" maxlength="3" value="<?php echo $fax[0];?>">
              <input class="textinput span3" type="text" pattern="[0-9]+" placeholder="Prefix" name="cert_fax[]" maxlength="3" value="<?php echo $fax[1];?>">
              <input class="textinput span3" type="text" pattern="[0-9]+" placeholder="Sub" name="cert_fax[]" maxlength="5" value="<?php echo $fax[2];?>">
            </span>
          </div>
		  <div class="row-fluid">
           
            <span class="span6">
              <label class="control-label span5">Any Additional insured(s)?</label>
              <select class="pull-left pull-left-5 width_option" name="Additional_ins[]" style="float:left; margin:0px;" required>
              <option value="" >Select</option>
                   <?php $Additional_ins = isset($value->Additional_ins) ? $value->Additional_ins : '';?>
                <option value="No" <?php if($Additional_ins  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($Additional_ins  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
            </span>
            <span class="span6">
              <label class="control-label"></label>
            </span>
          </div>
         </div>
         </div>
          <?php } ?>
		 </div> 
         </div>
         <input type="hidden" value="<?php echo isset($app_add[0]->application_add_ins_id) ? $app_add[0]->application_add_ins_id  : ''; ?>" name="application_add_ins_id" />

		   <div class="row-fluid mt_15">
            <span class="span6">
              <label class="control-label pull-left ">18.Is the scheduled vehicle(s) driven by the owner(s)?&nbsp;&nbsp;</label>
              <select class="pull-left pull-left-2 width_option" name="owner_vehicle" onChange="owner_vehicle13();" id="owner_id13" style="float:left;" required>
               
			        <?php echo  $owner_vehicle = isset($app_add[0]->owner_vehicle) ? $app_add[0]->owner_vehicle : '';?>
                    <option value="" >Select</option>
                <option value="No" <?php if($owner_vehicle  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($owner_vehicle  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>   
  <!--<a href="#addowner"   class="fancybox edit"><label class="pull-left edit_button" id="addown" style="display:none;">Edit</label></a>	-->
  <!-- GHI-6  -->

 
  
  <!--<a href="<?php echo base_url();?>application_form/applicant_owner/<?php echo isset($quote_id) ?$quote_id :'';?>"  data-fancybox-type="iframe" style="" class="fancybox edit"> <label class="pull-left edit_button">Edit&nbsp;&nbsp;&nbsp;&nbsp;</label></a>	-->		  
            </span>

            
            
			<div id="" style="">
            <span class="span6" >
              <label class="control-label pull-left span8">19.Do you have workmen &nbsp;compensation insurance?</label>
              <select class="pull-left pull-left-2 width_option" name="insurance_val" onChange="insurance_val_func(); " id="insurance_val_id" style="float:left;margin:0px" required>
                
		    <?php   $insurance_val = isset($app_add[0]->insurance_val) ? $app_add[0]->insurance_val : '';?>
            <option value="" >Select</option>
                <option value="No" <?php if($insurance_val  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($insurance_val  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
			  
            </span>
			</div>
          </div>
                      
            <div class="container-fluid" id="addown" style="display:none;">
      <div class="page-header">
        <h1>GHI-6 <small>Driver / Owner exclusion supplement form</small>
        </h1>
      </div>
      <div class="well">
        <label class="control-label control-label-1">18.A.Named Insured / DBA</label>
        <input class="textinput span11 supplement_dba" type="text"  value="<?php echo $app_ghi6[0]->supplement_dba; ?>"name="supplement_dba" id="supplement_dba1" value="" placeholder="">
      </div>
      <p>
        <br>It is hereby understood and agreed that all coverages and our obligation to defend under this policy shall not apply nor accrue to the benefit of any INSURED or any third party claimant while any VEHICLE or MOBILE EQUIPMENT described in the policy or
        any other VEHICLE or MOBILE EQUIPMENT, to which the terms of the policy are extended, is being driven, used or operated by any person designated below.</p>
      <p>
        <br>The driver exclusion shall be binding upon every INSURED to whom such policy or endorsements provisions apply while such policy is in force and shall continue to be binding with respect to any continuation, renewal or replacement of such policy by the
        Named Insured or with respect to any reinstatement of such policy within 30 days of any lapse thereof. This DRIVER EXCLUSION provision shall conform to State statutes and laws.</p>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">18.B.How many additional persons?</label>
        </span>
        <span class="span1">
          <input class="textinput span12 supplement_val" type="text" value="<?php if(count($app_ghi6)==0){}else echo count($app_ghi6); ?>" name="supplement_val" id="supplement_val" onchange="additional_add(this.value);" placeholder="" pattern="[0-9]+"  maxlength="2">
        </span>
        <span class="span7">
        <!--  <button class="btn"> <i class="icon icon-plus"></i> Add</button>-->
        </span>
      </div>
      <div class="well" >
       
      <?php $o=1; if(!empty($app_ghi6)) { foreach( $app_ghi6 as $values1) { ?>
<input type="hidden" value="18" name="supplement_parent_id[]"/> 
      <input type="hidden" value="<?php echo  isset($values1->supplement_id) ? $values1->supplement_id : '';?>" name="supplement_id[]"/> 
      
		<div id="add_per_rows<?php echo $o; ?>" class="add_addition_row">	
        		<div class="row-fluid">      
                     <span class="span12">       
                         <button class="btn pull-right" style="margin-bottom:5px;" onclick="persons_delete2(<?php echo $o; ?>);">Delete</button>		
                            </span>		
                               </div>		   
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label">18.BA.Name of person excluded</label>
            <input class="textinput" type="text" value="<?php echo  isset($values1->supplement_fname) ? $values1->supplement_fname : '';?>" placeholder="First name"  name="supplement_fname[]">
            <input class="textinput" type="text" value="<?php echo  isset($values1->supplement_mname) ? $values1->supplement_mname : '';?>" placeholder="Middle name" name="supplement_mname[]">
            <input class="textinput" type="text" value="<?php echo  isset($values1->supplement_lname) ? $values1->supplement_lname : '';?>" placeholder="Last name" name="supplement_lname[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">18.BB.Date of Birth &nbsp;</label>
            <input class="textinput span7 datepick" value="<?php echo  isset($values1->supplement_dfrom) ? $values1->supplement_dfrom : '';?>" type="text" name="supplement_dfrom[]" id="supplement_dfrom" placeholder="mm/dd/yyy">
          </span>
          <span class="span4">
            <label class="control-label pull-left">18.BC.SSN &nbsp;</label>
            <input class="textinput span10" value="<?php echo  isset($values1->supplement_dto) ? $values1->supplement_dto : '';?>" type="text" name="supplement_dto[]" id="supplement_dto" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">18.BD.License number &nbsp;</label>
            <input class="textinput span7" value="<?php echo  isset($values1->supplement_lnum) ? $values1->supplement_lnum : '';?>" type="text" placeholder="" name="supplement_lnum[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">18.BE.Reason for exclusion</label>
            <textarea class="span12" placeholder="" value="" name="supplement_Reason[]"><?php echo  isset($values1->supplement_Reason) ? $values1->supplement_Reason : '';?> </textarea>
          </span>
        </div>
		
      </div>
    
	<div id="add_additional_row" style="display:none;">
       <input type="hidden" value="18" name="supplement_parent_id[]"/> 
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label">18.BA.Name of person excluded</label>
            <input class="textinput" type="text" placeholder="First name" name="supplement_fname[]">
            <input class="textinput" type="text" placeholder="Middle name" name="supplement_mname[]">
            <input class="textinput" type="text" placeholder="Last name" name="supplement_lname[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">18.BB.Date of Birth &nbsp;</label>
            <input class="textinput span7 datepick" type="text" name="supplement_dfrom[]" placeholder="mm/dd/yyy">
          </span>
          <span class="span4">
            <label class="control-label pull-left">18.BC.SSN &nbsp;</label>
            <input class="textinput span10" type="text" name="supplement_dto[]" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">18.BD.License number &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="supplement_lnum[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">18.BE.Reason for exclusion</label>
            <textarea class="span12" placeholder="" name="supplement_Reason[]"></textarea>
          </span>
        </div>
		
      </div>
     
     
     <?php $o++; } } else { ?> 
      
        <div>
       <!--  <button class="btn pull-right">Delete</button>-->
        </div>
		<div id="add_additional_row" style="display:none;">
       <input type="hidden" value="18" name="supplement_parent_id[]"/> 
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label">18.BA.Name of person excluded</label>
            <input class="textinput" type="text" placeholder="First name" name="supplement_fname[]">
            <input class="textinput" type="text" placeholder="Middle name" name="supplement_mname[]">
            <input class="textinput" type="text" placeholder="Last name" name="supplement_lname[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">18.BB.Date of Birth &nbsp;</label>
            <input class="textinput span7 datepick" type="text" name="supplement_dfrom[]" placeholder="mm/dd/yy">
          </span>
          <span class="span4">
            <label class="control-label pull-left">18.BC.SSN &nbsp;</label>
            <input class="textinput span10" type="text" name="supplement_dto[]" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">18.BD.License number &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="supplement_lnum[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">18.BE.Reason for exclusion</label>
            <textarea class="span12" placeholder="" name="supplement_Reason[]"></textarea>
          </span>
        </div>
		
      </div>
       <?php } ?>
	  <div id="add_addition">
		</div>
	  </div>
      <div>
      <!--  <div class="form-actions">
          <button class="btn btn-primary" onclick="fancybox_close();">Save</button>
          <button class="btn" onclick="reload_close();">Cancel</button>
        </div>-->
      </div>
    </div>
          
		
		     <div class="row-fluid" id="Policy_id" style="display:none;">
            <span class="span2">
              <label class="control-label">19.A.Policy Number</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo isset($app_add[0]->ins_Policy_num) ? $app_add[0]->ins_Policy_num:''; ?>" name="ins_Policy_num">
            </span>
            <span class="span5">
              <label class="control-label">19.B.Insurance company for workmen compensation</label>
              <input class="textinput span12" type="text" placeholder="" value="<?php echo $app_add[0]->ins_Policy_comp ?>" name="ins_Policy_comp">
            </span>
            <span class="span2">
              <label class="control-label">19.C.Effective Date</label>
              <input  maxlength="10" type="text" name="effective_from_ins" id="date_from_effective1" value="<?php echo $app_add[0]->effective_from_ins ?>" class="span12 " pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}">
            </span>
            <span class="span2">
              <label class="control-label">19.D.Expiring Date</label>
              <input maxlength="10" type="text" name="expiration_from_ins"  id="date_to_expiration1"  value="<?php echo $app_add[0]->expiration_from_ins ?>" class="span12 " pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}">
            </span>
            
             <div class="row-fluid">
            
              
             
			   <div class="span6 lightblue">
                                    <label class="" style="float:left;">19.E.Please send the certificates &nbsp;</label>
                                    <!--span class="btn btn-file">Upload<input type="file" multiple="multiple" name="file_sec_1[]" id="file_sec_1" /></span-->
									<!-- input type="button" value="Upload" id="loss_report_upload1" class="btn btn-primary" style="cursor:pointer;" -->
									
									<!--<label class="cabinet label label-info" >
										<div id="upload_bottom1"><input type="hidden" id="upload_count1" value="1" ></div>
										<input onChange="add_upload_1(this.id);" type="file" name="file_sec_1[]" id="fileup1_1" class="upload_browse_multi1 file" title="Select file to upload" />
									</label>-->
									<input type="button" id="first_upload_second" value="Upload"><br/>
									

									<input type="hidden" name="uploaded_files_values_second" id="uploaded_files_values_second" value="<?php echo $app_add[0]->uploaded_files_values_second; ?>">
                         <?php $file=explode(",",$app_add[0]->uploaded_files_values_second); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
									<ul id="upload_file_name_second"></ul>
                                </div>
            
          </div>
            
          </div>
		 
		     
		     <div class="row-fluid mt_15">
            <span class="span12">
              <label class="control-label left_pull">20. Does the owner(s) have a commercial license(s)? &nbsp;</label>
              <select class="pull-left pull-left-8 width_option" onchange="owner_vehicle12();" id="owner_id12" name="owner_license" required>
                <?php $owner_license = isset($app_add[0]->owner_license) ? $app_add[0]->owner_license : '';?>
                <option value="" >Select</option> 
                <option value="No" <?php if($owner_license  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($owner_license  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
            </span>
          </div>
          
           <div class="container-fluid" id="addown1" style="display:none;">
      <div class="page-header">
        <h1>GHI-6 <small>Driver / Owner exclusion supplement form</small>
        </h1>
      </div>
      <div class="well">
        <label class="control-label control-label-1">20.A.Named Insured / DBA</label>
        <input class="textinput span11 supplement_dba" type="text"  value="<?php echo $app_ghi61[0]->supplement_dba; ?>"name="supplement_dba" id="supplement_dba2" value="" placeholder="">
      </div>
      <p>
        <br>It is hereby understood and agreed that all coverages and our obligation to defend under this policy shall not apply nor accrue to the benefit of any INSURED or any third party claimant while any VEHICLE or MOBILE EQUIPMENT described in the policy or
        any other VEHICLE or MOBILE EQUIPMENT, to which the terms of the policy are extended, is being driven, used or operated by any person designated below.</p>
      <p>
        <br>The driver exclusion shall be binding upon every INSURED to whom such policy or endorsements provisions apply while such policy is in force and shall continue to be binding with respect to any continuation, renewal or replacement of such policy by the
        Named Insured or with respect to any reinstatement of such policy within 30 days of any lapse thereof. This DRIVER EXCLUSION provision shall conform to State statutes and laws.</p>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">20.B.How many additional persons?</label>
        </span>
        <span class="span1">
          <input class="textinput span12 supplement_val" type="text" value="<?php if(count($app_ghi61)==0){}else echo count($app_ghi61); ?>" name="supplement_val" id="supplement_val" onchange="additional_add1(this.value);" placeholder="" pattern="[0-9]+"  maxlength="2">
        </span>
        <span class="span7">
        <!--  <button class="btn"> <i class="icon icon-plus"></i> Add</button>-->
        </span>
      </div>
      <div class="well" >
          
      <?php $o=1; ; if(!empty($app_ghi61)) { foreach( $app_ghi61 as $values1) { ?>
      <input type="hidden" value="20" name="supplement_parent_id[]"/> 
      <input type="hidden" value="<?php echo  isset($values1->supplement_id) ? $values1->supplement_id : '';?>" name="supplement_id[]"/> 
      
		<div id="add_per_rows1<?php echo $o; ?>" class="add_addition_row1">	
        		<div class="row-fluid">      
                     <span class="span12">       
                         <button class="btn pull-right" style="margin-bottom:5px;" onclick="persons_delete2(<?php echo $o; ?>);">Delete</button>		
                            </span>		
                               </div>		   
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label">20.BA.Name of person excluded</label>
            <input class="textinput" type="text" value="<?php echo  isset($values1->supplement_fname) ? $values1->supplement_fname : '';?>" placeholder="First name"  name="supplement_fname[]">
            <input class="textinput" type="text" value="<?php echo  isset($values1->supplement_mname) ? $values1->supplement_mname : '';?>" placeholder="Middle name" name="supplement_mname[]">
            <input class="textinput" type="text" value="<?php echo  isset($values1->supplement_lname) ? $values1->supplement_lname : '';?>" placeholder="Last name" name="supplement_lname[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">20.BB.Date of Birth &nbsp;</label>
            <input class="textinput span7 datepick" value="<?php echo  isset($values1->supplement_dfrom) ? $values1->supplement_dfrom : '';?>" type="text" name="supplement_dfrom[]" id="supplement_dfrom" placeholder="mm/dd/yyy">
          </span>
          <span class="span4">
            <label class="control-label pull-left">20.BC.SSN &nbsp;</label>
            <input class="textinput span10" value="<?php echo  isset($values1->supplement_dto) ? $values1->supplement_dto : '';?>" type="text" name="supplement_dto[]" id="supplement_dto" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">20.BD.License number &nbsp;</label>
            <input class="textinput span7" value="<?php echo  isset($values1->supplement_lnum) ? $values1->supplement_lnum : '';?>" type="text" placeholder="" name="supplement_lnum[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">20.BE.Reason for exclusion</label>
            <textarea class="span12" placeholder="" value="" name="supplement_Reason[]"><?php echo  isset($values1->supplement_Reason) ? $values1->supplement_Reason : '';?> </textarea>
          </span>
        </div>
		
      </div>
    
	<div id="add_additional_row1" style="display:none;">
       <input type="hidden" value="20" name="supplement_parent_id[]"/> 
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label">20.BA.Name of person excluded</label>
            <input class="textinput" type="text" placeholder="First name" name="supplement_fname[]">
            <input class="textinput" type="text" placeholder="Middle name" name="supplement_mname[]">
            <input class="textinput" type="text" placeholder="Last name" name="supplement_lname[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">20.BB.Date of Birth &nbsp;</label>
            <input class="textinput span10 datepick" type="text" name="supplement_dfrom[]" placeholder="mm/dd/yyy">
          </span>
          <span class="span4">
            <label class="control-label pull-left">20.BC.SSN &nbsp;</label>
            <input class="textinput span10" type="text" name="supplement_dto[]" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">20.BD.License number &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="supplement_lnum[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">20.BE.Reason for exclusion</label>
            <textarea class="span12" placeholder="" name="supplement_Reason[]"></textarea>
          </span>
        </div>
		
      </div>
     
     
     <?php $o++; } } else { ?> 
      
        <div>
       <!--  <button class="btn pull-right">Delete</button>-->
        </div>
		<div id="add_additional_row1" style="display:none;">
       <input type="hidden" value="20" name="supplement_parent_id[]"/> 
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label">20.BA.Name of person excluded</label>
            <input class="textinput" type="text" placeholder="First name" name="supplement_fname[]">
            <input class="textinput" type="text" placeholder="Middle name" name="supplement_mname[]">
            <input class="textinput" type="text" placeholder="Last name" name="supplement_lname[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">20.BB.Date of Birth &nbsp;</label>
            <input class="textinput span10 datepick" type="text" name="supplement_dfrom[]" placeholder="mm/dd/yy">
          </span>
          <span class="span4">
            <label class="control-label pull-left">20.BC.SSN &nbsp;</label>
            <input class="textinput span10" type="text" name="supplement_dto[]" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">20.BD.License number &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="supplement_lnum[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">20.BE.Reason for exclusion</label>
            <textarea class="span12" placeholder="" name="supplement_Reason[]"></textarea>
          </span>
        </div>
		
      </div>
       <?php } ?>
	  <div id="add_addition1">
		</div>
	  </div>
      <div>
      <!--  <div class="form-actions">
          <button class="btn btn-primary" onclick="fancybox_close();">Save</button>
          <button class="btn" onclick="reload_close();">Cancel</button>
        </div>-->
      </div>
    </div>
          
		      <div class="row-fluid">
            <span class="span12">
              <label class="control-label left_pull">21.Is applicant the registered owner(s) of all the vehicle(s) listed on this application, other than the unidentified trailer(s)? &nbsp;</label>
              <select class="pull_left pull-left-1 width_option" onchange="owner_applicant1(this.value);" id="owner_reg_id" name="owner_applicant" style="float:left;" required>
                
                 <?php $owner_applicant = isset($app_add[0]->owner_applicant) ? $app_add[0]->owner_applicant : '';?>
                 <option value="" >Select</option> 
                <option value="No" <?php if($owner_applicant  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($owner_applicant  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
			<!--   <a href="#addregistered"   class="fancybox edit"><label class="pull-left edit_button" style="display:none;" id="addregister">Edit</label></a>
			  <!-- <a href="<?php echo base_url();?>application_form/applicant_registered/<?php echo isset($quote_id) ?$quote_id :'';?>"  data-fancybox-type="iframe" style="" class="fancybox edit"> <label class="pull-left edit_button">Edit&nbsp;&nbsp;&nbsp;&nbsp;</label></a>-->
			 <!-- <a href="" onclick="app_register(<?php echo isset($quote_id) ? $quote_id :'';?>);"><label class="edit_button" >Edit </label></a>-->
            </span>
			
          </div>
          
          <!-- GHI-9  -->
	 <div class="container-fluid" style="display:none;" id="addregister">
    
      <div class="page-header ">
        <h1>GHI-9 <small>Registered owner info supplement form</small>
        </h1>
      </div>
      <div class="row-fluid">
        <span class="span6">
          <label class="control-label">21.A.How many additional Registered Owner or Partner's name?</label>
        </span>
        <span class="span1">
          <input class="textinput span12" type="text" value="<?php if(count($app_ghi9)==0){}else echo count($app_ghi9); ?>" name="persons_val" id="persons_val" onchange="persons_add(this.value);" placeholder=""  pattern="[0-9]+"  maxlength="2">
        </span>
        <span class="span7">
       <!--   <button class="btn" onclick="persons_add();"> <i class="icon icon-plus"></i> Add</button>-->
        </span>
      </div>
      <div class="well" >

      <?php if(!empty($app_ghi9)) { $g=1; foreach($app_ghi9 as $values9) {?>
     <input type="hidden" value="<?php echo  isset($values9->reg_id) ? $values9->reg_id : '';?>" name="reg_id[]"/> 
	  
	  <div id="add_per_row1<?php echo $g; ?>" class="add_per_row">			
      <div class="row-fluid">           
      <span class="span12">          
       <button class="btn pull-right" style="margin-bottom:5px;" onclick="persons_delete12(<?php echo $g; ?>);">Delete</button>		   
       </span>		  
        </div>			
        <div class="row-fluid">           
         <span class="span4">           
          <label class="control-label pull-left span10" style="margin-bottom:-5px;">21.AA.Registered Owner or Partners name &nbsp;</label>            
          <input class="textinput span4 val_inserts" type="text" id="first_name<?php echo $g; ?>"  onchange="" name="reg_fname[]" value="<?php echo  isset($values9->reg_fname) ? $values9->reg_fname : '';?>" placeholder="First name">            
          <input class="textinput span4 val_inserts" type="text" id="middle_name<?php echo $g; ?>"  onchange="" name="reg_mname[]" value="<?php echo  isset($values9->reg_mname) ? $values9->reg_mname : '';?>" placeholder="Middle name">            
          <input class="textinput span4 val_inserts" type="text" id="last_name<?php echo $g; ?>"  onchange="" name="reg_lname[]" value="<?php echo  isset($values9->reg_lname) ? $values9->reg_lname : '';?>" placeholder="Last name">         
           </span>		
             <span class="span2">            
           <label class="control-label span10">21.AB.Address</label>            
           <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($values9->reg_address) ? $values9->reg_address : '';?>" name="reg_address[]">          
           </span>      
               <span class="span1">           
            <label class="control-label span10">21.AC.City</label>            
            <input class="textinput span12" type="text" name="reg_city[]" value="<?php echo  isset($values9->reg_city) ? $values9->reg_city : '';?>" placeholder="">          
            </span>       
               <span class="span1" style="width:10%">          
              <label class="control-label span10">21.AD.Country</label>           
               <input class="textinput span12" type="text" placeholder="" value="<?php echo  isset($values9->reg_country) ? $values9->reg_country : '';?>" name="reg_country[]">       
                  </span>        
                    <span class="span1">		  
            <label class="control-label span10">21.AE.State</label>
        	<?php
									$attr = "class='span7 ui-state-valid' style='width: 60px;' ";
									$broker_state =   isset($values9->reg_state) ? $values9->reg_state : '';
									
									?>
									<?php echo get_state_dropdown('reg_state[]', $broker_state, $attr); ?>
            
            
          </span>          
          <span class="span2">           
           <label class="control-label span10">21.AF.Zip</label>           
            <input class="textinput span5" type="text" placeholder="" value="<?php echo  isset($values9->reg_zip) ? $values9->reg_zip : '';?>" name="reg_zip[]"  pattern="[0-9]+"  maxlength="5">			
            <input class="textinput span5" type="text" placeholder="" value="<?php echo  isset($values9->reg_zip1) ? $values9->reg_zip1 : '';?>" name="reg_zip1[]"   pattern="[0-9]+"  maxlength="4">         
             </span>        
             </div>     
             <div class="row-fluid">         
              <span class="span3">            
              <label class="control-label span10">21.AG.License Number</label>            
              <input class="textinput val_inserts" type="text" placeholder="" id="lnumber<?php echo $g; ?>" onchange="" value="<?php echo  isset($values9->reg_lnum) ? $values9->reg_lnum : '';?>" name="reg_lnum[]">          
              </span>          
              <span class="span3 ">            
              <label class="control-label span10">21.AH.S.S.N</label>            
              <input class="textinput val_inserts" type="text" placeholder="" id="Snumber<?php echo $g; ?>" onchange="" value="<?php echo  isset($values9->reg_ssn) ? $values9->reg_ssn : '';?>" name="reg_ssn[]">         
               </span>          
               <span class="span3">            
               <label class="control-label span10">21.AI.Commercial License</label>
                      <?php  $reg_r0=  isset($values9->reg_r1) ? $values9->reg_r1 : '';?>    
                <select class="pull-left pull-left-1 width_option" name="reg_r0[]" id="radio_1<?php echo $g; ?>" onChange="radio_button(this.value,'<?php echo $g; ?>');" style="float:left;" required>\
		     	<option value="No" <?php if($reg_r0  == 'No') echo 'selected="selected"'; ?>>No</option>
                 <option value="Yes" <?php if($reg_r0  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                </select>       
                        </span>     
                           </div>    
<script>
var t12=1;
$(document).ready(function(){

radio_button($('#radio_1'+t12).val(),t12);

t12++;

});

</script>
						   
                            <div class="row-fluid">     
                                 <span class="span12">      
                                       <label class="control-label pull-left">21.AJ.Please provide a photo copy of license and MVR &nbsp;</label> 
                          <!-- <input class="textinput" type="file" placeholder="Upload file" name="" value="" >-->			
                          <input type="button" id="first_loadeds" value="Upload">			
                          <input type="hidden" name="MVR_file[]" id="uploaded_file" value="<?php echo $values9->MVR_file;?>">		<br />
                          
              <?php $file=explode(",",$values9->MVR_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>						
                          <ul id="upload_file" class="upload_file"></ul>    
                              
                            </span>       
                           </div> 
       <div class="row-fluid">          
                      <!--     <span class="span12">  
                       <a class="dom-link  edit" >If vehicle is not driven by the above person, please use GHI-6</a>          
                            <a class="dom-link fancybox edit" href="#addowner">If vehicle is not driven by the above person, please use GHI-6</a>        
                             </span>  -->

           <!--new ghi-6 -->


                    <div class="container-fluid" id="addown12<?php echo $g; ?>" style="display:none;">
      <div class="page-header">
        <h1>GHI-6 <small>Driver / Owner exclusion supplement form</small>
        </h1>
      </div>
      <div class="well">
        <label class="control-label control-label-1">21.BA.Named Insured / DBA</label>
        <input class="textinput span11 supplement_dba" type="text"  value="<?php echo $app_ghi62[0]->supplement_dba; ?>"name="supplement_dba" id="supplement_dba1" value="" placeholder="">
      </div>
      <p>
        <br>It is hereby understood and agreed that all coverages and our obligation to defend under this policy shall not apply nor accrue to the benefit of any INSURED or any third party claimant while any VEHICLE or MOBILE EQUIPMENT described in the policy or
        any other VEHICLE or MOBILE EQUIPMENT, to which the terms of the policy are extended, is being driven, used or operated by any person designated below.</p>
      <p>
        <br>The driver exclusion shall be binding upon every INSURED to whom such policy or endorsements provisions apply while such policy is in force and shall continue to be binding with respect to any continuation, renewal or replacement of such policy by the
        Named Insured or with respect to any reinstatement of such policy within 30 days of any lapse thereof. This DRIVER EXCLUSION provision shall conform to State statutes and laws.</p>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">21.C.How many additional persons?</label>
        </span>
        <span class="span1">
          <input class="textinput span12 supplement_val" type="text" value="<?php if(count($app_ghi62)==0){}else echo count($app_ghi62); ?>" name="supplement_val" id="supplement_val" onchange="additional_add12(this.value,'<?php echo $g; ?>');" placeholder="" pattern="[0-9]+"  maxlength="2">
        </span>
        <span class="span7">
        <!--  <button class="btn"> <i class="icon icon-plus"></i> Add</button>-->
        </span>
      </div>
      
      
      <?php $o=1; if(!empty($app_ghi62)) { foreach( $app_ghi62 as $values1) { ?>
      <input type="hidden" value="21" name="supplement_parent_id[]"/> 
      <input type="hidden" value="<?php echo  isset($values1->supplement_id) ? $values1->supplement_id : '';?>" name="supplement_id[]"/> 
      
		<div id="add_per_rows12<?php echo $g; ?><?php echo $o; ?>" class="add_addition_row12<?php echo $g; ?>">	
        		<div class="row-fluid">      
                     <span class="span12">       
                         <button class="btn pull-right" style="margin-bottom:5px;" onclick="persons_delete22(<?php echo $g; ?>,<?php echo $o; ?>);">Delete</button>		
                            </span>		
                               </div>		   
        <div class="row-fluid">
          <span class="span12 mt_15">
            <label class="control-label">21.CA.Name of person excluded</label>
            <input class="textinput fname<?php echo $g; ?>" type="text" id="" value="<?php echo  isset($values1->supplement_fname) ? $values1->supplement_fname : '';?>" placeholder="First name"  name="supplement_fname[]">
            <input class="textinput mname<?php echo $g; ?>" type="text" id="" value="<?php echo  isset($values1->supplement_mname) ? $values1->supplement_mname : '';?>" placeholder="Middle name" name="supplement_mname[]">
            <input class="textinput lname<?php echo $g; ?>" type="text" id="" value="<?php echo  isset($values1->supplement_lname) ? $values1->supplement_lname : '';?>" placeholder="Last name" name="supplement_lname[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left">21.CB.Date of Birth &nbsp;</label>
            <input class="textinput span7 datepick"  value="<?php echo  isset($values1->supplement_dfrom) ? $values1->supplement_dfrom : '';?>" type="text" name="supplement_dfrom[]" id="supplement_dfrom" placeholder="mm/dd/yyy">
          </span>
          <span class="span4">
            <label class="control-label pull-left">21.CC.SSN &nbsp;</label>
            <input class="textinput span10 ssn<?php echo $g; ?>" id="" value="<?php echo  isset($values1->supplement_dto) ? $values1->supplement_dto : '';?>" type="text" name="supplement_dto[]" id="supplement_dto" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left">21.CD.License number &nbsp;</label>
            <input class="textinput span7 license<?php echo $g; ?>" value="<?php echo  isset($values1->supplement_lnum) ? $values1->supplement_lnum : '';?>" type="text" placeholder="" name="supplement_lnum[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label">21.CE.Reason for exclusion</label>
            <textarea class="span12" placeholder="" value="" name="supplement_Reason[]"><?php echo  isset($values1->supplement_Reason) ? $values1->supplement_Reason : '';?> </textarea>
          </span>
        </div>
	</div>	
     
<?php } } ?>
   <div id="add_addition12<?php echo $g; ?>">
		</div>
	  </div>


    <!-- End -->     
                              </div>
                              <div class="row-fluid">         
                              <!-- <span class="span12">         
                                  <label class="control-label">Please explain</label>  
                                            
                               <textarea class="span10" placeholder="" name="reg_explain[]"> <?php echo  isset($values9->reg_explain) ? $values9->reg_explain : '';?></textarea>         
                                </span>    -->   
                                 </div>
                             	</div>
      
      
      <?php  $g++; } } ?>
		  <div id="add_persons">
	  
	  </div>
      </div>
	</div>
	
     <div id="add_additional_row12" style="display:none;">
       
       
		
      </div>
	  
      
       <!-- <div class="form-actions">
          <button class="btn btn-primary" onclick="fancybox_close();">Save</button>
          <button class="btn" onclick="reload_close1();">Cancel</button>
        </div>-->
      
   
	
          
          
		     <div class="row-fluid ">
            <span class="span12">
              <label class="control-label left_pull">22.Does the applicant own any vehicle(s) not scheduled on this application? &nbsp;</label>
              <select class="pull-left pull-left-1 width_option" name="applicant_scheduled" id="applicant_scheduled" onChange="applicant_schedule1(this.value);" style="float:left;" required>
               
                 <?php $applicant_scheduled = isset($app_add[0]->applicant_scheduled) ? $app_add[0]->applicant_scheduled : '';?>
                 <option value="" >Select</option> 
                <option value="No" <?php if($applicant_scheduled  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($applicant_scheduled  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
			 <!--  <a href="#addscheduled"   class="fancybox edit"><label class="pull-left edit_button" style="display:none" id="addschedul">Edit</label></a>
			  <a href="<?php echo base_url();?>application_form/applicant_scheduled/<?php echo isset($quote_id) ?$quote_id :'';?>"  data-fancybox-type="iframe" style="" class="fancybox edit"> <label class="pull-left edit_button">Edit&nbsp;&nbsp;&nbsp;&nbsp;</label></a>-->
            </span>
          </div>
          
          	<!-- GHI-11  -->
	  <div class="container-fluid" id="addschedul" style="display:none;">
      <div class="page-header ">
        <h1>GHI-11 <small>Non-operational supplement form</small>
        </h1>
      </div>
      <div class="row-fluid">
        <span class="span12">
          <label class="control-label">Exclusion</label>
        </span>
      </div>
      <p>The Named Insured agrees that the said Policy shall not and does not protect the Named Insured from claims for injury, damage or loss sustained by any person when caused by a motor vehicle not specified in said policy, and if the company shall be obliged
        to pay any claim that it would not otherwise be obligated to pay but for the attachment of any endorsements required by any State or Federal authority, the Insured agrees to reimburse the company in the amount paid and all sums including costs and expenses
        which shall have been paid in connection with such claims.</p>
      <div class="row-fluid">
        <span class="span4">
          <label class="control-label">How many additional vehicles?</label>
        </span>
        <span class="span1">
          <input class="textinput span12" type="text" name="" value="<?php if(count($edit_app)>0){echo count($edit_app); } ?>" id="value_id" onchange="vehicles_add(this.value)" placeholder=""  pattern="[0-9]+"  maxlength="2">
        </span>
        <span class="span7">
        <!-- <button class="btn"> <i class="icon icon-plus"></i> Add</button>-->
        </span>
      </div>
      <div class="well">
	  
	  <?php  $p=1; if(!empty($edit_app)){ foreach($edit_app as $value4) { ?> 
      <input type="hidden" value="<?php echo  isset($value4->vehicles_id) ? $value4->vehicles_id : '';?>" name="vehicles_id[]" /> 
      
	 
	<div id="add_per_rowss<?php echo $p;  ?>" class="vehicles_add_row">
	     <div class="row-fluid" >
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label pull-left span4">22.A.VIN &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" value="<?php echo  isset($value4->vehicles_VIN) ? $value4->vehicles_VIN : '';?>" name="vehicles_VIN[]">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4">22.B.GVW &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_GVW[]" value="<?php echo  isset($value4->vehicles_GVW) ? $value4->vehicles_GVW : '';?>" placeholder="">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4">22.C.Year &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" pattern="[0-9]+" value="<?php echo  isset($value4->vehicles_Year) ? $value4->vehicles_Year : '';?>" name="vehicles_Year[]"    pattern="^\d{4}$"  maxlength="4" >
          </span>
          <span class="span3">
            <label class="control-label pull-left span4">22.D.Make &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" value="<?php echo  isset($value4->vehicles_make) ? $value4->vehicles_make : '';?>" name="vehicles_make[]">
          </span>
        </div>
        <div class="row-fluid">
          
          <span class="span3">
            <label class="control-label pull-left span4">22.E.Body Type &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_body[]" value="<?php echo  isset($value4->vehicles_body) ? $value4->vehicles_body : '';?>" placeholder="">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4">22.F.Model &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_model[]" value="<?php echo  isset($value4->vehicles_model) ? $value4->vehicles_model : '';?>" placeholder="Tractors, Trailers, etc">
          </span>
           <span class="span3">
            <label class="control-label pull-left span5">22.G.Reason &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_reason[]" placeholder="" value="<?php echo  isset($value4->vehicles_reason) ? $value4->vehicles_reason : '';?>">
          </span>
        </div>
     </div>
    </div>
  
 
 
 <?php  $p++ ;  } } else{ ?>
	
	      <!-- <div class="row-fluid">
          <span class="span12">
           <button class="btn pull-right">Delete</button>
		  </span>
        </div>
 <div class="row-fluid" id="add_vehicles_row">
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left span3">VIN &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="vehicles_VIN[]">
          </span>
          <span class="span4">
            <label class="control-label pull-left span3">GVW &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_GVW[]" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left span3">Year &nbsp;</label>
            <input class="textinput span7" type="text" placeholder=""   pattern="^\d{4}$"  maxlength="4" name="vehicles_Year[]">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label pull-left span3">Make &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="vehicles_make[]">
          </span>
          <span class="span4">
            <label class="control-label pull-left span3">Body Type &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_body[]" placeholder="">
          </span>
          <span class="span4">
            <label class="control-label pull-left span3">Model &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_model[]" placeholder="Tractors, Trailers, etc">
          </span>
        </div>
	</div>-->
	<?php } ?>
    
    
    
	 <div class="row-fluid" id="add_vehicles_row" style="display:none;">
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label pull-left span4">22.A.VIN &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="vehicles_VIN[]">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4">22.B.GVW &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_GVW[]" placeholder="">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4">22.C.Year &nbsp;</label>
            <input class="textinput span7" type="text" placeholder=""   pattern="^\d{4}$"  maxlength="4" name="vehicles_Year[]">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4">22.D.Make &nbsp;</label>
            <input class="textinput span7" type="text" placeholder="" name="vehicles_make[]">
          </span>
        </div>
        <div class="row-fluid">
          
          <span class="span3">
            <label class="control-label pull-left span4">22.E.Body Type &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_body[]" placeholder="">
          </span>
          <span class="span3">
            <label class="control-label pull-left span4">22.F.Model  &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_model[]" value="" placeholder="Tractors, Trailers, etc">
          </span>
           <span class="span3">
            <label class="control-label pull-left span5">22.G.Reason &nbsp;</label>
            <input class="textinput span7" type="text" name="vehicles_reason[]" placeholder="" value="">
          </span>
        </div>
	</div>
	<div class="row-fluid" id="add_vehicles">
    </div>
	
  </div>
    
 </div>
          
          
		    <div class="row-fluid mt_15">
            <span class="span12">
              <label class="control-label left_pull">23.Does the applicant rent, lease, or sub haul vehicle(s) to others? &nbsp;</label>
              <select class="pull-left pull-left-1 width_option" onChange="loading_add(this.value);" id="applicant_rent1" name="applicant_rent" required>
               <option value="" >Select</option> 
              <?php $applicant_rent = isset($app_add[0]->applicant_rent) ? $app_add[0]->applicant_rent : '';?>
                <option value="No" <?php if($applicant_rent  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($applicant_rent  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
            </span>
          </div>
	
		<script>
//var t12=1;
$(document).ready(function(){

loading_add($('#applicant_rent1').val());

//t12++;

});

</script>  
		          		  <!-- GHI-5  -->
	
	 <div class="container-fluid" id="addload12" style="display:none;">
      <div class="page-header">
        <h1>GHI-5 <small>Driver/Sub-Hauler supplement form</small>
        </h1>
      </div>
      <div class="well">
	  <input type="hidden" name="ghi5_parent_id1" value="23"/>
	 <?php   if(!empty($app_ghi51a)) {  ?>
	  <input type="hidden" value="<?php echo isset($app_ghi51a[0]->haul_id) ? $app_ghi51a[0]->haul_id : ''; ?>" name="hauled_n_id1" />
        <label class="control-label">Check all practices used by your company: (Give full explanation of each question. Use separate sheet, if necessary)</label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">1.&nbsp;</label>
            <label class="checkbox pull-left">
			  <?php   $hauler_MVR = isset($app_ghi51a[0]->hauler_MVR) ? $app_ghi51a[0]->hauler_MVR : '';?>
             
              <input type="checkbox" value="1" <?php if($hauler_MVR  == 1) echo 'checked="checked"'; ?>  name="hauler_MVR1">
              <span>MVR check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Roa = isset($app_ghi51a[0]->hauler_Roa) ? $app_ghi51a[0]->hauler_Roa : '';?>
              <input type="checkbox" value="1" <?php if($hauler_Roa  == 1) echo 'checked="checked"'; ?> name="hauler_Roa1">
              <span>Road Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Wri = isset($app_ghi51a[0]->hauler_Wri) ? $app_ghi51a[0]->hauler_Wri : '';?>
              <input type="checkbox" value="1" <?php if($hauler_Wri  == 1) echo 'checked="checked"'; ?> name="hauler_Wri1">
              <span>Written application&nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Phy = isset($app_ghi51a[0]->hauler_Phy) ? $app_ghi51a[0]->hauler_Phy : '';?>
              <input type="checkbox" value="1"  <?php if($hauler_Phy  == 1) echo 'checked="checked"'; ?> name="hauler_Phy1">
              <span>Physical exam &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Drug = isset($app_ghi51a[0]->hauler_Drug) ? $app_ghi51a[0]->hauler_Drug : '';?>
              <input type="checkbox" value="1" <?php if($hauler_Drug  == 1) echo 'checked="checked"'; ?> name="hauler_Drug1">
              <span>Drug Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Ref = isset($app_ghi51a[0]->hauler_Ref) ? $app_ghi51a[0]->hauler_Ref : '';?>
              <input type="checkbox" value="1" <?php if($hauler_Ref  == 1) echo 'checked="checked"'; ?> name="hauler_Ref1">
              <span>Reference Check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Ver = isset($app_ghi51a[0]->hauler_Ver) ? $app_ghi51a[0]->hauler_Ver : '';?>
              <input type="checkbox" value="1" <?php if($hauler_Ver  == 1) echo 'checked="checked"'; ?> name="hauler_Ver1">
              <span>Employment Verification &nbsp;</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span10">
            <label class="control-label pull-left">2. Describe acceptability for hiring drivers:&nbsp;</label>
            <textarea class="span12 span12-1" placeholder=""  name="hauler_hir_name1"> <?php echo isset($app_ghi51a[0]->hauler_hir_name) ? trim($app_ghi51a[0]->hauler_hir_name) : '';?></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left ">3. Use Owner/Operators? &nbsp;</label>
            <select name="hauler_Own1" id="hauler_Own" class="span2" onchange="hauler_own_func(this.value);" required>
            <?php  $hauler_Own = isset($app_ghi51a[0]->hauler_Own) ? $app_ghi51a[0]->hauler_Own : '';?>
            <option value="" >Select</option>
                <option value="No" <?php if($hauler_Own  == 'No') echo 'selected="selected"'; ?> >No</option>

                 <option value="Yes" <?php if($hauler_Own  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>			
			
             
            </select>
			      <script>
																
																 $(document).ready(function(){
																	
																     hauler_own_func($('#hauler_Own').val());
																	
																 });
																 </script>	
			

            <input class="textinput  h5-active ui-state-valid" style="display:none;" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="% of Revenues" value="<?php echo isset($app_ghi51a[0]->hauler_Rev) ? trim($app_ghi51a[0]->hauler_Rev) : '';?>" name="hauler_Rev" id="hauler_Rev">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">4. Use team drivers? &nbsp;</label>
            <select id="hauler_team" name="hauler_team1" class="span2" onchange="hauler_team_func(this.value);">
			      <?php   $hauler_team = isset($app_ghi51a[0]->hauler_team) ? $app_ghi51a[0]->hauler_team : '';?>
                <option value="No" <?php if($hauler_team  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_team  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>			
			
             
            </select>
			      <script>
																
																 $(document).ready(function(){
																	
																     hauler_team_func($('#hauler_team').val());
																	
																 });
																 </script>	
            <input class="textinput" style="display:none;" type="text" placeholder="Number / Teams" name="hauler_Num" id="hauler_Num">
            <textarea name="hauler_Num_y1"  style="display:none;" id="hauler_Num_y" class="span12" placeholder="If yes, please explain"></textarea>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">5. Are Motor Vehicle Reports of employed drivers pulled and reviewed? &nbsp;</label>
            <select name="hauler_Vehi1" id="hauler_Vehi" class="span2" onchange="hauler_Moto_func(this.value);">
		    <?php   $hauler_Vehi = isset($app_ghi51a[0]->hauler_Vehi) ? $app_ghi51a[0]->hauler_Vehi : '';?>
                <option value="No" <?php if($hauler_Vehi  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_Vehi  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>			
			
             
            </select>
			      <script>
																
																 $(document).ready(function(){
																	
																     hauler_Moto_func($('#hauler_Vehi').val());
																	
																 });
																 </script>	
                                                                 <br/>
            <input class="textinput" style="display:none; float: left;" id="hauler_often" type="text" value="<?php echo isset($app_ghi51a[0]->hauler_often) ? $app_ghi51a[0]->hauler_often : '';?>" placeholder="If yes, how often" name="hauler_often">
      <!--      <input class="textinput" style="display:none;" id="hauler_attach" type="text" value="<?php echo isset($app_ghi51a[0]->hauler_attach) ? $app_ghi51a[0]->hauler_attach : '';?>" placeholder="Attach policies" name="hauler_attach">-->
       <span style="display:none;margin-top: 0px;float: left;" id="hauler_attach">
          <input type="button" id="first_uploaded15"   value="Upload">
									
								<input type="hidden"  name="hauler_attach1" id="uploaded_file15" value="<?php echo $app_ghi51a[0]->hauler_attach; ?>">
                                         <?php $file=explode(",",$app_ghi51a[0]->hauler_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
						<ul id="upload_file15" class="upload_file_name1"></ul>
                        </span>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">6. Are all drivers covered under worker's Compensation? &nbsp;</label>
            <select class="span2" name="hauler_drivers1" id="hauler_drivers" onchange="hauler_drivers1(this.value)">
            
		    <?php   $hauler_drivers = isset($app_ghi51a[0]->hauler_drivers) ? $app_ghi51a[0]->hauler_drivers : '';?>
                <option value="No" <?php if($hauler_drivers  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_drivers  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>	
            </select>
          </span>
        </div>
        <script>
		 $(document).ready(function(){
	    	hauler_drivers1($('#hauler_drivers').val());
		 });
		</script>
        <div class="row-fluid" id="insurance1" >
          <span class="span3">
            <label class="control-label">Name of insurance co.</label>
            <input class="textinput" type="text" value="<?php echo isset($app_ghi51a[0]->hauler_ins) ? $app_ghi51a[0]->hauler_ins : '';?>" placeholder="" name="hauler_ins1">
          </span>
          <span class="span3">
            <label class="control-label">Policy no.</label>
            <input class="textinput" type="text" placeholder="" value="<?php echo isset($app_ghi51a[0]->hauler_Pol) ? $app_ghi51a[0]->hauler_Pol : '';?>" name="hauler_Pol1">
          </span>
          <span class="span3">
            <label class="control-label">Effective date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" value="<?php echo isset($app_ghi51a[0]->hauler_Effect) ? $app_ghi51a[0]->hauler_Effect : '';?>" name="hauler_Effect" id="hauler_Effect1">
          </span>
          <span class="span3">
            <label class="control-label">Expiry date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" value="<?php echo isset($app_ghi51a[0]->hauler_Exp) ? $app_ghi51a[0]->hauler_Exp : '';?>" name="hauler_Exp" id="hauler_Exp1">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">7. Driver Turnover in the past year. &nbsp;</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label">Hired</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Hir1" value="<?php echo isset($app_ghi51a[0]->hauler_Hir) ? $app_ghi51a[0]->hauler_Hir : '';?>" >
          </span>
          <span class="span3">
            <label class="control-label">Terminated</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Ter1" value="<?php echo isset($app_ghi51a[0]->hauler_Ter) ? $app_ghi51a[0]->hauler_Ter : '';?>">
          </span>
          <span class="span3">
            <label class="control-label">Quit</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Quit1" value="<?php echo isset($app_ghi51a[0]->hauler_Quit) ? $app_ghi51a[0]->hauler_Quit : '';?>">
          </span>
          <span class="span3">
            <label class="control-label">Others</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Oth1" value="<?php echo isset($app_ghi51a[0]->hauler_Oth) ? $app_ghi51a[0]->hauler_Oth : '';?>">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span4">
            <label class="control-label">8. Max hours driven per day</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Max1" value="<?php echo  isset($app_ghi51a[0]->hauler_Max) ? $app_ghi51a[0]->hauler_Max : '';?>">
          </span>
          <span class="span4">
            <label class="control-label">Per week</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Per1" value="<?php echo isset($app_ghi51a[0]->hauler_Per) ? $app_ghi51a[0]->hauler_Per : '';?>">
          </span>
          <span class="span4">
            <label class="control-label">(5 day week or 7 day)</label>
            <input class="textinput" type="text" placeholder="" name="hauler_day1" value="<?php echo  isset($app_ghi51a[0]->hauler_day) ? $app_ghi51a[0]->hauler_day : '';?>" pattern="[0-9]+"  >
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">9. How are drivers compensated? &nbsp;</label>
    <?php $radio_c = isset($app_ghi51a[0]->hauler_comp) ? $app_ghi51a[0]->hauler_comp : '';?>         
		  <label class="radio pull-left">
              <input type="radio" value="Hourly" <?php if($radio_c  == 'Hourly') echo 'checked="checked"'; ?> name="hauler_comp1" >
              <span>Hourly &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Per trip" <?php if($radio_c  == 'Per trip') echo 'checked="checked"'; ?> name="hauler_comp1">
              <span>Per trip &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Salary" <?php if($radio_c  == 'Salary') echo 'checked="checked"'; ?> name="hauler_comp1">
              <span>Salary &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" <?php if($radio_c  == 'Others') echo 'checked="checked"'; ?> name="hauler_comp1">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" value="<?php echo  isset($app_ghi51a[0]->hauler_Others) ? $app_ghi51a[0]->hauler_Others : '';?>" name="hauler_Others1">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">10. What hours of the day do you drivers operate?</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">10.A.6 AM to 2 PM</label>
            <input class="textinput  h5-active ui-state-valid" id="hauler_hours" value="<?php echo  isset($app_ghi51a[0]->hauler_hours) ? $app_ghi51a[0]->hauler_hours : '';?>" pattern="([1-9]{1}|(10)|[0-9]{2,2})" type="text" placeholder="" name="hauler_hours1">
          </span>
          <span class="span4">
            <label class="control-label">10.B.2 PM to 10 PM</label>
            <input class="textinput h5-active ui-state-valid" id="hauler_hour" value="<?php echo  isset($app_ghi51a[0]->hauler_hour) ? $app_ghi51a[0]->hauler_hour : '';?>" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="" name="hauler_hour1">
          </span>
          <span class="span4">
            <label class="control-label">10.C.10 PM to 6 AM</label>
            <input class="textinput h5-active ui-state-valid" id="hauler_hou" value="<?php echo  isset($app_ghi51a[0]->hauler_hou) ? $app_ghi51a[0]->hauler_hou : '';?>" type="text"  pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="" name="hauler_hou1">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">11. Where do your drivers sleep when they are on a trip? &nbsp;</label>
            <label class="radio pull-left radio-1">
			<?php $radio_sleep = isset($app_ghi51a[0]->hauler_sleep) ? $app_ghi51a[0]->hauler_sleep : '';?>
              <input type="radio" value="At Home" <?php if($radio_sleep  == 'At Home') echo 'checked="checked"'; ?>  name="hauler_sleep1">
              <span>At Home &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Motel" <?php if($radio_sleep  == 'Motel') echo 'checked="checked"'; ?> name="hauler_sleep1">
              <span>Motel &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="In the cab" <?php if($radio_sleep  == 'In the cab') echo 'checked="checked"'; ?> name="hauler_sleep1">
              <span>In the cab &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" <?php if($radio_sleep  == 'Others') echo 'checked="checked"'; ?> name="hauler_sleep1">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" <?php echo  isset($app_ghi51a[0]->hauler_hother) ? $app_ghi51a[0]->hauler_hother : '';?> name="hauler_hother1">
            <label class="radio pull-left">
              <input type="radio" value="Hourly" <?php if($radio_sleep  == 'Hourly') echo 'checked="checked"'; ?> name="hauler_sleep1">
              <span>Hourly &nbsp;</span>
            </label>
          </span>
        </div>
        <p>You must inform the company before hiring any new driver. You should have confirmation in writing regarding the acceptability of the driver by GHI.</p>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">12. Is your operation subject to time restraints when delivering the commodity? &nbsp;</label>
            <select name="hauler_rest1" class="span2">
               <?php   $hauler_rest = isset($app_ghi51a[0]->hauler_rest) ? $app_ghi51a[0]->hauler_rest : '';?>
                <option value="No" <?php if($hauler_rest  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_rest  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>	
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">13. If not hauling for others, will the vehicles be parked at a jobsite most of the day? &nbsp;</label>
            <select class="span2" name="hauler_roth1">
               <?php   $hauler_roth = isset($app_ghi51a[0]->hauler_roth) ? $app_ghi51a[0]->hauler_roth : '';?>
                <option value="No" <?php if($hauler_roth  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_roth  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>	
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">14. Do you have haul for others? &nbsp;</label>
            <select class="span2" name="hauler_haul1" id="hauler_haul" onchange="hauler_haul1(this.value)">
               <?php   $hauler_haul = isset($app_ghi51a[0]->hauler_haul) ? $app_ghi51a[0]->hauler_haul : '';?>
                <option value="No" <?php if($hauler_haul  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_haul  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>	
            </select>
          </span>
        </div>
        <script>
			 $(document).ready(function(){
	    	hauler_haul1($('#hauler_haul').val());
		 });
		</script>
        <div class="row-fluid" id="hauler_haul12">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Is there any written agreement?</th>
                  <th>Copy attached?</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <input class="textinput" type="text" placeholder="" value="<?php echo  isset($app_ghi51a[0]->haul_name) ? $app_ghi51a[0]->haul_name : '';?>" name="haul_name1">
                  </td>
                  <td>
                    <select  name="haul_agree1" class="span6">
                    <?php   $haul_agree = isset($app_ghi51a[0]->haul_agree) ? $app_ghi51a[0]->haul_agree : '';?>
                <option value="No" <?php if($haul_agree  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_agree  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                    </select>
                  </td>
                  <td>
                    <select name="haul_attach1" class="span9" onchange="uploaded_show(this.value)">
                        <?php   $haul_attach = isset($app_ghi51a[0]->haul_attach) ? $app_ghi51a[0]->haul_attach : '';?>
                <option value="No" <?php if($haul_attach  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_attach  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                    </select>
                   <script type="text/javascript">
				   function uploaded_show(val)
				   {
					   if(val=='Yes')
					   {
						  $('#first_uploaded1').show();
					   }
					   else
					   {
						   $('#first_uploaded1').hide();
					   }
				   }
				   </script>
                  </td>
				  <td>
				  <input type="button" id="first_uploaded1"  value="Upload">
									
								<input type="hidden" name="haul_attach_file1" id="uploaded_file1" value="<?php echo $app_ghi51a[0]->haul_attach_file; ?>">
                                         <?php $file=explode(",",$app_ghi51a[0]->haul_attach_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
						<ul id="upload_file1" class="upload_file_name1"></ul>
				  
				  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">15. Gross receipts - Estimate this year:&nbsp;</label>
          </span>
          <span class="span4">
            <label class="control-label">Last year:</label>
            <input class="textinput price_format" type="text" placeholder="$" value="<?php echo  isset($app_ghi51a[0]->haul_Last) ? $app_ghi51a[0]->haul_Last : '';?>" name="haul_Last1">
          </span>
          <span class="span4">
            <label class="control-label">Next year:</label>
            <input class="textinput price_format" type="text" placeholder="$" value="<?php echo  isset($app_ghi51a[0]->haul_Next) ? $app_ghi51a[0]->haul_Next : '';?>" name="haul_Next1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">16. (i) Are any vehicles or equipment loaned, rented or leased to others? &nbsp;</label>
            <select name="haul_equi1">
              <?php   $haul_equi = isset($app_ghi51a[0]->haul_equi) ? $app_ghi51a[0]->haul_equi : '';?>
                <option value="No" <?php if($haul_equi  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_equi  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <label class="control-label pull-left">&nbsp; &nbsp; &nbsp;(ii) Do you lease, hire, rent or borrow any vehicles from others? &nbsp;</label>
        <label class="radio pull-left">
		<?php $haul_rent= isset($app_ghi51a[0]->haul_rent) ? $app_ghi51a[0]->haul_rent : '';?>
          <input type="radio" value="Yes" <?php if($haul_rent  == 'Yes') echo 'checked="checked"'; ?> name="haul_rent1">
          <span>Yes &nbsp;</span>
        </label>
        <label class="radio pull-left">
          <input type="radio" value="No" <?php if($haul_rent  == 'No') echo 'checked="checked"'; ?> name="haul_rent1">
          <span>No &nbsp;</span>
        </label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; What is the average length of the lease? &nbsp;</label>
            <input class="textinput" type="text" placeholder="" value="<?php echo  isset($app_ghi51a[0]->haul_lease) ? $app_ghi51a[0]->haul_lease : '';?>" name="haul_lease1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_agreement1" onchange="agreement_yes(this.value)">
             <?php   $haul_agreement = isset($app_ghi51a[0]->haul_agreement) ? $app_ghi51a[0]->haul_agreement : '';?>
                <option value="No" <?php if($haul_agreement  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_agreement  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
            <script type="text/javascript">
			function agreement_yes(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded2').show();
					   }
					   else
					   {
						   $('#first_uploaded2').hide();
					   }
	 		}
			</script>
         <!--   <input class="textinput pull-left" type="text" placeholder="Upload a copy of the agreement" name="">-->
			
			
			      <input type="button" id="first_uploaded2" class="up_file" value="Upload">
									
								<input type="hidden" name="haul_agree_file1" id="uploaded_file2" value="<?php echo $app_ghi51a[0]->haul_agree_file;?>">
                                <br />
                                                          <?php $file=explode(",",$app_ghi51a[0]->haul_agree_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
						<ul id="upload_file2" class="upload_file_name1"></ul>
			
			
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">17. What is the cost to lease, hire, rent or borrow vehicles? &nbsp;</label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span6">
            <label class="control-label">Per month</label>
            <input class="textinput price_format" type="text" value="<?php echo  isset($app_ghi51a[0]->haul_month) ? $app_ghi51a[0]->haul_month : '';?>" placeholder="$" name="haul_month1">
          </span>
          <span class="span6">
            <label class="control-label">Per year</label>
            <input class="textinput price_format" type="text" value="<?php echo  isset($app_ghi51a[0]->haul_year) ? $app_ghi51a[0]->haul_year : '';?>" placeholder="$" name="haul_year1">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">18. What type of vehicles do you lease, hire, rent or borrow? &nbsp;</label>
            <input class="textinput span6" type="text" value="<?php echo  isset($app_ghi51a[0]->haul_bor) ? $app_ghi51a[0]->haul_bor : '';?>" placeholder="" name="haul_bor1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">19. Do you use owner/operators (sub-hauler)? &nbsp;</label>
            <select name="haul_ope" class="span2" onchange="haul_ope1(this.value)" id="haul_ope1">
             <?php   $haul_ope = isset($app_ghi51a[0]->haul_ope) ? $app_ghi51a[0]->haul_ope : '';?>
                <option value="No" <?php if($haul_ope  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_ope  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="haul_ope11">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp;If yes, is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_yagree1" onchange="haul_yagree(this.value)">
    
             <?php   $haul_yagree = isset($app_ghi51a[0]->haul_yagree) ? $app_ghi51a[0]->haul_yagree : '';?>
                <option value="No" <?php if($haul_yagree  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_yagree  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
                        <script type="text/javascript">
			function haul_yagree(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded3').show();
					   }
					   else
					   {
						   $('#first_uploaded3').hide();
					   }
	 		}
			</script>
       <!--     <input class="textinput" type="text" placeholder="Upload If yes, provide the agreement" name="haul_yagree_file">-->
			        
			    <input type="button" id="first_uploaded3" class="up_file" value="Upload">
									
								<input type="hidden" name="haul_yagree_file1" id="uploaded_file3" value="<?php $app_ghi51a[0]->haul_yagree_file; ?>">
                                       <br />
                                                          <?php $file=explode(",",$app_ghi51a[0]->haul_yagree_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
						<ul id="upload_file3" class="upload_file_name1"></ul>
						
			 </span>
        </div>
            <div class="row-fluid">
              <span class="span9">
                <label class="control-label pull-left">20. Owner operator/sub-hauler have their own insurance? &nbsp;</label>
                <select  class="span2" name="haul_yins1" id="haul_yins" onchange="haul_yins1(this.value)">
                  <?php   $haul_yins = isset($app_ghi51a[0]->haul_yins) ? $app_ghi51a[0]->haul_yins : '';?>
                <option value="No" <?php if($haul_yins  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_yins  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                </select>
              </span>
            </div>
            <div class="row-fluid" id="haul_yins12">
              <span class="span9">
                <button class="btn btn-small" onclick="add_insurance();">Add insurance</button>
              </span>
            </div>
         
        <table class="table" id="haul_yins11">
          <thead>
            <tr>
              <th>Name</th>
              <th>Insurance Carrier</th>
              <th>Insurance Certificate Attached</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
		  
		  <?php if(!empty($app_ghi51b)){ foreach($app_ghi51b as $values3){ ?>
		 <input type="hidden" value="<?php echo  isset($values3->add_ins_id) ? $values3->add_ins_id : '';?>" name="add_ins_id1[]" />
		 <input type="hidden" value="<?php echo  isset($values3->haul_id) ? $values3->haul_id : '';?>" name="haul_ins_id1[]" />
		      <tbody id="add_new_table">
            <tr>
              <td>
                <input class="textinput" type="text" value="<?php echo  isset($values3->add_ins_name) ? $values3->add_ins_name : '';?>" placeholder="" name="add_ins_name1[]">
              </td>
              <td>
                <input class="textinput" type="text" value="<?php echo  isset($values3->add_ins_carrier) ? $values3->add_ins_carrier : '';?>" placeholder="" name="add_ins_carrier1[]">
              </td>
              <td>
                <select name="ins_attach_y1[]" class="span1" onchange="ins_attach_y(this.value)">
                <?php  $ins_attach_y = isset($values3->ins_attach_y) ? $values3->ins_attach_y : '';?>
                <option value="No" <?php if($ins_attach_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($ins_attach_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                </select>
                            <script type="text/javascript">
			function ins_attach_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded4').show();
					   }
					   else
					   {
						   $('#first_uploaded4').hide();
					   }
	 		}
			</script>
              </td>
              <td>
			  <input type="button" id="first_uploaded4" value="Upload">
									

									<input type="hidden" name="add_ins_attach1[]" id="uploaded_file4" value="<?php echo $values3->add_ins_attach; ?>">
                                                             <br />
                                                          <?php $file=explode(",",$values3->add_ins_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
									<ul id="upload_file4" class="upload_file_name1"></ul>
              
              </td>
              <td><!-- <i class="icon icon-remove"></i> -->
			  <button class="btn btn-mini btn-1 remove_ins_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
              </td>
            </tr>
		
          </tbody>
		  <?php } } else {  ?>
		  
          <tbody id="add_new_table">
            <tr>
              <td>
                <input class="textinput" type="text"  placeholder="" name="add_ins_name1[]">
              </td>
              <td>
                <input class="textinput" type="text" placeholder="" name="add_ins_carrier1[]">
              </td>
              <td>
                <select name="ins_attach_y1[]" class="span1" onchange="ins_attach_y(this.value)">
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </td>
              <td>
                          <script type="text/javascript">
			function ins_attach_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded4').show();
					   }
					   else
					   {
						   $('#first_uploaded4').hide();
					   }
	 		}
			</script>
			  <input type="button" id="first_uploaded4" value="Upload">
									

									<input type="hidden" name="add_ins_attach1[]" id="uploaded_file4" value="<?php echo $values3->add_ins_attach ; ?>">
                                                                               <br />
                                                          <?php $file=explode(",",$values3->add_ins_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
									<ul id="upload_file4" class="upload_file_name1"></ul>
              
              </td>
              <td><!-- <i class="icon icon-remove"></i> -->
			  <button class="btn btn-mini btn-1 remove_ins_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
              </td>
            </tr>
		
          </tbody>
		  
	<?php }  ?>
        </table>
		 
		<table class="table" >
		</table>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">21. Will owner/operators(sub-haulers) be scheduled on your policy? &nbsp;</label>
            <select name="ins_pol1" class="span2" id="ins_pol" onchange="ins_pol1(this.value)">
               <?php  $ins_pol = isset($app_ghi51a[0]->ins_pol) ? $app_ghi51a[0]->ins_pol : '';?>
                <option value="No" <?php if($ins_pol  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($ins_pol  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="ins_pol12">
          <span class="span12">
            <button class="btn btn-small" onclick="add_owner();">Add owner/operator</button>
          </span>
        </div>
        <div class="row-fluid" id="ins_pol11">
          <span class="span12">
            <table class="table" id="add_owner_table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Agreement Attached?</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
		<?php  if(!empty($app_ghi51c)){ foreach($app_ghi51c as $values4){ ?>	  
		 <input type="hidden" value="<?php echo  isset($values4->add_owner_id) ? $values4->add_owner_id : '';?>" name="add_owner_id1[]" />
		 <input type="hidden" value="<?php echo  isset($values4->haul_id) ? $values4->haul_id : '';?>" name="haul_owner_id1[]" />
		
              <tbody id="add_owner_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" value="<?php echo  isset($values4->add_owner_name) ? $values4->add_owner_name : '';?>" name="add_owner_name1[]">
                  </td>
                  <td>
                    <select name="add_owner_y1[]" class="span6" onchange="add_owner_y(this.value)">
                   <?php  $add_owner_y = isset($values4->add_owner_y) ? $values4->add_owner_y : '';?>
                   <option value="No" <?php if($add_owner_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($add_owner_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                    </select>
                  </td>
                  <td>
				              <script type="text/javascript">
			function add_owner_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded5').show();
					   }
					   else
					   {
						   $('#first_uploaded5').hide();
					   }
	 		}
			</script>
				   <input type="button" id="first_uploaded5" value="Upload">
						<input type="hidden" name="add_owner_attach1[]" id="uploaded_file5" value="<?php echo $values4->add_owner_attach ;?>">
                                                                                      <br />
                                                          <?php $file=explode(",",$values4->add_owner_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
					<ul id="upload_file5" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td> <!--<i class="icon icon-remove"></i>-->
				 <button class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
			  <?php } } else {  ?>
                     <tbody id="add_owner_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_owner_name1[]">
                  </td>
                  <td>
                    <select name="add_owner_y1[]" class="span6" onchange="add_owner_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
				  	              <script type="text/javascript">
			function add_owner_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded5').show();
					   }
					   else
					   {
						   $('#first_uploaded5').hide();
					   }
	 		}
			</script>
				   <input type="button" id="first_uploaded5" value="Upload">
									

									<input type="hidden" name="add_owner_attach1[]" id="uploaded_file5" value="<?php echo $values4->add_owner_attach; ?>">
                                                             <br />
                                                          <?php $file=explode(",",$values4->add_owner_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
									<ul id="upload_file5" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td> <!--<i class="icon icon-remove"></i>-->
				 <button class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
			  

             <?php } ?>			  
			  
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">22. Do you use subcontractors? &nbsp;</label>
            <select class="span2" name="subcon_y1" onchange="subcon_y1(this.value)" id="subcon_y">
             <?php  $subcon_y = isset($app_ghi51a[0]->subcon_y) ? $app_ghi51a[0]->subcon_y : '';?>
                   <option value="No" <?php if($subcon_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($subcon_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
      <div class="row-fluid" id="subcon_y12">  
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">a. Who are your subcontractors? &nbsp;</label>
            <input class="textinput span9" value="<?php echo  isset($app_ghi51a[0]->subcon_name) ? $app_ghi51a[0]->subcon_name : ''; ?>"type="text" placeholder="" name="subcon_name1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">b. Are subcontractors required to provide Certificates of Insurance? &nbsp;</label>
            <select class="span2" name="subcon_cert1">
               <?php  $subcon_cert = isset($app_ghi51a[0]->subcon_cert) ? $app_ghi51a[0]->subcon_cert : '';?>
                   <option value="No" <?php if($subcon_cert  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($subcon_cert  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span   class="span12">
            <label class="control-label pull-left">c. What limit of Auto Liability are subcontractors required to carry? &nbsp;</label>
            <input class="textinput span6" value="<?php echo  isset($app_ghi51a[0]->subcon_lia) ? $app_ghi51a[0]->subcon_lia : ''; ?>" type="text" placeholder="" name="subcon_lia1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">d. What job duties are performed by the subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" value="<?php echo  isset($app_ghi51a[0]->subcon_duties) ? $app_ghi51a[0]->subcon_duties : ''; ?>" placeholder="" name="subcon_duties1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">e. What is your cost to use subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" value="<?php echo  isset($app_ghi51a[0]->subcon_cost) ? $app_ghi51a[0]->subcon_cost : ''; ?>" placeholder="" name="subcon_cost1">
          </span>
        </div>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">23. At any time will your employees, subcontractors, or owner/operators lease vehicles on your behalf? &nbsp;</label>
            <select class="span2" name="subcon_emp_y1" onchange="subcon_emp_y1(this.value)" id="subcon_emp_y">
               
               <?php  $subcon_emp_y = isset($app_ghi51a[0]->subcon_emp_y) ? $app_ghi51a[0]->subcon_emp_y : '';?>
                   <option value="No" <?php if($subcon_emp_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($subcon_emp_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="subcon_emp_y12">
          <span class="span12">
            <button class="btn btn-small" onclick="add_subcontract();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="subcon_emp_y11">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Type of Vehicle</th>
                  <th>Lease agreement attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
	 <?php  if(!empty($app_ghi51d)){ foreach($app_ghi51d as $values5){ ?>	
	  <input type="hidden" value="<?php echo  isset($values5->subcon_add_id) ? $values5->subcon_add_id : '';?>" name="subcon_add_id1[]" />
		 <input type="hidden" value="<?php echo  isset($values5->haul_id) ? $values5->haul_id : '';?>" name="haul_subcon_id1[]" />
		
              <tbody id="add_subcontract_row">
                <tr>
                  <td>
                    <input class="textinput" type="text" placeholder="" value="<?php echo  isset($values5->subcon_add_name) ? $values5->subcon_add_name : ''; ?>" name="subcon_add_name1[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" value="<?php echo  isset($values5->subcon_add_vehicle) ? $values5->subcon_add_vehicle : ''; ?>" name="subcon_add_vehicle1[]">
                  </td>
                  <td>
                    <select name="subcon_add_y[]" class="span9" onchange="subcon_add_y(this.value)">
                    <?php  $subcon_add_y = isset($values5->subcon_add_y) ? $values5->subcon_add_y : '';?>
                   <option value="No" <?php if($subcon_add_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($subcon_add_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                    </select>
                  </td>
                  <td>
              <script type="text/javascript">
			function subcon_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded6').show();
					   }
					   else
					   {
						   $('#first_uploaded6').hide();
					   }
	 		}
			</script>
				   <input type="button" id="first_uploaded6" value="Upload">
									

									<input type="hidden" name="subcon_add_attach1[]" id="uploaded_file6" value="<?php echo $values5->subcon_add_attach; ?>">
                                     <?php $file=explode(",",$values5->subcon_add_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
                                    
									<ul id="upload_file6" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td><!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_sub_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
			  <?php } } else { ?>
			  
			    <tbody id="add_subcontract_row">
                <tr>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_name1[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_vehicle1[]">
                  </td>
                  <td>
                    <select name="subcon_add_y[]" class="span9" onchange="subcon_add_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                  <script type="text/javascript">
			function subcon_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded6').show();
					   }
					   else
					   {
						   $('#first_uploaded6').hide();
					   }
	 		}
			</script>
				   <input type="button" id="first_uploaded6" value="Upload">
									

									<input type="hidden" name="subcon_add_attach1[]" id="uploaded_file6" value="">
                 
									<ul id="upload_file6" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td><!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_sub_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
			  
			  
			  <?php } ?>
			  
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">24. Do any employees, subcontractors or sub-haulers use their vehicles while conducting your business? &nbsp;</label>
            <select class="span2" name="sub_haul_y1" onchange="sub_haul_y1(this.value)"  id="sub_haul_y">
                  <?php  $sub_haul_y = isset($app_ghi51a[0]->sub_haul_y) ? $app_ghi51a[0]->sub_haul_y : '';?>
                   <option value="No" <?php if($sub_haul_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($sub_haul_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y11">
          <span class="span12">
            <button class="btn btn-small" onclick="add_conduct();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y12">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Insurance Carrier</th>
                  <th class="th-1">Limit of Liability Ins.</th>
                  <th class="th-1">Insurance Certificate Attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
	<?php  if(!empty($app_ghi51e)){ foreach($app_ghi51e as $values6){ ?>			  
		 <input type="hidden" value="<?php echo  isset($values6->sub_haul_id) ? $values6->sub_haul_id : '';?>" name="sub_haul_id[]" />
		 <input type="hidden" value="<?php echo  isset($values6->haul_id) ? $values6->haul_id : '';?>" name="haul_sub_id[]" />
		 
	  
              <tbody id="add_conduct_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" value="<?php echo  isset($values6->sub_haul_add_name) ? $values6->sub_haul_add_name : ''; ?>" placeholder="" name="sub_haul_add_name1[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" value="<?php echo  isset($values6->sub_haul_add_carrier) ? $values6->sub_haul_add_carrier : ''; ?>" name="sub_haul_add_carrier1[]">
                  </td>
                  <td>
                    <input class="textinput span11 price_format" type="text" value="<?php echo  isset($values6->sub_haul_add_limit) ? $values6->sub_haul_add_limit : ''; ?>" placeholder="$" name="sub_haul_add_limit1[]">
                  </td>
                  <td>
                    <select name="sub_haul_add_y1[]" class="span6" onchange="sub_haul_add_y(this.value)">
                     <?php  $sub_haul_add_y = isset($values6->sub_haul_add_y) ? $values6->sub_haul_add_y : '';?>
                   <option value="No" <?php if($sub_haul_add_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($sub_haul_add_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                    </select>
                  </td>
                  <td>
            <script type="text/javascript">
			//onchange="sub_haul_add_y(this.value)"
			function sub_haul_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded7').show();
					   }
					   else
					   {
						   $('#first_uploaded7').hide();
					   }
	 		}
			</script>                   
                  <input type="button" id="first_uploaded7" value="Upload">
				  <input type="hidden" name="sub_haul_add_attach1[]" id="uploaded_file7" value="<?php echo $values6->sub_haul_add_attach;?>">
                  <br/>
                                                         <?php $file=explode(",",$values6->sub_haul_add_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
					<ul id="upload_file7" class="upload_file_name1"></ul>
				  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_con_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                   
                  </td>
                </tr>
              </tbody>
			  <?php } } else { ?>
			  
			   <tbody id="add_conduct_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="sub_haul_add_name1[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="sub_haul_add_carrier1[]">
                  </td>
                  <td>
                    <input class="textinput span11 price_format" type="text" placeholder="$" name="sub_haul_add_limit1[]">
                  </td>
                  <td>
                    <select name="sub_haul_add_y1[]" class="span6" onchange="sub_haul_add_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                               <script type="text/javascript">
			//onchange="sub_haul_add_y(this.value)"
			function sub_haul_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded7').show();
					   }
					   else
					   {
						   $('#first_uploaded7').hide();
					   }
	 		}
			</script>
                  <input type="button" id="first_uploaded7" value="Upload">
				  <input type="hidden" name="sub_haul_add_attach1[]" id="uploaded_file7" value="">
					<ul id="upload_file7" class="upload_file_name1"></ul>
				  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_con_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                   
                  </td>
                </tr>
              </tbody>
			  
			  <?php }    ?>
			  
			  
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">25. Do you understand that we may audit your records? &nbsp;</label>
            <select class="span2" name="audit_y1">
                <?php  $audit_y = isset($app_ghi51a[0]->audit_y) ? $app_ghi51a[0]->audit_y : '';?>
                   <option value="No" <?php if($audit_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($audit_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">26. Do any of your family members use the vehicles? &nbsp;</label>
            <select class="span2" name="vehicles_y1" id="vehicles_y" onchange="vehicles_y1(this.value)">
                 <?php  $vehicles_y = isset($app_ghi51a[0]->vehicles_y) ? $app_ghi51a[0]->vehicles_y : '';?>
                   <option value="No" <?php if($vehicles_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($vehicles_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="vehicles_y11">
          <span class="span12">
            <button class="btn btn-small" onclick="add_members();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="vehicles_y12">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Relationship</th>
                  <th></th>
                </tr>
              </thead>
	<?php  if(!empty($app_ghi51f)){ foreach($app_ghi51f as $values7){ ?>			  
		 <input type="hidden" value="<?php echo  isset($values7->add_mem_id) ? $values7->add_mem_id : '';?>" name="add_mem_id1[]" />
		 <input type="hidden" value="<?php echo  isset($values7->haul_id) ? $values7->haul_id : '';?>" name="haul_mem_id1[]" />
		     <tbody id="add_members_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" value="<?php echo  isset($values7->add_mem_name) ? $values7->add_mem_name : ''; ?>" placeholder="" name="add_mem_name1[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" value="<?php echo  isset($values7->add_mem_rel) ? $values7->add_mem_rel : ''; ?>" placeholder="" name="add_mem_rel1[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_mem_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                    
                  </td>
                </tr>
              </tbody>
		
            
			  <?php } } else { ?>
			  
			    <tbody id="add_members_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_name1[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_rel1[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_mem_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                    
                  </td>
                </tr>
              </tbody>
			
			  
			  <?php } ?>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">27. Do you allow passengers to ride in your vehicles? &nbsp;</label>
            <select class="span2" name="ride_y1">
             <?php  $ride_y = isset($app_ghi51a[0]->ride_y) ? $app_ghi51a[0]->ride_y : '';?>
                   <option value="No" <?php if($ride_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($ride_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">28. Are you familiar with the U.S Department of Transportation driver requirements? &nbsp;</label>
            <select class="span2" name="driver_y1">
              <?php  $driver_y = isset($app_ghi51a[0]->driver_y) ? $app_ghi51a[0]->driver_y : '';?>
                   <option value="No" <?php if($driver_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($driver_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">29. (i) Do you mantain driver activity files? &nbsp;</label>
            <select class="span2" name="activity_y1">
            <?php  $activity_y = isset($app_ghi51a[0]->activity_y) ? $app_ghi51a[0]->activity_y : '';?>
                   <option value="No" <?php if($activity_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($activity_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iI) Do you review current MVRs on all drivers prior to hiring? &nbsp;</label>
            <select class="span2" name="prior_y1">
             <?php  $prior_y = isset($app_ghi51a[0]->prior_y) ? $app_ghi51a[0]->prior_y : '';?>
                   <option value="No" <?php if($prior_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($prior_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iii) Is there a formal driver hiring procedure? &nbsp;</label>
            <label class="radio pull-left">
			<?php $procedure_y= isset($app_ghi51a[0]->procedure_y) ? $app_ghi51a[0]->procedure_y : '';?>
       
              <input type="radio" value="Yes" <?php if($procedure_y  == 'Yes') echo 'checked="checked"'; ?> name="procedure_y1">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" <?php if($procedure_y  == 'No') echo 'checked="checked"'; ?> name="procedure_y1">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iv) Drug Screening? &nbsp;</label>
			<?php $Drug_y= isset($app_ghi51a[0]->Drug_y) ? $app_ghi51a[0]->Drug_y : '';?>
            <label class="radio pull-left">
              <input type="radio" value="Yes" <?php if($Drug_y  == 'Yes') echo 'checked="checked"'; ?> name="Drug_y1">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="NO" <?php if($Drug_y  == 'No') echo 'checked="checked"'; ?> name="Drug_y1">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(v) If you have a formal driver hiring/training program, provide a copy with this application? &nbsp;</label>
                <input type="button" id="first_uploaded8" value="Upload">
									

									<input type="hidden" name="hiring_file1" id="uploaded_file8" value="<?php echo $app_ghi51a[0]->hiring_file; ?>">
                                      <br/>
                                                         <?php $file=explode(",",$app_ghi51a[0]->hiring_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
									<ul id="upload_file8" class="upload_file_name1"></ul>
			
			<!--<input class="textinput" type="upload" placeholder="Upload file" name="hiring_file" value="Upload">-->
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">30. Are all drivers employees? &nbsp;</label>
			<?php $employees_y= isset($app_ghi51a[0]->employees_y) ? $app_ghi51a[0]->employees_y : '';?>
            <label class="radio pull-left">
              <input type="radio" value="Yes" <?php if($employees_y  == 'Yes') echo 'checked="checked"'; ?> onclick="driver_emp(this.value);" name="employees_y1">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" <?php if($employees_y  == 'Yes') echo 'checked="checked"'; ?> onclick="driver_emp(this.value);"  name="employees_y1">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" id="explain_no" >
          <span class="span12">
            <label class="control-label pull-left">If no, please explain &nbsp;</label>
            <input class="textinput span10" type="text" placeholder="" value="<?php echo isset($app_ghi51a[0]->explain_no) ? $app_ghi51a[0]->explain_no : ''; ?>" name="explain_no1">
          </span>
        </div>
		<span id="yes_employee" style="display:none;">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">If yes, list below &nbsp;</label>
            <button class="btn btn-small" onclick="add_employee();">Add</button>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Length of employment</th>
                  <th class="th-1">Years/Months</th>
                  <th></th>
                </tr>
              </thead>
<?php  if(!empty($app_ghi51g)){ foreach($app_ghi51g as $values8){ ?>		
 <input type="hidden" value="<?php echo  isset($values8->add_list_id) ? $values8->add_list_id : '';?>" name="add_list_id1[]" />
		 <input type="hidden" value="<?php echo  isset($values8->haul_id) ? $values8->haul_id : '';?>" name="haul_list_id1[]" />
			  
              <tbody id="add_employee_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" value="<?php echo  isset($values8->add_list_name) ? $values8->add_list_name : '';?>"  name="add_list_name1[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" value="<?php echo  isset($values8->add_list_length) ? $values8->add_list_length : '';?>"  name="add_list_length1[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" value="<?php echo  isset($values8->add_list_year) ? $values8->add_list_year : '';?>"  name="add_list_year1[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_emp_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
				   </td>
                </tr>
              </tbody>
			  
			  <?php } } else { ?>
			  
			       <tbody id="add_employee_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_name1[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_length1[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_year1[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_emp_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
				   </td>
                </tr>
              </tbody>
             
			  
			  
			  <?php } ?>
            </table>
          </span>
        </div>
		</span>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">31. Is there a formal safety program? &nbsp;</label>
            <label class="radio pull-left">
				<?php $safety_y= isset($app_ghi51a[0]->safety_y) ? $app_ghi51a[0]->safety_y : '';?>
           
            
              <input type="radio" onclick="formal_safety(this.value);" id="safety_y" value="Yes" <?php if($safety_y  == 'Yes') echo 'checked="checked"'; ?> name="safety_y1">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" onclick="formal_safety1(this.value);" id="safety_y1" <?php if($safety_y  == 'No') echo 'checked="checked"'; ?> value="No" name="safety_y1">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" id="details_y" style="display:none;">
          <span class="span12">
            <label class="control-label pull-left">If yes, provide details or a copy:</label>
             
             	<input type="button" id="first_uploaded16" value="Upload"><br/>
				<input type="hidden" name="safety_attach_file" id="uploaded_file16" value="<?php echo $app_ghi51a[0]->safety_attach_file; ?>">   
              <?php $file=explode(",",$app_ghi51a[0]->safety_attach_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>						
                          <ul id="upload_file16" class="upload_file_name1"></ul> 
            <textarea class="span12 span12-1" placeholder="" value="<?php echo  isset($app_ghi51a[0]->details_y) ? $app_ghi51a[0]->details_y : '';?>" name="details_y1" ></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">32. Provide details of your maintenance program:</label>
            <textarea class="span10 span12-1" placeholder=""  name="program_detail1"><?php echo  isset($app_ghi51a[0]->program_detail) ? $app_ghi51a[0]->program_detail : '';?></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">33. Do you agree to screen and report all potential operators immediately upon hiring before giving them a load? &nbsp;</label>
            <label class="radio pull-left">
			<?php $load_y= isset($app_ghi51a[0]->load_y) ? $app_ghi51a[0]->load_y : '';?>
              <input type="radio" value="Yes" <?php if($load_y  == 'Yes') echo 'checked="checked"'; ?> name="load_y1">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" <?php if($load_y  == 'Yes') echo 'checked="checked"'; ?> name="load_y1">
              <span>No</span>
            </label>
          </span>
        </div>
		 </div>
		<?php }  else {  ?>
		
		<label class="control-label">Check all practices used by your company: (Give full explanation of each question. Use separate sheet, if necessary)</label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">1.&nbsp;</label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_MVR1">
              <span>MVR check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Roa1">
              <span>Road Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Wri1">
              <span>Written application&nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Phy1">
              <span>Physical exam &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Drug1">
              <span>Drug Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Ref1">
              <span>Reference Check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Ver1">
              <span>Employment Verification &nbsp;</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span10">
            <label class="control-label pull-left">2. Describe acceptability for hiring drivers:&nbsp;</label>
            <textarea class="span12 span12-1" placeholder="" name="hauler_hir_name1"></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left ">3. Use Owner/Operators? &nbsp;</label>
            <select name="hauler_Own1" class="span2" onchange="hauler_own_func(this.value);">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
             
            </select>

            <input class="textinput  h5-active ui-state-valid" style="display:none;" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="% of Revenues" name="hauler_Rev1" id="hauler_Rev">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">4. Use team drivers? &nbsp;</label>
            <select name="hauler_team1" class="span2" onchange="hauler_team_func(this.value);">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
             
            </select>
            <input class="textinput" style="display:none;" type="text" placeholder="Number / Teams" name="hauler_Num" id="hauler_Num1">
            <textarea name="hauler_Num_y1"  style="display:none;" id="hauler_Num_y" class="span12" placeholder="If yes, please explain"></textarea>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">5. Are Motor Vehicle Reports of employed drivers pulled and reviewed? &nbsp;</label>
            <select name="hauler_Vehi1" class="span2" onchange="hauler_Moto_func(this.value);">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
              
            </select>
            <br/>
            <input class="textinput" style="display:none;float: left;" id="hauler_often" type="text" placeholder="If yes, how often" name="hauler_often">
            <span style="display:none;margin-top: 0px;float: left;" id="hauler_attach1">
            <input type="button" id="first_uploaded15"  value="Upload">
									
								<input type="hidden"  name="hauler_attach" id="uploaded_file15" value="">
                                       
						<ul id="upload_file15" class="upload_file_name1"></ul>
                        </span>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">6. Are all drivers covered under worker's Compensation? &nbsp;</label>
            <select class="span2" name="hauler_drivers1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label">Name of insurance co.</label>
            <input class="textinput" type="text" placeholder="" name="hauler_ins1">
          </span>
          <span class="span3">
            <label class="control-label">Policy no.</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Pol1">
          </span>
          <span class="span3">
            <label class="control-label">Effective date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" name="hauler_Effect" id="hauler_Effect1">
          </span>
          <span class="span3">
            <label class="control-label">Expiry date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" name="hauler_Exp" id="hauler_Exp1">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">7. Driver Turnover in the past year. &nbsp;</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label">Hired</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Hir1" >
          </span>
          <span class="span3">
            <label class="control-label">Terminated</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Ter1">
          </span>
          <span class="span3">
            <label class="control-label">Quit</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Quit1">
          </span>
          <span class="span3">
            <label class="control-label">Others</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Oth1">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span4">
            <label class="control-label">8. Max hours driven per day</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Max1">
          </span>
          <span class="span4">
            <label class="control-label">Per week</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Per1">
          </span>
          <span class="span4">
            <label class="control-label">(5 day week or 7 day)</label>
            <input class="textinput" type="text" placeholder="" name="hauler_day1" pattern="[0-9]+"  >
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">9. How are drivers compensated? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Hourly" name="hauler_comp1">
              <span>Hourly &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Per trip" name="hauler_comp1">
              <span>Per trip &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Salary" name="hauler_comp1">
              <span>Salary &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" name="hauler_comp1">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" name="hauler_Others1">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">10. What hours of the day do you drivers operate?</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">10.A. &nbsp;6 AM to 2 PM</label>
            <input class="textinput  h5-active ui-state-valid" pattern="([1-9]{1}|(10)|[0-9]{2,2})" type="text" placeholder="%" name="hauler_hours1">
          </span>
          <span class="span4">
            <label class="control-label">10.B. &nbsp;2 PM to 10 PM</label>
            <input class="textinput h5-active ui-state-valid" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="%" name="hauler_hour1">
          </span>
          <span class="span4">
            <label class="control-label">10.C. &nbsp;10 PM to 6 AM</label>
            <input class="textinput h5-active ui-state-valid" type="text"  pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="%" name="hauler_hou1">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">11. Where do your drivers sleep when they are on a trip? &nbsp;</label>
            <label class="radio pull-left radio-1">
              <input type="radio" value="At Home" name="hauler_sleep1">
              <span>At Home &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Motel" name="hauler_sleep1">
              <span>Motel &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="In the cab" name="hauler_sleep1">
              <span>In the cab &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" name="hauler_sleep1">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" name="hauler_hother1">
            <label class="radio pull-left">
              <input type="radio" value="Hourly" name="hauler_sleep1">
              <span>Hourly &nbsp;</span>
            </label>
          </span>
        </div>
        <p>You must inform the company before hiring any new driver. You should have confirmation in writing regarding the acceptability of the driver by GHI.</p>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">12. Is your operation subject to time restraints when delivering the commodity? &nbsp;</label>
            <select name="hauler_rest1" class="span2">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">13. If not hauling for others, will the vehicles be parked at a jobsite most of the day? &nbsp;</label>
            <select class="span2" name="hauler_roth1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">14. Do you have haul for others? &nbsp;</label>
            <select class="span2" name="hauler_haul1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Is there any written agreement?</th>
                  <th>Copy attached?</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="haul_name1">
                  </td>
                  <td>
                    <select  name="haul_agree" class="span6">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                    <select name="haul_attach1" class="span9" onchange="uploaded_show(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                   <script type="text/javascript">
				   function uploaded_show(val)
				   {
					   if(val=='Yes')
					   {
						  $('#first_uploaded1').show();
					   }
					   else
					   {
						   $('#first_uploaded1').hide();
					   }
				   }
				   </script>
                  </td>
				  <td>
				  <input type="button" id="first_uploaded1"  value="Upload">
									
								<input type="hidden" name="haul_attach_file1" id="uploaded_file1" value="">
						<ul id="upload_file1" class="upload_file_name1"></ul>
				  
				  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">15. Gross receipts - Estimate this year:&nbsp;</label>
          </span>
          <span class="span4">
            <label class="control-label">Last year:</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_Last1">
          </span>
          <span class="span4">
            <label class="control-label">Next year:</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_Next1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">16. (i) Are any vehicles or equipment loaned, rented or leased to others? &nbsp;</label>
            <select name="haul_equi1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <label class="control-label pull-left">&nbsp; &nbsp; &nbsp;(ii) Do you lease, hire, rent or borrow any vehicles from others? &nbsp;</label>
        <label class="radio pull-left">
          <input type="radio" value="Yes" name="haul_rent1">
          <span>Yes &nbsp;</span>
        </label>
        <label class="radio pull-left">
          <input type="radio" value="No" name="haul_rent1">
          <span>No &nbsp;</span>
        </label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; What is the average length of the lease? &nbsp;</label>
            <input class="textinput" type="text" placeholder="" name="haul_lease1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_agreement1" onchange="agreement_yes(this.value)">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
            <script type="text/javascript">
			function agreement_yes(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded2').show();
					   }
					   else
					   {
						   $('#first_uploaded2').hide();
					   }
	 		}
			</script>
         <!--   <input class="textinput pull-left" type="text" placeholder="Upload a copy of the agreement" name="">-->
			
			
			      <input type="button" id="first_uploaded2" class="up_file" value="Upload">
									
								<input type="hidden" name="haul_agree_file1" id="uploaded_file2" value="">
						<ul id="upload_file2" class="upload_file_name1"></ul>
			
			
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">17. What is the cost to lease, hire, rent or borrow vehicles? &nbsp;</label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span6">
            <label class="control-label">Per month</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_month1">
          </span>
          <span class="span6">
            <label class="control-label">Per year</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_year1">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">18. What type of vehicles do you lease, hire, rent or borrow? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="haul_bor1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">19. Do you use owner/operators (sub-hauler)? &nbsp;</label>
            <select name="haul_ope" class="span2" onchange="haul_ope1(this.value)" id="haul_ope1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="haul_ope11">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp;If yes, is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_yagree1" onchange="haul_yagree(this.value)">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
       <!--     <input class="textinput" type="text" placeholder="Upload If yes, provide the agreement" name="haul_yagree_file">-->
			      <script type="text/javascript">
			function haul_yagree(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded3').show();
					   }
					   else
					   {
						   $('#first_uploaded3').hide();
					   }
	 		}
			</script>  
			    <input type="button" id="first_uploaded3" class="up_file" value="Upload">
									
								<input type="hidden" name="haul_yagree_file1" id="uploaded_file3" value="">
						<ul id="upload_file3" class="upload_file_name1"></ul>
						
			 </span>
        </div>
            <div class="row-fluid">
              <span class="span9">
                <label class="control-label pull-left">20. Owner operator/sub-hauler have their own insurance? &nbsp;</label>
                <select  class="span2" name="haul_yins1" id="haul_yins" onchange="haul_yins1(this.value)">
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </span>
            </div>
            <div class="row-fluid" id="haul_yins12">
              <span class="span9">
                <button class="btn btn-small" onclick="add_insurance();">Add insurance</button>
              </span>
            </div>
         
        <table class="table"  id="haul_yins11">
          <thead>
            <tr>
              <th>Name</th>
              <th>Insurance Carrier</th>
              <th>Insurance Certificate Attached</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody id="add_new_table">
            <tr>
              <td>
                <input class="textinput" type="text" placeholder="" name="add_ins_name1[]">
              </td>
              <td>
                <input class="textinput" type="text" placeholder="" name="add_ins_carrier1[]">
              </td>
              <td>
                <select name="ins_attach_y1[]" class="span1" onchange="ins_attach_y(this.value)">
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </td>
              <td>
                          <script type="text/javascript">
			function ins_attach_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded4').show();
					   }
					   else
					   {
						   $('#first_uploaded4').hide();
					   }
	 		}
			</script>
			  <input type="button" id="first_uploaded4" value="Upload">
									

									<input type="hidden" name="add_ins_attach1[]" id="uploaded_file4" value="">
                                    
									<ul id="upload_file4" class="upload_file_name1"></ul>
              
              </td>
              <td><!-- <i class="icon icon-remove"></i> -->
			  <button class="btn btn-mini btn-1 remove_ins_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
              </td>
            </tr>
		
          </tbody>
		  
	
        </table>
		 
		<table class="table" >
		</table>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">21. Will owner/operators(sub-haulers) be scheduled on your policy? &nbsp;</label>
            <select name="ins_pol" class="span2" id="ins_pol" onchange="ins_pol1(this.value)">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="ins_pol12">
          <span class="span12">
            <button class="btn btn-small" onclick="add_owner();">Add owner/operator</button>
          </span>
        </div>
        <div class="row-fluid"  id="ins_pol11">
          <span class="span12">
            <table class="table" id="add_owner_table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Agreement Attached?</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_owner_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_owner_name1[]">
                  </td>
                  <td>
                    <select name="add_owner_y1[]" class="span6" onchange="add_owner_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
            <script type="text/javascript">
			function add_owner_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded5').show();
					   }
					   else
					   {
						   $('#first_uploaded5').hide();
					   }
	 		}
			</script>				  
				   <input type="button" id="first_uploaded5" value="Upload">
									

									<input type="hidden" name="add_owner_attach1[]" id="uploaded_file5" value="">
									<ul id="upload_file5" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td> <!--<i class="icon icon-remove"></i>-->
				 <button class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">22. Do you use subcontractors? &nbsp;</label>
            <select class="span2" name="subcon_y" onchange="subcon_y1(this.value)" id="subcon_y1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div id="subcon_y12">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">a. Who are your subcontractors? &nbsp;</label>
            <input class="textinput span9" type="text" placeholder="" name="subcon_name1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">b. Are subcontractors required to provide Certificates of Insurance? &nbsp;</label>
            <select class="span2" name="subcon_cert1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span   class="span12">
            <label class="control-label pull-left">c. What limit of Auto Liability are subcontractors required to carry? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_lia1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">d. What job duties are performed by the subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_duties1">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">e. What is your cost to use subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_cost1">
          </span>
        </div>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">23. At any time will your employees, subcontractors, or owner/operators lease vehicles on your behalf? &nbsp;</label>
            <select class="span2" name="subcon_emp_y1" onchange="subcon_emp_y1(this.value)" id="subcon_emp_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="subcon_emp_y12">
          <span class="span12">
            <button class="btn btn-small" onclick="add_subcontract();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="subcon_emp_y11">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Type of Vehicle</th>
                  <th>Lease agreement attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_subcontract_row">
                <tr>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_name1[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_vehicle1[]">
                  </td>
                  <td>
                    <select name="subcon_add_y1[]" class="span9" onchange="subcon_add_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                  <script type="text/javascript">
			function subcon_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded6').show();
					   }
					   else
					   {
						   $('#first_uploaded6').hide();
					   }
	 		}
			</script>
				   <input type="button" id="first_uploaded6" value="Upload">
									

									<input type="hidden" name="subcon_add_attach1[]" id="uploaded_file6" value="">
									<ul id="upload_file6" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td><!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_sub_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">24. Do any employees, subcontractors or sub-haulers use their vehicles while conducting your business? &nbsp;</label>
            <select class="span2" name="sub_haul_y1" onchange="sub_haul_y1(this.value)" id="sub_haul_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y11">
          <span class="span12">
            <button class="btn btn-small" onclick="add_conduct();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y12">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Insurance Carrier</th>
                  <th class="th-1">Limit of Liability Ins.</th>
                  <th class="th-1">Insurance Certificate Attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_conduct_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="sub_haul_add_name1[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="sub_haul_add_carrier1[]">
                  </td>
                  <td>
                    <input class="textinput span11 price_format" type="text" placeholder="$" name="sub_haul_add_limit1[]">
                  </td>
                  <td>
                    <select name="sub_haul_add_y1[]" class="span6" onchange="sub_haul_add_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                   <script type="text/javascript">
			//onchange="sub_haul_add_y(this.value)"
			function sub_haul_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded7').show();
					   }
					   else
					   {
						   $('#first_uploaded7').hide();
					   }
	 		}
			</script> 
                  <input type="button" id="first_uploaded7" value="Upload">
				  <input type="hidden" name="sub_haul_add_attach1[]" id="uploaded_file7" value="">
					<ul id="upload_file7" class="upload_file_name1"></ul>
				  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_con_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                   
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">25. Do you understand that we may audit your records? &nbsp;</label>
            <select class="span2" name="audit_y1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">26. Do any of your family members use the vehicles? &nbsp;</label>
            <select class="span2" name="vehicles_y1" id="vehicles_y" onchange="vehicles_y1(this.value)">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="vehicles_y11">
          <span class="span12">
            <button class="btn btn-small" onclick="add_members();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="vehicles_y12">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Relationship</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_members_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_name1[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_rel1[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_mem_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                    
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">27. Do you allow passengers to ride in your vehicles? &nbsp;</label>
            <select class="span2" name="ride_y1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">28. Are you familiar with the U.S Department of Transportation driver requirements? &nbsp;</label>
            <select class="span2" name="driver_y1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">29. (i) Do you mantain driver activity files? &nbsp;</label>
            <select class="span2" name="activity_y1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iI) Do you review current MVRs on all drivers prior to hiring? &nbsp;</label>
            <select class="span2" name="prior_y1">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iii) Is there a formal driver hiring procedure? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" name="procedure_y1">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" name="procedure_y1">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iv) Drug Screening? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" name="Drug_y1">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="NO" name="Drug_y1">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(v) If you have a formal driver hiring/training program, provide a copy with this application? &nbsp;</label>
                <input type="button" id="first_uploaded8" value="Upload">
									

									<input type="hidden" name="hiring_file1" id="uploaded_file8" value="">
									<ul id="upload_file8" class="upload_file_name1"></ul>
			
			<!--<input class="textinput" type="upload" placeholder="Upload file" name="hiring_file" value="Upload">-->
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">30. Are all drivers employees? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" onclick="driver_emp(this.value);" name="employees_y1">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" onclick="driver_emp(this.value);"  name="employees_y1">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" id="explain_no" style="display:none;">
          <span class="span12">
            <label class="control-label pull-left">If no, please explain &nbsp;</label>
            <input class="textinput span10" type="text" placeholder="" name="explain_no1">
          </span>
        </div>
		<span id="yes_employee" style="display:none;">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">If yes, list below &nbsp;</label>
            <button class="btn btn-small" onclick="add_employee();">Add</button>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Length of employment</th>
                  <th class="th-1">Years/Months</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_employee_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_name1[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_length1[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_year1[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_emp_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
				   </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
		</span>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">31. Is there a formal safety program? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" onclick="formal_safety(this.value);" id="safety_y" value="Yes" name="safety_y1">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" onclick="formal_safety1(this.value);" id="safety_y1" value="No" name="safety_y1">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" id="details_y" style="display:none;">
          <span class="span12">
            <label class="control-label pull-left">If yes, provide details or a copy:</label>
            	  <input type="button" id="first_uploaded16"  value="Upload">
									
								<input type="hidden" name="safety_attach_file" id="uploaded_file16" value="">
						<ul id="upload_file16" class="upload_file_name1"></ul>
            <textarea class="span12 span12-1" placeholder="" name="details_y" ></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">32. Provide details of your maintenance program:</label>
            <textarea class="span10 span12-1" placeholder="" name="program_detail1"></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">33. Do you agree to screen and report all potential operators immediately upon hiring before giving them a load? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" name="load_y1">
              <span>Yes &nbsp;</span>
            </label>

            <label class="radio pull-left">
              <input type="radio" value="No" name="load_y1">
              <span>No</span>
            </label>
          </span>
        </div>
		</div>
		  
		 	<?php } ?> 
		  
		 </div> 
		  <!--end Ghi-5> 
		  
		  
		  
       <!--   <div class="row-fluid " style="display:none;" id="badge_id_2">
		  <span class="badge">Liability, Cargo</span> -->
            <div class="span12" style="margin: 0px;">
              <label class="control-label left_pull">24.Does the applicant hire vehicle(s), owner operator(s) or vehicle(s) owned by other parties? &nbsp;</label>
              <select class="pull-left pull-left-1 width_option" id="broker_loads" onChange="broker_loads1(this.value);" name="owner_operator" required>                
              <?php $owner_operator = isset($app_own[0]->owner_operator) ? $app_own[0]->owner_operator : '';?>
              <option value="" >Select</option>
                <option value="No" <?php if($owner_operator  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($owner_operator  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
            </div>
        <!--  </div>-->
           <script>
																
																 $(document).ready(function(){
																	
																     broker_loads1($('#broker_loads').val());
																	
																 });
																 </script>	
		  
          
          		  <!-- GHI-5  -->
	
	 <div class="container-fluid" id="addload" style="display:none;">
      <div class="page-header">
        <h1>GHI-5 <small>Driver/Sub-Hauler supplement form</small>
        </h1>
      </div>
      <div class="well">
	  <input type="hidden" name="ghi5_parent_id" value="24"/>
	 <?php   if(!empty($app_ghi5a)) {  ?>
	  <input type="hidden" value="<?php echo isset($app_ghi5a[0]->haul_id) ? $app_ghi5a[0]->haul_id : ''; ?>" name="hauled_n_id" />
        <label class="control-label">Check all practices used by your company: (Give full explanation of each question. Use separate sheet, if necessary)</label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">1.&nbsp;</label>
            <label class="checkbox pull-left">
			  <?php   $hauler_MVR = isset($app_ghi5a[0]->hauler_MVR) ? $app_ghi5a[0]->hauler_MVR : '';?>
             
              <input type="checkbox" value="1" <?php if($hauler_MVR  == 1) echo 'checked="checked"'; ?>  name="hauler_MVR">
              <span>MVR check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Roa = isset($app_ghi5a[0]->hauler_Roa) ? $app_ghi5a[0]->hauler_Roa : '';?>
              <input type="checkbox" value="1" <?php if($hauler_Roa  == 1) echo 'checked="checked"'; ?> name="hauler_Roa">
              <span>Road Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Wri = isset($app_ghi5a[0]->hauler_Wri) ? $app_ghi5a[0]->hauler_Wri : '';?>
              <input type="checkbox" value="1" <?php if($hauler_Wri  == 1) echo 'checked="checked"'; ?> name="hauler_Wri">
              <span>Written application&nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Phy = isset($app_ghi51a[0]->hauler_Phy) ? $app_ghi5a[0]->hauler_Phy : '';?>
              <input type="checkbox" value="1"  <?php if($hauler_Phy  == 1) echo 'checked="checked"'; ?> name="hauler_Phy">
              <span>Physical exam &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Drug = isset($app_ghi5a[0]->hauler_Drug) ? $app_ghi5a[0]->hauler_Drug : '';?>
              <input type="checkbox" value="1" <?php if($hauler_Drug  == 1) echo 'checked="checked"'; ?> name="hauler_Drug">
              <span>Drug Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Ref = isset($app_ghi5a[0]->hauler_Ref) ? $app_ghi5a[0]->hauler_Ref : '';?>
              <input type="checkbox" value="1" <?php if($hauler_Ref  == 1) echo 'checked="checked"'; ?> name="hauler_Ref">
              <span>Reference Check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
			<?php   $hauler_Ver = isset($app_ghi5a[0]->hauler_Ver) ? $app_ghi5a[0]->hauler_Ver : '';?>
              <input type="checkbox" value="1" <?php if($hauler_Ver  == 1) echo 'checked="checked"'; ?> name="hauler_Ver">
              <span>Employment Verification &nbsp;</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span10">
            <label class="control-label pull-left">2. Describe acceptability for hiring drivers:&nbsp;</label>
            <textarea class="span12 span12-1" placeholder=""  name="hauler_hir_name"> <?php echo isset($app_ghi5a[0]->hauler_hir_name) ? trim($app_ghi5a[0]->hauler_hir_name) : '';?></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left ">3. Use Owner/Operators? &nbsp;</label>
            <select name="hauler_Own" id="hauler_Own" class="span2" onchange="hauler_own_func(this.value);" required>
            <?php  $hauler_Own = isset($app_ghi5a[0]->hauler_Own) ? $app_ghi5a[0]->hauler_Own : '';?>
            <option value="" >Select</option>
                <option value="No" <?php if($hauler_Own  == 'No') echo 'selected="selected"'; ?> >No</option>

                 <option value="Yes" <?php if($hauler_Own  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>			
			
             
            </select>
			      <script>
																
																 $(document).ready(function(){
																	
																     hauler_own_func($('#hauler_Own').val());
																	
																 });
																 </script>	
			

            <input class="textinput  h5-active ui-state-valid" style="display:none;" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="% of Revenues" value="<?php echo isset($app_ghi5a[0]->hauler_Rev) ? trim($app_ghi5a[0]->hauler_Rev) : '';?>" name="hauler_Rev" id="hauler_Rev">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">4. Use team drivers? &nbsp;</label>
            <select id="hauler_team" name="hauler_team" class="span2" onchange="hauler_team_func(this.value);">
			      <?php   $hauler_team = isset($app_ghi5a[0]->hauler_team) ? $app_ghi5a[0]->hauler_team : '';?>
                <option value="No" <?php if($hauler_team  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_team  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>			
			
             
            </select>
			      <script>
																
																 $(document).ready(function(){
																	
																     hauler_team_func($('#hauler_team').val());
																	
																 });
																 </script>	
            <input class="textinput" style="display:none;" type="text" placeholder="Number / Teams" name="hauler_Num" id="hauler_Num">
            <textarea name="hauler_Num_y"  style="display:none;" id="hauler_Num_y" class="span12" placeholder="If yes, please explain"></textarea>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">5. Are Motor Vehicle Reports of employed drivers pulled and reviewed? &nbsp;</label>
            <select name="hauler_Vehi" id="hauler_Vehi" class="span2" onchange="hauler_Moto_func(this.value);">
		    <?php   $hauler_Vehi = isset($app_ghi5a[0]->hauler_Vehi) ? $app_ghi5a[0]->hauler_Vehi : '';?>
                <option value="No" <?php if($hauler_Vehi  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_Vehi  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>			
			
             
            </select>
			      <script>
																
																 $(document).ready(function(){
																	
																     hauler_Moto_func($('#hauler_Vehi').val());
																	
																 });
																 </script>	
                                                                 <br/>
            <input class="textinput" style="display:none; float: left;" id="hauler_often" type="text" value="<?php echo isset($app_ghi5a[0]->hauler_often) ? $app_ghi5a[0]->hauler_often : '';?>" placeholder="If yes, how often" name="hauler_often">
      <!--      <input class="textinput" style="display:none;" id="hauler_attach" type="text" value="<?php echo isset($app_ghi5a[0]->hauler_attach) ? $app_ghi5a[0]->hauler_attach : '';?>" placeholder="Attach policies" name="hauler_attach">-->
       <span style="display:none;margin-top: 0px;float: left;" id="hauler_attach">
          <input type="button" id="first_uploaded15"   value="Upload">
									
								<input type="hidden"  name="hauler_attach" id="uploaded_file15" value="<?php echo $app_ghi5a[0]->hauler_attach; ?>">
                                         <?php $file=explode(",",$app_ghi5a[0]->hauler_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
						<ul id="upload_file15" class="upload_file_name1"></ul>
                        </span>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">6. Are all drivers covered under worker's Compensation? &nbsp;</label>
            <select class="span2" name="hauler_drivers" id="hauler_drivers" onchange="hauler_drivers1(this.value)">
            
		    <?php   $hauler_drivers = isset($app_ghi5a[0]->hauler_drivers) ? $app_ghi5a[0]->hauler_drivers : '';?>
                <option value="No" <?php if($hauler_drivers  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_drivers  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>	
            </select>
          </span>
        </div>
        <script>
		 $(document).ready(function(){
	    	hauler_drivers1($('#hauler_drivers').val());
		 });
		</script>
        <div class="row-fluid" id="insurance1" >
          <span class="span3">
            <label class="control-label">Name of insurance co.</label>
            <input class="textinput" type="text" value="<?php echo isset($app_ghi5a[0]->hauler_ins) ? $app_ghi5a[0]->hauler_ins : '';?>" placeholder="" name="hauler_ins">
          </span>
          <span class="span3">
            <label class="control-label">Policy no.</label>
            <input class="textinput" type="text" placeholder="" value="<?php echo isset($app_ghi5a[0]->hauler_Pol) ? $app_ghi5a[0]->hauler_Pol : '';?>" name="hauler_Pol">
          </span>
          <span class="span3">
            <label class="control-label">Effective date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" value="<?php echo isset($app_ghi5a[0]->hauler_Effect) ? $app_ghi5a[0]->hauler_Effect : '';?>" name="hauler_Effect" id="hauler_Effect">
          </span>
          <span class="span3">
            <label class="control-label">Expiry date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" value="<?php echo isset($app_ghi5a[0]->hauler_Exp) ? $app_ghi5a[0]->hauler_Exp : '';?>" name="hauler_Exp" id="hauler_Exp">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">7. Driver Turnover in the past year. &nbsp;</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label">Hired</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Hir" value="<?php echo isset($app_ghi5a[0]->hauler_Hir) ? $app_ghi5a[0]->hauler_Hir : '';?>" >
          </span>
          <span class="span3">
            <label class="control-label">Terminated</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Ter" value="<?php echo isset($app_ghi5a[0]->hauler_Ter) ? $app_ghi5a[0]->hauler_Ter : '';?>">
          </span>
          <span class="span3">
            <label class="control-label">Quit</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Quit" value="<?php echo isset($app_ghi5a[0]->hauler_Quit) ? $app_ghi5a[0]->hauler_Quit : '';?>">
          </span>
          <span class="span3">
            <label class="control-label">Others</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Oth" value="<?php echo isset($app_ghi5a[0]->hauler_Oth) ? $app_ghi5a[0]->hauler_Oth : '';?>">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span4">
            <label class="control-label">8. Max hours driven per day</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Max" value="<?php echo  isset($app_ghi5a[0]->hauler_Max) ? $app_ghi5a[0]->hauler_Max : '';?>">
          </span>
          <span class="span4">
            <label class="control-label">Per week</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Per" value="<?php echo isset($app_ghi5a[0]->hauler_Per) ? $app_ghi5a[0]->hauler_Per : '';?>">
          </span>
          <span class="span4">
            <label class="control-label">(5 day week or 7 day)</label>
            <input class="textinput" type="text" placeholder="" name="hauler_day" value="<?php echo  isset($app_ghi5a[0]->hauler_day) ? $app_ghi5a[0]->hauler_day : '';?>" pattern="[0-9]+"  >
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">9. How are drivers compensated? &nbsp;</label>
    <?php $radio_c = isset($app_ghi5a[0]->hauler_comp) ? $app_ghi5a[0]->hauler_comp : '';?>         
		  <label class="radio pull-left">
              <input type="radio" value="Hourly" <?php if($radio_c  == 'Hourly') echo 'checked="checked"'; ?> name="hauler_comp" >
              <span>Hourly &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Per trip" <?php if($radio_c  == 'Per trip') echo 'checked="checked"'; ?> name="hauler_comp">
              <span>Per trip &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Salary" <?php if($radio_c  == 'Salary') echo 'checked="checked"'; ?> name="hauler_comp">
              <span>Salary &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" <?php if($radio_c  == 'Others') echo 'checked="checked"'; ?> name="hauler_comp">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" value="<?php echo  isset($app_ghi5a[0]->hauler_Others) ? $app_ghi5a[0]->hauler_Others : '';?>" name="hauler_Others">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">10. What hours of the day do you drivers operate?</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">10.A.6 AM to 2 PM</label>
            <input class="textinput  h5-active ui-state-valid" id="hauler_hours" value="<?php echo  isset($app_ghi5a[0]->hauler_hours) ? $app_ghi5a[0]->hauler_hours : '';?>" pattern="([1-9]{1}|(10)|[0-9]{2,2})" type="text" placeholder="" name="hauler_hours">
          </span>
          <span class="span4">
            <label class="control-label">10.B.2 PM to 10 PM</label>
            <input class="textinput h5-active ui-state-valid" id="hauler_hour" value="<?php echo  isset($app_ghi5a[0]->hauler_hour) ? $app_ghi5a[0]->hauler_hour : '';?>" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="" name="hauler_hour">
          </span>
          <span class="span4">
            <label class="control-label">10.C.10 PM to 6 AM</label>
            <input class="textinput h5-active ui-state-valid" id="hauler_hou" value="<?php echo  isset($app_ghi5a[0]->hauler_hou) ? $app_ghi5a[0]->hauler_hou : '';?>" type="text"  pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="" name="hauler_hou">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">11. Where do your drivers sleep when they are on a trip? &nbsp;</label>
            <label class="radio pull-left radio-1">
			<?php $radio_sleep = isset($app_ghi5a[0]->hauler_sleep) ? $app_ghi5a[0]->hauler_sleep : '';?>
              <input type="radio" value="At Home" <?php if($radio_sleep  == 'At Home') echo 'checked="checked"'; ?>  name="hauler_sleep">
              <span>At Home &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Motel" <?php if($radio_sleep  == 'Motel') echo 'checked="checked"'; ?> name="hauler_sleep">
              <span>Motel &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="In the cab" <?php if($radio_sleep  == 'In the cab') echo 'checked="checked"'; ?> name="hauler_sleep">
              <span>In the cab &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" <?php if($radio_sleep  == 'Others') echo 'checked="checked"'; ?> name="hauler_sleep">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" <?php echo  isset($app_ghi5a[0]->hauler_hother) ? $app_ghi5a[0]->hauler_hother : '';?> name="hauler_hother">
            <label class="radio pull-left">
              <input type="radio" value="Hourly" <?php if($radio_sleep  == 'Hourly') echo 'checked="checked"'; ?> name="hauler_sleep">
              <span>Hourly &nbsp;</span>
            </label>
          </span>
        </div>
        <p>You must inform the company before hiring any new driver. You should have confirmation in writing regarding the acceptability of the driver by GHI.</p>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">12. Is your operation subject to time restraints when delivering the commodity? &nbsp;</label>
            <select name="hauler_rest" class="span2">
               <?php   $hauler_rest = isset($app_ghi5a[0]->hauler_rest) ? $app_ghi5a[0]->hauler_rest : '';?>
                <option value="No" <?php if($hauler_rest  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_rest  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>	
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">13. If not hauling for others, will the vehicles be parked at a jobsite most of the day? &nbsp;</label>
            <select class="span2" name="hauler_roth">
               <?php   $hauler_roth = isset($app_ghi5a[0]->hauler_roth) ? $app_ghi5a[0]->hauler_roth : '';?>
                <option value="No" <?php if($hauler_roth  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_roth  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>	
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">14. Do you have haul for others? &nbsp;</label>
            <select class="span2" name="hauler_haul" id="hauler_haul" onchange="hauler_haul1(this.value)">
               <?php   $hauler_haul = isset($app_ghi5a[0]->hauler_haul) ? $app_ghi5a[0]->hauler_haul : '';?>
                <option value="No" <?php if($hauler_haul  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($hauler_haul  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>	
            </select>
          </span>
        </div>
        <script>
			 $(document).ready(function(){
	    	hauler_haul1($('#hauler_haul').val());
		 });
		</script>
        <div class="row-fluid" id="hauler_haul12">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Is there any written agreement?</th>
                  <th>Copy attached?</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <input class="textinput" type="text" placeholder="" value="<?php echo  isset($app_ghi5a[0]->haul_name) ? $app_ghi5a[0]->haul_name : '';?>" name="haul_name">
                  </td>
                  <td>
                    <select  name="haul_agree" class="span6">
                    <?php   $haul_agree = isset($app_ghi5a[0]->haul_agree) ? $app_ghi5a[0]->haul_agree : '';?>
                <option value="No" <?php if($haul_agree  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_agree  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                    </select>
                  </td>
                  <td>
                    <select name="haul_attach" class="span9" onchange="uploaded_show(this.value)">
                        <?php   $haul_attach = isset($app_ghi5a[0]->haul_attach) ? $app_ghi5a[0]->haul_attach : '';?>
                <option value="No" <?php if($haul_attach  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_attach  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                    </select>
                   <script type="text/javascript">
				   function uploaded_show(val)
				   {
					   if(val=='Yes')
					   {
						  $('#first_uploaded1').show();
					   }
					   else
					   {
						   $('#first_uploaded1').hide();
					   }
				   }
				   </script>
                  </td>
				  <td>
				  <input type="button" id="first_uploaded1"  value="Upload">
									
								<input type="hidden" name="haul_attach_file" id="uploaded_file1" value="<?php echo $app_ghi5a[0]->haul_attach_file; ?>">
                                         <?php $file=explode(",",$app_ghi5a[0]->haul_attach_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
						<ul id="upload_file1" class="upload_file_name1"></ul>
				  
				  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">15. Gross receipts - Estimate this year:&nbsp;</label>
          </span>
          <span class="span4">
            <label class="control-label">Last year:</label>
            <input class="textinput price_format" type="text" placeholder="$" value="<?php echo  isset($app_ghi5a[0]->haul_Last) ? $app_ghi5a[0]->haul_Last : '';?>" name="haul_Last">
          </span>
          <span class="span4">
            <label class="control-label">Next year:</label>
            <input class="textinput price_format" type="text" placeholder="$" value="<?php echo  isset($app_ghi5a[0]->haul_Next) ? $app_ghi5a[0]->haul_Next : '';?>" name="haul_Next">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">16. (i) Are any vehicles or equipment loaned, rented or leased to others? &nbsp;</label>
            <select name="haul_equi">
              <?php   $haul_equi = isset($app_ghi5a[0]->haul_equi) ? $app_ghi5a[0]->haul_equi : '';?>
                <option value="No" <?php if($haul_equi  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_equi  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <label class="control-label pull-left">&nbsp; &nbsp; &nbsp;(ii) Do you lease, hire, rent or borrow any vehicles from others? &nbsp;</label>
        <label class="radio pull-left">
		<?php $haul_rent= isset($app_ghi5a[0]->haul_rent) ? $app_ghi5a[0]->haul_rent : '';?>
          <input type="radio" value="Yes" <?php if($haul_rent  == 'Yes') echo 'checked="checked"'; ?> name="haul_rent">
          <span>Yes &nbsp;</span>
        </label>
        <label class="radio pull-left">
          <input type="radio" value="No" <?php if($haul_rent  == 'No') echo 'checked="checked"'; ?> name="haul_rent">
          <span>No &nbsp;</span>
        </label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; What is the average length of the lease? &nbsp;</label>
            <input class="textinput" type="text" placeholder="" value="<?php echo  isset($app_ghi5a[0]->haul_lease) ? $app_ghi5a[0]->haul_lease : '';?>" name="haul_lease">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_agreement" onchange="agreement_yes(this.value)">
             <?php   $haul_agreement = isset($app_ghi5a[0]->haul_agreement) ? $app_ghi5a[0]->haul_agreement : '';?>
                <option value="No" <?php if($haul_agreement  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_agreement  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
            <script type="text/javascript">
			function agreement_yes(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded2').show();
					   }
					   else
					   {
						   $('#first_uploaded2').hide();
					   }
	 		}
			</script>
         <!--   <input class="textinput pull-left" type="text" placeholder="Upload a copy of the agreement" name="">-->
			
			
			      <input type="button" id="first_uploaded2" class="up_file" value="Upload">
									
								<input type="hidden" name="haul_agree_file" id="uploaded_file2" value="<?php echo $app_ghi5a[0]->haul_agree_file;?>">
                                <br />
                                                          <?php $file=explode(",",$app_ghi5a[0]->haul_agree_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
						<ul id="upload_file2" class="upload_file_name1"></ul>
			
			
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">17. What is the cost to lease, hire, rent or borrow vehicles? &nbsp;</label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span6">
            <label class="control-label">Per month</label>
            <input class="textinput price_format" type="text" value="<?php echo  isset($app_ghi5a[0]->haul_month) ? $app_ghi5a[0]->haul_month : '';?>" placeholder="$" name="haul_month">
          </span>
          <span class="span6">
            <label class="control-label">Per year</label>
            <input class="textinput price_format" type="text" value="<?php echo  isset($app_ghi5a[0]->haul_year) ? $app_ghi5a[0]->haul_year : '';?>" placeholder="$" name="haul_year">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">18. What type of vehicles do you lease, hire, rent or borrow? &nbsp;</label>
            <input class="textinput span6" type="text" value="<?php echo  isset($app_ghi5a[0]->haul_bor) ? $app_ghi5a[0]->haul_bor : '';?>" placeholder="" name="haul_bor">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">19. Do you use owner/operators (sub-hauler)? &nbsp;</label>
            <select name="haul_ope" class="span2" onchange="haul_ope1(this.value)" id="haul_ope">
             <?php   $haul_ope = isset($app_ghi5a[0]->haul_ope) ? $app_ghi5a[0]->haul_ope : '';?>
                <option value="No" <?php if($haul_ope  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_ope  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="haul_ope11">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp;If yes, is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_yagree" onchange="haul_yagree(this.value)">
    
             <?php   $haul_yagree = isset($app_ghi5a[0]->haul_yagree) ? $app_ghi5a[0]->haul_yagree : '';?>
                <option value="No" <?php if($haul_yagree  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_yagree  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
                        <script type="text/javascript">
			function haul_yagree(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded3').show();
					   }
					   else
					   {
						   $('#first_uploaded3').hide();
					   }
	 		}
			</script>
       <!--     <input class="textinput" type="text" placeholder="Upload If yes, provide the agreement" name="haul_yagree_file">-->
			        
			    <input type="button" id="first_uploaded3" class="up_file" value="Upload">
									
								<input type="hidden" name="haul_yagree_file" id="uploaded_file3" value="<?php $app_ghi5a[0]->haul_yagree_file; ?>">
                                       <br />
                                                          <?php $file=explode(",",$app_ghi5a[0]->haul_yagree_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
						<ul id="upload_file3" class="upload_file_name1"></ul>
						
			 </span>
        </div>
            <div class="row-fluid">
              <span class="span9">
                <label class="control-label pull-left">20. Owner operator/sub-hauler have their own insurance? &nbsp;</label>
                <select  class="span2" name="haul_yins" id="haul_yins" onchange="haul_yins1(this.value)">
                  <?php   $haul_yins = isset($app_ghi5a[0]->haul_yins) ? $app_ghi5a[0]->haul_yins : '';?>
                <option value="No" <?php if($haul_yins  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($haul_yins  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                </select>
              </span>
            </div>
            <div class="row-fluid" id="haul_yins12">
              <span class="span9">
                <button class="btn btn-small" onclick="add_insurance();">Add insurance</button>
              </span>
            </div>
         
        <table class="table" id="haul_yins11">
          <thead>
            <tr>
              <th>Name</th>
              <th>Insurance Carrier</th>
              <th>Insurance Certificate Attached</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
		  
		  <?php if(!empty($app_ghi5b)){ foreach($app_ghi5b as $values3){ ?>
		 <input type="hidden" value="<?php echo  isset($values3->add_ins_id) ? $values3->add_ins_id : '';?>" name="add_ins_id[]" />
		 <input type="hidden" value="<?php echo  isset($values3->haul_id) ? $values3->haul_id : '';?>" name="haul_ins_id[]" />
		      <tbody id="add_new_table">
            <tr>
              <td>
                <input class="textinput" type="text" value="<?php echo  isset($values3->add_ins_name) ? $values3->add_ins_name : '';?>" placeholder="" name="add_ins_name[]">
              </td>
              <td>
                <input class="textinput" type="text" value="<?php echo  isset($values3->add_ins_carrier) ? $values3->add_ins_carrier : '';?>" placeholder="" name="add_ins_carrier[]">
              </td>
              <td>
                <select name="ins_attach_y[]" class="span1" onchange="ins_attach_y(this.value)">
                <?php  $ins_attach_y = isset($values3->ins_attach_y) ? $values3->ins_attach_y : '';?>
                <option value="No" <?php if($ins_attach_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($ins_attach_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                </select>
                            <script type="text/javascript">
			function ins_attach_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded4').show();
					   }
					   else
					   {
						   $('#first_uploaded4').hide();
					   }
	 		}
			</script>
              </td>
              <td>
			  <input type="button" id="first_uploaded4" value="Upload">
									

									<input type="hidden" name="add_ins_attach[]" id="uploaded_file4" value="<?php echo $values3->add_ins_attach; ?>">
                                                             <br />
                                                          <?php $file=explode(",",$values3->add_ins_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
									<ul id="upload_file4" class="upload_file_name1"></ul>
              
              </td>
              <td><!-- <i class="icon icon-remove"></i> -->
			  <button class="btn btn-mini btn-1 remove_ins_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
              </td>
            </tr>
		
          </tbody>
		  <?php } } else {  ?>
		  
          <tbody id="add_new_table">
            <tr>
              <td>
                <input class="textinput" type="text"  placeholder="" name="add_ins_name[]">
              </td>
              <td>
                <input class="textinput" type="text" placeholder="" name="add_ins_carrier[]">
              </td>
              <td>
                <select name="ins_attach_y[]" class="span1" onchange="ins_attach_y(this.value)">
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </td>
              <td>
                          <script type="text/javascript">
			function ins_attach_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded4').show();
					   }
					   else
					   {
						   $('#first_uploaded4').hide();
					   }
	 		}
			</script>
			  <input type="button" id="first_uploaded4" value="Upload">
									

									<input type="hidden" name="add_ins_attach[]" id="uploaded_file4" value="<?php echo $values3->add_ins_attach ; ?>">
                                                                               <br />
                                                          <?php $file=explode(",",$values3->add_ins_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
									<ul id="upload_file4" class="upload_file_name1"></ul>
              
              </td>
              <td><!-- <i class="icon icon-remove"></i> -->
			  <button class="btn btn-mini btn-1 remove_ins_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
              </td>
            </tr>
		
          </tbody>
		  
	<?php }  ?>
        </table>
		 
		<table class="table" >
		</table>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">21. Will owner/operators(sub-haulers) be scheduled on your policy? &nbsp;</label>
            <select name="ins_pol" class="span2" id="ins_pol" onchange="ins_pol1(this.value)">
               <?php  $ins_pol = isset($app_ghi5a[0]->ins_pol) ? $app_ghi5a[0]->ins_pol : '';?>
                <option value="No" <?php if($ins_pol  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($ins_pol  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="ins_pol12">
          <span class="span12">
            <button class="btn btn-small" onclick="add_owner();">Add owner/operator</button>
          </span>
        </div>
        <div class="row-fluid" id="ins_pol11">
          <span class="span12">
            <table class="table" id="add_owner_table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Agreement Attached?</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
		<?php  if(!empty($app_ghi5c)){ foreach($app_ghi5c as $values4){ ?>	  
		 <input type="hidden" value="<?php echo  isset($values4->add_owner_id) ? $values4->add_owner_id : '';?>" name="add_owner_id[]" />
		 <input type="hidden" value="<?php echo  isset($values4->haul_id) ? $values4->haul_id : '';?>" name="haul_owner_id[]" />
		
              <tbody id="add_owner_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" value="<?php echo  isset($values4->add_owner_name) ? $values4->add_owner_name : '';?>" name="add_owner_name[]">
                  </td>
                  <td>
                    <select name="add_owner_y[]" class="span6" onchange="add_owner_y(this.value)">
                   <?php  $add_owner_y = isset($values4->add_owner_y) ? $values4->add_owner_y : '';?>
                   <option value="No" <?php if($add_owner_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($add_owner_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                    </select>
                  </td>
                  <td>
				              <script type="text/javascript">
			function add_owner_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded5').show();
					   }
					   else
					   {
						   $('#first_uploaded5').hide();
					   }
	 		}
			</script>
				   <input type="button" id="first_uploaded5" value="Upload">
						<input type="hidden" name="add_owner_attach[]" id="uploaded_file5" value="<?php echo $values4->add_owner_attach ;?>">
                                                                                      <br />
                                                          <?php $file=explode(",",$values4->add_owner_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
					<ul id="upload_file5" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td> <!--<i class="icon icon-remove"></i>-->
				 <button class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
			  <?php } } else {  ?>
                     <tbody id="add_owner_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_owner_name[]">
                  </td>
                  <td>
                    <select name="add_owner_y[]" class="span6" onchange="add_owner_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
				  	              <script type="text/javascript">
			function add_owner_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded5').show();
					   }
					   else
					   {
						   $('#first_uploaded5').hide();
					   }
	 		}
			</script>
				   <input type="button" id="first_uploaded5" value="Upload">
									

									<input type="hidden" name="add_owner_attach[]" id="uploaded_file5" value="<?php echo $values4->add_owner_attach; ?>">
                                                             <br />
                                                          <?php $file=explode(",",$values4->add_owner_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
									<ul id="upload_file5" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td> <!--<i class="icon icon-remove"></i>-->
				 <button class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
			  

             <?php } ?>			  
			  
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">22. Do you use subcontractors? &nbsp;</label>
            <select class="span2" name="subcon_y" onchange="subcon_y1(this.value)" id="subcon_y">
             <?php  $subcon_y = isset($app_ghi5a[0]->subcon_y) ? $app_ghi5a[0]->subcon_y : '';?>
                   <option value="No" <?php if($subcon_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($subcon_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
      <div class="row-fluid" id="subcon_y12">  
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">a. Who are your subcontractors? &nbsp;</label>
            <input class="textinput span9" value="<?php echo  isset($app_ghi5a[0]->subcon_name) ? $app_ghi5a[0]->subcon_name : ''; ?>"type="text" placeholder="" name="subcon_name">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">b. Are subcontractors required to provide Certificates of Insurance? &nbsp;</label>
            <select class="span2" name="subcon_cert">
               <?php  $subcon_cert = isset($app_ghi5a[0]->subcon_cert) ? $app_ghi5a[0]->subcon_cert : '';?>
                   <option value="No" <?php if($subcon_cert  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($subcon_cert  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span   class="span12">
            <label class="control-label pull-left">c. What limit of Auto Liability are subcontractors required to carry? &nbsp;</label>
            <input class="textinput span6" value="<?php echo  isset($app_ghi5a[0]->subcon_lia) ? $app_ghi5a[0]->subcon_lia : ''; ?>" type="text" placeholder="" name="subcon_lia">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">d. What job duties are performed by the subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" value="<?php echo  isset($app_ghi5a[0]->subcon_duties) ? $app_ghi5a[0]->subcon_duties : ''; ?>" placeholder="" name="subcon_duties">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">e. What is your cost to use subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" value="<?php echo  isset($app_ghi5a[0]->subcon_cost) ? $app_ghi5a[0]->subcon_cost : ''; ?>" placeholder="" name="subcon_cost">
          </span>
        </div>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">23. At any time will your employees, subcontractors, or owner/operators lease vehicles on your behalf? &nbsp;</label>
            <select class="span2" name="subcon_emp_y" onchange="subcon_emp_y1(this.value)" id="subcon_emp_y">
               
               <?php  $subcon_emp_y = isset($app_ghi5a[0]->subcon_emp_y) ? $app_ghi5a[0]->subcon_emp_y : '';?>
                   <option value="No" <?php if($subcon_emp_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($subcon_emp_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="subcon_emp_y12">
          <span class="span12">
            <button class="btn btn-small" onclick="add_subcontract();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="subcon_emp_y11">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Type of Vehicle</th>
                  <th>Lease agreement attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
	 <?php  if(!empty($app_ghi5d)){ foreach($app_ghi5d as $values5){ ?>	
	  <input type="hidden" value="<?php echo  isset($values5->subcon_add_id) ? $values5->subcon_add_id : '';?>" name="subcon_add_id[]" />
		 <input type="hidden" value="<?php echo  isset($values5->haul_id) ? $values5->haul_id : '';?>" name="haul_subcon_id[]" />
		
              <tbody id="add_subcontract_row">
                <tr>
                  <td>
                    <input class="textinput" type="text" placeholder="" value="<?php echo  isset($values5->subcon_add_name) ? $values5->subcon_add_name : ''; ?>" name="subcon_add_name[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" value="<?php echo  isset($values5->subcon_add_vehicle) ? $values5->subcon_add_vehicle : ''; ?>" name="subcon_add_vehicle[]">
                  </td>
                  <td>
                    <select name="subcon_add_y[]" class="span9" onchange="subcon_add_y(this.value)">
                    <?php  $subcon_add_y = isset($values5->subcon_add_y) ? $values5->subcon_add_y : '';?>
                   <option value="No" <?php if($subcon_add_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($subcon_add_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                    </select>
                  </td>
                  <td>
              <script type="text/javascript">
			function subcon_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded6').show();
					   }
					   else
					   {
						   $('#first_uploaded6').hide();
					   }
	 		}
			</script>
				   <input type="button" id="first_uploaded6" value="Upload">
									

									<input type="hidden" name="subcon_add_attach[]" id="uploaded_file6" value="<?php echo $values5->subcon_add_attach; ?>">
                                     <?php $file=explode(",",$values5->subcon_add_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
                                    
									<ul id="upload_file6" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td><!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_sub_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
			  <?php } } else { ?>
			  
			    <tbody id="add_subcontract_row">
                <tr>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_name[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_vehicle[]">
                  </td>
                  <td>
                    <select name="subcon_add_y[]" class="span9" onchange="subcon_add_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                  <script type="text/javascript">
			function subcon_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded6').show();
					   }
					   else
					   {
						   $('#first_uploaded6').hide();
					   }
	 		}
			</script>
				   <input type="button" id="first_uploaded6" value="Upload">
									

									<input type="hidden" name="subcon_add_attach[]" id="uploaded_file6" value="">
                 
									<ul id="upload_file6" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td><!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_sub_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
			  
			  
			  <?php } ?>
			  
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">24. Do any employees, subcontractors or sub-haulers use their vehicles while conducting your business? &nbsp;</label>
            <select class="span2" name="sub_haul_y" onchange="sub_haul_y1(this.value)"  id="sub_haul_y">
                  <?php  $sub_haul_y = isset($app_ghi5a[0]->sub_haul_y) ? $app_ghi5a[0]->sub_haul_y : '';?>
                   <option value="No" <?php if($sub_haul_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($sub_haul_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y11">
          <span class="span12">
            <button class="btn btn-small" onclick="add_conduct();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y12">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Insurance Carrier</th>
                  <th class="th-1">Limit of Liability Ins.</th>
                  <th class="th-1">Insurance Certificate Attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
	<?php  if(!empty($app_ghi5e)){ foreach($app_ghi5e as $values6){ ?>			  
		 <input type="hidden" value="<?php echo  isset($values6->sub_haul_id) ? $values6->sub_haul_id : '';?>" name="sub_haul_id[]" />
		 <input type="hidden" value="<?php echo  isset($values6->haul_id) ? $values6->haul_id : '';?>" name="haul_sub_id[]" />
		 
	  
              <tbody id="add_conduct_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" value="<?php echo  isset($values6->sub_haul_add_name) ? $values6->sub_haul_add_name : ''; ?>" placeholder="" name="sub_haul_add_name[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" value="<?php echo  isset($values6->sub_haul_add_carrier) ? $values6->sub_haul_add_carrier : ''; ?>" name="sub_haul_add_carrier[]">
                  </td>
                  <td>
                    <input class="textinput span11 price_format" type="text" value="<?php echo  isset($values6->sub_haul_add_limit) ? $values6->sub_haul_add_limit : ''; ?>" placeholder="$" name="sub_haul_add_limit[]">
                  </td>
                  <td>
                    <select name="sub_haul_add_y[]" class="span6" onchange="sub_haul_add_y(this.value)">
                     <?php  $sub_haul_add_y = isset($values6->sub_haul_add_y) ? $values6->sub_haul_add_y : '';?>
                   <option value="No" <?php if($sub_haul_add_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($sub_haul_add_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
                    </select>
                  </td>
                  <td>
            <script type="text/javascript">
			//onchange="sub_haul_add_y(this.value)"
			function sub_haul_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded7').show();
					   }
					   else
					   {
						   $('#first_uploaded7').hide();
					   }
	 		}
			</script>                   
                  <input type="button" id="first_uploaded7" value="Upload">
				  <input type="hidden" name="sub_haul_add_attach[]" id="uploaded_file7" value="<?php echo $values6->sub_haul_add_attach;?>">
                  <br/>
                                                         <?php $file=explode(",",$values6->sub_haul_add_attach); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
					<ul id="upload_file7" class="upload_file_name1"></ul>
				  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_con_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                   
                  </td>
                </tr>
              </tbody>
			  <?php } } else { ?>
			  
			   <tbody id="add_conduct_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="sub_haul_add_name[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="sub_haul_add_carrier[]">
                  </td>
                  <td>
                    <input class="textinput span11 price_format" type="text" placeholder="$" name="sub_haul_add_limit[]">
                  </td>
                  <td>
                    <select name="sub_haul_add_y[]" class="span6" onchange="sub_haul_add_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                               <script type="text/javascript">
			//onchange="sub_haul_add_y(this.value)"
			function sub_haul_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded7').show();
					   }
					   else
					   {
						   $('#first_uploaded7').hide();
					   }
	 		}
			</script>
                  <input type="button" id="first_uploaded7" value="Upload">
				  <input type="hidden" name="sub_haul_add_attach[]" id="uploaded_file7" value="">
					<ul id="upload_file7" class="upload_file_name1"></ul>
				  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_con_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                   
                  </td>
                </tr>
              </tbody>
			  
			  <?php }    ?>
			  
			  
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">25. Do you understand that we may audit your records? &nbsp;</label>
            <select class="span2" name="audit_y">
                <?php  $audit_y = isset($app_ghi5a[0]->audit_y) ? $app_ghi5a[0]->audit_y : '';?>
                   <option value="No" <?php if($audit_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($audit_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">26. Do any of your family members use the vehicles? &nbsp;</label>
            <select class="span2" name="vehicles_y" id="vehicles_y" onchange="vehicles_y1(this.value)">
                 <?php  $vehicles_y = isset($app_ghi5a[0]->vehicles_y) ? $app_ghi5a[0]->vehicles_y : '';?>
                   <option value="No" <?php if($vehicles_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($vehicles_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="vehicles_y11">
          <span class="span12">
            <button class="btn btn-small" onclick="add_members();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="vehicles_y12">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Relationship</th>
                  <th></th>
                </tr>
              </thead>
	<?php  if(!empty($app_ghi5f)){ foreach($app_ghi5f as $values7){ ?>			  
		 <input type="hidden" value="<?php echo  isset($values7->add_mem_id) ? $values7->add_mem_id : '';?>" name="add_mem_id[]" />
		 <input type="hidden" value="<?php echo  isset($values7->haul_id) ? $values7->haul_id : '';?>" name="haul_mem_id[]" />
		     <tbody id="add_members_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" value="<?php echo  isset($values7->add_mem_name) ? $values7->add_mem_name : ''; ?>" placeholder="" name="add_mem_name[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" value="<?php echo  isset($values7->add_mem_rel) ? $values7->add_mem_rel : ''; ?>" placeholder="" name="add_mem_rel[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_mem_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                    
                  </td>
                </tr>
              </tbody>
		
            
			  <?php } } else { ?>
			  
			    <tbody id="add_members_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_name[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_rel[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_mem_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                    
                  </td>
                </tr>
              </tbody>
			
			  
			  <?php } ?>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">27. Do you allow passengers to ride in your vehicles? &nbsp;</label>
            <select class="span2" name="ride_y">
             <?php  $ride_y = isset($app_ghi5a[0]->ride_y) ? $app_ghi5a[0]->ride_y : '';?>
                   <option value="No" <?php if($ride_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($ride_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">28. Are you familiar with the U.S Department of Transportation driver requirements? &nbsp;</label>
            <select class="span2" name="driver_y">
              <?php  $driver_y = isset($app_ghi5a[0]->driver_y) ? $app_ghi5a[0]->driver_y : '';?>
                   <option value="No" <?php if($driver_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($driver_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">29. (i) Do you mantain driver activity files? &nbsp;</label>
            <select class="span2" name="activity_y">
            <?php  $activity_y = isset($app_ghi5a[0]->activity_y) ? $app_ghi5a[0]->activity_y : '';?>
                   <option value="No" <?php if($activity_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($activity_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iI) Do you review current MVRs on all drivers prior to hiring? &nbsp;</label>
            <select class="span2" name="prior_y">
             <?php  $prior_y = isset($app_ghi5a[0]->prior_y) ? $app_ghi5a[0]->prior_y : '';?>
                   <option value="No" <?php if($prior_y  == 'No') echo 'selected="selected"'; ?> >No</option>
                   <option value="Yes" <?php if($prior_y  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iii) Is there a formal driver hiring procedure? &nbsp;</label>
            <label class="radio pull-left">
			<?php $procedure_y= isset($app_ghi5a[0]->procedure_y) ? $app_ghi5a[0]->procedure_y : '';?>
       
              <input type="radio" value="Yes" <?php if($procedure_y  == 'Yes') echo 'checked="checked"'; ?> name="procedure_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" <?php if($procedure_y  == 'No') echo 'checked="checked"'; ?> name="procedure_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iv) Drug Screening? &nbsp;</label>
			<?php $Drug_y= isset($app_ghi5a[0]->Drug_y) ? $app_ghi5a[0]->Drug_y : '';?>
            <label class="radio pull-left">
              <input type="radio" value="Yes" <?php if($Drug_y  == 'Yes') echo 'checked="checked"'; ?> name="Drug_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="NO" <?php if($Drug_y  == 'No') echo 'checked="checked"'; ?> name="Drug_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(v) If you have a formal driver hiring/training program, provide a copy with this application? &nbsp;</label>
                <input type="button" id="first_uploaded8" value="Upload">
									

									<input type="hidden" name="hiring_file" id="uploaded_file8" value="<?php echo $app_ghi5a[0]->hiring_file; ?>">
                                      <br/>
                                                         <?php $file=explode(",",$app_ghi5a[0]->hiring_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>
									<ul id="upload_file8" class="upload_file_name1"></ul>
			
			<!--<input class="textinput" type="upload" placeholder="Upload file" name="hiring_file" value="Upload">-->
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">30. Are all drivers employees? &nbsp;</label>
			<?php $employees_y= isset($app_ghi5a[0]->employees_y) ? $app_ghi5a[0]->employees_y : '';?>
            <label class="radio pull-left">
              <input type="radio" value="Yes" <?php if($employees_y  == 'Yes') echo 'checked="checked"'; ?> onclick="driver_emp(this.value);" name="employees_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" <?php if($employees_y  == 'Yes') echo 'checked="checked"'; ?> onclick="driver_emp(this.value);"  name="employees_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" id="explain_no" >
          <span class="span12">
            <label class="control-label pull-left">If no, please explain &nbsp;</label>
            <input class="textinput span10" type="text" placeholder="" value="<?php echo isset($app_ghi5a[0]->explain_no) ? $app_ghi5a[0]->explain_no : ''; ?>" name="explain_no">
          </span>
        </div>
		<span id="yes_employee" style="display:none;">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">If yes, list below &nbsp;</label>
            <button class="btn btn-small" onclick="add_employee();">Add</button>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Length of employment</th>
                  <th class="th-1">Years/Months</th>
                  <th></th>
                </tr>
              </thead>
<?php  if(!empty($app_ghi5g)){ foreach($app_ghi5g as $values8){ ?>		
 <input type="hidden" value="<?php echo  isset($values8->add_list_id) ? $values8->add_list_id : '';?>" name="add_list_id[]" />
		 <input type="hidden" value="<?php echo  isset($values8->haul_id) ? $values8->haul_id : '';?>" name="haul_list_id[]" />
			  
              <tbody id="add_employee_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" value="<?php echo  isset($values8->add_list_name) ? $values8->add_list_name : '';?>"  name="add_list_name[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" value="<?php echo  isset($values8->add_list_length) ? $values8->add_list_length : '';?>"  name="add_list_length[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" value="<?php echo  isset($values8->add_list_year) ? $values8->add_list_year : '';?>"  name="add_list_year[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_emp_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
				   </td>
                </tr>
              </tbody>
			  
			  <?php } } else { ?>
			  
			       <tbody id="add_employee_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_name[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_length[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_year[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_emp_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
				   </td>
                </tr>
              </tbody>
             
			  
			  
			  <?php } ?>
            </table>
          </span>
        </div>
		</span>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">31. Is there a formal safety program? &nbsp;</label>
            <label class="radio pull-left">
				<?php $safety_y= isset($app_ghi5a[0]->safety_y) ? $app_ghi5a[0]->safety_y : '';?>
           
            
              <input type="radio" onclick="formal_safety(this.value);" id="safety_y" value="Yes" <?php if($safety_y  == 'Yes') echo 'checked="checked"'; ?> name="safety_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" onclick="formal_safety1(this.value);" id="safety_y1" <?php if($safety_y  == 'No') echo 'checked="checked"'; ?> value="No" name="safety_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" id="details_y" style="display:none;">
          <span class="span12">
            <label class="control-label pull-left">If yes, provide details or a copy:</label>
             
             	<input type="button" id="first_uploaded16" value="Upload"><br/>
				<input type="hidden" name="safety_attach_file" id="uploaded_file16" value="<?php echo $app_ghi5a[0]->safety_attach_file; ?>">   
              <?php $file=explode(",",$app_ghi5a[0]->safety_attach_file); ?>
             <?php for($i=0;$i<count($file);$i++) { ?>
              <a href="<?php echo base_url();?>uploads/docs/<?php echo  $file[$i];  ?>" target="_blank"><?php echo  $file[$i];  ?></a><br/>
              <?php } ?>						
                          <ul id="upload_file16" class="upload_file_name1"></ul> 
            <textarea class="span12 span12-1" placeholder="" value="<?php echo  isset($app_ghi5a[0]->details_y) ? $app_ghi5a[0]->details_y : '';?>" name="details_y" ></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">32. Provide details of your maintenance program:</label>
            <textarea class="span10 span12-1" placeholder=""  name="program_detail"><?php echo  isset($app_ghi5a[0]->program_detail) ? $app_ghi5a[0]->program_detail : '';?></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">33. Do you agree to screen and report all potential operators immediately upon hiring before giving them a load? &nbsp;</label>
            <label class="radio pull-left">
			<?php $load_y= isset($app_ghi5a[0]->load_y) ? $app_ghi5a[0]->load_y : '';?>
              <input type="radio" value="Yes" <?php if($load_y  == 'Yes') echo 'checked="checked"'; ?> name="load_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" <?php if($load_y  == 'Yes') echo 'checked="checked"'; ?> name="load_y">
              <span>No</span>
            </label>
          </span>
        </div>
		 </div>
		<?php }  else {  ?>
		
		<label class="control-label">Check all practices used by your company: (Give full explanation of each question. Use separate sheet, if necessary)</label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">1.&nbsp;</label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_MVR">
              <span>MVR check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Roa">
              <span>Road Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Wri">
              <span>Written application&nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Phy">
              <span>Physical exam &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Drug">
              <span>Drug Test &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Ref">
              <span>Reference Check &nbsp;</span>
            </label>
            <label class="checkbox pull-left">
              <input type="checkbox" value="1" name="hauler_Ver">
              <span>Employment Verification &nbsp;</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span10">
            <label class="control-label pull-left">2. Describe acceptability for hiring drivers:&nbsp;</label>
            <textarea class="span12 span12-1" placeholder="" name="hauler_hir_name"></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left ">3. Use Owner/Operators? &nbsp;</label>
            <select name="hauler_Own" class="span2" onchange="hauler_own_func(this.value);">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
             
            </select>

            <input class="textinput  h5-active ui-state-valid" style="display:none;" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="% of Revenues" name="hauler_Rev" id="hauler_Rev">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">4. Use team drivers? &nbsp;</label>
            <select name="hauler_team" class="span2" onchange="hauler_team_func(this.value);">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
             
            </select>
            <input class="textinput" style="display:none;" type="text" placeholder="Number / Teams" name="hauler_Num" id="hauler_Num">
            <textarea name="hauler_Num_y"  style="display:none;" id="hauler_Num_y" class="span12" placeholder="If yes, please explain"></textarea>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">5. Are Motor Vehicle Reports of employed drivers pulled and reviewed? &nbsp;</label>
            <select name="hauler_Vehi" class="span2" onchange="hauler_Moto_func(this.value);">
			  <option value="No">No</option>
              <option value="Yes">Yes</option>
              
            </select>
            <br/>
            <input class="textinput" style="display:none;float: left;" id="hauler_often" type="text" placeholder="If yes, how often" name="hauler_often">
            <span style="display:none;margin-top: 0px;float: left;" id="hauler_attach">
            <input type="button" id="first_uploaded15"  value="Upload">
									
								<input type="hidden"  name="hauler_attach" id="uploaded_file15" value="">
                                       
						<ul id="upload_file15" class="upload_file_name1"></ul>
                        </span>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">6. Are all drivers covered under worker's Compensation? &nbsp;</label>
            <select class="span2" name="hauler_drivers">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label">Name of insurance co.</label>
            <input class="textinput" type="text" placeholder="" name="hauler_ins">
          </span>
          <span class="span3">
            <label class="control-label">Policy no.</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Pol">
          </span>
          <span class="span3">
            <label class="control-label">Effective date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" name="hauler_Effect" id="hauler_Effect">
          </span>
          <span class="span3">
            <label class="control-label">Expiry date</label>
            <input class="textinput " type="text" placeholder="mm/dd/yyyy" name="hauler_Exp" id="hauler_Exp">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">7. Driver Turnover in the past year. &nbsp;</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span3">
            <label class="control-label">Hired</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Hir" >
          </span>
          <span class="span3">
            <label class="control-label">Terminated</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Ter">
          </span>
          <span class="span3">
            <label class="control-label">Quit</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Quit">
          </span>
          <span class="span3">
            <label class="control-label">Others</label>
            <input class="textinput datepick" type="text" placeholder="mm/dd/yyyy" name="hauler_Oth">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span4">
            <label class="control-label">8. Max hours driven per day</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Max">
          </span>
          <span class="span4">
            <label class="control-label">Per week</label>
            <input class="textinput" type="text" placeholder="" name="hauler_Per">
          </span>
          <span class="span4">
            <label class="control-label">(5 day week or 7 day)</label>
            <input class="textinput" type="text" placeholder="" name="hauler_day" pattern="[0-9]+"  >
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">9. How are drivers compensated? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Hourly" name="hauler_comp">
              <span>Hourly &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Per trip" name="hauler_comp">
              <span>Per trip &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Salary" name="hauler_comp">
              <span>Salary &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" name="hauler_comp">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" name="hauler_Others">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">10. What hours of the day do you drivers operate?</label>
            <label class="control-label pull-left"></label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">10.A. &nbsp;6 AM to 2 PM</label>
            <input class="textinput  h5-active ui-state-valid" pattern="([1-9]{1}|(10)|[0-9]{2,2})" type="text" placeholder="%" name="hauler_hours">
          </span>
          <span class="span4">
            <label class="control-label">10.B. &nbsp;2 PM to 10 PM</label>
            <input class="textinput h5-active ui-state-valid" type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="%" name="hauler_hour">
          </span>
          <span class="span4">
            <label class="control-label">10.C. &nbsp;10 PM to 6 AM</label>
            <input class="textinput h5-active ui-state-valid" type="text"  pattern="([1-9]{1}|(10)|[0-9]{2,2})" placeholder="%" name="hauler_hou">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">11. Where do your drivers sleep when they are on a trip? &nbsp;</label>
            <label class="radio pull-left radio-1">
              <input type="radio" value="At Home" name="hauler_sleep">
              <span>At Home &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Motel" name="hauler_sleep">
              <span>Motel &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="In the cab" name="hauler_sleep">
              <span>In the cab &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="Others" name="hauler_sleep">
              <span>Others &nbsp;</span>
            </label>
            <input class="textinput" type="text" placeholder="" name="hauler_hother">
            <label class="radio pull-left">
              <input type="radio" value="Hourly" name="hauler_sleep">
              <span>Hourly &nbsp;</span>
            </label>
          </span>
        </div>
        <p>You must inform the company before hiring any new driver. You should have confirmation in writing regarding the acceptability of the driver by GHI.</p>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">12. Is your operation subject to time restraints when delivering the commodity? &nbsp;</label>
            <select name="hauler_rest" class="span2">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">13. If not hauling for others, will the vehicles be parked at a jobsite most of the day? &nbsp;</label>
            <select class="span2" name="hauler_roth">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">14. Do you have haul for others? &nbsp;</label>
            <select class="span2" name="hauler_haul">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Is there any written agreement?</th>
                  <th>Copy attached?</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="haul_name">
                  </td>
                  <td>
                    <select  name="haul_agree" class="span6">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                    <select name="haul_attach" class="span9" onchange="uploaded_show(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                   <script type="text/javascript">
				   function uploaded_show(val)
				   {
					   if(val=='Yes')
					   {
						  $('#first_uploaded1').show();
					   }
					   else
					   {
						   $('#first_uploaded1').hide();
					   }
				   }
				   </script>
                  </td>
				  <td>
				  <input type="button" id="first_uploaded1"  value="Upload">
									
								<input type="hidden" name="haul_attach_file" id="uploaded_file1" value="">
						<ul id="upload_file1" class="upload_file_name1"></ul>
				  
				  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span4">
            <label class="control-label">15. Gross receipts - Estimate this year:&nbsp;</label>
          </span>
          <span class="span4">
            <label class="control-label">Last year:</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_Last">
          </span>
          <span class="span4">
            <label class="control-label">Next year:</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_Next">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">16. (i) Are any vehicles or equipment loaned, rented or leased to others? &nbsp;</label>
            <select name="haul_equi">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <label class="control-label pull-left">&nbsp; &nbsp; &nbsp;(ii) Do you lease, hire, rent or borrow any vehicles from others? &nbsp;</label>
        <label class="radio pull-left">
          <input type="radio" value="Yes" name="haul_rent">
          <span>Yes &nbsp;</span>
        </label>
        <label class="radio pull-left">
          <input type="radio" value="No" name="haul_rent">
          <span>No &nbsp;</span>
        </label>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; What is the average length of the lease? &nbsp;</label>
            <input class="textinput" type="text" placeholder="" name="haul_lease">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_agreement" onchange="agreement_yes(this.value)">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
            <script type="text/javascript">
			function agreement_yes(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded2').show();
					   }
					   else
					   {
						   $('#first_uploaded2').hide();
					   }
	 		}
			</script>
         <!--   <input class="textinput pull-left" type="text" placeholder="Upload a copy of the agreement" name="">-->
			
			
			      <input type="button" id="first_uploaded2" class="up_file" value="Upload">
									
								<input type="hidden" name="haul_agree_file" id="uploaded_file2" value="">
						<ul id="upload_file2" class="upload_file_name1"></ul>
			
			
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span9">
            <label class="control-label pull-left">17. What is the cost to lease, hire, rent or borrow vehicles? &nbsp;</label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span6">
            <label class="control-label">Per month</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_month">
          </span>
          <span class="span6">
            <label class="control-label">Per year</label>
            <input class="textinput price_format" type="text" placeholder="$" name="haul_year">
          </span>
        </div>
        <div class="row-fluid mt_15">
          <span class="span12">
            <label class="control-label pull-left">18. What type of vehicles do you lease, hire, rent or borrow? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="haul_bor">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">19. Do you use owner/operators (sub-hauler)? &nbsp;</label>
            <select name="haul_ope" class="span2" onchange="haul_ope1(this.value)" id="haul_ope">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="haul_ope11">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp; &nbsp; &nbsp;If yes, is there a written agreement? &nbsp;</label>
            <select class="span2" name="haul_yagree" onchange="haul_yagree(this.value)">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
       <!--     <input class="textinput" type="text" placeholder="Upload If yes, provide the agreement" name="haul_yagree_file">-->
			      <script type="text/javascript">
			function haul_yagree(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded3').show();
					   }
					   else
					   {
						   $('#first_uploaded3').hide();
					   }
	 		}
			</script>  
			    <input type="button" id="first_uploaded3" class="up_file" value="Upload">
									
								<input type="hidden" name="haul_yagree_file" id="uploaded_file3" value="">
						<ul id="upload_file3" class="upload_file_name1"></ul>
						
			 </span>
        </div>
            <div class="row-fluid">
              <span class="span9">
                <label class="control-label pull-left">20. Owner operator/sub-hauler have their own insurance? &nbsp;</label>
                <select  class="span2" name="haul_yins" id="haul_yins" onchange="haul_yins1(this.value)">
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </span>
            </div>
            <div class="row-fluid" id="haul_yins12">
              <span class="span9">
                <button class="btn btn-small" onclick="add_insurance();">Add insurance</button>
              </span>
            </div>
         
        <table class="table"  id="haul_yins11">
          <thead>
            <tr>
              <th>Name</th>
              <th>Insurance Carrier</th>
              <th>Insurance Certificate Attached</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody id="add_new_table">
            <tr>
              <td>
                <input class="textinput" type="text" placeholder="" name="add_ins_name[]">
              </td>
              <td>
                <input class="textinput" type="text" placeholder="" name="add_ins_carrier[]">
              </td>
              <td>
                <select name="ins_attach_y[]" class="span1" onchange="ins_attach_y(this.value)">
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select>
              </td>
              <td>
                          <script type="text/javascript">
			function ins_attach_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded4').show();
					   }
					   else
					   {
						   $('#first_uploaded4').hide();
					   }
	 		}
			</script>
			  <input type="button" id="first_uploaded4" value="Upload">
									

									<input type="hidden" name="add_ins_attach[]" id="uploaded_file4" value="">
                                    
									<ul id="upload_file4" class="upload_file_name1"></ul>
              
              </td>
              <td><!-- <i class="icon icon-remove"></i> -->
			  <button class="btn btn-mini btn-1 remove_ins_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
              </td>
            </tr>
		
          </tbody>
		  
	
        </table>
		 
		<table class="table" >
		</table>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">21. Will owner/operators(sub-haulers) be scheduled on your policy? &nbsp;</label>
            <select name="ins_pol" class="span2" id="ins_pol" onchange="ins_pol1(this.value)">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="ins_pol12">
          <span class="span12">
            <button class="btn btn-small" onclick="add_owner();">Add owner/operator</button>
          </span>
        </div>
        <div class="row-fluid"  id="ins_pol11">
          <span class="span12">
            <table class="table" id="add_owner_table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Agreement Attached?</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_owner_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_owner_name[]">
                  </td>
                  <td>
                    <select name="add_owner_y[]" class="span6" onchange="add_owner_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
            <script type="text/javascript">
			function add_owner_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded5').show();
					   }
					   else
					   {
						   $('#first_uploaded5').hide();
					   }
	 		}
			</script>				  
				   <input type="button" id="first_uploaded5" value="Upload">
									

									<input type="hidden" name="add_owner_attach[]" id="uploaded_file5" value="">
									<ul id="upload_file5" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td> <!--<i class="icon icon-remove"></i>-->
				 <button class="btn btn-mini btn-1 remove_own_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">22. Do you use subcontractors? &nbsp;</label>
            <select class="span2" name="subcon_y" onchange="subcon_y1(this.value)" id="subcon_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div id="subcon_y12">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">a. Who are your subcontractors? &nbsp;</label>
            <input class="textinput span9" type="text" placeholder="" name="subcon_name">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">b. Are subcontractors required to provide Certificates of Insurance? &nbsp;</label>
            <select class="span2" name="subcon_cert">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span   class="span12">
            <label class="control-label pull-left">c. What limit of Auto Liability are subcontractors required to carry? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_lia">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">d. What job duties are performed by the subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_duties">
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">e. What is your cost to use subcontractors? &nbsp;</label>
            <input class="textinput span6" type="text" placeholder="" name="subcon_cost">
          </span>
        </div>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">23. At any time will your employees, subcontractors, or owner/operators lease vehicles on your behalf? &nbsp;</label>
            <select class="span2" name="subcon_emp_y" onchange="subcon_emp_y1(this.value)" id="subcon_emp_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="subcon_emp_y12">
          <span class="span12">
            <button class="btn btn-small" onclick="add_subcontract();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="subcon_emp_y11">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Type of Vehicle</th>
                  <th>Lease agreement attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_subcontract_row">
                <tr>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_name[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="subcon_add_vehicle[]">
                  </td>
                  <td>
                    <select name="subcon_add_y[]" class="span9" onchange="subcon_add_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                  <script type="text/javascript">
			function subcon_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded6').show();
					   }
					   else
					   {
						   $('#first_uploaded6').hide();
					   }
	 		}
			</script>
				   <input type="button" id="first_uploaded6" value="Upload">
									

									<input type="hidden" name="subcon_add_attach[]" id="uploaded_file6" value="">
									<ul id="upload_file6" class="upload_file_name1"></ul>
				  
                   
                  </td>
                  <td><!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_sub_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">24. Do any employees, subcontractors or sub-haulers use their vehicles while conducting your business? &nbsp;</label>
            <select class="span2" name="sub_haul_y" onchange="sub_haul_y1(this.value)" id="sub_haul_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y11">
          <span class="span12">
            <button class="btn btn-small" onclick="add_conduct();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="sub_haul_y12">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Insurance Carrier</th>
                  <th class="th-1">Limit of Liability Ins.</th>
                  <th class="th-1">Insurance Certificate Attached</th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_conduct_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="sub_haul_add_name[]">
                  </td>
                  <td>
                    <input class="textinput" type="text" placeholder="" name="sub_haul_add_carrier[]">
                  </td>
                  <td>
                    <input class="textinput span11 price_format" type="text" placeholder="$" name="sub_haul_add_limit[]">
                  </td>
                  <td>
                    <select name="sub_haul_add_y[]" class="span6" onchange="sub_haul_add_y(this.value)">
                      <option value="Yes">Yes</option>
                      <option value="No">No</option>
                    </select>
                  </td>
                  <td>
                   <script type="text/javascript">
			//onchange="sub_haul_add_y(this.value)"
			function sub_haul_add_y(val){
			if(val=='Yes')
					   {
						  $('#first_uploaded7').show();
					   }
					   else
					   {
						   $('#first_uploaded7').hide();
					   }
	 		}
			</script> 
                  <input type="button" id="first_uploaded7" value="Upload">
				  <input type="hidden" name="sub_haul_add_attach[]" id="uploaded_file7" value="">
					<ul id="upload_file7" class="upload_file_name1"></ul>
				  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_con_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                   
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">25. Do you understand that we may audit your records? &nbsp;</label>
            <select class="span2" name="audit_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">26. Do any of your family members use the vehicles? &nbsp;</label>
            <select class="span2" name="vehicles_y" id="vehicles_y" onchange="vehicles_y1(this.value)">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid" id="vehicles_y11">
          <span class="span12">
            <button class="btn btn-small" onclick="add_members();">Add</button>
          </span>
        </div>
        <div class="row-fluid" id="vehicles_y12">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Relationship</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_members_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_name[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_mem_rel[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_mem_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
                    
                  </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">27. Do you allow passengers to ride in your vehicles? &nbsp;</label>
            <select class="span2" name="ride_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">28. Are you familiar with the U.S Department of Transportation driver requirements? &nbsp;</label>
            <select class="span2" name="driver_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">29. (i) Do you mantain driver activity files? &nbsp;</label>
            <select class="span2" name="activity_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iI) Do you review current MVRs on all drivers prior to hiring? &nbsp;</label>
            <select class="span2" name="prior_y">
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iii) Is there a formal driver hiring procedure? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" name="procedure_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" name="procedure_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(iv) Drug Screening? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" name="Drug_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="NO" name="Drug_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">&nbsp; &nbsp;(v) If you have a formal driver hiring/training program, provide a copy with this application? &nbsp;</label>
                <input type="button" id="first_uploaded8" value="Upload">
									

									<input type="hidden" name="hiring_file" id="uploaded_file8" value="">
									<ul id="upload_file8" class="upload_file_name1"></ul>
			
			<!--<input class="textinput" type="upload" placeholder="Upload file" name="hiring_file" value="Upload">-->
          </span>
        </div>
        <div class="row-fluid">
          <span class="span9">
            <label class="control-label pull-left">30. Are all drivers employees? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" onclick="driver_emp(this.value);" name="employees_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" value="No" onclick="driver_emp(this.value);"  name="employees_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" id="explain_no" style="display:none;">
          <span class="span12">
            <label class="control-label pull-left">If no, please explain &nbsp;</label>
            <input class="textinput span10" type="text" placeholder="" name="explain_no">
          </span>
        </div>
		<span id="yes_employee" style="display:none;">
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">If yes, list below &nbsp;</label>
            <button class="btn btn-small" onclick="add_employee();">Add</button>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <table class="table">
              <thead>
                <tr>
                  <th class="th-1">Name</th>
                  <th class="th-1">Length of employment</th>
                  <th class="th-1">Years/Months</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="add_employee_row">
                <tr>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_name[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_length[]">
                  </td>
                  <td>
                    <input class="textinput span11" type="text" placeholder="" name="add_list_year[]">
                  </td>
                  <td> <!-- <i class="icon icon-remove"></i> -->
				   <button class="btn btn-mini btn-1 remove_emp_row" id="remove_phone_row"><i class="icon icon-remove"></i></button>
				   </td>
                </tr>
              </tbody>
            </table>
          </span>
        </div>
		</span>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">31. Is there a formal safety program? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" onclick="formal_safety(this.value);" id="safety_y" value="Yes" name="safety_y">
              <span>Yes &nbsp;</span>
            </label>
            <label class="radio pull-left">
              <input type="radio" onclick="formal_safety1(this.value);" id="safety_y1" value="No" name="safety_y">
              <span>No</span>
            </label>
          </span>
        </div>
        <div class="row-fluid" id="details_y" style="display:none;">
          <span class="span12">
            <label class="control-label pull-left">If yes, provide details or a copy:</label>
            	  <input type="button" id="first_uploaded16"  value="Upload">
									
								<input type="hidden" name="safety_attach_file" id="uploaded_file16" value="">
						<ul id="upload_file16" class="upload_file_name1"></ul>
            <textarea class="span12 span12-1" placeholder="" name="details_y" ></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">32. Provide details of your maintenance program:</label>
            <textarea class="span10 span12-1" placeholder="" name="program_detail"></textarea>
          </span>
        </div>
        <div class="row-fluid">
          <span class="span12">
            <label class="control-label pull-left">33. Do you agree to screen and report all potential operators immediately upon hiring before giving them a load? &nbsp;</label>
            <label class="radio pull-left">
              <input type="radio" value="Yes" name="load_y">
              <span>Yes &nbsp;</span>
            </label>

            <label class="radio pull-left">
              <input type="radio" value="No" name="load_y">
              <span>No</span>
            </label>
          </span>
        </div>
		</div>
       
		<?php } ?>
		
        </div>
		     <div class="row-fluid ">
            <span class="span12">
              <label class="control-label left_pull">25.Does the applicant broker loads out to others? &nbsp;</label>
              <select class="pull-left pull-left-1 width_option" name="broker_loads"  style="float:left;" required>
                
                <?php $broker_loads = isset($app_own[0]->broker_loads) ? $app_own[0]->broker_loads : '';?>
                <option value="" >Select</option>
                <option value="No" <?php if($broker_loads  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($broker_loads  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              
              </select>
                   
              
		 <!--	  <a href="#addloads"   class="fancybox edit"><label class="pull-left edit_button" style="display:none" id="addload">Edit</label></a>
			 
			 <a href="<?php echo base_url();?>application_form/applicant_registered/<?php echo isset($quote_id) ?$quote_id :'';?>"  data-fancybox-type="iframe" style="" class="fancybox edit"> <label class="pull-left edit_button">Edit&nbsp;&nbsp;&nbsp;&nbsp;</label></a>-->
            </span>
          </div>

     
	<!-- <div id="absolute-wrapper">
      <div class="div-1">
        <div class="form-actions">
          <button class="btn btn-primary" onclick="fancybox_close();">Save</button>
          <button class="btn" onclick="reload_close3();">Cancel</button>
        </div>
      </div>
    </div>-->
    <div class="row-fluid ">
            <span class="span12">
              <label class="control-label left_pull">26.Does the applicant participate in a formal safety inspection? &nbsp;&nbsp;</label>
              <select class="pull-left pull-left-1 width_option" name="inspection__name" id="inspection_id" onchange="inspection_func();" required>
                <option value="" >Select</option>
                 <?php $inspection__name = isset($app_own[0]->inspection__name) ? $app_own[0]->inspection__name : '';?>
                <option value="No" <?php if($inspection__name  == 'No') echo 'selected="selected"'; ?> >No</option>
                 <option value="Yes" <?php if($inspection__name  == 'Yes') echo 'selected="selected"'; ?>>Yes</option>
              </select>
			  &nbsp;
              <input class="textinput" type="text" id="if_yes" value="<?php echo $app_own[0]->if_yes_name; ?>" style="display:none;" placeholder="Please Explain" name="if_yes_name">
            </span>
          </div>
		     <div class="row-fluid">
            <span class="span4">
              <label class="control-label">27.Estimated financial worth</label>
              <input class="textinput price_format" type="text" name="Estimated_financial" value="<?php echo $app_own[0]->Estimated_financial; ?>" placeholder="$"  maxlength="15">
            </span>
            <span class="span4">
              <label class="control-label">Gross receipts last year</label>
              <input class="textinput price_format" type="text" name="Gross_receipts" value="<?php echo $app_own[0]->Gross_receipts; ?>" placeholder="$"  maxlength="15">
            </span>
            <span class="span4">
              <label class="control-label">Estimated next year</label>
              <input class="textinput price_format" type="text" name="Estimated_next" value="<?php echo $app_own[0]->Estimated_next; ?>" placeholder="$"  maxlength="15">
            </span>
          </div>
         
		 <!--  <input type="hidden" name="uploaded_files_values" id="uploaded_files_values" value="">

                               div class="row-fluid " >  	
                                <div class="span12 lightblue">
                                <label>How Many Vehicles to Schedule ?</label>
                                <input type="text" onchange="add_vehicle(this.value)" class="span1" />
                                </div>
                                </div-->

                               <!-- <span id="add_vehicle_div"></span>


                                <span id="add_tractor_div"></span>


                          

                             Vehicle Schedule Section End -->	


                            <!-- Trailer Schedule Section Start -->	




                        <!--    <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div>

                            <div class="row-fluid">  
                                <div class="span6 lightblue">
                                    <label >Owner Driven</label>

                                    <select id="owner_driven_tr" name="owner_driven_tr" >
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                </div>
                                <div class="span6 lightblue"></div>
                            </div-->

                          <!--  <div class="row-fluid">  
                                <div class="span12 lightblue">
                                    <label style="float:left">Attach MVR's for all Driver(s) and Owner(s) no more than 30 days old</label>
									
									
									
		input type="button" value="Upload" id="mvr_upload" class="btn btn-primary" style="cursor:pointer;" -->
		<!--<label class="cabinet label label-info" >
			<div id="upload_bottom"><input type="hidden" id="upload_count" value="1" ></div>
			<input  style="cursor:pointer" onChange="add_upload_3(this.id);" type="file" name="file_sec_3[]" id="fileup_1" class="upload_browse_multi file" title="Select file to upload" />
		</label>
									<input type="button" id="first_upload_second" value="Upload">
									

									<input type="hidden" name="uploaded_files_values_second" id="uploaded_files_values_second" value="">
									<ul id="upload_file_name_second"></ul>        
									
									<!--span class="label label-info" style="width:42px; margin-bottom:5px; cursor:pointer;" > Upload
									<input  style="cursor:pointer" onchange="add_upload_3(this.id);" type="file" name="file_sec_3[]" id="fileup_1" class="upload_browse_multi file" title="Select file to upload" />
																	
									<div id="upload_bottom"><input type="hidden" id="upload_count" value="1" ></div>
									</span>
									
									<div id="upload_file_name"></div>
									
									
								    <!--span class="btn btn-file">Upload<input type="file" multiple="multiple" name="file_sec_3[]" id="file_sec_3" /></span-->
                                    <!--MAYANK
									<label><b>Disclaimer :</b> Please attach the MVR for each driver, otherwise the quote is an indication only and not a formal quote. </label>
									<!--END MAYANK
                                </div>
                            </div> -->
                          


                          
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                       <!--    <fieldset id="section4" class="number_of_owner_box">
                                <legend>Number of Owner</legend>

                                <div class="row-fluid ">  	
                                    <div class="span12 lightblue">
                                        MAYANK
                                        <label>Number of owner of the vehicle or entity?</label>
										<!--END MAYANK
                                        <input required="" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" type="text" id="number_of_owner" name="number_of_owner" onchange="add_owner(this.value)" class="span1" value="">
                                    </div>
                                </div>

  

                                    <div id="row_owner" style="display:none;">
                                        <div class="row-fluid">  
                                            <div class="span12 lightblue">
                                                <table id="owner_table" class="table table-bordered table-striped table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Middle Name</th>
                                                            <th>Last Name</th>
                                                            <th> Driver ?</th>
                                                            <th>Driver License number</th>
                                                            <th>License issue state</th>

                                                        </tr>

                                                    </thead>
                                                    <tbody id="add_owner_row">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> 

                                <!--span id="add_driver_div"></span

                            </fieldset>



                       
                       
                       
                       
                       
                       
                       
                            <fieldset id="section5" class="fillings_section_box">
                                <legend>Filing Section</legend>
						<!--MAYANK-->
  
                                    <div class="row-fluid">  
                                        
										<script type="text/javascript">
											$(document).ready(function(){
												$("#filings_need").change(function(){
													if($(this).val()=="yes") {
														$("#number_of_filings_div").show();
														$('#number_of_filings').attr('required', 'required'); 
														
													}else{
														$("#number_of_filings_div").hide();
														$('#number_of_filings').removeAttr('required'); 
														}
												});
											});
											
														function add_filing(val){
													if(val >=0 ){
												
														 $("#filing_table").show();
														var valAA='<?php echo $filing_type_show;?>';
														//alert(valAA);
														
														var currcount = $("#add_filing_row tr").length;
														var i =currcount+1;
														//alert(i);
														var diff = currcount - val;
													if(diff < 0) {
														while(diff < 0){
														var row = '<tr>\
														<td>'+valAA+'<br><input style="display: none;" id="filing_type_otherXYZ123add" type="text" name="filing_type_other[]" class="span12 filing_numbers"/></td>\
														<td><input type="text" name="filings_num[]" class="span12 filing_numbers" required="required"="required" /></td>\
														</tr>';		row =row.replace('ABC123',i);
																	row =row.replace('XYZ123',i);
																	var v = $('#add_filing_row').append(row) ; 
																	diff++;
																	i++;
																} 
															 }
														 else if(diff > 0)
															 {
																 while(diff > 0){
																	 var v = $('#add_filing_row tr:last').remove() ; 
																	 diff--;
																 }
															 }	
														apply_on_all_elements();
													}
												}
												
													function filling_other1(val1,id){
														//alert(val1+id);
												if(val1=='Other'){
													$('#'+id+'add').show();
													
												}
												else{
													$('#'+id+'add').hide();
												}
											}
												
										</script>
									<!--	<div class="span12 lightblue">
											<label>Do you need Filing / Filings :</label>
											<select id="filings_need" name="filings_need" required="required">
												<option value="">--Select--</option>
												<option value="yes">YES</option>
												<option value="no">NO</option>
											</select>
                                        </div>
										
										<div class="span12 lightblue" id="number_of_filings_div" style="display:none;">
											<label>Number of Filings :</label>
											<input type="text" pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" id="number_of_filings" name="number_of_filings" class="span1 filing_numbers" onchange="add_filing(this.value)">
										</div>
										
										<div id="row_filing" style="display:none;">
											<div class="row-fluid">  
												<div class="span6 lightblue">
													<table id="filing_table" class="table table-bordered table-striped table-hover">
														<thead>
															<tr>
																<th>Filing Type:</th>
																<th>Filing Num.</th>
															</tr>
														</thead>
														<tbody id="add_filing_row">
														</tbody>
													</table>
												</div>
											</div>
										</div> 
									</div>							
		                                <!--END OF MAYANK-->


                            </fieldset>

	
	

	
	
	
	
	<div id="upload_form_pop" style="display:none;">
	<input type="hidden" value="action='<?php echo base_url()?>upload.php'" id="url_action"/>
	</div>
	
   
	
							
				

     <div class="row-fluid" style="margin-top: 7px;">  
                                <div class="span lightblue">
                                    <div class="form-actions">
                                    <input type="submit" name="save" id="save" class="btn btn-primary" value="Submit">
                                        <!--button class="btn btn-primary" type="submit" onclick="return check_vehicle_entry();">Submit Quote</button--
                                       <button class="btn btn-primary" type="submit" onclick="" style="float:right; margin-left:3px;padding:5px 5px;">Next Step</button>
                                       <button class="btn btn-primary" type="submit" name="saved1" id="" onclick="" style="float:right; margin-left: 5px; padding:5px 5px;">Save</button> 
									 <button  type="submit" name="back1" class="btn btn-primary" type="" style="float:right;padding:5px 5px;" onclick="" >Back</button>
								<!--		<a href="<?php echo base_url();?>application_form/losses" class="btn btn-primary" type="" style="float:right;padding:5px 5px;" onclick="">Back</a>-->
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                       
						<!--</div>
</form>-->
<div style="display:none;" id="reg_state">
  <span class="span1">		  
            <label class="control-label span10">21.AE.State</label>
        	<?php
									$attr = "class='span7 ui-state-valid' style='width: 60px;' ";
									$broker_state =   isset($values9->reg_state) ? $values9->reg_state : '';
									
									?>
									<?php echo get_state_dropdown('reg_state[]', $broker_state, $attr); ?>
            
            
          </span>   
          </div>   



 <!--<script src="<?php echo base_url('js');?>/jquery.knob.js"></script>

		<!-- jQuery File Upload Dependencies 
		<script src="<?php echo base_url('js');?>/jquery.ui.widget.js"></script>
		<script src="<?php echo base_url('js');?>/jquery.iframe-transport.js"></script>
		<script src="<?php echo base_url('js');?>/jquery.fileupload.js"></script>
		
		<!-- Our main JS file 
		<script src="<?php echo base_url('js');?>/scripts_file.js"></script>	 --> 

       
<?php 
/*if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}*/
?>
<script>
	 $(document).ready(function() {
		 if($('#owner_id13').val()=='No'){
			
			  $('#addown').show();
			 
			 }else{
				
				 $('#addown').hide();
				 }
				 
				  if($('#owner_id12').val()=='No'){
			
			  $('#addown1').show();
			 
			 }else{
				
				 $('#addown1').hide();
				 }
				 
				 
			 	if($('#applicant_scheduled').val()=="Yes"){
	             $('#addschedul').show();
	         // $('#explain_no').hide();
	
            	}
	  var _startDate = new Date(); //todays date
		var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		 var _startDate1 = new Date(); //todays date
		var _endDate1 = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
		$('#date_from_effective').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to_expiration').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to_expiration').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate1 = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from_effective').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		
		
      $('#date_from_effective1').datepicker({

		  format: 'mm/dd/yyyy',
		  autoclose: true,
		  //startDate: _startDate,
		  todayHighlight: true
		}).on('changeDate', function(e){ //alert('here');
			_endDate1 = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
			$('#date_to_expiration1').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
			});

		$('#date_to_expiration1').datepicker({
		  format: 'mm/dd/yyyy',
		  autoclose: true,
		 // startDate: _endDate,
		  todayHighlight: false
		}).on('changeDate', function(e){ 
			_endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
			//alert(_endDate);
			$('#date_from_effective1').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
		});
		});
		$('#date_from_effective1').mask('99/99/9999');
		$('#date_to_expiration1').mask('99/99/9999');
		
		
	/*	$("#click_add_phone").click(function(){
          // $("#add_phone").show();
		   $( "#add_phone" ).slideToggle( "slow", function() {
    // Animation complete.
	if ($.trim($('input.textinput.span1').val()) != '')
{
      $('input.textinput.span1.phone').val('');
	  }
	  if ($.trim($('#select_home_p').val()) != '')
{
	   $('#select_home_p').val('Home');
	   }
	   if ($.trim($('#textinput_att_p').val()) != '')
{
	  $('#textinput_att_p').val('');
}
  });
          });
		  	$("#click_add_email").click(function(){
             $( "#add_email1" ).slideToggle( "slow", function() {
    // Animation complete.
	if ($.trim($('input.textinput.email').val()) != '')
{
      $('input.textinput.email').val('');
	  }
	  	if ($.trim($('#select_home_e').val()) != '')
{
	   $('#select_home_e').val('Home');
	   }	if ($.trim($('#textinput_att_e').val()) != '')
{
	  $('#textinput_att_e').val('');
}
  });
          });*/
		  

		  
		  
        </script>
        <script>
		/*$(document).ready(function(){
		//when the Add Filed button is clicked
	$("#add_phone_row").click(function (e) {
			//alert();
			//Append a new row of code to the "#items" div
			$("#table_phone").append('<tr><td><input class="textinput span1" type="text" placeholder="Area" id="insured_telephone1" name="insured_telephone_add[]" maxlength="3"  pattern="[0-9]+"  value="" required="required"="required">&nbsp;<input class="textinput span1" id="insured_telephone2" type="text" placeholder="Prefix" name="insured_telephone_add[]" maxlength="4"  pattern="[0-9]+"  value="" required="required"="">&nbsp;<input class="textinput span1" id="insured_telephone3" type="text" placeholder="Sub" name="insured_telephone_add[]" maxlength="3"  pattern="[0-9]+"  value="" required="required"="">&nbsp;<input class="textinput span1" type="text" placeholder="Ext" name="insured_tel_ext_add[]"></td><td><select name="insured_telephone_type[]" class="select-1"><option value="Home">Home</option><option value="Business">Business</option></select></td><td><input class="textinput" type="text" placeholder="" name="insured_telephone_attention[]"></td><td> <button class="btn btn-mini btn-1 remove_phone_row" id="remove_phone_row"><i class="icon icon-remove"></i></button></td></tr>');
		});

	$("body").on("click", ".remove_phone_row", function (e) {
		//alert($(this).parent("div"));
		$(this).closest('tr').remove();
	});
	});*/
	
	$(document).ready(function(){
		//when the Add Filed button is clicked
				if($("#broker_loads").val() ==="Yes"){
	    $('#addload').show();
	   // $('#explain_no').hide();
	
	}
		/*if($('#trucking_comp_id').val()=='Other'){
			 $('#trucking_specify').show();
			}*/
		if($("#certificate_id_val").val() ==="Yes"){
										 //alert(1);
										 $("#certificate_app_id").show();
										 }
		  if($("#insurance_val_id").val() ==="Yes"){
										 //alert(1);
										 $("#Policy_id").show();
										 }
										 
										/* if($("#owner_id").val() ==="No"){
										 
										 $("#insurance_vehicle").show();
										 $("#addown").show();
										 }*/
	/*	$("#add_email_row").click(function (e) {
			//alert();
			//Append a new row of code to the "#items" div
			$("#table_email").append('<tr><td><input class="textinput" type="text" placeholder="" id="email" name="insured_email_add[]" pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" maxlength="30"></td><td><select name="insured_email_type[]" class="select-1"><option value="Home">Home</option><option value="Business">Business</option></select></td><td><input class="textinput" type="text" placeholder="" name="insured_email_attention[]"></td><td> <button class="btn btn-mini btn-1 remove_phone_row" id="remove_phone_row"><i class="icon icon-remove"></i></button></td></tr>');
		});

	$("body").on("click", ".remove_email_row", function (e) {
		//alert($(this).parent("div"));
		$(this).closest('tr').remove();
	});*/
	
	
	/*var arr=$('#app_id').val();
	//alert(arr);
				myarray = arr.split(',');
				if($.inArray( "Liability", myarray )> -1){
				$("#filings_id").show();
				}
	if(($.inArray( "Liability", myarray )> -1) || ($.inArray( "Cargo", myarray )> -1) ){
				//$("#losses_lia").show();
				$("#badge_id_2").show();
				$("#badge_id_4").show();
				
				
				// alert(1);
				}else{
				//$("#losses_lia").hide();
			//	alert(2);
				$("#badge_id_2").hide();
				$("#badge_id_4").hide();
				}
				if($.inArray("Cargo", myarray )> -1){
				//$(".cargo_show").show();
				$("#badge_id_3").show();
				// alert(1);
				}else{
				//$(".cargo_show").hide();
			//	alert(2);
				$("#badge_id_3").hide();
				}*/
	});
		function audit() {
    var val=$("#audit_name").val();
	
	if(val === "Other"){
			$("#Specify").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify").hide();
		}
		}
	function audit1() {
    var val=$("#audit_name1").val();
	
	if(val === "Other"){
		
			$("#Specify_other").show();
			//Append a new row of code to the "#items" div
		}else{
		$("#Specify_other").hide();
		}
		}
		
		

	
// alert(foo);
	/*	var foo = []; 
$('#app_id :selected').map(function(i, selected){ 
  foo.push($(this).val()); 
 
});
var val_app1=foo;
		$("#id_remarks_text").val(val_app1);
	
	var val_app=$("#app_id").val();
		//alert(val_app);
		var val_remark=$("#id_remarks_text").val();
		// var myarray=[];
        // myarray.push(val_app);
		 // alert($(this).attr("selected",true));
		if(val_remark === ""){
		$("#id_remarks_text").val(val_app);		
		}else
		{
		  myarray = val_remark.split(',');
		 //alert(myarray[0]);
	
		//alert(myarray.indexOf( val_app ));
		
		if(myarray.indexOf( val_app )>=0){
			//alert(2);				
		 }else{
		// alert(3);
	//var val_app1=val_remark+","+val_app;
		//$("#id_remarks_text").val(val_app1);
		 }
		}*/
		
	
		function aaplic_other(i){
		var val=$("#appli_id"+i+"").val();
		if(val === "Other"){
		
			$("#aapli_name_other"+i).show();
			//Append a new row of code to the "#items" div
		}else{
		$("#aapli_name_other"+i).hide();
		}
		}
	
</script>
<script>
										 function trans_val(){
										 var n=$("#transported_id").val();
										 if(n==="No"){
										 $("#text_no").show();										 
										 }else{
										 $("#text_no").hide();	
										 
										 }
										 }
										 </script>
										 <script>
										 function Company_trucking1(){
								//alert();
									   //alert(vara);
									   var vara= $("#trucking_comp_id").val();
											//alert(vara);
											///alert();
											if(vara!=null){
												//alert(vara);
									for(var i=0;i<vara.length;i++){
										//alert(i)
										 if(vara[i] =="Other"){
											// alert( vara[i]);
											// alert();
										 $("#trucking_specify").show();
										 }else{
										 $("#trucking_specify").hide();
										 }
											}
											}
										 }
										 function certificate_app_val(){
										// alert($("#certificate_id_val").val());
										 if($("#certificate_id_val").val() ==="Yes"){
										 //alert(1);
										 $("#certificate_app_id").show();
										 }else{
										 $("#certificate_app_id").hide();
										// alert(2);
										 }
										 }
										 function owner_vehicle13(){
											 //alert();
										 if($("#owner_id13").val() ==="No"){
										 
										 $("#insurance_vehicle").show();
										 $("#addown").show();
										 }else{
										 $("#insurance_vehicle").hide();
										 $("#addown").hide();
										// alert(2);
										 }
										 }
										 
										 	 function owner_vehicle12(){
										 if($("#owner_id12").val() ==="No"){
										 
										// $("#insurance_vehicle").show();
										 $("#addown1").show();
										 }else{
										 //$("#insurance_vehicle").hide();
										 $("#addown1").hide();
										// alert(2);
										 }
										 }
										 
										 
										 function insurance_val_func(){
										  if($("#insurance_val_id").val() ==="Yes"){
										 //alert(1);
										 $("#Policy_id").show();
										 }else{
										 $("#Policy_id").hide();
										// alert(2);
										 }
										 
										 }
										 	

function load_trucks(){

	 var n=$("#loaded_trucks_id").val();
										 if(n==="Yes"){
										 $("#loaded_id").show();										 
										 }else{
										 $("#loaded_id").hide();	
										 
										 }

}						
          function primary_coverage(){
		  var n=$("#primary_coverage_id").val();
										 if(n==="Yes"){
										 $("#coverage_id").show();										 
										 }else{
										 $("#coverage_id").hide();	
										 
										 }
		  
		  }
					function inspection_func(){
					if($("#inspection_id").val()=== "Yes"){
					  $("#if_yes").show();
					}else{
					  $("#if_yes").hide();
					}
					
					}
					function app_register(id){
					//alert();
					 $.fancybox({
	  type: 'iframe',
	  href: '<?php echo base_url('index.php/application_form/applicant_conducted/');?>'+id,
	  autoSize: false,
	  closeBtn: true,
	  width: '400',
	  height: '200',
	  closeClick: false,
	  enableEscapeButton: true,
	  beforeLoad: function () {},
	}); 
					
					}
					
										 </script>
										 <script type="text/javascript">
  
		if($('#appli_id').val()=='Other'){
			$('#other_tr').show();
			}
			if($('#transported_id').val()=='No'){
			$('#text_no').show();
			}
	if($('#owner_reg_id').val()=="No"){
	    $('#addregister').show();
	   // $('#explain_no').hide();
	
	}
		function fancybox_close(){
		parent.jQuery.fancybox.close();
		//location.reload(true);
		}
		function reload_close(){
	
		 var l=$('#addowner input').length;
		
		 for(i=1;i<l;i++){
		 $('#addowner input').val('');
		  $('#addowner textarea').val('');
		
		  $('#add_addition').hide();
		 }
	
		parent.jQuery.fancybox.close();

		
		}
				function reload_close1(){
	
		 var l=$('#addregistered input').length;
		
		 for(i=1;i<l;i++){
		 $('#addregistered input').val('');
		  $('#addregistered textarea').val('');
		
		  $('#add_persons').hide();
		 }
	
		parent.jQuery.fancybox.close();

		
		}
				function reload_close2(){
	
		 var l=$('#add_vehicles_row input').length;
		
		 for(i=1;i<l;i++){
		 $('#add_vehicles_row input').val('');
		  $('#add_vehicles_row textarea').val('');
		  $('#add_vehicles input').val('');
		  $('#add_vehicles textarea').val('');
		   $('#add_vehicles').hide();
		 }
	
		parent.jQuery.fancybox.close();

		
		}
				function reload_close3(){
	
		 var l=$('#addloads input').length;
		
		 for(i=1;i<l;i++){
		 $('#addloads input').val('');
		  $('#addloads textarea').val('');
		 }
	
		parent.jQuery.fancybox.close();

		
		}
function hauler_own_func(val){
	if(val=="Yes"){
	$('#hauler_Rev').show();
	}else{
	$('#hauler_Rev').hide();	
	}
	
	}
	
	function hauler_own_func(val){
	if(val=="Yes"){
	$('#hauler_Rev').show();
	}else{
	$('#hauler_Rev').hide();	
	}
	
	}
	
	function hauler_team_func(val){
	if(val=="Yes"){
	$('#hauler_Num_y').show();
	$('#hauler_Num').show();
	
	}else{
	$('#hauler_Num_y').hide();	
	$('#hauler_Num').hide();
	}
	
	}
	
	function hauler_Moto_func(val){
	if(val=="Yes"){
	$('#hauler_often').show();
	$('#hauler_attach').show();
	
	}else{
	$('#hauler_often').hide();	
	$('#hauler_attach').hide();
	}
	
	}
	$('.datepick').datepicker({
			    format: 'mm/dd/yyyy',
    autoclose: true,
    // endDate: _startDate,
    todayHighlight: true
		});
		$('.datepick').mask('99/99/9999');
		
 
    var _startDate = new Date(); //todays date
  var _endDate = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
   var _startDate1 = new Date(); //todays date
  var _endDate1 = new Date(_startDate.getTime() + (24 * 60 * 60 * 1000)); //plus 1 day
  $('#hauler_Effect').datepicker({

       format: 'mm/dd/yyyy',
    autoclose: true,
     endDate: _startDate,
    todayHighlight: true
  }).on('changeDate', function(e){ //alert('here');
   _endDate = new Date(e.date.getTime() + (24 * 60 * 60 * 1000)); //get new end date
   $('#hauler_Exp').datepicker('setStartDate', _endDate).focus(); //dynamically set new start date for #to
   });

  $('#hauler_Exp').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
   startDate: _endDate,
    todayHighlight: false
  }).on('changeDate', function(e){ 
   _endDate = new Date(e.date.getTime() - (24 * 60 * 60 * 1000)); //get new end date
   //alert(_endDate);
   $('#hauler_Effect').datepicker('setEndDate', _endDate); //dynamically set new End date for #from
  });
  
  $('#hauler_Effect').mask('99/99/9999');
  $('#hauler_Exp').mask('99/99/9999');
		function driver_emp(val){
	
	if(val=="Yes"){
	$('#yes_employee').show();
	$('#explain_no').hide();
	
	}else{
	$('#yes_employee').hide();	
	$('#explain_no').show();
	}
		}
		
		
		function formal_safety(val){
	
		if(val=="Yes"){
	    $('#details_y').show();
	   // $('#explain_no').hide();
	
	       }
		}
	
	function formal_safety1(val){
	  if(val=="No"){
	    $('#details_y').hide();
	   // $('#explain_no').hide();
	
	}
		
		}
		function owner_applicant1(val){
			if(val=="No"){
	    $('#addregister').show();
	   // $('#explain_no').hide();
	
	}else{
	    $('#addregister').hide();	
    	//$('#explain_no').show();
	}
		
		}
		
		
			function broker_loads1(val){
			if(val=="Yes"){
	    $('#addload').show();
	   // $('#explain_no').hide();
	
	}else{
	    $('#addload').hide();	
    	//$('#explain_no').show();
	}
		
		
		
		
		}
		
		
	function loading_add(val){
	//alert(val);
			if(val=="Yes"){
	    $('#addload12').show();
	   // $('#explain_no').hide();
	
	}else{
	    $('#addload12').hide();	
    	//$('#explain_no').show();
	}
	}
		
		
			function applicant_schedule1(val){
			if(val=="Yes"){
	    $('#addschedul').show();
	   // $('#explain_no').hide();
	
	}else{
	    $('#addschedul').hide();	
    	//$('#explain_no').show();
	}
		
		}
	
		
</script>
<script src="<?php echo base_url('js'); ?>/edit_application.js"></script>

      <script>
		  	
			
			function haul_ope1(val){
				
				 if(val=="Yes"){
				  $('#haul_ope11').show();
				 // $('.add_filling_row').show();
				  
				  }else{
				  $('#haul_ope11').hide();
				 // $('.add_filling_row').hide();
				  
				  }
				
				}
			
			function haul_yins1(val){
				
				 if(val=="Yes"){
				  $('#haul_yins11').show();
				  $('#haul_yins12').show();
				  
				  }else{
				  $('#haul_yins11').hide();
				  $('#haul_yins12').hide();
				  
				  }
				
				}
					function ins_pol1(val){
				
				 if(val=="Yes"){
				  $('#ins_pol11').show();
				  $('#ins_pol12').show();
				  
				  }else{
				  $('#ins_pol11').hide();
				  $('#ins_pol12').hide();
				  
				  }
				
				}
			
			
		  function hauler_drivers1(val){
			//  alert(val)
			    if(val=="Yes"){
				  $('#insurance1').show();
				 // $('.add_filling_row').show();
				  
				  }else{
				  $('#insurance1').hide();
				 // $('.add_filling_row').hide();
				  
				  }
			  
			  
			  
			  }
			   function subcon_y1(val){
			//  alert(val)
			    if(val=="Yes"){
				  $('#subcon_y12').show();
				 // $('.add_filling_row').show();
				  
				  }else{
				  $('#subcon_y12').hide();
				 // $('.add_filling_row').hide();
				  
				  }
			  		  		  
			  }
			  
			  		   function subcon_emp_y1(val){
			//  alert(val)
			    if(val=="Yes"){
				  $('#subcon_emp_y12').show();
				  $('#subcon_emp_y11').show();
				  
				  }else{
				  $('#subcon_emp_y12').hide();
				  $('#subcon_emp_y11').hide();
				  
				  }
			  			  
			  }
			  
			  
			  
			  function hauler_haul1(val){
				  	    if(val=="Yes"){
				  $('#hauler_haul12').show();
				 // $('.add_filling_row').show();
				  
				  }else{
				  $('#hauler_haul12').hide();
				 // $('.add_filling_row').hide();
				  
				  }
			  
				  
				  }
			  
		  
		  function filings_name1(val){
			 // alert(val);
			  if(val=="Yes"){
				  $('#filings_number').show();
				  $('.add_filling_row').show();
				  
				  }else{
				  $('#filings_number').hide();
				  $('.add_filling_row').hide();
				  
				  }
			  
			  }
			  		  function sub_haul_y1(val){
			 // alert(val);
			  if(val=="Yes"){
				  $('#sub_haul_y11').show();
				  $('#sub_haul_y12').show();
				  
				  }else{
				  $('#sub_haul_y11').hide();
				  $('#sub_haul_y12').hide();
				  
				  }
			  
			  }
			  
			  
			 
			  
			  
			   		  function vehicles_y1(val){
			 // alert(val);
			  if(val=="Yes"){
				  $('#vehicles_y11').show();
				  $('#vehicles_y12').show();
				  
				  }else{
				  $('#vehicles_y11').hide();
				  $('#vehicles_y12').hide();
				  
				  }
			  
			  }
			  
			  
			  
		    $(document).ready(function() {

        $('.fancybox').fancybox();
		haul_yins1($('#haul_yins').val());
		haul_ope1($('#haul_ope').val());
       ins_pol1($('#ins_pol').val());
	   subcon_y1($('#subcon_y').val());
	   subcon_emp_y1($('#subcon_emp_y').val());
	   sub_haul_y1($('#sub_haul_y').val());
	   vehicles_y1($('#vehicles_y').val());
	  
	    formal_safety($('#safety_y1').val());
		formal_safety1($('#safety_y2').val());
    });
	
		$(document).ready(function(){
		//when the Add Filed button is clicked
		var f=50;
		$("#add_com_row1").click(function (e) {
			//alert(f);
		var row = '<tr>\
			     <td>\
					<select name="aapli_name[]" id="appli_id'+f+'" onchange="aaplic_other('+f+');" class="ui-state-valid">\
                  <option value="Appliances" selected="selected">Appliances</option>\
                 <option value="Other">Other</option>\
				                 </select><br/>\
								 <input class="textinput span8" style="display:none;"  id="aapli_name_other'+f+'" type="text" placeholder="" name="aapli_name_other[]">\
								 </td>\
								 <td class="cargo_show" style="display:none;" >\
								 <input class="textinput input-mini" type="text" placeholder="" name="commodity_cargo_per[]">\
								 </td>\
								 <td class="cargo_show" style="display:none;" >\
								 <input class="textinput input-mini" type="text" placeholder="" name="commodity_cargo_val[]">\
                    </td>\<td>\
					<a class="btn btn-mini btn-1 remove_commodity_row" name="button" id="remove_commodity_row" ><i class="icon icon-remove"></i> </a>\
					</td>\
                  </tr>';
			$("#add_commodity_row").append(row);
			f++;
		 var myarray1=$('#app_id').val();
		 
	 if(myarray1!=null){
		// alert(myarray1);
				//var myarray12 = arr1.split(',');
			    //  alert(myarray12);
				if($.inArray("Cargo", myarray1 )> -1){
					//alert();
				$(".cargo_show").show();
				//$("#badge_id_3").show();
				// alert(1);
				}else{
				$(".cargo_show").hide();
				//alert(2);
			//	$("#badge_id_3").hide();
				}
			
	 }
		});
		
		function show_case(){
				
			
			}
		

	$("body").on("click", ".remove_commodity_row", function (e) {
		//alert($(this).parent("div"));
	//	f--;
	$(this).closest('tr').remove();
	//var i=0;
					
	});
	});


/*$(document).ready(function () {                            
    $("#radio_1").click(function () {
alert();
        if ($("#radio_1").is(":checked")) {
            $('#addown12').show();
        }
        else if ($("#radio_2").is(":checked")) {
            $('#addown12').hide();
        }
        
    });        
});*/

function radio_button(val,id){
//alert(val);
if(val=="Yes"){
$('#addown12'+id).show();

$('html, body').animate({
    scrollTop: $("#add_drivers_truck_div1").offset().top
}, 1000);

$('#1driver_license_num').focus();

//$('#1driver_license_class').focus();

} else if(val="NO"){

$('#addown12'+id).hide();

}
}



	/* function insert_value(id){
var fname=$('#first_name'+id).val();
var mname=$('#middle_name'+id).val();
var lname=$('#last_name'+id).val();
var lnumber=$('#lnumber'+id).val();
var Snumber=$('#lnumber'+id).val();

//console.log(fname+mname+lname);
//alert(fname);
if(fname!=''){$('.fname'+id).val(fname) }
if(mname!=''){$('.mname'+id).val(mname) }
if(lname!=''){$('.lname'+id).val(lname) }
if(lnumber!=''){$('.license'+id).val(lnumber) }
if(Snumber!=''){$('.ssn'+id).val(Snumber) }


}		
*/		  </script>


