<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');
	
	if ($this->session->flashdata('error')){
		echo '<div class="error"><p>'.$this->session->flashdata('error').'</p></div>';
	}
	
	if ($this->session->flashdata('success')){    
		echo '<div class="success" style="font-size: 13px;">'.$this->session->flashdata('success').'</div>';
	}
}
?>

<div id="left">
  <link href="<?php echo base_url('application/modules/user/css');?>/bookmark.css" rel="stylesheet" type="text/css" />
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('application/modules/user/css') ?>/mystyle.css" media="all" />
  <div id="center-column" style="font-size: 13px;">
    <h4>Welcome, <?php echo $this->session->userdata('member_name');?></h4>
    <span><strong>Your Account information is:</strong></span> <!--<span id="editaccount"><a href="<?php //echo base_url('user/account/edit'); ?>">Edit Account</a></span>--> <!--<span id="editaccount"><a href="<?php //echo base_url('user/changepassword') ?>" > Change Password</a></span>-->
    <table width="100%" border="1" class="tsc_table_s3">
      <tr>
        <td><strong>email:</strong></td>
        <td><?php echo $underwriter->email ?></td>
      </tr>
      <tr>
        <td><strong>Name:</strong></td>
        <td><?php echo $underwriter->name ?></td>
      </tr>
      <tr>
        <td><strong>Status:</strong></td>
        <td><?php echo ($underwriter->status==1)? 'Active':'Inactive'; ?></td>
      </tr>
      <tr>
        <td><strong>Created:</strong></td>
        <td><?php echo date('d-M-Y', strtotime($underwriter->created_date)) ?></td>
      </tr>
      <tr>
        <td><strong>Last Modified:</strong></td>
        <td><?php echo date('d-M-Y', strtotime($underwriter->updated_date)) ?></td>
      </tr>
    </table>
  </div>
</div>
<?php
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>
