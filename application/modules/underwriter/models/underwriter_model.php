<?php
/**
 * Admin_model Class extends CI_Model
 *
 * @package    Admin
 * @category   Adminnistrator
 * @author     Gary
 * @link http://www.example.com/adminnistrator/admin_model.html
 */

class Underwriter_model extends CI_Model {

	/**
	 * _delete function
	 *
	 * @access	protected
	 * @params	integer id, string table name
	 * @return	boolean
	 */
	protected function _delete($id, $table)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table); 
	}
	/**
	 * _insert function
	 *
	 * @access	protected
	 * @params	array data, string table name
	 * @return	boolean
	 */
	protected function _insert($data, $table)
	{
		$this->db->set($data);
		if($this->db->insert($table) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	/**
	 * _update function
	 *
	 * @access	protected
	 * @params	integer id, array data, string table name
	 * @return	boolean
	 */
	protected function _update($id, $data, $table)
	{
		$this->db->where('id', $id);
		if($this->db->update($table, $data) !== FALSE)
		{
			return TRUE;
		}

		return FALSE;
	}
	
	public function validate_underwriter()
	{
	  $this->db->where('email', $this->input->post('username'));
	  $this->db->where('password', $this->input->post('password'));
	  $query = $this->db->get($this->config->item('underwriter_table'));
	  
	  if($query->num_rows == 1) 
	  {
			return $query->row();
	  }
	  else  
	  { 
			return FALSE;
	  }
	}
	
	public function get_underwriter_detail($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->config->item('underwriter_table'));
	  
	  	if($query->num_rows == 1) 
	  	{
			return $query->row();
	  	}
	  	else  
	  	{ 
			return FALSE;
	  	}
	}
}