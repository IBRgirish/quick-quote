<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Underwriter extends RQ_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->require_admin_login();
		$this->load->model('underwriter_model');
		$this->load->library('pagination');  
		$this->load->helper('date');
	}
	public function index()
	{
		//echo 1;
		$limit = 25;
		$total = $this->admin_model->count_admins(); 
		$data['admins'] = $this->admin_model->list_admins($limit, 0);
		$data['object'] = $this;
		
		$config['base_url'] = base_url('administration/admins');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;
		
		$data['total'] = $total;
		$this->pagination->initialize($config);

		$this->load->view('administration/admins', $data);
	}
	
	public function page($offset)
    {
		$limit = 25;
		$total = $this->admin_model->count_admins(); 
		$data['admins'] = $this->admin_model->list_admins($limit, $offset);
		$data['object'] = $this;
		
		$config['base_url'] = base_url('administration/admins/index');
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;

		$data['total'] = $total;
		$this->pagination->initialize($config);

			$this->load->view('administration/admins', $data);
      
    }
	 public function edit($id)
	 {
				
				$this->form_validation->set_rules('name', 'Name', 'xss_clean|required|is_unique['.$this->config->item('admin_table').'.name.id.'.$id.']');
				if(!$id)
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		  
        if ($this->form_validation->run() === TRUE)
        {
				$data = array(
									'email' => $this->input->post('email'),
									'name' => $this->input->post('name'),
									'admin_type' => $this->input->post('admin_type'),
									'status' => $this->input->post('status')
								  );
				if($this->input->post('id') > 0)
				{
					if(trim($this->input->post('password')) != '')
						$data['password'] = md5($this->input->post('password'));
					$data['modified_date'] = date('Y-m-d H:i:s');
				}else{
					$data['password'] = md5($this->input->post('password'));
					$data['create_date'] = date('Y-m-d H:i:s');
				}
   	         $this->admin_model->insert_update_admins($data, $this->input->post('id'));
				redirect('administration/admins');
				exit;
		  }
            $admins = $this->admin_model->get_admins_info($id);
            $data['admins'] = $admins;
			$this->load->view('administration/admins_edit', $data);
		  
	 }
	 function delete($id)
	 {
			$this->admin_model->delete_admins($id);
			//redirect('administration/admins');//changed on 29aug12
			redirect($this->config->item('prevous_url'));
			exit;
	 }

	public function verifycode($code)
	{
       $admins = $this->admin_model->get_admins_info($this->input->post('id'), $code);
		if($admins)
		{
            $this->form_validation->set_message('verifycode', 'Promo Code already exists.  Please enter another one.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	
	
	
}	
/* End of file test_post.php */
/* Location: ./application/controllers/test_post.php */