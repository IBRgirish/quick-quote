<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends RQ_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('underwriter_model');
	}
	
	public function index()
	{
		//echo 1;
		if ($this->session->userdata('underwriter_id'))
		{
			$data['header'] = array('title'=>'Underwriter');
			$data['underwriter'] = $this->underwriter_model->get_underwriter_detail($this->session->userdata('underwriter_id'));
			$this->load->view('underwriter/index',$data);
		}
		else
		{
			$this->form_validation->set_rules('username', 'Username','required|valid_email');
			$this->form_validation->set_rules('password','Password','required');
			if ($this->form_validation->run() == TRUE)
			{
				$validate_underwriter = $this->underwriter_model->validate_underwriter();
				
				if ($validate_underwriter)
				{
					if ($validate_underwriter->status==1)
					{
						$uData = array(
							'underwriter_id' 	=> $validate_underwriter->id,
							'underwriter_name' 	=> $validate_underwriter->name,
							'underwriter_email'	=> $validate_underwriter->email,
							'underwriter_pass'	=> $validate_underwriter->password
						);
						
						$this->session->set_flashdata('success', 'You have successfully logged into your account');
						$this->session->set_userdata($uData);
						redirect('underwriter/index');
					}
					else
					{
						$this->session->set_flashdata('error','Account is disabled!');
						redirect('underwriter/index');
					}
				}
				else
				{
					$this->session->set_flashdata('error','Login credentials incorrect');
					redirect('underwriter/index');
				}
			}
			else
			{
				$data['header'] = array('title'=>'Underwriter Login');
				$this->load->view('underwriter/login',$data);
			}
		}
	}
	
	
	public function logout()
	{
        $this->session->sess_destroy();
		  echo '<script language="javascript">window.location.href=\''.base_url('underwriter/index').'\';</script>';
	}
}