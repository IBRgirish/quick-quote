<?php  
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/admin_header');	
	} 
?>

<style>
		label.cabinet {
			width: 79px;
			background: url(images/upload_icon.png) 0 0 no-repeat;
			display: block;
			overflow: hidden;
			cursor: pointer;
			width:62px; margin-bottom:5px; cursor:pointer;height:23px;
	
		}
 
		label.cabinet input.file {
			position: relative;
			cursor: pointer;
			height: 100%;
			width: 120px;
			opacity: 0;
			-moz-opacity: 0;
			filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
		
		}
</style>

<?php
$agency_name = $this->session->userdata('member_name'); 
$characters_only = ' pattern="^[a-zA-Z0-9][a-zA-Z0-9-_\.\ \;\,\*\@\&\)\(\-\=\+\:/]{0,50}$"  ';

//placeholder="MM/DD/YYYY" 
//$date_pattern = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" ';

$date_pattern_js = ' placeholder="MM/DD/YYYY" class="date"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" ';

/*
  $date_pattern = ' placeholder="MM/DD/YYYY" class="date"  pattern="^([0-9]{2,2})/([0-9]{2,2})/([0-9]{4,4})" ';
 */

$email_pattern = 'pattern="^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$"';
$number_pattern = ' pattern="[0-9]+" ';
$less_100_limit = ' pattern="([1-9]{1}|(10)|[0-9]{2,2})" size="2" maxlength="2" ';
?>


<?php
//echo includes_dir(); die;
//include(base_url().'includes/functions.php');
//echo date_default_timezone_get();
$date = date('m/d/Y', time());
$time = date('h:i:s a', time());

$quote_id = 1;
//echo '<pre>';
//print_r($user_quote_data);

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title> Quick Quote </title>
		<link href="<?php echo base_url('css');?>/chosen.min.css" rel="stylesheet">
     	<script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
    	<script src="<?php echo base_url('js'); ?>/jquery.price_format.js"></script>
    	<script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"></script >
		<script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>

		<script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
		
		<script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
		<!--script src="<?php echo base_url('js'); ?>/jquery.validateForm.js"></script-->
   

        <style>
            .table thead th {
                text-align: center;
                vertical-align: inherit;
            }

            .table th, .table td {
                font-size: 0.8em;
            }

            .row_added_vehicle{
                margin:0px 0px 5px 0px;
            }
            .status_msg {
                color: #ff0000;
            }
			
			#upload_file_name_second{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name_second li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name_second input{
				display: none;
			}
			
			#upload_file_name_second li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name_second li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name_second li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name_second li span{
				width: 15px;
				height: 12px;
				background: url('images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name_second li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name_second li.error p{
				color:red;
			}

			
			
			
			#upload_file_name1{
				list-style:none;
				margin:0 -30px;
				/*border-top:1px solid #2b2e31;
				border-bottom:1px solid #3d4043;*/
			}
			
			#upload_file_name1 li{
			
				/*background-color:#333639;
			
				background-image:-webkit-linear-gradient(top, #333639, #303335);
				background-image:-moz-linear-gradient(top, #333639, #303335);
				background-image:linear-gradient(top, #333639, #303335);
			
				border-top:1px solid #3d4043;
				border-bottom:1px solid #2b2e31;*/
				padding:15px;
				height: 52px;
			
				position: relative;
			}
			
			#upload_file_name1 input{
				display: none;
			}
			
			#upload_file_name1 li p{
				width: 144px;
				overflow: hidden;
				white-space: nowrap;
				color: #000;
				font-size: 16px;
				font-weight: bold;
				position: absolute;
				top: 20px;
				left: 100px;
			}
			
			#upload_file_name1 li i{
				font-weight: normal;
				font-style:normal;
				color:#7f7f7f;
				display:block;
			}
			
			#upload_file_name1 li canvas{
				top: 15px;
				left: 32px;
				position: absolute;
			}
			
			#upload_file_name1 li span{
				width: 15px;
				height: 12px;
				background: url('images/icons.png') no-repeat;
				position: absolute;
				top: 34px;
				right: 33px;
				cursor:pointer;
			}
			
			#upload_file_name1 li.working span{
				height: 16px;
				background-position: 0 -12px;
			}
			
			#upload_file_name1 li.error p{
				color:red;
			}
        </style>	 

<script type="text/javascript">


function apply_on_all_elements(){
		
		$('input').not('.datepick').autotab_magic().autotab_filter();
		
		//$('input').attr('required', 'required');
		
		$('.price_format').priceFormat({
		prefix: '$ ',
		centsLimit: 0,
		thousandsSeparator: ','
		});
		
		$(".license_issue_date").mask("99/99/9999");
		
		//  $('input').attr('required', 'required');  
		//$('select').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		
		$('#vehicle_tractor').removeAttr('required');
		$('#vehicle_truck').removeAttr('required');
		$('#zip2').removeAttr( 'required' );
		$('.zip2').removeAttr( 'required' );
		$('.filing_numbers').removeAttr('required');
		$('.filing_type').removeAttr('required');
		$('.last_name').removeAttr('required');
		$('.middle_name').removeAttr('required');
		
		//$('#vehicle_pd').removeAttr('required');
		//$('#vehicle_liability').removeAttr('required');
		//$('#vehicle_cargo').removeAttr('required');
		$('#cab').removeAttr('required');
		$('#tel_ext').removeAttr('required');
		$('#insured_email').removeAttr('required');
		$('#insured_tel_ext').removeAttr('required');
		
		
		// $('form').h5Validate(); 	
		
}

	$(document).ready(function() {


			$('#first_upload').click(function(){
			
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values').value = '';
				$('#drop a').parent().find('input').click();
    		});
			
			$('#first_upload_second').click(function(){
				
        	// Simulate a click on the file input button
				//uploaded_files = '';
				document.getElementById('uploaded_files_values_second').value = '';
				$('#drop_second a').parent().find('input').click();
    		});
			
			
		apply_on_all_elements();
		$("#coverage_date").mask("99/99/9999");
		
		$('form').h5Validate();
		
		//$('input').attr('required', 'required'); 
		//$('input[type="checkbox"]').removeAttr( 'required' );
		//$('input[type="checkbox"]').removeAttr( 'required' );
		$('input[type="file"]').removeAttr( 'required' ); 
		
/* 		$('#coverage_date').datepicker({
			format: 'mm/dd/yyyy'
		}); */
		//$("#coverage_date").mask("99/99/9999");
 	 	/* var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

		var checkin = $('#coverage_date').datepicker({
		onRender: function(date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
		}).on('changeDate', function(ev) {
		// $(this).blur();
		checkin.hide();		 
		}).data('datepicker');   */
		
		/* $('#coverage_date').blur(function() {
  			$('#coverage_date').datepicker("hide");
		});
		 */
		
		//Hide datepicker on Focus Out		
	/* 	$('#coverage_date').blur(function(){
			//setTimeout(function(){$('#coverage_date').datepicker('hide');},100)
		}); */
		
		var date = new Date();
		date.setDate(date.getDate()-0);
				
		 var inputs1 = $('#coverage_date').datepicker({
			startDate: date,
			format: 'mm/dd/yyyy',
			autoclose: true
		});  


		
		$('#dba').change(function(){
			//check_insured_dba_required();			
		});
		
		$('#insured_name').change(function(){
			//check_insured_dba_required();			
		});

		

    var config = {
      '.chzn-select'           : {},
      '.chzn-select-deselect'  : {allow_single_deselect:true},
      '.chzn-select-no-single' : {disable_search_threshold:10},
      '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chzn-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }

//$('.request_quote').validateForm();
/* 
		var hiddenEls = $("body").find(":hidden").not("script");

	$("body").find(":hidden").each(function(index, value) {
	selctor= this.id;
	if(selctor){
		$( '#'+selctor+' :input').removeAttr("required");
		}
		alert(this.id);
	}); */
	
	/*  $(".request_quote").validateForm({
		ignore: ":hidden",
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			
			$('#loss_report_upload1').click(function() {  
				//var up_id = $('.upload_select:last').attr('id');
				//alert(up_id);
				$('#fileup1_1').click();  
				//$('#'+up_id).click();  
			
			});
			 
			$('#mvr_upload').click(function() { 
			$('#fileup_1').click();  });
 
			
	

	});
	
	
	
		function form_sub(){
		/* $(".request_quote").validateForm({
			submitHandler: function(form) {
			// do other stuff for a valid form
			form.submit();
			}
			}); */
			//$('.request_quote').validateForm();
			
/* 			if($('#theid').css('display') == 'none'){ 
   $('#theid').show('slow'); 
} else { 
   $('#theid').hide('slow'); 
}
$("br[style$='display: none;']") */


			
	}
	
	function check_insured_dba_required(){
			dba_val = $('#dba').val().trim();
			insured_val = $('#insured_name').val().trim();
			if(dba_val){
				$('#insured_name').removeAttr('required'); 
				$('#insured_middle_name').removeAttr('required');
				$('#dba').attr('required', 'required'); 
			}else if(insured_val){
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').removeAttr('required'); 		
			} else {
				$('#insured_name').attr('required');
				$('#insured_middle_name').attr('required');
				$('#dba').attr('required', 'required'); 	
			}
		}
		
	
	<!-----updated by amit03-03-2014------>
	
	
			function rofoperation(val , id){
			
				if(id=='1tractor_radius_of_operation')
				{							
					$(".tractor_radius_of_operation").val(val);
					$(".tractor_radius_of_operation").not(":first").attr("disabled",true);
					if(val == 'other'){
						$('.show_tractor_others').show();
						$(".specify_other_cp").not(":first").attr("disabled",true);
					} else {
						$('.show_tractor_others').hide();
					}	
				}
				else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').show();
					} else {
						$("#"+id).parents('.row_added_tractor ').find('.show_tractor_others').hide();
					}	
					
				}
				
				
				
			/*  alert(id);
			alert(val);  */
			
			
			}
			
			
			
			function rofoperation2(val , id){
				
				if(id=='1truck_radius_of_operation')
				{
					$(".truck_radius_of_operation").val(val);
					if(val == 'other'){
						$('.show_truck_others').show();
					} else {
						$('.show_truck_others').hide();
					}
				}	
							else
				{
					if(val == 'other'){
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').show();
					} else {
						$("#"+id).parents('.row_added_vehicle ').find('.show_truck_others').hide();
					}	
					
				}
			}

				
				
				
				
			
					
			
		function check_commodities_other(id){ 
			$("#"+id+" option:selected").each(function(){
			  val = $(this).text(); 
			  if(val == 'other'){ 
					$('.'+id+'_otr').show();
				} else { 
					$('.'+id+'_otr').hide();
				}
			});
		}
	
	
	$(document).ready(function(e) {
        $("body").on("keyup",".commhauled:first",function(){
			$(".commhauled").val($(this).val());
		});
		$("body").on("keyup",".commhauled2:first",function(){
			$(".commhauled2").val($(this).val());
		});
		
		
		 $("body").on("keyup",".cargo_class_price:first",function(){
			$(".cargo_class_price").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_price2:first",function(){
			$(".cargo_class_price2").val($(this).val());
		});
		
		
		$("body").on("keyup",".cargo_class_ded:first",function(){
			$(".cargo_class_ded").val($(this).val());
		});
		$("body").on("keyup",".cargo_class_ded2:first",function(){
			$(".cargo_class_ded2").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp:first",function(){
			$(".specify_other_cp").val($(this).val());
		});
		
		$("body").on("keyup",".specify_other_cp2:first",function(){
			$(".specify_other_cp2").val($(this).val());
		});
		
    });
	
	
	
	
	<!-----updated by amit03-03-2014------>
</script>
       
		<style>
    .well{width:80%; margin:auto;}
            .vehicle_schedule, .trailer_div { border :1px solid #E3E3E3; padding:5px; }

            .row-fluid .span1_of_11 { width: 7.1%; }
        </style>

        <style>
            @media only screen and (min-width: 950px) {
                label {display: inline-block; padding: 0 5px 0 10px; } 
                .default_input{ width:20%;}
                .mini_input{ width:8%;}
                .micro_input{ width:6%;}
                .max_input{ width:40%;}
            }
			
		.coverage_label_sub{
			margin-left: -10px;
			margin-top: 25px;
			padding: 15px;
			}
			
			



.ui-state-error {
    background: none repeat scroll 0 0 #FEF3F3 !important;
    border: 1px solid #FAABAB !important;
    box-shadow: 0 0 5px rgba(204, 0, 0, 0.5) !important;
}

        </style>
    </head>
    <body>




        <h2 style="text-align:center;" >Quick Quote Sheet</h2>
        <div class="container-fluid">      

            <?php
            $attributes = array('class' => 'request_quote', 'id' => 'main_form');
            echo form_open_multipart(base_url('manualquote/QuickQuote'), $attributes);
            ?> 
            <div class="well">
                <?php if (!empty($message)) { ?>
                    <div id="message">
                        <?php echo $message; ?>
                    </div>
                <?php } ?>
                <fieldset class="head_box">



                    <!-- Form Name -->
                    <legend>Broker Info</legend>

                    <div class="row-fluid">
                        <div class="span12">

                            <div class="row-fluid">  
                                <div class="span2 lightblue">
                                    <label >Today's Date</label>
                                    <input id="date" name="date" type="text" class="span12" value="<?php echo $date; ?>"  maxlength="10" readonly >
                                </div>
                                <div class="span2 lightblue">
                                    <label >Time</label>
                                    <input id="time" name="time" type="text" class="span12" readonly  value="<?php echo $time; ?>" maxlength="11" >
                                </div>
                                <div class="span2 lightblue">
                                    <label >Coverage Date</label>
                                    <input required pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" id="coverage_date" name="coverage_date" type="text" class="span12 datepick" maxlength="10" value="<?php if(isset($quote->coverage_date)) { $timestamp = strtotime($quote->coverage_date);  echo date('m/d/Y', $timestamp); } else { echo ''; } ?>" <?php    if (isset($quote->coverage_date)) { echo "readonly";} ?>>
                                </div>	
                                <div class="span6 lightblue">
                                    <label > Person submitting business </label>
									<?php
									$broker_name_first = '';
									$broker_name_middle = '';
									$broker_name_last = '';
									/* if(isset($user_quote_data) && !empty($user_quote_data)) {
										$broker_name_first = $user_quote_data->first_name;
										$broker_name_middle = $user_quote_data->middle_name;
										$broker_name_last = $user_quote_data->last_name;
									} else {
										$broker_name_first = isset($quote->contact_name) ? $quote->contact_name : '';
										$broker_name_middle = isset($quote->contact_middle_name) ? $quote->contact_middle_name : '';
										$broker_name_last = isset($quote->contact_last_name) ? $quote->contact_last_name : '';
									} */
									?>
									  <div class="span12" >
                                    <input required id="name" maxlength="80" name="name" placeholder="First Name" type="text"  class="span4 first_name" value="<?php echo $broker_name_first; ?>" <?php    if (!empty($broker_name_first)) { echo "readonly";} ?> >
							      <input id="middle_name" maxlength="80" placeholder="Middle Name" name="middle_name" type="text"  class="span4 middle_name" value="<?php echo $broker_name_middle; ?>" <?php    if (!empty($broker_name_middle)) { echo "readonly";} ?> >
                                   <input id="last_name" maxlength="80" name="last_name" placeholder="Last Name" type="text"  class="span4 last_name" value="<?php echo $broker_name_last; ?>" <?php    if (!empty($broker_name_last)) { echo "readonly";} ?> >
                             
								</div>
								</div>
							

                            </div>	
							
						
							

                            <div class="row-fluid">  
							
							     <div class="span2 lightblue">
                                    <label >Cab Number</label>
                                    <input id="cab" name="cab" type="text"  class="span12" maxlength="8" value="<?php echo (isset($quote->cab) ? $quote->cab : ''); ?>" >
                                </div>

                                <div class="span3 lightblue no-use" style="display:none;" >
                                    <label >Agency Name</label>
                                    <input id="agency" name="agency" type="text" class="span12" maxlength="15" value="<?php echo $agency_name; ?>" <?php    if (isset($quote->agency)) { echo "readonly";} else { echo '';} ?> >
                                </div>

                                <div class="span4 lightblue">
                                    <?php
                                   
                                    ?>
                                    <label >Phone Num.</label> 
									<?php 
									$tel_ext = '';
									if(isset($user_quote_data) && !empty($user_quote_data)) {
										$pieces[0] = substr($user_quote_data->phone_number, 0, 3);
										$pieces[1] = substr($user_quote_data->phone_number, 3, 3);
										$pieces[2] = substr($user_quote_data->phone_number, 6);
										
										if(isset($user_quote_data->tel_ext)){
											$tel_ext = $user_quote_data->tel_ext;
										}
										
									}elseif (isset($quote->phone_no)) {
                                        $pieces = explode("-", $quote->phone_no);
										if(isset($quote->tel_ext)){
											$tel_ext = $quote->tel_ext;
										}
                                    } else {
                                        $pieces = array();
                                    }
									
									
									?>
                                    <div class="span12">
										 <input id="telephone1" <?php echo $number_pattern; ?> name="telephone[1]" type="text" class="span3" maxlength="3" value="<?php
                                    if (isset($pieces[0])) {
                                        echo $pieces[0];
                                    } 
                                  
                                    ?>" <?php    if (!empty($pieces[0])) { echo "readonly";} else { echo  ' required ';}  ?>>
                                        <input id="telephone2" name="telephone[2]" type="text" class="span3"  maxlength="3" <?php echo $number_pattern; ?> value="<?php
                                               if (isset($pieces[1])) {
                                                   echo $pieces[1];
                                               }
                                               
                                    ?>" <?php    if (!empty($pieces[1])) { echo "readonly";} else { echo  ' required ';} ?> >
                                        <input id="telephone3" name="telephone[3]" type="text" class="span3"  maxlength="4" <?php echo $number_pattern; ?> value="<?php
                                               if (isset($pieces[2])) {
                                                   echo $pieces[2];
                                               }
                                              
                                    ?>"  <?php    if (!empty($pieces[2])) { echo "readonly";} else { echo  ' required ';} ?>>
									 <input id="tel_ext" name="tel_ext" placeholder="Ext" type="text"  class="span3" maxlength="5" <?php //echo $number_pattern; ?> value="<?php echo $tel_ext; ?>" <?php if(!empty($tel_ext)) { echo 'readonly';}?> >
                                    
                                    </div>
									
                                </div>
								
								   <div class="span3 lightblue">
                                    <?php
									if(isset($user_quote_data) && !empty($user_quote_data)) {
										$pieces_cell[0] = substr($user_quote_data->cell_number, 0, 3);
										$pieces_cell[1] = substr($user_quote_data->cell_number, 3, 3);
										$pieces_cell[2] = substr($user_quote_data->cell_number, 6);
									}elseif (isset($quote->cell_no)) {
                                        $pieces_cell = explode("-", $quote->cell_no);
                                    } else {
                                        $pieces_cell = array();
                                    }
                                    ?>
                                    <label >Cell Ph. Num.</label> 
                                    <div class="span12">
                                        <input id="cell1" name="cell[1]" type="text" class="span4" maxlength="3" <?php echo $number_pattern; ?> value="<?php
                                    if (!empty($pieces_cell)) {
                                        echo $pieces_cell[0];
                                    } 
                                  
                                    ?>" <?php    if (!empty($pieces_cell[0])) { echo "readonly";}  ?>>
                                        <input id="cell2" name="cell[2]" type="text" class="span4"  maxlength="3" <?php echo $number_pattern; ?> value="<?php
                                               if (isset($pieces_cell[1])) {
                                                   echo $pieces_cell[1];
                                               }
                                               
                                    ?>" <?php    if (!empty($pieces_cell[1])) { echo "readonly";}  ?> >
                                        <input id="cell3" name="cell[3]" type="text" class="span4"  maxlength="4" <?php echo $number_pattern; ?>value="<?php
                                               if (isset($pieces_cell[2])) {
                                                   echo $pieces_cell[2];
                                               }
                                              
                                    ?>"  <?php    if (!empty($pieces_cell[2])) { echo "readonly";}  ?>>
                                    </div>
                                </div>
								
																<div class="span3 lightblue">
                                    <label >Email</label>
									<?php
									if(isset($user_quote_data) && !empty($user_quote_data)) {
									$broker_email = $user_quote_data->email;
									} elseif(isset($quote->email)) {
									$broker_email = $quote->email;
									}else { 
									$broker_email = '';
									}
									?>
								
									<?php echo $agencies; ?>
                                   <input id="email" name="email" type="hidden"  maxlength="100" <?php echo $email_pattern; ?> class="span12" maxlength="30" value="<?php echo $broker_email; ?>" <?php    if (!empty($broker_email)) { echo "readonly";} else { echo ' required '; }  ?>  > 
                                </div>
								
							</div>	
						



                            <div class="row-fluid">  
							                                   
							 <div class="span3 lightblue">
							 <label >Other contact Num.</label>
                             <div class="span12">
							 <select name="contact_field" id="contact_field" class="span12 not_required" onChange="show_contact_field(this.value)" >
							 <option value="">Select</option>
							 <option value="fax">Fax</option>
							 <option value="other">Other Phone</option>
							 <option value="other_email">Other Email</option>
							 </select>
							 </div>
							 </div>
							 <div class="span3 lightblue" id="broker_fax" style="display:none;">
                                    <label >Fax</label>
                                    <div class="span12">
                                        <?php
										if(isset($user_quote_data) && !empty($user_quote_data)) {
										$pieces1[0] = substr($user_quote_data->fax_number, 0, 3);
										$pieces1[1] = substr($user_quote_data->fax_number, 3, 3);
										$pieces1[2] = substr($user_quote_data->fax_number, 6);
										}elseif (isset($quote->fax)) {
                                            $pieces1 = explode("-", $quote->fax);
                                        } else {
                                            $pieces1 = array();
                                        }
                                        ?>
                                        <input id="fax1" name="fax[1]" type="text"  class="span4" maxlength="3" value="<?php
                                        if (isset($pieces1[0])) {
                                            echo $pieces1[0];
                                        }
                                        ?>"  <?php    if (!empty($pieces1[0])) { echo "readonly";}  ?> >
                                        <input id="fax2" name="fax[2]" type="text"  class="span4" maxlength="3" value="<?php
                                               if (isset($pieces1[1])) {
                                                   echo $pieces1[1];
                                               }
                                        ?>"  <?php    if (!empty($pieces1[1])) { echo "readonly";}  ?> >
                                        <input id="fax3" name="fax[3]" type="text"  class="span4" maxlength="4" value="<?php
                                               if (isset($pieces1[2])) {
                                                   echo $pieces1[2];
                                               }
                                        ?>"  <?php    if (!empty($pieces1[2])) { echo "readonly";}  ?> >
                                    </div>
                                </div>
								
								 <div class="span3 lightblue" id="broker_other" style="display:none;">
                                    <label >Other</label>
                                    <div class="span12">
                                        <?php
										if(isset($user_quote_data) && !empty($user_quote_data) && isset($user_quote_data->other_number)) {
										$pieces1[0] = substr($user_quote_data->other_number, 0, 3);
										$pieces1[1] = substr($user_quote_data->other_number, 3, 3);
										$pieces1[2] = substr($user_quote_data->other_number, 6);
										}elseif (isset($quote->other_number)) {
                                            $pieces1 = explode("-", $quote->other_number);
                                        } else {
                                            $pieces1 = array();
                                        }
                                        ?>
                                        <input id="other1" name="other[1]" type="text"  class="span4" maxlength="3" value="<?php
                                        if (isset($pieces1[0])) {
                                            echo $pieces1[0];
                                        }
                                        ?>"  <?php    if (!empty($pieces1[0])) { echo "readonly";}  ?> >
                                        <input id="other2" name="other[2]" type="text"  class="span4" maxlength="3" value="<?php
                                               if (isset($pieces1[1])) {
                                                   echo $pieces1[1];
                                               }
                                        ?>"  <?php    if (!empty($pieces1[1])) { echo "readonly";}  ?> >
                                        <input id="other3" name="other[3]" type="text"  class="span4" maxlength="4" value="<?php
                                               if (isset($pieces1[2])) {
                                                   echo $pieces1[2];
                                               }
                                        ?>"  <?php    if (!empty($pieces1[2])) { echo "readonly";}  ?> >
                                    </div>
                                </div>
								
								 <div class="span3 lightblue" id="broker_email" style="display:none;">
                                    <label >Other Email</label>
                                    <div class="span12">
									<input type="text" <?php echo $email_pattern; ?> name="broker_other_email" maxlength="100" >
									</div>
								</div>	
								

								
								 <div class="span3 lightblue">
                                    <label >Zip</label>
                                    <div class="span12">
                                        <?php
										if(isset($user_quote_data) && !empty($user_quote_data)) {
											$pieces3[0] = $user_quote_data->zip_code;
											$pieces3[1] = $user_quote_data->zip_code_extd;
										}
                                        elseif (isset($quote->zip)) {
                                            $pieces3 = explode("-", $quote->zip);
                                        } else {
                                            $pieces3 = array();
                                        }
                                        ?>
                                        <input id="zip1" name="zip[1]" type="text" class="span6" value="<?php
                                        if (isset($pieces3[0])) {
                                            echo $pieces3[0];
                                        }
                                        ?>"  <?php if (!empty($pieces3[0])) { echo "readonly";} ?> >
                                        <input id="zip2" name="zip[2]" type="text" class="span6 zip2" value="<?php
                                               if (isset($pieces3[1])) {
                                                   echo $pieces3[1];
                                               }
                                        ?>" <?php if (!empty($pieces3[1])) { echo "readonly";} ?>  >
                                    </div>
                                </div>
							</div>
							</fieldset>
							
						<fieldset class="insured_info_box" >
							<legend >Insured info </legend>
						<div class="row-fluid">  
							 
								
								<div class="span6 lightblue">
                                    <label> Insured </label>
									<div class="span12">
                                    <input type="text" maxlength="255" value="<?php echo (isset($quote->insured) ? $quote->insured : ''); ?>" class="span4 first_name ui-state-valid" placeholder="First Name" name="insured_name" id="insured_name" required>
									<input type="text" value="" class="span4 middle_name" name="insured_middle_name" placeholder="Middle Name" id="insured_middle_name" >
									<input type="text" value="" class="span4 last_name" placeholder="Last Name" name="insured_last_name" id="insured_last_name">
                             
									</div>
								</div>
								
								
                                <div class="span3 lightblue">
                                    <label >DBA</label>
                                    <input id="dba" name="dba" type="text" class="span12" maxlength="255" value="<?php echo (isset($quote->dba) ? $quote->dba : ''); ?>"  >
                                </div>
								
								<div class="span3 lightblue">
                                    <label >Email</label>
									<?php

									if(isset($quote->insured_email) ) {
									$insured_email = $quote->insured_email;
									} else { 
									$insured_email = '';
									}
									?>
                                    <input id="insured_email" <?php echo $email_pattern; ?> name="insured_email" type="text"  class="span12" maxlength="30" value="<?php echo $insured_email; ?>" >
                            </div>
                              
                               

                               

                            </div>	

                            <div class="row-fluid">  
							
							 <div class="span4 lightblue">
                                    <?php
                                   
                                    ?>
                                    <label >Phone Num.</label> 
									<?php 
									$insured_tel_ext = '';
									if(isset($insured_data) && !empty($insured_data)) {
										$insured_pieces[0] = substr($insured_data->phone_number, 0, 3);
										$insured_pieces[1] = substr($insured_data->phone_number, 3, 3);
										$insured_pieces[2] = substr($insured_data->phone_number, 6);
										
										if(isset($insured_data->tel_ext)){
											$insured_tel_ext = $insured_data->tel_ext;
										}
										
									} else {
                                        $insured_pieces = array();
                                    }
									
									
									?>
                                    <div class="span12">
										 <input id="insured_telephone1" name="insured_telephone[1]" type="text" class="span3" maxlength="3" <?php echo $number_pattern; ?> value="<?php
                                    if (isset($insured_pieces[0])) {
                                        echo $insured_pieces[0];
                                    } 
                                  
                                    ?>"  required="required">
                                        <input id="insured_telephone2" name="insured_telephone[2]" type="text" class="span3"  <?php echo $number_pattern; ?> maxlength="3" value="<?php
                                               if (isset($insured_pieces[1])) {
                                                   echo $insured_pieces[1];
                                               }
                                               
                                    ?>" required >
                                        <input id="insured_telephone3" name="insured_telephone[3]" type="text" class="span3"  <?php echo $number_pattern; ?> maxlength="4" value="<?php
                                               if (isset($insured_pieces[2])) {
                                                   echo $insured_pieces[2];
                                               }
                                              
                                    ?>" required >
									 <input id="insured_tel_ext" name="insured_tel_ext" placeholder="Ext" type="text"  <?php //echo $number_pattern; ?> class="span3" maxlength="5" value="<?php echo $insured_tel_ext; ?>"  >
                                    
                                    </div>
									
                                </div>
							
							  <div class="span3 lightblue">
                                    <label >Garaging City</label>
                                    <input id="garaging_city" name="insured_garaging_city" type="text"  class="span12" value="<?php echo (isset($quote->garaging_city) ? $quote->garaging_city : ''); ?>"  >
                                </div>
								
								 <div class="span2 lightblue">
                                    <label >State</label>
									<?php
									$attr = "";
									$broker_state = 'CA';
									
									?>
                                    <?php echo get_state_dropdown('insured_state[]', $broker_state, $attr); ?>

                                </div>
							
								 <div class="span3 lightblue">
                                    <label >Zip</label>
                                    <div class="span12">
                                        <?php
										if(isset($quote->insured_zip)) {
											$pieces3[0] = $user_quote_data->zip_code;
											$pieces3[1] = $user_quote_data->zip_code_extd;
										}
                                        elseif (isset($quote->zip)) {
                                            $pieces3 = explode("-", $quote->zip);
                                        } else {
                                            $pieces3 = array();
                                        }
                                        ?>
                                        <input id="zip1" name="insured_zip[1]" type="text" class="span6" value="<?php
                                        if (isset($pieces3[0])) {
                                            echo $pieces3[0];
                                        }
                                        ?>"  <?php if (!empty($pieces3[0])) { echo "";} ?>  required="required" >
                                        <input id="zip2" name="insured_zip[2]" type="text" class="span6 zip2" value="<?php
                                               if (isset($pieces3[1])) {
                                                   echo $pieces3[1];
                                               }
                                        ?>" <?php if (!empty($pieces3[1])) { echo "";} ?>  >
                                    </div>
                                </div>
								
								
							
					  </div>
					  
					<div class="row-fluid">  
					
                                <div class="span3 lightblue">
                                    <label>Nature of Business</label>
                                    <?php echo $nature_of_business; ?>
                                </div>

                                <div class="span3 lightblue">
                                    <label>Commodities Hauled</label>
                                    <?php echo $commodities_haulted; ?>
                                </div>
								<div class="span3 commodities_haulted_otr" style="display:none;">
									<label>Specify Other</label>
								 <input type="text" id="commodities_haulted" name="commodities_haulted[]" value="" >
								 
								</div>
					   <div class="span3 lightblue">
                                    <label>Years in Business</label>
                                    <select id="business_years" name="business_years" >
                                        <option>New venture</option>
                                        <option <?php echo ((isset($quote->years_in_business) && $quote->years_in_business == 1 ) ? 'selected="selected"' : ''); ?> >1 year</option>
                                        <option <?php echo ((isset($quote->years_in_business) && $quote->years_in_business == 2 ) ? 'selected="selected"' : ''); ?> >2 years</option>
                                        <option <?php echo ((isset($quote->years_in_business) && $quote->years_in_business == 3 ) ? 'selected="selected"' : ''); ?> >3 years</option>
                                        <option <?php echo ((isset($quote->years_in_business) && $quote->years_in_business >= 4 ) ? 'selected="selected"' : ''); ?> >4+ years</option>
                                    </select>
                                </div>
                 </div>
				 <div class="row-fluid ">
					<div class="span12">
						<label>*Disclaimer: If there is a commodity not listed, please specify them by selecting “Other".</label>
					</div>
				</div>

				

</fieldset>

<!--LOSS REPORT -->

<fieldset class="loses_information_box">
	<legend >Loss(es) information</legend>
	<div class="row-fluid " >
		<div class="span12" >
		<label >Disclaimer: If you are uploading the loss runs then you do not need to fill out the Losses information section</label>
		</div>
	</div>
	
    <div class="row-fluid">
    <div class="span12 lightblue">
        <label>Do you have any losses in the last three years ?</label>
        <select name="losses_need" id="losses_need" required="required" >
            <option value="">--Select--</option>
            <option value="yes">YES</option>
            <option value="no">NO</option>
        </select>
    </div>
    
	
		
    <div id="losses_div" class="losses_div" style="display:none;">
		
<?php 
$losses_liability = array('name'=> 'losses_liability',
						  'title' => 'Liability',
						  'other' => '0',
							);
							
$losses_pd = array('name'=> 'losses_pd',
'title' => 'P.D.',
'other' => '0',
);

$losses_cargo = array('name'=> 'losses_cargo',
'title' => 'Cargo',
'other' => '0',
);

$losses_other1 = array('name'=> 'losses_other1',
'title' => 'Other 1',
'other' => '1',
);

$losses_other2 = array('name'=> 'losses_other2',
'title' => 'Other 2',
'other' => '1',
);

							
$sections_data = array ( $losses_liability , $losses_pd, $losses_cargo, $losses_other1, $losses_other2);

foreach( $sections_data as $sec_d){
	$sec_name =  $sec_d['name']; ?>    
		<div class="row-fluid " >
			<div class="span6"   >
			
			<?php /* Edited By Sunil @ 02-08-2013 Start */ ?>
			<!--<button class="btn btn-primary" type="button" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >Click To Open</button>-->
			<div class="span4 lightblue losses_head" ><p><b><?php echo $sec_d['title'] ?> </b></p></div>
			<button class="label label-info" type="button" id="<?php echo $sec_name ?>" onclick="show_losses(this.id)" style="width:250px; height:31px;margin-bottom:15px; padding:5px;" >
			<label class="open_btn" id="<?php echo $sec_name ?>_text">Click To Open</label></button>
			<!--<a href="javascript:void(0);" id="< ?php echo $sec_name ?>" onclick="show_losses(this.id)" >
				<div id="< ?php echo $sec_name ?>_icon" class="icon-arrow-down"> </div>
			</a>-->
			<?php /* Edited By Sunil @ 02-08-2013 End */ ?>
			
			<!--a href="javascript:void(0);" id="<?php echo $sec_name ?>" onclick="show_losses(this.id)" >
				<div class="span8 lightblue losses_head" ><p><b><?php echo $sec_d['title'] ?> </b></p></div><div id="<?php echo $sec_name ?>_icon" class="icon-arrow-down"> </div>
			</a-->
			</div> 
			<div id="<?php echo $sec_name ?>_div" style="display:none;" >
				<div class="row-fluid " >
					<div class="span12"  >
						<div class="span4">
						Number of losses in last 3 years ? 
						<input type="text" <?php echo $less_100_limit; ?> onChange="add_losses(this.value, '<?php echo $sec_name ?>')" class="ssss span3 number_limit" value="<?php echo (!empty($losses))? count($losses):''; ?>" />
						</div>
						<?php if($sec_d['other']){ ?>
						<div class="span4">
							Please specify
							<input type="text" class="span5" name="<?php echo $sec_name ?>_specify_other" />
						</div>
						<?php } ?>
					</div>
				</div>
				                           
				<div class="row-fluid">  
					<div class="span12 lightblue">
						<table id="<?php echo $sec_name ?>_table" class="table table-bordered table-striped table-hover" style="display:none" >
							<thead> 
							  <tr>
								<th colspan="2" >Period of <br> losses</th>
								<th rowspan="2" ><?php echo $sec_d['title'] ?> <br> Amount</th>
								<th rowspan="2" >Company</th>
								<th rowspan="2" >General Agent </th>
								<th rowspan="2" >Date of Loss</th>
							  </tr>
							  <tr colspan="2" >
								<th>From (mm/dd/yyyy)</th>
								<th>To (mm/dd/yyyy)</th>
							  </tr>
							 
							</thead>
							<tbody id="add_<?php echo $sec_name ?>_row" >
							</tbody>
						</table>
					</div>
				</div>
			</div> 
		</div> 
<?php } ?>
</div>
		
		
</fieldset>
 <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div> 

<!--
                            <div class="row-fluid " >  	
                                <div class="span12 lightblue">
                                    Number of losses in last 3 years ? 
                                    <input type="text" onchange="add_losses(this.value)" class="span1" value="<?php echo (!empty($losses))? count($losses):''; ?>" />
                                </div>
                            </div>	
                            <?php if (!empty($losses)) { //echo count($losses); die;  ?> 


                                <div class="row-fluid">  
                                    <div class="span12 lightblue">
                                        <table id="losses_table" class="table table-bordered table-striped table-hover"  >
                                            <thead>
                                                <tr>
                                                    <th colspan="2" >Period of <br> losses</th>
                                                    <th rowspan="2" >Liability <br> Amount</th>
                                                    <th rowspan="2" >Physical Damage <br> Amount</th>
                                                    <th rowspan="2" >Cargo <br> Amount </th>
                                                </tr>
                                                <tr colspan="2" >
                                                    <th>From (mm/dd/yyyy)</th>
                                                    <th>To (mm/dd/yyyy)</th>
                                                </tr>
                                            </thead>
                                            <?php
                                            foreach ($losses as $losses) {
                                                //  for($i=0; $i<count($losses); $i++) { 
                                                ?>   
                                                <tbody id="add_losses_row"><tr>                
												<td>
												<input type="text" maxlength="10" class="span12 datepicker" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" id="date_from" name="losses_from[]" value="<?php if(isset($losses->losses_from_date)) { $timestamp = strtotime($losses->losses_from_date);  echo date('m/d/Y', $timestamp); } else { echo ''; } ?>"></td>                <td><input type="text" maxlength="10" id="date_to" class="span12 datepicker" pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)[0-9]{2,2}" name="losses_to[]" value="<?php if(isset($losses->losses_to_date)) { $timestamp = strtotime($losses->losses_to_date);  echo date('m/d/Y', $timestamp); } else { echo ''; } ?>"></td>                <td><input type="text" maxlength="15" class="span12 price_format" name="losses_liability_amount[]" value="<?php echo (isset($losses->liability_amount) ? $losses->liability_amount : ''); ?>" ></td>                <td><input type="text" maxlength="15" class="span12 price_format" name="losses_pd_amount[]" value="<?php echo (isset($losses->physical_damage_amount) ? $losses->physical_damage_amount : ''); ?>"></td>                <td><input type="text" maxlength="15" class="span12 price_format" name="losses_cargo_amount[]" value="<?php echo (isset($losses->cargo_amount) ? $losses->cargo_amount : ''); ?>" ></td>                        </tr> 
                                                </tbody>
    <?php } ?>
                                        </table>
                                    </div>
                                </div>
<?php }  else {?>
                            
<div class="row-fluid">  
							<div class="span12 lightblue">
								<table id="losses_table" class="table table-bordered table-striped table-hover" style="display:none" >
									<thead>
									  <tr>
										<th colspan="2" >Period of <br> losses</th>
										<th rowspan="2" >Liability <br> Amount</th>
										<th rowspan="2" >Physical Damage <br> Amount</th>
										<th rowspan="2" >Cargo <br> Amount </th>
									  </tr>
									  <tr colspan="2" >
										<th>From (mm/dd/yyyy)</th>
										<th>To (mm/dd/yyyy)</th>
									  </tr>
									</thead>
									<tbody id="add_losses_row_old" >
									</tbody>
								</table>
							</div>
						</div>


<?php  } ?>
--> 

                            <div class="row-fluid">  
                                <div class="span6 lightblue">
                                    <label class="loses_information_box" style="float:left;" >Attach Loss Report(s) for all Accident(s)</label>
                                    <!--span class="btn btn-file">Upload<input type="file" multiple="multiple" name="file_sec_1[]" id="file_sec_1" /></span-->
									<!-- input type="button" value="Upload" id="loss_report_upload1" class="btn btn-primary" style="cursor:pointer;" -->
									
									<!--<label class="cabinet label label-info" >
										<div id="upload_bottom1"><input type="hidden" id="upload_count1" value="1" ></div>
										<input onChange="add_upload_1(this.id);" type="file" name="file_sec_1[]" id="fileup1_1" class="upload_browse_multi1 file" title="Select file to upload" />
									</label>-->
									<input type="button" id="first_upload" value="Upload"/>
									

									<input type="hidden" name="uploaded_files_values" id="uploaded_files_values" value=""/>
									<ul id="upload_file_name1"></ul>
                                </div>
                                <div class="span6 lightblue"></div>
                            </div>
                            

                            <!-- Vehicle Schedule Section Start -->	
                            <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div>
                            <fieldset class="section2 vehicle_schedule_box">
                                <legend  >Vehicle schedule section</legend>

                                <div class="row-fluid">  
                                    <div class="span2 lightblue">Coverage : </div>  
                                    <div class="span6 lightblue">
                                        Liability <input class="coverage" required type="checkbox" id="vehicle_liability" name="vehicle_liability" <?php echo ((isset($quote->is_vehicle_liability) && $quote->is_vehicle_liability=='Y' ) ?  'checked="checked"' : ''); ?> onChange="update_option()" />
                                    </div>
                                </div>
                                <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div>
                                <div class="row-fluid " >  	
                                    <div class="span12 lightblue">
                                        <div class="span3">

                                            Number of vehicle/vehicles?
                                        </div>
                                        <div class="span2">
                                            <label>Tractor</label>
                                            <input required <?php echo $less_100_limit; ?> type="text"  name="vehicle_tractor_count" id="vehicle_tractor" onChange="add_tractor(this.value)" class="span4" value="<?php echo (!empty($vehicle))? get_vehicle_count($quote_id,'TRACTOR'):''; ?>" />
                                        </div> 

                                        <div class="span2">
                                            <label>Truck</label>
                                            <input required <?php echo $less_100_limit; ?> type="text" name="vehicle_truck_count" id="vehicle_truck" onChange="add_truck(this.value)" class="span4" value="<?php echo (!empty($vehicle))? get_vehicle_count($quote_id,'TRUCK'):''; ?>"  />
                                        </div> 
                                    </div>
                                </div>	
                                <?php if (!empty($vehicle)) {
                                    $vehicle11 = $vehicle;
                                    $trailer_count=1;
                                    ?> 
                                    <?php
                                    foreach ($vehicle as $vehicle) {  //echo $vehicle->radius_of_operation; die;
                                        // for($j=0; $j<count($vehicle); $j++) { echo $vehicle->adius_of_operation; die;

                                        ?>  

                                        <!--  FOR TRACTOR  -->
        <?php if ($vehicle->vehicle_type == 'TRACTOR') { ?>

                                            <div class="row_added_tractor vehicle_schedule"><div class="row-fluid ">                                        <div style="text-align:center; font-weight:bold;" class="span12 btn-success">                                        TRACTOR #<?php echo $trailer_count; ?></div><div class="row-fluid">                                                <div class="span12 lightblue">                                                        <div style="margin-top:10px;" class="span12" id="trailer_count">                                                               
											<label>How many trailers do you pull with this tractor?</label>   
												<select class="span3" name="vehicle_trailer_no[]" id="vehicle_trailer" onChange="add_trailer(this.value,<?php echo $trailer_count; ?>);">
													<option value="1" >Single</option>
													<option value="2" >Double</option>           	
													<option value="3">Triple</option>
												</select>											
											<!--input type="text" class="span1" name="vehicle_trailer_no[]" onchange="add_trailer(this.value,<?php echo $trailer_count; ?>)" id="vehicle_trailer" value="<?php echo (!empty($vehicle))? get_trailer_count($vehicle->vehicle_id):''; ?>" -->    
											</div>                                                 </div>                                         </div> 


                                                    <div class="row-fluid">  
                                                        <div class="span3 lightblue">
                                                            <label>Radius of Operation</label>
                                                            <select class="span12" name="tractor_radius_of_operation[]" id="radius_of_operation" onChange="rofoperation(this.value);" >
                                                                <option value="0-100 miles">0-100 miles</option>
                                                                <option value="101-500 miles" >101-500 miles</option>           	
                                                                <option value="501+ miles">501+ miles</option>
                                                              	<option value="other">Other</option>
                                                            </select>

                                                        </div>
														<!--MAYANK-->
														<div class="span4 lightblue">
                                                            <label>Any special request?</label>
															<input type="text" class="span12" name="tractor_request_for_radius_of_operation[]">
                                                            <!--select class="span12" name="tractor_request_for_radius_of_operation[]">
                                                                <option>One Time Out of State</option>
                                                                <option>Multiple States</option>
                                                                <option>Border Cross to Mexico</option>
                                                            </select-->
                                                        </div>
														<!--END MAYANK-->
                                                        <div class="span3 lightblue">
                                                            <label>Number of Axles</label>
															<?php echo $tractor_number_of_axles; ?>
                                                            <!--select class="span12" name="tractor_trailers[]" id="trailers">
                                                                <option>Single</option>
                                                                <option>Double</option>
                                                                <option>Triple</option>
                                                            </select-->
                                                        </div>

                                                    </div>	




                                                    <div class="row-fluid ">  	
                                                        <div class="span2 lightblue">

                                                            <label>Vehicle Year</label>

                                                            <select class="span12" name="tractor_vehicle_year_vh[]" id="vehicle_year_vh">
                                                                <option>1985</option>
                                                                <option>1986</option>
                                                                <option>1987</option>
                                                                <option>1988</option>
                                                                <option>1989</option>
																<option>1990</option>
                                                                <option>1991</option>
                                                                <option>1992</option>
                                                                <option>1993</option>
                                                                <option>1994</option>
                                                                <option>1995</option>
                                                                <option>1996</option>
                                                                <option>1997</option>
                                                                <option>1998</option>
                                                                <option>1999</option>
                                                                <option>2000</option>
                                                                <option>2001</option>
                                                                <option>2002</option>
                                                                <option>2003</option>
                                                                <option>2004</option>
                                                                <option>2005</option>
                                                                <option>2006</option>
                                                                <option>2007</option>
                                                                <option>2008</option>
                                                                <option>2009</option>
                                                                <option>2010</option>
                                                                <option>2011</option>
                                                                <option>2012</option>
                                                                <option>2013</option>
																<option>2014</option>
                                                            </select>
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>Make Model</label>
															<?php echo $make_model_tractor;?>
                                                            <!--select class="span12" name="tractor_make_model_vh[]" id="make_model_vh">
                                                                <option>Freightliner</option>
                                                                <option>International</option>
                                                                <option>Isuzu</option>
                                                                <option>Kenworth</option>
                                                                <option>Mack</option>
                                                                <option>Peterbilt</option>
                                                                <option>Sterling</option>
                                                                <option>Western Star</option>
                                                                <option>Other</option>
                                                            </select-->
                                                        </div>

                                                        <div class="span3 lightblue">
                                                            <label>GVW</label>

                                                            <select class="span12" name="tractor_gvw_vh[]" id="gvw_vh">
                                                                <option>0-10,000 pounds</option>
                                                                <option>10,001-26,000 pounds</option>
                                                                <option>26,001-40,000 pounds</option>
                                                                <option>40,001-80,000 pounds</option>
                                                                <option>Over 80,000 pounds</option>
                                                            </select>
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>VIN</label>
                                                            <input type="text"  maxlength="19" class="span12" name="tractor_vin_vh[]" id="vin_vh" value="<?php echo (isset($vehicle->vin) ? $vehicle->vin : ''); ?>">
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>Special req.</label>
                                                            <input type="text" class="span12 " name="tractor_value_amount_vh[]" id="value_amount_vh" value="<?php echo (isset($vehicle->vaule) ? $vehicle->vaule : ''); ?>" >
                                                        </div>



                                                    </div>
                                                </div><div class="row-fluid"><div id="option_div" class="span12">
                                                        <div class="span2 lightblue">
                                                            <label>Liability</label>
                                                            <select class="span12" name="tractor_liability_vh[]" id="tractor_liability_vh">
                                                                <option>$1,000,000</option>
                                                                <option>$750,000</option>
                                                                <option>$500,000</option>
                                                            </select>
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>Liability Ded</label>
                                                            <select class="span12" name="tractor_liability_ded_vh[]" id="tractor_liability_ded_vh">
                                                                <option>$1,000</option>
                                                                <option>$1,500</option>
                                                                <option>$2,000</option>
                                                                <option>$2,500</option>
                                                            </select>
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>PD</label>
                                                            <input type="text" class="span12 price_format" name="tractor_pd_vh[]" id="tractor_pd_vh" value="<?php echo (isset($vehicle->pd) ? $vehicle->pd : ''); ?>">
                                                        </div>
                                                        <div class="span2 lightblue">
                                                            <label>PD Ded</label>
                                                            <input type="text" class="span12 price_format" name="tractor_ph_ded_vh[]" id="tractor_ph_ded_vh" value="<?php echo (isset($vehicle->pd_ded) ? $vehicle->pd_ded : ''); ?>">
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>Cargo</label>
                                                            <input type="text" class="span12 price_format" name="tractor_cargo_vh[]" id="tractor_cargo_vh" value="<?php echo (isset($vehicle->cargo) ? $vehicle->cargo : ''); ?>">
                                                        </div>
                                                        <div class="span2 lightblue">
                                                            <label>Cargo Ded</label>
                                                            <input type="text" class="span12 price_format" name="tractor_cargo_ded_vh[]" id="tractor_cargo_ded_vh" value="<?php echo (isset($vehicle->cargo_ded) ? $vehicle->cargo_ded : ''); ?>">
                                                        </div>
                                                    </div></div><!--div class="row-fluid ">                                         <div class="span3 lightblue">                                         Refrigerated break down:                                         <input type="checkbox" onclick="check_deduct(this.id) " id="tractor_refrigerated_breakdown1" name="tractor_refrigerated_breakdown[]"></div>                                         <div style="display:none;" class="tractor_refrigerated_breakdown1">Deductible : <input type="text" id="deductible" name="tractor_deductible[]" class="span2 price_format"></div></div-->
            <?php foreach ($vehicle11 as $vehicle1) {
                if ($vehicle1->vehicle_type == 'TRAILER' && $vehicle->vehicle_id == $vehicle1->parent_id) { ?>


                                                        <div class="row_added_trailer1 vehicle_schedule"><div class="row-fluid ">                                        <div style="text-align:center; font-weight:bold; background-color:black; color:white;" class="span12">                                        TRAILER #1</div>

                                                                <div class="row-fluid ">  	
                                                                    <div class="span2 lightblue">

                                                                        <label>Vehicle Year</label>

                                                                        <select class="span12" name="trailer_vehicle_year_vh[]" id="vehicle_year_vh">
                                                                             <option>1985</option>
																			<option>1986</option>
																			<option>1987</option>
																			<option>1988</option>
																			<option>1989</option>
																			<option>1990</option>
                                                                            <option>1991</option>
                                                                            <option>1992</option>
                                                                            <option>1993</option>
                                                                            <option>1994</option>
                                                                            <option>1995</option>
                                                                            <option>1996</option>
                                                                            <option>1997</option>
                                                                            <option>1998</option>
                                                                            <option>1999</option>
                                                                            <option>2000</option>
                                                                            <option>2001</option>
                                                                            <option>2002</option>
                                                                            <option>2003</option>
                                                                            <option>2004</option>
                                                                            <option>2005</option>
                                                                            <option>2006</option>
                                                                            <option>2007</option>
                                                                            <option>2008</option>
                                                                            <option>2009</option>
                                                                            <option>2010</option>
                                                                            <option>2011</option>
                                                                            <option>2012</option>
                                                                            <option>2013</option>
																			<option>2014</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="span2 lightblue">
                                                                        <label>Make Model</label>
																		<?php echo $make_model_trailer;?>
                                                                        <!--select class="span12" name="trailer_make_model_vh[]" id="make_model_vh">
                                                                            <option value="Freahauf">Freahauf</option>
                                                                            <option value="Greate Dane" >Greate Dane</option>
                                                                            <option value="Stoughton" >Stoughton</option>
                                                                            <option value="Trailmobile" >Trailmobile</option>
                                                                            <option value="Utility" >Utility</option>
                                                                            <option value="Wabash" >Wabash</option>
                                                                            <option value="Other" >Other</option>
                                                                        </select-->
                                                                    </div>

                                                                    <div class="span3 lightblue">
                                                                        <label>GVW</label>

                                                                        <select class="span12" name="trailer_gvw_vh[]" id="gvw_vh">
                                                                            <option>0-10,000 pounds</option>
                                                                            <option>10,001-26,000 pounds</option>
                                                                            <option>26,001-40,000 pounds</option>
                                                                            <option>40,001-80,000 pounds</option>
                                                                            <option>Over 80,000 pounds</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="span2 lightblue">
                                                                        <label>VIN</label>
                                                                        <input type="text"  maxlength="19" class="span12" name="trailer_vin_vh[]" id="vin_vh" value="<?php echo (isset($vehicle1->vin) ? $vehicle1->vin : ''); ?>">
                                                                    </div>
																	
																	
																	
                                                                    <div id="trailer_special_req_div" class="span2 lightblue tractor_special_req_div trailer_special_req_div">
                                                                        <label>Special req.</label>
                                                                        <input type="text" class="span12 " name="trailer_value_amount_vh[]" id="value_amount_vh" value="<?php echo (isset($vehicle1->value) ? $vehicle1->value : ''); ?>">
                                                                    </div>



                                                                </div>
                                                                <div class="row-fluid ">  	
                                                                    <div class="span3 lightblue">
                                                                        Trailer Type
                                                                        <select class="span6" name="trailer_type[]" type="text">
                                                                            <option value="">Select</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div><div class="row-fluid"><div id="option_div" class="span12">
                                                                    <div class="span2 lightblue">
                                                                        <label>Liability</label>
                                                                        <select class="span12" name="trailer_liability_vh[]" id="trailer_liability_vh">
                                                                            <option>$1,000,000</option>
                                                                            <option>$750,000</option>
                                                                            <option>$500,000</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="span2 lightblue">
                                                                        <label>Liability Ded</label>
                                                                        <select class="span12" name="trailer_liability_ded_vh[]" id="trailer_liability_ded_vh">
                                                                            <option>$1,000</option>
                                                                            <option>$1,500</option>
                                                                            <option>$2,000</option>
                                                                            <option>$2,500</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="span2 lightblue">
                                                                        <label>PD</label>
                                                                        <input type="text" class="span12" name="trailer_pd_vh[]" id="trailer_pd_vh" value="<?php echo (isset($vehicle1->pd) ? $vehicle1->pd : ''); ?>">
                                                                    </div>
                                                                    <div class="span2 lightblue">
                                                                        <label>PD Ded</label>
                                                                        <input type="text" class="span12 price_format" name="trailer_ph_ded_vh[]" id="trailer_ph_ded_vh" value="<?php echo (isset($vehicle1->pd_ded) ? $vehicle1->pd_ded : ''); ?>">
                                                                    </div>

                                                                    <div class="span2 lightblue">
                                                                        <label>Cargo</label>
                                                                        <input type="text" class="span12" name="trailer_cargo_vh[]" id="trailer_cargo_vh" value="<?php echo (isset($vehicle1->cargo) ? $vehicle1->cargo : ''); ?>">
                                                                    </div>
                                                                    <div class="span2 lightblue">
                                                                        <label>Cargo Ded</label>
                                                                        <input type="text" class="span12 price_format" name="trailer_cargo_ded_vh[]" id="trailer_cargo_ded_vh" value="<?php echo (isset($vehicle1->cargo_ded) ? $vehicle1->cargo_ded : ''); ?>">
                                                                    </div>
                                                                </div></div><div class="row-fluid ">                                         <div class="span3 lightblue">                                         Refrigeration breakdown:                                         <input type="checkbox" onClick="check_deduct(this.id) " id="trailer_refrigerated_breakdown1" name="trailer_refrigerated_breakdown[]"  <?php echo ((isset($vehicle1->refrigerated_break_down) && $vehicle1->refrigerated_break_down=='Y' ) ?  'checked="checked"' : ''); ?> ></div>                                         <div style="display:none;" class="trailer_refrigerated_breakdown1">Deductible : <input type="text" id="deductible" name="trailer_deductible[]" class="span2 price_format"  value="<?php echo (isset($vehicle1->break_down_deductible) ? $vehicle1->break_down_deductible : ''); ?>" ></div></div>
                                                        </div>




                <?php }
            } ?>
                                                <span id="add_trailer_div<?php echo $trailer_count; ?>"></span>

                                            </div>


        <?php $trailer_count++;  } ?>
                                        <!--  FOR TRUCK   -->
        <?php if ($vehicle->vehicle_type == 'TRUCK') { ?>
                                            <div class="row_added_vehicle vehicle_schedule row_added_truck"><div class="row-fluid ">				<div style="text-align:center; font-weight:bold;" class="span12 btn-primary">				TRUCK #1</div>

                                                    <div class="row-fluid">  
                                                        <div class="span3 lightblue">
                                                            <label>Radius of Operation</label>
                                                            <select class="span12 truck_radius_of_operation" name="truck_radius_of_operation[]" id="radius_of_operation" onChange="rofoperation2(this.value);" >
                                                                <option value="0-100 miles">0-100 miles</option>
                                                                <option value="101-500 miles" >101-500 miles</option>           	
                                                                <option value="501+ miles">501+ miles</option>
                                                              	<option value="other">Other</option>
                                                            </select>

                                                        </div>
														<!--MAYANK-->
														<div class="span4 lightblue">
                                                           <label>Any special request?</label>
															<input type="text" class="span12" name="truck_request_for_radius_of_operation[]">
                                                        </div>
														<!--END MAYANK-->
                                                        <div class="span3 lightblue">
                                                            <label>Number of Axles</label>
															<?php echo $truck_number_of_axles; ?>
                                                            <!--select class="span12" name="trailers[]" id="trailers">
                                                                <option>Single</option>
                                                                <option>Double</option>
                                                                <option>Triple</option>
                                                            </select-->
                                                        </div>

                                                    </div>	




                                                    <div class="row-fluid ">  	
                                                        <div class="span2 lightblue">

                                                            <label>Vehicle Year</label>

                                                            <select class="span12" name="vehicle_year_vh[]" id="vehicle_year_vh">
                                                                 <option>1985</option>
                                                                <option>1986</option>
                                                                <option>1987</option>
                                                                <option>1988</option>
                                                                <option>1989</option>
																<option>1990</option>
                                                                <option>1991</option>
                                                                <option>1992</option>
                                                                <option>1993</option>
                                                                <option>1994</option>
                                                                <option>1995</option>
                                                                <option>1996</option>
                                                                <option>1997</option>
                                                                <option>1998</option>
                                                                <option>1999</option>
                                                                <option>2000</option>
                                                                <option>2001</option>
                                                                <option>2002</option>
                                                                <option>2003</option>
                                                                <option>2004</option>
                                                                <option>2005</option>
                                                                <option>2006</option>
                                                                <option>2007</option>
                                                                <option>2008</option>
                                                                <option>2009</option>
                                                                <option>2010</option>
                                                                <option>2011</option>
                                                                <option>2012</option>
                                                                <option>2013</option>
																<option>2014</option>
                                                            </select>
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>Make Model</label>
																<?php echo $make_model_truck;?>
                                                            <!--select class="span12" name="make_model_vh[]" id="make_model_vh">
                                                                <option>Freightliner</option>
                                                                <option>International</option>
                                                                <option>Isuzu</option>
                                                                <option>Kenworth</option>
                                                                <option>Mack</option>
                                                                <option>Peterbilt</option>
                                                                <option>Sterling</option>
                                                                <option>Western Star</option>
                                                                <option>Other</option>
                                                            </select-->
                                                        </div>

                                                        <div class="span3 lightblue">
                                                            <label>GVW</label>

                                                            <select class="span12" name="gvw_vh[]" id="gvw_vh">
                                                                <option>0-10,000 pounds</option>
                                                                <option>10,001-26,000 pounds</option>
                                                                <option>26,001-40,000 pounds</option>
                                                                <option>40,001-80,000 pounds</option>
                                                                <option>Over 80,000 pounds</option>
                                                            </select>
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>VIN</label>
                                                            <input type="text" maxlength="19" class="span12" name="vin_vh[]" id="vin_vh" value="<?php echo (isset($vehicle->vin) ? $vehicle->vin : ''); ?>">
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>Special req.</label>
                                                            <input type="text" class="span12 " name="value_amount_vh[]" id="value_amount_vh" value="<?php echo (isset($vehicle->vaule) ? $vehicle->vaule : ''); ?>">
                                                        </div>



                                                    </div>
                                                </div>

                                                <div class="row-fluid"><div id="option_div" class="span12">
                                                        <div class="span2 lightblue">
                                                            <label>Liability</label>
                                                            <select class="span12" name="liability_vh[]" id="liability_vh">
                                                                <option>$1,000,000</option>
                                                                <option>$750,000</option>
                                                                <option>$500,000</option>
                                                            </select>
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>Liability Ded</label>
                                                            <select class="span12" name="liability_ded_vh[]" id="liability_ded_vh">
                                                                <option>$1,000</option>
                                                                <option>$1,500</option>
                                                                <option>$2,000</option>
                                                                <option>$2,500</option>
                                                            </select>
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>PD</label>
                                                            <input type="text" class="span12 price_format" name="pd_vh[]" id="pd_vh" value="<?php echo (isset($vehicle->pd) ? $vehicle->pd : ''); ?>">
                                                        </div>
                                                        <div class="span2 lightblue">
                                                            <label>PD Ded</label>
                                                            <input type="text" class="span12 price_format" name="ph_ded_vh[]" id="ph_ded_vh" value="<?php echo (isset($vehicle->pd_ded) ? $vehicle->pd_ded : ''); ?>">
                                                        </div>

                                                        <div class="span2 lightblue">
                                                            <label>Cargo</label>
                                                            <input type="text" class="span12 price_format" name="cargo_vh[]" id="cargo_vh" value="<?php echo (isset($vehicle->cargo) ? $vehicle->cargo : ''); ?>">
                                                        </div>
                                                        <div class="span2 lightblue">
                                                            <label>Cargo Ded</label>
                                                            <input type="text" class="span12 price_format" name="cargo_ded_vh[]" id="cargo_ded_vh" value="<?php echo (isset($vehicle->cargo_ded) ? $vehicle->cargo_ded : ''); ?>">
                                                        </div>
                                                    </div></div>



                                                <!--div class="row-fluid "> 				<div class="span3 lightblue"> 				Refrigerated break down: 				<input type="checkbox" onclick="check_deduct(this.id) " id="refrigerated_breakdown1" name="refrigerated_breakdown[]" <?php echo ((isset($vehicle->refrigerated_break_down) && $vehicle->refrigerated_break_down=='Y' ) ?  'checked="checked"' : ''); ?> ></div> 				<div style="display:none;" class="refrigerated_breakdown1">Deductible : <input type="text" id="deductible" name="deductible[]" class="span2 price_format"  value="<?php echo (isset($vehicle->break_down_deductible) ? $vehicle->break_down_deductible : ''); ?>"  ></div></div-->
                                            </div>

        <?php } ?>

    <?php }
}
?> 
                                <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div> 




                                <!--div class="row-fluid " >  	
                                <div class="span12 lightblue">
                                <label>How Many Vehicles to Schedule ?</label>
                                <input type="text" onchange="add_vehicle(this.value)" class="span1" />
                                </div>
                                </div-->

                                <span id="add_vehicle_div"></span>


                                <span id="add_tractor_div"></span>


                          

                            <!-- Vehicle Schedule Section End -->	


                            <!-- Trailer Schedule Section Start -->	




                            <div class="row-fluid"> <span style="height:5px">&nbsp;</span> </div>

                            <!--div class="row-fluid">  
                                <div class="span6 lightblue">
                                    <label >Owner Driven</label>

                                    <select id="owner_driven_tr" name="owner_driven_tr" >
                                        <option value="Y">Yes</option>
                                        <option value="N">No</option>
                                    </select>
                                </div>
                                <div class="span6 lightblue"></div>
                            </div-->

                            <div class="row-fluid">  
                                <div class="span12 lightblue">
                                    <label style="float:left">Attach MVR's for all Driver(s) and Owner(s) no more than 30 days old</label>
									
									
									
		<!--input type="button" value="Upload" id="mvr_upload" class="btn btn-primary" style="cursor:pointer;" -->
		<!--<label class="cabinet label label-info" >
			<div id="upload_bottom"><input type="hidden" id="upload_count" value="1" ></div>
			<input  style="cursor:pointer" onChange="add_upload_3(this.id);" type="file" name="file_sec_3[]" id="fileup_1" class="upload_browse_multi file" title="Select file to upload" />
		</label>-->
									<input type="button" id="first_upload_second" value="Upload"/>
									

									<input type="hidden" name="uploaded_files_values_second" id="uploaded_files_values_second" value=""/>
									<ul id="upload_file_name_second"></ul>        
									
									<!--span class="label label-info" style="width:42px; margin-bottom:5px; cursor:pointer;" > Upload
									<input  style="cursor:pointer" onchange="add_upload_3(this.id);" type="file" name="file_sec_3[]" id="fileup_1" class="upload_browse_multi file" title="Select file to upload" />
																	
									<div id="upload_bottom"><input type="hidden" id="upload_count" value="1" ></div>
									</span-->
									
									<div id="upload_file_name"></div>
									
									
								    <!--span class="btn btn-file">Upload<input type="file" multiple="multiple" name="file_sec_3[]" id="file_sec_3" /></span-->
                                    <!--MAYANK-->
									<label><b>Disclaimer :</b> Please attach the MVR for each driver, otherwise the quote is an indication only and not a formal quote. </label>
									<!--END MAYANK -->
                                </div>
                            </div>

                            </fieldset>


                            <fieldset id="section4" class="schedule_of_drivers_box">
                                <legend >Schedule of Drivers</legend>

                                <div class="row-fluid " >  	
                                    <div class="span12 lightblue">
                                        <label>Number of drivers ?</label>
                                        <input <?php echo $less_100_limit; ?> required type="text" id="num_of_drivers" onChange="add_driver(this.value)" name="driver_count" class="span1"  value="<?php echo (!empty($drivers))? count($drivers):''; ?>"/>
                                    </div>
                                </div>

<?php if (!empty($drivers)) { ?> 
                                    <div id="row_driver" >
                                        <div class="row-fluid">  
                                            <div class="span12 lightblue">
                                                <table id="driver_table" class="table table-bordered table-striped table-hover" >
                                                    <thead>
                                                        <tr>
                                                            <th >Name</th>
                                                            <th >Driver License number</th>

                                                        </tr>

                                                    </thead>
                                                    <tbody id="add_driver_row">
    <?php
    foreach ($drivers as $drivers) {
        //  for($i=0; $i<count($losses); $i++) { 
        ?>   
<tr>
<td><input type="text" maxlength="20" class="span12 first_name" name="driver_name[]" value="<?php echo (isset($drivers->driver_name) ? $drivers->driver_name : ''); ?>"></td>   
<td><input type="text" maxlength="20" class="span12 middle_name" name="driver_middle_name[]" value="<?php echo (isset($drivers->driver_middle_name) ? $drivers->driver_middle_name : ''); ?>"></td>   
<td><input type="text" maxlength="20" class="span12 last_name" name="driver_last_name[]" value="<?php echo (isset($drivers->driver_last_name) ? $drivers->driver_last_name : ''); ?>"></td>   
	<td><input type="text" maxlength="20" class="span12" name="driver_license[]" value="<?php echo (isset($drivers->driver_licence_no) ? $drivers->driver_licence_no : ''); ?>" ></td> 
	</tr> 

    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
<?php } else { ?>
                                
                                <div id="row_driver" style="display:none;">
						<div class="row-fluid">  
							<div class="span12 lightblue">
								<table id="driver_table" class="table table-bordered table-striped table-hover" >
									<thead>
									  <tr>
										<th >First Name</th>
										<th >Middle Name</th>
										<th >Last Name</th>
										<th >Driver License number</th>
										<th >License Class</th>
										<th >Date Hired</th>
										<th >DOB</th>
										<th >How Many Years Class A license</th>
										<th >License issue state</th>
									
									  </tr>
									  
									</thead>
									<tbody id="add_driver_row" >
									</tbody>
								</table>
							</div>
						</div>
					</div>
                                <?php } ?>
                                <!--span id="add_driver_div"></span-->

                            </fieldset>

                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                         
                           <fieldset id="section4" class="number_of_owner_box" >
                                <legend >Number of Owner</legend>

                                <div class="row-fluid " >  	
                                    <div class="span12 lightblue">
                                        <!--MAYANK-->
                                        <label>Number of owner of the vehicle or entity?</label>
										<!--END MAYANK-->
                                        <input required <?php echo $less_100_limit; ?> type="text" id="number_of_owner" name="number_of_owner" onChange="add_owner(this.value)" class="span1"  value="<?php echo (!empty($owner))? count($owner):''; ?>"/>
                                    </div>
                                </div>

<?php if (!empty($owner)) { ?> 

                                    <div id="row_owner">
                                        <div class="row-fluid">  
                                            <div class="span12 lightblue">
                                                <table id="owner_table" class="table table-bordered table-striped table-hover" >
                                                    <thead>
                                                        <tr>
                                                            <th >First Name</th>
                                                            <th >Middle Name</th>
                                                            <th >Last Name</th>
                                                            <th >Driver ?</th>
                                                            <th >Driver License number</th>

                                                        </tr>

                                                    </thead> 
                                                    <tbody id="add_owner_row">
    <?php foreach ($owner as $owner) { ?>
 <tr>
 <td><input type="text" maxlength="20" class="span12 first_name" name="owner_name[]" value="<?php echo (isset($owner->owner_name) ? $owner->owner_name : ''); ?>"></td>
 <td><input type="text" maxlength="20" class="span12 middle_name" name="owner_middle_name[]" value="<?php echo (isset($owner->owner_middle_name) ? $owner->owner_middle_name : ''); ?>"></td>
 <td><input type="text" maxlength="20" class="span12 last_name" name="owner_last_name[]" value="<?php echo (isset($owner->owner_last_name) ? $owner->owner_last_name : ''); ?>"></td>
															
															<td colspan="2">
															<select name="owner_is_driver[]" class="span12" >
																<option value="">SELECT</option>
																<option value="Y" <?php echo (isset($owner->is_driver) && $owner->is_driver == 'Y' ? 'selected="selected"' : ''); ?> >Yes</option>
																<option value="N" <?php echo (isset($owner->is_driver) && $owner->is_driver == 'N' ? 'selected="selected"' : ''); ?> >NO</option>
															</select>
															</td> 
															<td><input type="text" maxlength="20" class="span12" name="owner_license[]" value="<?php echo (isset($owner->driver_licence_no) ? $owner->driver_licence_no : ''); ?>"></td>                                 </tr>

    <?php } ?>  
                                                    </tbody> 
                                                </table>
                                            </div>
                                        </div>
                                    </div>
<?php } else { ?>  

                                    <div id="row_owner"  style="display:none;" >
                                        <div class="row-fluid">  
                                            <div class="span12 lightblue">
                                                <table id="owner_table" class="table table-bordered table-striped table-hover" >
                                                    <thead>
                                                        <tr>
                                                            <th >First Name</th>
                                                            <th >Middle Name</th>
                                                            <th >Last Name</th>
                                                            <th > Driver ?</th>
                                                            <th >Driver License number</th>
                                                            <th >License issue state</th>

                                                        </tr>

                                                    </thead>
                                                    <tbody id="add_owner_row" >
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> 

<?php } ?>
                                <!--span id="add_driver_div"></span-->

                            </fieldset>



                       
                       
                       
                       
                       
                       
                       
                            <fieldset id="section5" class="fillings_section_box">
                                <legend >Filing Section</legend>
						<!--MAYANK-->
<?php if (!empty($fillings)) { ?> 
					<div class="row-fluid">	
							<div class="span12 lightblue">
								<label>Do you need Filing / Filings :</label>
								<select id="filings_need" name="filings_need">
									<option value="yes" <?php echo 'selected="selected"';?>>YES</option>
									<option value="no">NO</option>
								</select>
							</div>
							<div class="span12 lightblue" id="number_of_filings_div">
								<label>Number of Filings :</label>
								<input type="text" <?php echo $less_100_limit; ?>  id="number_of_filings" name="number_of_filings" class="span1 filing_numbers" value="<?php echo count($fillings);?>" />
							</div>
							<div class="row-fluid">  
								<div class="span6 lightblue">
									<table id="filing_table" class="table table-bordered table-striped table-hover" >
										<thead>
											<tr>
												<th >Filing Type:</th>
												<th >Filing Num</th>
											</tr>
										</thead>
										<tbody id="add_filing_row" >						
    <?php foreach ($fillings as $fillings) { ?>
											<tr>
												<td>
													<input type="text" maxlength="20" class="span12" name="filing_type[]" value="<?php echo (isset($fillings->filings_type) ? $fillings->filings_type : ''); ?>">
												</td>
												<td>
													<input type="text" maxlength="20" class="span12" name="filing_numbers[]" value="<?php echo (isset($fillings->filing_no) ? $fillings->filing_no : ''); ?>">
												</td>
											</tr>
    <?php }
	?>								</tbody>
                                </table>
                            </div>
                        </div>
                    </div> 
<?php } else { ?>  
                                    <div class="row-fluid">  
                                        
										<script type="text/javascript">
											$(document).ready(function(){
												$("#filings_need").change(function(){
													if($(this).val()=="yes") {
														$("#number_of_filings_div").show();
														$('#number_of_filings').attr('required', 'required'); 
														
													}else{
														$("#number_of_filings_div").hide();
														$('#number_of_filings').removeAttr('required'); 
														}
												});
											});
											
											function add_filing(val){
													if(val >=0 ){
														if(val == 0)
														{
															$("#filing_table").hide();
														}
														//$("#filing_table").show();
														$("#row_filing").show();
														//$("#add_filing_row").html('');
														var i =1;
														var row = '<tr>\
														<td><input type="text" name="filing_type[]" class="span12 filing_type" required="required" /></td> \
														<td><input type="text" name="filing_numbers[]" class="span12 filing_numbers"  required="required" /></td> \
														</tr>';
														var currcount = $("#add_filing_row tr").length;
														var diff = currcount - val;
														
														 if(diff < 0) {
																while(diff < 0){
																	var v = $('#add_filing_row').append(row) ; 
																	diff++;
																	
																} 
															 }
															 else if(diff > 0)
															 {
																
																 while(diff > 0){
																
																	 var v = $('#add_filing_row tr:last').remove() ; 
																	 diff--;
																 }
															 }	
															
														apply_on_all_elements();
													}
												}
										</script>
										<div class="span12 lightblue">
											<label>Do you need Filing / Filings :</label>
											<select id="filings_need" name="filings_need"  required="required"  >
												<option value="">--Select--</option>
												<option value="yes">YES</option>
												<option value="no">NO</option>
											</select>
                                        </div>
										
										<div class="span12 lightblue" id="number_of_filings_div" style="display:none;">
											<label>Number of Filings :</label>
											<input type="text" <?php echo $less_100_limit; ?> id="number_of_filings" name="number_of_filings" class="span1 filing_numbers" onChange="add_filing(this.value)" />
										</div>
										
										<div id="row_filing"  style="display:none;" >
											<div class="row-fluid">  
												<div class="span6 lightblue">
													<table id="filing_table" class="table table-bordered table-striped table-hover" >
														<thead>
															<tr>
																<th >Filing Type:</th>
																<th >Filing Num.</th>
															</tr>
														</thead>
														<tbody id="add_filing_row" >
														</tbody>
													</table>
												</div>
											</div>
										</div> 
									</div>							
		<?php } ?>
                                <!--END OF MAYANK-->


                            </fieldset>

				

                            <div class="row-fluid">  
                                <div class="span lightblue">
                                    <div class="form-actions">
                                        <!--button class="btn btn-primary" type="submit" onclick="return check_vehicle_entry();">Submit Quote</button-->
                                        <button class="btn btn-primary" type="submit" onClick="form_sub()" >Submit Quote</button>
									
                                        <button class="btn" type="reset">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
<?php echo form_close(); ?>

	<div style="display:none;">
		<form id="upload" method="post" action="<?php echo base_url();?>/upload.php" enctype="multipart/form-data">
			<div id="drop">
				Drop Here

				<a>Browse</a>
				<input type="file" name="upl" multiple />
			</div>

			<ul>
				<!-- The file uploads will be shown here -->
			</ul>

		</form>
        
        
        <form id="upload_second" method="post" action="<?php echo base_url('upload.php');?>" enctype="multipart/form-data">
			<div id="drop_second">
				Drop Here

				<a>Browse</a>
				<input type="file" name="upl" multiple />
			</div>

			<ul>
				<!-- The file uploads will be shown here -->
			</ul>

		</form>
     </div>   
        <script src="<?php echo base_url('js');?>/jquery.knob.js"></script>

		<!-- jQuery File Upload Dependencies -->
		<script src="<?php echo base_url('js');?>/jquery.ui.widget.js"></script>
		<script src="<?php echo base_url('js');?>/jquery.iframe-transport.js"></script>
		<script src="<?php echo base_url('js');?>/jquery.fileupload.js"></script>
		
		<!-- Our main JS file -->
		<script src="<?php echo base_url('js');?>/script.js"></script>
	
        </div>	

        <!-- truck Vehicle Row -->

        <div id="row_vehicle" style="display:none; ">

            <div class="row-fluid">  
                <div class="span3 lightblue">
                    <label>Radius of Operation</label>
					<select class="span12 truck_radius_of_operation" name="truck_radius_of_operation[]" id="truck_radius_of_operation" onChange="rofoperation2(this.value, this.id);" >
						<option value="0-100 miles">0-100 miles</option>
						<option value="101-500 miles" >101-500 miles</option>           	
						<option value="501+ miles">501+ miles</option>
						<option value="other">Other</option>
					</select>

                </div>
				
		 <div class="count_truck_radius_of_operation span3 lightblue show_truck_others" style="display:none;" >
			<label>Specify Other</label>
			<input type="text" name="truck_radius_of_operation_other[]" class="specify_other_cp2" />
		 </div>
		 
				<!--MAYANK-->
				<div class="span3 lightblue">
					<label>Any special request?</label>
					<input type="text" class="span12" name="truck_request_for_radius_of_operation[]">
				</div>
				<!--END OF MAYANK-->
                <div class="span3 lightblue">
                    <label>Number of Axles</label>
					<?php echo $truck_number_of_axles; ?>
                    <!--select id="trailers" name="trailers[]" class="span12" >
                        <option>Single</option>
                        <option>Double</option>
                        <option>Triple</option>
                    </select-->
                </div>

            </div>	




            <div class="row-fluid " >  	
                <div class="span2 lightblue">

                    <label>Vehicle Year</label>

                    <select id="vehicle_year_vh" name="vehicle_year_vh[]"  class="span12" >
                         <option>1985</option>
						<option>1986</option>
						<option>1987</option>
						<option>1988</option>
						<option>1989</option>
						<option>1990</option>
                        <option>1991</option>
                        <option>1992</option>
                        <option>1993</option>
                        <option>1994</option>
                        <option>1995</option>
                        <option>1996</option>
                        <option>1997</option>
                        <option>1998</option>
                        <option>1999</option>
                        <option>2000</option>
                        <option>2001</option>
                        <option>2002</option>
                        <option>2003</option>
                        <option>2004</option>
                        <option>2005</option>
                        <option>2006</option>
                        <option>2007</option>
                        <option>2008</option>
                        <option>2009</option>
                        <option>2010</option>
                        <option>2011</option>
                        <option>2012</option>
                        <option>2013</option>
						<option>2014</option>
                    </select>
                </div>

                <div class="span2 lightblue">
                    <label>Make Model</label>
					<?php echo $make_model_truck;?>
                    <!--select id="make_model_vh" name="make_model_vh[]"  class="span12" >
                        <option>Freightliner</option>
                        <option>International</option>
                        <option>Isuzu</option>
                        <option>Kenworth</option>
                        <option>Mack</option>
                        <option>Peterbilt</option>
                        <option>Sterling</option>
                        <option>Western Star</option>
                        <option>Other</option>
                    </select-->
                </div>

                <div class="span3 lightblue">
                    <label>GVW</label>

                    <select id="gvw_vh" name="gvw_vh[]" class="span12" >
                        <option>0-10,000 pounds</option>
                        <option>10,001-26,000 pounds</option>
                        <option>26,001-40,000 pounds</option>
                        <option>40,001-80,000 pounds</option>
                        <option>Over 80,000 pounds</option>
                    </select>
                </div>

                <div class="span2 lightblue">
                    <label>VIN</label>
                    <input id="vin_vh" maxlength="19" name="vin_vh[]" type="text" class="span12"  >
                </div>

                <div id="truck_special_req_div" class="span2 lightblue value_amount_vh_div truck_special_req_div" style="display:none;">
					
						<div style="margin-left:auto;"></div>
                    <label>Special req.</label>
                    <input id="value_amount_vh" name="value_amount_vh[]" type="text" class="span12 " />
					
					
                </div>
            </div>
            
        </div>


</div>			

<!-- tractor Vehicle Row -->

<div id="row_tractor" style="display:none; ">


    <div class="row-fluid">  
        <div class="span3 lightblue">
             <label>Radius of Operation</label>
			<select class="span12 tractor_radius_of_operation " name="tractor_radius_of_operation[]" id="tractor_radius_of_operation" onChange="rofoperation(this.value, this.id);" >
				<option value="0-100 miles">0-100 miles</option>
				<option value="101-500 miles" >101-500 miles</option>           	
				<option value="501+ miles">501+ miles</option>
				<option value="other">Other</option>
			</select>

        </div> 
		 <div class="count_tractor_radius_of_operation span3 lightblue show_tractor_others" style="display:none;" >
		  <label>Specify Other</label>
		 <input type="text" name="tractor_radius_of_operation_other[]" class="specify_other_cp" />
		 </div>
		<!--MAYANK-->
		<div class="span3 lightblue">
			<label>Any special request?</label>
			<input type="text" class="span12" name="tractor_request_for_radius_of_operation[]">
		</div>
		<!--END MAYANK-->
        <div class="span3 lightblue">
            <label>Number of Axles</label>
			<?php echo $tractor_number_of_axles; ?>
            <!--select id="trailers" name="tractor_trailers[]" class="span12" >
                <option>Single</option>
                <option>Double</option>
                <option>Triple</option>
            </select-->
        </div>
    </div>	




    <div class="row-fluid " >  	
        <div class="span2 lightblue">

            <label>Vehicle Year</label>

            <select id="vehicle_year_vh" name="tractor_vehicle_year_vh[]"  class="span12" >
                 <option>1985</option>
				<option>1986</option>
				<option>1987</option>
				<option>1988</option>
				<option>1989</option>
				<option>1990</option>
                <option>1991</option>
                <option>1992</option>
                <option>1993</option>
                <option>1994</option>
                <option>1995</option>
                <option>1996</option>
                <option>1997</option>
                <option>1998</option>
                <option>1999</option>
                <option>2000</option>
                <option>2001</option>
                <option>2002</option>
                <option>2003</option>
                <option>2004</option>
                <option>2005</option>
                <option>2006</option>
                <option>2007</option>
                <option>2008</option>
                <option>2009</option>
                <option>2010</option>
                <option>2011</option>
                <option>2012</option>
                <option>2013</option>
				<option>2014</option>
            </select>
        </div>

        <div class="span2 lightblue">
            <label>Make Model</label>
				<?php echo $make_model_tractor;?>
            <!--select id="make_model_vh" name="tractor_make_model_vh[]"  class="span12" >
                <option>Freightliner</option>
                <option>International</option>
                <option>Isuzu</option>
                <option>Kenworth</option>
                <option>Mack</option>
                <option>Peterbilt</option>
                <option>Sterling</option>
                <option>Western Star</option>
                <option>Other</option>
            </select-->
        </div>

        <div class="span3 lightblue">
            <label>GVW</label>

            <select id="gvw_vh" name="tractor_gvw_vh[]" class="span12" >
                <option>0-10,000 pounds</option>
                <option>10,001-26,000 pounds</option>
                <option>26,001-40,000 pounds</option>
                <option>40,001-80,000 pounds</option>
                <option>Over 80,000 pounds</option>
            </select>
        </div>

        <div class="span2 lightblue">
            <label>VIN</label>
            <input id="vin_vh" maxlength="19" name="tractor_vin_vh[]" type="text" class="span12"  >
        </div>

        <div id="tractor_special_req_div" class="span2 lightblue tractor_special_req_div" style="display:none" >
			<div style="margin-left:auto;"></div>
            <label>Special req.</label>
            <input id="value_amount_vh" name="tractor_value_amount_vh[]" type="text" class="span12 "/>			
        </div>



    </div>
</div>
<!-- END Tractor Row --> 


<!-- Trailer Row -->

<div id="row_trailer" style="display:none; ">

    <div class="row-fluid " >  	
        <div class="span2 lightblue">

            <label>Vehicle Year</label>

            <select id="vehicle_year_vh__SECID__" name="trailer_vehicle_year_vh[]"  class="span12" >
                 <option>1985</option>
				<option>1986</option>
				<option>1987</option>
				<option>1988</option>
				<option>1989</option>
				<option>1990</option>
                <option>1991</option>
                <option>1992</option>
                <option>1993</option>
                <option>1994</option>
                <option>1995</option>
                <option>1996</option>
                <option>1997</option>
                <option>1998</option>
                <option>1999</option>
                <option>2000</option>
                <option>2001</option>
                <option>2002</option>
                <option>2003</option>
                <option>2004</option>
                <option>2005</option>
                <option>2006</option>
                <option>2007</option>
                <option>2008</option>
                <option>2009</option>
                <option>2010</option>
                <option>2011</option>
                <option>2012</option>
                <option>2013</option>
				<option>2014</option>
            </select>
        </div>

        <div class="span2 lightblue">
            <label>Make Model</label>
			<?php echo $make_model_trailer;?>
            <!--select id="make_model_vh" name="trailer_make_model_vh[]"  class="span12" >
				<option value="Freahauf">Freahauf</option>
				<option value="Greate Dane" >Greate Dane</option>
				<option value="Stoughton" >Stoughton</option>
				<option value="Trailmobile" >Trailmobile</option>
				<option value="Utility" >Utility</option>
				<option value="Wabash" >Wabash</option>
				<option value="Other" >Other</option>
            </select-->
        </div>

        <div class="span2 lightblue">
            <label>GVW</label>

            <select id="gvw_vh" name="trailer_gvw_vh[]" class="span12" >
                <option>0-10,000 pounds</option>
                <option>10,001-26,000 pounds</option>
                <option>26,001-40,000 pounds</option>
                <option>40,001-80,000 pounds</option>
                <option>Over 80,000 pounds</option>
            </select>
        </div>

        <div class="span2 lightblue">
            <label>VIN</label>
            <input id="vin_vh" maxlength="19" name="trailer_vin_vh[]" type="text" class="span12"  >
        </div>
		
		<div class="span2 lightblue">
		<label>Trailer Type</label>
		<?php echo $trailer_type_select; ?>
		</div>
		
		<div class="span2 lightblue">
		<label>Owned Vehicle</label>
		<select name="trailer_owned_vehicle[]" id="trailer_owned_vehicle__SECID__" class="span12" onchange="update_unknown(&quot;__SECID__&quot;)">
           
            <option value="yes" selected="selected" >YES</option>
            <option value="no">NO</option>
        </select>
		</div>
		
        <div id="trailer_special_req_div" class="span2 lightblue tractor_special_req_div trailer_special_req_div" style="display:none; margin-left:2px;">
            <label>Special req.</label>
            <input id="value_amount_vh" name="trailer_value_amount_vh[]" type="text" class="span12 " />
        </div>



    </div>
    <div class="row-fluid " >  	

    </div>
</div>

<!-- Drive Row -->



<!--div id="row_driver" style="display:none;">
        <div class="add_driver">
        <div class="row-fluid">
                <div class="span4 lightblue">
        <input type="text" name="driver_name[]"  class="span12" />
        </div>
        <div class="span8 lightblue"> </div>
        </div>
        
        </div>
</div-->
<input type="hidden" id="driver_count" value="1">


<!-- Filing Row -->

<div id="row_filing" style="display:none;">
    <div class="add_filing">
        <div class="row-fluid">  
            <div class="span2 lightblue">
                <label >Filing Type :</label><input type="text" name="filing_type[]" class="span12 filing_type" />
            </div>
            <div class="span2 lightblue">
                <label >Filing # </label><input type="text" name="filing_numbers[]" class="span12 filing_numbers" />
            </div>
        </div>

    </div>
</div>

<div id="option_liability" style="display:none;">
	<div id="truck_liability" style="display:none;" class="truck_liability vehicle_liability_val span12" >
		
    <div class="span2 lightblue">
        <label>Liability</label>
        <select id="liability_vh" name="liability_vh[]" class="span12" >
            <option>$1,000,000</option>
            <option>$750,000</option>
            <option>$500,000</option>
        </select>
    </div>

    <div class="span2 lightblue">
        <label>Liability Ded</label>
        <select id="liability_ded_vh" name="liability_ded_vh[]" class="span12" >
            <option>$1,000</option>
            <option>$1,500</option>
            <option>$2,000</option>
            <option>$2,500</option>
        </select>
    </div>
	<div class="span3">
		<label>UM</label>
		<select id="truck_um" name="truck_um[]" class="span12" >
			<option>None</option>
			<option>$15,000/$30,000</option>
			<option>$30,000/$60,000</option>
			<option>Rejected</option>
		</select>
	</div>
	
	<div class="span2">
		<label>PIP</label>
		<input type="text" name="truck_pip[]" class="span12 price_format" />
	</div>
	
    </div>
</div>

<div id="option_pd" style="display:none;">
	<div id="truck_pd" style="display:none;" class="truck_pd span4" >
		
    <div class="span6 lightblue">
        <label>PD</label>
        <input id="pd_vh" name="pd_vh[]" type="text"  class="span12 price_format" />
    </div>
    <div class="span6 lightblue">
        <label>PD Ded</label>
        <input id="ph_ded_vh" name="ph_ded_vh[]" type="text"   class="span12 price_format" />
    </div>
    </div>
</div>

<div id="option_cargo" style="display:none;">
	<div id="truck_cargo" style="display:none;" class="truck_cargo span4 truck_cargos" >
		
		<div class="span6 lightblue">
			<label>Cargo</label>
			<input id="cargo_vh" name="cargo_vh[]" type="text" class="span12 price_format cargo_class_price2" />
		</div>
		<div class="span6 lightblue">
			<label>Cargo Ded</label>
			<input id="cargo_ded_vh" name="cargo_ded_vh[]" type="text" class="span12 price_format cargo_class_ded2" />
		</div>
	</div>
</div>

<!--Tractor-->
<div id="tractor_option_liability" style="display:none;">
	<div id="tractor_liability" style="display:none;" class="tractor_liability vehicle_liability_val span12" >
		<div class="span2 lightblue">
			<label>Liability</label>
			<select id="tractor_liability_vh" name="tractor_liability_vh[]" class="span12" >
				<option>$1,000,000</option>
				<option>$750,000</option>
				<option>$500,000</option>
			</select>
		</div>

		<div class="span2 lightblue">
			<label>Liability Ded</label>
			<select id="tractor_liability_ded_vh" name="tractor_liability_ded_vh[]" class="span12" >
				<option>$1,000</option>
				<option>$1,500</option>
				<option>$2,000</option>
				<option>$2,500</option>
			</select>
		</div>
		
		<div class="span3">
		<label>UM</label>
		<select id="tractor_um" name="tractor_um[]" class="span12" >
			<option>None</option>
			<option>$15,000/$30,000</option>
			<option>$30,000/$60,000</option>
			<option>Rejected</option>
		</select>		
	</div>
	
	<div class="span2">
		<label>PIP</label>
		<input type="text" name="tractor_pip[]" class="span12 price_format" />
	</div>
	
	</div>
</div>

<div id="tractor_option_pd" style="display:none;">
	<div id="tractor_pd" style="display:none;" class="tractor_pd span4" >
	
		<div class="span6 lightblue"> 
			<label>PD</label>
			<input id="tractor_pd_vh" name="tractor_pd_vh[]" type="text"  class="span12 price_format" />
		</div>
		<div class="span6 lightblue">
			<label>PD Ded</label>
			<input id="tractor_ph_ded_vh" name="tractor_ph_ded_vh[]" type="text"   class="span12 price_format" />
		</div>
	</div>
</div>

<div id="tractor_option_cargo" style="display:none;">
	<div id="tractor_cargo" style="display:none;" class="tractor_cargo span4 tractor_cargos" >
	
		<div class="span6 lightblue">
			<label>Cargo</label>
			<input id="tractor_cargo_vh" name="tractor_cargo_vh[]" type="text" class="span12 price_format cargo_class_price" />
		</div>
		<div class="span6 lightblue">
			<label>Cargo Ded</label>
			<input id="tractor_cargo_ded_vh" name="tractor_cargo_ded_vh[]" type="text" class="span12 price_format cargo_class_ded" />
		</div>
	</div>
</div>

<div id="trailer_option_liability" style="display:none;">
	<div id="trailer_liability" style="display:none;" class="trailer_liability vehicle_liability_val span4" >
	
    <div class="span6 lightblue">
        <label>Liability</label>
        <select id="trailer_liability_vh" name="trailer_liability_vh[]" class="span12" >
            <option>$1,000,000</option>
            <option>$750,000</option>
            <option>$500,000</option>
        </select>
    </div>

    <div class="span6 lightblue">
        <label>Liability Ded</label>
        <select id="trailer_liability_ded_vh" name="trailer_liability_ded_vh[]" class="span12" >
            <option>$1,000</option>
            <option>$1,500</option>
            <option>$2,000</option>
            <option>$2,500</option>
        </select>
    </div>
	
</div>
</div>

<div id="trailer_option_pd" style="display:none;">
	<div id="trailer_pd" style="display:none;" class="trailer_pd tractor_pd span4" >
	
    <div class="span6 lightblue">
        <label>PD</label>
        <input id="trailer_pd_vh" name="trailer_pd_vh[]" type="text"  class="span12 price_format cargo_price_trk" />
    </div>
    <div class="span6 lightblue">
        <label>PD Ded</label>
        <input id="trailer_ph_ded_vh" name="trailer_ph_ded_vh[]" type="text"   class="span12 price_format" />
    </div>
	
</div>
</div>

<div id="trailer_option_cargo" style="display:none;">
<div id="trailer_cargo" style="display:none;" class="trailer_cargo tractor_cargo span4" >
	
    <div class="span6 lightblue">
        <label>Cargo</label>
        <input id="trailer_cargo_vh" name="trailer_cargo_vh[]" type="text" class="span12 price_format" />
    </div>
    <div class="span6 lightblue">
        <label>Cargo Ded</label>
        <input id="trailer_cargo_ded_vh" name="trailer_cargo_ded_vh[]" type="text" class="span12 price_format" />
    </div>
</div>
</div>

<!--MAYANK-->

<div id="specifier_table_div" class="row-fluid" style="display:none;" >  <!-- Hidded as per instruction in 24-07-2013 Doc-->
	<div class="row-fluid" style="display:none" >
		<div class="span12 lightblue">
			<table  class="table table-bordered table-striped table-hover">
				<tbody>
					<tr>
						<th colspan="6"  >What type of Cargo are you carrying? <i>(Please check all that applies)</i>
						</th>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Appliances" /></td>
						<td>Appliances</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Electronics" /></td>
						<td>Electronics</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Oilfield Equip." /></td>
						<td>Oilfield Equip.</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Automobiles" /></td>
						<td>Automobiles</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Explosives" /></td>
						<td>Explosives</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Paper" /></td>
						<td>Paper</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Auto parts" /></td>
						<td>Auto parts</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Fertilizers" /></td>
						<td>Fertilizers</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Petroleum" /></td>
						<td>Petroleum</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Boats" /></td>
						<td>Boats</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Furniture" /></td>
						<td>Furniture</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Pipe" /></td>
						<td>Pipe</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Build. Matls." /></td>
						<td>Build. Matls.</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Grain" /></td>
						<td>Grain</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Poultry" /></td>
						<td>Poultry</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Candy" /></td>
						<td>Candy</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Livestock" /></td>
						<td>Livestock</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Produce" /></td>
						<td>Produce</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Canned goods" /></td>
						<td>Canned goods</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Liquors" /></td>
						<td>Liquors</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Seafood" /></td>
						<td>Seafood</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Carpets" /></td>
						<td>Carpets</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Lumber" /></td>
						<td>Lumber</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Steel" /></td>
						<td>Steel</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Chemicals" /></td>
						<td>Chemicals</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Machinery" /></td>
						<td>Machinery</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Textile" /></td>
						<td>Textile</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Clothing" /></td>
						<td>Clothing</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Meat" /></td>
						<td>Meat</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Tires" /></td>
						<td>Tires</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Cotton" /></td>
						<td>Cotton</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Milk &amp; cream" /></td>
						<td>Milk &amp; cream</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Tobacco" /></td>
						<td>Tobacco</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Containers" /></td>
						<td>Containers</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Mobile homes" /></td>
						<td>Mobile homes</td>
						<td><input type="checkbox" name="cargo_specifier[]" value="Nuts" /></td>
						<td>Nuts</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="cargo_specifier[]" value="Eggs" /></td>
						<td>Eggs</td>
						<td><input type="checkbox" name="other_cargo_specifier_no" value="Other" id="other_specifier" onChange="show_specifier_input(this.id, 1)" /></td>
						<td>Other </td>
						<td colspan="3"><input type="text" name="cargo_specifier[]" id="other_specifier_inp" style="display:none;" ></td>
						
					</tr>
					<tr>
						
						<td><input type="checkbox" name="other_cargo_specifier_no2" value="Other" id="other_specifier_2" onChange="show_specifier_input(this.id, 2)" /></td>
						<td>Other </td>
						<td colspan="2"><input type="text" name="cargo_specifier[]" id="other_specifier_2_inp2" style="display:none;" ></td>
						<td colspan="3"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
	<!--END MAYANK-->
<div id="license_state" style="display:none;" >
<?php echo get_state_dropdown('license_issue_state[]', 'CA', 'style="width:70px;" required="required" '); ?>
</div>


    <!--div class="input-append date" id="dp3" data-date="12-02-2012" data-date-format="dd-mm-yyyy">
    <input class="span2" size="16" type="text" value="12-02-2012" readonly >
    <span class="add-on"><i class="icon-th"></i></span>
    </div-->


<script>
$(document).ready(function(){
	$("#agencies").change(function(){
		//window.location.href = window.location.href + "/" + $(this).val();
		$("#broker_email").val($(this).text());
		
		$.ajax({
			url: '<?php echo base_url();?>manualquote/setpublishersession',
			type: "POST",
			data: {
					action:'via_ajax',
					publisherid : $(this).val()
			},
			success: function(data) {
				
				window.location.reload(true);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				//alert(thrownError);
			}
		});
	});
});
function update_unknown(val){
	var owned_veh = $('#trailer_owned_vehicle'+val).val();


	if(owned_veh == 'no'){
		$("#vehicle_year_vh"+val).removeAttr("selected");
		$("#vehicle_year_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#make_model_vh"+val).removeAttr("selected");
		$("#make_model_vh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
		
		$("#trailer_type_veh"+val).removeAttr("selected");
		$("#trailer_type_veh"+val).append('<option value="Unknown" selected="selected"  >Unknown</option>'); 
	}else{
		$("#vehicle_year_vh"+val+" option[value='Unknown']").remove();
		$("#make_model_vh"+val+" option[value='Unknown']").remove();
		$("#trailer_type_veh"+val+" option[value='Unknown']").remove();
	}
	/* vehicle_year_vh1-2
	make_model_vh1-2
	trailer_type_veh1-2 */
	//alert();
}


	
	//$("#coverage_date").mask("99/99/9999");
	
	/* ('#dp3').datepicker({
			//format: 'mm/dd/yyyy'
		}); */

    /* $("input").each(function() {
      this.value = "";}) */
    //add_driver();
    //alert($(window).width()); 
	
/* 	$( ".number_limit" ).validate({
rules: {
field: {

range: [1, 99]
}
}
}); */
</script>




</body>
</html>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>