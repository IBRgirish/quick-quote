<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
?>
<script type="text/javascript" src="<?php echo base_url('js'); ?>/jquery1.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/fancybox'); ?>/jquery.fancybox.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('css/datatable.css') ?>">
<script src="<?php echo base_url('js/datatables.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function () {
    load_underwriter_list();
});

function load_underwriter_list() {
    $("#underwriterList").dataTable({
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "bServerSide": true,
        "fnDrawCallback": function (oSettings) {
            if (oSettings.bSorted || oSettings.bFiltered) {
                for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
                    $('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
                }
            }
        },

        "sAjaxSource": "<?php echo site_url(); ?>quote/ajax_assigned_quote/",

        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": fnCallback
            })
        },


        "aoColumns": [{
                "sClass": "center"
            },

            //{"sClass": "center"},
            {
                "fnRender": function (oObj) {
                    var a = oObj.aData[1];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[2];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[3];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[4];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[5];
                    return (a);
                }
            }, {
                "fnRender": function (oObj) {
                    var a = oObj.aData[6];
					
					if(oObj.aData[7] > 0)
					{
						a = oObj.aData[8];
						b = '(<a href="<?php echo base_url(); ?>ratesheet/rate_by_quote/'+oObj.aData[0]+'">View</a>)';
					}
					else 
					{
						a = 'Pending';
						b = '(<a href="<?php echo base_url('ratesheet/generate_ratesheet');?>/'+oObj.aData[0]+'">Generate</a>)';
						
					}
					
					//var b = '(<a href="javascript:void(0);">Change</a>)';
                    return (a+'&nbsp;'+b);
                }

            }
        ]
    });

}
</script>
<br>
<h1>Quote List</h1>
<div class="table"> 
<!--<img src="<?php // echo base_url();?>img/admin/bg-th-left.gif" alt="" class="left" width="8" height="7">--> 
<!--<img src="<?php //echo base_url();?>img/admin/bg-th-right.gif" alt="" class="right" width="7" height="7">-->
<!--<a class="fancybox-add btn btn-primary" onclick="javascript:void(0)" href="">Add New</a>-->

  <!-- Quote List -->
  <table id="underwriterList" width="60%" class="dataTable">
    <thead>
      <tr>
        <th class="center" width="10%">Sr.</th>
        <th width="15%">Name</th>
        <th width="15%">Email</th>
        <th width="15%">Phone</th>
		<th width="15%">Requester</th>
		<th width="15%">Cov. Date</th>
        <th width="15%">Status</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
  <!-- Quote List -->
</div>
<div class="pagination">
  <?php //$this->pagination->create_links();?>
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('administration/includes/footer');
}
?>
