<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ratesheet_physical_damage extends RQ_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
 public function __construct()
	{
		parent::__construct();
		$this->load->model('ratesheet_physical_damage_model');
	}
	
	public function index()
	{
		$data = array();
		
		$data['row'] = new stdClass();
		//$data['row'] = $this->admin_init_elements->set_post_vals($this->input->post());
		$data['row']->id = $this->uri->segment(3);	
		$data['action'] = 'add';
		$post_action =$data['action'];
		
		if ($this->input->post('for_submit')){
			$post_array = $this->input->post();
			
			$action = ($post_action == 'add')?'inserted':'updated';
			$data['error_message'] = $this->ratesheet_physical_damage_model->insert_ratesheet($post_array,$action);
			if($data['error_message'] == 'Record '.$action.' successfully'){
				$data['row'] = new stdClass();
				$data['row']->id = $this->uri->segment(3);	
				$data['row']->status = 1;
			}
		}
		if($data['row']->id>0){
			$data['action'] = 'update';
			$data['heading'] = 'Edit';
		}
		//echo '<pre>';print_r($data);die;
		$this->load->view('physical_damage_ratesheet',$data); 
				
	}



	public $file_upload_path = './uploads/quote_request/';
	
    /*public function __construct()
	{
		parent::__construct();
		if(!$this->require_login_user() && !$this->require_login_underwriter())
		{
			$base = base_url('login');
			redirect($base);
		}
		$this->load->model('ratesheet_model');
		 
		
		// Your own constructor code
		 
		  
		
	}*/

	
  public function QuickQuote2($ratesheet_id = '')
	{
		$this->ratesheet_physical_damage_model->insert_ratesheet($ratesheet_id);
			$upload_path = $this->file_upload_path;
			
			if($_POST){  
				$result = $this->ratesheet_physical_damage_model->insert_ratesheet($upload_path);
				//$this->session->set_flashdata('success', '<strong>Success</strong> Quote Request Sent Successfully');
			} else{
				//$this->session->set_flashdata('error', '<strong>Error</strong> In Sending Quote Request ');
			}
			
			if($ratesheet_id){
				$this->load->view('physical_damage_ratesheet', $this->data);
			}else {				
				$this->load->view('');
				/* $red_url = base_url('quote/response');
				redirect($red_url); */
			}
	}	
	
  
	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */