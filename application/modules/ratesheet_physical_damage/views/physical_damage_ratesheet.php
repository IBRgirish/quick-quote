<?php  
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
?>

<style>
		label.cabinet {
			width: 79px;
			background: url(images/upload_icon.png) 0 0 no-repeat;
			display: block;
			overflow: hidden;
			cursor: pointer;
			width:62px; margin-bottom:5px; cursor:pointer;height:23px;
	
		}
 
		label.cabinet input.file {
			position: relative;
			cursor: pointer;
			height: 100%;
			width: 120px;
			opacity: 0;
			-moz-opacity: 0;
			filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0);
		
		}
		.container-fluid {
			display:table;
		}
		

table {
    margin: 19px 0;
}
</style>

<?php
$ratesheet_id = 1;
?>







<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title> cargo_rate_sheet </title>
    <link href="<?php echo base_url('css');?>/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url('css');?>/bootstrap.css" rel="stylesheet">
     <link href="<?php echo base_url('css');?>/common.css" rel="stylesheet">    
    <link href="<?php echo base_url('css');?>/fontawesome.css" rel="stylesheet">    
   <link href="<?php echo base_url('css');?>/project.css" rel="stylesheet">
    <link href="<?php echo base_url('css');?>/meetingminute.css" rel="stylesheet">
    
    <script src="<?php echo base_url('js');?>/jquery.autotab-1.1b.js"></script>
    <script src="<?php echo base_url('js'); ?>/jquery.price_format.js"> </script>
    <script src="<?php echo base_url('js'); ?>/jquery.h5validate.js"> </script>
    <script src="<?php echo base_url('js'); ?>/jquery.maskedinput.js" type="text/javascript"></script>
    
    <script src="<?php echo base_url('js'); ?>/chosen.jquery.min.js" type="text/javascript"></script>
    
    <script src="<?php echo base_url('js'); ?>/request_quote.js"></script>
    <!--script src="<?php echo base_url('js'); ?>/jquery.validateForm.js"></script-->
    
    
  <script type='text/javascript'>
		$(document).ready(function() {
				$('.show_tractor_others').hide();
			<!----readonly system---> 
			/*$(".mkread").attr("readonly","true"); 
			$('.efl').change(function(){
				
				if ($(this).val()=="None")
				{
					$(this).parents('.row-efl').find('.mkread').attr("readonly",true); 			
				}
				else
				{
					$(this).parents('.row-efl').find('.mkread').attr("readonly",false);
				}
			});*/
			
			
			
			$(".mkread").attr("readonly","true"); 	
			$('.efl').change(function(){
				
				if (($(this).val()=="None") || ($(this).val()=="0") || ($(this).val()=="No"))
				{
					$(this).parents('.row-efl').find('.mkread').attr("readonly",true); 			
				}
				else
				{
					$(this).parents('.row-efl').find('.mkread').attr("readonly",false);
				}
			});
			
		
		
		
		
<!------------------Premium section--------------------->
			$('.total_pd_pr').change(function(){
				
				var val1=$('.total_pd_pr').val();	
							
				$('.row-efl').each(function(){
					if(($(this).find('.efl').val() != 'None') && ($(this).find('.efl').val() != undefined))
					{						
						var val2=$(this).find('.nvc').val();
						$(this).find(".nvp").val(val1*val2);					
					}
				});
					
			})	
			$('.tractor_trailer_qunt,.ttcc').change(function(){
				var val5=$('.ttcc').val();			
				var val6=$('.tractor_trailer_qunt').val();
				$(".ttcp").val(val5 * val6);	
			})

			$('.nvc').change(function(){
				var val1=$('.total_pd_pr').val();
				//var val2=$('.quantity').val();
				var val2=$(this).val();
				$(this).parent().parent().find(".nvp").val(val1*val2);
			})
			
			$(".drc").change(function(){
				$(".drp").val($(this).val());
			});
			
			$(".tls").change(function(){
				$(".tlp").val($(this).val());
			});

<!------------------Premium section--------------------->
			
			
			
			
			/*$('.loses_cost,.total_pd_pr').change(function(){
				var val1=$('.total_pd_pr').val();			
				var val2=$('.loses_cost').val();								
				$(".lsp").val(val1 * val2);							
			})
			
			$('.mis_cost,.total_pd_pr').change(function(){
				var val1=$('.total_pd_pr').val();			
				var val3=$('.mis_cost').val();
				$(".msp").val(val1 * val3);			
			})
			$('.driver_cost,.total_pd_pr').change(function(){
				var val1=$('.total_pd_pr').val();			
				var val4=$('.driver_cost').val();
				$(".dsp").val(val1 * val4);	
			})
				*/		
			
<!---------------------integer validation--------------------->	
			$(".numr_valid").keypress(function(evt){				
				var charCode = (evt.which) ? evt.which : event.keyCode;
				if (charCode != 46 && charCode > 31
				&& (charCode < 48 || charCode > 57))
				{
					//alert('Please enter numeric value');
					$(this).css("border", "1px solid #FAABAB");
					$(this).css("box-shadow", "0 0 5px rgba(204, 0, 0, 0.5");
					
					$(this).focus();
					return false;
				}			
				//return true;
				
else {
					$(this).css("border", "1px solid #CCCCCC");
					$(this).css("box-shadow", "none");
				}
			});				
<!---------------------integer validation--------------------->		







		
			
        });
		
  </script>   
    
    
    
    
</head>

<body>

	<?php
		$attributes = array('class' => 'request_ratesheet_physical_damage', 'id' => 'main_form4');
		echo form_open_multipart(base_url('ratesheet_physical_damage'), $attributes);
		
    ?>   
                

<div class="container-fluid">
 <div class="row-fluid">
        <span class="span12">
          
          <div class="well pull-center well-3 bga">
            <h4 class="heading pull-center heading-3">Physical Damage Rate Sheet</h4>           
          </div>
        </span>
      </div>
       <div class="well well-3 well">
        <div class="row-fluid">
          <span class="span6">
            <h5 class="heading  heading-4">Broker</h5>
           <input type="text" class="span9" name="broker" placeholder="Full Name/ DBA		">
          </span>
          <span class="span6">
            <h5 class="heading ">Insured</h5>
            <input type="text" class="span9" name="insured" placeholder="Full Name/ DBA		">
          </span>
        </div>
         <div class="row-fluid">
          <span class="custom_span3">
           <h5 class="heading  heading-3 hed-3">Radius of Operation</h5>
                <!--<input class="textinput span10 pull-left textinput-3 textinput-4 radiusofoperation" type="text" name="radius_of_operation" placeholder="">-->                
                <select name="radius_of_operation" class="select-3 span12 tractor_radius_of_operation" id="tractor_radius_of_operation" onChange="rofoperation(this.value);">
                    <option value="0-100 miles">0-100 miles</option>
                    <option value="101-500 miles" >101-500 miles</option>           	
                    <option value="501+ miles">501+ miles</option>
                    <option value="other">Other</option>            
                </select><br>
					
			
           
          </span>
		  <span class="span2 show_tractor_others">
            <h5 class="heading  heading-4 hed-3">Others</h5>
           <input class="textinput span12 pull-left " type="text" name="claim_venue" placeholder="">
           
          </span>
           <span class="span1">
            <h5 class="heading  heading-4 hed-3">State</h5>
			
					<?php
					$attr = "class='span12 ui-state-valid'";
					$broker_state = 'CA';
					
					?>
					<?php echo get_state_dropdown('insured_state[]', $broker_state, $attr); ?>
           
          </span>
           <!-- <span class="span1">
            <h5 class="heading  heading-4 hed-3">Clame Venue</h5>
            <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="claim_venue_rate" placeholder="">
           
          </span>
          <span class="span2">
            <h5 class="heading  heading-4 hed-3">Number of Power Units</h5>
            <input class="textinput span10 pull-left textinput-3 textinput-4" type="text" name="no_of_power_unit" placeholder="">
           
          </span>
           <span class="span1">
            <h5 class="heading  heading-4 hed-3"> Limit</h5>
            <input class="textinput span10 pull-left textinput-3 textinput-4" type="text" name="limit" placeholder="">
           
          </span>
           <span class="span1">
            <h5 class="heading  heading-4 hed-3">Deductible</h5>
            <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="reffer_deductible" placeholder="">
           
          </span>  
           <span class="span2">
            <h5 class="heading  heading-4 hed-3"> Reffer Deductible</h5>
            <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="deductible" placeholder="">
           
          </span>  -->     
           <span class="span2" >
            <h5 class="heading  heading-4 hed-3">Base rate</h5>
            <input class="textinput span10 pull-left textinput-3 textinput-4" type="text" name="base_rate" placeholder="">
           
          </span>              
        </div>
        <div class="hold">
         <div class="row-fluid pull-center row-efl" style="text-align:right"> 
             <span class="span5">
            <h5 class="heading pull-left heading-4" style="text-align:right">Losses</h5>&nbsp;
             <select name="losses_surcharge" class="select-3 span6 efl">
              <option value="None">None</option>
               <option value="Yes">Yes</option>              
            </select> 
           
          </span>
            <span style="
    padding: 0px 0px 5px 14px;
    display: block;
    float: left;
">@</span> 
          </span>
           <span class="span2">           
            <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
            <select name="losses_surcharge_cost" class="select-3 span12 mkread loses_cost nvc">
              <option value="0">0</option>	
              <option value="5">5%</option> 
               <option value="10">10%</option> 
                <option value="15">15%</option> 
                 <option value="20">20%</option> 
                  <option value="25">25%</option> 
                   <option value="30">30%</option>             
            </select>
           
          </span>
          <span class="span3">
            <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
           &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread lsp nvp" type="text" name="losses_surcharge_premium" placeholder="" readonly>
           
          </span>
        </div>
         <div class="row-fluid pull-center row-efl" style="text-align:right"> 
             <span class="span5">
            <h5 class="heading pull-left heading-4" style="text-align:right">Misc.surcharges</h5>&nbsp;
             <select name="miscellaneos_surcharge" class="select-3 span6 efl">
              <option value="None">None</option>
               <option value="Yes">Yes</option>                             
            </select>
           
          </span>
            <span style="
    padding: 0px 0px 5px 14px;
    display: block;
    float: left;
">@</span> 
          </span>
           <span class="span2">           
            <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
            <select name="miscellaneos_surcharge_cost" class="select-3 span12 mkread mis_cost nvc">
              <option value="0">0</option>	
              <option value="5">5%</option> 
               <option value="10">10%</option> 
                <option value="15">15%</option> 
                 <option value="20">20%</option> 
                  <option value="25">25%</option> 
                   <option value="30">30%</option>             
            </select>
           
          </span>
          <span class="span3">
            <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
           &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread msp nvp" type="text" name="miscellaneos_surcharge_premium" placeholder="" readonly>
           
          </span>
        </div>
          <div class="row-fluid pull-center row-efl" style="text-align:right"> 
             <span class="span5">
            <h5 class="heading pull-left heading-4" style="text-align:right">Driver surcharges(value)</h5>&nbsp;
                      
            <input class="textinput span6 pull-left textinput-3 textinput-4 select-3 span4 driver_surcharge numr_valid efl" type="text" name="driver_surcharge" placeholder="">
           
          </span>
            <span style="
    padding: 0px 0px 5px 14px;
    display: block;
    float: left;
">@</span> 
          </span>
           <span class="span2">           
            <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
           <select name="driver_surcharge_cost" class="select-3 span12 driver_cost mkread mis_cost nvc" >
              <option value="0">0</option>	
              <option value="5">5%</option> 
               <option value="10">10%</option> 
                <option value="15">15%</option> 
                 <option value="20">20%</option> 
                  <option value="25">25%</option> 
                   <option value="30">30%</option>             
            </select>
          </span>
          <span class="span3">
            <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
           &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 dsp nvp" type="text" name="driver_surcharge_premium" placeholder="" readonly>
           
          </span>
        </div>
         <div class="row-fluid pull-center row-efl" style="text-align:right"> 
             <span class="span5">
            <h5 class="heading pull-left heading-4" style="text-align:right">15 year tractor/trailer quantity</h5>&nbsp;
            <input class="select-3 span3 tractor_trailer_qunt numr_valid efl" type="text" name="tractor/trailer_quantity" placeholder="">
          </span>
          
            <span style="
    padding: 0px 0px 5px 14px;
    display: block;
    float: left;
">@</span> 
          </span>
           <span class="span2">           
            <input class="textinput span12 pull-left textinput-3 textinput-4 ttcc mkread mis_cost" type="text" name="tractor/trailer_surcharge_cost" placeholder="$200" value="200" >
           
          </span>
          <span class="span3">
            <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
           &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 ttcp scp" type="text" name="tractor/trailer_surcharge_premium" placeholder="" readonly>
           
          </span>
        </div>
        </div>
		     <strong > Optional endrosements:</strong>

        <div class="row-fluid pull-center row-efl" style="text-align:right"> 
             <span class="span4">
            <h5 class="heading pull-left heading-4" style="text-align:right">Debris removal</h5>&nbsp;
             <select name="debris_removal_limit" class="select-3 span4 efl">
              <option value="None">None</option>
               <option value="2,500">$2,500</option>                            
            </select> 
          </span>
             <span style="
    padding: 0px 0px 5px 14px;
    display: block;
    float: left;
">@</span> 
          </span>
        
           <span class="span2">           
            <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
           	<select name="debris_removal_cost" class="select-3 span12 mkread drc">
              <option value="0">0</option>
               <option value="150">$150</option>                            
            </select> 
          </span>
          <span class="span3">
            <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
           &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread drp" type="text" name="debris_removal_premium" placeholder="" readonly>
           
          </span>
        </div>
        <div class="row-fluid pull-center row-efl" style="text-align:right"> 
             <span class="span4">
            <h5 class="heading pull-left heading-4" style="text-align:right">Towing labor storage</h5>&nbsp;
             <select name="towing_labor_storage" class="select-3 span4 efl">
              <option value="None">None</option>
              <option value="7500">$7,500</option>                             
            </select> 
          
          </span>
          <span style="
    padding: 0px 0px 5px 14px;
    display: block;
    float: left;
">@</span> 
          </span>
           <span class="span2">           
        
          <!--<input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="" placeholder="">-->
           <select name="towing_labor_storage_cost" class="select-3 span12 mkread tls">
              <option value="None">None</option>
              <option value="200">$200</option>                             
            </select> 
          </span>
          <span class="span3">
            <h5 class="heading pull-left heading-4" style="text-align:right">Premium</h5>
           &nbsp; <input class="textinput span6 pull-left textinput-3 textinput-4 mkread tlp" type="text" name="towing_labor_storage_premium" placeholder="" readonly>
           
          </span>
        </div>
        <div class="row-fluid pull-center" style="text-align:right"> 
         <span class="span4">
            <h5 class="heading pull-left heading-4" style="text-align:right">Number of  Tractor</h5>&nbsp;
                 <input class="textinput span5 pull-left textinput-3 textinput-4 select-3 span4" type="text" name="number_of_tractor" placeholder="" onChange="add_vehicle(this.value)" id="no_of_tractor">
           
          </span>
		   <span class="span3">  
            <h5 class="heading pull-left heading-4" style="text-align:right">Number of Truck</h5>&nbsp;         
            <input class="textinput span5 pull-left textinput-3 textinput-4 select-3 span4" type="text" name="no_of_truck" placeholder="" id="no_of_truck">
          </span>
           <span class="span3">  
            <h5 class="heading pull-left heading-4" style="text-align:right">Number of Trailer</h5>&nbsp;         
            <input class="textinput span5 pull-left textinput-3 textinput-4 select-3 span4" type="text" name="number_of_trailer" placeholder="" id="no_of_trailer">
          </span>
        </div>
		
        <div class="tb" style="display:none">
        <table width="100%" border="1">
		<thead>
  <tr style="background-color:
  #CCCCCC; color:#000000; text-weight:bold;">
    <th width="16%">Vehicles</th>
    <th width="19%">value/limit</th>
    <th width="15%">Deductible</th>
    <th width="16%">Rate</th>
    <th width="18%">% or Rate</th>
    <th width="16%">Premium</th>
  </tr>
  </thead>
  <tbody id="add_owner_row">
  
  <tbody>
</table>
        </div>
       
        <div class="tdd">
         <div class="row-fluid pull-center" style="text-align:left;"> 
             <span class="span4">
            <h5 class="heading pull-left heading-4" style="text-align:left">Total Tractor Limit</h5>&nbsp;
                 <input class="textinput span5 pull-left textinput-3 textinput-4" type="text" name="total_tractor_limit" placeholder="">
           
          </span>
           <span class="span4">  
            <h5 class="heading pull-left heading-4" style="text-align:left">Total Tractor premium</h5>&nbsp;         
            <input class="textinput span5 pull-left textinput-3 textinput-4" type="text" name="total_tractor_premium" placeholder="">
          </span><br />
          
        </div>
         <div class="row-fluid pull-center" style="text-align:left; "> 
             <span class="span4">
            <h5 class="heading pull-left heading-4" style="text-align:left">Total Trailer Limit</h5>&nbsp;
                 <input class="textinput span5 pull-left textinput-3 textinput-4" type="text" name="total_trailer_limit" placeholder="">
           
          </span>
           <span class="span4">  
            <h5 class="heading pull-left heading-4" style="text-align:left">Total Trailer premium</h5>&nbsp;         
            <input class="textinput span5 pull-left textinput-3 textinput-4" type="text" name="total_trailer_premium" placeholder="">
          </span><br />
          
        </div>
        </div>
     
         <div class="row-fluid">
          <span class="span2">
            <h5 class="heading  heading-4">Total PD premium</h5>
            <input class="textinput span12 pull-left textinput-3 textinput-4 total_pd_pr" type="text" name="total_pd_premium" placeholder="">
           
          </span>
           <span class="span2">
            <h5 class="heading  heading-4">PD police fee</h5>
            <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="pd_policy_fee" placeholder="">
           
          </span>
           <span class="span2">
            <h5 class="heading  heading-4">PD SLA tax</h5>
            <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="pd_sla_tax" placeholder="">
           
          </span>
          <span class="span3">
            <h5 class="heading  heading-4"> Carrier</h5>
            <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="carrier" placeholder="">
           
          </span>
           <span class="span3">
            <h5 class="heading  heading-4">Required for FIRM quote</h5>
            <input class="textinput span12 pull-left textinput-3 textinput-4" type="text" name="required_for_firm_quote" placeholder="">
          </span>
        </div>
         <div class="row-fluid">
          <span class="span12">
            <h5 class="heading pull-left">Comments</h5>
            <input class="textinput span9" type="text" placeholder="" name="comments">
          </span>
         </div>
 
        </div>
        	<button class="btn btn-primary" type="submit" onClick="form_sub()">Submit Sheet</button>
		</div>
				<input type="hidden" name="for_submit" value="1" />
                <input type="hidden" name="action" value="add" />
            </form>
	</div>
      
<script>
function clonedivids(){
	$('.clonediv').each(function(index, element) {
		var num = index + 1;
		$(this).attr('id','clonediv'+num);
        
    });
}
function add_vehicle(val)
{
	var num     = $('#add_owner_row tr.vehicle_row').length; 
	
	totalveh = 0;
	if($("#no_of_tractor").val() != '')
	{
		 totalveh += parseInt($("#no_of_tractor").val());
	}
	if($("#no_of_truck").val() != '')
	{
		totalveh += parseInt($("#no_of_truck").val());
	}
	if($("#no_of_trailer").val() != '')
	{
		totalveh += parseInt($("#no_of_trailer").val());
	}
	if(totalveh > 0)
	{
		diff = num - totalveh;
		alert(diff);
		 if(diff < 0) {
			var row = $(".clonediv");
			
			while(diff < 0){
				var row = '<tr><td width="16%">Vehicle</td> <td><input type="text" maxlength="20" style="margin:5px" class="textinput span2 pull-left textinput-3 textinput-4" name="owner_middle_name[]" value=""></td>	 <td><input type="text" style="margin:5px" maxlength="20" class="textinput span2 pull-left textinput-3 textinput-4 " name="owner_last_name[]" value=""></td> <td><select name="owner_is_driver[]" class="span2" style="margin:5px"  >	<option value="">SELECT</option>	<option value="Y">Yes</option>				<option value="N">NO</option></select>		</td> 		<td><input type="text" style="margin:5px"  maxlength="20" class="span2" name="owner_license[]" value=""></td>		<td><input type="text" style="margin:5px"  maxlength="20" class="span2" name="owner_license[]" value=""></td>	</tr>';
				var v = $('#add_owner_row').append(row) ; 
				diff++;
			} 
		 }
		  else if(diff > 0)
		  {
			 while(diff > 0){
				 var v = $('#add_owner_row tr:last').remove() ; 
				 diff--;
			 }
		 }	
		$(".tb").show();
	} 
}

function rofoperation(val , id){
	if(val == 'other'){
		$('.show_tractor_others').show();
	} else {
		$('.show_tractor_others').hide();
	}	
}
	
	
	
/*  alert(id);
alert(val);  */



</script>        
           

</body>
</html>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>