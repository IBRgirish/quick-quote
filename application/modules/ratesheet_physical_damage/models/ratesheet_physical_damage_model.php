<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ratesheet_physical_damage_model extends CI_Model {

    public function &__get($key) {
        $CI = & get_instance();
        return $CI->$key;
        $this->load->database();
    }
	function insert_ratesheet($post_array,$action) {
		$insert_data = array();
		$insert_data = array();
		$insert_data['broker'] = trim($post_array['broker']);
		$insert_data['insured'] = trim($post_array['insured']);
		$insert_data['radius_of_operation'] = trim($post_array['radius_of_operation']);
		$insert_data['state'] = trim($post_array['state']);
		$insert_data['claim_venue_rate'] = trim($post_array['claim_venue_rate']);
		$insert_data['no_of_power_unit'] = trim($post_array['no_of_power_unit']);
		$insert_data['limit'] = trim($post_array['limit']);
		$insert_data['reffer_deductible'] = trim($post_array['reffer_deductible']);
		$insert_data['deductible'] = trim($post_array['deductible']);
		$insert_data['base_rate'] = trim($post_array['base_rate']);
		
		
		$insert_data['losses_surcharge'] = trim($post_array['losses_surcharge']);
		$insert_data['losses_surcharge_cost'] = trim($post_array['losses_surcharge_cost']);
		$insert_data['losses_surcharge_premium'] = trim($post_array['losses_surcharge_premium']);
		$insert_data['miscellaneos_surcharge'] = trim($post_array['miscellaneos_surcharge']);
		$insert_data['miscellaneos_surcharge_cost'] = trim($post_array['miscellaneos_surcharge_cost']);
		$insert_data['miscellaneos_surcharge_premium'] = trim($post_array['miscellaneos_surcharge_premium']);
		$insert_data['driver_surcharge'] = trim($post_array['driver_surcharge']);
		$insert_data['driver_surcharge_cost'] = trim($post_array['driver_surcharge_cost']);
		$insert_data['driver_surcharge_premium'] = trim($post_array['driver_surcharge_premium']);
		$insert_data['tractor/trailer_quantity'] = trim($post_array['tractor/trailer_quantity']);
		$insert_data['tractor/trailer_surcharge_cost'] = trim($post_array['tractor/trailer_surcharge_cost']);
		$insert_data['tractor/trailer_surcharge_premium'] = trim($post_array['tractor/trailer_surcharge_premium']);
		$insert_data['debris_removal_limit'] = trim($post_array['debris_removal_limit']);
		$insert_data['debris_removal_cost'] = trim($post_array['debris_removal_cost']);
		$insert_data['debris_removal_premium'] = trim($post_array['debris_removal_premium']);
		$insert_data['towing_labor_storage'] = trim($post_array['towing_labor_storage']);
		$insert_data['towing_labor_storage_cost'] = trim($post_array['towing_labor_storage_cost']);
		$insert_data['towing_labor_storage_premium'] = trim($post_array['towing_labor_storage_premium']);
		
		$insert_data['number_of_tractor'] = trim($post_array['number_of_tractor']);
		$insert_data['number_of_trailer'] = trim($post_array['number_of_trailer']);
		$insert_data['total_tractor_limit'] = trim($post_array['total_tractor_limit']);
		$insert_data['total_trailer_limit'] = trim($post_array['total_trailer_limit']);
		$insert_data['total_tractor_premium'] = trim($post_array['total_tractor_premium']);
		$insert_data['total_trailer_premium'] = trim($post_array['total_trailer_premium']);
		$insert_data['total_pd_premium'] = trim($post_array['total_pd_premium']);
		$insert_data['pd_policy_fee'] = trim($post_array['pd_policy_fee']);
		$insert_data['pd_sla_tax'] = trim($post_array['pd_sla_tax']);
		$insert_data['carrier'] = trim($post_array['carrier']);
		$insert_data['required_for_firm_quote'] = trim($post_array['required_for_firm_quote']);
		$insert_data['comments'] = trim($post_array['comments']);
		
		
		
		
		$this->db->insert('rq_ratesheet_damage_ratesheet', $insert_data);
		$result_id_ratesheet = $this->db->insert_id();
		$result =  $result_id_ratesheet;
	}

}
















/* End of file quote_model.php */
/* Location: ./application/models/quote_model.php */