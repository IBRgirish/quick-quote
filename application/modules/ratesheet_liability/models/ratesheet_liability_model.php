<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ratesheet_liability_model extends CI_Model {

    public function &__get($key) {
        $CI = & get_instance();
        return $CI->$key;
        $this->load->database();
    }
	function insert_ratesheet($post_array,$action) {
		$insert_data = array();
		$insert_data['broker'] = trim($post_array['broker']);
		$insert_data['insured'] = trim($post_array['insured']);
		$insert_data['radius_of_operation'] = trim($post_array['radius_of_operation']);
		$insert_data['state'] = trim($post_array['state']);
		$insert_data['claim_venue_rate'] = trim($post_array['claim_venue_rate']);
		$insert_data['no_of_power_unit'] = trim($post_array['no_of_power_unit']);
		$insert_data['combined_single_limit'] = trim($post_array['combined_single_limit']);
		$insert_data['deductible'] = trim($post_array['deductible']);
		$insert_data['unit_price_a'] = trim($post_array['unit_price_a']);
		$insert_data['quantity_a'] = trim($post_array['quantity_a']);
		
		
		$insert_data['unit_price_b'] = trim($post_array['unit_price_b']);
		$insert_data['quantity_b'] = trim($post_array['quantity_b']);
		$insert_data['new_venture_surcharge'] = trim($post_array['new_venture_surcharge']);
		$insert_data['new_venture_cost'] = trim($post_array['new_venture_cost']);
		$insert_data['new_venture_premium'] = trim($post_array['new_venture_premium']);
		$insert_data['lapse_in_coverage_surcharge'] = trim($post_array['lapse_in_coverage_surcharge']);
		$insert_data['lapse_in_coverage_percentage'] = trim($post_array['lapse_in_coverage_percentage']);
		$insert_data['lapse_in_coverage_premium'] = trim($post_array['lapse_in_coverage_premium']);
		$insert_data['auto_hauler_surcharge'] = trim($post_array['auto_hauler_surcharge']);
		$insert_data['auto_hauler_cost'] = trim($post_array['auto_hauler_cost']);
		$insert_data['auto_hauler_premium'] = trim($post_array['auto_hauler_premium']);
		$insert_data['um_coverage_surcharge'] = trim($post_array['um_coverage_surcharge']);
		$insert_data['um_coverage_cost'] = trim($post_array['um_coverage_cost']);
		$insert_data['um_coverage_premium'] = trim($post_array['um_coverage_premium']);
		$insert_data['pip_coverage_surcharge'] = trim($post_array['pip_coverage_surcharge']);
		$insert_data['pip_coverage_cost'] = trim($post_array['pip_coverage_cost']);
		$insert_data['pip_coverage_premium'] = trim($post_array['pip_coverage_premium']);
		$insert_data['losses_surcharge'] = trim($post_array['losses_surcharge']);
		$insert_data['losses_percentage'] = trim($post_array['losses_percentage']);
		
		
		$insert_data['losses_premium'] = trim($post_array['losses_premium']);
		$insert_data['miscellaneos_surcharge'] = trim($post_array['miscellaneos_surcharge']);
		$insert_data['miscellaneos_surcharge_cost'] = trim($post_array['miscellaneos_surcharge_cost']);
		$insert_data['miscellaneos_surcharge_premium'] = trim($post_array['miscellaneos_surcharge_premium']);
		$insert_data['drivers_surcharge_a'] = trim($post_array['drivers_surcharge_a']);
		$insert_data['drivers_surcharge_a'] = trim($post_array['drivers_surcharge_a']);
		$insert_data['drivers_surcharge_a_percentage'] = trim($post_array['drivers_surcharge_a_percentage']);
		$insert_data['drivers_surcharge_a_premium'] = trim($post_array['drivers_surcharge_a_premium']);
		$insert_data['drivers_surcharge_b'] = trim($post_array['drivers_surcharge_b']);
		$insert_data['drivers_surcharge_b_percentage'] = trim($post_array['drivers_surcharge_b_percentage']);
		$insert_data['drivers_surcharge_b_premium'] = trim($post_array['drivers_surcharge_b_premium']);
		$insert_data['extra_trailer_quantity'] = trim($post_array['extra_trailer_quantity']);
		$insert_data['extra_trailer_cost'] = trim($post_array['extra_trailer_cost']);
		$insert_data['extra_trailer_premium'] = trim($post_array['extra_trailer_premium']);
		$insert_data['tractor/trailer_quantity'] = trim($post_array['tractor/trailer_quantity']);		
		$insert_data['tractor/trailer_surcharge_cost'] = trim($post_array['tractor/trailer_surcharge_cost']);
		$insert_data['tractor/trailer_surcharge_premium'] = trim($post_array['tractor/trailer_surcharge_premium']);
		
		$insert_data['discount'] = trim($post_array['discount']);
		$insert_data['discount_percentage'] = trim($post_array['discount_percentage']);
		$insert_data['discounted_premium'] = trim($post_array['discounted_premium']);
		$insert_data['total_liability_premium'] = trim($post_array['total_liability_premium']);
		$insert_data['unit_price_breakdown'] = trim($post_array['unit_price_breakdown']);
		$insert_data['liability_policy_fee'] = trim($post_array['liability_policy_fee']);
		$insert_data['liablity_filing_fee'] = trim($post_array['liablity_filing_fee']);
		$insert_data['carrier'] = trim($post_array['carrier']);
		$insert_data['comments'] = trim($post_array['comments']);
		
		
		$this->db->insert('rq_ratesheet_liability', $insert_data);
		$result_id_ratesheet = $this->db->insert_id();
		$result =  $result_id_ratesheet;
	}

}
















/* End of file quote_model.php */
/* Location: ./application/models/quote_model.php */