<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends RQ_Controller {


		public function __construct()
		{


			parent::__construct();
			$this->load->model('members_model');
			$this->lang->load('member');	
			$this->load->helper('string');
			$this->load->helper('my_helper');
			$this->load->model('login_model');
			$this->load->helper('my_helper');
			$this->load->helper('text');
			$this->load->library('lotencrypt');	
			$this->lang->load('common'); 
		}

		public function index()
		{ 
			$this->load->view('user/index');
			//redirect('user/account');	
		}


		public function register()
		{		

			//$this->template->write('title', "Member Subscription");
			$data['metaKeywords'] = "Member Subscription";
			$data['underWriter'] = $this->members_model->active_underwriter_list();
			
			if(!$this->session->userdata('member_id')){		
				//$this->template->write_view('content', 'registrationform', $data);	
				//$this->template->write_view('content', 'registrationform_home', $data);
				$this->load->view('user/registrationform_home',$data);
			} else {
				redirect('user/account');
			}
			if($this->session->userdata('member_id')){		
				//$this->template->write_view('content', '404');
					
			}
			//$this->template->render();

		}

		
		
		public function processform()
		{
				$member_table = $this->config->item('member_table');
				//$registration_success = $this->lang->line('registration_success');
				$registration_success = 'Your account will be activated after review.';
				
				$this->form_validation->set_rules('txtFirstName', 'Agency name', 'required');
				//$this->form_validation->set_rules('txtMiddleName', 'Middle name', '');
				//$this->form_validation->set_rules('txtLastName', 'Last name', 'required');	
				$this->form_validation->set_rules('txaAddress', 'Address', 'required');
				$this->form_validation->set_rules('txtCountry', 'Country', 'required');
				$this->form_validation->set_rules('txtState', 'State', 'required');
				$this->form_validation->set_rules('txtCity', 'City', 'required');
				$this->form_validation->set_rules('txtZipCode', 'Zip Code', 'required');
				
				/* New Phone Number End - Sunil Chouhan @ 11/08/2013 */
				//$this->form_validation->set_rules('txtPhoneNumber', 'Phone Number', 'required');
				//$this->form_validation->set_rules('txtPhArea', 'Phone Area Code', 'required');
				$this->form_validation->set_rules('txtPhone1', 'Phone Number', 'required');
				$this->form_validation->set_rules('txtPhone2', 'Phone Number', 'required');
				$this->form_validation->set_rules('txtPhone3', 'Phone Number', 'required');
				/* New Phone Number End - Sunil Chouhan @ 11/08/2013 */
				
				$this->form_validation->set_rules('email', 'Email', "required|valid_email|is_unique[$member_table.email]" );
				$this->form_validation->set_rules('password', 'password', 'required|md5');
				$this->form_validation->set_rules('cpassword', 'Confirm password', 'required|matches[password]');
				
				if ($this->form_validation->run() === TRUE)
				{
						$formData = $this->input->post();
						
					/* 	$phone = $formData['txtPhArea'].'-'.$formData['txtPhone1'].'-'.$formData['txtPhone2'].'-'.$formData['txtPhone3'];
						$cell = $formData['txtMoArea'].'-'.$formData['txtCell1'].'-'.$formData['txtCell2'].'-'.$formData['txtCell3'];
						$faxN = $formData['txtFaxArea'].'-'.$formData['txtFax1'].'-'.$formData['txtFax2'].'-'.$formData['txtFax3']; */
						
						//$phone_area =  $formData['txtPhArea'];
						$phone = $formData['txtPhone1'].$formData['txtPhone2'].$formData['txtPhone3'];
						//$phoneext = $formData['txtPhoneExt'];
						
						//$cell_area =  $formData['txtMoArea'];
						$cell = $formData['txtCell1'].$formData['txtCell2'].$formData['txtCell3'];
						//$cellext = $formData['txtCellExt'];
						//$fax_area = $formData['txtFaxArea'];
						$faxN = $formData['txtFax1'].$formData['txtFax2'].$formData['txtFax3'];
						$faxext = $formData['txtFaxExt'];
						
						/* $underWriter = $formData['optUnderWriter'].':';			
						if ($this->input->post('chkchkUnderWriter'))
						{
							$underWriter .= implode(', ',$this->input->post('chkchkUnderWriter'));
						} */
						$underWriter = '';
						/* $country_post = $this->input->post('txtCountry');
						$country = urldecode($country_post); */		
						$country = $this->input->post('txtCountry');
						
						$data = array(		
									'first_name' => $this->input->post('txtFirstName'),
									//'middle_name' => $this->input->post('txtMiddleName'),
									//'last_name' => $this->input->post('txtLastName'),
									'address' => $this->input->post('txaAddress'),
									'city' => $this->input->post('txtCity'),
									'state' => $this->input->post('txtState'),
									'country' => $country,
									'zip_code' => $this->input->post('txtZipCode'),
									'zip_code_extd' => $this->input->post('txtZipCodeExtd'),
									
									'phone_number' => $phone,
									'phone_ext' => $this->input->post('txtPhoneExt'),
								
									'cell_number' => $cell,
									'fax_number' => $faxN,
									'fax_ext' => $faxext,
									'email' => $this->input->post('email'),	
									'sec_email' => $this->input->post('txtSecEmail'),
									'website' => $this->input->post('txtWebsite'),
									'password' => trim($this->input->post('password')),
									'underwriter' => $underWriter,
									'created_date' => date("Y-m-d : H:i:s", time()),
									'status' => 'REQUESTED'
							);
							
							$lastinsertid=$this->members_model->insert_update_members($data);
							
							$this->session->set_flashdata('success', $registration_success);
							
							//$this->session->set_userdata('member_id', $lastinsertid);	
							
							//$this->session->set_userdata('member_name', $this->input->post('txtFirstName'));
								
							//$this->session->set_userdata('username', $this->input->post('email'));
							
							echo '<script language="javascript">window.location.href=\''.base_url('user/registration_response').'\';</script>';
					} else {
						$data['metaKeywords'] = "Member Subscription";	
						$data['underWriter'] = $this->members_model->active_underwriter_list();	
						$this->load->view('user/registrationform_home',$data);
						
						//redirect('user/register');
					}
							
					//$this->template->write('title', "Member Subscription");
					//$data['metaKeywords'] = "Member Subscription";	
					//$data['underWriter'] = $this->members_model->active_underwriter_list();	
					
					//	$this->load->view('user/registrationform_home',$data);
				/* 		
					if($this->session->userdata('member_id'))
					{
						echo '<script type="text/javascript">
								parent.$.fancybox.close();
								parent.location.href = "'.base_url('user/account').'";
								</script>';
						
					} else {
						$this->template->write_view('content', 'registrationform_home', $data);		
						$this->template->render();
						
					} */
			}
			
		

	public function logout()
	{
        $this->session->sess_destroy();
		echo '<script language="javascript">window.location.href=\''.base_url().'\';</script>';
	}

	public function login($val="")
	{  
		$auto_unblock_period = $this->login_model->get_auto_unblock_period();
		$unblock_period=24;
		if ($auto_unblock_period) {
			if ($auto_unblock_period->value!='') {
				$unblock_period=$auto_unblock_period->value;
			}
		}
		
		$failed_attempts = $this->login_model->get_failed_attemps();
		$failed_attempt=3;
		if ($failed_attempts) {
			if ($failed_attempts->value!='') {
				$failed_attempt = $failed_attempts->value;
			}
		}
							
		if(($val == "1") && (base_url(uri_string()) != $this->config->item('prevous_url'))) {				
			$prev_url = $this->config->item('prevous_url');
			$this->session->set_userdata('prev_url', "$prev_url");		
		}		
		if($this->require_login_user()){ 						
			redirect('user/account');
		}
		if($this->session->userdata('member_id'))
		{
			$this->load->view('frontend/index/view_detail/viewdetail/');
		}else{
			$this->form_validation->set_rules('username', 'Username', 'required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if($this->form_validation->run() === FALSE)
			{				
				$this->template->write('title', "Member Login");
				$data['metaKeywords'] = "Member Login";
				//$this->load->view('user/login');
				$this->template->write_view('content', 'user/login', $data);		
				$this->template->render();
				
			}	
			else{
					// if user is valid
					$login_flag=0;
					$valid_user = $this->login_model->validate_member();
					if($valid_user) 
					{
						if ($valid_user->status=='BLOCKED')
						{
							$current_date=date("Y-m-d H:i:s", time());
							$last_login=date('Y-m-d H:i:s', strtotime('+'.$unblock_period.' hour', strtotime($valid_user->failed_attempt_timestamp)));
							
							if ($current_date>=$last_login)
							{
								$this->login_model->unblock_member_account();
								$login_flag=1;
							}
							else
							{
								$login_flag=0;
							}
						
						}
						elseif($valid_user->status=='REQUESTED'){
							$login_flag=2;
						}else
						{
							$login_flag=1;
						}
						
						if ($login_flag==1)
						{
							$data = array(
								'username' => $valid_user->email,
								'member_name' => $valid_user->first_name,
								'member_id' => $valid_user->id,
								);
							
							$this->session->set_userdata($data);
							// set_subscription_session();
							$this->members_model->update_lastlogin($valid_user->id);
							$this->session->set_flashdata('success', 'You have successfully logged into your account');
							if($val == "1")
							{
								$prev_url = $this->session->userdata('prev_url');
								$this->session->unset_userdata('prev_url');
								redirect($prev_url);
							}
							else
							{
								if($this->input->post('login_type') == 'fancybox')
								{
									echo '<script type="text/javascript">
											parent.$.fancybox.close();
											parent.location.href = "'.base_url('user/account').'";
										</script>';
								}
								else
								{
									redirect(base_url('user/account'));
								}
							}
						}
						elseif($login_flag === 2){
							$this->session->set_flashdata('error', 'Account is not active. Please contact admin or wait for activation.');
							if($this->input->post('login_type') == 'base')
							{
							   redirect(base_url());
							  
							}
							else
							{
							   redirect(base_url());
							}
						}else
						{
							$this->session->set_flashdata('error', 'Account is blocked contact admin or wait for '.$unblock_period.' hours to get unblock.');
							if($this->input->post('login_type') == 'base')
							{
							   redirect(base_url());
							  
							}
							else
							{
							   redirect(base_url());
							}
						}
				}
				else 
				{
					$failed_login_count = $this->login_model->get_failed_login_count();
					
					if ($failed_login_count)
					{
						if ($failed_login_count->failed_attempt < $failed_attempt)
						{
							$this->login_model->update_failed_login_count();
							$count = $this->login_model->get_failed_login_count();
							if ($count->failed_attempt == $failed_attempt)
							{
								$this->login_model->block_member_account();
								$this->session->set_flashdata('error', 'Account is blocked contact admin or wait for '.$unblock_period.' hours to get unblock.');
							}
							else
							{
								$this->session->set_flashdata('error', 'Information submitted is incorrect.');
							}
						}
						else
						{
							$this->session->set_flashdata('error', 'Account is blocked contact admin or wait for '.$unblock_period.' hours to get unblock.');
						}
					
					}
					else
					{
						$this->session->set_flashdata('error', 'Information submitted is incorrect.');
					}
					
					
					
					if($this->input->post('login_type') == 'base')
					{
					   redirect(base_url());
					  
					}
					else
					{
					   redirect(uri_string());
					}
				}
			  }

		}


	}
	
	public function loginmain()
	{
		$this->load->view('user/loginmain');
	}


	public function account($edit='')
	{				
		if(!$this->session->userdata('member_id')){		
			redirect('/');	
		} 
		//$this->template->write('title', "Member Account");
		$data['metaKeywords'] = "Member Account";	  
		$data['member']=$this->members_model->get_member_info($this->session->userdata('member_id'));
		$data['object'] = $this; 
		//$this->template->add_css('application/modules/user/css/account.css'); 
		if($edit=="edit"){
			//$this->template->write_view('content', 'user', $data);
			$data['underWriter'] = $this->members_model->active_underwriter_list();
			$this->load->view('user/user',$data);

		}else{
			//$this->template->write_view('content', 'account', $data);	
			$this->load->view('user/account',$data);		
		}
		//$this->template->render();	

	}
	
	public function accountedit()
	{		
			if(!$this->require_login_user()){
				redirect('/');
			}		
			$id = $this->session->userdata('member_id');
			if($id >0 && $this->input->post('password')!=''){	
				$this->form_validation->set_rules('confirm_password', 'Confirm password', 'required|matches[password]');
			}
			
			$registration_success = $this->lang->line('registration_success');		
		
			$this->form_validation->set_rules('txtFname', 'Agency name', 'required');
			//$this->form_validation->set_rules('txtMname', 'Middle name', '');
			//$this->form_validation->set_rules('txtLname', 'Last name', 'required');
			$this->form_validation->set_rules('txaAddress', 'Address', 'required');
			$this->form_validation->set_rules('txtCountry', 'Country', 'required');
			$this->form_validation->set_rules('txtState', 'State', 'required');
			$this->form_validation->set_rules('txtCity', 'City', 'required');
			$this->form_validation->set_rules('txtZip', 'Zip Code', 'required');
			$this->form_validation->set_rules('txtZipExtd', 'Zip Code Extended', '');
			$this->form_validation->set_rules('txtPhone', 'Phone', 'required');
			$this->form_validation->set_rules('txtExtension', 'Phone Extension', '');
			$this->form_validation->set_rules('txtCellNumber', 'Cell Number', '');
			$this->form_validation->set_rules('txtFaxNumber', 'Fax', '');
			$this->form_validation->set_rules('txtSecEmail', 'Secondary Email', '');
			$this->form_validation->set_rules('txtWebsite', 'Website', '');
			
			if ($this->form_validation->run() === TRUE)
			{
				
				$underWriter = $this->input->post('optUnderWriter').':';
				if ($this->input->post('chkchkUnderWriter'))
				{
					$underWriter.= implode(', ',$this->input->post('chkchkUnderWriter'));
				}						
				
				$data = array(		
					'first_name' => $this->input->post('txtFname'),
					//'middle_name' => $this->input->post('txtMname'),
					//'last_name' => $this->input->post('txtLname'),
					'address' => $this->input->post('txaAddress'),
					'city' => $this->input->post('txtCity'),
					'state' => $this->input->post('txtState'),
					'country' => $this->input->post('txtCountry'),
					'zip_code' => $this->input->post('txtZip'),
					'zip_code_extd' => $this->input->post('txtZipExtd'),
					'phone_number' => $this->input->post('txtPhone'),
					'phone_ext' => $this->input->post('txtExtension'),
					'cell_number' => $this->input->post('txtCellNumber'),
					'fax_number' => $this->input->post('txtFaxNumber'),	
					'email' => $this->input->post('email'),	
					'sec_email' => $this->input->post('txtSecEmail'),
					'website' => $this->input->post('txtWebsite'),
					'underwriter' => $underWriter,
					'updated_date' => date("Y-m-d : H:i:s", time()),
				);
				
				if($this->input->post('edit_memberinfo'))
				{
					if($this->input->post('members_id') > 0)
					{	
						if(trim($this->input->post('password'))!=''){
							$data['password'] = md5($this->input->post('password'));
						} else {
							$data['password'] = $this->input->post('savepassword');
						}
					}
				}
				
				$saved=$this->members_model->insert_update_members($data, $this->input->post('members_id'));
				//echo $this->db->last_query; die;
				if($saved){
					$this->session->set_flashdata('success', 'Your Information has been saved successfully '); 
					$data['saved']='Your Information has been saved successfully ';
				}
				$this->template->write('title', "Member Account");
			$data['metaKeywords'] = "Member Account";		
			$data['member']=$this->members_model->get_member_info($this->session->userdata('member_id'));		
			$this->template->write_view('content', 'account', $data);
			$this->template->render();
			$red_url = base_url('user/account');
			redirect($red_url);	
			} else {
				$data['metaKeywords'] = "Member Account";	  
			$data['member']=$this->members_model->get_member_info($this->session->userdata('member_id'));
			$data['object'] = $this; 
			$this->load->view('user/user',$data);
			}
				
			
		}


		public function changepassword()
		{ 
			
			if(!$this->require_login_user()){
				redirect('/');
			}		
			$id = $this->session->userdata('member_id');
			
			$this->form_validation->set_rules('old_passwords', 'Old Password', 'required');
			$this->form_validation->set_rules('new_password', 'New Password', 'required|md5');	
			$this->form_validation->set_rules('re_password', 'Re-Password', 'required|matches[new_password]');
			
			$pww = $this->input->post('old_passwords');
			if(!empty($pww)){
				if($this->session->userdata('current_password')!= md5($this->input->post('old_passwords'))){
					$this->form_validation->set_rules('old_passwords', 'Old Password', 'matches[Existing Password]');
				}
			}
			
			if ($this->form_validation->run() === TRUE)
			{						
				$currentDateTime=($this->input->post('create_date')!='' && $this->input->post('create_date')!='0000-00-00 00:00:00') ? $this->input->post('create_date') :date("Y-m-d : H:i:s", time());

				$data = array(
								'password' => $this->input->post('new_password')									
							);
							
				if($this->members_model->change_password($data, $id)){
					$this->session->set_flashdata('success', 'Your Password has been changed successfully '); 	
					redirect('user/changepassword');	
				}
							
				/* if($this->input->post('change_password'))
				{
					if($this->members_model->change_password($data, $id)){
						
					}
				} 		
					$this->session->set_flashdata('success', 'Your Password has been changed successfully '); 
					*/
			}
				$this->template->write('title', "Member Change Pasword");
				$data['metaKeywords'] = "Member Change Pasword";		
				$data['member']=$this->members_model->get_member_info($this->session->userdata('member_id'));	
			
				//$this->template->write_view('content', 'changepassword', $data);
				//$this->template->render();
				$this->load->view('user/changepassword',$data);

		}

	public function forgotpassword()
		{
			$member_table = $this->config->item('member_table');
			
			$this->form_validation->set_rules('email', 'Email', "required|valid_email|is_valid_data[$member_table.email]");   
			if ($this->form_validation->run() === TRUE)
			{
				$member = $this->members_model->get_member_info_via_email($this->input->post('email'));
				
				if ($member)
				{
					$random_pass=random_string('alnum', 10);
					
					$data = array (
						'password' 		=>	md5(trim($random_pass)),
						'updated_date'	=>	date("Y-m-d : H:i:s", time())
					);
					
					$setdata['first_name']	= $member->first_name;
					$setdata['last_name']	= $member->last_name;
					$setdata['email'] 		= $this->input->post('email');
					$setdata['password'] 	= $random_pass;
					
					$this->members_model->forgot_password($data,$this->input->post('email'));
					
					$body	=	"Your new password is : ".$random_pass;	
					$subject=	"Forgot Password Notification";		
							
					$this->load->library('email');
					$this->email->set_newline("\r\n");
					$this->email->from(ADMIN_EMAIL_ADDRESS, ADMIN_SITE_NAME);
					$this->email->to($this->input->post('email')); 						
					$this->email->subject($subject);
					$this->email->message($body);
					if($this->email->send())
					{
						$this->session->set_flashdata('success', 'New password has been sent to your email id.');
					}
					else
					{
						$this->session->set_flashdata('error', show_error($this->email->print_debugger()));
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'Email Does Not Exist.');
				}
				redirect('user/forgotpassword');
			}
			else
			{
				$data['metaKeywords'] = "Member Forgot Pasword";		
				$data['member']=$this->members_model->get_member_info($this->session->userdata('member_id'));		
				$this->load->view('user/forgotpassword',$data);
			} 					
		}

		function delete($id)
		{	
			$id = $this->session->userdata('member_id');
			$this->members_model->delete($id);
			redirect('user/account');		
		}
		
		function registration_response(){
			$this->load->view('user/registration_success');
		}
	


		
	

} 
/* End of file user.php */


