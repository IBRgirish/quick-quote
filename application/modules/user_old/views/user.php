<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<script type="text/javascript">
function select_uw(id) 
{
	$('#chkUnderWriter_'+id).prop('checked', true);
} 

function check_pri(id)
{
	if ($("#optUnderWriter_"+id).prop("checked") == true) {
		$('#chkUnderWriter_'+id).prop('checked', true);
	}
}
</script>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');
	$flash_success = '';
if ($this->session->flashdata('error')){    
		//echo '<div class="error"><p>'.$this->session->flashdata('error').'</p></div>';
		$flash_error = $this->session->flashdata('error');
	}
	if ($this->session->flashdata('success')){    
		$flash_success = '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; 
	}
	// Validation Errors
	  $error_message = (validation_errors()?'<div class="error">'.validation_errors().'</div>':'');
	  
    if(!empty ($error_message)) {
		$error_message_div = '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$error_message.'</div>';
	} else {
		$error_message_div = '';
	}	
	
    if(!empty ($flash_error)) {
		$error_message_div .= '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$flash_error.'</div>';
	} 
}


	
	$id	=isset($member->id)?$member->id :$this->session->userdata('member_id');
	$email = isset($_POST['email']) ? $_POST['email']  :(isset($member->email)?$member->email: NULL);
	$password = isset($_POST['password']) ? $_POST['password'] :(isset($member->password)?$member->password : NULL);
	$first_name = isset($_POST['txtFname']) ? $_POST['txtFname'] : (isset($member->first_name)?$member->first_name : NULL);
	//$middle_name = isset($_POST['txtMname']) ? $_POST['txtMname'] : (isset($member->middle_name)?$member->middle_name : NULL);
	//$last_name = isset($_POST['txtLname']) ? $_POST['txtLname']  :(isset($member->last_name)?$member->last_name : NULL);	
	$address = isset($_POST['txaAddress']) ? $_POST['txaAddress'] :(isset($member->address)?$member->address : NULL);
	$country_id= isset($_POST['country']) ? $_POST['country'] :( isset($member->country)?$member->country : NULL);
	$state= isset($_POST['state']) ? $_POST['state']:( isset($member->state)?$member->state : NULL);
	$city = isset($_POST['city']) ? $_POST['city'] :( isset($member->city)?$member->city : NULL);
	$zip_code = isset($_POST['zip_code']) ? $_POST['zip_code']  :( isset($member->zip_code)?$member->zip_code : NULL); 
	$zip_code_extd = isset($_POST['txtZipExtd']) ? $_POST['txtZipExtd']  :( isset($member->zip_code_extd)?$member->zip_code_extd : NULL);
	$phone = isset($_POST['phone']) ? $_POST['phone']  :( isset($member->phone_number)?$member->phone_number: NULL);
	$extension = isset($_POST['txtExtension']) ? $_POST['txtExtension']  :( isset($member->phone_ext)?$member->phone_ext: NULL);
	$cell_number = isset($_POST['txtCellNumber']) ? $_POST['txtCellNumber']  :( isset($member->cell_number)?$member->cell_number: NULL);
	$fax_number = isset($_POST['txtFaxNumber']) ? $_POST['txtFaxNumber']  :( isset($member->fax_number)?$member->fax_number: NULL);
	$sec_email = isset($_POST['txtSecEmail']) ? $_POST['txtSecEmail']  :( isset($member->sec_email)?$member->sec_email: NULL);
	$website = isset($_POST['txtWebsite']) ? $_POST['txtSecEmail']  :( isset($member->website)?$member->website: NULL);
?>

<div class="content">


  <!--div class="page-header">
    <h1>Edit Profile</h1>
  </div-->
   
   
   	
<div class="row">
    <div class="span4 offset4">
		<div class="well">
		<legend>Edit Profile</legend>
			<form action="<?php echo base_url('user/accountedit')?>" method="post" id="manageaccount_add_edit" name="manageaccount_add_edit" >
			<?php echo $error_message_div ?>
			


   <form action="<?php echo base_url('user/accountedit')?>" method="post" id="manageaccount_add_edit" name="manageaccount_add_edit" >
      <div>
       
        <div class="account form-wrapper" id="edit-account">
          <div id="account-login-container">
            <div id="edit-account-login" class="form-wrapper">
              
			  <div class="">
                <label for="email">E-mail </label>
                <input class="form-text" size="60" maxlength="128" type="text" name="email" id="email" value="<?php echo $email ?>" disabled />
              </div> <!-- Email * -->
			  
              <!--div class="">
                <label for="password">Password </label-->
                <input class="form-text" size="60" maxlength="128" type="hidden" name="password" id="password" value=""/>
              <!--/div--> <!-- Password * -->
			  
              <!--div class="">
                <label for="confirm_password">Re-Password</label-->
                <input  class="form-text" size="60" maxlength="128"  type="hidden" name="confirm_password" id="confirm_password" value=""/>
              <!--/div--> <!-- Confirm Password * -->
			  
              <div class="">
                <label for="txtFname">Agency Name <span class="form-required" title="This field is required.">*</span></label>
                <input name="txtFname" type="text" class="form-text" size="60" maxlength="128" value="<?php echo $first_name ?>" >
              </div> <!-- First Name * -->
			  
            </div>
			
			<!--<div class="">
              <label for="txtLname">Middle Name </label>
              <input name="txtMname" class="form-text" type="text" size="60" maxlength="128" value="< ?php echo $middle_name ?>" >
            </div>--> <!-- Middle Name -->
			
            <!--<div class="">
              <label for="txtLname">Last Name <span class="form-required" title="This field is required.">*</span></label>
              <input name="txtLname" class="form-text" type="text" size="60" maxlength="128" value="< ?php echo $last_name ?>" >
            </div>--> <!-- Last Name * -->
			
            <div class="">
              <label for="txtcompany">Address <span class="form-required" title="This field is required.">*</span></label>
              <textarea  class="form-textarea" name="txaAddress" id="txaAddress"><?php echo $address ?></textarea>
            </div> <!-- Address * -->
			
			<div>
              <label for="txtCountry">Country <span class="form-required" title="This field is required.">*</span></label>
              <?php 
  $country_table = $this->config->item('country_table');
			$country=form_simple_dropdown_from_db_valuebyname('txtCountry', "SELECT id,country_name FROM ".$country_table." where status=1", $country_id, 'Choose', 'class="select_input"');
			echo $country;
 ?>
            </div> <!-- Country * -->
			
			<div>
              <label for="txtState">state <span class="form-required" title="This field is required.">*</span></label>
              <input name="txtState"  id="txtState" class="form-text" type="text" size="30" maxlength="128" value="<?php echo $state ?>"  >
            </div> <!-- State * -->
			
			<div class="">
              <label for="txtCity">City <span class="form-required" title="This field is required.">*</span></label>
              <input name="txtCity"  id="txtCity" class="form-text" type="text" size="30" maxlength="128"  value="<?php echo $city ?>" >
            </div> <!-- City * -->
			
			<div class="">
              <label for="txtZip">Zip Code <span class="form-required" title="This field is required.">*</span></label>
              <input name="txtZip"  id="txtZip" maxlength="5" class="form-text" type="text" size="10" value="<?php echo $zip_code ?>" >
            </div> <!-- Zip Code * -->
			
			<div class="">
              <label for="txtZipExtd">Zip Code Extended <span class="form-required" title="This field is required.">*</span></label>
              <input name="txtZipExtd"  id="txtZipExtd" maxlength="5" class="form-text" type="text" size="10" value="<?php echo $zip_code_extd ?>" />
            </div> <!-- Zip Code Extended -->
			
            <div class="">
              <label for="txtPhone">Phone Number <span class="form-required" title="This field is required.">*</span></label>
              <input name="txtPhone" class="form-text" type="text" size="30" maxlength="128" value="<?php echo $phone ?>"/>
            </div> <!-- Phone Number * -->
			
            <div class="">
              <label for="txtExtension">Phone Extension </label>
              <input name="txtExtension" class="form-text" id="txtExtension" type="text" size="30" maxlength="6" value="<?php echo $extension ?>" >
            </div> <!-- Phone Extension --> 
			
			<div class="">
              <label for="txtCellNumber">Cell Number </label>
              <input name="txtCellNumber" class="form-text" id="txtCellNumber" type="text" size="30" maxlength="128" value="<?php echo $cell_number ?>" >
            </div> <!-- Cell Number -->
			
			<div class="">
              <label for="txtFaxNumber">Fax Number </label>
              <input name="txtFaxNumber" class="form-text" id="txtFaxNumber" type="text" size="30" maxlength="128" value="<?php echo $fax_number ?>" >
            </div> <!-- Fax Number -->
			
			<div class="">
              <label for="txtSecEmail">Secondary Email </label>
              <input name="txtSecEmail" class="form-text" id="txtSecEmail" type="text" size="30" maxlength="128" value="<?php echo $sec_email ?>" >
            </div> <!-- Secondary Email -->
			
			<div class="">
              <label for="txtWebsite">Website </label>
              <input name="txtWebsite" class="form-text" id="txtWebsite" type="text" size="30" maxlength="128" value="<?php echo $website ?>" >
            </div> <!-- Website -->
            
            <div>
                <label for="chkUnderWriter">Underwriter Assigned <span class="form-required" title="This field is required.">*</span></label>
                <?php
				if ($underWriter['flag']==1)
				{
					echo '<table border="0" width="100%">';
					echo '<tr>';
					echo '<th width="80%" align="left">Underwriter</th>';
					echo '<th width="20%" align="left">Primary</th>';
					echo '</tr>';
					$count=0;
					echo '<tr><td colspan="2">';
					echo '<div style="height:170px; overflow-y:scroll; overflow-x:hidden" >';
					echo '<table width="100%">';
					foreach($underWriter['resultData'] as $val)
					{
						$count++;
						
						list($primary, $underwriter) = explode(':',$member->underwriter);
						$uwData = explode(',',$underwriter);
						
						$chk = '';
						if(in_array($val['id'],$uwData)) {
							$chk='checked';
						}
						
						$prim = '';
						if ($val['id'] == $primary) {
							$prim = 'checked="checked"';
						}
						
						echo '<tr>';
						echo '<td><input type="checkbox" id="chkUnderWriter_'.$count.'" name="chkchkUnderWriter[]" value="'.$val['id'].'" '.$chk.' onChange="check_pri('.$count.')" />
						&nbsp;<label for="chkUnderWriter_'.$count.'" style="display:inline" >'.$val['name'].'</label></td>';
						echo '<td><input type="radio" id="optUnderWriter_'.$count.'" name="optUnderWriter" value="'.$val['id'].'" '.$prim.' onChange="select_uw('.$count.');" /></td>';
						echo '</tr/>';
					}
					echo '</table>';
					echo '</div>';
					echo '</td></tr>';
					echo '</table>';
				}
                ?>
              </div>
			
			<input type="hidden" name="members_id" id="members_id" value="<?php echo $id?>" >
            <input type="hidden" name="subscription_id" id="subscription_id" value="1" >
            <input type="hidden" name="email" id="email" value="<?php echo $email?>"/>
            <input type="hidden" name="savepassword" id="savepassword" value="<?php echo $password?>" >
          </div>
        </div>
        <p>&nbsp;</p>
      </div>
      </div>
	   <button class="btn btn-primary"  id="edit_memberinfo" name="edit_memberinfo" type="submit">Update</button>
	   
      </div>
      </div>

    			</form>
		</div>
  
</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>
