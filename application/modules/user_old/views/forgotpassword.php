<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<?php 
	if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
	{	
		$this->load->view('includes/header');	
	} 
?>

 <div class="content">

<?php
	
$flash_success = '';
if ($this->session->flashdata('error')){    
		//echo '<div class="error"><p>'.$this->session->flashdata('error').'</p></div>';
		$flash_error = $this->session->flashdata('error');
	}
	if ($this->session->flashdata('success')){    
		$flash_success = '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; 
	}
	// Validation Errors
	  $error_message = (validation_errors()?'<div class="error">'.validation_errors().'</div>':'');
	  
    if(!empty ($error_message)) {
		$error_message_div = '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$error_message.'</div>';
	} else {
		$error_message_div = '';
	}	
	
    if(!empty ($flash_error)) {
		$error_message_div .= '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$flash_error.'</div>';
	} 
	
?>
<div id="content" >
     <?php 	
		  
	$forgot_password=isset($forgot_password) ? $forgot_password:'';
	$email = isset($_POST['email']) ? $_POST['email'] : NULL;
	?>
         
	 <div class="row">
    <div class="span4 offset4">
		<div class="well">
		<legend>Forgot Password</legend>
			  <form id="frmForgotPassword" name="frmForgotPassword" method="post" action="<?php echo base_url(uri_string());?>">
				<?php echo $flash_success ?>
				<?php echo $error_message_div ?>
			<input class="span3" placeholder="Email" name="email" type="text" id="email" value="<?php echo set_value('txtEmail');?>">
			
			<button class="btn btn-primary"name="forgot_password" type="submit">Retrive Password !</button>
			</form>
		</div>
    </div>
</div>
</div>

<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>
