<?php $this->load->view('includes/header'); ?>
<script type="text/javascript">
function select_uw(id) 
{
	$('#chkUnderWriter_'+id).prop('checked', true);
} 

function check_pri(id)
{
	if ($("#optUnderWriter_"+id).prop("checked") == true) {
		$('#chkUnderWriter_'+id).prop('checked', true);
	}
}
$(document).ready(function(e) {
    $('input, textarea').click(function(){
		$(this).removeClass('error');	
	});
});
</script>
<?php 
	$id	=isset($member->id)?$member->id :0;	

if(isset($member->id) != 0){
if(!$this->uri->slash_segment(2) || $this->uri->slash_segment(2) != 'register/'){
	redirect(base_url());
}elseif($this->uri->segment(3) != 'onfanlycy'){
	redirect(base_url());
}
}
	$password = isset($_POST['password']) ? $_POST['password'] : NULL;
	$email = isset($_POST['email']) ? $_POST['email'] : NULL;
	$txtCell1 = isset($_POST['txtCell1']) ? $_POST['txtCell1'] : NULL;
	$txtCell2 = isset($_POST['txtCell2']) ? $_POST['txtCell2'] : NULL;
	$txtCell3 = isset($_POST['txtCell3']) ? $_POST['txtCell3'] : NULL;

	$txtFax1 = isset($_POST['txtFax1']) ? $_POST['txtFax1'] : NULL;
	$txtFax2 = isset($_POST['txtFax2']) ? $_POST['txtFax2'] : NULL;
	$txtFax3 = isset($_POST['txtFax3']) ? $_POST['txtFax3'] : NULL;
	
	$txtWebsite = isset($_POST['txtWebsite']) ? $_POST['txtWebsite'] : NULL;
	
	$txtZipCodeExtd = isset($_POST['txtZipCodeExtd']) ? $_POST['txtZipCodeExtd'] : NULL;
	
	$txtSecEmail = isset($_POST['txtSecEmail']) ? $_POST['txtSecEmail'] : NULL;
	
	$country_id= isset($_POST['country']) ? $_POST['country'] : 'United States';
			
			
				$flash_success = '';
if ($this->session->flashdata('error')){    
		//echo '<div class="error"><p>'.$this->session->flashdata('error').'</p></div>';
		$flash_error = $this->session->flashdata('error');
	}
	if ($this->session->flashdata('success')){    
		$flash_success = '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; 
	}
	// Validation Errors
	  $error_message = (validation_errors()?'<div class="error">'.validation_errors().'</div>':'');
	  
    if(!empty ($error_message)) {
		$error_message_div = '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$error_message.'</div>';
	} else {
		$error_message_div = '';
	}	
	
    if(!empty ($flash_error)) {
		$error_message_div .= '<div class="alert alert-error"><a class="close" data-dismiss="alert" href="#">x</a>'.$flash_error.'</div>';
	} 
	
?>
<style>
input {
	height:30px !important;
}
</style>

<div class="content">
  <div class="row">
    <div class="span10 offset1">
      <div class="well">
      <legend>Registration Form ..</legend>
      <?php echo $error_message_div ?>
      <form action="<?php echo base_url('user/processform')?>" method="post" id="commerce-checkout-form-checkout" accept-charset="UTF-8">
        <div>
          <h4 class="no_ribbon_1"> Set up your account information</h4>
        </div>
        <div class="account form-wrapper" id="edit-account">
          <div id="account-login-container">
            <div id="edit-account-login" class="form-wrapper">
              <div>
                <input type="hidden" value="<?php echo $id ?>" id="registration_id" name="registration_id"/>
              </div>
              <div class="row-fluid">
              	<div class="span3">
                    <label for="email">Agency Name <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" value="<?php echo set_value('txtFirstName')?>" id="txtFirstName" name="txtFirstName" class="text-input form-text span12 <?php if (form_error('txtFirstName')) { echo 'error'; } ?>" size="60" maxlength="128" />
                    <?php //echo form_error('txtFirstName'); ?>
                </div>
              <!-- First Name * -->
              <!--<div>
                          <label for="email">Middle Name </label>
                          <input type="text" value="< ?php echo set_value('txtMiddleName')?>" id="txtMiddleName" name="txtMiddleName" class="text-input form-text" size="60" maxlength="128" />
                        </div>-->
              <!-- Middle Name -->
              <!--<div>
                          <label for="email">Last Name <span class="form-required" title="This field is required.">*</span></label>
                          <input type="text" value="< php echo set_value('txtLastName')?>" id="txtLastName" name="txtLastName" class="validate[required] text-input form-text" size="60" maxlength="128" />
                        </div>-->
              <!-- Last Name * -->
                  <div class="span9">
                    <label for="email">Address <span class="form-required" title="This field is required.">*</span></label>
                    <textarea id="txaAddress" name="txaAddress" class="validate[required] span12 <?php if (form_error('txaAddress')) { echo 'error'; } ?>"><?php echo set_value('txaAddress')?>
    </textarea>
                  </div>
                  <!-- Address * -->
                  
              </div>
             
             <div class="row-fluid">
             		<div class="span3">
                    <label for="txtCountry">Country <span class="form-required" title="This field is required.">*</span></label>
                    <?php $country_id = 'United States';/*set_value('txtCountry');*/
                                $country_table = $this->config->item('country_table');
                                $country=form_simple_dropdown_from_db_valuebyname('txtCountry', "SELECT id,country_name FROM ".$country_table." where status=1", $country_id, 'Choose', 'onChange="getState(this.value)" class="select_input span12"');
                                echo $country;
     ?>
                  </div>
                  <!-- Country * -->
                  <div class="span2 sts_select" class="sts_select">
                    <label for="email">State<span class="form-required" title="This field is required.">*</span></label>
                    <?php $name='txtState'; echo get_state_dropdown($name, '', 'class="span12"'); ?>
                    <!--<input type="text" value="< ?php echo set_value('txtState')?>" id="txtState" name="txtState" class="validate[required] text-input form-text" size="60" maxlength="128" />-->
                  </div>
                  <script>
				  $(document).ready(function(){
				  	$('.sts_select select').removeClass('span2');
				  	$('.sts_select select').addClass('span12');
				 });
				  </script>
                  <!-- State * -->
                  <div class="span2">
                    <label for="email">City <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" value="<?php echo set_value('txtCity')?>" id="txtCity" name="txtCity" class="validate[required] text-input form-text span12 <?php if (form_error('txtCity')) { echo 'error'; } ?>" size="60" maxlength="128" />
                  </div>
                  <!-- City * -->
                  <div class="span2">
                    <label for="email">Zip Code <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" value="<?php echo set_value('txtZipCode')?>" id="txtZipCode" name="txtZipCode" class="validate[required] text-input form-text span12 <?php if (form_error('txtZipCode')) { echo 'error'; } ?>" size="60" maxlength="5" />
                  </div>
                  <!-- Zip Code * -->
                      <div class="span3">
                    <label for="email">Zip Code Extended </label>
                    <input type="text" value="<?php echo isset($txtZipCodeExtd) ? $txtZipCodeExtd : ''; ?>" id="txtZipCodeExtd" name="txtZipCodeExtd" class="text-input form-text span12 <?php if (form_error('txtZipCodeExtd')) { echo 'error'; } ?>" size="60" maxlength="5" />
                  </div>
                  <!-- Zip Code Extended -->
                
                 
                <!--  <div class="span2">
                  	  <label for="extension">Extension <span class="form-required" title="This field is required.">*</span></label>
                  	  <input placeholder="Ext" type="text" value="<?php set_value('txtPhoneExt')?>" id="txtPhoneExt" name="txtPhoneExt" class="text-input form-text span12" maxlength="5" />
                  </div>-->
                  <!-- Phone Extension -->
              </div>
              <!--div>
                          <label for="email"> </label>
                          <input type="text" value="< ?php echo set_value('txtPhoneExt')?>" id="txtPhoneExt" name="txtPhoneExt" class="text-input form-text" size="60" maxlength="6" />
                        </div-->
              <div class="row-fluid">
              		 <div class="span4">
                    <label for="email">Phone Number <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" value="<?php echo set_value('txtPhArea')?>" id="txtPhoneNumber" name="txtPhArea" class="validate[required] text-input form-text span3 <?php if (form_error('txtPhArea')) { echo 'error'; } ?>" size="60" maxlength="3" />
                    <input type="text" value="<?php echo set_value('txtPhone1')?>" id="txtPhone1" name="txtPhone1" class="validate[required] text-input form-text span3 <?php if (form_error('txtPhone1')) { echo 'error'; } ?>" maxlength="3" />
                    <input type="text" value="<?php echo set_value('txtPhone2')?>" id="txtPhone2" name="txtPhone2" class="validate[required] text-input form-text span3 <?php if (form_error('txtPhone2')) { echo 'error'; } ?>" maxlength="4"  />
                    <input type="text" value="<?php echo set_value('txtPhone3')?>" id="txtPhone3" name="txtPhone3" class="validate[required] text-input form-text span3 <?php if (form_error('txtPhone3')) { echo 'error'; } ?>" maxlength="5" />                  
                  </div>
                  <!-- Phone Number * -->
                  <div class="span3">
                    <label for="email">Cell Number </label>
                    <!--<input type="text" value="< ?php echo set_value('txtCellNumber')?>" id="txtCellNumber" name="txtCellNumber" class="text-input form-text" size="60" maxlength="100" />-->
                    <input type="text" value="<?php echo isset($txtCell1) ? $txtCell1 : ''; ?>" id="txtCell1" name="txtCell1" class="validate[required] text-input form-text span4 <?php if (form_error('txtCell1')) { echo 'error'; } ?>" maxlength="3"  />
                    <input type="text" value="<?php echo isset($txtCell2) ? $txtCell2 : ''; ?>" id="txtCell2" name="txtCell2" class="validate[required] text-input form-text span4 <?php if (form_error('txtCell2')) { echo 'error'; } ?>" maxlength="3"  />
                    <input type="text" value="<?php echo isset($txtCell3) ? $txtCell3 : ''; ?>" id="txtCell3" name="txtCell3" class="validate[required] text-input form-text span4 <?php if (form_error('txtCell3')) { echo 'error'; } ?>" maxlength="4" />
                    <!-- input placeholder="Ext" type="text" value="<?php set_value('txtCellExt')?>" id="txtCellExt" name="txtCellExt" class="validate[required] text-input form-text" maxlength="5" style="width:45px !important" /-->
                  </div>
                  <!-- Cell Number -->
                  <div class="span3">
                    <label for="email">Fax Number </label>
                    <!--<input type="text" value="< ?php echo set_value('txtFaxNumber')?>" id="txtFaxNumber" name="txtFaxNumber" class="text-input form-text" size="60" maxlength="128" />-->
                    <input type="text" value="<?php echo isset($txtFax1) ? $txtFax1 : ''; ?>" id="txtFax1" name="txtFax1" class="validate[required] text-input form-text span4 <?php if (form_error('txtFax1')) { echo 'error'; } ?>" maxlength="3"  />
                    <input type="text" value="<?php echo isset($txtFax2) ? $txtFax2 : ''; ?>" id="txtFax2" name="txtFax2" class="validate[required] text-input form-text span4 <?php if (form_error('txtFax2')) { echo 'error'; } ?>" maxlength="3" />
                    <input type="text" value="<?php echo isset($txtFax3) ? $txtFax3 : ''; ?>" id="txtFax3" name="txtFax3" class="validate[required] text-input form-text span4 <?php if (form_error('txtFax3')) { echo 'error'; } ?>" maxlength="4"  />
                   <!-- <input placeholder="Ext" type="text" value="<?php set_value('txtFaxExt')?>" id="txtFaxExt" name="txtFaxExt" class="validate[required] text-input form-text" maxlength="5" style="width:45px !important" />-->
                  </div>
                  <!-- Fax Number -->
                 
                   <div class="span2">
                    <label for="txtSecEmail">Website </label>
                    <input type="text" value="<?php echo isset($txtWebsite) ? $txtWebsite : ''; ?>" id="txtWebsite" name="txtWebsite" class="text-input form-text span12" size="60" maxlength="128" />
                  </div>
                  <!-- Web Site -->
                 
              </div>
                 
              
              <div class="row-fluid">
              		 <div class="span3">
                    <label for="email">E-mail <span class="form-required" title="This field is required.">*</span></label>
                    <input type="text" value="<?php echo set_value('email')?>" id="email" name="email" class="validate[required,custom[email]] text-input form-text span12 <?php if (form_error('email')) { echo 'error'; } ?>" size="60" maxlength="128" />
                  </div>
                  <!-- Email * -->
              		 <div class="span3">
                    <label for="password">Password <span class="form-required" title="This field is required.">*</span></label>
                    <input type="password" id="password" value="<?php echo set_value('password')?>" name="password" class="validate[required,minSize[6]] text-input form-text span12 <?php if (form_error('password')) { echo 'error'; } ?>" size="60" maxlength="128"/>
                  </div>
                  <!-- Password * -->
                  <div class="span3">
                    <label for="cpassword">Re-Password<span class="form-required" title="This field is required.">*</span></label>
                    <input type="password" value="<?php echo set_value('cpassword')?>" id="cpassword" name="cpassword" class="validate[required,equals[password]] text-input form-text span12 <?php if (form_error('cpassword')) { echo 'error'; } ?>" size="60" maxlength="128" />
                  </div>
                  <!-- Confirm Password * -->
                  <div class="span3">
                    <label for="txtSecEmail">Secondary Email </label>
                    <input type="text" value="<?php echo isset($txtSecEmail) ? $txtSecEmail : ''; ?>" id="txtSecEmail" name="txtSecEmail" class="validate[custom[email]] text-input form-text span12 <?php if (form_error('txtSecEmail')) { echo 'error'; } ?>" size="60" maxlength="128" />
                  </div>
                  <!-- Secondary Email -->
                
              </div>
              <!--div>
                <label for="chkUnderWriter">Underwriter Assigned <span class="form-required" title="This field is required.">*</span></label>
                <?php
				if ($underWriter['flag']==1)
				{
					echo '<table border="0" width="100%" style="height:20px; overflow-x:hidden;">';
					echo '<tr>';
					echo '<th width="80%" align="left">Underwriter</th>';
					echo '<th width="20%" align="left">Primary</th>';
					echo '</tr>';
					$count=0;
					echo '<tr><td colspan="2">';
					echo '<div style="height:170px; overflow-y:scroll; overflow-x:hidden" >';
					echo '<table width="100%">';
					foreach($underWriter['resultData'] as $val)
					{
						$count++;
						
						$chk = '';
						if ($count==1) {
							$chk = 'checked="checked"';
						}
						
						echo '<tr>';
						echo '<td><input type="checkbox" id="chkUnderWriter_'.$count.'" name="chkchkUnderWriter[]" value="'.$val['id'].'" '.$chk.' onChange="check_pri('.$count.')" />
						&nbsp;<label for="chkUnderWriter_'.$count.'" style="display:inline">'.$val['name'].'</label></td>';
						echo '<td><input type="radio" id="optUnderWriter_'.$count.'" name="optUnderWriter" value="'.$val['id'].'" '.$chk.' onChange="select_uw('.$count.');" /></td>';
						echo '</tr/>';
					}
					echo '</table>';
					echo '</div>';
					echo '</td></tr>';
					
					echo '</table>';
				}
                ?>
              </div-->
              <!-- Underwriter -->
            </div>
          </div>
        </div>
        <p>&nbsp;</p>
        </div>
        <p class="italics privacy_policy_desc">By completing this form, you agree to our <a class="light_blue" href="javascript:void(0)">Terms of Use</a> and <a class="light_blue" href="javascript:void(0)">Privacy Policy</a></p>
        <hr>
        <button class="btn btn-primary"  id="edit-continue" name="op" type="submit">Create Account</button>
      </form>
    </div>
  </div>
</div>
<script>
$('[name="txtState"]').attr('class', 'span2');
</script>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>
