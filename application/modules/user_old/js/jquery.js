function IsElementExist(ElementName, ElementType) {
  if( ElementType === 'radio' )  ElementType = 'input';
  return ( $(ElementType+'[name="'+ElementName+'"]').length > 0 );
} // IsElementExist()


function GetValue(ElementName, ElementType) {
  if( ElementType === 'radio' )  ElementType = 'input';
  ElementType = SetDefaultValue(ElementType, 'input');
  if( IsElementExist(ElementName, ElementType) )
    return $(ElementType+'[name="'+ElementName+'"]').val();
  return '';
} // GetValue()


function SetDefaultValue(Variable, DefaultValue) {
  if(Variable === undefined)
    return DefaultValue;
  return Variable;
} // SetDefaultValue()


function ValidateField(ElementName, ElementType, DefaultValue, DoHighlight, ErrorClass) {
  ElementType = SetDefaultValue(ElementType, 'input');
  DefaultValue = SetDefaultValue(DefaultValue, '');
  DoHighlight = SetDefaultValue(DoHighlight, true);
  ErrorClass = SetDefaultValue(ErrorClass, 'input-error');

  if( IsElementExist(ElementName, ElementType) ) {
    var obj = $(ElementType+'[name="'+ElementName+'"]');
    if( obj.val() == DefaultValue ) {
      if( DoHighlight )
        obj.addClass(ErrorClass);
    } else {
      if( DoHighlight )
        obj.removeClass(ErrorClass);
      return true;
    }
  }
  return false;
} // ValidateField()


function ValidatePhoneNumber(Country, ElementName, ElementNamePrefix, ElementNameSuffix) {
  ElementNamePrefix = SetDefaultValue(ElementNamePrefix, ElementName+'_prefix');
  ElementNameSuffix = SetDefaultValue(ElementNameSuffix, ElementName+'_suffix');
  var validated = false;
  
  switch(Country) {
    case 'United States':
    case 'Canada':
      validated = ValidateField(ElementName);
      validated = ValidateField(ElementNamePrefix) && validated;
      validated = ValidateField(ElementNameSuffix) && validated;
      break;
  
    default:
      validated = ValidateField(ElementName);
      break;
  }
  return validated;
} // ValidatePhoneNumber()


function ValidatePassword(ElementName, ElementNameConfirm, ErrorClass) {
  ElementNameConfirm = SetDefaultValue(ElementNameConfirm, ElementName+'_confirm');
  ErrorClass = SetDefaultValue(ErrorClass, 'input-error');
  var validated = false;
  
  if( ValidateField(ElementName, 'input', '', true, ErrorClass) & ValidateField(ElementNameConfirm, 'input', '', true, ErrorClass) ) {
    var value1 = GetValue(ElementName);
    var value2 = GetValue(ElementNameConfirm);
    
    if( value1 == value2 ) {
      return true;
    } else {
      var obj1 = $(ElementType+'[name="'+ElementName+'"]');
      var obj2 = $(ElementType+'[name="'+ElementName+'"]');
      
      obj1.addClass(ErrorClass);
      obj2.addClass(ErrorClass);
    }
  }
  return false;
} // ValidatePassword()

  function Validatecountry() {  
  var validated = true; 
   $('#txtCountry').addClass('input-error');
      validated = false;
  
 
  
  /* {
    validated = ValidateInput('txtCountry') && validated;
    var country = $('input[name=txtCountry]').val();
    if(validated && password != passwordConfirm) {
      $('input[name=txtCountry]').addClass('input-error');
      validated = false;
    }
  } */
  return validated;
} // Validatecountry()

  function Validaterequired() {  
  var validated = true; 
  //validated = ValidateInput('txtFname') && validated;
    if(!ValidateInput('txtFname')){
	   $('#txtFname').addClass('input-error');
		validated = false;
	}
	
	if(!ValidateInput('txtLname')){
	   $('#txtLname').addClass('input-error');
		validated = false;
	}
	
	if(!ValidateInput('txtCompany')){
	   $('#txtCompany').addClass('input-error');
		validated = false;
	}
	
	if(!ValidateInput('txtAddress')){
	   $('#txtAddress').addClass('input-error');
		validated = false;
	}
	
	if(!ValidateInput('txtCity')){
	   $('#txtCity').addClass('input-error');
		validated = false;
	}
	
	if(!ValidateInput('txtZip')){
	   $('#txtZip').addClass('input-error');
		validated = false;
	}
	
	if($('#txtCountry').val() == "United%20States"){ 
			validated_state = ValidateInput('txtState', 'select', true, 0);
		if(!validated_state){
		   $('#txtState').addClass('input-error');
			validated = false;
		}
	}
	
		if($('#txtCountry').val() == "Canada"){ 
			validated_state = ValidateInput('txtState', 'select', true, 0);
		if(!validated_state){
		   $('#txtState').addClass('input-error');
			validated = false;
		}
	}
	
	
  
 
  
  /* {
    validated = ValidateInput('txtCountry') && validated;
    var country = $('input[name=txtCountry]').val();
    if(validated && password != passwordConfirm) {
      $('input[name=txtCountry]').addClass('input-error');
      validated = false;
    }
  } */
  return validated;
} // Validatecountry()

 function Validatecc() {  
  var validated = true; 
  //validated = ValidateInput('txtFname') && validated;
    if(!ValidateInput('cnumber')){
	   $('#cnumber').addClass('input-error');
		validated = false;
	}  
	
	validated_month = ValidateInput('month', 'select', true, 0);
	if(!validated_month){
	   $('#month').addClass('input-error');
		validated = false;
	}
/*	
	if(!ValidateInput('year')){
	   $('#year').addClass('input-error');
		validated = false;
	} 
*/	
	validated_year = ValidateInput('year', 'select', true, 0);
	if(!validated_year){
	   $('#year').addClass('input-error');
		validated = false;
	}
	
	if(!validated){
	 $('#cctable').css('background-color','#FFCC99');
	}

	return validated;
} // Validatecc()

 function Validateterms() {  
  var validated = true; 
  //validated = ValidateInput('txtFname') && validated;
	if(!$('#checkbox').is(':checked') ) {
	 $('#terms_n_conditions').css('background-color','#FFCC99');
	 validated = false;
	 }
	return validated;
} // Validatecc()


function ValidatePrivacyPrefFull(Permission1, Permission2, Permission3) {
  if(Permission1 === undefined)  Permission1 = true;
  if(Permission2 === undefined)  Permission2 = true;
  if(Permission3 === undefined)  Permission3 = true;
  
  var validated = true;

  var SelectorArray = new Array();
  if( Permission1 )
    SelectorArray.push( 'permission_0' );
  if( Permission2 )
    SelectorArray.push( 'permission_1' );
  if( Permission3 )
    SelectorArray.push( 'permission_2' );
  
  var i = 0;
  while(i < SelectorArray.length && validated) {
    validated = ValidateInput(SelectorArray[i], 'radio', false);
    i++;
  }

  if(!validated)
    $('#PrivacyPreferences').css('background-color', '#FFCC99');
  return validated;
} // ValidatePrivacyPref1()


// Validate Privacy Preferences
function ValidatePrivacyPref(AllPermissions) {
  if(AllPermissions === undefined)  AllPermissions = true;
  var validated = true;

  var SelectorArray = new Array();
  if(AllPermissions) {
    SelectorArray[0] = 'permission_0';
    SelectorArray[1] = 'permission_1';
    SelectorArray[2] = 'permission_2';
  } else {
    SelectorArray[0] = 'permission_1';
    SelectorArray[1] = 'permission_2';
  }
  
  var i = 0;
  
  while(i < SelectorArray.length && validated) {
    validated = ValidateInput(SelectorArray[i], 'radio', false);
    i++;
  }
  
  if(!validated)
    $('#PrivacyPreferences').css('background-color', '#FFCC99');
  return validated;
} // ValidatePrivacyPref()


function IsEmailValid(email) {
  var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
  return pattern.test(email);
} // IsEmailValid()






// All selectors in this class are defined by name unless specified explicitly

function CheckOptionId(element_check, values, element_toggle) {
  element_check = GetElement(element_check);
  element_toggle = GetElement(element_toggle);
  element_toggle.style.display = 'none'; // Hide wrapper id
  var show = false;
  if(values == 'checked') {
    show = element_check.checked; // true if checked
  } else {
    var values_array = values.split(',');
    for(var i=0; i<values_array.length; i++) {
      if( element_check.value == values_array[i] ) {
        show = true;
        break;
      }
    }
  }
  if(show == true)
    element_toggle.style.display = ''; // Show wrapper id
} // CheckOptionId()

// Show or hide element depending on a select option
// element_check can be an object or an id (string) of an element
// values are comma delimited without any spaces
// element_toggle is the identifier of the element to toggle
function ToggleOptionId(element_check, values, element_toggle) {
  element_check = GetElement(element_check);
  element_toggle = GetElement(element_toggle);
  element_toggle.style.display = 'none'; // Hide wrapper id
  var show = false;
  if(values == 'checked') {
    show = element_check.checked; // true if checked
  } else {
    var values_array = values.split(',');
    for(var i=0; i<values_array.length; i++) {
      if( element_check.value == values_array[i] ) {
        show = true;
        break;
      }
    }
  }
  if(show == true)
    element_toggle.style.display = ''; // Show wrapper id
} // ToggleOptionId()


// Get element object depending on browser
// id can be an object or an id (string) of an element
function GetElement(id) {
  if( typeof(id)!='string' ) return id; // Return object
  var element = null;
  if(document.getElementById)
    element = document.getElementById(id);
  else if(document.all)
    element = document.all[id];
  else if(document.layers)
    element = document.layers[id];
  return element;
} // GetElement()


// Highlight
function Highlight(Selector, InputType, DoHighlight, CSSClass) {
  if(InputType === undefined)     InputType = 'input';
  if(DoHighlight === undefined)   DoHighlight = true;
  if(CSSClass === undefined) CSSClass = 'input-error';
  
  if(DoHighlight)
    $(InputType +'[name='+ Selector +']').addClass(CSSClass);
  else
    $(InputType +'[name='+ Selector +']').removeClass(CSSClass);
} // Highlight()


// Set selector to Value. If empty, then set to DefaultValue
// Don't set DefaultValue if don't want anything selected
// Selector is targetted by name
function SetSelection(SelectorType, Selector, Value, DefaultValue) {
  if(Value === undefined)     Value = '';

  switch(SelectorType) {
    case 'radio':
      if( Value != '' ) {
        $('input[name="' + Selector + '"]').filter('[value="' + Value + '"]').attr('checked', true);
      } else if(DefaultValue != '') {
        $('input[name="' + Selector + '"]').filter('[value="' + DefaultValue + '"]').attr('checked', true);
      } else if(DefaultValue === '') {
        $('input[name="' + Selector + '"]:nth(0)').attr('checked', true);
      }
      break;
      
    case 'option':
      if( Value != '') {
        $('select[name="' + Selector + '"]').val(Value);
      } else if(DefaultValue != '') {
        $('select[name="' + Selector + '"]').val(DefaultValue);
      }
      break;

    case 'checkbox':
      var new_values_array = Value.split(',');
      var new_values_array_length = new_values_array.length;
      
      if(new_values_array_length > 1) {
        for(var k=0; k<new_values_array_length; k++) {
          var new_value = new_values_array[k];
          $('input[name="' + Selector + '"]').each(function() {
            if(this.value == new_value)
              $(this).attr('checked', true);
          });
        }
      } else {
        if( Value !== '' ) {
          $('input[name="' + Selector + '"]').attr('checked', true);
        } else {
          $('input[name="' + Selector + '"]').attr('checked', false);
        }
      }
      break;
  }
} // SetSelection()


// Validate if input is empty and highlight the input (by default)
function ValidateInput(Selector, InputType, DoHighlight, EmptyValue) {
  if(InputType === undefined)     InputType = 'input';
  if(EmptyValue === undefined)    EmptyValue = '';
  if(DoHighlight === undefined)   DoHighlight = true;
  
  var validated = true;
  
  switch(InputType) {
    case  'radio' :
      var value = $('input[name=term]:checked').val();

      if($('input[name='+ Selector + ']:checked').length <= 0 || value == '')
        validated = false;
      break;
      
    default :
      var value = $.trim($(InputType +'[name='+ Selector +']').val());
      if(value == EmptyValue)
        validated = false;
      break;
  }
  
  if(!validated && DoHighlight)
    Highlight(Selector, InputType);
  return validated;
} // ValidateInput()


// Validate term being chose
function ValidateTerm() {
  var validated = ValidateInput('term', 'radio', false);
  
  if(!validated)
    $('#SubscriptionOffers').css('background-color', '#FFCC99');
  return validated;
} // CheckTerm()





// Validate Credit Card
function ValidateCreditCard() {
  var validated = true;
  var ccType = $('input[name=cc_type]:checked').val();
  if(ccType != 'Bill Me') {
    validated = ValidateInput('cc_number');
    validated = ValidateInput('cc_month', 'select', true, 0) && validated;
    validated = ValidateInput('cc_year', 'select', true, 0) && validated;
  }
  
  if(!validated)
    $('#PaymentInformation').css('background-color', '#FFCC99');
  return validated;
} // ValidateCreditCard()


// Validate billing and shipping address.
function ValidateAddress(Type) { alert('address valid 1');
  if(Type ===  undefined)    Type = '';
  alert('address valid 2');
  Type = Type.toLowerCase();
  
  var validated = true;
  var country = $('#'+ Type +'_country').val();
  
  validated = ValidateInput(Type +'txtCountry', 'select') && validated;
 // validated = ValidateInput(Type +'_name') && validated;
 // validated = ValidateInput(Type +'_job_title') && validated;
 // validated = ValidateInput(Type +'_company_name') && validated;
 // validated = ValidateInput(Type +'_address_line_1') && validated;
 // validated = ValidateInput(Type +'_city') && validated;
  
  if(country == 'United States' || country == 'Canada') {
   //validated = ValidateInput(Type +'_state', 'select') && validated;
    validated = ValidateInput(Type +'txtZip') && validated;
  }
  return validated;
} // ValidateAddress()


// Validate phone and fax number
function ValidatePhone(Type, Required) {
  if(Type ===  undefined)    Type = 'phone';
  if(Required === undefined) Required = false;
  
  Type = Type.toLowerCase();
  
  var country = $('#bill_country').val();
  
  if(country == 'United States' || country == 'Canada') {
    var areaCode  = $.trim($('input[name=bill_'+ Type +'_number_area_code]').val());
    var prefix    = $.trim($('input[name=bill_'+ Type +'_number_prefix]').val());
    var suffix    = $.trim($('input[name=bill_'+ Type +'_number_suffix]').val());
    
    if(!((areaCode == '' && prefix == '' && suffix == '') || (areaCode != '' && prefix != '' && suffix != ''))) {
      ValidateInput('bill_'+ Type +'_number_area_code');
      ValidateInput('bill_'+ Type +'_number_prefix');
      ValidateInput('bill_'+ Type +'_number_suffix');
      return false;
    } else if( Required && areaCode == '' && prefix == '' && suffix == '') {
      ValidateInput('bill_'+ Type +'_number_area_code');
      ValidateInput('bill_'+ Type +'_number_prefix');
      ValidateInput('bill_'+ Type +'_number_suffix');
      return false;
    }
  }
  return true;
} // ValidatePhone()


// Validate password and password confirmation
function ValidatePassword() { 
  var validated = true;
  {
    validated = ValidateInput('password') && validated;
    validated = ValidateInput('cpassword') && validated;
    
    var password = $('input[name=password]').val();
    var passwordConfirm = $('input[name=cpassword]').val();
    
    if(validated && password != passwordConfirm) {
      $('input[type=password]').addClass('input-error');
      validated = false;
    }
  }
  return validated;
} // ValidatePassword()




// Validate e-mail address
function ValidateEmail() {
  var validated = true;
  var email = $('input[name=email]').val();
  
  validated = ValidateInput('email') && validated;
  if(validated && !IsEmailValid(email)) {
    $('input[name=email]').addClass('input-error');
    validated = false;
  }
  return validated;
} // ValidateEmail()


function ValidateZipCode(InputChar, Type, Allows) {
  if( Allows === undefined )    Allows = '';
  if( Type === undefined )      Type = 'bill';
  Type = Type.toLowerCase();
  
  var prevents = "!@#$%^&*()+=[]\\\';,/{}|\":<>?~`.\_- ";
  switch( $('select[name=' + Type + '_country]').val() )
    {
      case 'United States':
        var alpha = 'abcdefghijklmnopqrstuvwxyz';
        alpha += alpha.toUpperCase();
        prevents += alpha;
      case 'Canada':
        break;
      default:
        prevents = '';
        break;
    }
    
  s = Allows.split('');
  for(i = 0; i < s.length; i++)
    if(prevents.indexOf(s[i]) != -1)
      s[i] = "\\" + s[i];
  Allows = s.join('|');

  var reg = new RegExp(Allows,'gi');
  var ch = prevents + Allows;
  ch = ch.replace(reg,'');
  
  if(ch.indexOf(k) != -1)
    return false;
  return true;
} // ValidateZipCode()


// Called when the checkbox for separate shipping address is checked
function ToggleShippingAddress() {
  if($('#ship_separate_address').is(':checked')) {
    $('#ship_country').val($('#bill_country').val());
    $('#ship_name').val($('#bill_name').val());
    $('#ship_job_title').val($('#bill_job_title').val());
    $('#ship_company_name').val($('#bill_company_name').val());
    $('#ship_address_line_1').val($('#bill_address_line_1').val());
    $('#ship_address_line_2').val($('#bill_address_line_2').val());
    $('#ship_address_line_3').val($('#bill_address_line_3').val());
    $('#ship_city').val($('#bill_city').val());
    $('#ship_state').val($('#bill_state').val());
    $('#ship_state_international').val($('#bill_state_international').val());
    $('#ship_postal_code').val($('#bill_postal_code').val());
    
    ChangeCountry('ship');
  
    $('#ship_separate_address_wrapper').show();
  } else {
    $('#ship_separate_address_wrapper').hide();
  }
} // ToggleShippingAddress()


// This function is being called every time the user changes the country
function ChangeCountry(Type) {
  if(Type ===  undefined)    Type = 'bill';
  
  Type = Type.toLowerCase();
  
  var country = $('select[name=' + Type + '_country]').val();

  switch(country) {
    case  'United States' :
    case  'Canada'        :
    case  ''              :
      if(Type == 'bill') {
	$('.domestic-required').show();
	$('.international-required').hide();
      }

      $('#'+ Type + '_state_wrapper').show();
      $('#'+ Type + '_phone_number_wrapper').show();
      $('#'+ Type + '_fax_number_wrapper').show();
      
      $('#'+ Type + '_state_international_wrapper').hide();
      $('#'+ Type + '_phone_number_international_wrapper').hide();
      $('#'+ Type + '_fax_number_international_wrapper').hide();
      $('#'+ Type + '_address_line_3_wrapper').hide();
        
      if(country == '') {
        $('.'+ Type + '_info').attr('disabled', true);
      } else {
        $('.'+ Type + '_info').attr('disabled', false);
        
        if(country == 'United States')
          $('input[name='+ Type + '_postal_code]').attr({size:5, maxlength:5});
        else
          $('input[name='+ Type + '_postal_code]').attr({size:6, maxlength:6});
      }  
      break;
      
    default   :
      if(Type == 'bill') {
	$('.domestic-required').hide();
	$('.international-required').show();
      }

      // Postal Code is not a required field
      $('input[name='+ Type + '_postal_code]').removeClass('input-error');
      
      $('.'+ Type + '_info').attr('disabled', false);
      
      $('#'+ Type + '_state_wrapper').hide();
      $('#'+ Type + '_phone_number_wrapper').hide();
      $('#'+ Type + '_fax_number_wrapper').hide();
      
      $('#'+ Type + '_state_international_wrapper').show();
      $('#'+ Type + '_phone_number_international_wrapper').show();
      $('#'+ Type + '_fax_number_international_wrapper').show();
      $('#'+ Type + '_address_line_3_wrapper').show();
      
      $('input[name='+ Type + '_postal_code]').attr({size:8, maxlength:8});
      break;
  }
} // ChangeCountry()


// This function is being called everytime the email is changed
function ChangeEmail() {
  $('input[type=password]').val('');
  $('.password').show();
  $('tr#email-validation-message').remove();
  
  var email = $.trim($('input[name=email]').val());
  if(IsEmailValid(email)) {    
    // database check
    $.post('..//lookup.php', {username: email}, function(data) {
      if(data == true) {
        $('input[type=password]').val('');
        $('.password').hide();
        
        if($('input[name=logged_in]').val() == '0') {
          $('tr#email').after('<tr id="email-validation-message" style="color:#900;"><td colspan="2" style="text-align:center;">There is an account associated with this e-mail. <br /><a href="javascript:;" class="btnShowLoginForm">Click here to login</a></td></tr>');
        }
      }
    });
  }
} // ChangeEmail()


// jQuery codes begin
$(document).ready(function() {
  // START - Country is changed
  $('select[name=bill_country]').change(function() {
    ChangeCountry();
    
    if(this.value == '')
      $('input[name=email]').focus();
    else
      $('input[name=bill_name]').focus();
  });
  ChangeCountry();    // For first time check
  //END - Country is changed
  
  // START - Email is changed  
  $('input[name=email]').change(function() {
    ChangeEmail();
  });
  ChangeEmail();    // For first time check
  //END - Email is changed
  
  // Remove highlight if a value is entered
  $('select, input').change(function() {
    if(this.value != '')  $(this).removeClass('input-error');
  });
  
  // Prevent certain chars in the zipcode (for US: only numeric)
  $('input[name=bill_postal_code]').keypress(function (e) {
    if (!e.charCode)
      k = String.fromCharCode(e.which);
    else
      k = String.fromCharCode(e.charCode);
    if(e.ctrlKey&&k=='v' || !ValidateZipCode(k))
      e.preventDefault();
    return true;
  });
  

  
  
  
  
  /*if($('input[name=showLoginForm]').val() == 1)
    tb_show("", "../login.php??KeepThis=true&TB_iframe=true&height=300&width=300", "");*/
  
  // show thickbox
  $('.btnShowLoginForm').live('click', function() {
    tb_show("", "../login.php??KeepThis=true&TB_iframe=true&height=300&width=300", "");
  });
});
